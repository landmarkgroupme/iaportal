<?php

  require __DIR__ . '/../vendor/autoload.php';
  use OpenCloud\Rackspace;

  // 1. Instantiate a Rackspace client.
  $client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
      'username' => 'lmguae',
      'apiKey'   => '1bc63b29f89abd24b1e4350b213c880b'));
  
  // 2. Obtain an Object Store service object from the client.
  $region = 'ORD';
  $objectStoreService = $client->objectStoreService(null, $region);
  
  // 3. Get container.
  $container = $objectStoreService->getContainer('intranet_photos');
  
  // 4.uploading the intranet uploaded photos directory to cloud
 // $container->uploadDirectory('/var/www/html/dev/public_html/phpalbums_new1');
  // Enabled the Debugging
  $container->enableLogging();
  
  // Get the list of all files in recursive . ListIn is custom function, Added at end.
  $fileList = ListIn('/var/www/html/v2/public_html/phpalbum/albums');
  
  $uploadFiles = array();
  $i = 0;
  
  foreach($fileList as $file) {
    $uploadFiles[$i]['name'] = $file; 
    $uploadFiles[$i]['path'] = '/var/www/html/v2/public_html/phpalbum/albums/'.$file; //Full path of images
    $i++;
  }
  
  // Dividing all files into 100 and upload the images on rackspace
  $bulkUploadFilesList = array_chunk($uploadFiles,100);
  foreach($bulkUploadFilesList as $bulkUpload) {
    $container->uploadObjects($bulkUpload);
  }
  
  /*** Function to get the all files list recursly **/
  function ListIn($dir, $prefix = '') {
    $dir = rtrim($dir, '\\/');
    $result = array();  
    foreach (scandir($dir) as $f) {
      if ($f !== '.' and $f !== '..') {
        if (is_dir("$dir/$f")) {
          $result = array_merge($result, ListIn("$dir/$f", "$prefix$f/"));
        } else {
          $result[] = $prefix.$f;
        }
      }
    }
    return $result;
  }

?>
