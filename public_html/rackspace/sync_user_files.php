<?php

require __DIR__ . '/../vendor/autoload.php';
use OpenCloud\Rackspace;

// 1. Instantiate a Rackspace client.
$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
    'username' => 'lmguae',
    'apiKey'   => '1bc63b29f89abd24b1e4350b213c880b'));

// 2. Obtain an Object Store service object from the client.
$region = 'ORD';
$objectStoreService = $client->objectStoreService(null, $region);

// 3. Get container.
$container = $objectStoreService->getContainer('intranet_user_files');

// 4.uploading the intranet user files directory to cloud
$container->uploadDirectory('/var/www/html/v2/public_html/files_downloads');

?>
