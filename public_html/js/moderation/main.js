$(document).ready(function() {
  
  
	$('.showcomment').hover(function(){
		$('.hovercomment').hide();
		$('.showcomment').css('background-position','0px');
		var id = $(this).attr('id');
		$('#comment-'+id).show();
		$(this).css('background-position','-18px');
	});
	
	$('.hovercomment').hover(function(){
		$(this).show();
	});
	
	$('.hovercomment').mouseleave(function(){
		$(this).hide();
		$('.showcomment').css('background-position','0px');
	});
	$('#submit_announcement').click(function(){
	  var txt_body = $.trim($(".nicEdit-main").html());//General
	  var txt_date = $('#txt_date').val();
	  if(txt_body.length < 5){
	    $('#error-body').html('Announcement text is required');
	    $('#txt_body').focus();
	    return false;
	  }else{
	    $('#error-body').html('');
	  }
	  if(txt_date == 'Pick a date'){
	    $('#error_date').html('Expiry date is required');
	    $('#txt_date').focus();
	    return false;
	  }else{
	    $('#error_date').html('');
	  }
	  return true;
	})
	$(".delete-status-update").click(function(){
			
		var e;
		var del_id = this.id;
		e = window.confirm("This will delete the status update!")
		if(e){
			$.ajax({
				url: "../delete_status_update",
				data: {"del_id":del_id},
				async: false,
				dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						$("#"+del_id).parents("tr").remove();
					}else{
						alert("Error Removing, Please try again");
					}
				}
			});
		}
		window.location.reload();	
		return false;
	})
	$(".disble-announcement-bar").click(function(){
			
		var e;
		var del_id = this.id;
		e = window.confirm("This will disable the Announcement!")
		if(e){
			$.ajax({
				url: "../moderate/disble_announcement_bar",
				data: {"del_id":del_id},
				async: false,
				dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						//$("#"+del_id).parents("tr").remove();
						$("#status_"+del_id).html('<div class="post_status disabled">Disabled</div>');
						$("#status_action_"+del_id).html('');
					}else{
						alert("Error Removing, Please try again");
					}
				}
			});
		}
		return false;
	})
	$(".restore-status-update").click(function(){
			
		var e;
		var res_id = this.id;
		e = window.confirm("This will restore the status update!")
		if(e){
			$.ajax({
				url: "../restore_status_update",
				data: {"res_id":res_id},
				async: false,
				dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						$("#"+res_id).parents("tr").remove();
					}else{
						alert("Error Restoring, Please try again");
					}
				}
			});
		}
		window.location.reload();	
		return false;
	})
	
	$(".delete-market").click(function(){
			
		var e;
		var del_id = this.id;
		e = window.confirm("This will delete market item!")
		if(e){
			$.ajax({
				url: "../delete_market",
				data: {"del_id":del_id},
				async: false,
				dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						$("#"+del_id).parents("tr").remove();
					}else{
						alert("Error Removing, Please try again");
					}
				}
			});
		}
		window.location.reload();	
		return false;
	})
	$(".restore-market").click(function(){
      
    var e;
    var res_id = this.id;
    e = window.confirm("This will restore the market item!")
    if(e){
      $.ajax({
        url: "../restore_market",
        data: {"res_id":res_id},
        async: false,
        dataType: "json",
        type: "POST", 
        success: function(msg){
          if(msg.status){
            $("#"+res_id).parents("tr").remove();
          }else{
            alert("Error Restoring, Please try again");
          }
        }
      });
    }
    window.location.reload(); 
    return false;
  })
	$(".delete-file").click(function(){
			
		var e;
		var del_id = this.id;
		e = window.confirm("This will delete file!")
		if(e){
			$.ajax({
				url: "../delete_file",
				data: {"del_id":del_id},
				async: false,
				dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						$("#"+del_id).parents("tr").remove();
					}else{
						alert("Error Removing, Please try again");
					}
				}
			});
		}
		window.location.reload();	
		return false;
	})
	
	$(".restore-file").click(function(){
			
		var e;
		var res_id = this.id;
		e = window.confirm("This will restore the file!")
		if(e){
			$.ajax({
				url: "../restore_file",
				data: {"res_id":res_id},
				async: false,
				dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						$("#"+res_id).parents("tr").remove();
					}else{
						alert("Error Restoring, Please try again");
					}
				}
			});
		}
		window.location.reload();	
		return false;
	})
	
	
//mt category file on category
	fn_get_mt_category_title()

//Searchbox filter	
	search_box_filter();
	
//Send Notifications
  fn_send_notifications();	
});

//mt category file on category
function fn_get_mt_category_title(){
  $("#cmb_category").change(function(){
    var mt_category = $(this).val();
    var base_url = $("#base_url").val();
    $.ajax({
      url:"get_mt_category_title",
      data: {"mt_cat":mt_category},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        $("#cmb_category_title").html(msg.options);
      }
    });
  })
}

//Send notification submit
function fn_send_notifications(){
  $("#send_notifications").click(function(){
    var mt_cat = $("#cmb_category").val();
    var cat_title = $("#cmb_category_title").val();
    
    if(!mt_cat){
      $("#error_cmb_category").html("Notification for is required");
      $("#cmb_category").focus()
      return false;
    }else{
      $("#error_cmb_category").html("");
    }
    if(!cat_title){
      $("#error_cmb_category_title").html("Content item is required");
      $("#cmb_category_title").focus()
      return false;
    }else{
      $("#error_cmb_category_title").html("");
    }
    return true;
  })
}


function search_box_filter(){
//Notification Check All the Concepts 
  $('.concept_check_all').click(function(){
    $('.checkbox_concepts').attr("checked","checked");
    return false;
  })
//Uncheck All the Concepts  
  $('.concept_check_none').click(function(){
    $('.checkbox_concepts').removeAttr("checked");
    return false;
  })
//Check All the Locations 
  $('.location_check_all').click(function(){
    $('.checkbox_locations').attr("checked","checked");
    return false;
  })
//Uncheck All the Locations 
  $('.location_check_none').click(function(){
    $('.checkbox_locations').removeAttr("checked");
    return false;
  })

//Notification Check All the Bands 
  $('.band_check_all').click(function(){
    $('.checkbox_bands').attr("checked","checked");
    return false;
  })
//Uncheck All the Bands
  $('.band_check_none').click(function(){
    $('.checkbox_bands').removeAttr("checked");
    return false;
  })
}
