function table2csv(oTable, exportmode, tableElm) {
        var csv = '';
        var headers = [];
        var rows = [];
 
        // Get header names
        $(tableElm+' thead').find('th').each(function() {
            var $th = $(this);
            var text = $th.text();
            var head_class = $th.attr('class');
            var class_array = head_class.split(' ');
            var header = '"' + text + '"';
            if(jQuery.inArray('tmhead',class_array) < 0) {
             if(text != "") headers.push(header); // actually datatables seems to copy my original headers so there ist an amount of TH cells which are empty
            }else{
             if(text != "")  csv += text + "\n";
            }
             headers.push(header); // original code
        });
        csv += headers.join(',') + "\n";
 
        // get table data
        if (exportmode == "full") { // total data
            var total = oTable.fnSettings().fnRecordsTotal()
            for(i = 0; i < total; i++) {
                var row = oTable.fnGetData(i);
                row = strip_tags(row);
                rows.push(row);
            }
        } else { // visible rows only
            $(tableElm+' tbody tr:visible').each(function(index) {
                var row = oTable.fnGetData(this);
                row = strip_tags(row);
                rows.push(row);
            })
        }
        csv += rows.join("\n");
        csv +="\n\n";
        return csv;
 
}

function strip_tags(html) {
    var tmp = document.createElement("div");
    tmp.innerHTML = html;
    return tmp.textContent||tmp.innerText;
}