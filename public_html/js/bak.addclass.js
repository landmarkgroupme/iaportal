function initScript() {
	//Used for general search fields
	  addClass({
		tagName:'div',
		tagClass:'searchcategory',
		classAdd:'open'
	})
	addClass({
		tagName:'a',
		tagClass:'search-options',
		classAdd:'open-options',
		addToParent:true
	})

	$('.searchcategory li a').click(function(){ 
		var item = $(this).html();
		$('.searchcategory strong').html(item);
		base_url = $(this).attr("href");
	});
}

function addClass (_options) {
	var _tagName = _options.tagName;
	var _tagClass = _options.tagClass;
	var _classAdd = _options.classAdd;
	var _addToParent = false || _options.addToParent;
	var _el = document.getElementsByTagName(_tagName);
	if (_el) {
		for (var i=0; i < _el.length; i++) {
			if (_el[i].className.indexOf(_tagClass) != -1) {
				_el[i].onclick = function() {
					if (_addToParent) {
						if (this.parentNode.parentNode.className.indexOf(_classAdd) == -1) {
						  var base_url = $("#base_url").val();
						  $(this).css("background","url("+base_url+"images/bullet4.gif) no-repeat 13px 12px");
							this.parentNode.parentNode.className += ' '+_classAdd;
						} else {
						  var base_url = $("#base_url").val();
						  $(this).css("background","url("+base_url+"images/bullet13.gif) no-repeat 16px 10px");
							this.parentNode.parentNode.className = this.parentNode.parentNode.className.replace(_classAdd,'');
						}
					} else {
						if (this.className.indexOf(_classAdd) == -1) {
							this.className += ' '+_classAdd;
						} else {
							this.className = this.className.replace(_classAdd,'');
						}
					}
					return false;
				}
			}
		}
	}
}
if (window.addEventListener)
	window.addEventListener("load", initScript, false);
else if (window.attachEvent)
	window.attachEvent("onload", initScript);

function searchPage(){
	//var service_url = $(this).attr("href");
	var service_url = $("#base_url").val();
	var page = $('.searchcategory strong').html();
	page = page.toLowerCase();
	if(page == 'items')
	{
		document.searchform.action = service_url+"index.php/market_place/search/"
	}
	else
	{
		document.searchform.action = service_url+"index.php/"+page+"/search/";
	}
	document.searchform.submit();
}	