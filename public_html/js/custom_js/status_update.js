/* Status Update(su) javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* Status Updates */
initialise_status_update();

/* Add/remove Contact (newcommer)*/
	add_remove_contacts()
		
/* Share Button Click */
	$("#btn-ar-share").click(function(){
		var txtarShare = $.trim($("#txtarShare").val());
		var activeTab = ($(".active").attr("id"))
		if((txtarShare == "What are you working on ?") || (txtarShare.length < 2)){
			$("#txtarShare").focus();
			return false;
		}
		
		var tab = $(".filters ul .active a").text();
		var service_url = $("#base_url").val()+"index.php/status_update/ajax_submit_status_message";
		
     $.ajax({
      url: service_url,
      data: {"txtarShare":txtarShare},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        var update = msg.update *1;
        if(update){
          //Upadate if Tabs are All or Status update 
          if(tab != "My Updates" && tab != "All People"){
            $('#status-msg-status').html("New status message posted");
            $('#status-msg-status').show();
          }else{
            $("#show-list li").removeClass("highlight_status_message")
            $("#show-list").prepend(msg.data);
          }
          $("#txtarShare").val('What are you working on ?');
          $("#txtarShare").animate({ height: "2em" }, 200,function(){
            $('#btn-ar-share').hide();
              $('#status-msg-count').hide();
          });
        }
      }
    });
    return false;
	})
//Reply Message
	$(".msg-reply").live("click",function(){
		var user = this.id;
		$("#txtarShare").val("@"+user+":");
		$("#txtarShare").focus();
		return false;
	})
//Unfollow User	
	$(".msg-unfollow").live("click",function(){
		var user = $(this).attr("rel");
		var active_tab = $(".active").attr("id");
		$.ajax({
			url: "status_update/unfollow_user",
			data: {"unfollow_user":user},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$("[rel="+user+"]").text("Follow");
					$("[rel="+user+"]").removeClass("msg-unfollow");
					$("[rel="+user+"]").addClass("msg-follow");
					if(active_tab == "su_show_following_people"){
						setTimeout(function(){ window.location = "following_people";}, 2000 );
					}
				}
		  }
		});
		return false;
	})
//Follow User
	$(".msg-follow").live("click",function(){
		var user = $(this).attr("rel");
		var active_tab = $(".active").attr("id");
		$.ajax({
			url: "status_update/follow_user",
			data: {"follow_user":user},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$("[rel="+user+"]").text("Unfollow");
					$("[rel="+user+"]").addClass("msg-unfollow");
					$("[rel="+user+"]").removeClass("msg-follow");
				}
		  }
		})
		return false;
	})
//Delete Message
	$(".msg-delete").live("click",function(){
		var message_id = this.id;
		var active_tab = $(".active").attr("id");
		$.ajax({
			url: "status_update/delete_status_message",
			data: {"message_id":message_id},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					pagesize =0;
					loadFiltersContents(active_tab,"new");
				}
		  }
		})
		return false;
	})	
//Load More ACtion
	$(".more-stories").click(function(){
	  var img_path = $("#base_url").val()+"images/indicator.gif";
	  $(this).html("<img src='"+img_path+"' alt='loader' />");
		var active_tab = $(".active").attr("id");
		
		pagesize = pagesize + (this.id*1);
		loadFiltersContents(active_tab,"append")
		$('a[rel*=pop_win]').facebox(); //Facebox Loader
		fn_report_staus_update();
		return false;
	})
})

//Filters Load
function loadFiltersContents(filter,load_type){
	$(".filters li").removeClass("active");
	$("#"+filter).addClass("active");
	var callurl = filter;
	$.ajax({
		url: "pagination",
		data: {"pagesize":pagesize,"page":callurl},
		async: false,
    dataType: "json",
		type: "POST", 
		success: function(msg){
			if(msg.data){
				if(load_type == "new"){
					$("#show-list").html(msg.data);
				}else if(load_type == "append"){
					$("#show-list").append(msg.data);
				}
				
				if(msg.load_more){
          $(".more-stories").text("View More");				  
				}else{
				  $(".more-stories").hide();  
				}
				
				if(msg.load_page == "su_show_my_replies"){
				  $("#su_show_my_replies a span em").html(msg.my_replies_cnt)
				}
				
				
			}else{
				$(".more-stories").hide();
			}
	  }
	});
}
//Add or remove contact function
function add_remove_contacts(){
	//Remove from Contact list
	$(".remove-contact").live("click",function(){
		var e = this;
		var service_url = $(this).attr("href")+"remove_contact";
		var remove_contact = this.id;
		$.ajax({
			url: service_url,
			data: {"remove_contact":remove_contact},
			async: true,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$(e).removeClass("remove-contact");					
					$(e).addClass("add-contact");
					$(e).text("Add to my contacts");
				};
		  }
		});
		return false;
	})
	//Add to Contact list
	$(".add-contact").live("click",function(){
		var e = this;
		var service_url = $(this).attr("href")+"add_contact";
		var add_contact = this.id;
		$.ajax({
			url: service_url,
			data: {"add_contact":add_contact},
			async: true,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$(e).removeClass("add-contact");					
					$(e).addClass("remove-contact");
					$(e).text("Remove from my contacts");
				};
		  }
		});
		return false;
	})
}
function initialise_status_update(){
  //Focus on textarea changes backgroud
  $("#txtarShare").focus(function(){
    var base_url = $("#base_url").val()
    var text = $("#txtarShare").val();
    if(text == "What are you working on ?"){
      $("#txtarShare").val("");
    }
    $(this).animate({ height: "5.5em" }, 200,function(){
      $('#btn-ar-share').show();
      $('#status-msg-count').show();
    });
   // $(this).css("background","url('"+base_url+"images/bg-share-form-textarea-focus.jpg') no-repeat scroll 0 0 transparent");
  })

  $("#txtarShare").blur(function(){
    var base_url = $("#base_url").val()
    var text = $.trim($("#txtarShare").val());
    if(text == ""){
      $("#txtarShare").val("What are you working on ?");
      $(this).animate({ height: "2em" }, 200,function(){
            $('#btn-ar-share').hide();
        $('#status-msg-count').hide();
      });
    }
    //$(this).css("background","url('"+base_url+"images/bg-share-form-textarea.jpg') no-repeat scroll 0 0 transparent");
  })

  limitChars("txtarShare",160,"status-msg-count");// Checking when page is reloaded
  $("#txtarShare").keyup(function(){
    limitChars("txtarShare",160,"status-msg-count");
  })
}
function limitChars(textid, limit, infodiv) {
  var text = $('#'+textid).val();
  text = (text == "What are you working on ?")? "":text;
  var textlength = text.length;
  if(textlength > (limit-1)){
    $('#' + infodiv).html("0");
    $('#'+textid).val(text.substr(0,limit));
    return false;
  }else{
    $('#' + infodiv).html((limit - textlength));
    return true;
  }
}