var base_url = "";
var user_menu_clicked = 0; //When user menu clicked the global value 
var clicked_link
$(document).ready(function(){
  /* General Files */
  //loading base Url of the site
    base_url = siteurl;
  
  
  //General overlay show
  $('.forgot-sso-pass').click(function(){

    $('.sso-dialog').hide();
    $('#sso-pass-forgot-pass-confirm').show();
    return false;
  })
   //Focus on User name
  $("#sso-pass-in").focus(function(){
    var text = $("#sso-pass-in").val();
    if(text == "pass here"){
      $("#sso-pass-in").val("");
    }
  })
  $("#sso-pass-in").blur(function(){
    var text = $.trim($("#sso-pass-in").val());
    if(text == ""){
      $("#sso-pass-in").val("pass here");
    }
  })
  
  
  
  $('#confirm-fgt-submit-bt').click(function(){
    //alert('Confirm forget pass code here')
	var pass_fgt = "true";
    $.ajax({
      url: base_url+"newhome/sso/process",
      data: {
        "forgot_pass":pass_fgt
      },
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'Success'){
          window.location = msg.url

			// Here is the error handling part
			}else{

              $(".sso-dialog").hide();
			        //alert('wrong pass')
			      	$("#sso-pass-forgot-pass").show();

			}
			// End of error handling part

      }
    });
    return false;
  })
  $('#sso-login-submit-bt').click(function(){
   // console.log('here');
//    alert('SSO login code here code here')
                $(".sso-dialog").hide();
                $("#sso-loader").show();
                $("#sso-wrong").hide();
                //alert(clicked_link)
                var txt_pass = $.trim($("#sso-pass-in").val());
    $.ajax({
      url: base_url+"newhome/sso/process",
      data: {
        "txtpassword":txt_pass,
        "clicked_link":clicked_link
      },
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'Success'){
          window.location = msg.url

			// Here is the error handling part
			}else{

              $(".sso-dialog").hide();
			        //alert('wrong pass')
			      	$("#sso-pass").show();
              $('#sso-pass').find('p').find('em').fadeOut();
              $('#sso-pass').find('p').append('<em style="color:red;"><br/><br/>Error: Incorrect password. Please try again.</em>');
			      	//$("#sso-wrong").show();
			      	//$("#sso-wrong").html('Oops');

			}
			// End of error handling part

      }
    });
    return false;
  })
  
//Webmail password submit
$('#sso-webmail-submit-bt').click(function(){
    console.log('here webmail sumit');
//    alert('SSO login code here code here')
                $(".sso-dialog").hide();
                $("#sso-loader").show();
                $("#sso-wrong").hide();
                //alert(clicked_link)
                var txt_pass = $.trim($("#sso-pass-in-webmail").val());
    $.ajax({
      url: base_url+"newhome/sso/process",
      data: {
        "txtpassword":txt_pass,
        "clicked_link":clicked_link
      },
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
		  //console.log(msg.status);
        if(msg.status == 'Success'){
          window.location = msg.url
			//console.log(msg.url);
			// Here is the error handling part
			}else{
			//console.log('error');
              $(".sso-dialog").hide();
			        //alert('wrong pass')
			      	$("#sso-webmail").show();
          $('#sso-webmail').find('p').find('em').fadeOut();
          $('#sso-webmail').find('p').append('<em style="color:red;"><br/><br/>Error: Incorrect password. Please try again.</em>');
			      	//$("#sso-wrong").show();
			      	//$("#sso-wrong").html('Oops');

			}
			// End of error handling part

      }
    });
    return false;
})

  $('.show_overlay').click(function(){
   	clicked_link = $(this).attr("class");
    clicked_link = clicked_link.split(' ');
    clicked_link = clicked_link[0];
    $('.overlay').show();

  	if (clicked_link == 'webmail') {
    //console.log('true');
    $('#sso-webmail').find('p').find('em').hide();
			//Change Title and loading text to Webmail
			$('.head-text').html('<a href="#" class="close-overlay"><img src="http://intranet.landmarkgroup.com/images/sso_close.png" /></a><img src="http://intranet.landmarkgroup.com/images/sso_login.png" /> Login to Webmail');
			$('#sso-loader').find('p').text('Please wait while we connect you to Webmail:');
    $('#show-sso-pass-box').show();
            $('#sso-webmail').show();
            $(".sso-dialog").hide();
            $("#sso-loader").show();

			$.ajax({
			  url: base_url+"newhome/sso/process",
			  data: {
				"clicked_link":clicked_link,
				"checklogin":"true"
			  },
			  async: false,
			  dataType: "json",
			  type: "POST",
			  success: function(msg){
				if(msg.status == 'Success'){
				  window.location = msg.url

				// Here is the error handling part
				}else if(msg.status == 'Error'){

				  $(".sso-dialog").hide();
				  $("#sso-webmail").show();

				  //$("#sso-wrong").show();
				  //$("#sso-wrong").html('The password that the user has stored in the DB is not correct.');

				}else{
				  $("#sso-wrong").hide();
				  $(".sso-dialog").hide();
				  $("#sso-webmail").show();
				}
				// End of error handling part
      }
    });


  } else {
    $('#sso-pass').find('p').find('em').hide();
	  //Change Title and loading text to HR Konnekt
	  	$('.head-text').html('<a href="#" class="close-overlay"><img src="http://intranet.landmarkgroup.com/images/sso_close.png" /></a><img src="http://intranet.landmarkgroup.com/images/sso_login.png" /> Login to HR Konnekt');
	  	$('#sso-loader').find('p').text('Please wait while we connect you to HR Konnekt:');

		$('#show-sso-pass-box').show();
		//$('#sso-pass').show();
		$(".sso-dialog").hide();
		$("#sso-loader").show();
		$.ajax({
		  url: base_url+"newhome/sso/process",
		  data: {
			"clicked_link":clicked_link,
			"checklogin":"true"
		  },
		  async: false,
		  dataType: "json",
		  type: "POST",
		  success: function(msg){
			if(msg.status == 'Success'){
			  window.location = msg.url

			// Here is the error handling part
			}else if(msg.status == 'Error'){

			  $(".sso-dialog").hide();
			  $("#sso-pass").show();
        $('#sso-pass').find('p').find('em').fadeOut();
        $('#sso-pass').find('p').append('<em style="color:red;"><br/><br/><br/>Error: Incorrect password. Please try again.</em>');
			  //$("#sso-wrong").show();
			  //$("#sso-wrong").html('The password that the user has stored in the DB is not correct.');

			}else{
			  $("#sso-wrong").hide();
			  $(".sso-dialog").hide();
			  $("#sso-pass").show();
			}
			// End of error handling part

		  }
		});
  }

    return false;
  })

 //Hide Incorrct Passord on close
  $('.close-overlay').live('click',function(){
   $("#sso-wrong").hide();
    hide_overlay();
    return false;
  })
   

 $('.forgot-sso-pass').click(function() {
  $('#sso-wrong').hide();
});
});

function hide_overlay(){
  $('.overlay').hide();
  $('#show-sso-pass-box').hide();
  $('.sso-dialog').hide(); 
}