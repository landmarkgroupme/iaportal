/* Events javascript File */
$(document).ready(function(){
/* Events */
//Advance search Check All the sidebar Concepts	
	$('#check-all').click(function(){
		$('.side-concept').attr("checked","checked");
		$('.cal-events').show();
		return false;
	})
//Uncheck All the sidebar Concepts	
	$('#check-none').click(function(){
		$('.side-concept').removeAttr("checked");
		$('.cal-events').hide();
		return false;
	})
	
	//Filter Calendar
	$(".side-concept").click(function(){	
		var id = this.id;
		if($(this).is(':checked')){
			$('.'+id).show();
    }else{
			$('.'+id).hide();
    }
	})
	
	//Event Reminder On
	$(".event_reminder_on").live("click",function(){
		var event_id = this.id;
		var service_url = $(this).attr("href");
		var event_time = $(this).attr("rel"); 
		var e = this;
		$.ajax({
			url: service_url,
			data: {"event_id":event_id,"event_time":event_time},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.reminder_set >= 1){
					//Icon Replacement
					var reminder_ico = $(".event_reminder_ico").attr("src"); 
					var new_ico = reminder_ico.replace("ico-46.png", "ico-45.png");
					$(".event_reminder_ico").attr("src",new_ico);
					
					//Status Msg
					$("#event_reminder_status").text("on");
					
					//Status msg Class
					$('#reminder-status-msg').removeClass('reminder-status-text-off');
					$('#reminder-status-msg').addClass('reminder-status-text');
					
					//replacing link
					var prev_href = $(e).attr("href"); 
					var new_href = prev_href.replace("event_reminder_turnon", "event_reminder_turnoff");
					$(e).attr("href",new_href);
					$(e).text("Turn off");
					$(e).removeClass("event_reminder_on");
					$(e).addClass("event_reminder_off");

				}
		  }
		});
		return false;
	})

	//Event Reminder Off
	$(".event_reminder_off").live("click",function(){
		var event_id = this.id;
		var service_url = $(this).attr("href");
		var e = this;
		$.ajax({
			url: service_url,
			data: {"event_id":event_id},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.reminder_set >= 1){
					//Icon Replacement
					var reminder_ico = $(".event_reminder_ico").attr("src"); 
					var new_ico = reminder_ico.replace("ico-45.png", "ico-46.png");
					$(".event_reminder_ico").attr("src",new_ico);
					
					//Status Msg
					$("#event_reminder_status").text("off");
					
					//Status msg Class
					$('#reminder-status-msg').removeClass('reminder-status-text');
					$('#reminder-status-msg').addClass('reminder-status-text-off');
					
					//replacing link
					var prev_href = $(e).attr("href"); 
					var new_href = prev_href.replace("event_reminder_turnoff", "event_reminder_turnon");
					$(e).attr("href",new_href);
					$(e).text("Turn on");
					$(e).removeClass("event_reminder_off");
					$(e).addClass("event_reminder_on");

				}
		  }
		});
		return false;
	})
	
})
