var base_url = "";
$(document).ready(function(){
/* General Files */
//loading base Url of the site
  base_url = $("#base_url").val()+"index.php/";
   
  $('#list-table tr').hover(
    function(){
      $(".report-list").hide();
      var id = this.id;
      $("#rep_"+id).show();
    }
    
  );
   
   
   
   
  //Profile Progress Bar Status
  fn_profile_progress_bar();  
  
  //Load Facebox
  $('a[rel*=pop_win]').facebox(); //Facebox Loader
  //Close Facebox
  $('.pop-win-close').live("click",function(){
    $(document).trigger('close.facebox');
  	return false;
  })
  //General Function
  general_search_form();
  //Change Password pop window
	fn_change_password();
	//Change Profile Pic pop window
	fn_change_profile_pic();
	//Add Fav. Link pop window
	fn_add_fav_link();
	//fav Link remove option toggle
	fu_toggle_fav_remove_link();
	//Report Files
	fn_report_files();
	//Report Status Updates
	fn_report_staus_update();
	//Admin Menu's
	fn_toggle_admin_menu();
	
	//Widget box open close
	fn_toogle_widgets();
})

/* Custom function */

//Change Password
function fn_change_password(){
	$('#fn-change-pass').live("click",function(){
		var txt_pass = $.trim($("#txtpassword").val());
		var txt_cnf_pass = $.trim($("#txtcnfpassword").val());
		if(!txt_pass){
			$(".error-msg").html("New password is required.");
			return false;
		}else if(txt_pass != txt_cnf_pass){
			$(".error-msg").html("Passwords do not match.");
			return false;
		}else{
			$(".error-msg").html("");
		}
		$.ajax({
			url: base_url+"profile/submit_change_password",
			data: {"txtpassword":txt_pass,'txtcnfpassword':txt_cnf_pass},
			async: false,
			dataType: "json",
			type: "POST", 
			success: function(msg){
				$(".error-msg").html(msg.status);
		  }
		});
		return false;
	})
}
//Change profile Pic function
function fn_change_profile_pic(){
  $('#btn_change_pic').live("click",function(){
    var profile_pic = $.trim($("#file_new_pic").val());
    var ext = profile_pic.split('.').pop().toLowerCase();
    var allow = new Array('gif','png','jpg','jpeg');
    if(!profile_pic){
      $(".error-msg").html("Profile Photo is required");
      return false;
    }else if($.inArray(ext, allow) == -1) {
      $(".error-msg").html("Invalid File Format");
      return false;  
    }else{
      $(".error-msg").html("");
    }
    var options = {
         success: profile_pic_change,
         url: base_url+'profile/submit_change_profile_pic',
         dataType: 'json',
         type: 'post'
      };
    $('#frm_change_pic').ajaxForm(options);
    return true;
  })
}
// Called if the profile pic is uploaded withour any errors
function profile_pic_change(msg){
  //$('#frm_change_pic').html(msg);
  //Clear the pic cache
  var timestamp = new Date().getTime();
  $(".profile_pic").attr("src",msg.pic+'?'+timestamp);
  $(".error-msg").html(msg.status);
  
  
}
//General Search Field
function general_search_form(){
  //Focus on textarea changes backgroud 
  $("#txt_general_search").focus(function(){
    var text = $("#txt_general_search").val();
    if(text == "Search by name, job role or concept.."){
      $("#txt_general_search").val("");
    }
  })
  $("#txt_general_search").blur(function(){
    var text = $.trim($("#txt_general_search").val());
    if(text == ""){
      $("#txt_general_search").val("Search by name, job role or concept..");
    }
  })
}
//Add fav. link
function fn_add_fav_link(){
  $("#fn-fav-link").live("click",function(){
    var txt_url = $.trim($("#txturl").val());
    var txt_url_title = $.trim($("#txturltitle").val());
    
    if(!txt_url){
      $(".error-msg").html("URL is required");
      $("#txturl").focus();
      return false;
    }else if(!url_validation(txt_url)){
      $(".error-msg").html('Invalid Url');
      $("#txturl").focus();
      return false;
    }else if(!txt_url_title){
      $(".error-msg").html("URL Title is required");
      $("#txturltitle").focus();
      return false;
    }else{
      $(".error-msg").html("");
    }
    
    $.ajax({
      url: base_url+"profile/submit_add_fav_link",
      data: {"txturl":txt_url,'txturltitle':txt_url_title},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        $(".error-msg").html(msg.status);
        $(".favourites").prepend(msg.link);
      }
    });
    return false;
  })
}

//Toggle Fav remove link
function fu_toggle_fav_remove_link(){
  //Delete Show
  $(".favourites li").hover(function(){
    $(this).css("background","#f5f5f5");
    $(".remove-link",this).show();
   },
   function(){
    $(this).css("background","none");
    $(".remove-link",this).hide();
  })
  
  $(".remove-link").live("click",function(){
    var link_id = this.id;
    var e = this;
    $.ajax({
      url: base_url+"profile/fav_remove_link",
      data: {"link_id":link_id},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        if(msg.status == 1){
          $(e).closest("li").remove();
        }else{
          alert("Please try again");
        }
      }
    });
    return false;
  })
}
//Report Files
function fn_report_files(){
  $("#fn-report-files").live("click",function(){
    var file_id = $("#txtfileid").val();
    var comments = $.trim($("#txtarcomments").val());
    var e = this;
    if(!comments){
      $(".error-msg").html("Comments required");
      $("#txtarcomments").focus();
      return false;
    }
    $.ajax({
      url: base_url+"profile/submit_report_files",
      data: {"file_id":file_id,"comments":comments},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
          $(".error-msg").html(msg.status);
          $("#fn-report-files").attr("disabled","disabled")
      }
    });
	  //$(document).trigger('close.facebox');
    return false;
  })
}
//Report  status update
function fn_report_staus_update(){
  $("#fn-report-status_update").live("click",function(){
    var message_id = $("#txtmessageid").val();
    var comments = $.trim($("#txtarcomments").val());
    var e = this;
    if(!comments){
      $(".error-msg").html("Comments required");
      $("#txtarcomments").focus();
      return false;
    }
    $.ajax({
      url: base_url+"profile/submit_report_status_update",
      data: {"message_id":message_id,"comments":comments},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
          $(".error-msg").html(msg.status);
      }
    });
	$(document).trigger('close.facebox');
    return false;
  })
}
//Profile Progress Bar Status
  function fn_profile_progress_bar(){
    $.ajax({
      url: base_url+"profile/profile_progress_bar",
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        var pg_bar_len = (msg.progress)*1;
        $(".progress span").css("width",pg_bar_len+"%");
        $(".info strong").text(pg_bar_len+"%");
      }
    });
  }

//Admin Menu
function fn_toggle_admin_menu(){
  $('.toogle-admin-menu').toggle(function() {
    $(this).addClass("admin-link-hide");
    $("#admin-nav").show();
  }, function() {
    $(this).removeClass("admin-link-hide");
    $("#admin-nav").hide();
  });
  return false;
}
//Genaral Url validation function
function url_validation(url) {
  var v = new RegExp();
  v.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");
  if (!v.test(url)) {
    return false;
  }else{
    return true;
  }
} 

//Toogle widget box
function fn_toogle_widgets(){
	$('.open-close').toggle(function() {
		$(this).closest('.box').removeClass("open");
		$(this).closest('.box').removeClass("close");
		return false;
	}, function() {
		$(this).closest('.box').removeClass("close");
		$(this).closest('.box').addClass("open");
		return false;
	});
	
	
}