/* Interest Groups(ig) javascript File */

$(document).ready(function(){
/* Interest Groups */
/* Advance Search Load */
	load_advance_search();
	
	//Leave or Join Groups function
	leave_join_groups();
	
	//Follow or Unfollow Updates
	follow_unfollow_updates();
	
	//Add or remove contacts
	add_remove_contacts();
	
})
function load_advance_search(){
//Advance search Check All the Concepts	
	$('.concept_check_all').click(function(){
		$('.checkbox_concepts').attr("checked","checked");
		return false;
	})
//Uncheck All the Concepts	
	$('.concept_check_none').click(function(){
		$('.checkbox_concepts').removeAttr("checked");
		return false;
	})
//Check All the Locations	
	$('.location_check_all').click(function(){
		$('.checkbox_locations').attr("checked","checked");
		return false;
	})
//Uncheck All the Locations	
	$('.location_check_none').click(function(){
		$('.checkbox_locations').removeAttr("checked");
		return false;
	})
}

//Leave or Join Groups function
function leave_join_groups(){
	//Leave Group
	$(".interest-group-leave").live("click",function(){
		var e = this;
		var ig_id = this.id;
		var service_url = $(this).attr("href")+"leave_group";
		$.ajax({
			url: service_url,
			data: {"ig_id":ig_id},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.status == 1){
					$("#"+e.id).removeClass("interest-group-leave");					
					$("#"+e.id).addClass("interest-group-join");
					$("#"+e.id).text("Join")					
				};
		  }
		});
		return false;
	})
	//Join Group
	$(".interest-group-join").live("click",function(){
		var e = this;
		var ig_id = this.id;
		var service_url = $(this).attr("href")+"join_group";
		$.ajax({
			url: service_url,
			data: {"ig_id":ig_id},
			async: false,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.status == 1){
					$("#"+e.id).removeClass("interest-group-join");					
					$("#"+e.id).addClass("interest-group-leave");
					$("#"+e.id).text("Leave");
				};
		  }
		});
		return false;
	})
}

//Follow or unfollow Updates
function follow_unfollow_updates(){
	//follow User
	$(".follow").live("click",function(){
		var e = this;
		var service_url = $(this).attr("href")+"follow_user";
		var follow_user = this.id;
		$.ajax({
			url: service_url,
			data: {"follow_user":follow_user},
			async: true,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$(e).removeClass("follow");					
					$(e).addClass("unfollow");
					$(e).text("Unfollow Updates");
				};
		  }
		});
		return false;
	})
	//Unfollow User
	$(".unfollow").live("click",function(){
		var e = this;
		var service_url = $(this).attr("href")+"unfollow_user";
		var unfollow_user = this.id;
		$.ajax({
			url: service_url,
			data: {"unfollow_user":unfollow_user},
			async: true,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$(e).removeClass("unfollow");					
					$(e).addClass("follow");
					$(e).text("Follow Updates");
				};
		  }
		});
		return false;
	})
}

//Add or remove contact function
function add_remove_contacts(){
	//Remove from Contact list
	$(".remove-contact").live("click",function(){
		var e = this;
		var service_url = $(this).attr("href")+"remove_contact";
		var remove_contact = this.id;
		$.ajax({
			url: service_url,
			data: {"remove_contact":remove_contact},
			async: true,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$(e).removeClass("remove-contact");					
					$(e).addClass("add-contact");
					$(e).text("Add to my contacts");
				};
		  }
		});
		return false;
	})
	//Add to Contact list
	$(".add-contact").live("click",function(){
		var e = this;
		var service_url = $(this).attr("href")+"add_contact";
		var add_contact = this.id;
		$.ajax({
			url: service_url,
			data: {"add_contact":add_contact},
			async: true,
      dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$(e).removeClass("add-contact");					
					$(e).addClass("remove-contact");
					$(e).text("Remove from my contacts");
				};
		  }
		});
		return false;
	})
}
