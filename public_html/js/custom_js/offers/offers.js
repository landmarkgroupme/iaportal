/* Offers javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* Offers*/
	search_box_filter()
 

	
//Load More ACtion
	$(".more-stories").click(function(){
	  var img_path = $("#base_url").val()+"images/indicator.gif";
    $(this).html("<img src='"+img_path+"' alt='loader' />");
		pagesize = pagesize + (this.id*1);
		$.ajax({
			url: "offers/pagination",
			data: {"pagesize":pagesize},
			async: false,
	    dataType: "json",
			type: "POST", 
			success: function(msg){
			  //$(".more-stories").text("Load More Offers");
				if(msg.load_more){
          $(".more-stories").text("Load More Offers");         
        }else{
          $(".more-stories").hide();  
        }
        
        if(msg.data){
          $("#show-offers").append(msg.data);
        }else{
          $(".more-stories-search").hide();
        }
	  	}
		});
		return false;
	})
	
	$(".more-stories-search").click(function(){
	  var img_path = $("#base_url").val()+"images/indicator.gif";
    $(this).html("<img src='"+img_path+"' alt='loader' />");
		pagesize = pagesize + (this.id*1);
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"pagesize":pagesize},
			async: false,
	    dataType: "json",
			type: "POST", 
			success: function(msg){
			  //$(".more-stories").text("Load More Offers");
				if(msg.load_more){
          $(".more-stories").text("Load More Offers");         
        }else{
          $(".more-stories").hide();  
        }
        
        if(msg.data){
          $("#show-news").append(msg.data);
        }else{
          $(".more-stories-search").hide();
        }
	  	}
		});
		return false;
	})
})

//Concept Check all
function search_box_filter(){
//Advance search Check All the Concepts 
  $('.concept_check_all').live("click",function(){
    $('.checkbox_concepts').attr("checked","checked");
    return false;
  })
	

//Uncheck All the Concepts  
  $('.concept_check_none').live("click",function(){
    $('.checkbox_concepts').removeAttr("checked");
    return false;
  })
}
