/* Manage javascript File */
//Initial Loader
$(document).ready(function(){
  var base_url = $("#base_url").val();
  check_uncheck_all();
  $('#txt_date').calendricalDate();
  $('#txt_time').calendricalTime();
  $('#txt_to_date').calendricalDate();
  $('#txt_to_time').calendricalTime();
  
  $(".submit_entry").click(function(){
    var txt_title = $.trim($("#txt_title").val()); 
    var txt_desc = $.trim($("#txtar_desc").val());
    var txt_date = $.trim($("#txt_date").val());
    var txt_time = $.trim($("#txt_time").val()); 
    var cmb_location = $.trim($("#cmb_location").val());
    var txt_to_date = $.trim($("#txt_to_date").val());//Events
    var txt_to_time = $.trim($("#txt_to_time").val());//Events
    var btn_value = $.trim($(this).val());//Events

    if(btn_value == 'Reject'){
      $("#btn_save").val(btn_value);
    }

    if(txt_to_date == 'Pick a date'){
      $("#txt_to_date").val('');
    }
    if(txt_to_time == 'Pick a time'){
      $("#txt_to_time").val('');
    }




    if(!txt_title){
      $("#error_txt_title").html("Title is required");
      $("#txt_title").focus();
      return false;
    }else{
      $("#error_txt_title").html("");
    }
    if(!txt_desc){
      $("#error_txtar_desc").html("Description is required");
      $("#txtar_desc").focus();
      return false;
    }else{
      $("#error_txt_desc").html("");
    }
    if(!txt_date){
      $("#error_date_time").html("Date is required");
      $("#txt_date").focus();
      return false;
    }else{
      $("#error_date_time").html("");
    }
    if(!txt_time){
      $("#error_date_time").html("Time is required");
      $("#txt_time").focus();
      return false;
    }else{
      $("#error_date_time").html("");
    }
    if(cmb_location <=0){
      $("#error_cmb_location").html("Location is required");
      $("#cmb_location").focus();
      return false;
    }else{
      $("#error_cmb_location").html("");
    }


    $(this).attr("value","");
    $(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
    $(this).css("border",'0px');
    $(this).css("width","40px");
    //$(".submit_entry").attr("disabled","disabled");

    return true;
  });

});
function check_uncheck_all(){
  $('#concAll').click(
    function(){
      $("input.conc").attr('checked', true);   
    }
    );
  
  $('#concNone').click(
    function(){
      $("input.conc").attr('checked', false);   
    }
    );
  
  $('#gradAll').click(
    function(){
      $("input.grad").attr('checked', true);   
    }
    );
  
  $('#gradNone').click(
    function(){
      $("input.grad").attr('checked', false);   
    }
    );
  
  $('#terrAll').click(
    function(){
      $("input.terr").attr('checked', true);   
    }
    );
  
  $('#terrNone').click(
    function(){
      $("input.terr").attr('checked', false);   
    }
    );
}
