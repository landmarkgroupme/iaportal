/* Manage javascript File */
/* POST CONTENT */
//Initial Loader
var news_create_flag = false;
$(document).ready(function(){
  var base_url = $("#base_url").val();


     $('#submit_concept').click(function(){

    var submit_url = null;
    var sub_button = $(this).val();//General
    var txt_title = $.trim($("#txt_title").val());//General
    var txt_body = $.trim($(".nicEdit-main").html());//General
		$("#txt_body").val(txt_body);
	var txt_status = $.trim($("#txt_status").val());//General
	var txt_open = $.trim($("#txt_open").val());//General
    var btn_text = $.trim($("#submit_group").val()); //general


    /* General validation for all the FORMS */
    if(!txt_title){
      $("#error-title").html("required");
      $("#txt_title").focus()
      return false;
    }else{
      $("#error-title").html("");
	   $('#frm_edit_concept').submit();
    }

/* News SELECTION */


    $(this).attr("value","");
    $(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
    $(this).css("border",'0px');
    $(this).css("width","40px");
    $("#submit_concept").attr("disabled","disabled");

  })

  $('#update_concept').click(function(){

    var submit_url = null;
    var sub_button = $(this).val();//General
    var txt_title = $.trim($("#txt_title").val());//General
	var concept_id = $.trim($("#concept_id").val());//General
    var txt_body = $.trim($(".nicEdit-main").html());//General
		$("#txt_body").val(txt_body);
	var txt_status = $.trim($("#txt_status").val());//General
	var txt_open = $.trim($("#txt_open").val());//General
    var btn_text = $.trim($("#submit_group").val()); //general


    /* General validation for all the FORMS */
    if(!txt_title){
      $("#error-title").html("required");
      $("#txt_title").focus()
      return false;
    }else{
      $("#error-title").html("");
	  $('#frm_edit_concept').submit();
    }

    $(this).attr("value","");
    $(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
    $(this).css("border",'0px');
    $(this).css("width","40px");
    $("#update_concept").attr("disabled","disabled");

  })

});
