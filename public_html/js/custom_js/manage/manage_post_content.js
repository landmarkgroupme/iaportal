/* Manage javascript File */
/* POST CONTENT */
//Initial Loader
var news_create_flag = false;
$(document).ready(function(){
  var base_url = $("#base_url").val();

  $('#txt_title').limit('120','#charsLeft');
  $('#txt_excerpt').limit('350','#charsLeft2');
  $('#txt_date_start').calendricalDate();
  $('#txt_date_end').calendricalDate();
  $('#txt_date').calendricalDate();
  $('#txt_time').calendricalTime();
  $('#txt_to_date').calendricalDate();
  $('#txt_to_time').calendricalTime();
  check_uncheck_all();
  
  $('#show-end-date').live('click',function(){
    $('#datetime-to-box').show();
    $('.event-sep').html('-');
    $(this).html('Hide end date');
    $(this).attr('id', 'hide-end-date');
    return false;
  })

   $('#hide-end-date').live('click',function(){
    $('#datetime-to-box').hide();
    $('.event-sep').html('+');
    $(this).html('Add end date');
    $(this).attr('id', 'show-end-date');
    $('#txt_to_date').val('Pick a date');
    $('#txt_to_time').val('Pick a time');
    return false;
  })
  $('#create-photo-album').click(function(){
    $('#category_photos').attr('checked','checked');
    $('.custom-form-element').hide();
    $('#title-box').show();
    $('#alb-body-box').show();
    $('#album_category-period-box').show();
    $('#add-photo-box').show();
    $('#save_content').show();
    $('.help-info').hide();
    $('#help-text').show();
    $('#photo-help').show();
    $('#selected-content-type').html('Photos/Album');
    $('#submit_content').val('Publish Now');
    news_create_flag = true;
    $('#news_create_flag').val('yes');
    return false;
  })
  $('.submit_entry').click(function(){

    var argv_str = null; //Argument string for posting value to the forms, set at differnet section

    var content_type = $('input[name=entry_category]:checked', '#frm_post').val();
    var notify_concept = $('.conc:checked').map(function(i,n) {
        return $(n).val();
    }).get();
    
    var notify_terr = $('.terr:checked').map(function(i,n) {
        return $(n).val();
    }).get();

    var notify_grad = $('.grad:checked').map(function(i,n) {
        return $(n).val();
    }).get();
    var submit_url = null;
    var sub_button = $(this).val();//General
    var txt_title = $.trim($("#txt_title").val());//General
    var txt_excerpt = $.trim($("#txt_excerpt").val());//General
    var txt_body = $.trim($(".nicEdit-main").html());//General
    var txt_album = $.trim($("#txt_album").val());//News
    var album_category = $.trim($("#album_category_id").val());//Photos
    var alb_txt_body = $.trim($("#alb_txt_body").val());//Photos
    var event_txt_body = $.trim($("#event_txt_body").val());//Events
    var txt_date = $.trim($("#txt_date").val());//Events
    var txt_time = $.trim($("#txt_time").val());//Events
    var txt_to_date = $.trim($("#txt_to_date").val());//Events
    var txt_to_time = $.trim($("#txt_to_time").val());//Events
    var cmb_location = $.trim($("#cmb_location").val()); //Events
    var cmb_concept = $.trim($("#cmb_concept").val()); //Events
    var txt_date_start = $.trim($("#txt_date_start").val()); //Offers
    var txt_date_end = $.trim($("#txt_date_end").val()); //Offers
    var offer_notification = $.trim($("#offer-notification").val()); //Offers
    var btn_text = $.trim($("#submit_content").val()); //general


    if(txt_date == 'Pick a date'){
      txt_date = null;
    }
    if(txt_time == 'Pick a time'){
      txt_time = null;
    }
    if(txt_to_date == 'Pick a date'){
      txt_to_date = null;
    }
    if(txt_to_time == 'Pick a time'){
      txt_to_time = null;
    }

    if(txt_date_start == 'Start date'){
      txt_date_start = null;
    }
    if(txt_date_end == 'End date'){
      txt_date_end = null;
    }
    
    
    //[nicInstance].getContent()
    if(sub_button.trim() == "Save as draft"){
      sub_button = 1;
    }else{
      sub_button = 2;
    }
   
    /* General validation for all the FORMS */
    if(!txt_title){
      $("#error-title").html("required");
	  $("#txt_title").val('');
      $("#txt_title").focus()
      return false;
    }else{
      $("#error-title").html("");
    }
	if(IsScript(txt_title)){
      $("#error-title").html("Please check Input");
      $("#txt_title").focus()
      return false;
    }
	
	if(IsScript(txt_excerpt)){
      $("#error-excerpt").html("Please check Input");
      $("#txt_excerpt").focus();
      return false;
    }
	else{
      $("#error-excerpt").html("");
    }

    if(content_type == 'events'){
      if(event_txt_body.length < 5){
        $("#error-event-body").html("required");
        $("#event_txt_body").focus();
        return false;
      }else{
        $("#error-event-body").html("");
      }
    }else if(content_type != 'photos'){
      if(txt_body.length < 5){
        $("#error-body").html("required");
        $("#txt_body").focus();
        return false;
      }else{
        $("#txt_body").val(txt_body);
        $("#error-body").html("");
      }
    }else{
      if(alb_txt_body.length < 5){
        $("#error-alb-body").html("required");
        $("#alb_txt_body").focus();
        return false;
      }else{
        $("#error-alb-body").html("");
      }
	  
	  if(IsScript(alb_txt_body)){
				$("#error-alb-body").html("Please check Input");
				$("#alb_txt_body").focus();
				return false;
			}
    }
    
/* News SELECTION */
  if(content_type == 'news'){
      submit_url = base_url+"manage/submit_mt_entry";
      argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"txt_album": txt_album,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad};
   }

/* Announcement SELECTION */
  if(content_type == 'announcement'){
      submit_url = base_url+"manage/submit_mt_entry";
      argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad};
   }
    
/* PHOTO ALBUM SELECTION */
    if(content_type == 'photos'){
      if(album_category == 0 ){
        $("#error_album_category").html("required");
        $("#album_category_id").focus()
        return false;
      }else{
        $("#error_album_category").html("");
      }
	  
		if ($('.uploadifyQueueItem').length == 0) {
		$("#error_album_photos").html("required");
		return false;
		}
      var tmp_folder = $('#tmpfoldername').val();
      submit_url = base_url+"manage/submit_photo_album";
      argv_str = {"album_name":txt_title,"album_description":alb_txt_body,"album_category_id":album_category,"save": sub_button,"tmpfolder":tmp_folder};
    }

/* EVENTS SELECTION */
    if(content_type == 'events'){
      if(!txt_date){
        $("#error_date_time").html("Date is required");
        $("#txt_date").focus();
        return false;
      }else{
        $("#error_date_time").html("");
      }
      if(!txt_time){
        $("#error_date_time").html("Time is required");
        $("#txt_time").focus();
        return false;
      }else{
        $("#error_date_time").html("");
      }
      if(cmb_location <=0){
        $("#error_cmb_location").html("Location is required");
        $("#cmb_location").focus();
        return false;
      }else{
        $("#error_cmb_location").html("");
      }
      submit_url = base_url+"manage/submit_events";
      argv_str = {"hd_frm":1,"txt_title":txt_title,"txtar_desc":event_txt_body,"txt_date":txt_date,"txt_time":txt_time,"txt_to_date":txt_to_date,"txt_to_time":txt_to_time,"cmb_location":cmb_location,"cmb_concept":cmb_concept,"cmb_album":txt_album,"txt_publish": sub_button ,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad};
    }
/* OFFERS SELECTION */
    if(content_type == 'offers'){
      if(!txt_date_start){
        $("#error_offer_date_time").html("Start date is required");
        $("#txt_date_start").focus();
        return false;
      }else{
        $("#error_offer_date_time").html("");
      }
      if(!txt_date_end){
        $("#error_offer_date_time").html("End date is required");
        $("#txt_date_end").focus();
        return false;
      }
	  else
	  {
		var s_date = txt_date_start.split("/");
		var e_date = txt_date_end.split("/");
		var start_date = new Date(s_date[2],s_date[1],s_date[0]);
		var end_date = new Date(e_date[2],e_date[1],e_date[0]);
			if(start_date.getTime() > end_date.getTime())
			{
				$("#error_offer_date_time").html("Start date should not be greater than End date.");
				$("#txt_date_start").focus();
				 return false;
			}
			else
			{
				$("#error_offer_date_time").html("");
			}
      }
	  
      if(!offer_notification){
        $("#error_offer_notification").html("Required");
        $("#offer-notification").focus();
        return false;
      }else{
        $("#error_offer_notification").html("");
      }
      submit_url = base_url+"manage/submit_mt_entry";
      argv_str = {"entry_category":content_type,"offer_notification":offer_notification ,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"txt_date_start": txt_date_start,"txt_date_end":txt_date_end};
    }

    //$(this).attr("value","");
   // $(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
    //$(this).css("border",'0px');
   // $(this).css("width","40px");
    $(".submit_entry").attr("disabled","disabled");
    $.ajax({
      url: submit_url,
      data: argv_str,
      async: false,
      dataType: "json",
      type: "POST",
      success: function(result){
        if(content_type == 'photos'){
          if(result.status == "2"){
            $("#error_album_category").html(result.message);
            $("#album_category_id").focus();
			$(".submit_entry").removeAttr('disabled');
          }else{
	    $("#error_album_category").html('');
            $('#album_folder').val(result.album_folder);
            $('#album_id').val(result.album_id);
	    /** Change the tmp album path and and album id with Actual album id **/
	  
	    var tmp_name = $('#tmpfoldername').val();
	    /**  Updating the Actual photoalbum id with tmp_album path and id **/
	    $.post(base_url+'manage/update_album_id_path',{tmpfolder:tmp_name,new_album_id:result.album_id,new_album_folder:result.album_folder},function (data){
	      //alert(data);
	    });
	    
	    /** After completing all photo upload pusblish the photo album **/
	    album_id = $('#album_id').val();
	    publish_status = $('#alb_publish_status').val();
	    txt_title = $('#txt_title').val();
	    news_create_flag =  $('#news_create_flag').val();
	    var btn_text = $.trim($("#submit_content").val()); //general
	    
	    if(publish_status == 0 ){
	      $.ajax({
		type: 'POST',
		url: base_url+"manage/publish_album",
		data: {album_id : album_id},
		async: false,
		dataType: 'json',
		success: function(result){
		// window.location ='photos';
		  location.reload();
		}
	      });
	    }
            $('#alb_publish_status').val(result.publish_status);
            
          }
         return false;
        }
		
        if(result.status == "1"){
          window.location = result.redirect;
        }
      }
    });
    
    return false;
  })

  //$('#content_post_form').hide();
  $('.post_content_type').click(function(){
    var content_type = $(this).val();
    var btn_tite = $('#submit_content').attr('title');
    $('#content_post_form').show();
    $('.button-holder').show();
    $('.nicEdit-main').css('min-height','249px');
    $('.custom-form-element').hide();
    news_create_flag = false;
    $('#news_create_flag').val('no');
    //alert(content_type)
    switch(content_type){
      case 'news':$('#title-box').show();
                   $('#excerpt-box').show();
                   $('#body-box').show();
                   $('#album-box').show();
                   $('#email-notify-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#news-help').show();
                   $('#selected-content-type').html('News');
                   $('#submit_content').val(btn_tite);
                  break;
      case 'announcement':$('#title-box').show();
                   $('#excerpt-box').show();
                   $('#body-box').show();
                   $('#email-notify-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#announcement-help').show();
                   $('#selected-content-type').html('Announcement');
                   $('#submit_content').val(btn_tite);
                  break; 
      case 'photos':$('#title-box').show();
                   $('#alb-body-box').show();
                   $('#album_category-period-box').show();
                   $('#add-photo-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#photo-help').show();
                   $('#selected-content-type').html('Photos/Album');
                   $('#submit_content').val('Publish Now');
                  break;
      case 'events':$('#title-box').show();
                   $('#event-body-box').show();
                   $('#datetime-period-box').show();
                   $('#location-concept-box').show();
                   $('#email-notify-box').show();
                   $('#save_content').hide();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#events-help').show();
                   $('#selected-content-type').html('Events');
                   $('#submit_content').val(btn_tite);
                   break;
      case 'offers':$('#title-box').show();
                   $('#excerpt-box').show();
                   $('#body-box').show();
                   $('#date-period-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#offers-help').show();
                   $('#offer-notification-box').show();
                   $('#selected-content-type').html('Offers');
                   $('#submit_content').val('Publish Now');
                   break;

    }
  });
});

function remove_tags(html)
  {
       return html.replace(/<(?:.|\n)*?>/gm, '');
  }
  
function IsScript(sText){
    var filter = /<(?:.|\n)*?>/;
    if (filter.test(sText)) {
        return true;
    }else {
        return false;
    }
}
function check_uncheck_all(){
  $('#concAll').click(
    function(){
      $("input.conc").attr('checked', true);
    }
  );

  $('#concNone').click(
    function(){
      $("input.conc").attr('checked', false);
    }
  );

  $('#gradAll').click(
    function(){
      $("input.grad").attr('checked', true);
    }
  );

  $('#gradNone').click(
    function(){
      $("input.grad").attr('checked', false);
    }
  );

  $('#terrAll').click(
    function(){
      $("input.terr").attr('checked', true);
    }
  );

  $('#terrNone').click(
    function(){
      $("input.terr").attr('checked', false);
    }
  );
}
