/* Manage javascript File */
/* POST CONTENT */
//Initial Loader
var news_create_flag = false;
$(document).ready(function(){
  var base_url = siteurl;

  $('#txt_title').limit('120','#charsLeft');
  $('#txt_excerpt').limit('350','#charsLeft2');
  $('#txt_date_start').datepicker({ dateFormat: 'dd/mm/yy' }).val();
  $('#txt_date_end').datepicker({ dateFormat: 'dd/mm/yy' }).val();
  check_uncheck_all();
  
  $('#show-end-date').live('click',function(){
    $('#datetime-to-box').show();
    $('.event-sep').html('-');
    $(this).html('Hide end date');
    $(this).attr('id', 'hide-end-date');
    return false;
  })

   $('#hide-end-date').live('click',function(){
    $('#datetime-to-box').hide();
    $('.event-sep').html('+');
    $(this).html('Add end date');
    $(this).attr('id', 'show-end-date');
    $('#txt_to_date').val('Pick a date');
    $('#txt_to_time').val('Pick a time');
    return false;
  })
  $('#create-photo-album').click(function(){
    $('#category_photos').attr('checked','checked');
    $('.custom-form-element').hide();
    $('#title-box').show();
    $('#alb-body-box').show();
    $('#album_category-period-box').show();
    $('#add-photo-box').show();
    $('#save_content').show();
    $('.help-info').hide();
    $('#help-text').show();
    $('#photo-help').show();
    $('#selected-content-type').html('Photos/Album');
    $('#submit_content').val('Publish Now');
    news_create_flag = true;
    $('#news_create_flag').val('yes');
    return false;
  })
  $('.submit_entry').click(function(){

   // alert('er');

    var argv_str = null; //Argument string for posting value to the forms, set at differnet section

    var content_type = $('input[name=entry_category]:checked', '#frm_post').val();
    var notify_concept = $('.conc:checked').map(function(i,n) {
        return $(n).val();
    }).get();
    
    var notify_terr = $('.terr:checked').map(function(i,n) {
        return $(n).val();
    }).get();

    var notify_grad = $('.grad:checked').map(function(i,n) {
        return $(n).val();
    }).get();
    var submit_url = null;
    var sub_button = $(this).val();//General
    var txt_title = $.trim($("#txt_title").val());//General
    var txt_excerpt = $.trim($("#txt_excerpt").val());//General
    var txt_body = $.trim($("#frm_post .nicEdit-main").html());//General
    var txt_album = $.trim($("#txt_album").val());//News
    var album_category = $.trim($("#album_category_id").val());//Photos
    var alb_txt_body = $.trim($("#alb_txt_body").val());//Photos
    var event_txt_body = $.trim($("#event_txt_body").val());//Events
    var txt_date = $.trim($("#txt_date").val());//Events
    var txt_time = $.trim($("#txt_time").val());//Events
    var txt_to_date = $.trim($("#txt_to_date").val());//Events
    var txt_to_time = $.trim($("#txt_to_time").val());//Events
    var cmb_location = $.trim($("#cmb_location").val()); //Events
    var cmb_concept = $.trim($("#cmb_concept").val()); //Events
    var txt_date_start = $.trim($("#txt_date_start").val()); //Offers
    var txt_date_end = $.trim($("#txt_date_end").val()); //Offers
    var btn_text = $.trim($("#submit_content").val()); //general
	var entry_id = $.trim($("#entry_id").val()); //general


    if(txt_date == 'Pick a date'){
      txt_date = null;
    }
    if(txt_time == 'Pick a time'){
      txt_time = null;
    }
    if(txt_to_date == 'Pick a date'){
      txt_to_date = null;
    }
    if(txt_to_time == 'Pick a time'){
      txt_to_time = null;
    }

    if(txt_date_start == 'Start date'){
      txt_date_start = null;
    }
    if(txt_date_end == 'End date'){
      txt_date_end = null;
    }
    
    
    //[nicInstance].getContent()
    if(sub_button == "Save as draft"){
      sub_button = 1;
    }else{
      sub_button = 2;
    }
   
    /* General validation for all the FORMS */
    if(!txt_title){
      $("#error-title").html("required");
	  $("#txt_title").val('');
      $("#txt_title").focus()
      return false;
    }else{
      $("#error-title").html("");
    }
	if(IsScript(txt_title)){
      $("#error-title").html("Please check Input");
      $("#txt_title").focus()
      return false;
    }
	if(txt_excerpt){
		if(IsScript(txt_excerpt)){
		  $("#error-excerpt").html("Please check Input");
		  $("#txt_excerpt").focus()
		  return false;
		}
		else{
		  $("#error-excerpt").html("");
		}
	}
    if(content_type != 'photos'){
      if(txt_body.length < 5){
        $("#error-body").html("required");
        $("#txt_body").focus();
        return false;
      }else{
        $("#txt_body").val(txt_body);
        $("#error-body").html("");
      }
    }else{
			if(IsScript(alb_txt_body)){
				$("#error-alb-body").html("Please check Input");
				$("#alb_txt_body").focus();
				return false;
			}
			else{
				$("#error-alb-body").html("");
			}
			if(alb_txt_body.length < 5){
				$("#error-alb-body").html("required");
				$("#alb_txt_body").focus();
				return false;
			}else{
				$("#error-alb-body").html("");
			}
	  
	  
	
    }
    
/* News SELECTION */
  if(content_type == 'news'){
		  if(entry_id == 0 || entry_id == '')
		  {
		  submit_url = base_url+"manage/submit_mt_entry";
		  argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"txt_album": txt_album,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad};
		  } else {
		  submit_url = base_url+"manage/edit_mt_entry";
		  argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"txt_album": txt_album,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad, "entry_id":entry_id};
		  
		  }
   }

/* Announcement SELECTION */
  if(content_type == 'announcement'){
	if(entry_id == 0 || entry_id == '')
		  {
		  submit_url = base_url+"manage/submit_mt_entry";
		  argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad};
	  } else {
		submit_url = base_url+"manage/edit_mt_entry";
		  argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"terr[]":notify_terr,"conc[]":notify_concept,"grad[]":notify_grad,"entry_id":entry_id};
	  }
   }
    
/* PHOTO ALBUM SELECTION */
    if(content_type == 'photos'){
      if(album_category == 0 ){
        $("#error_album_category").html("required");
        $("#album_category_id").focus()
        return false;
      }else{
        $("#error_album_category").html("");
      }
	  
      if ($('.uploadifyQueueItem').length == 0) {
	$("#error_album_photos").html("required");
	return false;
      }
	
      // Get tmp folder name
      var tmp_folder = $('#tmpfoldername').val();
      submit_url = base_url+"manage/submit_photo_album";
      argv_str = {"album_name":txt_title,"album_description":alb_txt_body,"album_category_id":album_category,"save": sub_button,"tmpfolder":tmp_folder};
    }

    /* OFFERS SELECTION */
    if(content_type == 'offers'){
      if(!txt_date_start){
        $("#error_offer_date_time").html("Start date is required");
        $("#txt_date_start").focus();
        return false;
      }else{
        $("#error_offer_date_time").html("");
      }
      if(!txt_date_end){
        $("#error_offer_date_time").html("End date is required");
        $("#txt_date_end").focus();
        return false;
      }else{
        var s_date = txt_date_start.split("/");
	var e_date = txt_date_end.split("/");
	var start_date = new Date(s_date[2],s_date[1],s_date[0]);
	var end_date = new Date(e_date[2],e_date[1],e_date[0]);
	if(start_date.getTime() > end_date.getTime()) {
		$("#error_offer_date_time").html("Start date should not be greater than End date.");
		$("#txt_date_start").focus();
		 return false;
	} else {
		$("#error_offer_date_time").html("");
	}
      }
	  
      if(entry_id == 0 || entry_id == '') {
	submit_url = base_url+"manage/submit_mt_entry";
	argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"txt_date_start": txt_date_start,"txt_date_end":txt_date_end};
      } else {		
	submit_url = base_url+"manage/edit_mt_entry";
	argv_str = {"entry_category":content_type,"txt_title":txt_title,"txt_excerpt":txt_excerpt,"txt_body":txt_body,"txt_publish": sub_button ,"txt_date_start": txt_date_start,"txt_date_end":txt_date_end,"entry_id":entry_id};
	}
    }

    argv_str.concept_id = $('#concept_id').val();

    //$(this).attr("value","");
    //$(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
    //$(this).css("border",'0px');
    //$(this).css("width","40px");
    $(".submit_entry").attr("disabled","disabled");
	$.fancybox.showLoading();
    $.ajax({
      url: submit_url,
      data: argv_str,
      async: false,
	  cache: false,
      dataType: "json",
      type: "POST",
      success: function(result){
        if(content_type == 'photos'){
          if(result.status == "2"){
            $("#error_album_category").html(result.message);
            $("#album_category_id").focus();
			$(".submit_entry").removeAttr('disabled');
			$.fancybox.hideLoading();
          }else{
            $("#error_album_category").html('');
            $('#album_folder').val(result.album_folder);
            $('#album_id').val(result.album_id);
	    /**  Reomve the photo upload as photo upload inovkes automatically **/
	   // $('#upload').uploadifySettings('folder',result.album_upload_path+result.album_folder);
	   // $('#upload').uploadifyUpload();
	    
	    /** Change the tmp album path and and album id with Actual album id **/
	    var tmp_name = $('#tmpfoldername').val();
		 var album_id = $('#album_id').val();
	    /**  Updating the Actual photoalbum id with tmp_album path and id **/
	    $.post(base_url+'manage/update_album_id_path',{tmpfolder:tmp_name,new_album_id:result.album_id,new_album_folder:result.album_folder, album_id:album_id},function (data){
	      //alert(data);
	    });
	    
	    /** After completing all photo upload pusblish the photo album **/
	    album_id = $('#album_id').val();
	    publish_status = $('#alb_publish_status').val();
	    txt_title = $('#txt_title').val();
	    news_create_flag =  $('#news_create_flag').val();
	    var btn_text = $.trim($("#submit_content").val()); //general
	    
	    if(publish_status == 0 ){
	      $.ajax({
		type: 'POST',
		url: base_url+"manage/publish_album",
		data: {album_id : album_id},
		async: false,
		cache: false,
		dataType: 'json',
		success: function(result){
		//alert('ajax call here')
		// window.location ='photos';
		  location.reload();
		}
	      });
	    }
            $('#alb_publish_status').val(result.publish_status);
          }
         return false;
        }
        if(result.status == "1"){
		$(this).attr("value","");
        $(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
        $(this).css("border",'0px');
        $(this).css("width","40px");
			$.fancybox.hideLoading();
		  //location.reload();
		  window.location.href =document.URL;
		 //alert('hi');
        }
      }
    });
    
    return false;
  })

  //$('#content_post_form').hide();
  $('.post_content_type').click(function(){
    var content_type = $(this).val();
    var btn_tite = $('#submit_content').attr('title');
    $('#content_post_form').show();
    $('.button-holder').show();
    $('.nicEdit-main').css('min-height','249px');
    $('.custom-form-element').hide();
    news_create_flag = false;
    $('#news_create_flag').val('no');
    //alert(content_type)
    switch(content_type){
      case 'news':$('#title-box').show();
                   $('#excerpt-box').show();
                   $('#body-box').show();
                   $('#album-box').show();
                   $('#email-notify-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#news-help').show();
                   $('#selected-content-type').html('News');
                   $('#submit_content').val(btn_tite);
                  break;
      case 'announcement':$('#title-box').show();
                   $('#excerpt-box').show();
                   $('#body-box').show();
                   $('#email-notify-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#announcement-help').show();
                   $('#selected-content-type').html('Announcement');
                   $('#submit_content').val(btn_tite);
                  break; 
      case 'photos':$('#title-box').show();
                   $('#alb-body-box').show();
                   $('#album_category-period-box').show();
                   $('#add-photo-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#photo-help').show();
                   $('#selected-content-type').html('Photos/Album');
                   $('#submit_content').val('Publish Now');
                  break;
      case 'offers':$('#title-box').show();
                   $('#excerpt-box').show();
                   $('#body-box').show();
                   $('#date-period-box').show();
                   $('#save_content').show();
                   $('.help-info').hide();
                   $('#help-text').show();
                   $('#offers-help').show();
                   $('#offer-notification-box').show();
                   $('#selected-content-type').html('Offers');
                   $('#submit_content').val('Publish Now');
                   break;

    }
	//$.fancybox.resize();
  });
});

function remove_tags(html)
  {
       return html.replace(/<(?:.|\n)*?>/gm, '');
  }

function check_uncheck_all(){
  $('#concAll').click(
    function(){
      $("input.conc").attr('checked', true);
    }
  );

  $('#concNone').click(
    function(){
      $("input.conc").attr('checked', false);
    }
  );

  $('#gradAll').click(
    function(){
      $("input.grad").attr('checked', true);
    }
  );

  $('#gradNone').click(
    function(){
      $("input.grad").attr('checked', false);
    }
  );

  $('#terrAll').click(
    function(){
      $("input.terr").attr('checked', true);
    }
  );

  $('#terrNone').click(
    function(){
      $("input.terr").attr('checked', false);
    }
  );
}
