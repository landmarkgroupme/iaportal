/* Manage javascript File */
//Initial Loader
$(document).ready(function(){
  var base_url = $("#base_url").val();
  $('#txt_title').limit('120','#charsLeft');
  $('#txt_excerpt').limit('350','#charsLeft2');
  //$('#txt_body').limit('2500','#charsLeft3');
	
  check_uncheck_all();
	
  $(".submit_entry").click(function(){
  
    var sub_button = $(this).val();
    var txt_title = $.trim($("#txt_title").val());
    var txt_body = $.trim($(".nicEdit-main").html());
    //[nicInstance].getContent()
    if(sub_button.trim() == "Save as draft"){
      $("#txt_publish").val(1);
    }else if(sub_button.trim() == "Reject"){
      $("#txt_publish").val(4);
    }else{
      $("#txt_publish").val(2);
    }
    //var file_image = $.trim($("#file_image").val());
	  
    if(!txt_title){
      $("#error-title").html("required");
      $("#txt_title").focus()
      return false;
    }else{
      $("#txt_title").html("");
    }
	  
    if(txt_body.length < 5){
      $("#error-body").html("Body is required");
      $("#txt_body").focus();
      return false;
    }else{
      $("#txt_body").val(txt_body);
      $("#error-body").html("");
    }
	  
    $(this).attr("value","");
    $(this).css("background","url("+base_url+"images/indicator.gif) no-repeat 0 10px");
    $(this).css("border",'0px');
    $(this).css("width","40px");
    return true;
  })
	
  var options = {
    success: function(e){
      if(e.status == "1"){
        window.location = "manage/"+e.redirect;
      }else{
        alert("Error in submission");
      }
    },
    url: 'manage/submit_mt_entry',
    dataType: 'json',
    type: 'post',
    beforeSubmit:function(){
      $(".submit_entry").attr("disabled","disabled");
    }
  };
  $('#frm_create_news').ajaxForm(options);

  var edit_options = {
    success: function(res){
      window.location = base_url+"manage/"+res.redirect;
    },
    url: base_url+'manage/update_mt_entry',
    dataType: 'json',
    type: 'post',
    beforeSubmit:function(){
      $(".submit_entry").attr("disabled","disabled");
    }
  };
  $('#frm_update_news').ajaxForm(edit_options);

});
function check_uncheck_all(){
  $('#concAll').click(
    function(){
      $("input.conc").attr('checked', true);   
    }
  );
  
  $('#concNone').click(
    function(){
      $("input.conc").attr('checked', false);   
    }
  );
  
  $('#gradAll').click(
    function(){
      $("input.grad").attr('checked', true);   
    }
  );
  
  $('#gradNone').click(
    function(){
      $("input.grad").attr('checked', false);   
    }
  );
  
  $('#terrAll').click(
    function(){
      $("input.terr").attr('checked', true);   
    }
  );
  
  $('#terrNone').click(
    function(){
      $("input.terr").attr('checked', false);   
    }
  );
}
