/* Manage javascript File */

$(document).ready(function(){
/* Calendar */
  $('#txt_date').calendricalDate();
  $('#txt_time').calendricalTime();
  $("#event-submit").click(function(){
  	var txt_title = $.trim($("#txt_title").val()); 
    var txt_desc = $.trim($("#txtar_desc").val());
    var txt_date = $.trim($("#txt_date").val());
    var txt_time = $.trim($("#txt_time").val()); 
    var cmb_location = $.trim($("#cmb_location").val());
    
    if(!txt_title){
      $("#error_txt_title").html("Title is required");
      $("#txt_title").focus();
      return false;
    }else{
      $("#error_txt_title").html("");
    }
    if(!txt_desc){
      $("#error_txtar_desc").html("Description is required");
      $("#txtar_desc").focus();
      return false;
    }else{
      $("#error_txt_desc").html("");
    }
    if(!txt_date){
      $("#error_date_time").html("Date is required");
      $("#txt_date").focus();
      return false;
    }else{
      $("#error_date_time").html("");
    }
    if(!txt_time){
      $("#error_date_time").html("Time is required");
      $("#txt_time").focus();
      return false;
    }else{
      $("#error_date_time").html("");
    }
    if(cmb_location <=0){
      $("#error_cmb_location").html("Location is required");
      $("#cmb_location").focus();
      return false;
    }else{
      $("#error_cmb_location").html("");
    }
    return true;
  })
})
//Initial Loader
 