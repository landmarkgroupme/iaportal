/* Edit Profile javascript File */
$(document).ready(function(){
/* Edit Profile */
	function split(val) {
		return val.split(/,\s*/);
	}
	function extractLast(term) {
		return split(term).pop();
	}
	$("#frm-edit-profile").submit(function(){
	  var interest = $.trim($("#txtar-interest").val());
	  if(!interest){
	    return true;
	  }
	  var interest_arr = interest.split(",");
	  var error_str = [];
	  $.each(interest_arr, function(key, value) { 
      var val = $.trim(value);
      if(val.length > 15){
        error_str.push(val);
      }
    });
	  if(error_str.length >= 1){
	    var error_msg = "";
	    if(error_str.length <= 1){
	      error_msg = "is";
	    }else{
	      error_msg = "are";
	    }
	    error_str = error_str.join(", ");
      
	    //error_msg = error_str+" "+error_msg+" not allowed. <strong>Please restrict your interest group to 15 characters each (including spaces).</strong>";
	    error_msg = "Please restrict your interest group to <strong>15 characters</strong> each (including spaces).";
	    $("#error-interest-groups").html(error_msg);
	    $("#error-interest-groups").show();
	    return false;
	  }else{
	    $("#error-interest-groups").html("");
      $("#error-interest-groups").hide();
	  }
   	  
	})
	$("#txtar-interest").autocomplete({
		source: function(request, response){
	      $.ajax({
				  url: "autocomplete_interest_groups",
				  dataType: 'json',
					type:"post",
				  data: {"term":extractLast(request.term)}, 
				  success: response
				});
      },
		search: function() {
			// custom minLength
			var term = extractLast(this.value);
			if (term.length < 2) {
				return false;
			}
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		},
		select: function(event, ui) {
			var terms = split( this.value );
			// remove the current input
			terms.pop();
			// add the selected item
			terms.push( ui.item.value );
			// add placeholder to get the comma-and-space at the end
			terms.push("");
			this.value = terms.join(", ");
			return false;
		}
	});
})
