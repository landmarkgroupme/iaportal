/* My Profile javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* My Profile */
/* Intial Load */
	initialize();

  initialise_status_update();
})
//Initial Loader
function initialize(){
	/* Share Button Click */
  $("#btn-ar-share").click(function(){
    var txtarShare = $.trim($("#txtarShare").val());
    var activeTab = ($(".active").attr("id"))
    if((txtarShare == "What are you working on ?") || (txtarShare.length < 2)){
      $("#txtarShare").focus();
      return false;
    }
     var service_url = $("#base_url").val()+"index.php/status_update/ajax_submit_status_message";
    
     $.ajax({
      url: service_url,
      data: {"txtarShare":txtarShare},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        var update = msg.update *1;
        if(update){
          //Upadate if Tabs are All or Status update 
            $('#status-msg-status').html("New status message posted");
            $('#status-msg-status').show();
            $("#txtarShare").val('What are you working on ?');
            $("#txtarShare").animate({ height: "2em" }, 200,function(){
              $('#btn-ar-share').hide();
              $('#status-msg-count').hide();
            });
        }
      }
    });
    return false;
  })
	
}

function initialise_status_update(){
  //Focus on textarea changes backgroud
  $("#txtarShare").focus(function(){
    var base_url = $("#base_url").val()
    var text = $("#txtarShare").val();
    if(text == "What are you working on ?"){
      $("#txtarShare").val("");
    }
    $(this).animate({ height: "5.5em" }, 200,function(){
      $('#btn-ar-share').show();
      $('#status-msg-count').show();
    });
   // $(this).css("background","url('"+base_url+"images/bg-share-form-textarea-focus.jpg') no-repeat scroll 0 0 transparent");
  })

  $("#txtarShare").blur(function(){
    var base_url = $("#base_url").val()
    var text = $.trim($("#txtarShare").val());
    if(text == ""){
      $("#txtarShare").val("What are you working on ?");
      $(this).animate({ height: "2em" }, 200,function(){
            $('#btn-ar-share').hide();
        $('#status-msg-count').hide();
      });
    }
    //$(this).css("background","url('"+base_url+"images/bg-share-form-textarea.jpg') no-repeat scroll 0 0 transparent");
  })

  limitChars("txtarShare",160,"status-msg-count");// Checking when page is reloaded
  $("#txtarShare").keyup(function(){
    limitChars("txtarShare",160,"status-msg-count");
  })
}
function limitChars(textid, limit, infodiv) {
  var text = $('#'+textid).val();
  text = (text == "What are you working on ?")? "":text;
  var textlength = text.length;
  if(textlength > (limit-1)){
    $('#' + infodiv).html("0");
    $('#'+textid).val(text.substr(0,limit));
    return false;
  }else{
    $('#' + infodiv).html((limit - textlength));
    return true;
  }
}