/* Status Update(su) javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* Status Updates */
//Unfollow User	
	$(".msg-unfollow").live("click",function(){
		var user = $(this).attr("rel");
		var service_url = $(this).attr("href")+"/unfollow_user";
		var active_tab = $(".active").attr("id");
		$.ajax({
			url: service_url,
			data: {"unfollow_user":user},
			async: false,
			dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$("[rel="+user+"]").text("Follow");
					$("[rel="+user+"]").removeClass("msg-unfollow");
					$("[rel="+user+"]").addClass("msg-follow");
					if(active_tab == "su_show_following_people"){
						setTimeout(function(){ window.location = "following_people";}, 2000 );
					}
				}
		  }
		});
		return false;
	})
//Follow User
	$(".msg-follow").live("click",function(){
		var user = $(this).attr("rel");
		var service_url = $(this).attr("href")+"/follow_user";

		$.ajax({
			url: service_url,
			data: {"follow_user":user},
			async: false,
			dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					$("[rel="+user+"]").text("Unfollow");
					$("[rel="+user+"]").addClass("msg-unfollow");
					$("[rel="+user+"]").removeClass("msg-follow");
				}
		  }
		})
		return false;
	})
//Delete Message
	$(".msg-delete").live("click",function(){
		var message_id = this.id;
		var active_tab = $(".active").attr("id");
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"message_id":message_id},
			async: false,
			dataType: "json",
			type: "POST", 
			success: function(msg){
				if(msg.affected_row >= 1){
					pagesize =0;
					loadFiltersContents(active_tab,"new");
				}
		  }
		})
		return false;
	})	
//Load More ACtion
	$(".more-stories").click(function(){
		pagesize = pagesize + (this.id*1);
		var service_url = $(this).attr("href");
		var profile = $(this).attr("rel");
		profile = profile.split("_");
		var profile_id = profile[1];
		$.ajax({
			url: service_url,
			data: {"pagesize":pagesize,"profile_id":profile_id},
			async: false,
			dataType: "html",
			type: "POST", 
			success: function(msg){
				if(msg){
					$("#show-list").append(msg);
				}else{
					$(".more-stories").hide();
				}
		  }
		})
		return false;
	})
})

 