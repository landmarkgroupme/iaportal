/* Files Module javascript File */
$(document).ready(function(){
  /* Files */
  /* Intial Load */
  initialize();
	
/* Form validation init */
//form_validation_init();
})

function split(val){
  return val.split(/,\s*/);
}
function extractLast(term){
  return split(term).pop();
}

function url_validation(url) {
  var v = new RegExp();
  v.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");
  if (!v.test(url)) {
    return false;
  }else{
    return true;
  }
} 
function initialize(){
  //Focus on tags changes default text
  $("#txt_sendto").focus(function(){
    var text = $("#txt_sendto").val();
    if(text == "Start typing name.."){
      $("#txt_sendto").val("");
    }
  })
  $("#txt_sendto").blur(function(){
    var text = $.trim($("#txt_sendto").val());
    if(text == ""){
      $("#txt_sendto").val("Start typing name..");
    }
  })
	
  //Show/hide Subject and message
  $("#subject-toggle").hide();
  $("#show-subject").click(function(){
    $("#subject-toggle").toggle();
    $(this).toggleClass("toggle-show-label");
    return false;
  })

  $("#message-toggle").hide();
  $("#show-msg").click(function(){
    $("#message-toggle").toggle();
    $(this).toggleClass("toggle-show-label");
    return false;
  })
	
	
	
  var service_url = $("#base_url").val();
  $("#txt_sendto").autocomplete({
    source: function(request, response){
      $.ajax({
        url: service_url+"index.php/files/autocomplete_files_send_to",
        dataType: 'json',
        type:"post",
        data: {
          "term":extractLast(request.term)
          },
        success: response
      });
    },
    search: function(){
      // custom minLength
      var term = extractLast(this.value);
      if(term=="Start typing name.."){
        return false;
      }
      if (term.length < 2) {
        return false;
      }
    },
    focus: function(){
      // prevent value inserted on focus
      return false;
    },
    select: function(event, ui){
      var terms = split(this.value);
      // remove the current input
      terms.pop();
      // add the selected item
      terms.push(ui.item.value);
      // add placeholder to get the comma-and-space at the end
      terms.push("");
      this.value = terms.join(", ");
      $("#error-txt_sendto").html("");
      return false;
    }
  });
}
function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
}

function form_validation_init(){
  //Form Validation
  //$("#bt_send_file").click(function(){
  var base_url = $("#base_url").val();
  var sendTo = $.trim($("#txt_sendto").val());
  var file_share = $.trim($("#file_share").val());
		
  //alert(sendTo);
		
  if((sendTo == "Start typing name..") || (sendTo.length < 2) ||(sendTo == "No Results,")){
    $("#error-txt_sendto").html("Required");
    $("#txt_sendto").focus();
    return false;
  }else{
    var email_arr= sendTo.split(",");
    var email_check = false;
    $.each(email_arr, function() {
      var email_addr = $.trim(this);
      if(email_addr){
        if(!isValidEmailAddress(email_addr)){
          $("#error-txt_sendto").html("<strong>"+this+"</strong> seems to be a invalid email format");
          email_check = false;
          return false;
        }
        email_check = true;
      }
    });

    if(!email_check){
      $("#txt_sendto").focus();
      return false;
    }
    $("#error-txt_sendto").html("");
  }
		
  if(!file_share){
    $("#error-file_share").html("Required");
    $("#file_share").focus()
    return false;
  }else{
    $("#error-file_share").html("");
  };
		
  $(this).attr("disabled","disabled");
  $(this).attr("src",base_url+"images/indicator.gif");
  $(".submit-div").prepend('<span style="word-spacing: 0px;">File Upload in progress </span>');
  return true;
//})
}

