/* Files Module javascript File */
$(document).ready(function(){
/* Files */
  //files_term search
  files_term_search_form()
	/* Popular Tag limit */
	popular_tag_limit();
	
	//File home page design IMP FILES
	$(".imp_cat_link li a").click(function(){
	  var id = $(this).attr("rel");
	  var class_name = "imp_files";
    
	  $("."+class_name).hide();
	  $("#"+id).show();
	  $(".imp_cat_link li").removeClass("active-cat");
	  $(this).parent("li").addClass("active-cat");
	  
	  return false;
	})
	
	
	//File home page design IMP FILES
  $(".gen_cat_link li a").click(function(){
    var id = $(this).attr("rel");
    var class_name = "gen_files";
    
    $("."+class_name).hide();
    $("#"+id).show();
    $(".gen_cat_link li").removeClass("active-cat");
    $(this).parent("li").addClass("active-cat");
    
    return false;
  })
	
	//Dropbox Help
	$("#show-dropbox-help").click(function(){
	  $("#dropbox-info").show();
	  return false;
	})
	$("#dropbox-info").mouseleave(function(){
	  $("#dropbox-info").fadeOut("slow");
	})
	
	$(".show-tip").mouseover(function(){
	  var id = this.id;
	  var tool_tip_margin = ($(this).width())+8;
    
	  $(".tool-tip").hide();
	  $(".tool-tip").css("margin-left",tool_tip_margin);
	  $("#show_tip_"+id).show(); 
	})
	$(".tool-tip").mouseleave(function(){
	  $(".tool-tip").hide();
	})
	
	//Advanced search Check all function
	load_advance_search();
	
	/* File Tag Sort */
	$(".tag_sort_no").live("click",function(){
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"sort_order":"total_count"},
			async: false,
      dataType: "html",
			type: "POST", 
			success: function(tag_list){
				$("#files-popular-tag").html(tag_list);
				popular_tag_limit();//reloading the tag limit
		  }
		});
		return false;
	});
	$(".tag_sort_name").live("click",function(){
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"sort_order":"tags"},
			async: false,
      dataType: "html",
			type: "POST", 
			success: function(tag_list){
				$("#files-popular-tag").html(tag_list);
				popular_tag_limit();//reloading the tag limit
		  }
		});
		return false;
	})
	/* File Category Sort */
	$(".file_cat_sort_no").live("click",function(){
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"sort_order":"total_count"},
			async: false,
      dataType: "html",
			type: "POST", 
			success: function(category_list){
				$(".category-list-holder").html(category_list);
				popular_tag_limit();//reloading the tag limit
		  }
		});
		return false;
	});
	$(".file_cat_sort_name").live("click",function(){
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"sort_order":"file_category"},
			async: false,
      dataType: "html",
			type: "POST", 
			success: function(category_list){
				$(".category-list-holder").html(category_list);
				popular_tag_limit();//reloading the tag limit
		  }
		});
		return false;
	})
	
	
})

/* Popular Tag limit */
function popular_tag_limit(){
	var list = $('.tag-list li:gt(15)');
	list.hide();
	$('.files-see-more a').click(function() {
		list.toggle(200);
		$(this).toggleClass('hide-some');
		return false;
	})
}

//Advance Search Function
function load_advance_search(){
	//Advance search Check All the Concepts	
	$('.concept_check_all').click(function(){
		$('.checkbox_concepts').attr("checked","checked");
		return false;
	})
//Uncheck All the Concepts	
	$('.concept_check_none').click(function(){
		$('.checkbox_concepts').removeAttr("checked");
		return false;
	})
//Check All the Locations	
	$('.location_check_all').click(function(){
		$('.checkbox_locations').attr("checked","checked");
		return false;
	})
//Uncheck All the Locations	
	$('.location_check_none').click(function(){
		$('.checkbox_locations').removeAttr("checked");
		return false;
	})
}

//Search for files
function files_term_search_form(){
  //Focus on textarea changes backgroud 
  $("#files_term").focus(function(){
    var text = $("#files_term").val();
    if(text == "Search for files"){
      $("#files_term").val("");
    }
  })
  $("#files_term").blur(function(){
    var text = $.trim($("#files_term").val());
    if(text == ""){
      $("#files_term").val("Search for files");
    }
  })
}
