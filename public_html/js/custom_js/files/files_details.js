/*
 * File details javascripts
 * 
 */
var present_file_tags = ""; //Present File Tags
$(document).ready(function(){
  $('div.edit-file-desc').editableText({
    newlinesEnabled: true
  });

//File Description
  $('.edit-file-desc').change(function(){
    var desc = $(this).html();
    var service_url = $("#base_url").val();
    var file_id = $(".edit-file-desc").attr("id");

    $.ajax({
      type: "POST",
      url: service_url+"index.php/files/submit_edit_file_description",
      data: {"file_desc": desc,"file_id":file_id},
      dataType:"json",
      success: function(msg){
        //alert( "Data Saved: " + msg );
      }
    });

  });
//File Tags
  $("#edit-tags").live("click",function(){
    //Autocompelete for Tags
    present_file_tags = $("#file-tags").text();    
    var auto_box = '<input class="txt_title" type="text" name="txt_tags" id="txt_tags" size="60" value="'+present_file_tags+'"/>';
    $("#file-tags").html(auto_box);
    fn_tags_autocomplete();
    
    var links = '<a class="save" id="edit-tags-save" href="#"></a><a class="cancel" id="edit-tags-cancel" href="#"></a>';
    $('#edit-file-tags').html(links);
    
    $("#txt_tags").focus();
    
    return false;
  })
  //Cancel the File tag editing
  $("#edit-tags-cancel").live("click",function(){
    //Autocompelete for Tags
    $("#file-tags").text(present_file_tags);    
    
    var links = '<a class="edit" id="edit-tags" href="#"></a>';
    $('#edit-file-tags').html(links);
    return false;
  })
  
  //Save the File tag editing
  $("#edit-tags-save").live("click",function(){
    var service_url = $("#base_url").val();
    var file_id = $(".edit-file-desc").attr("id");
    var file_tags = $("#txt_tags").val();
    $.ajax({
      type: "POST",
      url: service_url+"index.php/files/submit_edit_file_tags",
      data: {"file_tags": file_tags,"file_id":file_id},
      success: function(msg){
        $("#file-tags").text(file_tags);    
        var links = '<a class="edit" id="edit-tags" href="#"></a>';
        $('#edit-file-tags').html(links);
      }
    });
    return false;
  })
  
})
  //Autocompelete for Tags
function fn_tags_autocomplete(){
  var service_url = $("#base_url").val();
  $("#txt_tags").autocomplete({
      source: function(request, response){
        $.ajax({
          url: service_url+"index.php/files/autocomplete_files_tag",
          dataType: 'json',
          type:"post",
          data: {"term":extractLast(request.term)}, 
          success: response
        });
      },
      search: function(){
        // custom minLength
        var term = extractLast(this.value);
        if(term=="Start typing.."){
          return false;
        }
        if (term.length < 2) {
            return false;
        }
      },
      focus: function(){
        // prevent value inserted on focus
        return false;
      },
      select: function(event, ui){
        var terms = split(this.value);
        // remove the current input
        terms.pop();
        // add the selected item
        terms.push(ui.item.value);
        // add placeholder to get the comma-and-space at the end
        terms.push("");
        this.value = terms.join(", ");
        return false;
      }
  });
}
function extractLast(term){
  return split(term).pop();
}
function split(val){
  return val.split(/,\s*/);
}