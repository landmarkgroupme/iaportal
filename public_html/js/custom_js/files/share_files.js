/* Files Module javascript File */
$(document).ready(function(){
  /* Files */
  /* Intial Load */
  initialize();

  /* Form validation init */
  //form_validation_init();

  //Web link or file upload toggle.
  $("#cmb_cat").change(function(){
    var cat_id = $(this).val();
    cat_id = cat_id*1;
    var share_link_allow = [ 8,15,16,17 ];
    var check = $.inArray(cat_id , share_link_allow)
    if(check < 0){
      $("#share-file").show()
      $("#share-link").hide()
      $("#share-file-link").hide()
    }else if(cat_id == 8){
      $("#share-file").hide()
      $("#share-file-link").hide()
      $("#share-link").show()
    } else{
      $("#share-file").show()
      $("#share-file-link").show()
      $("#share-link").show()
    }


  })

})

function split(val){
  return val.split(/,\s*/);
}
function extractLast(term){
  return split(term).pop();
}


function initialize(){
  //Focus on tags changes default text
  $("#txt_tags").focus(function(){
    var text = $("#txt_tags").val();
    if(text == "Start typing.."){
      $("#txt_tags").val("");
    }
  })
  $("#txt_tags").blur(function(){
    var text = $.trim($("#txt_tags").val());
    if(text == ""){
      $("#txt_tags").val("Start typing..");
    }
  })
  var base_url = $("#base_url").val();
  $("#txt_tags").autocomplete({
    source: function(request, response){
      $.ajax({
        url: base_url+"files/autocomplete_files_tag",
        dataType: 'json',
        type:"post",
        data: {
          "term":extractLast(request.term)
          },
        success: response
      });
    },
    search: function(){
      // custom minLength
      var term = extractLast(this.value);
      if(term=="Start typing.."){
        return false;
      }
      if (term.length < 2) {
        return false;
      }
    },
    focus: function(){
      // prevent value inserted on focus
      return false;
    },
    select: function(event, ui){
      if(document.selection) { 
          this.focus(); 
          var oSel = document.selection.createRange(); 
          oSel.moveStart('character',this.value.length); 
          oSel.moveEnd('character',0); 
          oSel.select(); 
      }
      var terms = split(this.value);
      // remove the current input
      terms.pop();
      // add the selected item
      terms.push(ui.item.value);
      // add placeholder to get the comma-and-space at the end
      terms.push("");
      this.value = terms.join(", ");
      return false;
    }
  });
}


function form_validation_init(){
  //Form Validation
  //$("#frm_add_item").submit(function(){
  var base_url = $("#base_url").val();
  var cat = $.trim($("#cmb_cat").val());
  var file_share = $.trim($("#file_share").val());
  var txt_link = $.trim($("#txt_link").val());
  var title = $.trim($("#txt_title").val());
  //var desc = $.trim($("#txtar_description").val());
  //var txtfilestag = $.trim($("#txt_tags").val());


  if(!cat){
    $("#error-cmb_cat").html("Required");
    $("#cmb_cat").focus()
    return false;
  }else{
    $("#error-cmb_cat").html("");
  };

  if(file_share){
    $("#error-files_upload").html("");

  }else if((txt_link == "") || (txt_link.length < 2) &&(file_share == "")){
    $("#error-files_upload").html("Required");
    $("#error-files_upload").focus()
    return false;
  }else if(txt_link){
    if(url_validation(txt_link)){
      $("#error-files_upload").html("");
    }else{
      $("#error-files_upload").html("Not a valid link");
      return false;
    }
  }else{
    $("#error-files_upload").html("");
  };


  if(!title){
    $("#error-txt_title").html("Required");
    $("#txt_title").focus()
    return false;
  }else{
    $("#error-txt_title").html("");
  };

  $(this).attr("disabled","disabled");
  $(this).attr("src",base_url+"images/indicator.gif");
  $("#progress-status").html('<span style="word-spacing: 0px;">File Upload in progress </span>');

  return true;
//	})
}

