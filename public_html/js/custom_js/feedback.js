$(function(){
	$('#feedbacksubmit').click(function(){
		var query = $('select[name=query]').val();
		var desc = $('textarea[name=describe]').val();
		var name = $('input[name=name]').val();
		//var reachtype = $('select[name=reachtype]').val();
		var reachtype = $('input[name=reachtype]').val();
		var contact = $('input[name=contact]').val();
		
		if(desc == '')
		{
			$('#desc_error').html('Please describe your query');
			$('#desc_error').show();
			return false;
		}
		else{
			$('#desc_error').hide();
		}
		
		if(name == '')
		{
			$('#name_error').html('Please enter your name');
			$('#name_error').show();
			return false;
		}
		else{
			$('#name_error').hide();
		}
		
		if(contact == '')
		{
			$('#contact_error').html('Please enter your contact details');
			$('#contact_error').show();
			return false;
		}
		else{
			$('#contact_error').hide();
		}
		
		if(reachtype == '1' && isEMailAddr(contact) == false)
		{
			$('#contact_error').html('Please enter a valid email id');
			$('#contact_error').show();
			return false;
		}
		else{
			$('#contact_error').hide();
		}
		
		if(reachtype == '2' && Validate_no(contact) == false)
		{
			$('#contact_error').html('Please enter a valid phone number');
			$('#contact_error').show();
			return false;
		}
		else{
			$('#contact_error').hide();
		}
		
		var dataString = 'query='+query+'&name='+name+'&desc='+desc+'&reachtype='+reachtype+'&contact='+contact;
		
		$("#load").ajaxStart(function(){$(this).show();})
		var service_url = $("#base_url").val();
		$("#feedbacksubmit").attr("disabled","disabled");
		$.ajax({
			type: "POST",
			url:service_url+"index.php/home/submit_feedback",
			data: dataString,  
			success: function(msg){
				$('#load').html(msg);
				$('#load').show();
			}
		});
	});
});

function isEMailAddr(elem){	 
	var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	if (!elem.match(re)) 
		return false;
	else 
	   return true;
}

function Validate_no(phone){ 
	count = phone.length;
	var valid_str="0123456789-+() ";
	
	for(var pos=0; pos<=count; pos++)
	{	
		if(valid_str.search(phone.charAt(pos)) == -1)
		return false;
	}
			
}