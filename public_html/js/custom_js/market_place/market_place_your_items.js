/* Market Place(mp) Your Items javascript File */
$(document).ready(function(){
/* Market Place Your Items*/
	$(".delete-item").click(function(){
		
		var e;
		var del_id = this.id;
		e = window.confirm("This will delete this item.!")
		
		if(e){
			$.ajax({
				url: "../market_place/delete_item",
				data: {"del_id":del_id},
				async: false,
	      dataType: "json",
				type: "POST", 
				success: function(msg){
					if(msg.status){
						$("#"+del_id).parents("tr").remove();
					}else{
						alert("Error Removin, Please try again");
					}
			  }
			});
			
			
			
			
		}
		
		return false;
	})

})
