/* Market Place(mp) javascript File */
$(document).ready(function(){
/* Market Place */
/* Intial Load */

/*
$('.itemImageReq').live('change',function(){
			var reader = new FileReader();
			var image  = new Image();
			var file = this.files[0];
			var id = this.id;
			reader.readAsDataURL(file);  
			reader.onload = function(_file) {
			image.src    = _file.target.result;              // url.createObjectURL(file);
			image.onload = function() {
			var w = this.width,
			h = this.height,
			t = file.type,                           // ext only: // file.type.split('/')[1],
			n = file.name,
			s = ~~(file.size/1024) +'KB';
			
			if( w < 530 || h < 530)
			{
				$("#error-"+id).html("Min resolution 530x530.");
				$('#'+id).val('');
				return false;
			}
			else
			{
				$("#error-"+id+"").html("");
			}
		};

			};		
});*/

		$("#bt_request_item").click(function(){
		var cat = $.trim($("#cmb_cat_req").val());
		var title = $.trim($("#txt_title_req").val());
		var desc = $.trim($("#txtar_description_req").val());
		var rd_price = $.trim($("input:radio[name=rd_price_req]:checked").val());
		if(!cat){
			$("#error-cmb_cat_req").html("Required");
			$("#cmb_cat_req").focus();
			return false;
		}else{
			$("#error-cmb_cat_req").html("");
		};
		if(title == "" || title == "Item Title" || IsScript(title)){
			$("#error-txt_title_req").html("Required");
			$("#txt_title_req").focus()
			return false;
		}else{
			$("#error-txt_title_req").html("");
		};
		if(desc == "" || desc == "Enter Item Description" || IsScript(desc)){
			$("#error-txtar_description_req").html("Required");
			$("#txtar_description_req").focus()
			return false;
		}else{
			$("#error-txtar_description_req").html("");
		};
		if(rd_price == 1){
			var txt_price = $.trim($("#txt_price").val());
			if(!txt_price){
				$("#error-rd_price_req").html("Price Required");
				$("#txt_price").focus()
				return false;
			}else if(isNaN(txt_price)){
				$("#error-rd_price_req").html("Should be a number");
				$("#txt_price").focus()
				return false;
			}else{
				$("#error-rd_price_req").html("");
			};
		}

		var error_flag = "";
		var error_flag_size = "";
		$(".itemImageReq").each(function(){
		
			if($(this).val()){
			//var file_size = this.files[0].size/1000;
				var ext = $(this).val().split('.').pop().toLowerCase();
				var allow = new Array('gif','png','jpg','jpeg');
				if($.inArray(ext, allow) == -1) {
				 	$("#error-"+this.id+"").html("Invalid File");
					error_flag =1;
				}else{
				
					/*if( file_size > 1024 )
					{
						$("#error-"+this.id).html("Maximum file upload: 1 MB");
						error_flag_size =1;
					}
					else
					{
						
					} */
					$("#error-"+this.id+"").html("");
				}
			}
		})
		if(error_flag){
			alert("Incorrect File Format");
			return false;
		}
		if(error_flag_size){
			//alert("Incorrect File Format");
			return false;
		}
		$("#txt_title_req").val(title);
		$("#txtar_description_req").val(desc);
		 market_add_request();
          //  return false;
/*
		var formData = new FormData($('#frm_add_request_item')[0]);

			$.ajax({
					beforeSend: function() {
								inactive_form_req();
								},
					complete: function() {
								active_form_req();
								start = 0;
								//loadFiles({'noclear': 0});
								},
					type	: 'POST',
					cache	: false,
					url		: $('#frm_add_request_item').attr('action'),
					data	: formData,
					success	: function(data2) {
					console.log(data2);
					if($.trim(data2) === 'yes')
						{
						$("#cmb_cat_req").focus();
						console.log("in success");
							$("input[type=text], textarea , select ").val("");
							$("input[type=file]").val("");
							$('#add_request .msg').addClass('success');
							$('#add_request .msg').append('<li/>').html('Your Request has been uploaded successfully!');
							$('#add_request .msg').fadeIn();
							
							end = false;
							start = 0;
							loadMarket({'noclear': 0});
						}
						else if($.trim(data2) == 'no')
						{
						$("#cmb_cat_req").focus();
							console.log("in un-success");
							$('#add_request .msg').removeClass('success');
							$('#add_request .msg').addClass('error');
							$('#add_request .msg').append('<li/>').html('Please retry, enter data');
							$('#add_request .msg').fadeIn();
							
						}


					},
					cache: false,
					contentType: false,
					processData: false
			});
*/

			return false;
	});

//Enable Currency and price field
	$("#rd_price").click(function(){
		$(".price_check").removeAttr("disabled");
	})
//Add more file control for Image Uploading
	$("#add-more-image-req").click(function(){
		var rnd_no = Math.floor(Math.random()*10000000000) ;
		var file_control = '<input type="file" class = "itemImageReq" id="'+rnd_no+'" name="file_image_req[]" size="73" />';
		$('#upload-image-req').append('<div class="image_file_contol">'+file_control+' <a href="#" class="remove_image_file_contol">Remove</a> <span class="error-msg" id="error-'+rnd_no+'"></span></div>');
		return false;
	})
	$(".remove_image_file_contol").live("click",function(){
		$(this).parent("div").remove();
		return false;
	})


	var min_slider_value = 233;
	var max_slider_value = 5405;

	/*$('#slider').slider({
		range: true,
		min: 0,
		max: 10000,
		values: [min_slider_value, max_slider_value],
		slide: function(event, ui) {
			$("#slider-result-max").html(ui.values[1]);
			$("#slider-result-min").html(ui.values[0]);
			$("#hd_max_price").val(ui.values[1]);
			$("#hd_min_price").val(ui.values[0]);
		}
	});*/
	$("#cmb_curreny_req").change(function(){
		var currency = this.options[this.selectedIndex].text;
		$("#slider-currency").html(currency);
	})

	//Edit markset place
  fn_remove_photos();

	//Edit Loaders
	var curr_currency = $("select#cmb_curreny_req :selected").text();
	$("#slider-currency").html(curr_currency);
})
//Initial Loader
function market_add_request(){
console.log('i am here');
    var options = { 
                url     : $('#frm_add_request_item').attr('action'),
                type    : $("#frm_add_request_item").attr('method'),
                dataType: 'json',
                  beforeSubmit: function() { inactive_form_req(); },
                   success:function( data ) {
                   // var msg = data.msg;
                    if(data.status == 'yes'){
							end = false;
							start = 0;
							$('#txtSearch').val('');
							loadMarket({'noclear': 0});
							$("#cmb_cat_req").focus();
							active_form_req();
							$("input[type=text], textarea , select ").val("");
							$("input[type=file]").val("");
							//$('#slider').slider({ values: [ 233, 5405 ] });
							$("#slider-result-max").html('1000');
							$("#slider-result-min").html('0');
							$("#hd_max_price").val('1000');
							$("#hd_min_price").val('0');
							$('#add_request .msg').addClass('success');
							$('#add_request .msg').append('<li/>').html('Your Request has been uploaded successfully!');
							$('#add_request .msg').fadeIn();
							
                    }else{
                        $("#cmb_cat_req").focus();
						active_form_req();
							console.log("in un-success");
							$('#add_request .msg').removeClass('success');
							$('#add_request .msg').addClass('error');
							$('#add_request .msg').append('<li/>').html('Please retry, enter data');
							$('#add_request .msg').fadeIn();
							
                    }
                },
            }; 
			
            $('#frm_add_request_item').ajaxSubmit(options);
                return false;
        }
		
function active_form_req()
{
$.fancybox.hideLoading();
$('#bt_request_item').removeClass('inactive');
$('#bt_request_item').removeAttr('disabled');

}

function inactive_form_req()
{
$.fancybox.showLoading();
$('#bt_request_item').addClass('inactive');
$('#bt_request_item').attr('disabled','disabled');
}
//Remove Photos
function fn_remove_photos(){
  $(".remove-photo").click(function(){
    var service_url = $("#base_url").val();
    var e = this;
    //remove_photo
    var img_id = this.id;
    $.ajax({
      url: service_url+"index.php/market_place/remove_photo",
      data: {"img_id":img_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status >=1){
          $(e).parents('.edit-photos').hide();
        }else{
          alert("Please try again");
        };
      }
    });
    return false;
  })
}