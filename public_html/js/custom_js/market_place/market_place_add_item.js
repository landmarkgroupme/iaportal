/* Market Place(mp) javascript File */
$(document).ready(function(){
/* Market Place */
/* Intial Load */
	//initialize();
	$("#add_more_image").live("click",function(){
		var rnd_no = Math.floor(Math.random()*10000000000) ;
		var file_control = '<input type="file" class="itemImage" id="'+rnd_no+'" name="file_image[]" size="73" />';
		$('#upload-image').append('<div class="image_file_contol">'+ file_control + '<a href="#" class = "remove_image_file_contol" > Remove </a> <span class="error-msg" id="error-'+rnd_no+'"></span></div>');
		return false;
	});
		$("#bt_add_item").click(function(){
		//$.fancybox.resize;
	
		var cat = $.trim($("#cmb_cat").val());
		var title = $.trim($("#txt_title").val());
		
		var desc = $.trim($("#txtar_description").val());
		var rd_price = $.trim($("input:radio[name=rd_price]:checked").val());

		/* $("input:radio[name=rd_price]:checked").val() */
		if(!cat){
			$("#error-cmb-cat").html("Required");
			$("#cmb_cat").focus()
			return false;
		}else{
			$("#error-cmb_cat").html("");
		};
		if(title == "" || title == "Item Title" || IsScript(title)){
			$("#error-txt_title").html("Required");
			$("#txt_title").focus()
			return false;
		}else{
			$("#error-txt_title").html("");
		};
		if(desc == "" || desc == "Enter Item Description"  || IsScript(desc)){
			$("#error-txtar_description").html("Required");
			$("#txtar_description").focus()
			return false;
		}else{
			$("#error-txtar_description").html("");
		};
		if(rd_price == 1){
			var txt_price = $.trim($("#txt_price").val());
			if(!txt_price){
				$("#error-rd_price").html("Price Required");
				$("#txt_price").focus()
				return false;
			}else if(isNaN(txt_price)){
				$("#error-rd_price").html("Should be a number");
				$("#txt_price").focus()
				return false;
			}else{
				$("#error-rd_price").html("");
			};
		}

		var error_flag = "";
		var error_flag_size = "";
		$(".itemImage").each(function(){
				
			if($(this).val()){
			//readImage(this.files[0], this.id);

			////
			//var file_size = this.files[0].size/1000;
				
				var ext = $(this).val().split('.').pop().toLowerCase();
				var allow = new Array('gif','png','jpg','jpeg');
				if($.inArray(ext, allow) == -1) {
				 	$("#error-"+this.id+"").html("Invalid File");
					error_flag =1;
				}else{
					
						$("#error-"+this.id+"").html("");
					
				}
			}
			
		});
		if(error_flag){
			alert("Incorrect File Format");
			return false;
		}
		if(error_flag_size){
			//alert("Incorrect File Format");
			return false;
		}
		$("#txt_title").val(title);
		$("#txtar_description").val(desc);
		add_market_add_item();
		return false;
		

			
	});
//Disable Currency and price field
	$("#rd_price_free").click(function(){
		$(".price_check").attr("disabled","disabled");
		$('#rd_price_free').attr('checked');
	});
//Enable Currency and price field
	$("#rd_price").click(function(){
		$(".price_check").removeAttr("disabled");
	});
//Add more file control for Image Uploading
	
	
	$(".remove_image_file_contol").live("click",function(){
		$(this).parent("div").remove();
		return false;
	});
    
	var price_free = $('#rd_price_free').attr('checked');
    if(price_free){
      $(".price_check").attr("disabled","disabled");
    };
	  //Edit markset place
	  fn_remove_photos();
});
//Initial Loader
function initialize(){
//Form Validation
/*
$('.itemImage').live('change',function(){
			var reader = new FileReader();
			var image  = new Image();
			var file = this.files[0];
			var id = this.id;
			reader.readAsDataURL(file);  
			reader.onload = function(_file) {
			image.src    = _file.target.result;              // url.createObjectURL(file);
			image.onload = function() {
			var w = this.width,
			h = this.height,
			t = file.type,                           // ext only: // file.type.split('/')[1],
			n = file.name,
			s = ~~(file.size/1024) +'KB';
			
			if( w < 530 || h < 530)
			{
				$("#error-"+id).html("Min resolution 530x530.");
				$('#'+id).val('');
				return false;
			}
			else
			{
				$("#error-"+id+"").html("");
			}
		};

			};		
}); */

}

function readImage(file, id) {

    var reader = new FileReader();
    var image  = new Image();

    reader.readAsDataURL(file);  
    reader.onload = function(_file) {
        image.src    = _file.target.result;              // url.createObjectURL(file);
        image.onload = function() {
            var w = this.width,
                h = this.height,
                t = file.type,                           // ext only: // file.type.split('/')[1],
                n = file.name,
                s = ~~(file.size/1024) +'KB';
           console.log(w+'x'+h+' '+s+' '+t+' '+n+'<br>');
				if( w < 220 || h < 220)
					{
						$("#error-"+id).html("Minimum resolution 220x220.");
						return false;
					}
					else
					{
						$("#error-"+id+"").html("");
					}
        };
		
					
       /* image.onerror= function() {
            alert('Invalid file type: '+ file.type);
        };*/
    };

}
function add_market_add_item(){

    var options1 = { 
                url     : $('#frm_add_item').attr('action'),
                type    : $("#frm_add_item").attr('method'),
                dataType: 'json',
				  beforeSubmit: function() { inactive_form(); },
                  success: function( data ) {
				  active_form();
                    //var msg = data.msg;
					 
                    if(data.status == 'yes'){
					$("#cmb_cat").focus();
					
							$("input[type=text], textarea , select ").val("");
							$("input[type=file]").val("");
							$('#add_item .msg').addClass('success');
							$('#add_item .msg').append('<li/>').html('Your Item has been uploaded successfully!');
							$('#add_item .msg').fadeIn();
							end = false;
							start = 0;
							loadMarket({'noclear': 0});
							
							
                    }else{
					
                        $("#cmb_cat").focus();
							console.log("in un-success");
							$('#add_item .msg').removeClass('success');
							$('#add_item .msg').addClass('error');
							$('#add_item .msg').append('<li/>').html('Please retry, enter data');
							$('#add_item .msg').fadeIn();
							
                    }
                },
            }; 
			
            $('#frm_add_item').ajaxSubmit(options1);
                return false;
        }
		
function active_form()
{
$.fancybox.hideLoading();
$('#bt_add_item').removeClass('inactive');
$('#bt_add_item').removeAttr('disabled');
}

function inactive_form()
{
$.fancybox.showLoading();
$('#bt_add_item').addClass('inactive');
$('#bt_add_item').attr('disabled','disabled');
}

//Remove Photos
function fn_remove_photos(){
  $(".remove-photo").click(function(){
    var service_url = $("#base_url").val();
    var e = this;
    //remove_photo
    var img_id = this.id;
    $.ajax({
      url: service_url+"index.php/market_place/remove_photo",
      data: {"img_id":img_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status >=1){
          $(e).parents('.edit-photos').hide();
        }else{
          alert("Please try again");
        };
      }
    });
    return false;
  })
}
