/* Market Place(mp) javascript File */
$(document).ready(function(){
/* Market Place */
/* Intial Load */
	initialize();
//Market Term search
  market_term_search_form();
	
/* Category Sort */
	$(".category_sort_no").live("click",function(){
		var service_url = $(this).attr("href");
		var item_type = $("#item_type").val();
		$.ajax({
			url: service_url,
			data: {"sort_order":"total_count","item_type":item_type},
			async: false,
      dataType: "html",
			type: "POST", 
			success: function(category_list){
				$("#sidebar").html(category_list);
		  }
		});
		return false;
	});
	$(".category_sort_name").live("click",function(){
		var service_url = $(this).attr("href");
		var item_type = $("#item_type").val();
		$.ajax({
			url: service_url,
			data: {"sort_order":"category","item_type":item_type},
			async: false,
      dataType: "html",
			type: "POST", 
			success: function(category_list){
				$("#sidebar").html(category_list);
		  }
		});
		return false;
	})
})
//Initial Loader
function initialize(){
//Advance search Check All the Concepts	
	$('.concept_check_all').click(function(){
		$('.checkbox_concepts').attr("checked","checked");
		return false;
	})
//Uncheck All the Concepts	
	$('.concept_check_none').click(function(){
		$('.checkbox_concepts').removeAttr("checked");
		return false;
	})
//Check All the Locations	
	$('.location_check_all').click(function(){
		$('.checkbox_locations').attr("checked","checked");
		return false;
	})
//Uncheck All the Locations	
	$('.location_check_none').click(function(){
		$('.checkbox_locations').removeAttr("checked");
		return false;
	})
}

//Search for Market Items
function market_term_search_form(){
  //Focus on textarea changes backgroud 
  $("#market_term").focus(function(){
    var text = $("#market_term").val();
    if(text == "Search for items"){
      $("#market_term").val("");
    }
  })
  $("#market_term").blur(function(){
    var text = $.trim($("#market_term").val());
    if(text == ""){
      $("#market_term").val("Search for items");
    }
  })
}
