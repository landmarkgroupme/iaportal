$(document).ready(function () {
var limit = 6;
var start = 0;
var type = 0;
var end = false;
var nearToBottom = 250;

    $('.marketDeleteBt').on('click', function (e) {
        e.preventDefault();
        item_id = $(this).attr('id');
        var data = {"item_id":item_id};
        var service_url = siteurl+"market/inactiveMarketItem";
        $.ajax({
            url: service_url,
            data: data,
            async: false,
            cache: false,
            dataType: "json",
            type: "GET",
            success: function(data){
                $('#'+item_id).slideUp('fast');
            }
        });
    });

    $('#cancel').on( "click", function() {
        $('.msg').hide();
        $.fancybox.close();

    });
    $('#cancel_req').on( "click", function() {
        $('.msg').hide();
        $.fancybox.close();

    });

    $('.addItemFan').fancybox({
        maxHeight: 500,
        beforeLoad : function() {
            var item_id = $(this)[0].element.context.attributes[1]['nodeValue'];
            if(!isNaN(item_id))
            {

                loadEditMarket(item_id,'add');
            }
        },
        afterLoad	: function(){
            $('#txtar_description').autosize();
            $('#txtar_description').trigger('autosize.resize');
        },
        afterClose  : function() {
            $("#frm_add_item")[0].reset();
            $('#add_item .msg').removeClass('success');
            $('#add_item .msg').removeClass('error');
            $('#add_item .msg').html('');
            $('#add_item .msg').hide();
            $('#add_item .error-msg').html('');
            $('#txtar_description').trigger('autosize.destroy');
            $('#add-item-heading').html('Add an Item');
            $('#bt_add_item').val('Post the Item');
            $('#edit_market').val('');
            $("#upload-image").find('img').remove();
        }
    });

    $('.addRequestFan').fancybox({
        maxHeight: 500,
        beforeLoad : function() {
            var item_id = $(this)[0].element.context.attributes[1]['nodeValue'];
            if(!isNaN(item_id)) {
                loadEditMarket(item_id,'request');

            }
        },
        afterLoad	: function(){

            //$('#slider').slider( "option", "max", 10000 );
            $('#txtar_description_req').autosize();
            $('#txtar_description_req').trigger('autosize.resize');

        },
        afterClose  : function() {
            $('#slider').slider({ values: [ 233, 5405 ] });
            $("#slider-result-max").html('1000');
            $("#slider-result-min").html('0');
            $("#hd_max_price").val('1000');
            $("#hd_min_price").val('0');
            $("#frm_add_request_item")[0].reset();
            $('#add_request .msg').removeClass('success');
            $('#add_request .msg').removeClass('error');
            $('#add_request .msg').html('');
            $('#add_request .msg').hide();
            $('#add_request .error-msg').html('');
            $('#txtar_description_req').trigger('autosize.destroy');

            $('#request-item-heading').html('Request An Item');
            $('#bt_request_item').val('Request the Item');
            $('#edit_market_req').val('');
            $("#upload-image-req").find('img').remove();
        }
    });

    $('#go').on( "click", function() {
        $('ul.filter-links li').removeClass('active');
        $('#0').addClass('active');
        start = 0;
        end = false;
        loadMarket({'noclear': 0, 'searched': 1});
//e.preventDefault();
        $('#ddCategories, #ddPrice').val('0');

        return false;
    });
    $('#goMobile').on( "click", function() {
		var searched = $('#txtSearchMobile').val();
		if(searched == '')
		{
			$("#txtSearchMobile").attr("placeholder", "Please enter keywords").placeholder();
			return false;
		}
        $('ul.filter-links li').removeClass('active');
        $('#0').addClass('active');
        start = 0;
        end = false;
        loadMarket({'noclear': 0, 'searched': 1});
//e.preventDefault();
        $('#ddCategoriesMobile, #ddPriceMobile').val('0');

        return false;
    });

    $('#clear').on( "click", function() {
        start = 0;
        end = false;
        $('#ddCategories').val('0');
        //$('#ddConcepts').val('0');
        $('#ddPrice').val('0');
        loadMarket({'noclear': 0});

    });
    $('#clearMobile').on( "click", function() {
        start = 0;
        end = false;
        $('#ddCategoriesMobile').val('0');
        //$('#ddConceptsMobile').val('0');
        $('#ddPriceMobile').val('0');
        loadMarket({'noclear': 0});

    });
    $('.snap-content').scroll(function() {
        if ($('.snap-content').scrollTop() + $('.snap-content').height() >=
            $('.left-contents').height() ) {
            start += 6;
            loadMarket({'noclear': 1});
        }
    });


    //open selected tab on window back button
    if ( window.location.hash ) {
        var hash = window.location.hash.substring(1);
        $("."+hash).click(); //clicks on element specified by hash
    }

function loadEditMarket(item_id,type) {

    var data = {"item_id":item_id};
    var service_url = siteurl+"market/load_market_detail";
    $.ajax({
        url: service_url,
        data: data,
        async: false,
        cache: false,
        dataType: "json",
        type: "GET",
        success: function(data){
            if(data.status == true) {
                if(type == 'request') {
                    $('#request-item-heading').html('asdjbsaj');
                    $('#request-item-heading').html('Edit Requested Item');
                    $('#bt_request_item').val('Edit Requested Item');
                    $('#edit_market_req').val(item_id);
                    $('#cmb_cat_req').val(data.records.category_id);
                    $('#txt_title_req').val(data.records.title);
                    $('#txtar_description_req').val(data.records.description);
                    $('#cmb_curreny_req').val(data.records.currency_id);
                    $("#slider-result-min").html(data.records.item_min_price);
                    $("#slider-result-max").html(data.records.item_max_price);
                    $("#hd_min_price").val(data.records.item_min_price);
                    $("#hd_max_price").val(data.records.item_max_price);
                    $('#slider').slider({ values: [ data.records.item_min_price, data.records.item_max_price ] });
                    if(data.records.original_image){
                        var image_url = siteurl+"images/marketplace/"+data.records.original_image;
                        $("#upload-image-req").append('<img src="'+image_url+'" alt="'+data.records.title+'" height="100" width="100" style="margin-left: -238px;margin-bottom: 25px;">');
                    }
                    console.log(data);
                }
                else if(type == 'add')
                {
                    $('#add-item-heading').html('Edit an Item');
                    $('#bt_add_item').val('Edit an Item');
                    $('#edit_market').val(item_id);
                    $('#cmb_cat').val(data.records.category_id);
                    $('#txt_title').val(data.records.title);
                    $('#txtar_description').val(data.records.description);
                    $('#cmb_curreny').val(data.records.currency_id);
                    if(data.records.item_type == 3) {
                        $('#rd_price').removeAttr('checked');
                        $('#rd_price_free').attr('checked','checked');
                    } else {
                        $('#rd_price_free').removeAttr('checked');
                        $('#rd_price').attr('checked','checked');
                        if(data.records.item_max_price != 0) {
                            $('#txt_price').val(data.records.item_max_price);
                        }
                    }
                    if(data.records.original_image){
                        var image_url = siteurl+"images/marketplace/"+data.records.original_image;
                        $("#upload-image").append('<img src="'+image_url+'" alt="'+data.records.title+'" height="100" width="100" style="margin-left: -238px;margin-bottom: 25px;">');
                    }
                }
            }
        }
    });
}
function loadMarket(prop)
{
    if($('.media-show').is(':visible'))
    {
        var searched = ($('#txtSearchMobile').val() != '' && $('#txtSearchMobile').val() != 'Search for item') ? true : false;
        if(searched)
        {
            var search = remove_tags($('#txtSearchMobile').val());
            var data = {'limit' : limit, 'start' : start, 'search_term': search};
        } else {
            //var concept = $('#ddConceptsMobile').val();
            var category = $('#ddCategoriesMobile').val();
            var price = $('#ddPriceMobile').val();
            var data = {'limit' : limit, 'start' : start, 'category': category, 'price': price, 'type': type};
        }
    } else {
        var searched = ($('#txtSearch').val() != '' && $('#txtSearch').val() != 'Search for item') ? true : false;
        if(searched)
        {
            var search = remove_tags($('#txtSearch').val());
            var data = {'limit' : limit, 'start' : start, 'search_term': search};
        } else {
            //var concept = $('#ddConcepts').val();
            var category = $('#ddCategories').val();
            var price = $('#ddPrice').val();
            var data = {'limit' : limit, 'start' : start, 'category': category, 'price': price, 'type': type};
        }

    }

    $('div.market-grid-view').show();
    if(!prop.noclear)
    {

        $('div.market-grid').html('');

        head ='<div class="list-view-head d-n">';
        head +='<span class="col-img">Item Image</span>';
        head += '<span class="col-details">Item Details</span>';
        head += '<span class="col-category">Category</span>';
        head += '<span class="col-updated-on">Added</span>';
        head += '<span class="col-price">Item Price</span>';
        head += '</div>'
        $('div.market-grid').append(head);

        $('div.no-result-block').html('');
        $('div.no-result-block').hide();
    }

    var service_url = siteurl+"market/market_ajax";




    if(end == false)
    {
        $('.loader-more').show();
        $.ajax({
            url: service_url,
            data: data,
            async: false,
            cache: false,
            dataType: "json",
            type: "GET",
            success: function(data){
                //var counter = 1;
                $(".all_items > a > span").text('('+ data.item_count +')');
                $(".sale_items > a > span").text('('+ data.sale_count +')');
                $(".wanted_items > a > span").text('('+ data.wanted_count +')');
                $(".freebies > a > span").text('('+ data.freebies +')');
                $(".your_items > a > span").text('('+ data.user_count +')');
                /*$.each(data, function() {
                 //console.log(this);

                 var midbox = (counter == 2 || counter == 5) ? 'mid' : '';
                 var html = '<div class="market-box '+ midbox +'">';
                 html +='<div class="xtra4-list-view"><span><a href="#Home & Garden">Category</a>';
                 html += '</span><span>2 days ago</span><span><mark>1600</mark> AED</span></div>';
                 if(this.item_max_price != null && this.currency != null)
                 {
                 html +='<span class="m-price">'+this.currency+' '+ this.item_max_price +'</span>';
                 }
                 html +='
                 <a href="market/items_details/'+this.id+'">
                 <img class="p-img-big" src="'+ this.item_image+'" alt="Sony Vaio" />
                 </a>
                 <br/>
                 ';
                 html += '
                 <div class="x-members">'+ this.title +'</div>
                 ';
                 html +='
                 <div class="p-followers-bx clearfix">
                 <div class="p-followers">';
                 html +='
                 <div class="imgMembers">
                 <a href="'+this.reply_username+'">
                 <img src="/images/user-images/25x25/' + this.profile_pic + '" alt="p1">
                 <b>'+this.display_name.split(' ')[0]+'</b>
                 </a>
                 ';
                 html +='
                 <span class="status online">&nbsp;</span>
                 ';
                 html +='</div>
                 </div>
                 <div class="cp-btn-follow"></div>
                 </div>
                 </div>
                 ';
                 $('div.market-grid').append(html);
                 counter++;
                 });*/
                if(jQuery.isEmptyObject(data.recently_added_item) && searched)
                {

                    $('div.market-grid-view').hide();
                    $('div.no-result-block').show();
                    $('.list-view-head').hide();
                    var noResult = 'No result found for <b>"'+search+'"</b>.';
                    $('div.no-result-block').append(noResult);
                    $('.loader-more').hide();
                    end = true;
                }
                else if(jQuery.isEmptyObject(data.recently_added_item) && start == 0)
                {
                    //console.log("empty");
                    $('div.no-result-block').show();
                    $('.list-view-head').hide();
                    var noResult = 'No feed found';
                    $('div.no-result-block').append(noResult);
                    $('.loader-more').hide();
                    end = true;
                }
                else
                {

                    //$('div.market-grid').html('');
                    //$('.list-view-head').show();
                    _.templateSettings.variable = "rc";
                    var template = _.template(
                        $('#marketplace').html()
                    );

                    $('div.market-grid').append(template(data.recently_added_item));
                    if(data.recently_added_item.length < 6)
                    {
                        $('.loader-more').hide();
                        end = true;
                    }

                }


            }
        });
    }
}
function remove_tags(html)
{
    if(html) {
        return html.replace(/<(?:.|\n)*?>/gm, '');
    }

}
function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

$(window).load(function(){
    var tourSubmitFunc = function(e,v,m,f){
            if(v === -1){
                $.prompt.prevState();
                return false;
            }
            else if(v === 1){
                $.prompt.nextState();
                return false;
            }
            else if(v === 0){
                $.prompt.goToState(0);
                return false;
            }
        },
        tourStates = [
            {
                title: 'Item',
                html: 'Add or request an item',
                buttons: { Next: 1 },
                focus: 0,
                position: { container: '#add_an_item', x: -100, y: 20, width: 300, arrow: 'tc' },
                submit: tourSubmitFunc
            },
            {
                title: 'Communicate seamlessly',
                html: 'Chat directly with co-worker, if you like what you see',
                buttons: { Prev: -1, First: 0, Done: 2 },
                focus: 0,
                position: { container: '.imgMembers', x: 0, y: 40, width: 300, arrow: 'tl' },
                submit: tourSubmitFunc
            }
        ];
});
function loadMarketUpdates() {
    $('#recent-posts div.loader-more').show();
    var service_url = siteurl+"market/items_load_ajax_data";
    var request_data = {};
    console.log(service_url);
    request_data = {'item_id': item_id};
    //filter by concept
    if (typeof feedtype != 'undefined')
	{
        request_data.feedtype = feedtype;
        request_data.feedtype_id = feedtype_id;
        }


    /*console.log(request_data);
    return;*/

    $.ajax({
        url: service_url,
        data: request_data,
        async: false,
        dataType: "json",
        type: "POST",
        cache: false,
        success: function(response){
        if(response.feed.length == 0 &&  start == 0)
        {
        $('#recent-posts div.loader-more').before('<dl class="block-2 clearfix no-feed-found"><dt><dd><h2>No feed found!</h2></dd></dt></dl>');
        }
    console.log(response);
    _.templateSettings.variable = "rc";

    // Grab the HTML out of our template tag and pre-compile it.
    var template = _.template(
    $( "script.template" ).html()
    );
    //$('#recent-posts ul.filter li').removeClass('active');
    //$(parent).addClass('active');
    //$('#recent-posts ul.filter').append(template(response));
    $('#recent-posts').append(template(response));

    },
    beforeSend: function(){
        loadProgress = true;
        },
    complete: function(response){
        loadProgress = false;
        $('#recent-posts div.loader-more').hide();
        }


    });

    }
});