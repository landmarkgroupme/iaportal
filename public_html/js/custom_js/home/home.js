/* Home javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* Home  */
		
	//Status Message functions
	fn_status_message_function();	

  initialise_status_update();

	/*Hide Anouncement bar */
	$('.close-announcement').click(function(){
	  var service_url = $("#base_url").val()+"index.php/home/hide_announcement_bar";
	  var ann_id = this.id;
     $.ajax({
      url: service_url,
      data: {"ann_id":ann_id},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        $('.message').hide();
      }
    });
	  return false;
	})
	
/* Share Button Click */
	$("#btn-ar-share").click(function(){
		var txtarShare = $.trim($("#txtarShare").val());
		var activeTab = ($(".active").attr("id"))
		if((txtarShare == "What are you working on ?") || (txtarShare.length < 2)){
			$("#txtarShare").focus();
			return false;
		}
		var tab = $(".filters ul .active a").text();
		
		 var service_url = $("#base_url").val()+"index.php/status_update/ajax_submit_status_message";
		 $.ajax({
      url: service_url,
      data: {"txtarShare":txtarShare},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        var update = msg.update *1;
        if(update){
          //Upadate if Tabs are All or Status update 
          if(tab != "Status Updates" && tab != "All"){
            $('#status-msg-status').html("New status message posted");
            $('#status-msg-status').show();
          }else{
            $("#show-list").prepend(msg.data);
          }
          $("#txtarShare").val('What are you working on ?');
          $("#txtarShare").animate({ height: "2em" }, 200,function(){
            $('#btn-ar-share').hide();
              $('#status-msg-count').hide();
          });
        }
      }
    });
		return false;
	})
	
	
	//Load More ACtion
	$(".more-stories").click(function(){
	  var img_path = $("#base_url").val()+"images/indicator.gif";
    $(this).html("<img src='"+img_path+"' alt='loader' />");
    var active_tab = $(".active").attr("id");
    
		fn_load_more(this);
		$('a[rel*=pop_win]').facebox(); //Facebox Loader
		fn_report_staus_update();
		return false;
	})
	
	
	//Home page list ad Grid view swapper
	fn_switch_views()
	
})
//Initial Loader

//Load More
function fn_load_more(e){
  pagesize = pagesize + (e.id*1);
  var service_url = $(e).attr("href");
  var request_type = $(e).attr("rel"); //MT or CI
  $.ajax({
    url: service_url,
    data: {"pagesize":pagesize,"request_type":request_type},
    async: false,
    dataType: "json",
    type: "POST", 
    success: function(msg){
      $(".more-stories").text("View More");
      if(msg.data){
        $("#show-list").append(msg.data);
        if(msg.load_more){
          $(".more-stories").text("View More");         
        }else{
          $(".more-stories").hide();  
        }
      }else{
        $(".more-stories").hide();
      }
    }
  });
}
function fn_status_message_function(){
  //Reply Message
  $(".msg-reply").live("click",function(){
    var user = this.id;
    $("#txtarShare").val("@"+user+":");
    $("#txtarShare").focus();
    return false;
  })
  //Delete Message
  $(".msg-delete").live("click",function(){
    var service_url = $("#base_url").val()+"index.php/"; 
    var message_id = this.id;
    var active_tab = $(".active").attr("id");
    var current_msg = this;
    $.ajax({
      url: service_url+"status_update/delete_status_message",
      data: {"message_id":message_id},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        if(msg.affected_row >= 1){
          pagesize =0;
          window.location = service_url+"home/status_updates";
        }
      }
    })
    return false;
  })
  //Unfollow User 
  $(".msg-unfollow").live("click",function(){
    var service_url = $("#base_url").val()+"index.php/";
    var user = $(this).attr("rel");
    var active_tab = $(".active").attr("id");
    $.ajax({
      url: service_url+"status_update/unfollow_user",
      data: {"unfollow_user":user},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        if(msg.affected_row >= 1){
          $("[rel="+user+"]").text("Follow");
          $("[rel="+user+"]").removeClass("msg-unfollow");
          $("[rel="+user+"]").addClass("msg-follow");
          if(active_tab == "su_show_following_people"){
            setTimeout(function(){ window.location = "following_people";}, 2000 );
          }
        }
      }
    });
    return false;
  })
//Follow User
  $(".msg-follow").live("click",function(){
    var service_url = $("#base_url").val()+"index.php/";
    var user = $(this).attr("rel");
    var active_tab = $(".active").attr("id");
    $.ajax({
      url: service_url+"status_update/follow_user",
      data: {"follow_user":user},
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        if(msg.affected_row >= 1){
          $("[rel="+user+"]").text("Unfollow");
          $("[rel="+user+"]").addClass("msg-unfollow");
          $("[rel="+user+"]").removeClass("msg-follow");
        }
      }
    })
    return false;
  })
}

function fn_switch_views(){
  var service_url = $("#base_url").val();
  $("#view-list").click(function(){
    bg_img_act = service_url+"images/ico-2.gif";
    bg_img_inact = service_url+"images/ico-1.gif";
    
    $("a#view-list").css("background","url("+bg_img_act+") no-repeat");
    $("a#view-details").css("background","url("+bg_img_inact+") no-repeat");
    
    $(".text p").hide();
    return false;
  })
  $("#view-details").click(function(){
    
    bg_img_act = service_url+"images/ico-55.jpg";
    bg_img_inact = service_url+"images/ico-54.jpg";
    
    $("a#view-list").css("background","url("+bg_img_act+") no-repeat");
    $("a#view-details").css("background","url("+bg_img_inact+") no-repeat");
    
    $(".text p").show();
    return false;
  })
}

function initialise_status_update(){
  //Focus on textarea changes backgroud
  $("#txtarShare").focus(function(){
    var base_url = $("#base_url").val()
    var text = $("#txtarShare").val();
    if(text == "What are you working on ?"){
      $("#txtarShare").val("");
    }
    $(this).animate({ height: "5.5em" }, 200,function(){
      $('#btn-ar-share').show();
      $('#status-msg-count').show();
    });
   // $(this).css("background","url('"+base_url+"images/bg-share-form-textarea-focus.jpg') no-repeat scroll 0 0 transparent");
  })

  $("#txtarShare").blur(function(){
    var base_url = $("#base_url").val()
    var text = $.trim($("#txtarShare").val());
    if(text == ""){
      $("#txtarShare").val("What are you working on ?");
      $(this).animate({ height: "2em" }, 200,function(){
            $('#btn-ar-share').hide();
        $('#status-msg-count').hide();
      });
    }
    //$(this).css("background","url('"+base_url+"images/bg-share-form-textarea.jpg') no-repeat scroll 0 0 transparent");
  })

  limitChars("txtarShare",160,"status-msg-count");// Checking when page is reloaded
  $("#txtarShare").keyup(function(){
    limitChars("txtarShare",160,"status-msg-count");
  })
}
function limitChars(textid, limit, infodiv) {
  var text = $('#'+textid).val();
  text = (text == "What are you working on ?")? "":text;
  var textlength = text.length;
  if(textlength > (limit-1)){
    $('#' + infodiv).html("0");
    $('#'+textid).val(text.substr(0,limit));
    return false;
  }else{
    $('#' + infodiv).html((limit - textlength));
    return true;
  }
}