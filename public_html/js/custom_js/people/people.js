/* People General javascript File */
$(document).ready(function(){
/* People */
/* Intial Load */
	initialize();
	search_box_filter();
})
//Initial Loader
function initialize(){
	//Focus on textarea changes backgroud	
	$("#term").focus(function(){
		var text = $("#term").val();
		if(text == "Search by name, job or role.."){
			$("#term").val("");
		}
	})
	$("#term").blur(function(){
		var text = $.trim($("#term").val());
		if(text == ""){
			$("#term").val("Search by name, job or role..");
		}
	})
}


function search_box_filter(){
//Advance search Check All the Concepts 
  $('.concept_check_all').click(function(){
    $('.checkbox_concepts').attr("checked","checked");
    return false;
  })
//Uncheck All the Concepts  
  $('.concept_check_none').click(function(){
    $('.checkbox_concepts').removeAttr("checked");
    return false;
  })
//Check All the Locations 
  $('.location_check_all').click(function(){
    $('.checkbox_locations').attr("checked","checked");
    return false;
  })
//Uncheck All the Locations 
  $('.location_check_none').click(function(){
    $('.checkbox_locations').removeAttr("checked");
    return false;
  })
}