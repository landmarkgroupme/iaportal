/* View Photos javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* About, Ablum->view_photo */
	//$("a[rel='view_photos']").colorbox();
	$('.view_photos').fancybox({
		maxHeight: 500,
		cyclic: true,
		 prevEffect      : 'none',
        nextEffect      : 'none',
		 helpers : {
                        title : { type : 'inside' },
                   },
		 afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? '        - ' + this.title : '');
            }
		
	});
})

