/* Announcement javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* Announcement */

		
//Load More ACtion
	$(".more-stories").click(function(){
	  
    var img_path = $("#base_url").val()+"images/indicator.gif";
    $(this).html("<img src='"+img_path+"' alt='loader' />");
		pagesize = pagesize + (this.id*1);
		$.ajax({
			url: "announcement/pagination",
			data: {"pagesize":pagesize},
			async: false,
	    dataType: "json",
			type: "POST", 
			success: function(msg){
			  //$(".more-stories").text("Load More Announcements");
				if(msg.load_more){
          $(".more-stories").text("Load More Announcements");         
        }else{
          $(".more-stories").hide();  
        }
        
        if(msg.data){
          $("#show-announcement").append(msg.data);
        }else{
          $(".more-stories-search").hide();
        }
	  	}
		});
		return false;
	})
})

