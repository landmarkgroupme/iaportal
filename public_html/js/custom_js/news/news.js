/* News javascript File */
var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
/* News */

		
//Load More ACtion
	$(".more-stories").click(function(){
	  var img_path = $("#base_url").val()+"images/indicator.gif";
    $(this).html("<img src='"+img_path+"' alt='loading..' />");
    
		pagesize = pagesize + (this.id*1);
		$.ajax({
			url: "news/pagination",
			data: {"pagesize":pagesize},
			async: false,
	    dataType: "json",
			type: "POST", 
			success: function(msg){
			  //$(".more-stories").text("Load More News");
			  if(msg.load_more){
          $(".more-stories").text("Load More News");         
        }else{
          $(".more-stories").hide();  
        }
			  
				if(msg.data){
          $("#show-news").append(msg.data);
        }else{
          $(".more-stories-search").hide();
        }
	  	}
		});
		return false;
	})
	
	$(".more-stories-search").click(function(){
	  var img_path = $("#base_url").val()+"images/indicator.gif";
    $(this).html("<img src='"+img_path+"' alt='loading..' />");
    
		pagesize = pagesize + (this.id*1);
		var service_url = $(this).attr("href");
		$.ajax({
			url: service_url,
			data: {"pagesize":pagesize},
			async: false,
	    dataType: "json",
			type: "POST", 
			success: function(msg){
			  //$(".more-stories").text("Load More News");
				
				if(msg.load_more){
          $(".more-stories-search").text("Load More News");         
        }else{
          $(".more-stories-search").hide();  
        }
				
				if(msg.data){
					$("#show-news").append(msg.data);
				}else{
					$(".more-stories-search").hide();
				}
				
				
	  	}
		});
		return false;
	})
})

