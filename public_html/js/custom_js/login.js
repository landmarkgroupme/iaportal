/* Login javascript File */

var pagesize =0; // Global Defined Page Limit
$(document).ready(function(){
  /* Login */
  initialize();
  $("#txtusername").val("Email");
  $("#txtpassword").val("Password");
  $('#fgt-submit-bt').attr('disabled',false);
  $('#trouble-submit-bt').attr('disabled',false);
})
//Initial Loader
function initialize(){
  //General overlay hide
  $(document).keypress(function (e){
    if(e.keyCode == 27){
      hide_overlay(); 
    }
  })
  //General overlay show
  $('.show_overlay').click(function(){
    $('.overlay').show();
  })
  $('.close-overlay').live('click',function(){
    hide_overlay();
    return false;
  })
  $('#send-fgt-pass').click(function(){
    $('#fgt-pass').show();
    $('#fgt-pass').css("top", ( $(window).height() - $('#fgt-pass').height() ) / 2+$(window).scrollTop() + "px");
    $('#fgt-pass').css("left", ( $(window).width() - $('#fgt-pass').width() ) / 2+$(window).scrollLeft() + "px");
    return false;
  })
  
  $('#show-trouble-screen').click(function(){
    $('#trouble-signin').show();
    $('#trouble-signin').css("top", ( $(window).height() - $('#trouble-signin').height() ) / 2+$(window).scrollTop() + "px");
    $('#trouble-signin').css("left", ( $(window).width() - $('#trouble-signin').width() ) / 2+$(window).scrollLeft() + "px");
    return false;
  })
  
  $('#fgt-pwd-trouble').live('click',function(){
    $('#status-fgt-pass').hide();
    var email = $('#trouble-email').val();
    $('#fgt-email').val(email);
    
    $('#fgt-pass').show();
    $('#fgt-pass').css("top", ( $(window).height() - $('#fgt-pass').height() ) / 2+$(window).scrollTop() + "px");
    $('#fgt-pass').css("left", ( $(window).width() - $('#fgt-pass').width() ) / 2+$(window).scrollLeft() + "px");
    return false;
  })
  
 /*
  $('.label_check').click(function(){
    setupLabel();
  });
   
  setupLabel();
  */
 
  //Focus on User name
  $("#txtusername").focus(function(){
    var text = $("#txtusername").val();
    if(text == "Email"){
      $("#txtusername").val("");
    }
  })
  $("#txtusername").blur(function(){
    var text = $.trim($("#txtusername").val());
    if(text == ""){
      $("#txtusername").val("Email");
    }
  })
  //Focus on Password Field
  $("#txtpassword").focus(function(){
    $('#txtpassword').hide();
    $('#password').show();
    $('#password').focus();
  })
  $("#password").blur(function(){
    if($('#password').val() == '') {
      $('#txtpassword').show();
      $('#password').hide();
    }
  })
  
  //Focus on Forget email
  $("#fgt-email").focus(function(){
    var text = $("#fgt-email").val();
    if(text == "Email"){
      $("#fgt-email").val("");
    }
  })
  $("#fgt-email").blur(function(){
    var text = $.trim($("#fgt-email").val());
    if(text == ""){
      $("#fgt-email").val("Email");
    }
  })
	
	
  //Focus on Trouble email
  $("#trouble-email").focus(function(){
    var text = $("#trouble-email").val();
    if(text == "Email"){
      $("#trouble-email").val("");
    }
  })
  $("#trouble-email").blur(function(){
    var text = $.trim($("#trouble-email").val());
    if(text == ""){
      $("#trouble-email").val("Email");
    }
  })
  
  
  $('#fgt-submit-bt').click(function(){
    $(this).attr('disabled',true)
    $(this).attr('value','loading...')
	  
    var base_url = $("#base_url").val()+"index.php/";
    var txtemail = $('#fgt-email').val();
    $.ajax({
      url: base_url+"login/submit_forgot_pass",
      data: {
        "txtemail":txtemail
      },
      async: false,
      dataType: "text",
      type: "POST", 
      success: function(msg){
        $('#fgt-submit-bt').attr('disabled',false);
        $('#fgt-submit-bt').attr('value','Send Password');
    
        $('#status-fgt-pass').html(msg);
        $('#fgt-pass').hide();
        
        $('#status-fgt-pass').css("top", ( $(window).height() - $('#fgt-pass').height() ) / 2+$(window).scrollTop() + "px");
        $('#status-fgt-pass').css("left", ( $(window).width() - $('#fgt-pass').width() ) / 2+$(window).scrollLeft() + "px");
        $('#status-fgt-pass').show();
        
      }
    });
    return false;
  })
	
	
  $('#trouble-submit-bt').click(function(){
    $(this).attr('disabled',true)
    $(this).attr('value','Check')
    
    var base_url = $("#base_url").val()+"index.php/";
    var txtemail = $('#trouble-email').val();
    $.ajax({
      url: base_url+"login/check_troble_email",
      data: {
        "txtemail":txtemail
      },
      async: false,
      dataType: "text",
      type: "POST", 
      success: function(msg){
       
        $('#trouble-submit-bt').attr('disabled',false);
        $('#trouble-submit-bt').attr('value','Check');
    
        $('#status-fgt-pass').html(msg);
        $('#trouble-signin').hide();
        
        $('#status-fgt-pass').css("top", ( $(window).height() - $('#trouble-signin').height() ) / 2+$(window).scrollTop() + "px");
        $('#status-fgt-pass').css("left", ( $(window).width() - $('#trouble-signin').width() ) / 2+$(window).scrollLeft() + "px");
        $('#status-fgt-pass').show();
        
      }
    });
    return false;
  })
}
function setupLabel() {
  if ($('.label_check input').length) {
    $('.label_check').each(function(){ 
      $(this).removeClass('c_on');
    });
    $('.label_check input:checked').each(function(){ 
      $(this).parent('label').addClass('c_on');
    });                
  };
};
function hide_overlay(){
  $('.overlay').hide();
  $('#fgt-pass').hide();
  $('#status-fgt-pass').hide();
  $('#trouble-signin').hide();
}