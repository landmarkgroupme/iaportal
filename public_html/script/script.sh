#!/bin/bash
SFTP_USER=dotahead
SFTP_SERVER=ftp.cplmg.com
SFTP_PASSWORD='D0t@head'
SFTP_PORT=22
SFTP_RDIR=/in/
now="$(date +'%Y%m%d')"
filename="intranet_users_$now.csv"
SFTP_RFILE=$SFTP_RDIR$filename
sshpass -p$SFTP_PASSWORD sftp -oPort=$SFTP_PORT $SFTP_USER@$SFTP_SERVER << EOF
get $SFTP_RFILE /var/www/html/v2/intranet_users_csv/
bye
EOF
# Cheking the CPMG csv exist
# If cpmlg user import csv exist then create the diff and ex-employee file else break.
hrms_import="/var/www/html/v2/intranet_users_csv/$filename"
if [ -s $hrms_import ];
then
    yest=$(date --date="yesterday" +"%Y%m%d")
    nowdate="$(date +'%Y%m%d')"
    yestfilename="intranet_users_$yest.csv"
    filepath="/var/www/html/v2/intranet_users_csv/$yestfilename"
    FILE=""
    i=1
    while [ ! -f "$filepath" ]
    do
        dec_date=$(date --date="-$i days" +"%Y%m%d")
        file_name="intranet_users_$dec_date.csv"
        yestfilename=$file_name
        filepath="/var/www/html/v2/intranet_users_csv/$yestfilename"
    let i=i+1
    if [ $i -gt 30 ]
     then
      break
     fi
    done
    echo "$yestfilename"
    
    diff_file="diff_$nowdate.csv"
    #header=head -n 1 /var/www/html/v2/intranet_users_csv/$yestfilename
    LINES=()
    while IFS= read -r line
    do
      echo -E "$line"
     header=$line
      LINES[${#LINES[@]}]="$line"
      break
    done < "/var/www/html/v2/intranet_users_csv/$yestfilename"
    diff --changed-group-format='%<' --unchanged-group-format='' /var/www/html/v2/intranet_users_csv/$filename /var/www/html/v2/intranet_users_csv/$yestfilename > /var/www/html/v2/intranet_users_csv/$diff_file
    
    sed -i 1i"$header" /var/www/html/v2/intranet_users_csv/$diff_file
    ex_emp_file="ex_emp_$nowdate.csv"
    new_list="new_emp_$nowdate.csv"
    old_list="old_emp_$nowdate.csv"
    cut -d , -f 4 /var/www/html/v2/intranet_users_csv/$filename > /var/www/html/v2/intranet_users_csv/$new_list
    cut -d , -f 4 /var/www/html/v2/intranet_users_csv/$yestfilename > /var/www/html/v2/intranet_users_csv/$old_list
    comm -23 <(sort /var/www/html/v2/intranet_users_csv/$old_list) <(sort /var/www/html/v2/intranet_users_csv/$new_list) > /var/www/html/v2/intranet_users_csv/$ex_emp_file
else
    echo "CPLMG file downloading failed"
fi