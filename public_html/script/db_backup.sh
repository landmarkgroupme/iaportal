#!/bin/bash
DATABASE_USERNAME=mtuser
DATABASE_PASSWORD="LmgDb\$\$10"
DATABASE_HOST=localhost
DATABASE_NAME=intranet_v2_webteam
BACKUP_FILENAME=$(date +'%d-%m-%Y')
LIVE_BACKUP_PATH="/var/www/html/v2/public_html/db_backup/$BACKUP_FILENAME"
mysqldump -u ${DATABASE_USERNAME} -p${DATABASE_PASSWORD} ${DATABASE_NAME} | gzip > ${LIVE_BACKUP_PATH}.sql.gz