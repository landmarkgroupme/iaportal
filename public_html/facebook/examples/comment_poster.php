<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

require '../src/facebook.php';

// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
  'appId'  => '156102207908505',
  'secret' => '2c84e113a98c2984278d24d29bd5acff',
));



//$access_token = 'CAACNZBWCmqpkBACHO85uDl68lgrZA3JelFTpiOlDgReHXRZAzYehKbr69eNpT4ZCVaxOoZCjdoUw9vBBCYqU4OFkChCywCptfOGSTMcBPaA1IhR3RXQmp6Y08WXEK8t4D6JHViH7FcwGa7pw0wnGp';

// Get User ID
$user = $facebook->getUser();

// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}


//$array_likes = $facebook->api("/681716965178443/likes", 'delete');
//$array_likes = $facebook->api("/681716965178443/likes", 'post');

//681716965178443_2586191


//$array_likes = $facebook->api("/681716965178443/comments", 'post', array('message' => 'I would definitely be looking to purchase this once once i get married :)'));

print_r($array_likes);
die();

// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
} else {
  $loginUrl = $facebook->getLoginUrl();
}

// This call will always work since we are fetching public data.
$naitik = $facebook->api('/naitik');

?>
<!doctype html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <title>php-sdk</title>
    <style>
      body {
        font-family: 'Lucida Grande', Verdana, Arial, sans-serif;
      }
      h1 a {
        text-decoration: none;
        color: #3b5998;
      }
      h1 a:hover {
        text-decoration: underline;
      }
    </style>
    <script src='https://connect.facebook.net/en_US/all.js'></script>
    <script type="text/javascript">
      FB.init({appId: "156102207908505", status: true, cookie: true});
    </script>
    <script type="text/javascript">
      function askPermissions() 
  {
    FB.login(function(response) {
   if (response.authResponse) {
     facebook_authenticated = true;
     console.log('Welcome!  Fetching your information.... ');
     FB.api('/me', function(response) {
       
      
       console.log(response);
       console.log('Good to see you, ' + response.name + '.');
     });
     FB.api('/me/interests', function(response) {
       //user['fb_id'] = response.id;
       var data = response.data;
       user['interests'] = [];
       $( data ).each(function (i) {
         user['interests'].push(data[i]);
         //console.log(data[i].name);
      });
       console.log(response);
       //console.log('Good to see you, ' + response.name + '.');
     });

   } else {
     facebook_authenticated = false;
     alert('Please Authorize Scratch & Win App to participate in contest');
     console.log('User cancelled login or did not fully authorize.');
   }
 }, {scope: 'user_interests, user_about_me, friends_interests, user_likes, friends_likes, email, publish_stream'});
  }
    </script>
  </head>
  <body>
    <h1>php-sdk</h1>

    <?php if ($user): ?>
      <a href="<?php echo $logoutUrl; ?>">Logout</a>
    <?php else: ?>
      <div>
        Login using OAuth 2.0 handled by the PHP SDK:
        <a href="<?php echo $loginUrl; ?>">Login with Facebook</a>
      </div>
    <?php endif ?>

    <h3>PHP Session</h3>
    <pre><?php print_r($_SESSION); ?></pre>

    <?php if ($user): ?>
      <h3>You</h3>
      <img src="https://graph.facebook.com/<?php echo $user; ?>/picture">

      <h3>Your User Object (/me)</h3>
      <pre><?php print_r($user_profile); ?></pre>
    <?php else: ?>
      <strong><em>You are not Connected.</em></strong>
    <?php endif ?>

    <h3>Public profile of Naitik</h3>
    <img src="https://graph.facebook.com/naitik/picture">
    <?php echo $naitik['name']; ?>
  </body>
</html>
