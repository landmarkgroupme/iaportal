<?php

class Curl
{
	public $agent = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3';
	public $cookie_file_path = "ckk";
	
	function xg($url)
	{
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);
		$result = curl_exec ($ch);
		curl_close ($ch);
		return $result;
	}
	
	function xp($url, $posts)
	{
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS,$posts); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);
		$result = curl_exec ($ch);
		curl_close ($ch);
		return $result;
	}

	function filter_text( $start, $end, $str_page )
	{
		$pos =  strpos ( $str_page, $start );
		if ( $pos != false ) {
			$pos = $pos + strlen($start);
			$field_value = substr( $str_page, $pos );
    		$pos =  strpos ( $field_value, $end );
	    	$field_value = substr( $field_value, 0, $pos );
			$field_value = trim($field_value);
		}
		else {
			$field_value = "";
		}		
		return $field_value;
	}

} // Class
	
?>