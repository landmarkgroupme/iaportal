<?php

// WHITELIST, BLACKLIST, IGNORE ACTIONS
if($_POST['action'] == "Update") {

	// Blacklist
	foreach($_POST['blacklist'] as $blacklist_item) {
		$blacklist_parts = explode("|", $blacklist_item);
		$blacklist_data['email'] = $db->escape(urldecode($blacklist_parts[0]));
		$blacklist_data['hrmsid'] = "0";
		$blacklisted_id = $db->query_insert("import_blacklist", $blacklist_data);
	}

	// Whitelist
	foreach($_POST['whitelist'] as $whitelist_item) {
		$whitelist_parts = explode("|", $whitelist_item);
		$whitelist_data['email'] = $db->escape(urldecode($whitelist_parts[0]));
		$whitelist_data['hrmsid'] = $db->escape($whitelist_parts[1]);
		$whitelisted_id = $db->query_insert("import_whitelist", $whitelist_data);
	}

	// Ignore
	foreach($_POST['ignore'] as $ignorelist_item) {
		$ignorelist_data['email'] = $db->escape(urldecode($ignorelist_item));
		$ignorelist_data['hrmsid'] = "0";
		$ignorelisted_id = $db->query_insert("import_blacklist", $ignorelist_data);
	}

}

			// Some emails are hard to match so we're gonna use a whitelist.
			// Maybe we can have a interface built for inserting whitelisted emails in the list and make a call to the DB to fetch the whitelisted email list
			$whitelist = array(
							"shaikh.jaffer@cplmg.com",
							"sheikh.anwar@cpksa.com",
							"anthony.clement@cplmg.com",
							"karwan.khurshid@cplmg.com",
							"sreekumar.balakrishnapillai@cplmg.com",
							"udayakumarsinniah@cplmg.com",
							"kunam.ramamohanarao@cplmg.com",
							"sheikpeer.mohd@cplmg.com",
							"mohamedsultan@cplmg.com",
							"Mariya.Menshikova@cplmg.com"
						);

$sqlblack1 = "SELECT email FROM `import_blacklist`";
$rowsblack1 = $db->fetch_all_array($sqlblack1);

$sqlwhite1 = "SELECT email,hrmsid FROM `import_whitelist`";
$rowswhite1 = $db->fetch_all_array($sqlwhite1);

if($_GET['whitelist'] == 1) {
	$blacklist_remove = "DELETE FROM `import_blacklist` WHERE hrmsid='".$db->escape($_GET['hrms'])."'";
	$db->query($blacklist_remove);

	$whitelist_data['email'] = $db->escape($_GET['email']);
	$whitelist_data['hrmsid'] = $db->escape($_GET['hrms']);
	$whitelisted_id = $db->query_insert("import_whitelist", $whitelist_data);

}
if($_GET['blacklist'] == 1) {
	$whitelist_remove = "DELETE FROM `import_whitelist` WHERE hrmsid='".$db->escape($_GET['hrms'])."'";
	$db->query($whitelist_remove);

	$blacklist_data['email'] = $db->escape($_GET['email']);
	$blacklist_data['hrmsid'] = $db->escape($_GET['hrms']);
	$blacklisted_id = $db->query_insert("import_blacklist", $blacklist_data);

}

$sql = "SELECT * FROM `ci_users`";
$rows = $db->fetch_all_array($sql);

$sqlemails = "SELECT email FROM `ci_users`";
$rowsemails = $db->fetch_all_array($sqlemails);
$emaildb = array();
foreach($rowsemails as $rowemail) {
	//$emaildb[] = $rowemail['email'];
	$emaildb[$rowemail['email']] = 1;
}


/*echo '<pre>';
print_r($emaildb);
die();*/


//print_r($rows); die();

$valid_emails = 0;
$dif_emails = 0;
$unique_emails = array();
$duplicate_emails = 0;
$indb = 0;
$indb2 = 0;
$notdb = 0;
$emailpoints = array();
$emails_for_import_warn = array();
$emails_for_import = array();

$emails_that_should_not_be_in_db = array();

//$file_path = "/Applications/MAMP/htdocs/hrms/ereview-".$review_date.".csv";
//$file_path = "/var/www/html/importer/review/review-".$review_date.".csv";
$file_path = 'intranet_users_20140506.csv';
$current_date = date("Ymd");

//$csv_file = "/Applications/MAMP/htdocs/hrms/P_intranet_users_".$current_date.".csv";
$csv_file = "/var/www/html/sftp_files/intranet_users_".$current_date.".csv";
//$review_file = "/Applications/MAMP/htdocs/hrms/for_review.csv";
$review_file = "/var/www/html/importer/review/for_review.csv";
$csv_data = 'BUSINESS_GROUP_ID,TERRITOTY_NAME,PERSON_TYPE_ID,EMPLOYEE_NUMBER,TITLE,FIRST_NAME,MIDDLE_NAMES,LAST_NAME,ORIGINAL_DATE_OF_HIRE,GDOJ,DATE_OF_BIRTH,FULL_NAME,POSITION_NAME,DEPARTMENT,CONCEPT,ORGANISATION_NAME,ORGANIZATION_ID,LOCATION,EMAIL_ADDRESS,SUPERVISOR,SUPERVISOR_ID,BAND,Mobile Phone,Board Number,EXTN,PREVIOUS_LAST_NAME,ACTUAL_WRK_PLACE';
$csv_data .= "\n";
$csv_data_verified = $csv_data;

    $file = fopen($file_path, "r") or exit("Unable to open file!");
    $row_cnt = 1;
    $row_validation = array();

    $processed_user_data = array();

    while (!feof($file)) {
      $row = fgets($file);
      $user_data = array();
      $user_data = explode(",", $row);
      //print_r($user_data); exit;
      if (!$user_data[0]) {
        continue;
      }
      $error_cnt = 0;
      //Row Header validation
      if ($row_cnt == 1) {
        $error_array = array();
        if (trim($user_data[0]) != 'BUSINESS_GROUP_ID') {
          $error_array[] = 'Business group ID missing(BUSINESS_GROUP_ID)';
        }
        if (trim($user_data[1]) != 'TERRITOTY_NAME') {
          $error_array[] = 'Territory Name missing(TERRITOTY_NAME)';
        }
        if (trim($user_data[2]) != 'PERSON_TYPE_ID') {
          $error_array[] = 'Person ID missing(PERSON_TYPE_ID)';
        }
        if (trim($user_data[3]) != 'EMPLOYEE_NUMBER') {
          $error_array[] = 'Employee number missing(EMPLOYEE_NUMBER)';
        }
        if (trim($user_data[4]) != 'TITLE') {
          $error_array[] = 'Title missing(TITLE)';
        }
        if (trim($user_data[5]) != 'FIRST_NAME') {
          $error_array[] = 'First name missing(FIRST_NAME)';
        }
        if (trim($user_data[6]) != 'MIDDLE_NAMES') {
          $error_array[] = 'Middle name missing(MIDDLE_NAMES)';
        }
        if (trim($user_data[7]) != 'LAST_NAME') {
          $error_array[] = 'Last name missing(LAST_NAME)';
        }
        if (trim($user_data[8]) != 'ORIGINAL_DATE_OF_HIRE') {
          $error_array[] = 'Hire date missing(ORIGINAL_DATE_OF_HIRE)';
        }
        if (trim($user_data[9]) != 'GDOJ') {
          $error_array[] = 'GDOJ missing(GDOJ)';
        }
        if (trim($user_data[10]) != 'DATE_OF_BIRTH') {
          $error_array[] = 'Date of birth missing(DATE_OF_BIRTH)';
        }
        if (trim($user_data[11]) != 'FULL_NAME') {
          $error_array[] = 'Full name missing(FULL_NAME)';
        }
        if (trim($user_data[12]) != 'POSITION_NAME') {
          $error_array[] = 'Position missing(POSITION_NAME)';
        }
        if (trim($user_data[13]) != 'DEPARTMENT') {
          $error_array[] = 'Department missing(DEPARTMENT)';
        }
        if (trim($user_data[14]) != 'CONCEPT') {
          $error_array[] = 'Concept missing(CONCEPT)';
        }
        if (trim($user_data[15]) != 'ORGANISATION_NAME') {
          $error_array[] = 'Organisation name missing(ORGANISATION_NAME)';
        }
        if (trim($user_data[16]) != 'ORGANIZATION_ID') {
          $error_array[] = 'Organisation id missing(ORGANIZATION_ID)';
        }
        if (trim($user_data[17]) != 'LOCATION') {
          $error_array[] = 'Location missing(LOCATION)';
        }
        if (trim($user_data[18]) != 'EMAIL_ADDRESS') {
          $error_array[] = 'Email missing(EMAIL_ADDRESS)';
        }
        if (trim($user_data[19]) != 'SUPERVISOR') {
          $error_array[] = 'Supervisor missing(SUPERVISOR)';
        }
        if (trim($user_data[20]) != 'SUPERVISOR_ID') {
          $error_array[] = 'Supervisor id missing(SUPERVISOR_ID)';
        }
        if (trim($user_data[21]) != 'BAND') {
          $error_array[] = 'Band missing(BAND)';
        }
        if (trim($user_data[22]) != 'Mobile Phone') {
          $error_array[] = 'Mobile missing(Mobile Phone)';
        }
        if (trim($user_data[23]) != 'Board Number') {
          $error_array[] = 'Board number missing(Board Number)';
        }
        if (trim($user_data[24]) != 'EXTN') {
          $error_array[] = 'Extension missing(EXTN)';
        }
        if (trim($user_data[25]) != 'PREVIOUS_LAST_NAME') {
          $error_array[] = 'Previous last name is missing(PREVIOUS_LAST_NAME)';
        }
        if (trim($user_data[26]) != 'ACTUAL_WRK_PLACE') {
          $error_array[] = 'Actual work place is missing(ACTUAL_WRK_PLACE)';
        }

        if (count($error_array)) {
          $check_result['proceed'] = 0;
          $check_result['error'] = $error_array;
          $check_result['data'] = array();
          return $check_result;
        }
        $row_cnt++;
        continue;
      }
      if (count($user_data) != 27) {
        $row_validation[] = 'Row count missmatch, didnt find 27 columns, Total columns: ' . count($user_data) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }

	  // Display complete row with user data - Commented out for speed
	  //echo $user_data[5]." - ".$user_data[6]." - ".$user_data[7]." email: ".$user_data[18];

	  // Validate email address for syntax errors
	  if (filter_var($user_data[18], FILTER_VALIDATE_EMAIL)) {
		$valid_emails++;


		$email = strtolower($user_data[18]);

		//echo ($email).'<br />';

		// Check the unique_emails array for a match - if a match is found, a duplicate flag is raised. Otherwise, save the email in the unique_emails array
		if(in_array($email, $unique_emails)) {
			// Commented out for speed
			//echo " -<u><b>DUPLICATE</b></u>- ";
			$duplicate_emails++; }
		else {
			$unique_emails[] = $email;
		}

		$emails = explode("@", $email); // extract the email username - the part before @
		$email1 = $emails[0];
		$email_parts = explode(".", $email1); // explode the email username in parts based on .
		$fname = strtolower(trim($user_data[5]));
		$mname = strtolower(trim($user_data[6]));
		$lname = strtolower(trim($user_data[7]));
		$hrmsid = $user_data[3];

		$emailpoints[$hrmsid][$email] = 0;

		foreach($email_parts as $emailp) {
				// First name contains spaces
				if(stristr($fname, " ")) {
					$fname_parts = explode(" ", $fname);
					if($emailp == $fname_parts[0]) {
						$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
					if($emailp == $fname_parts[1]) {
						$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
					if($emailp == $fname_parts[0].$fname_parts[1]) {
						$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
					if($emailp == $fname_parts[0].$fname_parts[2]) {
						$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
				}

			if($emailp == $fname) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 20; }
			if($emailp == $mname) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 20; }
			if($emailp == $lname) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 20; }

			// email is raja.pandian@cplmg.com, last name is Rajapandian
			if(str_replace(".", "", $email1) === $lname) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
			if(str_replace(".", "", $email1) === $fname) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
			if(str_replace(".", "", $email1) === $mname) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 10; }


			// ca.upm@fitnessfirst-me.com - previous preg_match matches some wrong entries - treat them here
			if($emailp === "ca") { $dif = 0; $emailpoints[$hrmsid][$email] = 0; }

			// Check the whitelist for matches
			//if(in_array($email, $whitelist)) { $dif = 1; $emailpoints[$hrmsid][$email] += 30; }

			// Whitelisting
			/*foreach ($rowswhite1 as $rowwhite) {
				if($rowwhite['email'] === $email && $rowwhite['$hrmsid'] === $hrmsid) {
					$dif = 1; $emailpoints[$hrmsid][$email] += 30;
				}
			}*/

			// Alternate Whitelisting technique
			/**********************************/
			$whitesql = "SELECT * FROM `import_whitelist` WHERE email='$email'";
			$whiterecord = $db->query_first($whitesql);
			if($whiterecord['hrmsid'] == $hrmsid) { $dif = 1; $emailpoints[$hrmsid][$email] += 35; }
			/**********************************/

			// Blacklisting
			/*foreach ($rowsblack1 as $rowblack) {
				if($rowblack['email'] === $email) {
					$dif = 1; $emailpoints[$hrmsid][$email] -= 50; $blacklisted = 1;
				}
			}	*/

			// Alternate Blacklisting technique
			/**********************************/
			$blacksql = "SELECT * FROM `import_blacklist` WHERE email='$email'";
			$blackrecord = $db->query_first($blacksql);
			if($blackrecord['email'] == $email) { $dif = 1; $emailpoints[$hrmsid][$email] -= 50; $blacklisted = 1; }
			//if($blackrecord['hrmsid'] == $hrmsid) { $dif = 1; $emailpoints[$hrmsid][$email] -= 50; $blacklisted = 1; }
			/**********************************/


			// Jaffar - - Shaik email: shaikh.jaffer@cplmg.com !!!
			// Sheik - - Abbu Saheb email: Sheikh.Anwar@cpksa.com
			/*if(preg_match("/$fname/i", $emailp)) {
				$dif = 1; }
			if(preg_match("/$mname/i", $emailp)) {
				$dif = 1; }
			if(preg_match("/$lname/i", $emailp)) {
				$dif = 1; }*/

			// Anthonthasan - - Clemen email: anthony.clement@cplmg.com !!!
			// Shaik - - Abdulqayum email: naeem.shaikh@cpksa.com SHOULD NOT BE IN DB - 16791 ??!?!?!??
			// Prakash - - Kumar email: prakashkumar.nair@cplmg.com -DUPLICATE- SHOULD NOT BE IN DB - 25335 ?!??!!??
			// Kharwan - - Khursheed email: karwan.khurshid@cplmg.com SHOULD NOT BE IN DB - 4326
			/*$subemailp = substr($emailp, 0, -2);
			if(preg_match("/$subemailp/i", $fname)) {
				$dif = 1; }
			if(preg_match("/$subemailp/i", $mname)) {
				$dif = 1; }
			if(preg_match("/$subemailp/i", $lname)) {
				$dif = 1; }*/

				// Abdul.Waheed@cplmg.com Problem  matching for more than one persons...!
				// mohammed.salman@cplmg.com

			// Bharathasankar - - Subramanian email: bharat.sankar@cplmg.com
			if(preg_match("/$emailp/i", $fname)) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 5; }
			if(preg_match("/$emailp/i", $mname)) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 5; }
			if(preg_match("/$emailp/i", $lname)) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 5; }

			// Abdul - - Al Dossary email: abdulrahman.aldossary@cpksa.com
			if(str_replace(" ", "", $fname) === $emailp) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
			if(str_replace(" ", "", $mname) === $emailp) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 10; }
			if(str_replace(" ", "", $lname) === $emailp) {
				$dif = 1; $emailpoints[$hrmsid][$email] += 10; }

			if(!stripos($email, "@cplmg.com")
				&& !stripos($email, "@cpksa.com")
				&& !stripos($email, "@fitnessfirst-me.com")
				&& !stripos($email, "@citymaxhotels.com")
				&& !stripos($email, "@arg.com.bh")
				&& !stripos($email, "@landmarkgroup.com")

			) {  $dif = 0; }

			if($dif == 1) {
				//if($fname != $emailp && $mname != $emailp && $lname != $emailp) echo " <B>WARN</B>  ";
			}
		}
		if($dif == 1) {
			// Commented out for speed
			//if(strlen($lname) < 2) echo ' <b><i>ALERT - NAME TOO SHORT!</i></b> ';
			//echo ' <i><b style="color:green;">Validated email! '.$emailpoints[$hrmsid][$email].' </b></i> ';

				$matchcounter = 0;
			foreach($rows as $row) {
				//if(in_array($email, $row)) {
				if($row['hrms_id'] === $user_data[3]) {
					//echo "<b>IN DB</b>";
					$indb++;
					$upd2status['found'] = "1";
					//$db->query_update("ci_users", $upd2status, "hrms_id='$user_data[3]'");
					$matchcounter = 1;
				}

				if(stripos($email, "@cplmg.com")) {
					$alternate_email = explode("@", $email);
					$alternate_email = $alternate_email[0]."@landmarkgroup.com";
				} elseif(stripos($email, "@landmarkgroup.com")) {
					$alternate_email = explode("@", $email);
					$alternate_email = $alternate_email[0]."@cplmg.com";
				} else {
					$alternate_email = "randomstringiiasid99";
				}

				$notindb1 = 1;
				if(isset($emaildb[$email])) $notindb1 = 0;
				if(isset($emaildb[$alternate_email])) $notindb1 = 0;

			}
				if($notindb1 === 1 && $matchcounter != 1) {
					//echo '<b style="color:blue;">NOT IN DB</b>';
					$notdb++;
					if($emailpoints[$hrmsid][$email] > 0) {
						$emails_for_import[] = array($fname." ".$mname." ".$lname, $hrmsid, $email, "points"=>$emailpoints[$hrmsid][$email]);
						$csv_data .= trim($user_data[0]).','.trim($user_data[1]).','.trim($user_data[2]).','.trim($user_data[3]).','.trim($user_data[4]).','.trim($user_data[5]).','.trim($user_data[6]).','.trim($user_data[7]).','.trim($user_data[8]).','.trim($user_data[9]).','.trim($user_data[10]).','.trim($user_data[11]).','.trim($user_data[12]).','.trim($user_data[13]).','.trim($user_data[14]).','.trim($user_data[15]).','.trim($user_data[16]).','.trim($user_data[17]).','.trim($user_data[18]).','.trim($user_data[19]).','.trim($user_data[20]).','.trim($user_data[21]).','.trim($user_data[22]).','.trim($user_data[23]).','.trim($user_data[24]).','.trim($user_data[25]).','.trim($user_data[26]);
						$csv_data .= "\n";
						if($emailpoints[$hrmsid][$email] > 35) {
							$csv_data_verified .= trim($user_data[0]).','.trim($user_data[1]).','.trim($user_data[2]).','.trim($user_data[3]).','.trim($user_data[4]).','.trim($user_data[5]).','.trim($user_data[6]).','.trim($user_data[7]).','.trim($user_data[8]).','.trim($user_data[9]).','.trim($user_data[10]).','.trim($user_data[11]).','.trim($user_data[12]).','.trim($user_data[13]).','.trim($user_data[14]).','.trim($user_data[15]).','.trim($user_data[16]).','.trim($user_data[17]).','.trim($user_data[18]).','.trim($user_data[19]).','.trim($user_data[20]).','.trim($user_data[21]).','.trim($user_data[22]).','.trim($user_data[23]).','.trim($user_data[24]).','.trim($user_data[25]).','.trim($user_data[26]);
							$csv_data_verified .= "\n";
						}
					} else {
						if($blacklisted != 1) {
							$emails_for_import_warn[] = array($fname." ".$mname." ".$lname, $hrmsid, $email, $emailpoints[$hrmsid][$email]);
						}
					}
				}
			$dif_emails++;

		} else {

			foreach($rows as $row) {
				if($row['hrms_id'] === $user_data[3]) {
					//echo ' <b style="color:red;">SHOULD NOT BE IN DB - '.$user_data[3].'</b>';

					$updstatus['mustnot'] = "1";
					//$db->query_update("ci_users", $updstatus, "hrms_id='$user_data[3]'");

					$indb2++; break;
				}
			}
		}
		$dif = 0;

	  }
	  //echo "<br>";


      $row_cnt++;
    }
    fclose($file);
    $check_result['proceed'] = 1;
    $check_result['error'] = $row_validation;
    $check_result['data'] = $processed_user_data;

	//print_r($check_result);
/*
echo "<hr><b>VALID EMAILS: $valid_emails</b>";
echo "<hr><b>VALIDATED EMAILS: $dif_emails</b> - Of which already in DB: $indb";
echo "<hr><b>VALID EMAILS NOT IN DB: $notdb</b>";
echo "<hr><b>EMAILS THAT SHOULD NOT BE IN DB: $indb2++</b>";
echo "<hr><b>UNIQUE EMAILS: "; echo count($unique_emails); echo "</b>";
echo "<hr><b>DUPLICATE EMAILS: $duplicate_emails</b>";

echo "<hr>";
*/

function array_sort_by_column(&$arr, $col, $dir = SORT_DESC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

array_sort_by_column($emails_for_import, 'points');

?>