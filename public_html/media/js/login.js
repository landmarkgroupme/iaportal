$(document).ready(function(){ // LOGIN

	$.fn.setAlignCenter = (function(){
		var margin_top = ($(window).height()/2) - ($('.wrapper').height()/2) - ($('#collage li').height()/4);
		$(this).css({'margin-top': margin_top+'px'});
	});

	if (typeof imgArray !== 'undefined')
	{
		var cols = Math.ceil($(window).width() / $('#collage li').width());
		var cols_sec = $(window).width() % $('#collage li').width();
		var rows = Math.ceil($(window).height() / $('#collage li').height()) + 1;
		var rows_sec = $(window).height() % $('#collage li').height();
		var max_random = (typeof(imgArray) == "undefined") ? 0 : imgArray.length;
		$('.wrapper .logo-intro').setAlignCenter();
		$('#collage').css({'left': ((cols_sec/2) - $('#collage li').width()/2 )+'px'});
		$('#collage li').remove();
		var repeat = [0, 0, 0];
		var index = 1;
		if(max_random > 0){

			for(var _j=0; _j<rows; _j++){
				for(var _i=0; _i<cols; _i++){
					rnd_index = Math.floor((Math.random() * max_random));
					while(rnd_index == repeat){
						rnd_index = Math.floor((Math.random() * max_random));
					}
					repeat = rnd_index;
						
					$newLi = $('<li/>').addClass('d-n');	//.html(_j + ' | ' + _i + ' = ' + rnd_index);
					$newLi.css({'text-align':'center', 'color':'#000', 'background':'url("'+ siteurl + 'media/images/avatar/'+imgArray[index]+'") no-repeat center center', 'filter' : 'progid:DXImageTransform.Microsoft.Alpha(opacity=30)'});
					$('#collage').append($newLi);
					$newLi.fadeIn(1500);
					index++;
					if(index >= max_random)
					{
						index = 1;
					}
				
			}
			}
		}
	}

	$('.textC input').focus(function(){
		$(this).parent().addClass('textC-focus');
	});
	$('.textC input').blur(function(){
		$(this).parent().removeClass('textC-focus');
	});

	$('#aForgot').click(function(){
		$('#frmForgot').slideDown(100);
		$('#frmForgot').resetForm();
		$('#frmLogin').slideUp(100);
		return false;
	});
	$('#aLogin').click(function(){
		$('#frmLogin').slideDown(100);
		$('#frmLogin').resetForm();
		$('#frmForgot').slideUp(100);
		return false;
	});

	$('.aForgetLogin').click(function(){
		$('#frmLogin').slideDown(100);
		$('#frmLogin').resetForm();
		$('#frmReset').slideUp(100);

		return false;
	});


});