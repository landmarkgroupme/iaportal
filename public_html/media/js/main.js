var siteurl = siteurl ? siteurl : '/';
var xyz;
var parent = '';
var tag_id = '';
var notification_check = false;
var loadProgress = false;
var current_time = 0;
var console=console||{"log":function(){}};
var photoSelected = false;
var ajax_uploading = false;
$(document).ready(function(){

    var progressbox_photo     = $('#progressbox_photo');
	var progressbar_photo     = $('#fileprogressbar_photo');
	var statustxt_photo       = $('#statustxt_photo');
	var completed_photo       = '0%';
	$("#frmChangePassword_w input[type=password]").val("");
	$("#frmChangePassword_w input[type=password]").blur(function() {
		if($(this).val() == "Current Password" || $(this).val() == "New Password" || $(this).val() == "Retype New Password"){
		$(this).val('');
		}
		//alert($("#frmChangePassword_w input[type=password]").val(""));
	});
  jQuery.fn.exists = function(){return this.length>0;}
  $('.fullscreen').live('click',function(){
    if ($('#report').exists()) {
      var reportCont = $('#report').html();
      $('#report-popup').html(reportCont);
      $.fancybox({
        href		: '#report-popup',
        minWidth	: '100%',
        maxWidth	: '100%',
        maxHeight	: '100%',
        openEffect	: 'fade',
        closeEffect	: 'fade'
      });
    }
  });

   $("#txt_title_widget").bind("keypress", function(e) {
          if (e.keyCode == 13) return false;
   });
	 
	if ($('#txtGlbSearch').exists()) {
	 $( "#txtGlbSearch" ).autocomplete({
      	source: siteurl + 'people/global_autocomplete_ajax?page=global',
		html: true,
		position: { my: "left 0 top", at: "left bottom", collision: "none" },
		//appendTo: "#fGlobalSearch",
		appendTo: ".newGlbSearch",
		select: function (a, b) {
			$(this).val(b.item.id);
			window.location.href = siteurl + b.item.link;
			start = 0;
			loadPeople({'noclear': 0});

			return false;
		},
    }).data("ui-autocomplete")._renderItem = function (ul, item) {

        ul.addClass('ui-glbAutocomplete'); //Ul custom class here

        return $("<li></li>")
        .addClass(item.customClass) //item based custom class to li here
        .append("<a href='#'>" + item.label + "</a>")
        .data("ui-autocomplete-item", item)
        .appendTo(ul);
    };
	}
	
	if ($('#txtGlbSearchMobile').exists()) {
	
	$( "#txtGlbSearchMobile" ).autocomplete({
      	source: siteurl + 'people/global_autocomplete_ajax?page=global',
		html: true,
		position: { my: "left 0 top", at: "left bottom", collision: "none" },
		appendTo: "#fGMobileSearch",
		select: function (a, b) {
			$(this).val(b.item.id);
			window.location.href = siteurl + b.item.link;
			start = 0;
			loadPeople({'noclear': 0});

			return false;
		},
    }).data("ui-autocomplete")._renderItem = function (ul, item) {

        ul.addClass('ui-glbAutocomplete'); //Ul custom class here

        return $("<li></li>")
        .addClass(item.customClass) //item based custom class to li here
        .append("<a href='#'>" + item.label + "</a>")
        .data("ui-autocomplete-item", item)
        .appendTo(ul);
    };
	}
	
	$('#goGlobal').click(function(){
		if($('#txtGlbSearch').val() != '' && $('#txtGlbSearch').val() != 'Who are you searching for?')
		{
			$('form#fGlobalSearch').submit();
		}
	});
	
	$('#glbGoMobile').click(function(){
		if($('#txtGlbSearchMobile').val() != '' && $('#txtGlbSearchMobile').val() != 'Who are you searching for?')
		{
			$('form#fGMobileSearch').submit();
		}
	});
	
	
  $('#fPhoto').bind("keyup keypress", function(e) {
  var code = e.keyCode || e.which; 
 /* if (code  == 13) {               
    e.preventDefault();
    return false;
  } */
});
    $('.u-menu-block').hide();
			
	$('.u-menu-pop').click(function(){
		$('.u-menu-block').html('');
		intranet_link = 'http://devnet.landmarkgroup.com/';
		pContents = '<div class="close" id="close-menu">x</div><ul>'+
					'<li><a href="' +intranet_link + myprofile.reply_username +'">My Profile</a></li>';
					
					pContents += '<li><a href="'+intranet_link+'logout">Log Out</a></li> '+'</ul>';
					$('.u-menu-block').append(pContents);
					if($('.u-menu-block').is(':hidden')){
						$('.u-menu-block').slideDown();
					}
					return false;
	});
	 
	 $('.u-menu-block > ul > li > a').live( "click", function() {
		$('.u-menu-block').slideUp();
	});
	
	 if ($('#alert').exists()) {
	$.fancybox({
			href		: '#alert',
			maxWidth	: 900,
			openEffect	: 'fade',
			closeEffect	: 'fade',
			
		});
  }
	$('.down').hide();
	$('.main-category').show();
  if($('.drops').exists()){
	  $(document).on('click', '.drops li a', function() {
		   if($(this).next().is(':hidden')){
				 $(this).addClass('active').next().slideDown().parent().addClass('opencontent');
		   }
		   else{
			   $(this).removeClass('active').next().slideUp().parent().removeClass('opencontent');
		   }
	 });
  }
	$('.hp-caption > li > a').click(function(){
		//$('.ph-loader').addClass('loader');
		$('.hp-caption > li').removeClass('active');
		$('.hp-data').addClass('d-n');
		$(this).parent().addClass('active');
		$(this).parent().find('.hp-data').removeClass('d-n');
		if($(this).hasClass('first_link'))
		{
			$(this).closest('ul.hp-caption').addClass('init-block');
			$(this).closest('.hp-caption .active').addClass('init');
			$('.hp-caption').animate({ height: ($(this).parent().find('.hp-data').height() - 12) + 'px' }, 'fast' );
		}
		else
		{
			$(this).closest('ul.hp-caption').removeClass('init-block');
			$(this).closest('ul.hp-caption').find('.init').removeClass('init');
			$('.hp-caption').animate({ height: ($(this).parent().find('.hp-data').height() + 10) + 'px' }, 'fast' );
		}
//		$('.hp-caption').height($(this).parent().find('.hp-data').height() + 10);
		if($(this).hasClass('second_link'))
		{ 
			if($('#txtPost').val() != '')
			{
				$('#addPhoto').focus().val($('#txtPost').val());
			}
		}
		return false;
	});
	
		$('.more-less').click(function(){
		if(!$(this).prev().hasClass('open')){
			$(this).prev().css({height:'auto'});
			$(this).html('less');
			$(this).prev().addClass('open');
		}
		else{
			$(this).prev().css({height:70});
			$(this).html('More');
			$(this).prev().removeClass('open');
		}
		return false;
		
	});
	
	$( ".feed-more-less" ).live( "click", function() {
		if(!$(this).prev().hasClass('open')){
			$(this).prev().css({height:'auto'});
			$(this).html('See less');
			$(this).prev().addClass('open');
		}
		else{
			$(this).prev().css({height:70});
			$(this).html('See More');
			$(this).prev().removeClass('open');
		}
		return false;
		
	});
	$( ".comment-more-less" ).live( "click", function() {
		if(!$(this).prev().hasClass('open')){
			$(this).prev().css({height:'auto'});
			$(this).html('See less');
			$(this).prev().addClass('open');
		}
		else{
			$(this).prev().css({height:65});
			$(this).html('See More');
			$(this).prev().removeClass('open');
		}
		return false;
		
	});
	
	$( "#close-menu" ).live( "click", function() {
	
	$(this).parent().slideUp();
		return false;
	});
	
	
	$('#upload_cv').click(function(){
		$('#fileUploadCV').trigger('click');
		$('#fileUploadCV').bind('change', function(){
			$('#upload_cv').val($('#fileUploadCV').val());
		});
		return false;
	});

	/* Files & Links */
	$('#upload_file_txt').on('click', function(){
		$('#file_share').trigger('click');
		$('#file_share').bind('change', function(){
		var file = $('#file_share').val();
		file= file.split('\\');
			$('#upload_file_txt').val(file.pop());
		});
		return false;
	});

	$(document).on('mouseenter', '.ph img', function(){
		$(this).closest('.fl-contents').find('blockquote.icon').css('opacity', '0.95');
	});
	$(document).on('mouseout', '.ph img', function(){
		$(this).closest('.fl-contents').find('blockquote.icon').css('opacity', '0');
	});

	$(document).on('mouseenter', function(){
		$(this).closest('.fl-contents').find('blockquote.icon').stop( true, true ).css('opacity', '0.95');
	});
	$(document).on('mouseout', function(){
		$(this).closest('.fl-contents').find('blockquote.icon').stop( true, true ).css('opacity', '0');
		return false;
	});
	/* MARKETPLACE INNTER 20 September 2013 01:47:21 */

	$('ul.tn-gallery li').click(function(){
		var _src = $(this).find('img').attr('data-big');
		$('ul.tn-gallery li').removeClass('active');
		$(this).addClass('active');
		$('.spotlight img').fadeOut("slow", function(){
			$(this).attr('src', _src);
			$('.spotlight').addClass('show-loader');
			$('.spotlight img').fadeIn("slow", function(){
				$('.spotlight').addClass('show-loader');
			});
		})
		return false;
	});

	$('x.hp-caption > li a').click(function(event){
		// console.log('akjdfdskjfalkdsjalj ...');	// + $(this).html());

		return false;
	});
	
	var ajax_request = false;
    $('textarea.tagged_text').textntags({
	 triggers: {'@': {
            uniqueTags   : true,
           // syntax       : _.template('@[<%= title %>](<%= type %>:<%= id %>)'),
			syntax       : _.template('@#<%= id %>:<%= title %>:'),
            parser       : /(@)\[([\w\s@\.,-\/#!$%\^&\*;:{}=\-_`~()]+)\]\(([\w\s\.\-]+):(\d+)\)/gi,
            parserGroups : {id: 4, type: 3, title: 2},
        }},
        onDataRequest:function (mode, query, triggerChar, callback) {
            // fix for overlapping requests
            if(ajax_request) ajax_request.abort();
            //ajax_request = $.getJSON('people/autocomplete_ajax_tags?{query='+query+'}', function(responseData) {
			var service_url = siteurl+"people/autocomplete_ajax_tags";
			var data = {'query' : query};
			ajax_request = $.ajax({url: service_url, data: data, async: false, cache: false, dataType: "json", type: "POST",
      success: function(responseData){
                query = query.toLowerCase();
               // responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query) > -1; });
                callback.call(this, responseData);
                ajax_request = false;
				}
            });
        }
    });
	
	$("#fShare .textntags-wrapper").css("height","50px");
	
	var ajax_request1 = false;
    $('textarea.tagged_text_photo').textntags({
	 triggers: {'@': {
            uniqueTags   : true,
           // syntax       : _.template('@[<%= title %>](<%= type %>:<%= id %>)'),
			syntax       : _.template('@#<%= id %>:<%= title %>:'),
            parser       : /(@)\[([\w\s@\.,-\/#!$%\^&\*;:{}=\-_`~()]+)\]\(([\w\s\.\-]+):(\d+)\)/gi,
            parserGroups : {id: 4, type: 3, title: 2},
        }},
        onDataRequest:function (mode, query, triggerChar, callback) {
            // fix for overlapping requests
            if(ajax_request1) ajax_request1.abort();
            //ajax_request = $.getJSON('people/autocomplete_ajax_tags?{query='+query+'}', function(responseData) {
			var service_url = siteurl+"people/autocomplete_ajax_tags";
			var data = {'query' : query};
			ajax_request1 = $.ajax({url: service_url, data: data, async: false, cache: false, dataType: "json", type: "POST",
      success: function(responseData){
                query = query.toLowerCase();
                //responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query) > -1; });
                callback.call(this, responseData);
                ajax_request1 = false;
				}
            });
        }
    });
	
	$('textarea.tagged_text_comment').live('click', function(){
	var ajax_request2 = false;
    //$('input.tagged_text_comment').textntags({
	$(this).css("height","43px");
	$(this).textntags({
	 triggers: {'@': {
            uniqueTags   : true,
//syntax       : _.template('@[<%= title %>](<%= type %>:<%= id %>)'),
			//syntax       : _.template('@#<%= id %>:<%= title %>:'),
            parser       : /(@)\[([\w\s@\.,-\/#!$%\^&\*;:{}=\-_`~()]+)\]\(([\w\s\.\-]+):(\d+)\)/gi,
            parserGroups : {id: 4, type: 3, title: 2},
        }},
        onDataRequest:function (mode, query, triggerChar, callback) {
            // fix for overlapping requests
            if(ajax_request2) ajax_request2.abort();
            //ajax_request = $.getJSON('people/autocomplete_ajax_tags?{query='+query+'}', function(responseData) {
			var service_url = siteurl+"people/autocomplete_ajax_tags";
			var data = {'query' : query};
			ajax_request2 = $.ajax({url: service_url, data: data, async: false, cache: false, dataType: "json", type: "POST",
      success: function(responseData){
                query = query.toLowerCase();
                //responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query) > -1; });
                callback.call(this, responseData);
                ajax_request2 = false;
				}
            });
        }
    });
	$(this).focus();
	});
	
	 if ($('.textntags-tag-list').exists()) {
	$(document).click(function (){
		var taglist=$('.textntags-tag-list');
		if(taglist.length>0){
			if( taglist.css('display') == 'block') {
				taglist.hide();
			}
		}
	});
	
	}
	

	$(function (){
  if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
    function resetPlaceholder() {
      if ($(this).val() === '') {
        $(this).val($(this).attr('placeholder'))
          .attr('data-placeholder', true)
          .addClass('ie-placeholder');
        if ($(this).is(':password')) {
          var field = $('<input />');
          $.each(this.attributes, function (i, attr) {
            if (attr.name !== 'type') {
              field.attr(attr.name, attr.value);
            }
          });
          field.attr({
            'type': 'text',
            'data-input-password': true,
            'value': $(this).val()
          });
          $(this).replaceWith(field);
        }
      }
    }

    $('[placeholder]').each(function () {
      //ie user refresh don't reset input values workaround
      if ($(this).attr('placeholder') !== '' && $(this).attr('placeholder') === $(this).val()){
        $(this).val('');
      }
      resetPlaceholder.call(this);
    });
    $(document).on('focus', '[placeholder]', function () {
      if ($(this).attr('data-placeholder')) {
        $(this).val('').removeAttr('data-placeholder').removeClass('ie-placeholder');
      }
    }).on('blur', '[placeholder]', function () { resetPlaceholder.call(this); });
    $(document).on('focus', '[data-input-password]', function () {
      var field = $('<input />');
      $.each(this.attributes, function (i, attr) {
	  // console.log(attr);
        if (attr.name !='type' && attr.name != 'data-placeholder' && attr.name != 'data-input-password') {
          field.attr(attr.name, attr.value);
        }
      });
      field.attr('type', 'password').on('focus', function () { this.select(); });
      $(this).replaceWith(field);
      field.trigger('focus');
    });
	
  }
});


	$.fn.initHPWidget = function(){
		$('.hp-caption > li').removeClass('active');
		$('.hp-data').addClass('d-n');
		$('.share').addClass('active');
		$('.share').find('.hp-data').removeClass('d-n');

		$('.hp-caption').height($('.share').find('.hp-data').height() + 10);
		return true;
	};

	$.fn.resetWidget = function(){
		// console.log('widget reseted...')
		$('.hp-caption').removeClass('init-block');
		$('.hp-caption li.share').removeClass('init');
	}

	
	//	Compacting input box for Post, Photo & Question
	$(document).on('click', '#txtPost', function(){
		$(this).closest('ul.hp-caption').removeClass('init-block');
		$(this).closest('ul.hp-caption').find('.init').removeClass('init');
		//$('#txtPost').autosize();
		$('.hp-caption').animate({height: ($('.share').find('.hp-data').height() + 10) + 'px'}, 'fast');
	});
	$(document).on('click', '#addPhoto', function(){
		$('.hp-caption').animate({height: ($('.photo').find('.hp-data').height() + 10) + 'px'}, 'fast');
	});
	$(document).on('click', '#addQuestion', function(){
		$('.hp-caption').animate({height: ($('.poll').find('.hp-data').height() + 10) + 'px'}, 'fast');	
	});

	$(document).on('click, focus', 'ul.options li:last-child input', function(_i){
		var _l = $('ul.options li input').length;
		if(_l <= 5)
		{
		if($(this).parent().prev().find('input').val() != '' && $(this).parent().prev().find('input').val() != 'Add an answer')	{
			$(this).closest('ul').append('<li><input type="text" placeholder="Add an answer" name="options[]" id="addAnswer_'+_l+'" /></li>');
			$('.hp-caption').animate({height: ($('.poll').find('.hp-data').height() + 10) + 'px'}, 'fast');
			//console.log('clicked...');
		}
		}
	})


	$('.bd-today').click(function(){
//			$.prompt("Hello World Custom Example...!");
		var _x = $(this).offset().left;
		var _y = $(this).offset().top;
		var tourSubmitFunc = function(e, v, m, f){
			// console.log('tourSubmitFunc...');
		},
		tourStates = [
			{
				html: '<div class="pop-bd clearfix"><img src="media/images/iconXYZ_sm.png" width="25" height="25" alt="Jesse Lander" class="round-c"/> <span class="normal-text">Nicholas Hughe</span> <div class="rp-social-comment-box"><input id="bd-post1" name="bd-post1" type="text"  class="rp-comment" placeholder="Wish him a Birthday" /></div></div>'+
				'<hr class="dotted"/>'+
				'<div class="pop-bd clearfix"><img src="media/images/iconXYZ_sm.png" width="25" height="25" alt="Jesse Lander" class="round-c"/> <span class="normal-text">Jesse Lander</span> <div class="rp-social-comment-box"><input id="bd-post2" name="bd-post2" type="text" class="rp-comment" placeholder="Wish him a Birthday" /></div></div>'+
				'<hr class="dotted"/>'+
				'<div class="pop-bd clearfix"><img src="media/images/iconXYZ_sm.png" width="25" height="25" alt="Jesse Lander" class="round-c"/> <span class="normal-text">Jesse Lander</span> <div class="rp-social-comment-box"><input id="bd-post2" name="bd-post2" type="text" class="rp-comment" placeholder="Wish him a Birthday" /></div></div>',
				buttons: false,
				focus: 0,
				position: { container: '.bd-today', x: -515, y: 0, width: 450, arrow: 'rt' },
				submit: tourSubmitFunc
			}
		];

		$.prompt(tourStates);
		return false;
	});

	$('.notification-pop').click(function(){
	if(notification_check == true)
	{
		return false;
	} else {
	notification_check = true;
	}
		//alert('here');
		var pContents = '';
		var service_url = siteurl+"status_update/notifications";
		$.ajax({
			url: service_url,
			async: false,
			dataType: "json",
			type: "POST",
			success: function(response){
				
				_.templateSettings.variable = "rc";

		        // Grab the HTML out of our template tag and pre-compile it.
		        var template = _.template(
		            $( "#notifications_temp" ).html()
		        );

		        pContents = template(response);
		        //$('.notification-pop').css('visibility', 'hidden');
				}
		});

		var notificationStates	= {
				//buttons: false,
				classes: {box: 'notification'},
				position: { container: '.notification-pop', x: -378, y: 35, width: 400, arrow: 'tr'},
				focus: 1,
				close: function(e, v, m, f){
				notification_check = false;
				
			   },
			   statechanged : function(e){
					if($('.media-show').is(':visible'))
					{
					var notifyHgt = $('.notification div.jqi').outerHeight();
					var realHgt = (notifyHgt / 2);
					$('.notification div.jqi').css({
					'margin-top': -(realHgt)
					});
					}
			
			   }, 
				//show: 'slideDown',
				//promptspeed: 200*/
			};
		$('.notification-pop span').css('visibility', 'hidden');
		$.prompt(pContents, notificationStates);
		return false;
	});

	function handleFileSelect(evt) {
	
		if($('#fileUploadFile').val() != '')
    		photoSelected = true;
    	else
    		photoSelected = false;

    	changeSubmitPhotoState();
	
	/*
		imgPlaceholder	= $('#add_more_photos img');
		_fileInputField	= $('#fileUploadFile');
    	//var files = evt.srcElement.files; // FileList object
		console.log(evt.srcElement);
		console.log('kunal');
		if (typeof evt.srcElement == "undefined")
		{
			var files = evt.target.files;
		}
		else
		{
			var files = evt.srcElement.files;
		}
		
    	if(files.length > 0)
    		photoSelected = true;
    	else
    		photoSelected = false;

    	changeSubmitPhotoState();
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          //var span = document.createElement('span');
          //span.innerHTML = ['<img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"/>'].join('');
          //document.getElementById('list').insertBefore(span, null);
          //e.target.result
          $(imgPlaceholder).attr('src', e.target.result);

        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    } */
  }
	$('#ddConceptsMobileClear').on( "click", function() {
		$('#ddConceptsMobile').val('0').trigger('change');
	});
	$('#ddDepartmentsMobileClear').on( "click", function() {
		$('#ddDepartmentsMobile').val('0').trigger('change');
	});
	$('#ddCountryMobileClear').on( "click", function() {
		$('#ddCountryMobile').val('0').trigger('change');
	});
	$('#ddCategoriesMobileClear').on( "click", function() {
		$('#ddCategoriesMobile').val('0').trigger('change');
	});
	$('#ddPriceMobileClear').on( "click", function() {
		$('#ddPriceMobile').val('0').trigger('change');
	});
	$('#ddPriceMobileClear').on( "click", function() {
		$('#ddPriceMobile').val('0').trigger('change');
	});


	$('#add_more_photos').click(function(){
		// $(this).parents(".formFile").find("input[type='file']").trigger('click');
		$('#fileUploadFile').trigger('click');
		return false;
	});

	//document.getElementById('fileUploadFile').addEventListener('change', handleFileSelect, false);

	//comente on 30 march to work in ie
	$('#fileUploadFile').change(handleFileSelect);


	function changeSubmitPhotoState()
	{
		var photo_text = $('#addPhoto');

		//if(photo_text.val() != '' && photoSelected)
		if(photoSelected)
		{
			$('#btnPhotoUpload').removeClass('inactive');
			$('#btnPhotoUpload').removeAttr('disabled');
		}
		else {
			$('#btnPhotoUpload').addClass('inactive');
			$('#btnPhotoUpload').attr('disabled','disabled');
		}
	}

	$('#addPhoto').keyup(changeSubmitPhotoState);

	$('#txtPost').keyup(function(){
		if($(this).val() != '')
		{
			$('#fShare input[type=submit], #fShare input[type=button].submit').removeClass('inactive');
			$('#fShare input[type=submit], #fShare input[type=button].submit').removeAttr('disabled');
		}
		else {
			$('#fShare input[type=submit], #fShare input[type=button].submit').addClass('inactive');
			$('#fShare input[type=submit], #fShare input[type=button].submit').attr('disabled','disabled');
		}

	});
	
	$('#addQuestion').keyup(function(){
		if($(this).val() != '')
		{
			$('#fPoll input[type=submit], #fPoll input[type=button].submit').removeClass('inactive');
			$('#fPoll input[type=submit], #fPoll input[type=button].submit').removeAttr('disabled');
		}
		else {
			$('#fPoll input[type=submit], #fPoll input[type=button].submit').addClass('inactive');
			$('#fPoll input[type=submit], #fPoll input[type=button].submit').attr('disabled','disabled');
		}

	});


	//$('#fShare, #fPhoto, #fPoll').click(function(){

	$('input[type=submit]').click(function(){
		if($(this).parents('form').find('.validate').val() == 'true'){
			$(this).parents('form').validate();
		}
		$(this).parents('form').sendAJAX();
		return false;
	});

	$('#btnPhotoUpload').click(function(){
		status_photo_upload();
	});


	$('#fShare input.submit').click(function(){

		if($(this).parents('form').find('.validate').val() == 'true'){
			$(this).parents('form').validate();
		}
		$(this).parents('form').sendAJAX();
		return false;
	});

	$('#fPhoto input.submit').click(function(){
		if($(this).parents('form').find('.validate').val() == 'true'){
			$(this).parents('form').validate();
		}
		// console.log('test...');
		//$(this).parents('form').sendAJAX();
		return false;
	});

	$('#fPoll input.submit').click(function(){
		if($(this).parents('form').find('.validate').val() == 'true'){
			$(this).parents('form').validate();
		}
		postPoll($(this).parents('form'));
		return false;
	});


	$( ".vPoll input.submit" ).live( "click", function() {
		if($(this).parents('form').find('.validate').val() == 'true'){
			$(this).parents('form').validate();
		}
		votePoll($(this).closest('form'));
		return false;
	});

	$.fn.enterKey = function (fnc) {
		return this.each(function () {
			$(this).keypress(function (ev) {
				var keycode = (ev.keyCode ? ev.keyCode : ev.which);
				if (keycode == '13') {
					fnc.call(this, ev);
				}
			});
		});
	}

	$('#frmEditProfile').bind('submit', function() {

		if ($('#display_name').val().length < 1 || $('#username').val().length < 1) {
			$('.msg').removeClass('success');
			$('.msg').addClass('error');
			$('.msg').append('<li/>').html('Please retry, enter data');
			$('.msg').fadeIn();
			//$.fancybox.resize();
			return false;
		}

		$.fancybox.showLoading();

		$.ajax({
			type	: 'POST',
			cache	: false,
			url		: $(this).attr('action'),
			data	: $(this).serializeArray(),
			success	: function(data) {
				$.fancybox.hideLoading();
				$('.msg').addClass('success');
				$('.msg').append('<li/>').html('Your profile updated successfully!');
				$('.msg').fadeIn();
				$.fancybox.resize();
				// $.fancybox(data);
				// console.log(data);
			}
		});

		return false;
	});

	$('.avatar-big').bind('mouseenter', function(){
		$('.editPicture').removeClass('d-n');
	})
	$('.avatar-big').bind('mouseout', function(){
		$('.editPicture').bind('mouseenter', function(){
			$('.editPicture').removeClass('d-n');
			//console.log('mouse entered in editpix');
			return false;
		})
		$('.editPicture').addClass('d-n');
	});

	/*$('#add_edit_photo').click(function(){
		// $(this).parents(".formFile").find("input[type='file']").trigger('click');
		$('#fileUploadPicture').trigger('click');
		return false;
	}); 

	$('#upload_photos').click(function(){
		// $(this).parents(".formFile").find("input[type='file']").trigger('click');
		$('#fileUploadPhotos').trigger('click');
		return false;
	});
  Comment to fix the ie issue */
	$(document).on('focus', '.textC input, .textC textarea', function(){
		$(this).parent().addClass('textC-focus');
	});

	$(document).on('blur', '.textC input, .textC textarea', function(){
		$(this).parent().removeClass('textC-focus');
	});

	$('ul.filter li a').click(function (){
		$("div#recent-posts .block-2.clearfix").remove();
		$("div#recent-posts .block-1.clearfix").remove();
		$("div#recent-posts .cp-photo-gallery-box").remove();
		var type = $(this).attr('type');
		parent = $(this).closest('li');
		start = 0;
		loadUpdates(type, parent);
	});

	$('#recent-posts').on('click', '.bLike', function (e) {
		var res = likeStatus($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.cmLike', function (e) {
		var res = likeComment($(this));
		e.preventDefault();
	});

	$('#recent-posts').on('click', '.featured', function (e) {
	var res = featuredStatus($(this));
	e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.featured_file', function (e) {	
	var res = featuredFiles($(this));
	e.preventDefault();
	});
	$('#recent-posts').on('click', '.featured_polls', function (e) {	
	var res = featuredPolls($(this));
	e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.bComment', function (e) {
	/*	var parent_id = $(this).parents().eq(4).attr("id");
		$('#'+parent_id+' .rp-comment').focus();*/
		$(this).closest('.block-2').find('.rp-comment').click();
		e.preventDefault();
	});

	$('#recent-posts').on('click', '.bDelete', function (e) {
		var res = deleteStatus($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.cDelete', function (e) {
		var res = deleteComments($(this));
		e.preventDefault();
	});

  $('#recent-posts').on('click', '.pDelete', function (e) {
		var res = deletePhoto($(this));
		e.preventDefault();
	});
	
   $('#recent-posts').on('click', '.noaDelete', function (e) {
		var res = deleteNewsOffer($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.editnoaDelete', function (e) {
		var res = editNewsOffer($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.paDelete', function (e) {
		var res = deletePhotoAlbum($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.pollDelete', function (e) {
		var res = deletePoll($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.file_delete', function (e) {
		var res = deleteFile($(this));
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.pComment', function (e) {
		/*var parent_id = $(this).parents().eq(5).attr("id");
		console.log(this);
		alert(parent_id);
		$('#'+parent_id+' .rp-comment').focus();*/
		$(this).closest('.block-2').find('.rp-comment').click();
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.fDelete', function (e) {
		var res = deletefile($(this));		
		e.preventDefault();
	});
	
	$('#recent-posts').on('click', '.more-comments', function (e) {
		var res = showMoreComments($(this)); 
    load_all_comment($(this));
		e.preventDefault();
	});
  
	//Follow, Unfollowo a User
	$(document).on('click', '.btn-follow',  function(){
  		//var parent = $(this).closest('div.block-follow');
	  	var user_id = $(this).attr('user_id');
	  	// console.log(user_id);
	  	var action = $(this).val() == 'Follow' ?  'follow_user' : 'unfollow_user' ;
	  	var button = $(this);

    	var service_url = siteurl+"people/"+ action;
	     $.ajax({
	      url: service_url,
	      data: {'user':user_id},
	      async: false,
	      dataType: "json",
	      type: "POST",
	      success: function(msg){
	        if(msg.status == 'success'){
	         	if(msg.action == 'Follow')
	         	{
	         		$('.btn-follow[user_id='+ user_id +']').switchClass( "", "btn-grn", 400, "easeInOutQuad" ).val('Following');
	         	}
	         	else
	         	{
	         		$('.btn-follow[user_id='+ user_id +']').switchClass( "btn-grn", "", 400, "easeInOutQuad" ).val('Follow');
	         	}
	        }
	      }
	    });
  	});

	$(document).on('mouseover', '.btn-follow',  function(){
		if($(this).val() == 'Following')
		{
			$(this).val('Unfollow');
		}
	});

	$(document).on('mouseout', '.btn-follow',  function(){
		if($(this).val() == 'Unfollow')
		{
			$(this).val('Following');
		}
	});


  	$('.btn-object-follow').live('click',function(){
		var object_id = $(this).attr('object_id');
		var object_type = $(this).attr('object_type');


	  	var button = $(this);

    	var service_url = siteurl+"objects/subscription";
	     $.ajax({
	      url: service_url,
	      data: {'object_id':object_id, 'object_type': object_type, 'action': $(this).val()},
	      async: false,
	      dataType: "json",
	      type: "POST",
	      success: function(msg){
	        if(msg.status == 'success'){
	         	if(msg.action == 'Follow' || msg.action == 'Join')
	         	{
	         		$( button ).switchClass( "btn-grn", "", 400, "easeInOutQuad" ).val(msg.action == 'Follow' ? 'Follow' : 'Join');
	         	}
	         	else
	         	{
	         		$( button ).switchClass("", "btn-grn",  400, "easeInOutQuad" ).val(msg.action == 'Following' ? 'Following' : 'Leave');
	         	}
	        }
	      }
	    });
		if((typeof concept != 'undefined' && concept != 0 && typeof concept_page != 'undefined' && concept_page == true) || (typeof group != 'undefined' && group != 0  && typeof group_page != 'undefined' && group_page == true))
		{
			 window.location.href =document.URL;
		}

	});


	$('.cp-img-text').each(function(){$(this).css({'margin-top' : -$(this).outerHeight()/2})});
	$('.cp-img-logo, .cp-img-big, .cp-span-big, .cp-span-text div, .cp-img-text, .cp-bg-big').bind('mouseenter', function(){
		$(this).parents('.concept-box').find('.cp-img-big').css('opacity', '1');
		$(this).parents('.concept-box').find('.cp-bg-big').css('opacity', '1');
		$(this).parents('.concept-box').find('.cp-span-big').css('opacity', '1');
	});
	$('.cp-img-big, .cp-span-big, .cp-img-text, .cp-bg-big').on('mouseleave', function(){
		$(this).parents('.concept-box').find('.cp-img-big').css('opacity', '.4');
		$(this).parents('.concept-box').find('.cp-bg-big').css('opacity', '.4');
		$(this).parents('.concept-box').find('.cp-span-big').css('opacity', '.4');
	});


	var objTbl = $('.containerDiv.viewGrid .rowDiv .tCell');
	var objInnerTbl	= $('.containerDiv.viewGrid .rowDiv .tCell section ul');
	objTbl.height(objTbl.width() - 10);
	objInnerTbl.height(objTbl.height() - 2);

	$('#people-page #l-grid-view').on('click', function(e){
		e.preventDefault();
		$('.view .l-grid-view').addClass('active').removeClass('copacity-100');
		$('.view .l-list-view').removeClass('active').removeClass('xopacity-50');

		$('.people-grid').css('display', 'table');
		$('.people-list').css('display', 'none');

		sessionStorage.setItem("people_grid_view" , "true");
	    //alert(sessionStorage.getItem("people_grid_view"));
		return false;
	});
	$('#people-page #l-list-view').on('click', function(e){
		e.preventDefault();
		$('.view .l-grid-view').removeClass('active').removeClass('xopacity-100');
		$('.view .l-list-view').addClass('active').removeClass('xopacity-50');

		$('.people-grid').css('display', 'none');
		$('.people-list').css('display', 'table');

		sessionStorage.removeItem("people_grid_view");
		return false;
	});

	$('.market-grid-view #l-grid-view').on('click', function(e){
		e.preventDefault();
		$('.view li').removeClass('active');
		$('.market-grid').removeClass('list-view');
		$(this).closest('.l-grid-view').addClass('active');
		sessionStorage.setItem("grid_view" , "true");
		return false;
	});
	$('.market-grid-view #l-list-view').on('click', function(e){
		e.preventDefault();
		$('.view li').removeClass('active');
		$('.market-grid').addClass('list-view');
		$(this).closest('.l-list-view').addClass('active');
		sessionStorage.removeItem("grid_view");
		return false;
	});

	$('.clear_all').click(function() {
		$('.filter-dd').val(0);
		//return false;
	})
	
	$('#txtSearch').enterKey(function(){
		var searched = $('#txtSearch').val();
		if(searched == '' || searched == 'Please enter keywords' || searched == 'Search Jobs by keywords' || searched == 'Search by name' || searched == 'Search for files by keyword' || searched == 'Search for item' )
		{

				$("#txtSearch").attr("data-placeholder", "Please enter keywords");
				$("#txtSearch").attr("placeholder", "Please enter keywords");
				if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
					$("#txtSearch").val($("#txtSearch").attr('placeholder'))
					.attr('data-placeholder', true)
					.addClass('ie-placeholder');
					$("#txtSearch").focus();
				}
			return false;
		}
	
	});
	$('#go').on( "click", function() {

	   var searched = $('#txtSearch').val();
		if(searched == '' || searched == 'Please enter keywords' || searched == 'Search Jobs by keywords' || searched == 'Search by name' || searched == 'Search for files by keyword' || searched == 'Search for item' )
		{
			$("#txtSearch").attr("data-placeholder", "Please enter keywords");
			$("#txtSearch").attr("placeholder", "Please enter keywords");
			if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
			$("#txtSearch").val($("#txtSearch").attr('placeholder'))
			.attr('data-placeholder', true)
			.addClass('ie-placeholder');
			}
			return false;
		}
		//$('#txtSearch').enterKey();
	});
	
	/* TOOL TIP - AdeshM 28 October 2013 15:13:05 */
	$(document).on('mouseenter', 'span.qTip', function () {
		var qTipText = 'Follow Landmarkers to see their latest updates on your news feed.';
		qTipText = (($(this).attr('title') == 'undefined') || ($(this).attr('title') == undefined)) ? qTipText : $(this).attr('title');
    if(!$(this).find('.tooltip').length)
    {
		  $(this).append('<div class="tooltip"><p>'+qTipText+'</p></div>');
		  $(this).attr('title', '');
    }
	});
	$(document).on('mouseout', 'span.qTip', function () {
		$(this).attr('title', $('div.tooltip').find('p').html());
		$('div.tooltip').remove();
	});

	/* Jump to Top - AdeshM 28 October 2013 23:57:54 */
	$.fn.jumpToTop = function(){
	//	_objJumpBox = $('<div>').html('<a id="scroll-to-top" href="#header"><span>Jump to top</span></a>');
		var _objJumpBox = $('<a>')
			.attr('id', 'scroll-to-top')
			.attr('href', '#header')
			.html('<span>Jump to top</span>');
		_objJumpBox.appendTo('body');
	}();

	var offset = parseInt($('.content').height()) - 100;
	var duration = 500;
	$('.content').on('scroll', function() {
		if ($(this).scrollTop() > offset) {
			$('#scroll-to-top').fadeIn(duration);
		} else {
			$('#scroll-to-top').fadeOut(duration);
		}
	});

	$(document).on('click', '#scroll-to-top', function(e) {
		e.preventDefault();
		$('.content').animate({scrollTop: 0}, duration);
		return false;
	});



	if($('.notification-pop span').html() == '&nbsp;' || $('.notification-pop span').html() == '')
	{
		//there are no notifications so lets hide the box
		if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
		$('.notification-pop span').css('background-color', '#150A07');
		$('.notification-pop span').css('filter', 'alpha(opacity = 40)');
		}
		else
		{
		$('.notification-pop span').css('opacity', '0');
		}
	}
	
	$(".other_users").fancybox({
		beforeLoad : function() {
			//console.log(this.element[0].id);
			var others_id = this.element[0].id;
			parent_div = $("#"+others_id).closest('.block-1');
			if(parent_div.length==0){
				parent_div = $("#"+others_id).closest('.block-2');
			}
			post_id = $(parent_div).attr('id');
			post_type = $(parent_div).attr('content_type');
			object_id = $(parent_div).attr('object_id');
			object_type = $(parent_div).attr('object_type');
			//console.log(post_id);
			//console.log(post_type);
			//console.log(object_id);
			//console.log(object_type);
			var service_url = siteurl+"status_update/other_users_liked";
			var str = '';
			$.ajax({
				url: service_url,
				data: {"post_id":post_id, "post_type": post_type, "object_id" : object_id, "object_type" : object_type},
				async: false,
				dataType: "json",
				type: "POST",
				success: function(data){
					//console.log(data);
					//str = "<div id='followings-view-all-box' class='bd-all-box fb-container-box d-n' style='display: block;'>";
					$.each(data,function(i,item){
						str = str + "<div class='pop-bd clearfix'><a href='"+siteurl+data[i].reply_username+"' class='normal-text'><img src='"+siteurl+"/images/user-images/105x101/"+data[i].profile_pic+"' width='25' height='25' alt='"+data[i].display_name+"' class='round-c'>"+data[i].display_name+"</a></div><hr class='dotted'>";
					});
					//str = str + "</div>";
				$("#others_view_list").html(str);
				}
			});
		}
	});
	
	
	$(".other_users_comment").fancybox({
	beforeLoad : function() {
		var others_id = this.element[0].id;
		parent_div = $("#"+others_id).closest('.post-cmntlike');
		if(parent_div.length==0){
			parent_div = $("#"+others_id).closest('.block-2');
		}
		post_id = $(parent_div).attr('id');
		post_type = 'Status';
		object_id = $(parent_div).attr('object_id');
		object_type = $(parent_div).attr('object_type');
		//console.log(post_id);
		//console.log(post_type);
		//console.log(object_id);
		//console.log(object_type);
		var service_url = siteurl+"status_update/other_users_liked";
		var str = '';
		$.ajax({
			url: service_url,
			data: {"post_id":post_id, "post_type": post_type, "object_id" : object_id, "object_type" : object_type},
			async: false,
			dataType: "json",
			type: "POST",
			success: function(data){
				//console.log(data);
				//str = "<div id='followings-view-all-box' class='bd-all-box fb-container-box d-n' style='display: block;'>";
				$.each(data,function(i,item){
					str = str + "<div class='pop-bd clearfix'><a href='"+siteurl+data[i].reply_username+"' class='normal-text'><img src='"+siteurl+"/images/user-images/105x101/"+data[i].profile_pic+"' width='25' height='25' alt='"+data[i].display_name+"' class='round-c'>"+data[i].display_name+"</a></div><hr class='dotted'>";
				});
				//str = str + "</div>";
			}
		});	
		
		$("#others_comment_view_list").html(str);
	}
	});
	

	$('.feedback_open').fancybox({
	helpers : {
        title : null /* Title is a helper and is enabled by default, you can disable it */
    },
	afterClose  : function() {
           $("#frmFeedback")[0].reset();
           $('#frmFeedback .msg').removeClass('success');
           $('#frmFeedback .msg').removeClass('error');
           $('#frmFeedback .msg').html('');
		   $('#frmFeedback .msg').fadeIn();
		   $('.error-msg').html('');
		   }
	});
	$('#add_feedback').on('click', function(){
	var query = $('select[name=query]').val();
	var desc = $('textarea[name=describe]').val();
	if(query == 0 || query == ""){
		$("#error-query").html("Required");
		$("#query").focus();
		return false;
		}
		else
		{
			$("#error-query").html("");
		}
		if(desc =="" || desc == "Your thoughts here"){
		$("#describe").focus();
		return false;
		}

						$.ajax({
					beforeSend: function() {
								inactive_feedback_form();
								},
					complete: function() {
								active_feedback_form();

								},
					type	: 'POST',
					cache	: false,
					url		: $('#frmFeedback').attr('action'),
					data	:{'query': query, 'describe': desc},
					success	: function(data) {
					 obj = JSON.parse(data);

					// console.log(obj.status);
					// console.log(obj);
					// console.log(obj.status);
					if(obj.status === 'yes')
						{

							$("input[type=text], textarea , select ").val("");
							$("input[type=file]").val("");
							$('.msg').addClass('success');
							$('.msg').append('<li/>').html('Thanks for your thoughts.');
							$('.msg').fadeIn();
						}
						else
						{

							$('.msg').removeClass('success');
							$('.msg').addClass('error');
							$('.msg').append('<li/>').html('Please retry, enter data');
							$('.msg').fadeIn();
						}


					},

			});

	});

	$('.lbl').click(function(){
		if(!$(this).hasClass('active')){
			$('.lbl').removeClass('active');
			$(this).addClass('active');
		}
	});

	$("#submit_password_w").click(function() {

	if ($('#cPassword_w').val().length < 1 || $('#nPassword_w').val().length < 1 || $('#nPassword2_w').val().length < 1) {
			setError('#edit-profile-box-w .msg', 'Please retry, enter data.');
			return false;
		}
	else{
		$.fancybox.showLoading();

		$.ajax({
			type	: 'POST',
			cache	: false,
			url		: $('#frmChangePassword_w').attr('action'),
			data	: $('#frmChangePassword_w').serializeArray(),
			success	: function(data2) {
			// console.log(data2);
			obj = JSON.parse(data2);
			// console.log(obj.status);

			//console.log(data2);
			if(obj.status == 1 || obj.status == 0)
			{
				setError('#edit-profile-box-w .msg', obj.status_msg);
			return false;
			}
			else if(obj.status == 2)
			{
				setSuccess('#edit-profile-box-w .msg', obj.status_msg);
				
			}

			}

		});
		$.fancybox.hideLoading();
}


	///////////


	return false;
});

$('.pass_change').fancybox({
		maxHeight: 500,
		afterClose  : function() {
		   $("#frmChangePassword_w input[type=password]").val("");
        }
	});
  
  //Report Status Updates
  fn_report_staus_update(); 
  //Report Files
  fn_report_files();
  //Report Market item
  fn_report_market(); 
  
//  $('#hanging [title!=""]').qtip({
//	position: {
//        at: 'center left',
//		my: 'center right'
//    }	
//	});	

//document.ready ends here
function status_photo_upload(){

				var ext = $("#fileUploadFile").val().split('.').pop().toLowerCase();
				var allow = new Array('gif','png','jpg','jpeg');
				// console.log(ext);
				// console.log(allow);
				if($.inArray(ext, allow) == -1) {
				 	setError('#fMsg', 'Only image files are allowed.');
					return false;
				}
				if(IsScript($('#addPhoto').val()))
				{
					setError('#fMsg', 'Please check you inputs');
					return false;
				}	
				
			
			var tag_text='';
			if ($('textarea.tagged_text_photo').exists()){
			$('textarea.tagged_text_photo').textntags('val', function(text) {
				tag_text = text;
			});
			$('textarea.tagged_text_photo').textntags('reset');
			}
			var new_data = tag_text;
			$('#photos').val(new_data);
				
    var options1 = {
                url     : $('#fPhoto').attr('action'),
                type    : $("#fPhoto").attr('method'),
                dataType: 'json',
				  beforeSubmit: function() {
				  	ajax_uploading = true;
				  	$('.ph-loader').addClass('loader');
					progressbox_photo.show(); //show progressbar
					progressbar_photo.width(completed_photo); //initial value 0% of progressbar
					statustxt_photo.html(completed_photo); //set status text
					statustxt_photo.css('color','#000'); //initial color of status text
				  },
				  uploadProgress: function(event, position, total, percentComplete) {
								  //Progress bar
					progressbar_photo.width(percentComplete + '%') //update progressbar percent complete
					statustxt_photo.html(percentComplete + '%'); //update status text
					if(percentComplete>50)
						{
							statustxt_photo.css('color','#fff'); //change status text to white after 50%
						}
				  },
				  complete: function() {
				  	$('.ph-loader').removeClass('loader');
				  	ajax_uploading = false;
					progressbox_photo.hide();
				  },
				  
                  success: function(data) {
				  // console.log(data);
				  //active_form();
                    //var msg = data.msg;

                    if(data.status == 'yes'){
					$("div#recent-posts .no-feed-found").remove();
                    updatePicDiv(data);
      							$("input[type=text], textarea , select ").val("");
								 if($.browser.msie){
									$("#fPhoto input[type='file']").replaceWith($("#fPhoto input[type='file']").clone(true));
								} else {
										$("input[type='file']").val('');
								}
								$('#add_more_photos').html('');
								//$('#add_more_photos').append("<img src="+siteurl+"media/images/imgBrowsePhoto.png" width='72' height='72' alt='Add Photos'> ");
								$('#add_more_photos').append("<img src='"+siteurl+"media/images/imgBrowsePhoto.png' width='72' height='72' alt='Add Photos'>");
								$("#fPhoto input[type= button]").addClass('inactive');
								$("#fPhoto input[type= button]").attr('disabled','disabled');
      							setSuccess('#fMsg', 'Photo Updated');


					/*$("#full-name").focus();

							$("input[type=text], textarea , select ").val("");
							$("input[type=file]").val("");
							$('#career-form .msg').addClass('success');
							$('#career-form .msg').append('<li/>').html('Your application submited successfully!');
							$('#career-form .msg').fadeIn();*/


                    }else{
							if(data.image_size == 'yes')
							{
								setError('#fMsg', 'You can upload image up to 10 MB');
							}
							else {
								setError('#fMsg', 'could not update your status, try again');
						    }
                    }
					
                },
            };
            if(!ajax_uploading)
            {
            	$('#fPhoto').ajaxSubmit(options1);
            }

                return false;
        }

	/*$('.sb-icon-search').bind('click',function(){
	alert('hi');
			//$('#sb-search').addClass('kunal');
			$('#sb-search').removeAttr( "class" );
			$('#sb-search').addClass('kunal');
			console.log('hi');
	});*/
	
});

//Report  status update
function fn_report_staus_update(){
  $("#fn-report-status_update").live("click",function(){
    var message_id = $("#txtmessageid").val();
    var comments = $.trim($("#txtarcomments").val());
    var e = this;
    if(!comments){
      $("#pop-win .error-msg").html("Comments required");
      $("#txtarcomments").focus();
      return false;
    }
    $.ajax({
      url: siteurl+"profile/submit_report_status_update",
      data: {
        "message_id":message_id,
        "comments":comments
      },
      dataType: "json",
      type: "POST", 
      success: function(msg){
        $("#pop-win .error-msg").html(msg.status);
      }
    });
    $("#pop-win .error-msg").html('');
    return false;
  })
}

//Report Files
function fn_report_files(){
  $("#fn-report-files").live("click",function(){
    var file_id = $("#txtfileid").val();
    var comments = $.trim($("#txtarcomments-file").val());
    var e = this;
    if(!comments){
      $("#pop-win-file .error-msg").html("Comments required");
      $("#txtarcomments").focus();
      return false;
    }
    $.ajax({
      url: siteurl+"profile/submit_report_files",
      data: {
        "file_id":file_id,
        "comments":comments
      },
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        $("#pop-win-file .error-msg").html(msg.status);
      }
    });
    //$(document).trigger('close.facebox');
    return false;
  })
}
//Report market item
function fn_report_market(){
  $("#fn-report-item").live("click",function(){
    var item_id = $("#txtmarketid").val();
    var comments = $.trim($("#txtarcomments-market").val());
    var e = this;
    if(!comments){
      $("#pop-win-market .error-msg").html("Comments required");
      $("#txtarcomments").focus();
      return false;
    }
    $.ajax({
      url: siteurl+"profile/submit_report_market",
      data: {
        "item_id":item_id,
        "comments":comments
      },
      async: false,
      dataType: "json",
      type: "POST", 
      success: function(msg){
        $("#pop-win-market .error-msg").html(msg.status);
      }
    });
    //$(document).trigger('close.facebox');
    return false;
  })
}

function active_feedback_form()
{
$.fancybox.hideLoading();
$('#add_feedback').removeClass('inactive');
$('#add_feedback').removeAttr('disabled');

}
function IsScript(sText){
    var filter = /<(?:.|\n)*?>/;
    if (filter.test(sText)) {
        return true;
    }else {
        return false;
    }
}
function IsAlpha(sText){
 var regex = /^[a-zA-Z0-9 ]*$/;
     if (regex.test(sText)) {
        return true;
    }else {
        return false;
    }
}

function IsNumeric(sText){
 var regex = /^[0-9]*$/;
     if (regex.test(sText)) {
        return true;
    }else {
        return false;
    }
}
function inactive_feedback_form()
{
$.fancybox.showLoading();
$('#add_feedback').addClass('inactive');
$('#add_feedback').attr('disabled','disabled');
}

	function postPoll(form)
	{
		if($.trim($(form).find('#addQuestion').val()) == ''|| $.trim($(form).find('#addQuestion').val()) == "What's your question?")
		{
			setError('#fMsg', 'Please fill up your question');
			return false;
		}
		if($(form).find('#addQuestion').val().length < 10)
		{
			setError('#fMsg', 'Please add a long and descriptive question.');
			return false;
		}
		if(IsScript($.trim($('#addQuestion').val())))
		{
			setError('#fMsg', 'Please check you inputs');
			return false;
		}
		var total = 0;
		var invalid = 0;
		$(form).find(' ul.options li input' ).each(function( index ) {
  			total = $.trim($(this).val()) != '' ? total + 1 : total;
			invalid = (IsScript($.trim($(this).val())) || $.trim($(this).val())== 'Add an answer')? invalid + 1 : invalid;
		});
		if(total < 2)
		{
			setError('#fMsg', 'Please fill up atleast two answers');
			return false;
		}
		if(invalid > 0)
		{
			setError('#fMsg', 'Please check you inputs');
			return false;
		}

		var service_url = siteurl+"poll/create";
		$.ajax({
				url:service_url,
				type:'POST',
				dataType:'json',
				data:$(form).serialize(),
				beforeSend:function(){
					$('.ph-loader').addClass('loader');
				},
				success:function(result){
					if(result.status == 'success')
					{
						$("div#recent-posts .no-feed-found").remove();
						//alert('succeeded');
						setSuccess('#fMsg', 'Created Poll successfully');
						//loadUpdates('polls', parent);
						updatePollDiv(result);
						$('#fPoll')[0].reset();
						$("#fPoll input[type= button]").addClass('inactive');
						$("#fPoll input[type= button]").attr('disabled','disabled');
						$("#fPoll .options").html('');
						$("#fPoll .options").append('<li><input type="text" id="addAnswer_1" name="options[]" placeholder="Add an answer" /></li><li><input type="text" id="addAnswer_2" name="options[]" placeholder="Add an answer" /></li>');
						 $('.hp-caption').animate({height: ($('.poll').find('.hp-data').height() + 10) + 'px'}, 'fast');
					} else {
						setError('#fMsg', 'Could not create the poll');
					}
					// console.log('result:'+result);

				},
				complete:function(){
					$('.ph-loader').removeClass('loader');
					if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
					  function resetPlaceholder() {
					      if ($(this).val() === '') {
					        $(this).val($(this).attr('placeholder'))
					          .attr('data-placeholder', true)
					          .addClass('ie-placeholder');
					        if ($(this).is(':password')) {
					          var field = $('<input />');
					          $.each(this.attributes, function (i, attr) {
					            if (attr.name !== 'type') {
					              field.attr(attr.name, attr.value);
					            }
					          });
					          field.attr({
					            'type': 'text',
					            'data-input-password': true,
					            'value': $(this).val()
					          });
					          $(this).replaceWith(field);
					        }
					      }
					    }

					    $('[placeholder]').each(function () {
					      //ie user refresh don't reset input values workaround
					      if ($(this).attr('placeholder') !== '' && $(this).attr('placeholder') === $(this).val()){
					        $(this).val('');
					      }
					      resetPlaceholder.call(this);
					    });
					    $(document).on('focus', '[placeholder]', function () {
					      if ($(this).attr('data-placeholder')) {
					        $(this).val('').removeAttr('data-placeholder').removeClass('ie-placeholder');
					      }
					    }).on('blur', '[placeholder]', function () { resetPlaceholder.call(this); });
					    $(document).on('focus', '[data-input-password]', function () {
					      var field = $('<input />');
					      $.each(this.attributes, function (i, attr) {
						  // console.log(attr);
					        if (attr.name !='type' && attr.name != 'data-placeholder' && attr.name != 'data-input-password') {
					          field.attr(attr.name, attr.value);
					        }
					      });
					      field.attr('type', 'password').on('focus', function () { this.select(); });
					      $(this).replaceWith(field);
					      field.trigger('focus');
					    }); 
					}
				},
				error:function(result){
					setError('#fMsg', 'Could not create the poll');
				}
		});
	}

	function votePoll(form)
	{
		var poll_id = $(form).find('#poll_id').val();
		// console.log(poll_id);
		var option_id = $(form).find('input[type=radio]:checked').val();
		// console.log(option_id);
		if(typeof option_id == 'undefined' || option_id === '')
		{
			alert('Please make a selection');
			return;
		}

		var service_url = siteurl+"poll/vote/" + poll_id + '/' + option_id;
		$.ajax({
				url:service_url,
				type:'GET',
				dataType:'json',
				beforeSend:function(){
					$('.ph-loader').addClass('loader');
				},
				success:function(result){
					if(result.status == 'success')
					{
						$(form).addClass('d-n');
						$('#vPoll_wid_'+poll_id).addClass('d-n');
						$('#vPoll_home_'+poll_id).addClass('d-n');
						$('#vPoll_'+poll_id).addClass('d-n');
						setSuccess('#mPoll', 'You have successfully voted');
						$.each(result.results.options, function(index, item){
							_html	= '<li><label>'+item['title']+'</label><div id="progressBar_'+poll_id+'_'+index+'" class="progressBar"><div></div></div></li>';
							_html_wid	= '<li><label>'+item['title']+'</label><div id="progressBar_wid_'+poll_id+'_'+index+'" class="progressBar"><div></div></div></li>';
							_html_home	= '<li><label>'+item['title']+'</label><div id="progressBar_home_'+poll_id+'_'+index+'" class="progressBar"><div></div></div></li>';
							$('#vPollResponse_'+poll_id).append(_html);
							$('#vPollResponse_wid_'+poll_id).append(_html_wid);
							$('#vPollResponse_home_'+poll_id).append(_html_home);
							progress(item['percentage'], $('#progressBar_'+poll_id+'_'+index));
							progress(item['percentage'], $('#progressBar_home_'+poll_id+'_'+index));
							progress(item['percentage'], $('#progressBar_wid_'+poll_id+'_'+index));
						});

					}
					// console.log('result:'+result);

				},
				complete:function(){
					$('.ph-loader').removeClass('loader');
				},
				error:function(result){
					  // console.log('result:'+result);
				}
		});

	}


	function loadUpdates(type, parent)
	{

		$('#recent-posts div.loader-more').show();
		var service_url = siteurl+ pageurl + "/updates";
		var request_data = {};
    // console.log(service_url);
		//filter_user is used to filter status on user profile page.

		if(type !== '')
			request_data = {'type': type, 'limit': limit, 'start' : start, 'filter_user' : filter_user,'tag':tag_id};
		else
			request_data = {'limit': limit, 'start' : start, 'filter_user' : filter_user,'tag':tag_id};

		//filter by concept
		if (typeof feedtype != 'undefined')
		{
			request_data.feedtype = feedtype;
			request_data.feedtype_id = feedtype_id;
		}


		/*console.log(request_data);
		return;*/

		$.ajax({
			url: service_url,
			data: request_data,
			async: false,
			dataType: "json",
			type: "POST",
			cache: false,
			success: function(response){
			current_time = response.current_time;
			if(response.feed.length == 0 &&  start == 0)
			{
					$('#recent-posts div.loader-more').before('<dl class="block-2 clearfix no-feed-found"><dt><dd><h2>No feed found!</h2></dd></dt></dl>');        
			}
				// console.log(response);
				_.templateSettings.variable = "rc";

		        // Grab the HTML out of our template tag and pre-compile it.
		        var template = _.template(
		            $( "script.template" ).html()
		        );
		        $('#recent-posts ul.filter li').removeClass('active');
		        $(parent).addClass('active');
		        //$('#recent-posts ul.filter').append(template(response));
		        $('#recent-posts div.loader-more').before(template(response));
				/* var dl_class =$("dl.feature:last");				
				if(dl_class.length>0)
				{
				 $(dl_class).after(template(response));		
				}else
					{
					  $('#recent-posts div.loader-more').before(template(response));				
					}  */
				},
			beforeSend: function(){
				loadProgress = true;
				},
			complete: function(response){
				loadProgress = false;
				$('#recent-posts div.loader-more').hide();
				}
			   
  
		});
		$('div.readmore').expander({
                slicePoint: 200,
                expandText: 'See More',
                userCollapseText: ''
            });

	}



//	txtComment

	$.fn.updateAJAXResponse = function(){
		alert("Test");
	}
	function updateDiv(response)
	{

		_.templateSettings.variable = "response";

		var template = _.template(
            $( "#new_status" ).html()
        );
    $('#recent-posts ul.filter').after(template(response));
		$('#recent-posts dl.block-2:first').fadeIn('slow');
		//$('#recent-posts').append('added...');
		$('#txtPost').val('');
		$('div.readmore').expander({
                slicePoint: 200,
                expandText: 'See More',
                userCollapseText: ''
     });
	}

  function updatePicDiv(response)
	{
		_.templateSettings.variable = "response";
		var template = _.template(
            $( "#new_upload_image" ).html()
        );
    	$('#recent-posts ul.filter').after(template(response));
		$('#recent-posts dl.block-2:first').fadeIn('slow');
		$('div.readmore').expander({
                slicePoint: 200,
                expandText: 'See More',
                userCollapseText: ''
     });
	}


	function updatePollDiv(response)
	{

		_.templateSettings.variable = "response";

		var template = _.template(
            $( "#new_status_poll" ).html()
        );


		$('#recent-posts ul.filter').after(template(response));
		$('#recent-posts dl.block-2:first').fadeIn('slow');
		//$('#recent-posts').append('added...');
		$('#txtPost').val('');
	}



	function progress(percent, $element) {
		var progressBarWidth = percent * $element.width() / 100;
		$element.find('div').animate({ width: progressBarWidth }, 500).html(Math.round(Number(percent)) + "%&nbsp;");
	}

	function initMsg(_Obj){
		$(_Obj).removeClass('success');
		$(_Obj).removeClass('error');
		$(_Obj).html('');
		$(_Obj).slideUp('fast');
	};
	function setError(_Obj, msgText){
		initMsg(_Obj);
		$(_Obj).addClass('error');
		$(_Obj).html(msgText);
		$(_Obj).slideDown('fast');
		setTimeout(function(){$(_Obj).slideUp('slow')}, 4000);
	};
	function setSuccess(_Obj, msgText){
		initMsg(_Obj);
		$(_Obj).addClass('success');
		$(_Obj).html(msgText);
		$(_Obj).slideDown('fast');
		setTimeout(function(){$(_Obj).slideUp('slow')}, 4000);
	}

	var _z;
	$.fn.sendAJAX =
		function(aUrl){
      if($.trim($('#txtPost').val()) == '')
			{
				setError('#fMsg', 'Please enter few words to post.');
				return false;
			}
		else if(IsScript($('#txtPost').val()))
		{
			setError('#fMsg', 'Please check you inputs');
			return false;
		}
			_z = $(this);
			aUrl = $(this).attr('action');
			var formdata = (this).serialize();
			var tag_text='';
			if ($('textarea.tagged_text').exists()){
			$('textarea.tagged_text').textntags('val', function(text) {
				tag_text = text;
				$('#tag_text').val(tag_text);
			});
			var formdata = (this).serialize();
			$('textarea.tagged_text').textntags('reset');
			}
		
      $.ajax({
				url:aUrl,
				type:'POST',
				dataType:'json',
				data:formdata,
				beforeSend:function(){
					$('.ph-loader').addClass('loader');
				},
				success:function(result){
					// console.log('result:'+result);
					//$(this).updateAJAXResponse(result);
					if(result.time)
					{
						$("div#recent-posts .no-feed-found").remove();
						updateDiv(result);
						setSuccess('#fMsg', 'Status Updated');
						$("#btnShare").addClass('inactive');
						$("#btnShare").attr('disabled','disabled');
					}
				},
				complete:function(){
					$('.ph-loader').removeClass('loader');
				},
				error:function(result){
					setError('#fMsg', 'could not update your status, try again');
				}
			});
			
	};

	function loadPeople(prop)
	{
		$('.loader-more').show();
		var searched = ($('#txtSearch').length > 0 && $('#txtSearch').val() != '') ? true : false;
		//console.log(searched);
		if(!prop.noclear)
		{
			$('.people-grid').html('');
			$('.people-list .row:not(.head)').remove();
		}


		var service_url = siteurl+"people/people_ajax";

		if(searched)
		{
			var search = $('#txtSearch').val();
			var data = {'limit' : limit, 'start' : start, 'search_term': search};
		} else {
			var concept = prop.concept ? prop.concept : $('#ddConcepts').val();
			var department = $('#ddDepartments').val();
			var country = $('#ddCountry').val();
      var group = prop.group ? prop.group : 0;

			var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country,'group':group};

		}




	     $.ajax({
	      url: service_url,
	      data: data,
	      async: false,
	      dataType: "json",
	      type: "GET",
	      success: function(data){

	      	if(_.size(data) > 0)
	      	{
	      		$('.no-result-block').addClass('d-n');

	      		_.templateSettings.variable = "rc";

		        // Grab the HTML out of our template tag and pre-compile it.
		        var template = _.template(
		            $( "#people_card" ).html()
		        );


		        $('div.people-grid').append(template(data));

		        var template = _.template(
		            $( "#people_list" ).html()
		        );

		        $('div.people-list').append(template(data));

		        if($('.l-list-view').hasClass('active')) {
		        	$('.people-list').css('display', 'table');
		      		$('.people-grid').css('display', 'none');
		        } else {
		        	$('.people-list').css('display', 'none');
		      		$('.people-grid').css('display', 'table');
		        }
		        $('.people-grid-view').removeClass('d-n');

	      	} else {
	      		if(!prop.scroll)
	      		{
	      			$('.no-result-block').text('No Users found for specified criteria');
		      		$('.no-result-block').removeClass('d-n');
		      		$('.people-list .row:not(.head)').remove();
		      		$('.people-list').css('display', 'none');
		      		$('.people-grid').css('display', 'none');
		      		$('.people-grid-view').addClass('d-n');
	      		}



	      	}

			$('.loader-more').hide();
	      }
	    });
	}

	function checkstatus(data) {
		/*
		 * this function is callback for cometchat (getUser) to see user's status
		 * http://www.cometchat.com/documentation/developers-api/javascript-api/getuser
		 * jqcc.cometchat.getUser('USERID','CALLBACKFN');
		 * {id: "24126", n: "parves.shahid", s: "away", m: "dancing", a: "/images/user-images/105x101/24126.jpg"…}
		 */
		 $('input[user_id='+ data.id +']').closest('div.row').find('span.status').addClass(data.s);
	}
	
	function checkstatusmarket(data) {
		/*
		 * this function is callback for cometchat (getUser) to see user's status
		 * http://www.cometchat.com/documentation/developers-api/javascript-api/getuser
		 * jqcc.cometchat.getUser('USERID','CALLBACKFN');
		 * {id: "24126", n: "parves.shahid", s: "away", m: "dancing", a: "/images/user-images/105x101/24126.jpg"…}
		 */
		  if (data.s) {
				// console.log(data.s);
			}
		 $('.market_user_'+data.id).addClass(data.s);
	}
  
  function load_all_comment(obj){
    var user_id = obj.attr('user_id');
    var status_id = obj.attr('status_id');
    var ClosestDiv = obj.closest('dl.block-2');
    var content_type = ClosestDiv.attr('content_type');
    var service_url = siteurl+"status_update/ajax_fetch_comment";
    var request_data = {'status_id':status_id,'user_id':user_id,'content_type':content_type}; 
    $.ajax({
			url: service_url,
			data: request_data,
			async: false,
			dataType: "json",
			type: "POST",
			success: function(response){
          var html = '';
          if(response.status == true)
          {
            $('.cmntlike-'+status_id).remove();
            $.each(response.comments.reverse(),function(index,value){
              html+='<ul class="post-cmntlike cmntlike-'+status_id+'" id="'+value.id+'" object_id="'+value.object_id+'" object_type="User" >';
              html+='<li class="picture"><img src="'+siteurl+value.profile_pic+'" width="25" height="25" alt="icon small"></li>';
              html+='<li class="comnt-info"><a href="'+siteurl+value.reply_username+'">'+value.display_name+'</a> '+value.message;
              html+='<br/><span>'+moment(value.create_dttm+" +0400", "YYYY-MM-DD HH:mm:ss Z").fromNow()+'</span>';
              html+=' &#8226; ';
              html+='<a href="#" class="cmLike">';
              if(value.you_like > 0) {
                html+='Unlike';
              } else {
                html+='Like';
              }
              html+='</a>';
              
              html+='<a  href="#" class="fancybox other_users_comment" >';
              if(value.total_likes > 0) { 
                html+=' -'+value.total_likes;
              }
              html+='</a>';              
              html+='</li>';
              
              if(value.object_id == myprofile.id && value.object_type == 'User') { 
                html+='<li class="hide"><a href="#" class="cDelete">x<div class="hide-tooltip"><span class="text">Remove this comment</span><span class="arrow"><span></div></a></li>';
              } else if ( typeof concept_manager != 'undefined' && concept_manager && value.object_type == 'Concept' ) { 
                html+='<li class="hide"><a href="#" class="cDelete">x<div class="hide-tooltip"><span class="text">Remove this comment</span><span class="arrow"><span></div></a></li>';
              } 
              html+='</li>';
              html+='</ul>';
            });
            $('#'+status_id+' .rp-social-ext-text').after(html);
        }          		
			},
			complete: function(response){
        
			}
		});

    
  }


