<?php
$this->load->model('UserFollow');
$followers = $this->UserFollow->getUserFollowers($profile_id);
if(isset($followers) && !empty($followers)):
?>
	<div class="widget birthday-this-month clearfix">
		<div class="widget-head followers">
			<span class="view-all">&#8226; <a href="#followers-view-all-box" id="bd-view-all" class="fancybox">View All (<?php print count($followers); ?>)</a></span>
			<h3>Followers</h3>
		</div>
		<div class="box shadow-2">
			<ul>
				<?php
				$ii= 0;
				foreach($followers as $follower): ?>
					<li><a href="<?php echo site_url(); ?><?php echo strtolower($follower['reply_username']); ?>" ><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $follower['profile_pic']; ?>" width="25" height="25" alt="Jesse Lander" class="round-c" style="margin-top: 0;" /><?php echo ucwords(strtolower($follower['display_name'])); ?></a></li>
				<?php
				$ii++;
				if($ii == 4)
				{
					break;
				}
				endforeach; ?>
			</ul>
		</div>
	</div>

	<div id="followers-view-all-box" class="bd-all-box fb-container-box d-n">

	<?php foreach($followers as $follower): ?>
		<div class="pop-bd clearfix">
			<a href="<?php echo site_url(); ?><?php echo strtolower($follower['reply_username']); ?>" class="normal-text"><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $follower['profile_pic']; ?>" width="25" height="25" alt="Jesse Lander" class="round-c"/><?php echo ucwords(strtolower($follower['display_name'])); ?></a>
		</div>
		<?php if ($follower !== end($followers)): ?>
        	<hr class="dotted"/>
		<?php endif; ?>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

