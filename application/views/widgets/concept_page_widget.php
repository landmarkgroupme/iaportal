<?php
if(isset($widget) && !empty($widget))
{
?>
	<div class="widget members clearfix">
          	<div class="widget-head useful-links">
			 <span class="view-all">&#8226; <a href="<?php echo $concept['slug']; ?>/sort_widgets" >View All (<?php echo count($widget); ?>)</a></span>
			<h3><?php echo $concept['name'];?> Pages</h3>
		</div>
		<div class="box shadow-2">
			<ul class="widget-page drops">
			<?php  foreach($widgets_data as $key => $widgets ) { ?>
			
					<!-- <li class="ui-state-default"><a ><?php // echo $key; ?></a> -->
                    <ul class="down main-category ">
                    <?php foreach($widgets as $wid_key => $wid ) {
										if(sizeof($wid)) {
										?>
                                        	<li><a ><?php echo $wid_key.' ('.sizeof($wid).')'; ?></a>
                                            	<ul class="down">
												<?php foreach ($wid as $link) { ?>
												<li><a href= "<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>/pages/<?php echo $link->id; ?>" ><?php echo ucwords(strtolower(stripcslashes($link->name))); ?></a></li>
												<?php } ?>
                                                </ul>
                                            </li> 
										<?php 
										}
										} ?>	
                                        </ul>
                                    <!-- </li>           -->
			<?php  } ?>						
			</ul>
		</div>
	</div>
		
	   
<?php } ?>