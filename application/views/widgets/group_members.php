<div class="widget members clearfix">
          	<div class="widget-head group-members">
                <span class="view-all">&#8226; <a href="<?php echo site_url(); ?>people">Search People</a></span>
                <h3>Members</h3>
            </div>
            <div class="box shadow-2">

				<div class="cp-followers-bx clearfix">
                	<div class="cp-followers">
						<a href="#people_popup" class="load_members">All (<?php echo $group['total_members']; ?>)</a>
                    	<div class="imgMembers">
                    		<?php foreach ($group['members'] as $member) { ?>
                    		<a href="http://devnet.landmarkgroup.com/<?php echo $member['reply_username']; ?>" title="<?php echo $member['fullname']; ?>"><img src="http://devnet.landmarkgroup.com/images/user-images/105x101/<?php echo $member['profile_pic']; ?>" alt="<?php echo $member['fullname']; ?>"></a>
                    		<?php } ?>
                        </div>
                    </div>
                    <div class="cp-btn-follow"></div>
                </div>


				<?php if($group['managers_count']['managers'] > 0)
				{
				?>
				<hr style="border-color: #eaeaea; margin-top: 0; margin-bottom: 5px;" />
				<div class="cp-followers-bx clearfix">
                	<div class="cp-followers">
						<h4 style="color:#454545;">Moderators (<?php echo $group['managers_count']['managers']; ?>)</h4>
                    	<div class="imgMembers">
                        <?php foreach ($group['managers'] as $manager) { ?>
                    		<a href="http://devnet.landmarkgroup.com/<?php echo $manager['reply_username']; ?>" title="<?php echo $manager['display_name']; ?>"><img src="http://devnet.landmarkgroup.com/images/user-images/105x101/<?php echo $manager['profile_pic']; ?>" alt="<?php echo $manager['display_name']; ?>"></a>
                    		<?php } ?>
                        </div>
                    </div>
                    <div class="cp-btn-follow"></div>
                </div>
				<?php }?>
          </div>
      </div>