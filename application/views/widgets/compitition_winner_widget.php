<?php

$this->load->model('User');
//$winners = $this->User->getAllUsers($myprofile['id'],3,0);
//https://intranet.landmarkgroup.com/manish.chauhan
$winner_id = 24265; 
$winners = $this->User->getUserProfile($winner_id);
if(isset($winners) && !empty($winners)):
?>
<div class="widget competition clearfix">
	<div class="widget-head comp-winner">
		<h3>Corporate quiz Competition</h3>
	</div>
	<div class="box shadow-2">
	  <ul>
		  <li><a href="<?php echo site_url(); ?><?php echo $winners['reply_username']; ?>">
			  <img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $winners['profile_pic'];?>" alt="<?php echo $winners['fullname'];?>" class="round-c"/>
			  <span>Congratulate<br><b><?php echo $winners['fullname'];?></b> for winning this competition</span>
		  </a></li>
	  </ul>
  </div>
</div>
<?php endif; ?>