<div class="widget members clearfix">
    <div class="widget-head message">
        
        <h3><?php echo $title; ?></h3>
    </div>
    <div class="box shadow-2">
        <p><?php echo $message; ?></p>
    </div>
</div>