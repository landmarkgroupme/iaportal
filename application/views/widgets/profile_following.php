<?php
$this->load->model('UserFollow');
$followings = $this->UserFollow->getUserFollowings($profile_id);
if(isset($followings) && !empty($followings)):
?>
	<div class="widget birthday-this-month clearfix">
		<div class="widget-head following">
			<span class="view-all">&#8226; <a href="#followings-view-all-box" id="bd-view-all" class="fancybox">View All (<?php print count($followings); ?>)</a></span>
			<h3>Following</h3>
		</div>
		<div class="box shadow-2">
			<ul>
				<?php
				$i= 0;
				foreach($followings as $following): ?>
					<li><a href="<?php echo site_url(); ?><?php echo strtolower($following['reply_username']); ?>" ><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $following['profile_pic']; ?>" width="25" height="25" alt="Jesse Lander" class="round-c" style="margin-top: 0;"/><?php echo ucwords(strtolower($following['display_name'])); ?></a></li>
				<?php
				$i++;
				if($i == 4)
				{
					break;
				}
				endforeach;
				?>
			</ul>
		</div>
	</div>

	<div id="followings-view-all-box" class="bd-all-box fb-container-box d-n">

	<?php foreach($followings as $following): ?>
		<div class="pop-bd clearfix">
			<a href="<?php echo site_url(); ?><?php echo strtolower($following['reply_username']); ?>" class="normal-text"><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $following['profile_pic']; ?>" width="25" height="25" alt="Jesse Lander" class="round-c"/><?php echo ucwords(strtolower($following['display_name'])); ?></a>
		</div>
		<?php if ($following !== end($followings)): ?>
        	<hr class="dotted"/>
		<?php endif; ?>
	<?php endforeach; ?>
	</div>
<?php endif; ?>
