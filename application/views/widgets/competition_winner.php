<?php
$this->load->model('User');
$winner_ids = array(21922, 24483);
foreach($winner_ids as $winner_id){
$winners = $this->User->getUserProfile($winner_id);
$widget_title = 'Home Centre Selfie competition';
if(isset($winners) && !empty($winners)):
?>
<div class="widget competition clearfix">
<div class="widget-head comp-winner">
		<h3><?php echo $widget_title; ?></h3>
	</div>
            <div class="box shadow-2">
              <ul>
                  <li><a href="<?php echo site_url(); ?><?php echo $winners['reply_username']; ?>">
                      <img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $winners['profile_pic'];?>" alt="<?php echo $winners['fullname'];?>" class="round-c"/>
                      <span>Congratulate <br><b><?php echo ucwords(strtolower($winners['fullname']));?></b> on winning 500 AED in vouchers</span>
                  </a></li>
              </ul>
          </div>
        </div>

<?php endif; 
}
?>