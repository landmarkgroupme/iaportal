<div class="widget members clearfix">
	<div class="widget-head useful-links group-document">
    <h3><?php print ucfirst($group['name']);?> Documents</h3>
  </div>
  <div class="box shadow-2">
      <ul>
        <li><a href="<?php print base_url(); ?>groups/<?php print $group['id'];?>/files">Documents</a></li>        
      </ul>
  </div>
</div>