<?php
if(isset($birthdays) && !empty($birthdays)):
?>
	<div class="widget birthday-this-month clearfix">
		<div class="widget-head birthday">
			<span class="view-all">&#8226; <a href="#bd-view-all-box" id="bd-view-all" class="fancybox">View All</a></span>
			<h3>Birthdays today</h3>
		</div>
		<div class="box shadow-2">
			<ul>
				<?php foreach($birthdays as $birthday): ?>
					<li><a href="<?php echo site_url(); ?><?php echo strtolower($birthday->reply_username); ?>" ><?php echo ucwords(strtolower(stripcslashes($birthday->fullname))); ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>

	<div id="bd-view-all-box" class="bd-all-box fb-container-box d-n">

	<?php foreach($birthdays as $birthday): ?>
		<div class="pop-bd clearfix">
			<a href="<?php echo site_url(); ?><?php echo strtolower($birthday->reply_username); ?>" class="normal-text"><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $birthday->profile_pic; ?>" width="25" height="25" alt="Jesse Lander" class="round-c"/><?php echo ucwords(strtolower(stripcslashes($birthday->fullname))); ?></a>
		</div>
		<?php if ($birthday !== end($birthdays)): ?>
        	<hr class="dotted"/>
		<?php endif; ?>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

