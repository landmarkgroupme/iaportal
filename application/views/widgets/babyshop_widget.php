<div class="widget announcements clearfix news">
	<div class="widget-head news cookbook">
		<h3>Mom's Little Secret : An Informative Guide Offering Expert Advice to First-time Mothers</h3>
	</div>
	<div class="box shadow-2" style="text-align: center;">
	  <ul>
		  <li><a href="#babyshop_mom_secret" class="mom_secret">
			  <img src="<?php echo site_url(); ?>images/concepts/Babyshop-220-x-220.jpg" alt="Mom's Little Secret : An Informative Guide Offering Expert Advice to First-time Mothers" class="" style="max-width: 100%;"/>
			  </a>
			 <span> Please click here to download the <a href="https://intranet.landmarkgroup.com/files/files_download/b91d8dfe663abe8a024331713268efb7">English</a> and <a href="https://intranet.landmarkgroup.com/files/files_download/b91d8dfe663abe8a024331713268efb7">Arabic. </a></span>
		  </li>
	  </ul>
  </div>
</div>


<div id="babyshop_mom_secret" class="fb-container-box d-n">
<iframe frameborder="0" height="600" scrolling="no" src="http://beta.babyshopstores.com/sites/default/files/moms-little-secret/index.html" width="100%"></iframe>
</div>