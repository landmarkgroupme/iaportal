<div id="people_popup" class="d-n">

	<h3>PEOPLE </h3>

	<div class="no-result-block d-n">No Users found.</div>

		<div class="people-grid-view clearfix">
			<ul class="view">
				<li class="l-grid-view"><a id="l-grid-view" href="#" title="Grid View">&nbsp;</a></li>
				<li class="l-list-view active"><a id="l-list-view" href="#" title="List View">&nbsp;</a></li>
			</ul>
		</div>

		<div class="table people-list">

			<div class="row head">
				<div class="col1">
					<ul class="basic-info">
						<li> &nbsp; </li>
					</ul>
				</div>
				<div class="col2">
					<ul class="contact-info">
						<li>Board / Mobile</li>
						<li class="ext">Ext. No.</li>
						<li>&nbsp;</li>
					</ul>
				</div>
			</div>
			<!-- ajax based people list data loads here -->
		</div>

    	<div class="people-grid clearfix d-n">
			<!-- ajax based people card data loads here -->
    	</div>

</div>