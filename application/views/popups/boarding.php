<?php if($boarding) { ?>
<div class="onboarding-main" id="onboarding-main">
        <h2 class="onboarding">Welcome to the <span>Intranet 2.0</span></h2>
			<div class="profile-block clearfix">
				<div class="p-lft">
					<img class="avatar-big shadow-3" src="images/user-images/105x101/<?php echo $profile['profile_pic'];?>" alt="Avatar" /><br/>
		<?php echo form_open_multipart('profile/upload_picture',array('id'=>'upload_picture'));?>
			<input id="fileUploadPicture" name="fileUploadPicture" type="file" style="display: none;" />
		</form>
					<div class="block-edit"><a id="add_edit_photo" class="editPicture iconPhoto d-n" href="#">&nbsp;</a></div>
	
					<div class="progress-c">
					Profile Completeness:
					<div id="progressBar"><div></div></div>
				</div>
	
				</div>
				<div class="p-rgt profile-intro">
					<!-- <div class="block-edit"><a class="btn-sm btn-gry icon edit editProfile fancybox" href="#edit-profile-box" data-type="edit_profile">Edit Profile</a></div> -->
					<div class="block-edit"><a class="btn-sm btn-gry icon edit editProfile" href="<?php echo $profile['reply_username'];?>" data-type="edit_profile">Edit Profile</a></div>
					<h1><?php echo $profile['fullname'];?></h1>
					<span><?php echo $profile['designation']; ?>, <?php echo $profile['concept']; ?></span>
					<ul>
					<?php if($profile['mobile'] != ''): ?>
                  	<li class="iconMobile"> <a href=""><?php echo $profile['mobile']; ?></a></li>
                  	<?php endif;
                  	if($profile['phone'] != ''):
                  	?>
                    <li class="iconTelephone">&nbsp;<?php echo $profile['phone']; ?></li>
                	<?php endif; ?>
						<li><a href="mailto:<?php echo $profile['email'];?>"><?php echo $profile['email'];?></a></li>
					</ul>
				</div>
			</div>
			<!--<div class="intro-video clearfix">
			    <video id="video1" width="640" height="360" controls>
                  <source src="images/1.mp4" type="video/mp4">
                  <source src="images/1.ogv" type="video/ogg">
                  Your browser does not support HTML5 video.
                </video>
			</div>-->
			<div class="support-blocks clearfix">
				<div class="known-people">
                	<a href="#" class="left"></a>
                    <a href="#" class="right"></a>
				    <h2>Follow Leaders &amp; Peers</h2>
					<ul class="people">
					<?php foreach($boarding_people as $people)  { ?>
					    <li>
						    <img class="avatar" src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $people['profile_pic'];?>" alt="<?php echo $people['fullname'];?>" border="0" />
							<span><?php echo ucwords($people['fullname']);?></span>
					<?php if($people['follow_user'] > 0) { ?>
						<input type="button" class="btn-sm btn-follow btn-grn" name="submit" value="Following" user_id="<?php echo  $people['id']; ?>"/>
						<?php } else { ?>
						<input type="button" class="btn-sm btn-follow" name="submit" value="Follow" user_id="<?php echo  $people['id']; ?>"/>
					<?php } ?>
						</li>
					<? } ?>	
					</ul>
				</div>
				<div class="find-more"><a href="people" class="btn-sm btn-grn" name="btnFollow">FIND MORE</a></div>
			</div>
			<div class="support-blocks divider clearfix">
			    <div class="concepts-groups">
				    <div class="follow-concepts">
					    <h2>Follow Concepts</h2>
						<ul>
						    <li class="first-child">
							<?php if(isset($concepts[0])){ ?>
							    <div class="follow-block">
								    <img class="avatar" src="images/concepts/thumbs/<?php echo $concepts[0]['thumbnail'];?>" border="0" alt="<?php echo $concepts[0]['name'];?>">
									<span><?php echo $concepts[0]['name'];?></span>
								</div>
								<span class="members"><?php echo $concepts[0]['total_members'];?> members</span>
								<div class="cp-btn-follow"><input type="button" class="btn-sm btn-board-concept-follow" object_type="Concept" object_id="<?php echo $concepts[0]['id']; ?>" name="btnFollow" value="Follow" /></div>
								<?php } ?>
							</li>
						    <li class="last-child">
							<?php if(isset($concepts[1])){ ?>
							    <div class="follow-block">
								    <img class="avatar" src="images/concepts/thumbs/<?php echo $concepts[1]['thumbnail'];?>" border="0" alt="<?php echo $concepts[1]['name'];?>">
									<span><?php echo $concepts[1]['name'];?></span>
								</div>
								<span class="members"><?php echo $concepts[1]['total_members'];?> members</span>
								<div class="cp-btn-follow"><input type="button" class="btn-sm btn-board-concept-follow" object_type="Concept" object_id="<?php echo $concepts[1]['id']; ?>" name="btnFollow" value="Follow" /></div>
								<?php } ?>
							</li>
						</ul>
					</div>
					<div class="find-more">
						<div class="find-more"><a href="concepts" class="btn-sm btn-grn" name="btnFollow">FIND MORE</a></div>
					</div>
				</div>
			    <div class="concepts-groups">
				    <div class="join-groups">
					    <h2>Join Groups</h2>
						<ul>
						    <li class="first-child">
							<?php if(isset($groups[0])){ ?>
							    <div class="follow-block">
								    <img class="avatar" src="images/groups/thumbs/<?php echo $groups[0]['thumbnail'];?>" border="0" alt="<?php echo $groups[0]['name'];?>">
									<span><?php echo $groups[0]['name'];?></span>
								</div>
								<span class="members"><?php echo $groups[0]['total_members'];?> members</span>
								<div class="cp-btn-follow"><input type="button" class="btn-sm btn-board-group-follow" object_type="Group" object_id="<?php echo $groups[0]['id']; ?> name="btnFollow" value="Join" /></div>
								<?php } ?>
							</li>
						    <li class="last-child">
							<?php if(isset($groups[1])){ ?>
							    <div class="follow-block">
								    <img class="avatar" src="images/groups/thumbs/<?php echo $groups[1]['thumbnail'];?>" border="0" alt="<?php echo $groups[1]['name'];?>">
									<span><?php echo $groups[1]['name'];?></span>
								</div>
								<span class="members"><?php echo $groups[1]['total_members'];?> members</span>
								<div class="cp-btn-follow"><input type="button" class="btn-sm btn-board-group-follow" object_type="Group" object_id="<?php echo $groups[1]['id']; ?> name="btnFollow" value="Join" /></div>
								<?php } ?>
							</li>
						</ul>
					</div>
					<div class="find-more">
						<div class="find-more"><a href="groups" class="btn-sm btn-grn" name="btnFollow">FIND MORE</a></div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>