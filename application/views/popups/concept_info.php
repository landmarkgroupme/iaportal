<?php
$attributes = array('class' => '', 'id' => 'frm_add_item');
?>
<div id="upload_file" class="fb-container-box d-n">
<h3>Upload File</h3>
<ul class="msg d-n"></ul>
	<!--<form action="<?//=base_url();?>files/submit_share_files"  id="frm_add_item" enctype="multipart/form-data" method="post"> -->
	<?php echo form_open_multipart('files/submit_share_files',$attributes);?>
	<div class="form-fields">

	<dl class="cols-two">
		<dd><label for="cmb_cat">Category<span class="required">*</span></label> <span class="error-msg" id="error-cmb-cat"> </span><br />
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="cmb_cat" id="cmb_cat">
					<option value="0">Select a category</option>
					<?php	foreach($file_categories as $db_data):?>
					<option value="<?php echo $db_data['id']; ?>"><?php echo $db_data['name'];?></option>
					<?php endforeach?>
					</select>
				</label>
			</div>
		</div>
		</dd>
	</dl>
	<dl class="files-box-holder">
		<dd class="files-box patt-2">
			<label for="file_share">Upload a File<span class="required">*</span></label>
			<span id="error-files_upload" class="error-msg"></span>

			<div class="block" id="share-file" style="display: block;">
				<div id="upload-image">
				<input type="file" size="71" id="file_share" name="file_share"/>
					<!-- <div class="textC">
						<input type="file" size="71" id="file_share" name="file_share" class="d-n"/>
						<input type="text" id="upload_file_txt" placeholder="Max filesize 100MB" />
					</div> -->
				</div>
			</div>
		</dd>
		<div id="progressbox" style="display:none;"><div id="fileprogressbar"></div ><div id="statustxt">0%</div></div>
		<!-- <dd class="patt-or">
			<label for="txt_link" id="share-file-link" style="display: inline;">-- OR --</label>
		</dd>
		<dd class="patt-3">
				<div id="share-link" style="display: block;">
					<div class="block">
						<label for="txt_link">Share a link to the files</label>
						<span class="sub-text"> eg. http://www.yourdomain.com/folder_path/file_name.doc</span><br />
						<div class="textC"><input type="text"  id="txt_link" name="txt_link" placeholder="Share a link" /></div>
					</div>
				</div>
		</dd> -->
		<dd>
			<div class="block">
				<label for="txt_title">Title<span class="required">*</span></label>
				<span id="error-txt_title" class="error-msg"></span><br>
				<div class="textC"><input type="text" id="txt_title11" name="txt_title" placeholder="Title"/></div>
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txtar_description">Description </label>
				<span id="error-txtar_description" class="error-msg"></span><br>
				<div class="textC"><textarea cols="55" rows="10" id="txtar_description" name="txtar_description" placeholder="Enter File Description"></textarea></div>
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txt_tags">Tags </label><span class="sub-text">Add descriptive words about the content, marketing, HR, public relations. </span>
				<span id="error-txt_tags" class="error-msg"></span><br />
				<div class="textC"><input type="text" id="txt_tags" name="txt_tags" class="txt_title ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" placeholder="Start typing..." /></div>
			</div>
		</dd>
		<dd>
			<input type="hidden" value="1" name="hd_submit_form">
			<input type="hidden" value="<?php echo $concept['id'];?>" name="file_concept">
			<div class="submit-div">
				<span id="progress-status"></span>
        <p>Files in this section can be seen by the public. Send via chat, or create a private group to enable private sharing</p>
				<input type="button" id="bt_add_item" class="btn-sm edit fancybox" value="Upload File" />
				<input type="button" id="cancel_file" class="btn-sm" value="Cancel" />
			</div>
		</dd>
	</dl>
</div>

</form>
</div>



<div id="upload_concept_info" class="fb-container-box d-n">
<h3>Update Concept Info</h3>
<ul class="msg d-n"></ul>
<?php 
$this->load->helper('html');
$selected = 'selected = "selected"';
$attributes1 = array('class' => '', 'id' => 'frm_edit_concept');
if(isset($concept['id']))
	{
	$image_properties = array(
          'src' => site_url().'images/concepts/thumbs/'.$concept['thumbnail'],
          'alt' => $concept['name'],
          'class' => 'post_images',
          'width' => '100',
          'height' => '100',
          'title' => $concept['name'],
          'rel' => 'lightbox',
		  );
		  
	}
	
echo form_open_multipart('manage/post_concept_ajax',$attributes1);?>
<div class="form-fields">
<dl class="files-box-holder">
		<dd>
      <input type="hidden" name="entry_category" id="entry_category" value="concept" />
	  <input name="concept_id" id="concept_id" type="hidden" value="<?=isset($concept['id'])?$concept['id']:'';?>" />
		<div class="block">
			
			
			<label for="txt_title_concept">Title<span class="required">*</span></label>
			<span id="error-txt_title_concept" class="error-msg"></span><br>
			<div class="textC">
				<input type="text" name="txt_title" id="txt_title_concept" class="form-text" value="<?=isset($concept['name'])?$concept['name']:'';?>" placeholder="Title" />  
			</div>
		</div>
		</dd>
		<dd>
		<div class="block">
			
			
			<label for="txt_email_concept">Email</label>
			<span id="error-txt_email_concept" class="error-msg"></span><br>
			<div class="textC">
				<input type="text" name="txt_email" id="txt_email_concept" class="form-text" value="<?=isset($concept['email'])?$concept['email']:'';?>" placeholder="Email" />  
			</div>
		</div>
		</dd>
		<dd>
		<div class="block">
			
			
			<label for="txt_phone_concept">Phone</label>
			<span id="error-txt_phone_concept" class="error-msg"></span><br>
			<div class="textC">
				<input type="text" name="txt_phone" id="txt_phone_concept" class="form-text" value="<?=isset($concept['phone'])?$concept['phone']:'';?>" placeholder="Phone Number" />  
			</div>
		</div>
		</dd>
		<dd>
       <div class="form-title">thumbnail <span id="userfile" class="error-msg"></span></div>
		<?php
		if(isset($concept['id']))
		{ 
			echo img($image_properties);
		}
		?>
		<input type="file" name="userfile" id= "userfile" size="20" />
				</dd>

		<dd>
		<div class="block">
		<label for="txt_body_concept">Body</label>
			<span id="error-txt_body_concept" class="error-msg"></span><br>
			<div class="textC">
        <textarea rows="12" name="txt_body" id="txt_body" class="form-text" placeholder="Description"><?=isset($concept['description'])?$concept['description']:'';?></textarea>   
</div>		
        
        </div>
		</dd>
		<!--
		<dl class="cols-two">
		<dd><label for="txt_status">Status<span class="required">*</span></label><span class="error-msg" id="error-cmb_status"></span><br />
		<div class="block">
			<div class="selectC">
			
      <label class="custom-select">
          <select id="txt_status" name="txt_status" class="form-text-dropdown">
          <option value="">Select</option>
			<option value="active" <? // =( isset($concept['status']) && $concept['status'] == 'active')?$selected:'';?>>Active</option>
			<option value="inactive" <? // =(isset($concept['status']) && $concept['status']=='inactive')?$selected:'';?>>Inactive</option>
          </select>
		  </label>
        </div>
		</div>
		</dd>
		</dl>
		<dl class="cols-two">
		
		<dd><label for="txt_open">Is Open<span class="required">*</span></label><span class="error-msg" id="error-cmb_status"></span><br />
		<div class="block">
			<div class="selectC">
			
      <label class="custom-select">
          <select id="txt_open" name="txt_open" class="form-text-dropdown">
          <option value="">Select</option>
			<option value="yes" <?  // =(isset($concept['is_open']) && $concept['is_open'] == 'yes')?$selected:'';?>>Yes</option>
			<option value="no" <? // =(isset($concept['is_open']) && $concept['is_open'] == 'no')? $selected:'';?>>No</option>
          </select>
		  </label>
        </div>
		</div>
		</dd>
		</dl>
		-->
		
        <input name="txt_publish" id="txt_publish" type="hidden" value="1" />
		
        <div class="clear"></div>
         
            
      <div class="button-holder">
        <input type="button" id="cancel_concept" class="btn-sm" value="Cancel" />
        <div style="float:right;">
	
          <input id="update_concept" class="btn-sm" name="save" type="button" value="Update" />
		
        </div>
		<div class="clear"></div>
      </div>
	  </dl>
	 </div>
    </form>
</div>

<?php
$attributes1 = array('class' => '', 'id' => 'frm_add_widget');
?>
<div id="add_document" class="fb-container-box d-n">
<h3>Add Pages</h3>
<ul class="msg d-n"></ul>
	<!--<form action="<?//=base_url();?>files/submit_share_files"  id="frm_add_item" enctype="multipart/form-data" method="post"> -->
	<?php echo form_open_multipart('widgets/ajax_submit_widget',$attributes1);?>
	<div class="form-fields">

	
	<dl class="files-box-holder">
		<dd>
			<div class="block">
				<label for="txt_title">Title<span class="required">*</span></label>
				<span id="error-txt-title" class="error-msg"></span><br>
				<div class="textC"><input type="text" id="txt_title_widget" name="txt_title" placeholder="Title" /></div>
			</div>
		</dd>
		<dl class="cols-two">
		<dd><label for="cmb_cat">Category<span class="required">*</span></label> <span class="error-msg" id="error-cmb-cat-widget"> </span><br />
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="cmb_cat" id="cmb_cat_widget">
					<option value="0">Select a category</option>
					<?php	foreach($widget_select as $categories_select):?>
					 <optgroup label="<?php echo $categories_select['name'];?>">
					 <?php	foreach($categories_select['option'] as $sub):?>
					<option value="<?php echo $sub['id']; ?>"><?php echo $sub['name'];?></option>
					<?php endforeach?>
					 </optgroup>
					<?php endforeach?>
					</select>
				</label>
			</div>
		</div>
		</dd>
	</dl>
		<dd>
			<div class="block">
				<label for="txtar_description">Description </label>
				<span id="error-txtar-description" class="error-msg"></span><br>
				<div class="textC"><textarea cols="55" rows="10" id="txtar_descr" style="width:600px;" name="txtar_descr"></textarea></div>
			</div>
		</dd>
		
		<dd>
			<input type="hidden" value="<?php echo $concept['id']; ?>" name="object_id">
			<div class="submit-div">
				<span id="progress-status"></span>
				<input type="button" id="add_widget" class="btn-sm edit fancybox" value="Submit" />
				<input type="button" id="cancel_widget" class="btn-sm" value="Cancel" />
			</div>
		</dd>
	</dl>
</div>

</form>
</div>

<?php
$attributes1 = array('class' => '', 'id' => 'frm_edit_widget');
?>
<div id="update_document" class="fb-container-box d-n">
<h3>Add Pages</h3>
<ul class="msg d-n"></ul>
	<!--<form action="<?//=base_url();?>files/submit_share_files"  id="frm_add_item" enctype="multipart/form-data" method="post"> -->
	<?php echo form_open_multipart('widgets/ajax_update_widget',$attributes1);?>
	<div class="form-fields">

	
	<dl class="files-box-holder">
		<dd>
			<div class="block">
				<label for="txt_title">Title<span class="required">*</span></label>
				<span id="error-txt-title-update" class="error-msg"></span><br>
				<div class="textC">
				<input type="text" id="txt_title_widget_update" name="txt_title" placeholder="Title" />
				</div>
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txtar_description">Description </label>
				<span id="error-txtar-descr-update" class="error-msg"></span><br>
				<div class="textC"><textarea cols="55" rows="10" id="txtar_descr_update" style="width:600px;" name="txtar_descr" ></textarea></div>
			</div>
		</dd>
		
		
	</dl>
	<dl class="cols-two">
	<dd><label for="status">status<span class="required">*</span></label><span class="error-msg" id="error-cmb_cat"></span><br>
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="status" id="status">
					<option value="0">Hide</option>
					<option value="1">Show</option>
					</select>
				</label>
			</div>
		</div>
		</dd>
		
		</dl>
		<dl class="files-box-holder">
		<dd>
			<input type="hidden" value="<?php echo $concept['id']; ?>" name="object_id">
			<input type="hidden" value="" name="widget_id" id="widget_id">
			<div class="submit-div">
				<span id="progress-status"></span>
				<input type="button" id="update_widget" class="btn-sm edit fancybox" value="Update" />
				<input type="button" id="cancel_widget_update" class="btn-sm" value="Cancel" />
			</div>
		</dd>
		</dl>
</div>

</form>
</div>