<?php
$this->load->helper('html');
$attributes = array('class' => '', 'id' => 'frm_add_group');
?> <div class="fb-container-box d-n" id="group_create" >
		  <h3 id="form_popup_title">Create Group</h3>
		  <ul class="msg d-n"></ul>
      <?php	echo form_open_multipart('manage/post_group_ajax',$attributes);?>
      <div class="form-fields">
      	<dl>
					<dd>
							<label for="txt_title">Group Name </label><span class="error-msg" id="error-txt_title"><?= form_error('txt_title') ?></span><br />
							<div class="textC">
								<input class="txt_title" type="text" name="txt_title" id="txt_title" placeholder="Group Name" />
							</div>
					</dd>
				</dl>
				<dl>
					<dd>
						<label for="txt_thumbnail">Thumbnail </label><span class="error-msg" id="error-txt_thumbnail"><?= form_error('txt_thumbnail') ?></span><br />
							<div class="textC">
                <div id="render_thumbnail"></div>
                <input type="file" name="txt_thumbnail" id="txt_thumbnail" class="txt-item-desc" />
							</div>
					</dd>
				</dl>
        <dl>
					<dd>
						<label for="txt_body">Description </label><span class="error-msg" id="error-txt_body"><?= form_error('txt_body') ?></span><br />
							<div class="textC">
								<textarea rows="12" name="txt_body" id="txt_body" class="form-text"></textarea>
							</div>
					</dd>
				</dl>
        <dl>
					<dd>
						<label for="txt_open">Group Type </label><span class="error-msg" id="error-txt_open"><?= form_error('txt_open') ?></span><br />
							<div class="selectC">
                <select id="txt_open" name="txt_open" class="form-text-dropdown">
                  <option value="">Select</option>
                  <option value="yes">Public</option>
                  <option value="no">Private</option>
                </select>
							</div>
					</dd>
				</dl>
        <dl>
					<dd>
						<input type="button" name="bt_add_group" id="bt_add_group" class="btn-sm" value="Create a Group" /> &nbsp;
						<input type="button" id="cancel" class="btn-sm" value="Cancel" onclick="$.fancybox.close();"/>
				    <input type="hidden" name="hd_submit_form" value="1" />
            <input type="hidden" name="group_id" value="" id="group_id" />
            <input type="hidden" name="txt_status" value="1" />
					</dd>
				</dl>
		</form>
	</div>
</div>