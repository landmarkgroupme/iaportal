<div id="upload_file-box" class="fb-container-box d-n">
	<form action="<? site_url("manage/createnews") ?>" enctype="multipart/form-data" method="post" id="frm_post" class="intranet_form">
		<input type="hidden" id="base_url" name="base_url" value="<?php echo site_url(); ?>" />
		<input type="hidden" id="concept_id" name="concept_id" value="<?php echo $concept['id']; ?>" />
		<div id="create_content" class="main-table form" style="width:700px;">
			<div id="content-type-selector">
			<p class='content-type'><strong>Select a content type:</strong></p>
			<ul id="content-type-selection">
			<li><input class="post_content_type" type="radio" name="entry_category" id="category_news" value="news" checked /> <label for="category_news">News</label></li>
			<li><input class="post_content_type" type="radio" name="entry_category" id="category_announcements" value="announcement" /> <label for="category_announcements">Announcements</label></li>
			<li><input class="post_content_type"  type="radio" name="entry_category" id="category_photos" value="photos" /> <label for="category_photos">Photos album</label></li>
			<li><input  class="post_content_type"  type="radio" name="entry_category" id="category_offers" value="offers" /> <label for="category_offers">Offers</label></li>

			</ul>
			</div>
			<div id="content_post_form" >
				<p id="selected-content-type">News</p>
				<div id ="title-box" class="custom-form-element" >
					<div class="counter">Characters left: <span id="charsLeft"></span></div>
					<div class="form-title">Title <span id="error-title" class="error-msg"></span></div>
					<input type="text" name="txt_title" id="txt_title" class="form-text" style="width:670px;" value="" />
				</div>
				<div id="excerpt-box" class="custom-form-element">
					<div class="counter">Characters left: <span id="charsLeft2"></span></div>
					<div class="form-title">Excerpt <span id="error-excerpt" class="error-msg"></span></div>
					<textarea rows="6" name="txt_excerpt" id="txt_excerpt" style="width:670px;" class="form-text"></textarea>
				</div>
				<div id="body-box" class="custom-form-element">
					<div class="form-title">Body <span id="error-body" class="error-msg"></span></div><br/>
					<div class="txt_body_holder"><textarea rows="12" style="width:682px;" name="txt_body" id="txt_body" ></textarea></div>
					<div style="color:#5e5e5e;margin-top:5px;" class="media-hide">Maximum image width: 560 pixels and maximum file size: 70kb</div>
					<div style="display:none;" id="offer-info" class="custom-form-element" style="margin-top:10px;" class="offer-page"  >NOTE: The offer body will be center-aligned in the offer's page</div>
				</div>

				<div id="alb-body-box" class="custom-form-element" style="display:none;">
					<div class="form-title">Description <span id="error-alb-body" class="error-msg"></span></div>
					<div class="txt_body_holder"><textarea rows="3" style="width:670px;" name="alb_txt_body" id="alb_txt_body" ></textarea></div>
				</div>

				<div id="event-body-box" class="custom-form-element" style="display:none;">
					<div class="form-title">BODY <span id="error-event-body" class="error-msg"></span></div>
					<div class="txt_body_holder"><textarea rows="6" style="width:670px;" name="event_txt_body" id="event_txt_body" ></textarea></div>
				</div>
        <div id="album-box" class="custom-form-element">
					<div class="form-title">Linked Album</div>
					<div class="form-area">
						<select id="txt_album" name="txt_album" class="form-text-dropdown">
						<option value="0">- Select an existing album -</option>
						<?php
						if (count($albums)):
						foreach ($albums as $details):
						$ids = $details['aid'];
						$name = $details['title'];
						?>
						<option value="<?=$ids?>"><?=$name?></option>
						<?php
						endforeach;
						endif;
						?>
						</select> 
            OR 
            <span style="color:#1E5886;">+</span> <a id="create-photo-album" href="#">Create a new album</a>
					</div>
				</div>


				<div id="date-period-box" class="custom-form-element offer-page" style="display:none;" >
					<div class="form-title">OFFER PERIOD</div>
					<div class="form-area">
						<input type="text" class="date_field" name="txt_date_start" placeholder="Start date" value="Start date" readonly="readonly" id="txt_date_start" />
						<input type="text" class="date_field" name="txt_date_end"  placeholder="End date" value="End date" readonly="readonly" id="txt_date_end" style="margin-left:20px;" />
						
					</div>
					<span id="error_offer_date_time" class="error-msg"></span>
				</div>


				<div id="datetime-period-box" class="custom-form-element event-page" style="display:none;">
					<div class="form-title">WHEN?  <span id="error_date_time" class="error-msg"></span></div>
					<div class="form-area">
						<input type="text" class="date_field" name="txt_date" value="Pick a date" readonly="readonly" id="txt_date" />
						<input type="text" name="txt_time" readonly="readonly"  value="Pick a time" id="txt_time" style="margin-left:20px;" />
						&nbsp;&nbsp;<span class="event-sep" style="color:#1E5886;">+</span> <a id="show-end-date" href="#">Add end date</a>
					</div>
				</div>
				<div id="datetime-to-box" class="custom-form-element event-page" style="display:none;">
					<div class="form-title">TO?</div>
					<div class="form-area">
						<input type="text" class="date_field" name="txt_to_date" value="Pick a date" readonly="readonly" id="txt_to_date" />
						<input type="text" name="txt_to_time" class="time_field" readonly="readonly"  value="Pick a time" id="txt_to_time" style="margin-left:20px;" />
					</div>
				</div>

				<div id="location-concept-box" class="custom-form-element event-page" style="display:none;">
					<div class="form-title">WHERE?</div>
					<div class="form-area">
						<select id="cmb_location" name="cmb_location" class="form-text-dropdown">
							<option value="0">- Select a location -</option>
							<?php if (count($cmb_location)): foreach ($cmb_location as $loc): ?>
							<option value="<?= $loc['id'] ?>"><?= $loc['name'] ?></option>
							<?php endforeach;
							endif; ?>
						</select>
						<select id="cmb_concept" name="cmb_concept" class="form-text-dropdown"  style="margin-left:20px;">
							<option value="0">- Select a concept -</option>
							<?php if (count($cmb_concepts)): foreach ($cmb_concepts as $cp):
							if($concept['id'] == $cp['id']):
							?>
							<option value="<?= $cp['id'] ?>"><?= $cp['name'] ?></option>
							<?php
							endif;
							endforeach;
							endif; ?>
						</select>
						<span id="error_cmb_location" class="error-msg"></span>
					</div>
				</div>

				<div id="album_category-period-box" class="custom-form-element album-page" style="display:none;">
					<div class="form-title">ALBUM CATEGORY <span id="error_album_category" class="error-msg"></span></div>
					<div class="form-area">
						<select id="album_category_id" name="album_category_id" class="form-text-dropdown">
							<option value="0">- Select a category -</option>
							<?php 
							if (count($cmb_album_category))
							{
							foreach ($cmb_album_category as $alb_cat)
							{
							?>
							<option value="<?= $alb_cat['cid'] ?>"><?= $alb_cat['name'] ?></option>
							<?php
							}
							}
							?>
						</select>
					</div>
				</div>

				<div id="add-photo-box" class="custom-form-element album-page" style="display:none;">
					<input type="hidden" name="album_folder" id="album_folder" />
					<input type="hidden" name="album_id" id="album_id" />
					<input type="hidden" name="alb_publish_status" id="alb_publish_status" />
					<input type="hidden" name="news_create_flag" id="news_create_flag" value="no" />

					<div class="form-title">ADD PHOTOS <span id="error_album_photos" class="error-msg"></span></div>
					<div class="form-area" style="background-color:#FFFFFF;padding: 10px;width:500px;">
						<a href="#" id="clear_queue" style="color:#ff0000;position: absolute;margin-left: 125px;margin-top: 4px;">Clear queue</a> <span id="upload"></span>
					</div>
				</div>

				<input name="txt_publish" id="txt_publish" type="hidden" value="1" />
				<input name="entry_id" id="entry_id" type="hidden" value="0" />
				<div class="clear"></div>
			</div>
		</div>

		<div class="button-holder">
			<input type="button" id="cancel" class="btn-sm" value="Cancel">
			<div style="float:right;">
				<input id="submit_content" class="ItemButtonSave btn-sm submit_entry" name="save" type="button" title="Add Content for Review" value="Add Content for Review" />
			</div>
			<div class="clear"></div>
		</div>
	</form>
</div>