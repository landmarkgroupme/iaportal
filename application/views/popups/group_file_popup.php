<?php
$attributes = array('class' => '', 'id' => 'frm_add_item');
?>
<div id="group_upload_file" class="fb-container-box d-n">
<h3>Upload File</h3>
<ul class="msg d-n"></ul>
	<?php echo form_open_multipart('files/submit_share_files',$attributes);?>
	<div class="form-fields">
  
	<dl class="cols-two">
		<dd><label for="cmb_cat">Category<span class="required">*</span></label><span class="error-msg" id="error-cmb_cat"><?=form_error('cmb_cat')?></span><br />
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="cmb_cat" id="cmb_cat">
					<option value="">Select a category</option>
					<?php	foreach($file_categories as $db_data):?>
					<option value="<?php echo $db_data['id']; ?>"><?php echo $db_data['name'];?></option>
					<?php endforeach?>
					</select>
				</label>
			</div>
		</div>
		</dd>
	</dl>
  <?php if($group['id'] == $audit_constant['internal_audit_plan_group_id'] || $group['id'] == $audit_constant['knowledge_center_group_id'] || $group['id'] == $audit_constant['user_guide_group_id'] || $group['id'] == $audit_constant['outlet_audit_reports_group_id'] || $group['id'] == $audit_constant['process_audit_reports_group_id']):
   ?>
  <dl class="cols-two">
		<dd><label for="concept">Concept<span class="required">*</span></label><span class="error-msg" id="error-concept"><?=form_error('file_concept')?></span><br />
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="file_concept" id="file_concept">
					<option value="">Select a concept</option>
					<?php	foreach($concepts as $concept):?>
					<option value="<?php echo $concept->id; ?>"><?php echo $concept->name;?></option>
					<?php endforeach?>
					</select>
				</label>
			</div>
		</div>
		</dd>
	</dl>
  <?php else: ?>
  <input type="hidden" name="file_concept" value="0" />
  <?php endif; ?>
	<dl class="files-box-holder">
		<dd class="files-box patt-2">
			<label for="file_share">Upload a File<span class="required">*</span></label>
			<span id="error-files_upload" class="error-msg"></span><br/>
			<input type="file" size="71" id="file_share" name="file_share" />
			<!-- <div class="block" id="share-file" style="display: block;">
				<div id="upload-image">
					<div class="textC">
						<input type="file" size="71" id="file_share" name="file_share" class="d-n"/>
						<input type="text" id="upload_file_txt" placeholder="Max filesize 100MB" />
					</div>
				</div>
			</div> -->
		</dd>
		<div id="progressbox" style="display:none;"><div id="fileprogressbar"></div ><div id="statustxt">0%</div></div>
		
    <dd>
			<div class="block">
				<label for="txt_title11">Title<span class="required">*</span></label>
				<span id="error-txt_title11" class="error-msg"></span><br>
				<div class="textC"><input type="text" id="txt_title11" name="txt_title" placeholder="Title"/></div>
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txtar_description">Description </label>
				<span id="error-txtar_description" class="error-msg"></span><br>
				<div class="textC"><textarea cols="55" rows="10" id="txtar_description" name="txtar_description" placeholder="Enter File Description"></textarea></div>
			</div>
		</dd>
    <dd>
			<div class="block">
				<label for="txt_title11">Select People to share file with OR Leave the field blank to share with everyone</label>
				<span id="error-txt_title11" class="error-msg"></span><br>
				<div class="textC"><input type="text" id="txt_assign" name="file_assignee" class="autocomplete_user" /></div>
        
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txt_tags">Tags </label><span class="sub-text">Add descriptive words about the content, marketing, HR, public relations. </span>
				<span id="error-txt_tags" class="error-msg"></span><br>
				<div class="textC"><input type="text" id="txt_tags" name="txt_tags" class="txt_title ui-autocomplete-input" autocomplete="off" role="textbox"aria-autocomplete="list" aria-haspopup="true" placeholder="Start typing..."></div>
			</div>
		</dd>
		<dd>
			<input type="hidden" value="1" name="hd_submit_form" />
      <input type="hidden" value="<?php print $group['id'];?>" name="group" id="group"/>
			<div class="submit-div">
				<span id="progress-status"></span>
        <p>Files in this section can be seen by the public. Send via chat, or create a private group to enable private sharing</p>
				<input type="button" id="bt_add_item" class="btn-sm edit" value="Upload File" />
				<input type="button" id="cancel" class="btn-sm" value="Cancel" onclick="javascript:$.fancybox.close();"/>
			</div>
		</dd>
	</dl>
</div>

</form>
</div>