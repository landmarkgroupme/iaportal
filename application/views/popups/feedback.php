<div id="submit_feedback" class="fb-container-box d-n">
	
	<h3>Tell us what you think</h3>
	<div class="dlgFeedback">
		<p>We <strong>LOVE</strong> hearing from Landmarkers – yes, that's you!</p>
		<p>
		Tell us how we’re doing so we can make the intranet even better.
		If you want us to help you with anything else, do let us know. 
		</p>
		<form id="frmFeedback" method="post" action="<?php echo base_url();?>newhome/submit_feedback">
			<fieldset>
			<ul class="msg d-n"></ul>
			<label for="query">Choose Your feedback category<span class="required">*</span></label><span id="error-query" class="error-msg"><?=form_error('query')?></span>
			<dl class="cols-two">
				<dd>
					<div class="selectC">
						<label class="custom-select" for="query">
							<select class="" id="query" name="query">
								<option value="0" selected="selected">Please select</option>
								<option value="1">Question</option>
								<option value="2">Suggestion</option>
								<option value="3">Complaint</option>
								<option value="4">Praise</option>
							</select>
						</label>
					</div>
				</dd>
			</dl>
			<dl class="cols">
				
				<dd>"How can we improve? What can we help you with? Any thoughts you’d like to share?”	<div class="textC"><textarea id="describe" name="describe" rows="3" cols="50" placeholder="Your thoughts here"></textarea></div></dd>
				<dd><input type="button" id="add_feedback" name="submit" class="btn-sm submit" value="Send your feedback" /></dd>
			</dl>
			</fieldset>
		</form>
	</div>

</div>