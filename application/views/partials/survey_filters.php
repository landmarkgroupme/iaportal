    <div id="filter-search" class="clearfix surveyFilter">
    	<div class="filter-box">
            <form id="fFilter" name="filter" action="" method="get">
            <fieldset>
        	<!-- <div class="catalogue-bg">Filter By</div> -->
        	<label style="float: left;padding-top: 9px;">Filter By</label>
            <ul class="clearfix">
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddYear" name="ddYear">
                       <!-- <option value="2012" <?php // echo ((date('Y',time())+1) == 2012)?'selected="selected"':''?>>2011 - 2012</option>
                        <option value="2013" <?php // echo ((date('Y',time())+1) == 2013)?'selected="selected"':''?>>2012 - 2013</option>
                        <option value="2014" <?php // echo ((date('Y',time())+1) == 2014)?'selected="selected"':''?>>2013 - 2014</option> -->
                        <option value="2015" <?php echo ((date('Y',time())+1) == 2015)?'selected="selected"':''?>>2014 - 2015</option>
                    </select>
                    </label>
                </li>
                <?php
                $concept_id = '';
                $conceptattr = ''; 
				$conceptattr1 = '';
               if($myprofile['role_id'] != $permission_roles['AM'] && $myprofile['role_id'] != $permission_roles['SA'] && $myprofile['role_id'] != $permission_roles['ARV']){
                    if(empty($myconcept_name)) { 
                        $conceptattr = 'disabled="disabled"'; //25 Aug 2014
        				$conceptattr1 = 'style="display:none;"';
                        $concept_id = $myconcept;
                    }
                }
                #echo "<pre>".print_r($outlet_manager,"\n")."</pre>";
                if((!isset($outlet_manager) || empty($outlet_manager)) || ((isset($outlet_manager) && !empty($outlet_manager)) && !in_array($myprofile['id'],$outlet_manager))):
                ?>
                <li id="concept" <?php print $conceptattr1;?> >
                    <label class="custom-select">
                    <select class="filter-dd" id="ddConcepts" name="ddConcepts" <?php print $conceptattr;?>>
                        <?php if(!empty($myconcept_name)) { ?>
                        <option value="<?php echo $myconcept; ?>" selected="selected" ><?php echo ucwords(strtolower($myconcept_name)); ?></option>
                        <?php } else { ?>
                         <option value="0">All Concepts</option>
                        <?php  foreach($concepts as $concept): ?>
                      <option value="<?php echo $concept->id; ?>" <?php if($concept_id == $concept->id): print 'selected="selected"'; endif;?>><?php echo ucwords(strtolower($concept->name)); ?></option>
                        <?php endforeach;  } ?>
                    </select>
                    </label>
                </li>
                
                <li id="territory">
                    <label class="custom-select">
                    <select class="filter-dd" id="ddTerritory" name="ddTerritory">
                        <option value="0">All Territory</option>
                        <?php foreach($countries as $country): ?>
                        <option value="<?php echo $country->id; ?>"><?php echo ucwords(strtolower($country->name)); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <?php endif; ?>
                <?php 
                $style = '';
                if((isset($outlet_manager) && !empty($outlet_manager)) && in_array($myprofile['id'],$outlet_manager)):
                    $style = 'style="display:none;"';                    
                endif; ?>
                <li <?php print $style;?>>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddReporttype" name="ddReporttype">
                        <option value="1">Risk Rating by outlet</option>
                        
                        <option value="2">Risk Rating by category</option>
                        
                    </select>
                    </label>
                </li>
                <li>
                  <input type="button" name="bt_report_filter" id="bt_report_filter" class="btn-sm" value="Submit">
                </li>
 
                <li>
                  <a href="#" id="clear">Clear All</a>
                </li>
            </ul>
            </fieldset>
            </form>
        </div>
    </div>
