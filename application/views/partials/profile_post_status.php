<div id="hp-action">
            <ul class="hp-caption init-block">
                <li class="share init active"><a href="" class="first_link">Share Updates</a>
                    <ul class="hp-data">
                        <li>
                            <form id="fShare" name="share" action="<?php echo site_url(); ?>status_update/ajax_submit_status_message" method="post" class="share">
                            <?php if( (isset($concept_admin) && $concept_admin) or (isset($profile) && $profile['id'] == $myprofile['id']) ): ?>
                              <input type="hidden" name="object_id" value="<?php echo isset($target_id) ? $target_id : ''; ?>" />
                              <input type="hidden" name="object_type" value="<?php echo isset($target_type) ? $target_type : ''; ?>" />
                            <?php elseif(isset($target_id) && isset($target_type)) : ?>
                              <input type="hidden" name="target_id" value="<?php echo isset($target_id) ? $target_id : ''; ?>" />
                              <input type="hidden" name="target_type" value="<?php echo isset($target_type) ? $target_type : ''; ?>" />
                            <?php endif; ?>
							<input type="hidden" name="tag_text" id="tag_text" value="" />
                            <fieldset>
                                <textarea class="tagged_text" name="txtPost" id="txtPost" rows="1" cols="30" placeholder="Say Something..."></textarea>
                                <div class="action-button"><input type="submit" class="btn-sm inactive" id="btnShare" name="submit" value="Share" disabled="disabled"/></div>
                            </fieldset>
                            </form>
                        </li>
                    </ul>
                </li>
               <?php if($profile['id'] == $myprofile['id']):?>
              	  <li class="photo"><a href="" class="second_link">Add Photos/Videos</a>
                    <ul class="hp-data d-n">
                        <li>
                            <form id="fPhoto" name="fPhoto" action="<?php echo site_url(); ?>status_update/ajax_submit_photo" method="post" enctype="multipart/form-data">
                              <fieldset>
                              <p class="question">
							 <!-- <input type="text" name="addPhoto" id="addPhoto" placeholder="" /> -->
							  <textarea class="tagged_text_photo"  name="addPhoto" style="height:43px;" id="addPhoto" placeholder="Say Something..." ></textarea>
							  </p>
                                <input type="hidden" name="photos" id="photos" />
                                 <ul class="photos">
                                  <!--<li>
                                  	<input id="fileUploadFile" name="fileUploadFile" type="file" style="display: none;" />
                                    <a href="#" id="add_more_photos" title="Add Photo"><img src="<?php echo site_url(); ?>media/images/imgBrowsePhoto.png" width="72" height="72" alt="Add Photos"></a>
                                  </li> -->
								  <li class="photo-Upload">
								  <input id="fileUploadFile" name="fileUploadFile" type="file" />
								  </li>
                                </ul>
								
								  
                                <!-- Post the Videos 
                                <p class="post-video">You can also add <span>YouTube</span> link to share videos</p>
                                End Post the Videos -->
								
                                <div class="action-button">
								<div id="progressbox_photo" style=""><div id="fileprogressbar_photo"></div ><div id="statustxt_photo">0%</div></div>
								<input type="button" class="btn-sm submit inactive" id="btnPhotoUpload" name="btnPhoto" value="Share" disabled="disabled"/></div>
                              </fieldset>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="poll"><a href="">Post a Poll</a>
                    <ul class="hp-data d-n">
                        <li>
                            <form id="fPoll" class="fPoll" method="post">
                              <input type="hidden" name="created_by" value="<?php echo isset($target_id) ? $target_id : ''; ?>" />
                              <input type="hidden" name="created_by_type" value="<?php echo isset($target_type) ? $target_type : ''; ?>" />
                              <fieldset>
                              <p class="question"><input type="text" id="addQuestion" name="title" placeholder="What's your question?" /></p>
                                <input type="hidden" name="poll" value="1" />
                                <ul class="options">
                                  <li><input type="text" id="addAnswer_1" name="options[]" placeholder="Add an answer" /></li>
                                  <li><input type="text" id="addAnswer_2" name="options[]" placeholder="Add an answer" /></li>
                                </ul>
                                <div class="action-button"><input type="button" class="btn-sm inactive submit" name="btnVote" value="Create Poll" disabled="disabled"/></div>
                              </fieldset>
                            </form>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>
                <li class="ph-loader">
                	<img src="<?php echo site_url(); ?>/media/images/loading.gif" />
                </li>
            </ul>
        </div>