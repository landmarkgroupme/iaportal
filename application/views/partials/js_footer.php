<?php 
$ci = get_instance(); // CI_Loader instance
$ci->load->config('intranet');
?>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>media/js/jquery.qtip.min.js"></script>
<script src="<?php echo site_url(); ?>media/js/all.js?v=<?php echo $ci->config->item('css_ver'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>media/js/jquery.expander.js"></script>
<?php $this->load->view('templates/js/notifications'); ?>
<script type="text/javascript">
var siteurl = '<?php echo site_url(); ?>';

$(function() {
    $("img:below-the-fold").lazyload({
        event : "sporty"
    });
});
$(window).bind("load", function() {
    var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
});
</script>