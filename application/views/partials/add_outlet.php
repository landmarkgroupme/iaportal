<?php
$this->load->helper('html');
$attributes = array('class' => '', 'id' => 'frm_add_outlet');
?> <div class="add-outlet fb-container-box d-n" id="add_outlet" >
		  <h3>Add An Outlet</h3>
		  <ul class="msg d-n"></ul>
           <?php  echo form_open('surveys/outlets/save', $attributes);  ?>
      <div class="form-fields">
      	<dl>
					<dd>
							<label for="txt_name">User Name </label><span class="error-msg" id="error-txt_name"><?= form_error('txt_name') ?></span><br />
							<div class="textC">
								<input class="txt_name" type="text" name="txt_name" id="txt_name" placeholder="User Name" />
							</div>
					</dd>
				</dl>
				
				<dl>
					<dd>
						<label for="txt_outlet">Outlet Name </label><span class="error-msg" id="error-txt_outlet"><?= form_error('txt_outlet') ?></span><br />
							<div class="textC">
								<input type="text" name="txt_outlet" id="txt_outlet" class="txt-item-desc" placeholder="Enter Outlet Name" />
							</div>
					</dd>
				</dl>
        
        <dl>
					<dd>
						<label for="txt_concept">Concept </label><span class="error-msg" id="error-txt_concept"><?= form_error('txt_concept') ?></span><br />
							<div class="selectC">
								<select name="txt_concept" id="txt_concept">
                  <option value=""> Select Concept </option>
                  <?php foreach($concepts as $concept): ?>
                    <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                  <?php endforeach; ?>
                </select>
							</div>
					</dd>
				</dl>

        <dl>
					<dd>
						<label for="txt_territory">Territory </label><span class="error-msg" id="error-txt_territory"><?= form_error('txt_territory') ?></span><br />
							<div class="selectC">
								<select name="txt_territory" id="txt_territory">
                  <option value=""> Select Territory </option>
                  <?php foreach($countries as $country): ?>
                    <option value="<?php echo $country->id; ?>"><?php echo ucwords(strtolower($country->name)); ?></option>
                  <?php endforeach; ?>
                </select>
							</div>
					</dd>
				</dl>



				<dl>
					<dd>
						<input type="button" name="bt_add_outlet" id="bt_add_outlet" class="btn-sm" value="Add this Outlet" /> &nbsp;
						<input type="button" id="cancel" class="btn-sm" value="Cancel" />
					    <input type="hidden" name="hd_submit_form" value="1" />
              <input type="hidden" name="outlet_id" id="outlet_id" value="" />
					</dd>
				</dl>


				
				
				
			</form>
				
			</div>

			
	</div>