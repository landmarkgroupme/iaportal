<?php
$this->load->helper('html');
$attributes = array('class' => '', 'id' => 'frm_add_concept');
?> <div class="add-concept fb-container-box d-n" id="add_concept" >
		  <h3>Add A Concept</h3>
		  <ul class="msg d-n"></ul>
           <?php  echo form_open('surveys/concepts/save', $attributes);  ?>
      <div class="form-fields">
      	<dl>
					<dd>
							<label for="txt_name">User Name </label><span class="error-msg" id="error-txt_name"><?= form_error('txt_name') ?></span><br />
							<div class="textC">
								<input class="txt_name" type="text" name="txt_name" id="txt_name" placeholder="User Name" />
							</div>
					</dd>
				</dl>
        <dl>
					<dd>
						<label for="txt_concept">Concept </label><span class="error-msg" id="error-txt_concept"><?= form_error('txt_concept') ?></span><br />
							<div class="selectC">
								<select name="txt_concept" id="txt_concept">
                  <option value=""> Select Concept </option>
                  <?php foreach($concepts as $concept): ?>
                    <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                  <?php endforeach; ?>
                </select>
							</div>
					</dd>
				</dl>
				<dl>
					<dd>
						<input type="button" name="bt_add_concept" id="bt_add_concept" class="btn-sm" value="Add this concept" /> &nbsp;
						<input type="button" id="cancel" class="btn-sm" value="Cancel" />
					    <input type="hidden" name="hd_submit_form" value="1" />
              <input type="hidden" name="concept_id" id="concept_id" value="" />
					</dd>
				</dl>
			</form>
	</div>
</div>