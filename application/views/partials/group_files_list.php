<?php
/**
 * @author Sandeep Kumbhatil
 * @copyright 2014
 */
?>
<div class="section wrapper clearfix">

		<div class="left-contents">

			<div class="container">

				<?php
					$filters_data = array('search_placeholder' => 'Search for files by keyword','page'=>'group');
					$this->load->view('partials/group_file_filters', $filters_data);
				?>
				<div class="upload-share-block for_tags">
					<div class="tags"><strong>Popular Tags: </strong>
					<div class="tag-holder">
						<?php
  						$last_key = end(array_keys($filetags));
               
							foreach($filetags as $key => $filetag) { 
								?>
								<label class="lbl"><span value="<?php echo $filetag['id']; ?>"><?php echo ucwords($filetag['name']).' ('.$filetag['total'].')'; ?></span></label>
								<?php 
							} 
						?>
						</div>
					</div>
				</div>
				<div class="no-result-block d-n"></div>
				<div class="files-links-block">

				</div>
				<div class="loader-more">&nbsp;</div>


			</div> <!-- container -->

		</div> <!-- left-contents -->

	</div> <!-- section -->