    <div id="filter-search" class="media-hide clearfix">
    	<div class="filter-box">
            <form id="fFilter" name="filter" action="" method="get">
            <fieldset>
        	<label style="float: left; padding-top: 9px;">Filter By</label>
            <ul class="clearfix">
            	<li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddConcepts" name="ddConcepts">
                    	<option value="0">All Concepts</option>
                    	<?php foreach($concepts as $concept): ?>
                        <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                    	<?php endforeach; ?>
                    </select>
                    </label>
                </li>
            	<?php if(!isset($hide) || !in_array('department', $hide)): ?>
            	<li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddDepartments" name="ddDepartments">
                    	<option value="0">All Departments</option>
                        <?php foreach($departments as $dept): ?>
                        <option value="<?php echo $dept->id; ?>"><?php echo $dept->name; ?></option>
                    	<?php endforeach; ?>
                    </select>
                    </label>
                </li>
            	<?php endif; ?>
            	<li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddCountry" name="ddCountry">
                    	<option value="0">All Countries</option>
                        <?php foreach($countries as $country): ?>
                        <option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>
                    	<?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <li>

                <a href="#" class="clear_all" id="clear">Clear All</a>

                </li>
            </ul>
            </fieldset>
            </form>
        </div>

        <div class="search">
        	<!-- <form id="fSearch" name="search" action="" method="get"> -->
            <fieldset>
	            <div class="textC">
                	<input id="txtSearch" name="txtSearch" type="text" placeholder="<?php echo $search_placeholder; ?>" value="<?php echo(isset($searched) ? $searched : ''); ?>" />
					
                	<input type="button" id = "go" class="btn-go-inline" name="go" vdalue="Go" />
                </div>
            <!-- </fieldset> -->
            </form>
        </div>

    </div>
	
	
	<!-- Shows in Responsive View -->
<div id="filter-searchMobile" class="media-show clearfix">
   <div class="filter-dropdown">
      <div class="search">
         <!-- <form id="fSearch" name="search" action="" method="get"> -->
         <fieldset>
         <div class=" textC">
            <input id="txtSearchMobile" name="txtSearch" type="text" placeholder="<?php echo $search_placeholder; ?>" />
            <input type="button" id = "goMobile" class="btn-go-inline" name="go" vdalue="Go" />
         </div>
         <!-- </fieldset> -->
         </form>
      </div>
      <a href="#" class="more-filters">More</a>
   </div>
   
   <div class="filter-box" id="media-filter-dd">
	   <form id="fFilterMobile" name="filter" action="" method="get">
         <fieldset>
            <label style="float: left; padding-top: 9px;">Filter By</label>
            <ul class="clearfix">
               <li>
                  <label class="custom-select">
                     <select class="filter-dd" id="ddConceptsMobile" name="ddConceptsMobile">
                        <option value="0">All Concepts</option>
                        <?php foreach($concepts as $concept): ?>
                        <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                        <?php endforeach; ?>
                     </select>
                  </label>
                  <a href="#" id ="ddConceptsMobileClear" class="error-filter">&nbsp;</a>
               </li>
               <?php if(!isset($hide) || !in_array('department', $hide)): ?>
               <li>
                  <label class="custom-select">
                     <select class="filter-dd" id="ddDepartmentsMobile" name="ddDepartmentsMobile">
                        <option value="0">All Departments</option>
                        <?php foreach($departments as $dept): ?>
                        <option value="<?php echo $dept->id; ?>"><?php echo $dept->name; ?></option>
                        <?php endforeach; ?>
                     </select>
                  </label>
                  <a href="#" id ="ddDepartmentsMobileClear" class="error-filter">&nbsp;</a>
               </li>
               <?php endif; ?>
               <li>
                  <label class="custom-select">
                     <select class="filter-dd" id="ddCountryMobile" name="ddCountryMobile">
                        <option value="0">All Countries</option>
                        <?php foreach($countries as $country): ?>
                        <option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>
                        <?php endforeach; ?>
                     </select>
                  </label>
                  <a href="#" id ="ddCountryMobileClear" class="error-filter">&nbsp;</a>
               </li>
               <li>
                  <a href="#" class="clear_all" id="clearMobile">Clear All</a>
               </li>
            </ul>
         </fieldset>
      </form>
   </div>
</div>
<!-- End Shows in Responsive View -->
