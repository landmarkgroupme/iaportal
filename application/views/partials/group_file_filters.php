    <div id="filter-search" class="clearfix">
      <div class="filter-box">
            <form id="fFilter" name="filter" action="" method="get">
            <fieldset>
          <!-- <div class="catalogue-bg">Filter By</div> -->
          <label style="
    float: left;
    padding-top: 9px;
">Filter By</label>
            <ul class="clearfix">
              <!-- <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddfiles" name="ddfiles">
                      <option value="0">All</option>
                        <option value="1">Files</option>
            <option value="2">Links</option>
                    </select>
                    </label>
                </li> -->
               <?php if($group['id'] != $audit_constant['internal_audit_plan_group_id'] && $group['id'] != $audit_constant['knowledge_center_group_id'] && $group['id'] != $audit_constant['user_guide_group_id'] && $group['id'] != $audit_constant['outlet_audit_reports_group_id'] && $group['id'] != $audit_constant['process_audit_reports_group_id']): ?>
                <?php usort($file_categories_filter , 'cmp'); ?>
                <li>
                
                    <label class="custom-select">
                    <select class="filter-dd" id="ddCategories" name="ddCategories">
                        <option value="0">File Category</option>
                        <?php foreach($file_categories_filter as $category): ?>
                        <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <?php else: 
                if($group['id'] != $audit_constant['knowledge_center_group_id'] && $group['id'] != $audit_constant['user_guide_group_id']):
                ?>
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddYear" name="ddYear">
            <?php if($audit_constant['outlet_audit_reports_group_id'] == $group['id']) { 
            $i = 2012; 
            } 
            else if($audit_constant['internal_audit_plan_group_id'] == $group['id']) {
            $i = 2014;
            } else {
            $i = 2011;
            }
            ?>
                      <option value="0">Year</option>
                      <?php for( ; $i <= date('Y',time()); $i++) { ?>
                      <option value="<?php print $i;?>"><?php print $i;?></option>
                      <?php } ?>
                    </select>
                    </label>
                </li>
                <?php endif; ?>
                <?php endif; ?>
                <?php
                if($group['id'] == $audit_constant['internal_audit_plan_group_id'] || $group['id'] == $audit_constant['knowledge_center_group_id'] || $group['id'] == $audit_constant['user_guide_group_id'] || $group['id'] == $audit_constant['outlet_audit_reports_group_id'] || $group['id'] == $audit_constant['process_audit_reports_group_id']):
   
                  $managerConcept = 0;
                  $disabled = '';
                  if(isset($concept_manager) && !empty($concept_manager) || isset($outlet_manager) && !empty($outlet_manager))
                  {
                    if((isset($concept_manager) && in_array($myprofile['id'],$concept_manager)) || (isset($outlet_manager) && in_array($myprofile['id'],$outlet_manager)))
                    {
                      $disabled = 'style="display:none"';
                      $managerConcept =  $concept_manager_id[$myprofile['id']];
                    }
                  }
                #if((!isset($outlet_manager) || empty($outlet_manager)) || !in_array($myprofile['id'],$outlet_manager)):
                ?>
                
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddConcepts" name="ddConcepts" <?php print $disabled;?>>
                        <option value="0">All Concepts</option>
                        <?php foreach($concepts_feed as $concept):
                              $selected = ''; 
                              if($concept->id == $managerConcept)
                              {
                                 $selected = 'selected = "selected"';
                              }
                if(in_array($concept->id,$retail_concept))
                {
                        ?>
                        <option value="<?php echo $concept->id; ?>" <?php print $selected;?>><?php echo ucwords(strtolower($concept->name)); ?></option>
                        <?php 
            }
            endforeach; ?>
                    </select>
                    </label>
                </li>
                <?php #endif; ?>
                <li>
                    <label class="custom-select">
                    <?php $group_tags_title = strtolower(str_replace(' ','_',$group['name'])); ?>
                    <select class="filter-dd" id="ddTags" name="ddTags">
                     <?php if(isset($audit_constant[$group_tags_title.'_tag_title']) && !empty($audit_constant[$group_tags_title.'_tag_title'])): ?>
                        <option value="0"><?php print $audit_constant[$group_tags_title.'_tag_title']; ?></option>
                     <?php else: ?>
                        <option value="0">File Tags</option>
                     <?php endif; ?>
                        <?php foreach($files['filetags_filter'] as $key => $filetag) : ?>
                        <option value="<?php echo $filetag['id']; ?>"><?php echo $filetag['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <?php endif; ?>
                <?php if($group['id'] != $audit_constant['internal_audit_plan_group_id'] && $group['id'] != $audit_constant['knowledge_center_group_id'] && $group['id'] != $audit_constant['user_guide_group_id'] && $group['id'] != $audit_constant['outlet_audit_reports_group_id'] && $group['id'] != $audit_constant['process_audit_reports_group_id']): ?>
                <li><a href="#" onclick="$('#more_filter').toggle();">More Filter</a></li>
                <?php endif; ?>
                <li><a href="#" id="clear">Clear All</a></li>
                <?php if($group['id'] != $audit_constant['internal_audit_plan_group_id'] && $group['id'] != $audit_constant['knowledge_center_group_id'] && $group['id'] != $audit_constant['user_guide_group_id'] && $group['id'] != $audit_constant['outlet_audit_reports_group_id'] && $group['id'] != $audit_constant['process_audit_reports_group_id']): ?>
                <div id="more_filter" style="display: none;">
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddYear" name="ddYear">
                      <option value="0">Year</option>
                      <?php for($i = (date('Y',time())-5) ; $i <= date('Y',time()); $i++) { ?>
                      <option value="<?php print $i;?>"><?php print $i;?></option>
                      <?php } ?>
                    </select>
                    </label>
                </li>
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddMonth" name="ddMonth">
                      <option value="0">Month</option>
                      <?php
                      for($i = 1 ; $i <= 12; $i++)
                      {
                      ?>
                      <option value="<?php print $i;?>"><?php echo date("F",mktime(0,0,0,$i,1,date("Y"))); ?></option>
                      <?php 
                      }
                      ?>
                    </select>
                    </label>
                </li>
                </div>
                <?php endif; ?>

            </ul>
            </fieldset>
            </form>
        </div>

        <div class="search">
          <!-- <form id="fSearch" name="search" action="" method="get"> -->
            <fieldset>
              <div class=" textC" style="margin-top: 38px;">
                  <input id="txtSearch" name="txtSearch" type="text" placeholder="<?php echo $search_placeholder; ?>" />
                  <input type="button" id = "go" class="btn-go-inline" name="go" vdalue="Go" />
                </div>
            <!-- </fieldset> -->
            </form>
        </div>

    </div>
