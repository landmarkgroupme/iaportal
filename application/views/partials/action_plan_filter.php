   <div id="filter-search" class="clearfix surveyFilter">
    	<div class="filter-box">
            <form id="fFilter" name="filter" action="" method="get">
            <fieldset>
 	        <label style="float: left;padding-top: 9px;">Filter By</label>
            <ul class="clearfix">
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddYear" name="ddYear">
                      <option value="0">Years</option>
                      <?php for($i = 2014 ;$i <= date('Y',time());$i++): ?>
                      <option value="<?php print $i?>"><?php print $i;?>-<?php print ($i+1);?></option>
                      <?php endfor; ?>
                    </select>
                    </label>
                </li>
                 <?php
                $concept_id = '';
                $conceptattr = ''; 
				$conceptattr1 = '';
                if($myprofile['role_id'] != $permission_roles['AM'] && $myprofile['role_id'] != $permission_roles['SA'] && $myprofile['role_id'] != $permission_roles['ARV']){
                     if(empty($myconcept_name)) { 
                          $conceptattr = 'disabled="disabled"'; //25 Aug 2014
        				  $conceptattr1 = 'style="display:none;"';
                          $concept_id = $myconcept;
                    }
                } 

                 if(!isset($outlet_manager) || empty($outlet_manager) || !in_array($myprofile['id'],$outlet_manager)):?>
                <?php //if(!isset($concept_manager) || empty($concept_manager) || !in_array($myprofile['id'],$concept_manager)):?>
                <li id="concept" <?php print $conceptattr1;?> >
                    <label class="custom-select">
                    <select class="filter-dd" id="ddConcepts" name="ddConcepts">
                        <?php if(!empty($myconcept_name)) { ?>
                        <option value="<?php echo $myconcept; ?>" selected="selected" ><?php echo ucwords(strtolower($myconcept_name)); ?></option>
                        <?php } else { ?>
                        <option value="0">All Concepts</option>
                        <?php  foreach($concepts as $concept): ?>
                       <option value="<?php echo $concept['id']; ?>"><?php echo ucwords(strtolower($concept['name'])); ?></option> 
                        <?php endforeach;  } ?>
                    </select>
                    </label>
                </li>
                <?php //endif; ?>
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddTerritories" name="ddTerritories">
                        <option value="0">All Territories</option>
                        <?php foreach($countries as $country): ?>
                        <option value="<?php echo $country->id; ?>"><?php echo ucwords(strtolower($country->name)); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddOutlet" name="ddOutlet">
					<option value="0">Outlets</option>
                        <?php foreach($outlets as $outlet): ?>
                        <option value="<?php echo $outlet->id; ?>"><?php echo ucwords(strtolower($outlet->outlet_name)); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <?php endif; ?>

                <li>

                <a href="#" id="clear">Clear All</a>

                </li>
            </ul>
            </fieldset>
            </form>
        </div>
    </div>
