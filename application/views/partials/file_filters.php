    <div id="filter-search" class="media-hide clearfix">
    	<div class="filter-box">
            <form id="fFilter" name="filter" action="" method="get">
            <fieldset>
        	<!-- <div class="catalogue-bg">Filter By</div> -->
        	<label style="
    float: left;
    padding-top: 9px;
">Filter By</label>
            <ul class="clearfix">
            	<!-- <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddfiles" name="ddfiles">
                    	<option value="0">All</option>
                        <option value="1">Files</option>
						<option value="2">Links</option>
                    </select>
                    </label>
                </li> -->
                <li>
                    <label class="custom-select">
                    <select class="filter-dd" id="ddCategories" name="ddCategories">
                        <option value="0">File Category</option>
                        <?php foreach($file_categories as $category): ?>
                        <option value="<?php echo $category['id']; ?>"><?php echo ucwords(strtolower($category['name'])); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>
                <?php 
                  $class = "";
                  if(isset($page) && !empty($page) && $page == 'group'):
                        $class = 'display:none;';
                  endif;
                ?>
                <li style="<?php print $class;?>">
                    <label class="custom-select">
                    <select class="filter-dd" id="ddConcepts" name="ddConcepts">
                        <option value="0">All Concepts</option>
                        <?php foreach($concepts as $concept): ?>
                        <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </label>
                </li>

                <li>

                <a href="#" id="clear">Clear All</a>

                </li>
            </ul>
            </fieldset>
            </form>
        </div>

        <div class="search">
        	<!-- <form id="fSearch" name="search" action="" method="get"> -->
            <fieldset>
	            <div class=" textC">
                	<input id="txtSearch" name="txtSearch" type="text" placeholder="<?php echo $search_placeholder; ?>" />
                	<input type="button" id = "go" class="btn-go-inline" name="go" vdalue="Go" />
                </div>
            <!-- </fieldset> -->
            </form>
        </div>

    </div>
<!-- Shows in Responsive View -->
<div id="filter-searchMobile" class="media-show media-file clearfix">
   <div class="filter-dropdown">
      <div class="search">
         <!-- <form id="fSearch" name="search" action="" method="get"> -->
         <fieldset>
         <div class=" textC">
            <input id="txtSearchMobile" name="txtSearchMobile" type="text" placeholder="<?php echo $search_placeholder; ?>" />
            <input type="button" id = "goMobile" class="btn-go-inline" name="go" vdalue="Go" />
         </div>
         <!-- </fieldset> -->
         </form>
      </div>
      <a href="#upload_file-box" id ="upload_file_editMobile" class="btn-sm fancybox editProfile" >Upload</a>
      <a href="#" class="more-filters">More</a>
   </div>
   
   <div class="filter-box" id="media-filter-dd">
      <form id="fFilterMobile" name="filter" action="" method="get">
         <fieldset>
            <!-- <div class="catalogue-bg">Filter By</div> -->
            <label style="float: left; padding-top: 9px;">Filter By</label>
            <ul class="clearfix">
               <!-- <li>
                  <label class="custom-select">
                  <select class="filter-dd" id="ddfiles" name="ddfiles">
                  	<option value="0">All</option>
                      <option value="1">Files</option>
                  <option value="2">Links</option>
                  </select>
                  </label>
                  <a href="#" class="error-filter">&nbsp;</a>
                  </li> -->
               <li>
                  <label class="custom-select">
                     <select class="filter-dd" id="ddCategoriesMobile" name="ddCategoriesMobile">
                        <option value="0">File Category</option>
                        <?php foreach($file_categories as $category): ?>
                        <option value="<?php echo $category['id']; ?>"><?php echo ucwords(strtolower($category['name'])); ?></option>
                        <?php endforeach; ?>
                     </select>
                  </label>
                  <a href="#" id="ddCategoriesMobileClear" class="error-filter">&nbsp;</a>
               </li>
               <?php 
                  $class = "";
                  if(isset($page) && !empty($page) && $page == 'group'):
                        $class = 'display:none;';
                  endif;
                  ?>
               <li style="<?php print $class;?>">
                  <label class="custom-select">
                     <select class="filter-dd" id="ddConceptsMobile" name="ddConceptsMobile">
                        <option value="0">All Concepts</option>
                        <?php foreach($concepts as $concept): ?>
                        <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                        <?php endforeach; ?>
                     </select>
                  </label>
                  <a href="#" id="ddConceptsMobileClear" class="error-filter">&nbsp;</a>
               </li>
               <li>
                  <a href="#" id="clearMobile">Clear All</a>
               </li>
            </ul>
         </fieldset>
      </form>
   </div>
</div>
<!-- End Shows in Responsive View -->