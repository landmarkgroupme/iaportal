 <?php
$attributes = array('class' => '', 'id' => 'frm_add_request_item');
?>
 <div class="add-item fb-container-box d-n" id="add_request" >
  <h3 id="request-item-heading">Request An Item</h3>
           		  <ul class="msg d-n"></ul>
           <?php echo form_open_multipart('market/submit_request_items',$attributes);?>
              <div class="form-fields">
				<dl class="cols-two">
				<dd>
				<div class="block">
					<label for="cmb_cat_req">Category </label> <span class="error-msg" id="error-cmb_cat_req"><?= form_error('cmb_cat_req') ?></span><br />
					<div class="selectC">
						<label class="custom-select">
							<select name="cmb_cat_req" id="cmb_cat_req">
								<option value="">Select a category</option>
								<?php foreach ($categories as $db_data): ?>
								<option value="<?= $db_data->id ?>"><?= $db_data->name ?></option>
								<?php endforeach ?>
							</select>
						</label>
					</div>
				</div>
				</dd>
				</dl>

				<dl>
					<dd>
						<div class="block">
							<label for="txt_title_req">Title </label><span class="error-msg" id="error-txt_title_req"><?= form_error('txt_title_req') ?></span><br />
								<div class="textC">
									<input class="txt_title" type="text" name="txt_title_req" id="txt_title_req" placeholder="Item Title"/>
								</div>
						</div>
					</dd>
				</dl>
				<dl>
					<dd>
					<div class="block">
						<label for="txtar_description_req">Description </label><span class="error-msg" id="error-txtar_description_req"><?= form_error('txtar_description_req') ?></span><br />
						<div class="textC">
							<textarea name="txtar_description_req" id="txtar_description_req" rows="2" cols="50" class="txt-item-desc" placeholder="Enter Item Description"></textarea>
						</div>
					</div>

					</dd>
				</dl>

				<dl class="cols-two">
					<dd>
						<div class="block">
							<label for="rd_price_req"><input checked="checked" type="radio" name="rd_price_req" id="rd_price_req" value="2"/> Price</label>
							<span class="error-msg" id="error-rd_price_req"></span><br />
							<div class="selectC">
								<label class="custom-select">
									<select class="price_check" name="cmb_curreny_req" id="cmb_curreny_req">
										<?php foreach ($currencies as $db_data): ?>
										  <option value="<?= $db_data->id ?>"><?= $db_data->code ?></option>
										<?php endforeach ?>
									</select>
								</label>
							</div>
						</div>
					</dd>

					
				</dl>
				<dl>
				<dd>
						<div class="block">
							<div id="slider-holder">
								<div class="ui-slider-limit clearfix">
									<div id="slider-min">0</div>
									<div id="slider-max">10000</div>
								</div>
								<!-- <div id="slider"></div> -->
								 <div id="slider11"></div>
								<div id="slider-result-holder" style = "padding-top:10px"><img src="<?= base_url() ?>images/arrow-left.jpg" alt="let arrow" />
								<span id="slider-currency">AED</span> <span id="slider-result-min">0</span> - <span id="slider-result-max">1000</span>
								<input type="hidden" name="hd_min_price" id="hd_min_price" value="0" />
								<input type="hidden" name="hd_max_price" id="hd_max_price" value="1000" />
								</div>
							</div>
						</div>
					</dd>
				</dl>

				<dl>
					<dd>
						<label for="default_image_upload">Photos</label>
                          <div class="sub-text">Format (.jpeg, .gif, .png) Max filesize 10MB</div>
                          <div id="upload-image-req">
							<input type="file" name="file_image_req[]" class = "itemImageReq" id="default_image_upload" size="73" />  <span class="error-msg" id="error-default_image_upload"></span>
                          </div>
					</dd>
					<dd>
						<div class="add-more-div"><a id="add-more-image-req" href="#">+ Add another photo</a></div>
					</dd>
					<dd>
						<label for="rd_item_condition_used"><input checked="checked" type="radio" name="rd_item_condition" id="rd_item_condition_used" value="1" /> Used</label> &nbsp;
						<label for="rd_item_condition_new"><input type="radio" name="rd_item_condition" id="rd_item_condition_new" value="2" /> New</label>
					</dd>
					<dd>
						<input type="button" name="bt_request_item" id="bt_request_item" class="btn-sm" value="Request the Item" /> &nbsp;
						<input type="button" id="cancel_req" class="btn-sm" value="Cancel" />
					    <input type="hidden" name="hd_submit_form" value="1" />
              <input type="hidden" name="edit_market_req" id="edit_market_req" value="" />
					</dd>
				</dl>
				
			</div>
            </form>

				
				<hr class="dotted sp-25" style="margin-left: auto; margin-right: auto;" />

				
				<div id="sidebar">
				  <h2>Marketplace Rules:</h2>
				  <p>In order to make sure that people find your products conveniently,<br/> please add the item in the appropriate category.</p>
				  <p>Avoid adding offensive or jarring images to the product description.</p>
				  <p>Your peers have failth in you, so play by the rules and keep it that way ;)</p>
				</div>

            </div>