<?php
$this->load->helper('html');
$this->load->helper('form');
$attributes = array('class' => '', 'id' => 'career-form');
?>
<div id="refer_friend-box" class="fb-container-box refer_friend-box d-n">
	<h3>Refer A Friend</h3>
	<ul class="msg d-n"></ul>
	<!--<form id="career-form" method="post" enctype="multipart/form-data" action = "jobs/applayJob"> -->
	<?php echo form_open_multipart('jobs/applayJob',$attributes);?>
		<!-- <ul id="mReferFriend" class="msg d-n"></ul> -->
		<dl class="cols-two">
			<dd><label for="full-name">Name</label>	<span class="error-msg" id="error-full-name"></span><div class="textC"><input type="text"  id="full-name" name="full-name" placeholder="First Name" /></div></dd>
			<dd>&nbsp;	<div class="textC"><input type="text" id="last_rname" name="last_rname" placeholder="Last Name" /></div></dd>
		</dl>
		<dl class="cols-two">
			<dd><label for="email-id">Email</label>	<span class="error-msg" id="error-email-id"></span><div class="textC"><input type="text"  id="email-id" name="email-id" placeholder="Email Address" /></div></dd>
			<dd>Gender		<span class="error-msg" id="error-gender"></span><div class=""><label for="gender-male"><input type="radio" id="gender-male" name="gender" value="male" /> Male</label> &nbsp; <label for="gender-female"><input type="radio" id="gender-female" name="gender" value="female" /> Female</label></div></dd>
		</dl>

		<hr class=""/>

		<dl class="cols-two">
			<dd><label for="nationality">Nationality</label>&nbsp;<span class="error-msg" id="error-nationality"></span>
				<div class="selectC">
					<label class="custom-select">
						<select class="" id="nationality" name="nationality">
							<option value="">Select Your Nationality</option>
							<option value="Afghanistan">Afghanistan</option>
							<option value="Algeria">Algeria</option>
							<option value="Armenia">Armenia</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Belgium">Belgium</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bosnia And Herzegovina">Bosnia And Herzegovina</option>
							<option value="Brazil">Brazil</option>
							<option value="Britain">Britain</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burma Union Myanmar">Burma Union Myanmar</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Chad">Chad</option>
							<option value="China">China</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cote d'Ivoire">Cote d'Ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Czech">Czech</option>
							<option value="Denmark">Denmark</option>
							<option value="Egypt">Egypt</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Greece">Greece</option>
							<option value="Guinea">Guinea</option>
							<option value="Hungary">Hungary</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Italy">Italy</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyz Republic">Kyrgyz Republic</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Moldavia">Moldavia</option>
							<option value="Montenegro">Montenegro</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Namibia">Namibia</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Norway">Norway</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palestine">Palestine</option>
							<option value="Philippines">Philippines</option>
							<option value="Poland">Poland</option>
							<option value="Portugal">Portugal</option>
							<option value="Qatar">Qatar</option>
							<option value="Republic Of Belarus">Republic Of Belarus</option>
							<option value="Republic Of Macedonia">Republic Of Macedonia</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Serbia">Serbia</option>
							<option value="Singapore">Singapore</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Sultanate Of Oman">Sultanate Of Oman</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkey">Turkey</option>
							<option value="Turkmenistan">Turkmenistan</option>
							<option value="U S A">U S A</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Arab Emirates">United Arab Emirates</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="Uzbekistan">Uzbekistan</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Yemen">Yemen</option>
							<option value="Zimbabwe">Zimbabwe</option>
							<option value="Other">Other</option>
						</select>
					</label>
				</div>
			</dd>
			<dd><label for="education">Qualification</label>&nbsp;<span class="error-msg" id="error-education"></span>
				<div class="selectC">
					<label class="custom-select">
						<select class="" id="education" name="education">
							<option value="">Qualification</option>
							<option value="10th / Secondary / O Level / High School / Tawijhi">10th / Secondary / O Level / High School / Tawijhi</option>
							<option value="12th / Higher Secondary / A Level">12th / Higher Secondary / A Level</option>
							<option value="ACCA">ACCA</option>
							<option value="ACMA">ACMA</option>
							<option value="B.A.">B.A.</option>
							<option value="B.Com">B.Com</option>
							<option value="B.E./B.Tech">B.E./B.Tech</option>
							<option value="B.Pharma">B.Pharma</option>
							<option value="B.Sc">B.Sc</option>
							<option value="Baccalaureate Diploma">Baccalaureate Diploma</option>
							<option value="Baccalaureate Technical">Baccalaureate Technical</option>
							<option value="BBA">BBA</option>
							<option value="BFT">BFT</option>
							<option value="Brevet">Brevet</option>
							<option value="Brevet Diploma">Brevet Diploma</option>
							<option value="CA">CA</option>
							<option value="Certifications">Certifications</option>
							<option value="CFA">CFA</option>
							<option value="CIA">CIA</option>
							<option value="CS">CS</option>
							<option value="Diploma">Diploma</option>
							<option value="Elementary / Below Secondary">Elementary / Below Secondary</option>
							<option value="Elementary Below Secondary">Elementary Below Secondary</option>
							<option value="FCMA">FCMA</option>
							<option value="French Baccalaureate">French Baccalaureate</option>
							<option value="Graduate Degree / Bachelors Degree">Graduate Degree / Bachelors Degree</option>
							<option value="ICWA">ICWA</option>
							<option value="Lebanese Baccalaureate">Lebanese Baccalaureate</option>
							<option value="LT">LT</option>
							<option value="M. Pharma">M. Pharma</option>
							<option value="M.A">M.A</option>
							<option value="M.Arch">M.Arch</option>
							<option value="M.Com.">M.Com.</option>
							<option value="M.E./M.Tech">M.E./M.Tech</option>
							<option value="M.Sc.">M.Sc.</option>
							<option value="MBA">MBA</option>
							<option value="MCA">MCA</option>
							<option value="MD">MD</option>
							<option value="ML/LLM">ML/LLM</option>
							<option value="MS">MS</option>
							<option value="PhD">PhD</option>
							<option value="PHR">PHR</option>
							<option value="Post Graduate Diploma">Post Graduate Diploma</option>
							<option value="Professional Membership">Professional Membership</option>
							<option value="Specialized Training">Specialized Training</option>
							<option value="Technical Superior">Technical Superior</option>
							<option value="Vocational">Vocational</option>
							<option value="Other">Other</option>
						</select>
					</label>
				</div>
			</dd>
		</dl>
		<dl class="cols-two">
			<dd><label for="industry">Industry Type</label>&nbsp;<span class="error-msg" id="error-industry"></span>
				<div class="selectC">
					<label class="custom-select" for="industry">
						<select class="" id="industry" name="industry">
							<option value="">Please select</option>
							<option value="Advertising/PR/Events/Media">Advertising/PR/Events/Media</option>
							<option value="Architecture/Interior Design">Architecture/Interior Design</option>
							<option value="Aviation">Aviation</option>
							<option value="Banking/Financial Services/Insurance">Banking/Financial Services/Insurance</option>
							<option value="FMCG/Food/Beverages">FMCG/Food/Beverages</option>
							<option value="Hospitality/Tourism">Hospitality/Tourism</option>
							<option value="Recruitment/Placement Firm">Recruitment/Placement Firm</option>
							<option value="IT/Web/E-commerce">IT/Web/E-commerce</option>
							<option value="Medical/Healthcare">Medical/Healthcare</option>
							<option value="Retail">Retail</option>
							<option value="Real Estate">Real Estate</option>
							<option value="Shipping/Warehousing/Transportation">Shipping/Warehousing/Transportation</option>
							<option value="Other">Other</option>
						</select>
					</label>
				</div>
			</dd>
			<dd><label for="function">Function</label>&nbsp;<span class="error-msg" id="error-function"></span>
				<div class="selectC">
					<label class="custom-select" for="function">
						<select class="" id="function" name="function">
							<option value="">Please select</option>
							<option value="Administration">Administration</option>
							<option value="Architecture">Architecture</option>
							<option value="Audit">Audit</option>
							<option value="Buying">Buying</option>
							<option value="Customer Service">Customer Service</option>
							<option value="Finance and Accounts">Finance and Accounts</option>
							<option value="HR">HR</option>
							<option value="IT">IT</option>
							<option value="Learning and Development">Learning and Development</option>
							<option value="Legal">Legal</option>
							<option value="Marketing">Marketing</option>
							<option value="Planning and Merchandising">Planning and Merchandising</option>
							<option value="PR">PR</option>
							<option value="Properties and Facilities">Properties and Facilities</option>
							<option value="Quality Control">Quality Control</option>
							<option value="Retail">Retail</option>
							<option value="Warehouse and Logistics">Warehouse and Logistics</option>
							<option value="Web">Web</option>
							<option value="Other">Other</option>
						</select>
					</label>
				</div>
			</dd>
		</dl>

		<hr class=""/>

		<dl class="cols-two">
			<dd><label for="current-location">Current Employment Details</label>&nbsp;<span class="error-msg" id="error-employment"></span>	<div class="textC"><input type="text" id="current-location" name="current-location" placeholder="Current Location" /></div></dd>
			<dd><div class="textC"><input type="text" id="current-organization" name="current-organization" placeholder="Current Organization" /></div></dd>
		</dl>
		<dl class="cols-two">
			<dd><div class="textC"><input type="text" id="current-designation" name="current-designation" placeholder="Current Designation" /></div></dd>

			<dd><label for="experience">Relevant Experience</label>&nbsp;<span class="error-msg" id="error-experience"></span>

				<dl class="cols-two">
					<dd>
						<div class="selectC">
							<label class="custom-select">
								<select class="" name="experience-years">
									<option value = "" >Years</option>
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
								</select>
							</label>
						</div>
					</dd>
					<dd>
						<div class="selectC">
							<label class="custom-select">
								<select class="" id="experience" name="experience-months">
									<option value="">Months</option>
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
								</select>
							</label>
						</div>
					</dd>
				</dl>

		<!--	<div class="textC"><input type="text"  id="email" name="email" placeholder="Email Address" /></div> -->
			</dd>
		</dl>
		 <input type="hidden" name="ref_emp_email" id="ref_emp_email" value ="<?php echo $myprofile['email'];?>" />
		 <input type="hidden" name="ref_emp_hrmd_id" id="ref_emp_hrmd_id" value ="<?php echo $myprofile['hrms_id'];?>" />
		
		<dl >
			<dd><label for="upload_cv">Upload Your Resume</label>&nbsp;<span class="error-msg" id="error-resume"></span>
				<!-- <div class="textC">
					<input id="file-uploaded" name="resume" type="file" class="d-n">
					<input type="text"  id="file-name-text" name="file-name-text" placeholder=".pdf, .rtf, .docx, .doc files are accepted" />
				</div> -->
			</dd>
			<dd>
					<input id="file-uploaded" name="resume" type="file" >
					
			</dd>
			<dd>
				<input type="hidden" value="" style="opacity:0" name="job-code" id="job-code">
				<input id="bt_refer_friend_apply" type="button" name="refer_friend" class="btn-sm " value="Refer A Friend" />
				<input type="button" id="cancel" class="btn-sm" value="Cancel" />
			</dd>
		</dl>
	</form>
</div>
