<div id="filter-search" class="clearfix">
	<div class="filter-box">
        <form id="fFilter" name="filter" action="" method="get">
        <fieldset>
    	<label style="float: left; padding-top: 9px;">Filter By</label>
        <ul class="clearfix">
        	<li>
                <label class="custom-select">
                <select class="filter-dd" id="ddConcepts" name="ddConcepts">
                	<option value="0">All Concepts</option>
                	<?php foreach($concepts as $concept): ?>
                    <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                	<?php endforeach; ?>
                </select>
                </label>
          </li>
       	  <li>
            <a href="#" class="clear_all" id="clear_concept">Clear All</a>
          </li>
        </ul>
        </fieldset>
        </form>
    </div>
</div>