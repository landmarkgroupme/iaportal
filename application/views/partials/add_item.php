<?php
$this->load->helper('html');
$attributes = array('class' => '', 'id' => 'frm_add_item');
//echo form_open_multipart('market/submit_items',$attributes);
?> <div class="add-item fb-container-box d-n" id="add_item" >
		  <h3 id="add-item-heading">Add An Item</h3>
		  <ul class="msg d-n"></ul>
           <?php echo form_open_multipart('market/submit_items',$attributes);?>
              <div class="form-fields">
				<dl class="cols-two">
					<dd>
						<label for="cmb_cat">Category* </label><span class="error-msg" id="error-cmb-cat"><?= form_error('cmb_cat') ?></span><br />
							<div class="selectC">
								<label class="custom-select">
									<select name="cmb_cat" id="cmb_cat">
										<option value="">Select a category</option>
										<?php foreach ($categories as $db_data): ?>
										<?php if (isset($value_category) && $value_category == $db_data->id): ?>
										<option selected value="<?= $db_data->id ?>"><?= $db_data->name ?></option>
										<?php else: ?>
										<option value="<?= $db_data->id ?>"><?= $db_data->name ?></option>
										<?php endif; ?>
										<?php endforeach ?>
									</select>
								</label>
							</div>
					</dd>
				</dl>

				<dl>
					<dd>
							<label for="txt_title">Title </label><span class="error-msg" id="error-txt_title"><?= form_error('txt_title') ?></span><br />
							<div class="textC">
								<input class="txt_title" type="text" name="txt_title" id="txt_title" placeholder="Item Title" />
							</div>
					</dd>
				</dl>
				
				<dl>
					<dd>
						<label for="txtar_description">Description </label><span class="error-msg" id="error-txtar_description"><?= form_error('txtar_description') ?></span><br />
							<div class="textC">
								<textarea name="txtar_description" id="txtar_description" rows="2" cols="50" class="txt-item-desc" placeholder="Enter Item Description"></textarea>
							</div>
					</dd>
				</dl>

				<dl class="cols-two">
					<dd>
						<label for="rd_price"><input checked="checked" type="radio" name="rd_price" id="rd_price" value="1"/> Price</label>
						<span class="error-msg" id="error-rd_price"></span><br />
					</dd>
					<dd>
						<div class="selectC">
							<label class="custom-select">
								<select class="price_check" name="cmb_curreny" id="cmb_curreny">
								<?php foreach ($currencies as $db_data): ?>
									<option value="<?= $db_data->id ?>"><?= $db_data->code ?></option>
								<?php endforeach ?>
								</select>
							</label>
						</div>
					</dd>
					<dd>
						<div class="textC">
							<input class="price_check txt_price" type="text" name="txt_price" id="txt_price" maxlength="8" placeholder="Item Price" value=""/>
						</div>
					</dd>
					<dd>
						<label for="rd_price_free"><input type="radio" name="rd_price" id="rd_price_free" value="3"/> Free</label>
						<span class="error-msg" id="error-rd_price"></span><br />
					</dd>
				</dl>

				<dl>
					<dd>
						<label for="default_image_upload">Photos</label>
                          <div class="sub-text">Format (.jpeg, .gif, .png) Max filesize 10MB</div>
                          <div id="upload-image">
                            <input type="file" name="file_image[]" class = "itemImage" id="default_image_upload_add" size="73" />  
							<span class="error-msg" id="error-default_image_upload_add"></span>
                          </div>
					</dd>
					<dd>
						<div class="add-more-div"><a id="add_more_image" href="#">+ Add another photo</a></div>
					</dd>
					<dd>
						<label for="rd_item_condition_used"><input checked="checked" type="radio" name="rd_item_condition" id="rd_item_condition_used" value="1" /> Used</label> &nbsp;
						<label for="rd_price_new"><input type="radio" name="rd_item_condition" id="rd_item_condition_new" value="2" /> New</label>
					</dd>
					<dd>
						<input type="button" name="bt_add_item" id="bt_add_item" class="btn-sm" value="Post the Item" /> &nbsp;
						<input type="button" id="cancel" class="btn-sm" value="Cancel" />
					    <input type="hidden" name="hd_submit_form" value="1" />
              <input type="hidden" name="edit_market" id="edit_market" value="" />
					</dd>
				</dl>


				
				
				
			
				
			</div>
</form>
			<hr class="dotted sp-25" style="margin-left: auto; margin-right: auto;" />
			  
			<div id="sidebar">
				<h2>Marketplace Rules:</h2>
				<p>In order to make sure that people find your products conveniently,<br/> please add the item in the appropriate category.</p>
				<p>Avoid adding offensive or jarring images to the product description.</p>
				<p>Your peers have failth in you, so play by the rules and keep it that way ;)</p>
			</div>
			
	</div>