    <div id="filter-search" class="market-filter media-hide clearfix">







		<div class="filter-block-c clearfix">
			<div class="filter-block">
				<form id="fFilter" name="filter" action="ssi/ajax_response.php" method="get">
				<fieldset>
				<!-- <div class="catalogue-bg">Filter By</div> -->
				<label style="
				    float: left;
				    padding-top: 9px;
				">Filter By</label>
				<ul class="clearfix">
					<li>
						<label class="custom-select">
						<select class="filter-dd" id="ddCategories" name="ddCategories">
						<option value="0">Product Type</option>
						<?php foreach($categories as $categorie): ?>
                        <option value="<?php echo $categorie->id; ?>"><?php echo ucwords(strtolower($categorie->name)); ?></option>
                    	<?php endforeach; ?>
						</select>
						</label>
					</li>
					<!--<li>
						<label class="custom-select">
						<select class="filter-dd" id="ddConcepts" name="ddConcepts">
						<option value="0">Concepts</option>
                    	<?php //foreach($concepts as $concept): ?>
                        <option value="<?php //echo $concept->id; ?>"><?php //echo ucwords(strtolower($concept->name)); ?></option>
                    	<?php //endforeach; ?>
						</select>
						</label>
					</li>-->
					<li>
						<label class="custom-select">
						<select class="filter-dd" id="ddPrice" name="ddPrice">
							<option value="0">Price</option>
							<option value="1">Price (Low to High)</option>
							<option value="2">Price (High to Low)</option>
						</select>
						</label>
					</li>
					<li>
						<a href="#" id="clear">Clear All</a>
					</li>
				
				</ul>
				</fieldset>
				</form>
			</div>

<!--			<div class="filter-block">
				<form id="fFilter" name="filter" action="ssi/ajax_response.php" method="get">
				<fieldset>
				<div class="catalogue-bg">Filter By</div>
				<ul class="clearfix">
					<li>
						<label class="custom-select">
						<select class="filter-dd" id="ddPrice" name="ddPrice">
							<option value="0">Price</option>
							<option value="1">Price (Low to High)</option>
							<option value="2">Price (High to Low)</option>
						</select>
						</label>
					</li>
				</ul>
				</fieldset>
				</form>
			</div>-->

			<div class="search">
        
            <fieldset>
	            <div class=" textC">
                	<input id="txtSearch" name="txtSearch" type="text" placeholder="Search for item" />
                	<input type="submit" id = "go" class="btn-go-inline" name="go" value="Go" />
                </div>
				
            </fieldset>
        
        </div>
		   <ul class="clearfix btn-items">
					<li>
						<a href="#add_item" id ="add_an_item" class="btn-sm fancybox addItemFan" >Add An Item</a> &nbsp;
					</li>
					<li>
						<a href="#add_request" id ="request_an_item" class="btn-sm fancybox addRequestFan" >Request An Item</a> &nbsp;
					</li>
			</ul>
		</div>

    </div>
	
	<!-- Shows in responsive view -->
<div id="filter-searchMobile" class="market-filter media-show clearfix">
   <div class="filter-block-c clearfix">
      <div class="filter-dropdown">
         <div class="search">
            <fieldset>
               <div class=" textC">
                  <input id="txtSearchMobile" name="txtSearch" type="text" placeholder="Search for item" />
                  <input type="submit" id="goMobile" class="btn-go-inline" name="go" value="Go" />
               </div>
            </fieldset>
         </div>
         <ul class="clearfix btn-items">
            <li>
               <a href="#add_item" id ="add_an_itemMobile" class="fancybox addItemFan" >Add</a> &nbsp;
            </li>
            <li>
               <a href="#add_request" id ="request_an_itemMobile" class="fancybox addRequestFan" >Request</a> &nbsp;
            </li>
         </ul>
         <a href="#" class="more-filters">More</a>
      </div>
      <div class="filter-block" id="media-filter-dd">
         <form id="fFilterMobile" name="filter" action="ssi/ajax_response.php" method="get">
            <fieldset>
               <!-- <div class="catalogue-bg">Filter By</div> -->
               <label style="float: left; padding-top: 9px;">Filter By</label>
               <ul class="clearfix">
                  <li>
                     <label class="custom-select">
                        <select class="filter-dd" id="ddCategoriesMobile" name="ddCategoriesMobile">
                           <option value="0">Product Type</option>
                           <?php foreach($categories as $categorie): ?>
                           <option value="<?php echo $categorie->id; ?>"><?php echo ucwords(strtolower($categorie->name)); ?></option>
                           <?php endforeach; ?>
                        </select>
                     </label>
                     <a href="#" id="ddCategoriesMobileClear" class="error-filter">&nbsp;</a>
                  </li>
                  <!--<li>
                     <label class="custom-select">
                        <select class="filter-dd" id="ddConceptsMobile" name="ddConceptsMobile">
                           <option value="0">Concepts</option>
                           <?php //foreach($concepts as $concept): ?>
                           <option value="<?php //echo $concept->id; ?>"><?php //echo ucwords(strtolower($concept->name)); ?></option>
                           <?php //endforeach; ?>
                        </select>
                     </label>
                     <a href="#" class="error-filter">&nbsp;</a>
                  </li>-->
                  <li>
                     <label class="custom-select">
                        <select class="filter-dd" id="ddPriceMobile" name="ddPriceMobile">
                           <option value="0">Price</option>
                           <option value="1">Price (Low to High)</option>
                           <option value="2">Price (High to Low)</option>
                        </select>
                     </label>
                     <a href="#" id="ddPriceMobileClear" class="error-filter">&nbsp;</a>
                  </li>
                  <li>
                     <a href="#" id="clearMobile">Clear All</a>
                  </li>
               </ul>
            </fieldset>
         </form>
         <ul class="marketplace filter filter-links">
            <li class="active all_items" id ="0"><a href="#all_items">All <span>(<?php echo $item_count; ?>)</span></a></li>
            <li class="sale_items" id ="1"><a href="#sale_items">For Sale <span>(<?php echo $sale_count; ?>)</span></a></li>
            <li class="wanted_items" id ="2"><a href="#wanted_items">Items Wanted <span>(<?php echo $wanted_count; ?>)</span></a></li>
            <li class="freebies" id ="3"><a href="#freebies">Freebies <span>(<?php echo $freebies; ?>)</span></a></li>
            <li class="your_items" id ="4"><a href="#your_items">Your Items <span>(<?php echo $user_count; ?>)</span></a></li>
         </ul>
      </div>
      <!--<div class="filter-block">
         <form id="fFilter" name="filter" action="ssi/ajax_response.php" method="get">
             <fieldset>
                 <div class="catalogue-bg">Filter By</div>
                 <ul class="clearfix">
                    <li>
                      <label class="custom-select">
                      <select class="filter-dd" id="ddPriceMobile" name="ddPrice">
                        <option value="0">Price</option>
                        <option value="1">Price (Low to High)</option>
                        <option value="2">Price (High to Low)</option>
                      </select>
                      <a href="#" class="error-filter">&nbsp;</a>
                    </li>
                 </ul>
             </fieldset>
         </form>
      </div>-->
   </div>
</div>
<!-- End Shows in responsive view -->

	<ul class="marketplace filter filter-links media-hide">
			<li class="active all_items" id ="0"><a href="#all_items">All <span>(<?php echo $item_count; ?>)</span></a></li>
			<li class="sale_items" id ="1"><a href="#sale_items">For Sale <span>(<?php echo $sale_count; ?>)</span></a></li>
			<li class="wanted_items" id ="2"><a href="#wanted_items">Items Wanted <span>(<?php echo $wanted_count; ?>)</span></a></li>
			<li class="freebies" id ="3"><a href="#freebies">Freebies <span>(<?php echo $freebies; ?>)</span></a></li>
			<li class="your_items" id ="4"><a href="#your_items">Your Items <span>(<?php echo $user_count; ?>)</span></a></li>
		</ul>
