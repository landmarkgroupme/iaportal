<script type="text/javascript">


var comment_box = '.rp-comment';

  $('#recent-posts').on('keypress', comment_box, function (e) {
  		if (e.which == 13) {
			//console.log('Enter!');
			parent_div = $(this).closest('dl.block-2');
			parent_id = $(parent_div).attr('id');
			parent_type = $(parent_div).attr('content_type');
			// console.log(parent_div);

			var comment_tag='';			
			$(this).textntags('val', function(text) {
				comment_tag = text;
			});
			
			
			if($.trim(comment_tag) == '')
			{
			 return false;
			}
	  if(IsScript(comment_tag))
		{
			setError('#'+parent_id+' .fCMsg', 'Please check you inputs');
			return false;
		} else {
		$(this).textntags('reset');
		}
			//postComment($(this).val(), parent_id , parent_type);
			postComment(comment_tag, parent_id , parent_type);
			e.preventDefault();
	      	return false;
	      }
	});

  function postComment(text, parent_status_id, parent_type)
  {
    var parent_status_id = parent_status_id ? parent_status_id : 0;
	var parent_type = parent_type ? parent_type : 'Status';
    //var parent_id = parent_status_id.split('_');
    object_id = $('#' + parent_status_id).attr('object_id');
    object_type = $('#' + parent_status_id).attr('object_type');
    // console.log(parent_status_id);
	if (typeof concept_manager == 'undefined') // Any scope
	{
		concept_manager = 0;
	}
	if (typeof concept == 'undefined') // Any scope
	{
		concept = 0;
	}
    var service_url = siteurl+"status_update/ajax_submit_status_message";
     $.ajax({
      url: service_url,
      data: {"txtPost":text, "tag_text" : text, 'parent_status_id':parent_status_id, 'object_id': object_id, 'object_type': object_type, 'concept_manager': concept_manager, 'concept':concept, 'parent_type':parent_type},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        var update = msg.update *1;
        if(update){
          //Upadate if Tabs are All or Status update
		  replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
		  inputText = text;
          replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>'); 
			if(replacedText == text)
			{
				replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
				replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
			}
			
			replacePattern3 = /@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/gim;
	        replacedText = replacedText.replace(replacePattern3, '<a href="'+siteurl+'$1">$2</a>');

            // console.log($(parent_status_id+' .nedw'));
            var comment_html = '<ul class="post-cmntlike d-n" id="'+msg.details.id+'" object_id="'+msg.details.object_id+'" object_type="'+msg.details.object_type+'">';
			if(msg.details.object_type == "Concept")
			{
				comment_html += '<li class="picture"><img src="' + siteurl + '/images/concepts/thumbs/' + concept_thumb + '" width="25" height="25" alt="icon small"></li>';
				comment_html += '<li class="comnt-info"><a href="/concepts/' + concept_slug + '">'+ concept_name +'</a> ' + replacedText + '<span>' + moment(msg.time).fromNow() + '</span> &#8226; <a href="#" class="cmLike">Like</a><a  href="#" class="fancybox other_users_comment" ></a> </li><li class="hide"><a href="#" class="cDelete">x<div class="hide-tooltip"><span class="text">Remove this comment</span><span class="arrow"></span></div></a></li></ul>';
			} else {
				comment_html += '<li class="picture"><img src="' + siteurl + '/images/user-images/105x101/' + myprofile.profile_pic + '" width="25" height="25" alt="icon small"></li>';
				comment_html += '<li class="comnt-info"><a href="/' + myprofile.reply_username + '">'+ myprofile.display_name +'</a> ' + replacedText + '<br> <span>'+ moment(msg.time).fromNow() + '</span> &#8226; <a href="#" class="cmLike">Like</a><a  href="#" class="fancybox other_users_comment" ></a> </li><li class="hide"><a href="#" class="cDelete">x<div class="hide-tooltip"><span class="text">Remove this comment</span><span class="arrow"></span></div></a></li></ul>';
			}
			// console.log(comment_html);
			// console.log($('#check-last-ul-'+parent_status_id).attr('class'));
            $('#check-last-ul-'+parent_status_id).before(comment_html);
            $('#'+msg.details.id).fadeIn();
          	$(comment_box).val('');
        }
      }
    });
  }


  function likeStatus(object)
  {
    //parent_div = $(object).closest('div.block-1');
    parent_div = $(object).closest('dl.block-2');


    //console.log($(object).text());
    post_id = $(parent_div).attr('id');
	post_type = $(parent_div).attr('content_type');
    object_id = $(parent_div).attr('object_id');
    object_type = $(parent_div).attr('object_type');

    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/ajax_like_post";
     $.ajax({
      url: service_url,
      data: {'post_id':post_id, 'post_type': post_type, 'value' : $.trim($(object).text()), 'object_id' : object_id, 'object_type' : object_type},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update
            $(object).text(msg.value);

            if(msg.likes ==0 )
            {
            	$(parent_div).find('.post-cmntlike-block').slideUp('fast');
            }
            else
            {
            	if(msg.value == 'Unlike')
            	{
            		if(msg.likes == 1)
            			$(parent_div).find('.post-cmntlike-block .no-cmntlike').html('You like this');
					else
						$(parent_div).find('.post-cmntlike-block .no-cmntlike').html('You and ' + (msg.likes -1 ) + ' person like this ');
            	} else {
            		$(parent_div).find('.post-cmntlike-block .no-cmntlike').html( msg.likes + ' person like this ' );
            	}

            	$(parent_div).find('.post-cmntlike-block').slideDown('fast');
            }


        }
      }
    });
  }
  
    function likeComment(object)
  {



    //parent_div = $(object).closest('div.block-1');
    parent_div = $(object).closest('.post-cmntlike');


    // console.log(parent_div);
	// console.log('kp');
    post_id = $(parent_div).attr('id');
	post_type = 'Status';
    object_id = $(parent_div).attr('object_id');
    object_type = $(parent_div).attr('object_type');
    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/ajax_like_post";
     $.ajax({
      url: service_url,
      data: {'post_id':post_id, 'post_type': post_type, 'value' : $.trim($(object).text()), 'object_id' : object_id, 'object_type' : object_type},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update
            $(object).text(msg.value);

            if(msg.likes == 0 )
            {
            	$(parent_div).find('.rp-social-like-comment').slideUp('fast');
				$(parent_div).find('.other_users_comment').html('');
            }
            else
            {
            	if(msg.value == 'Unlike')
            	{
            		if(msg.likes == 1)
            			$(parent_div).find('.other_users_comment').html(' -1');
					else
						$(parent_div).find('.other_users_comment').html(' -'+msg.likes);
            	} else {
            		$(parent_div).find('.other_users_comment').html(' -'+msg.likes);
            	}

            	$(parent_div).find('.rp-social-like-comment').slideDown('fast');
            }


        }
      }
    });
  }
  
  
  function featuredStatus(object)
  {
      //parent_div = $(object).closest('div.block-1');
	  parent_div = $(object).closest('dl.block-2');      
    post_id = $(parent_div).attr('id');
	post_type = $(parent_div).attr('content_type');
    object_id = $(parent_div).attr('object_id');
    object_type = $(parent_div).attr('object_type');
    //$(object).text();
    //return;
    
	var service_url = siteurl+"status_update/ajax_featured_post";
    $.ajax({
      url: service_url,
      data: {'post_id':post_id, 'post_type': post_type, 'value' : $.trim($(object).text()), 'object_id' : object_id, 'object_type' : object_type},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
	   console.log(msg);
      if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update
            $(object).text(msg.value);
        }
      }
    });	
  }
  
  function featuredPolls(object)
  { 
  	parent_div = $(object).closest('dl.block-1'); 
	var checkElement=parent_div.length;
	if(checkElement>0)
	{
  	post_id = $(parent_div).attr('id'); 
	}
	else
	{
	parent_div = $(object).closest('div.block-1'); 
	post_id = $(parent_div).attr('id'); 
	}
  	var post_type='';	
  	post_type = 'Polls';   
  	var service_url = siteurl+"status_update/ajax_featured_post";
  	$.ajax({
  	url: service_url,
  	data: {'post_id':post_id, 'post_type': post_type, 'value' : $.trim($(object).text())},
  	async: false,
  	dataType: "json",
  	type: "POST",
  	success: function(msg){	 
      	if(msg.status == 'success'){	
      	//Upadate if Tabs are All or Status update
      	$(object).text(msg.value);
      	}
  	}
  	});	
  } 

  
  function featuredFiles(object)
  { 
	parent_div = $(object).closest('div.block-1');      
	post_id = $(parent_div).attr('id');	
	var post_type='';
	 // alert($(object).text());
	 // return;
	post_type = 'Files';	   
	var service_url = siteurl+"status_update/ajax_featured_post";
	$.ajax({
	url: service_url,
	data: {'post_id':post_id, 'post_type': post_type, 'value' : $.trim($(object).text())},
	async: false,
	dataType: "json",
	type: "POST",
	success: function(msg){	  
	if(msg.status == 'success'){	
	//Upadate if Tabs are All or Status update
	$(object).text(msg.value);
	}
	}
	});	
  }

  

  function showMoreComments(object)
  {
  
	$(object).closest('.post-cmntlike').addClass('d-n');
    //parent_div = $(object).closest('div.block-1');
    parent_div = $(object).closest('dl.block-2');
    // console.log($(object).text());
    post_id = $(parent_div).attr('id');
	$('.cmntlike-'+post_id).removeClass('d-n');
	
	}
  function deleteStatus(object)
  {


    //parent_div = $(object).closest('div.block-1');
    parent_div = $(object).closest('dl.block-2');


    //console.log($(object).text());
    status_id = $(parent_div).attr('id');

    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/delete_status_message";
     $.ajax({
      url: service_url,
      data: {'status_id':status_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      }
    });
  }
  
  function deleteComments(object)
  {
    parent_div = $(object).closest('.post-cmntlike');
    status_id = $(parent_div).attr('id');
    var service_url = siteurl+"status_update/delete_status_message";
     $.ajax({
      url: service_url,
      data: {'status_id':status_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      }
    });
  }
  
  
  function deletePhoto(object)
  {
    parent_div = $(object).closest('dl.block-2');
    //console.log($(object).text());
    photo_id = $(parent_div).attr('id');
    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/delete_status_photo";
     $.ajax({
      url: service_url,
      data: {'photo_id':photo_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      }
    });
  }
  
  function deletePhotoAlbum(object)
  {
    parent_div = $(object).closest('dl.block-2');
    //console.log($(object).text());
    album_id = $(parent_div).attr('id');
    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/delete_photo_album";
     $.ajax({
      url: service_url,
      data: {'album_id':album_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      }
    });
  }
  
  function deleteNewsOffer(object)
  {
    parent_div = $(object).closest('dl.block-2');
    noa_id = $(parent_div).attr('id');
    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/delete_status_noa";
     $.ajax({
      url: service_url,
      data: {'noa_id':noa_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      }
    });
  }
  
  function deletefile(object)
  {
    parent_div = $(object).closest('.block-1');
    //console.log($(object).text());
    file_id = $(parent_div).attr('id');
    /*$(object).text();
    return;*/
    var service_url = siteurl+"files/delete_file";
     $.ajax({
      url: service_url,
      data: {'file':file_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 1){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      },
    });
  }
  
  function deletePoll(object)
  {
    parent_div = $(object).closest('.block-1');
    //console.log($(object).text());
    poll_id = $(parent_div).attr('id');
    /*$(object).text();
    return;*/
    var service_url = siteurl+"status_update/delete_poll";
     $.ajax({
      url: service_url,
      data: {'poll_id':poll_id},
      async: false,
      dataType: "json",
      type: "POST",
      success: function(msg){
        if(msg.status == 'success'){
          //Upadate if Tabs are All or Status update

            $(parent_div).slideUp('fast');
            //console.log(msg);
            return;
        }
      },
    });
  }
  
  function deleteFile(object)
  {
  	var r=confirm("Do you really want to delete the file");
		if (r==true)
		{
			var parent_div = $(object).closest('.block-1');
			var file_id = $(object).attr('id');
			var service_url = siteurl+"files/delete_file";
			 $.ajax({
			  url: service_url,
			  data: {'file':file_id},
			  async: false,
			  dataType: "json",
			  type: "POST",
			  success: function(msg){
				if(msg.status == 1){
				  //Upadate if Tabs are All or Status update

					$(parent_div).slideUp('fast');
					//console.log(msg);
					return;
				}
			  },
			});
		}
  }
  

</script>