<div id="filter-search" class="clearfix surveyFilter">
	<div class="filter-box">
        <form id="surveyFilter" name="filter" action="" method="get">
        <fieldset>
        <label style="float: left;padding-top: 9px;">Filter By</label>
        <ul class="clearfix">
            <li id="concept">
                <label class="custom-select"><!-- input type="text" id="cal-date" class="ddDate" name="ddDate" placeholder="Survey Date"/ -->
                  <select id="ddDate" name="ddDate">
                    <option value="0">Year</option>
                    <?php for($i = (date('Y',time())-10);$i <= (date('Y',time()));$i++)
                    {
                      $selected = '';
                      if($i == date('Y',time()))
                      {
                        $selected = 'selected = "selected"';
                      }
                    ?>
                      <option value="<?php print $i; ?>" <?php print $selected; ?>><?php print $i; ?></option>
                    <?php   
                    }
                    ?>
                  </select>
                </label>
            </li>
            <?php 
            $style = '';
            if(isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager)): 
              $style = 'style="display:none;"';
            endif; ?>
            <li id="concept" <?php print $style; ?>>
                <label class="custom-select">
                <select class="filter-dd" id="ddConcepts" name="ddConcepts">
                    <option value="0">All Concepts</option>
                    <?php foreach($concepts as $concept):?>
                    <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                    <?php endforeach; ?>
                </select>
                </label>
            </li>
            
            <li>
                  <input type="button" name="bt_survey_filter" id="bt_survey_filter" class="btn-sm" value="Submit" />
            </li>
            <li>
              <a href="#" id="clear">Clear All</a>
            </li>
        </ul>
        </fieldset>
        </form>
    </div>
</div>