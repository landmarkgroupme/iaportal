<meta http-equiv="X-UA-Compatible" content="IE=7" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/facebox.css" />

<script type="text/javascript" src="<?=base_url();?>js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ie-hover-ns-pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/addclass.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.form.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/custom_js/general_js.js" type="text/javascript"></script>

<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/lt7.css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ie8.css" /><![endif]-->


<link type="text/css" href="<?php echo base_url(); ?>cometchat/cometchatcss.php" rel="stylesheet" charset="utf-8">
<script type="text/javascript" src="<?php echo base_url(); ?>cometchat/cometchatjs.php" charset="utf-8"></script>

<!-- <link type="text/css" href="<?php echo base_url(); ?>cometchat/cometchatcss.php" rel="stylesheet" charset="utf-8">
<script type="text/javascript" src="<?php echo base_url(); ?>cometchat/cometchatjs.php" charset="utf-8"></script>
-->