<?php 
$ci = get_instance(); // CI_Loader instance
$ci->load->config('intranet');
$ci->load->helper('cookie');

    $cookieData = get_cookie("intranet_ck");
    $session_array = unserialize( $cookieData );
    //echo "<pre>";
    //print_r($cookieData);
    //echo "<br>";
    $userida =  $session_array['userid'];

    //echo cho $session_array['is_logged_in'];

?>
<meta name="robots" content="noindex">
<meta http-equiv="x-ua-compatible" content="IE=edge" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link type="text/css" href="http://devnet.landmarkgroup.com/cometchat/cometchatcss.php" rel="stylesheet" charset="utf-8">
<script type="text/javascript" src="http://devnet.landmarkgroup.com/cometchat/cometchatjs.php" charset="utf-8"></script>
<script>
var userid = "<?php echo $userida; ?>"; // Must be populated programmatically
document.cookie = "cc_data="+userid;
</script>


<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<link rel="stylesheet" href="//fast.fonts.net/cssapi/a7307b6d-4641-44d1-b16a-c22888f05455.css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo site_url(); ?>media/css/jquery.ui.datepicker.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<link rel="stylesheet" href="<?php echo site_url(); ?>media/css/all.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<link rel="stylesheet" href="<?php echo site_url(); ?>media/css/jquery-textntags.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/media.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/component.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/snap.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/ratchet.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]--> 
<?php    
if(!isset($concept_manager) || empty($concept_manager))
{
  $this->load->model('Survey');
  $concepts = $this->Survey->getSurveyConcept();
  if(isset($concepts) && !empty($concepts))
  {
	  foreach($concepts as $concept)
	  {
		$concept_manager[] = $concept->user_id;
	  }
  }
  else
  {
	$concept_manager = array();
  }
}
if(!isset($outlet_manager) || empty($outlet_manager))
{
  $this->load->model('Outlets');
  $outlets = $this->Outlets->getOutletbyFilter(array());
  if(isset($outlets) && !empty($outlets))
  {
	  foreach($outlets as $outlet)
	  {
		$outlet_manager[] = $outlet->user_id;
	  }
  }
  else
  {
	$outlet_manager = array();
  }
}
?>
<script>
var myprofile = <?php echo json_encode($myprofile); ?>;
var pageurl = 'newhome';
var myfullname = myprofile.first_name + ' ' + myprofile.last_name;
var siteurl = '<?php echo site_url(); ?>';
var roles = <?php echo json_encode($permission_roles); ?>;
var concept_manager = <?php print json_encode($concept_manager); ?>;
var outlet_manager = <?php print json_encode($outlet_manager); ?>;
</script>
