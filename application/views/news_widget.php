<?php
if(isset($latest_news) && !empty($latest_news)):

?>
<div class="widget announcements clearfix news">
          	<div class="widget-head news">
                <h3>Latest News</h3>
            </div>
            <div class="box shadow-2">
                <ul>
				<?php foreach($latest_news as $news_item): ?>
                    <li><a class="announc clearfix" href="<?php echo site_url(); ?>news/<?php echo $news_item['entry_id']; ?>"><?php echo $news_item['entry_title'];?></a></li>
				<?php endforeach;?>
              </ul>
          </div>
        </div>
<?
endif;
?>