<div class="widget members clearfix">
	<div class="widget-head useful-links group-mgmt">
    <h3>Group Management</h3>
  </div>
  <div class="box shadow-2">
      <ul>
        <?php if(in_array($user_id,$uploader) || ($group['is_open'] == 'yes' && in_array($user_id,$groupMember))):?>
        <li><a href="#group_invite" class="fancybox group_invite">Invite More People</a></li>
        <?php endif; ?>
        <?php  if(in_array($user_id,$uploader)): ?>
        <li><a href="#group_upload_file" class="fancybox group_files">Post a File</a></li>
        <li><a href="#group_create" id="<?php echo $group['id']; ?>" class="fancybox group_create">Edit Group</a></li>
        <?php endif; ?>
        <li><a href="#group_create" class="fancybox group_create">Create a Group</a></li>        
      </ul>
  </div>
</div>