<div id="header" class="media-hide">
	<div class="wrapper">
        <div class="logo"><a href="<?php echo site_url(); ?>" class="home_icon">Landmark Group</a></div>

        <?php
        //echo "<pre>".print_r($myprofile,"\n")."</pre>";
         if(($myprofile['role_id'] == $permission_roles['US'] or $myprofile['role_id'] == $permission_roles['CONCM'] or $myprofile['role_id'] == $permission_roles['CONTM']) && ((!isset($concept_manager) && empty($concept_manager) || !in_array($myprofile['id'],$concept_manager)) && (!isset($outlet_manager) || empty($outlet_manager) || !in_array($myprofile['id'],$outlet_manager)))):
        ?>
     <ul id="menu">
          <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['user_guide_group_id'];?>/files">User Guide</a></li>
     </ul>
        
        <?php endif; ?>
        <?php if($myprofile['role_id'] == $permission_roles['AM'] or
                $myprofile['role_id'] == $permission_roles['SA'] or (isset($concept_manager) && !empty($concept_manager) && in_array($myprofile['id'],$concept_manager)) or (isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager)) or $myprofile['role_id'] == $permission_roles['ARV']):
        ?>
       <ul id="menu"> 
          <!-- li><a href="<?php //echo site_url(); ?>groups/<?php //print $audit_constant['user_guide_group_id'];?>/files">User Guide</a></li>
          <li><a href="<?php //echo site_url(); ?>groups/<?php //print $audit_constant['knowledge_center_group_id'];?>/files">Knowledge Center</a></li>
          <li><a href="<?php //echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
          <?php //if(!isset($outlet_manager) || empty($outlet_manager) || !in_array($myprofile['id'],$outlet_manager)): ?>          
          <li><a href="<?php //echo site_url(); ?>surveys/audit_report">Internal Audit Reports</a></li>
          <?php
         // endif;          
          //if((!isset($concept_manager) || empty($concept_manager) || (isset($outlet_manager) && !in_array($myprofile['id'],$outlet_manager))) && (!isset($outlet_manager) || empty($outlet_manager) || (isset($outlet_manager) && !in_array($myprofile['id'],$outlet_manager)))): ?>
          <li><a href="<?php// echo site_url(); ?>groups/<?php //print $audit_constant['internal_audit_plan_group_id'];?>/files">Internal Audit Plan</a></li>      
          <?php //endif; ?> -->

          <li><a href="http://devnet.landmarkgroup.com/people">People</a></li>
            <li><a href="http://devnet.landmarkgroup.com/files">Files</a></li>
            <li><a href="http://devnet.landmarkgroup.com/news">News</a></li>
            <li><a href="http://devnet.landmarkgroup.com/offers">Offers</a></li>
            <li><a href="http://devnet.landmarkgroup.com/announcement">Announcements</a></li>
            <li><a href="http://devnet.landmarkgroup.com/photos">Photos</a></li>
            <li><a href="http://devnet.landmarkgroup.com/concepts">Concepts</a></li>
            <li><a href="http://devnet.landmarkgroup.com/groups">Groups</a></li>
            <li><a href="http://devnet.landmarkgroup.com/market">Marketplace</a></li>
            <li><a href="http://devnet.landmarkgroup.com/jobs">Jobs</a></li>
         </ul>
         <?php endif; ?>

        <div class="accounts">
            <?php $time = time(); ?>
            <a href="http://devnet.landmarkgroup.com/<?php echo $myprofile['reply_username']; ?>"><img
                    src="http://devnet.landmarkgroup.com/images/user-images/105x101/<?php echo $myprofile['profile_pic'] . '?' . $time; ?>"
                    width="25" height="25"
                    alt="<?php echo $myprofile['display_name']; ?>"><b><?php echo substr(strtok($myprofile['display_name'], " "), 0, 8); ?></b></a>
            <a class="u-menu-pop" href="#"><span class="more"></span></a>
        </div>
	<div class="u-menu-block d-n">
		</div>
        <!-- <div class="notification"><a class="notification-pop" href="#">&nbsp; <span><?php //echo isset($myprofile['total_notifications']) && $myprofile['total_notifications'] > 0 ? $myprofile['total_notifications'] : '&nbsp;'; ?></span></a></div>
         -->
		<?php //$this->load->view('global-search'); ?>
    </div>
</div> <!-- header -->
<!-- Shows in Responsive view -->
    <div class="snap-drawers media-show">
           <div class="snap-drawer snap-drawer-left" id="left-drawer">
		            <div>
                <div class="small-profile">
                    <div class="profile-info">
                        <a href="<?php echo site_url(); ?><?php echo $myprofile['reply_username'];?>"><span class="prfl-pic"><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $myprofile['profile_pic'];?>" width="25" height="25" alt="<?php echo $myprofile['display_name']; ?>"></span><b><?php echo substr(strtok($myprofile['display_name'], " "), 0, 8); ?></b><span class="edit-profile">&nbsp;</span></a>
                    </div>
                </div>
                <ul id="menu">
                    <li class="notification"><a href="<?php echo site_url(); ?>notifications"><span><?php echo isset($myprofile['total_notifications']) && $myprofile['total_notifications'] > 0 ? $myprofile['total_notifications'] : '&nbsp;'; ?></span>Notification</a></li>
                    <li><a href="<?php echo site_url(); ?>people"><span id="media-people">&nbsp;</span>People</a></li>
                    <li><a href="<?php echo site_url(); ?>files"><span id="media-files">&nbsp;</span>Files</a></li>
                    <li><a href="<?php echo site_url(); ?>news"><span id="media-news">&nbsp;</span>News</a></li>
                    <li><a href="<?php echo site_url(); ?>offers"><span id="media-offers">&nbsp;</span>Offers</a></li>
                    <li><a href="<?php echo site_url(); ?>announcement"><span id="media-announcements">&nbsp;</span>Announcements</a></li>
                    <li><a href="<?php echo site_url(); ?>photos"><span id="media-photos">&nbsp;</span>Photos</a></li>
                    <li><a href="<?php echo site_url(); ?>concepts"><span id="media-concepts">&nbsp;</span>Concepts</a></li>
                    <li><a href="<?php echo site_url(); ?>groups"><span id="media-groups">&nbsp;</span>Groups</a></li>
                    <li><a href="<?php echo site_url(); ?>market"><span id="media-marketplace">&nbsp;</span>Marketplace</a></li>
                    <li><a href="<?php echo site_url(); ?>jobs"><span id="media-jobs">&nbsp;</span>Jobs</a></li>
                    <!-- More DropDown -->
					<li><a href="#" class="nav-drop-down"><span class="more-plus">&nbsp;</span>More</a>
                        <div class="media-dropdown">
                            <ol>
                                <li><a href="<?php echo site_url().$myprofile['reply_username'] ; ?>"><span id="media-mprofile">&nbsp;</span>My Profile</a></li>
                                <li><a href="#edit-profile-box-w" class="pass_change"><span id="media-cpassword">&nbsp;</span>Change Password</a></li>
                                <li><a href="#submit_feedback" class="feedback_open"><span id="media-feedback">&nbsp;</span>Feedback</a></li>
								<?php if($myprofile['role_id'] == $permission_roles['SA'] || $myprofile['role_id'] == $permission_roles['CONTM']) { ?>
                                <li><a href="<?php echo site_url(); ?>manage"><span id="media-mintranet">&nbsp;</span>Manage Intranet</a></li>
								<?php } ?>
                                <!--<li><a href="<?php echo site_url(); ?>"><span id="media-messenger">&nbsp;</span>Instant Messenger</a></li>-->
                                <li><a href="#submit_feedback" class="feedback_open contactus" title="Contact Us"><span id="media-contactus">&nbsp;</span>Contact Us</a></li>
                                <li><a href="<?php echo site_url(); ?>help"><span id="media-help">&nbsp;</span>Help</a></li>
                                <!--<li><a href="<?php echo site_url(); ?>"><span id="media-whatnew">&nbsp;</span>What's new</a></li>-->
                                <li><a href="<?php echo site_url(); ?>release_notes"><span id="media-rnotes">&nbsp;</span>Release Notes</a></li>
                                <li><a href="<?php echo site_url(); ?>code_of_conduct"><span id="media-codeconduct">&nbsp;</span>Code of Conduct</a></li>
                            </ol>
                        </div>
                    </li>
                    <!-- End More DropDown -->
                    <li><a href="<?php echo site_url(); ?>logout"><span id="media-logout">&nbsp;</span>Log Out</a></li>
                </ul>
            </div>
        </div>
          <div class="snap-drawer snap-drawer-right" id="right-drawer">
            <div>
            </div>
        </div>
    </div>

    <div id="content" class="snap-content">
        <div id="media-header" class="media-show">
            <div class="media-menu" id="toggle-left">&nbsp;</div>
            <div class="search">
                  <form id="fGMobileSearch" name="search" action="<?php echo site_url(); ?>people" method="get">
                    <fieldset>
                        <div class=" textC">
                      <input id="txtGlbSearchMobile" name="txtSearch" type="text" placeholder="Who are you searching for?" />
                      <input type="button" id="glbGoMobile" class="btn-go-inline" name="go" value="Go" />
                    </div>
                    </fieldset>
                </form>
            </div>
            <div class="widget-menu" id="toggle-right">&nbsp;</div>
        </div>
        
        <div class="content">
            <div class="spacer"></div>