<!DOCTYPE HTML>
<html lang="en">
<head>
        <?php $ci = get_instance(); // CI_Loader instance
					$ci->load->config('intranet');
				?>
<meta name="robots" content="index, nofollow">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <!-- Sets initial viewport load and disables zooming	-->
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
  <!-- Makes your prototype chrome-less once bookmarked to your phone's home screen -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <!-- Set Apple icons for when prototype is saved to home screen -->
  <!-- <link rel="apple-touch-icon-precomposed" sizes="114x114" href="touch-icons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="touch-icons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="touch-icons/apple-touch-icon-57x57.png"> -->
  
  <link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
  <title>Login - Landmark Group</title>
  <link rel="stylesheet" type="text/css" href="http://fast.fonts.net/cssapi/a7307b6d-4641-44d1-b16a-c22888f05455.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/default.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/common.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/login.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/media.css?v=<?php echo $ci->config->item('css_ver'); ?>" />
        <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/ie.css" media="all">
  <![endif]-->
  <!--[if gte IE 9]>
    <style type="text/css">
      .gradient { filter: none; }
    </style>
  <![endif]-->
  <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
  <!--[if IE 8]>     <html class="ie8"> <![endif]-->
  <!--[if IE 9]>     <html class="ie9"> <![endif]-->
  <!--[if !IE]><!--> <html>             <!--<![endif]-->
    </head>
    <body>
         <!--<img src="<?php /*?><?php echo site_url(); ?><?php */?>media/images/loading1.gif" alt="" style="display: none;">-->
        <form method="post" action="http://devnet.landmarkgroup.com/" id="fom" style="display:none">
          <input type="hidden" name="client_id" value="test_client_id">
          <input type="hidden" name="client_secret" value="test_client_secret_id">
          <input type="hidden" name="grant_type" value="password">
          <input type="hidden" name="redirect_url" value="<?php echo current_url(); ?>">
          <input type="submit" name="oauth_submit" >
        </form>
        
 <script src="<?php echo site_url(); ?>media/js/all.js?v=<?php echo $ci->config->item('css_ver'); ?>"></script>
 <?php $this->load->view('partials/js_footer'); ?>
        
      <script type="text/javascript">       
        $(document).ready(function() {
          $("#fom").submit();
        });       
      </script>

      <?php exit; ?>
       
    </body>
</html>