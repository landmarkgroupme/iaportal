<?php
if(isset($groups) && !empty($groups)):
?>
    	<div class="widget clearfix">
          	<div class="widget-head groups">
                <span class="view-all">&#8226; <a href="#view-all-the-groups" id="groups-all-view" class="fancybox">View All</a></span>
                <h3>Groups</h3>
            </div>
            <div class="box shadow-2">
				<ul>
				<?php
				$count= 0;
				foreach($groups as $group): ?>
					<li class="clearfix">
						<a href="<?php echo site_url(); ?>groups/<?php echo strtolower($group['id']); ?>" class="gr-tn"><img class="round-c" alt="Fun City" src="<?php echo site_url(); ?>images/groups/thumbs/<?php echo $group['thumbnail']; ?>"><div class="gr-title"><?php echo ucwords(strtolower($group['name'])); ?></div></a>
					</li>
					<?php 
				$count++;
				if($count == 4)
				{
					break;
				}
				endforeach; ?>
              </ul>
          </div>
        </div>
				
	<div id="view-all-the-groups" class="bd-all-box fb-container-box d-n">

	<?php foreach($groups as $group): ?>
		<div class="pop-bd clearfix">
			<a href="<?php echo site_url(); ?>groups/<?php echo strtolower($group['id']); ?>" class="normal-text"><img src="<?php echo site_url(); ?>images/groups/thumbs/<?php echo $group['thumbnail']; ?>" width="25" height="25" alt="Jesse Lander" class="round-c"/><?php echo ucwords(strtolower($group['name'])); ?></a>
		</div>
		<?php if ($group !== end($groups)): ?>
        	<hr class="dotted"/>
		<?php endif; ?>
	<?php endforeach; ?>
	</div>
		
<?php endif; ?>