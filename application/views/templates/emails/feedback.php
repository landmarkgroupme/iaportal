<p>Hi,<br>
A feedback has been posted at 
<a href='http://www.intranet.landmarkgroup.com'>www.intranet.landmarkgroup.com</a> 
which you might be interested in.</p>
<table cellpadding='0' border='0'>
	<tr>
		<td width='30%' style='border-bottom:1px solid #cccccc;padding:3px 0px;'>Feedback from</td>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $name . "</td>
	</tr>
	<tr>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>You can contact the sender at</td>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $contact . "</td>
	</tr>
	<tr>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>Feedback category</td>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $query . "</td>
	</tr>
	<tr>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;' valign='top'>Feedback</td>
		<td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $desc . "</td>
	</tr>
</table>
<br>
<p>Admin,<br>
	<a href='http://www.intranet.landmarkgroup.com'>www.intranet.landmarkgroup.com</a>
</p>
<p>NOTE: This is a system generated email, please do not try to reply to the email.</p>