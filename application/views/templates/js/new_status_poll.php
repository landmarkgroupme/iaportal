<script type="text/template" id="new_status_poll">
 <div class="block-1 clearfix" id="<%- response.poll_id %>">
            	<div class="rp-avtar"><span><a href="<%- siteurl %><%- response.latest_poll.username %>"><img src="<%- siteurl %><%- response.latest_poll.profile_pic %>" class="shadow-3 cs-clip" width="55" height="55" alt="<%- response.latest_poll.display_name %>"/></a></span></div>
                <div class="rp-contents">
                    <h2><a href="<%- siteurl %><%- response.latest_poll.username %>"><%- response.latest_poll.display_name %></a></h2>
                    <div class="poll-box">
	
					<ul>
                   <li class="question"><b><%- response.latest_poll.title %></b></li>
						<form method="post" class="vPoll" id="vPoll_<%- response.latest_poll.poll_id %>">      
				<input type="hidden" id="poll_id" value="<%- response.latest_poll.poll_id %>" />
						<li> <% _.each( response.latest_poll.options, function( poll ){ %>
							<label for="options"><input type="radio" value="<%- poll.option_id %>" name="options"> <%- poll.title %></label>
							<% });%>  
						</li>
						<li> <label><input type="button" class="btn-sm submit" name="submit" value="Vote" id="vpoll_submit" /></label></li>
						</form>
                    </li>
                </ul>
				<ul id="vPollResponse_<%- response.latest_poll.poll_id %>" class="PollResponse"></ul>
				<div class="rp-social">&#8226; <a href="#" class="pollDelete">Delete</a>&#8226; <a href="#" class="featured_polls">Mark as featured</a><span>a few seconds ago</span></div>
</div>
                  
</div>
</div>


						
</script>