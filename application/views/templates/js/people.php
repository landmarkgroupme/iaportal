<script type="text/template" id="people_card">
<%
_.each( rc, function( item ){
	%>
	<div class="block-follow box-fl-l" id="<%- item.id %>">
		<div class="box shadow-2">
			<div class="follow-box-c">
				<a href="<%- siteurl %><%- item.reply_username %>">
					<img style="border: 1px solid #DEDEDE;border-radius: 62px;" class="avatar" src="<%- siteurl %>images/user-images/25x25/<%- item.profile_pic %>" />
				</a>
				<%
				var name =  item.first_name + (item.middle_name == '' ? '' : ' '+ item.middle_name) + (item.last_name == '' ? '' : ' '+ item.last_name) %>
            	<h2 title="<%- item.fullname %>"><a href="<%- siteurl %><%- item.reply_username %>"> <%- name.replace(/\\/g, '') %></a>
            	<span class="status">&nbsp;</span></h2>
           		<span><%- item.designation %>, <%- item.concept %> </span>
            <ul>
            <% if (item.mobile != '')  {%>
            	<li class="iconMobile"><a href=""><%- item.mobile %></a></li>
            <% } %>
            <%
            if (item.phone != '') { %>
            	<li class="iconTelephone"><a href=""><%- item.phone %></a></li>
            <% } %>
			<% if (item.email != '') { %>
			 <li class="iconMail"><a href="mailto:<%- item.email %>"><%- item.email %></a></li>
			  <% } %>
    		</ul></div>
    		<div class="f-action">
			<% if(item.id != item.loged_user_id) { %>
    		<% if(item.follow_user > 0) { %>
    			<input type="button" class="btn-sm btn-follow btn-grn" name="submit" value="Following" user_id="<%-  item.id %>"/>
    		<% } else { %>
    			<input type="button" class="btn-sm btn-follow" name="submit" value="Follow" user_id="<%-  item.id %>"/>
    		<% } %>
				<span class="qTip" title="Follow Landmarkers to see their latest updates on your news feed.">?</span>
				<% } %>
				
				<li class="media-name">
				<% if (item.email != '') { %>
					<a href="mailto:<%- item.email %>" class="email" title="<%- item.email %>"><span>email</span></a>
				 <% } %>
					<a class="chat" href="javascript:void(0)" onclick="javascript:jqcc.cometchat.chatWith(<%- item.id %>);"><span>chat</span></a>
				</li>
				
    		</div>
    	</div>
	</div>
	<% 
		if ($.browser.msie  && parseInt($.browser.version, 10) === 8) {
} else {
	jqcc.cometchat.getUser(item.id,'checkstatus');
	}
	}); %>
</script>