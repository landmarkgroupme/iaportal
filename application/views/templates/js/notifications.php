<div id="collector" style="display:inline"></div>
<script type="text/template" id="notifications_temp">
<div class="notification-block clearfix">
	<span class="view-all">&#8226; <a href="<?php echo site_url().'notifications'; ?>">View All</a></span><div class="n-title">Notifications</div>
	<% _.each( rc.notifications, function( item,i ){%>
  	<ul class="rp-social-ext-block clearfix">
		<% if(item.activity_type == 'User_Status_Like') { 
			if(item.activity_source_type  == 'Status') {
			%>
		  <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"></li>
		  <li class="rp-ext-rgt"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.reply_username == myprofile.reply_username ? 'You' : item.fullname %></a></strong> likes your <a href="<%- siteurl %>status/<%- item.activity_source %>">Status</a><br/> <span><%- moment(item.time+ ' +00:00').fromNow() %></span></li>
    <% } else if(item.activity_source_type  == 'Photo') { %>
	<li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"></li>
		  <li class="rp-ext-rgt"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.reply_username == myprofile.reply_username ? 'You' : item.fullname %></a></strong> likes your <a href="<%- siteurl %>photo/<%- item.activity_source %>">post</a><br/> <span><%- moment(item.time+ ' +00:00').fromNow() %></span></li>
	
	<%	}
		}else if(item.activity_type == 'Invitation') { %>
		  <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"></li>
		  <li class="rp-ext-rgt"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.reply_username == myprofile.reply_username ? 'You' : item.fullname %></a></strong> has invited you to join <%= item.notification %><br/> <span><%- moment(item.time+ ' +00:00').fromNow() %></span></li>      
		<%
    } else if(item.activity_type == 'Group_File') { %>
		  <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"></li>
		  <li class="rp-ext-rgt"><%= item.notification %><br/> <span><%- moment(item.time+ ' +00:00').fromNow() %></span></li>      
		<% 
    } else if(item.activity_type == 'Comment') {%>
			<li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"></li>
		  <li class="rp-ext-rgt"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.reply_username == myprofile.reply_username ? 'You' : item.fullname %></a></strong> commented on your <a href="<%- siteurl %>status/<%- item.activity_source %>">Status</a><br/> <span><%- moment(item.time+ ' +00:00').fromNow() %></span></li>
		<% }
      else if(item.activity_type == 'Follow'){ %>
       <li class="rp-ext-lft"><a href="<%- siteurl %><%- item.reply_username %>"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"> </a></li>
      <li class="rp-ext-rgt"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.reply_username == myprofile.reply_username ? 'You' : item.fullname %></a></strong> is following you </li>
      <%}
	  else if(item.activity_type == 'Tag'){
	  if(item.activity_source_type == 'Status')
	  {
	  %>
       <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.fullname %></a></strong> tagged you in a <a href="<%- siteurl %>status/<%- item.activity_source %>">post</a></li>
	   <% } else if(item.activity_source_type == 'Photo') { %>
	   <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.fullname %></a></strong> tagged you in a <a href="<%- siteurl %>photo/<%- item.activity_source %>">post</a></li>
	   <% } else if(item.activity_source_type == 'Albums') { %>
	   <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.fullname %></a></strong> tagged you in a <a href="<%- siteurl %>about/view_photos/<%- item.activity_source %>">post</a></li>
	   <% } else if(item.activity_source_type == 'Marketplace') { %>
		 <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.fullname %></a></strong> tagged you in a <a href="<%- siteurl %>market/items_details/<%- item.activity_source %>">post</a></li>
		 
	   <% } else { %>
	   <li class="rp-ext-lft"><img width="25" height="25" alt="icon small" src="<%- siteurl %><%- item.profile_pic %>"><strong><a href="<%- siteurl %><%- item.reply_username %>"><%- item.fullname %></a></strong> tagged you in a <a href="<%- siteurl %><%- item.reply_username %>">post</a></li>
	   <% } %>
      <li class="rp-ext-rgt">
        <%= item.notification %>
      </li>
      <% }
    %>
		</ul>
	<% }); %>
	<% if(_.size(rc.notifications) < 1) { 
		$('.notification-pop span').css('opacity', '0');
		%>
		No Notifications
	<% }
	%>
</div>

</script>