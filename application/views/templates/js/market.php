<script type="text/template" id="marketplace">
<%
var counter = 1;
_.each( rc, function( item ){console.log(item);
var midbox = (counter == 2 || counter == 5) ? 'mid' : '';
	counter++;
	
%>

	<div class="market-box <%- midbox %>">
	<div class="xtra4-list-view">
	<span> <%- item.category_name %> </span>
	<span><%- item.added_on %></span>
		<% if (item.item_max_price != null && item.currency  != null && item.item_min_price  != null) { %>
		<span><mark><%- Number(item.item_min_price) %> - <%- Number(item.item_max_price) %></mark> <%- item.currency %></span>
	<% } else if(item.item_max_price != null && item.currency  != null) { %>
	<span><mark><%- Number(item.item_max_price) %></mark> <%- item.currency %></span>
<% }	
	else
	{
	%>
	<span><%- "Free" %></span>
	<% } %>
	</div>
	
	<a href="<%- siteurl %>market/items_details/<%-item.id %>"><img class="p-img-big" src="<%- item.item_image %>" alt="<%- item.title %>" /></a><br/>
	<span class="m-price"><%
	if(item.item_max_price && item.item_max_price != '' && item.item_min_price  != null) { %>
	<span><%-Number(item.item_min_price)%> - <%- Number(item.item_max_price)%> </span><%-item.currency%>
		
	<%} else if(item.item_max_price && item.item_max_price != '') { %>
	<span><%-Number(item.item_max_price)%> </span><%-item.currency%>
	<% }
	else 
	{
		print('Free');
	} 
	%></span>
	<% 
	title = toTitleCase(item.title); 
	if(title.length >= 50) { 
	title = title.substring(0,50);
	title = title+'...';
	 } 
	%>
	<div class="x-members"><a href="<%- siteurl %>market/items_details/<%-item.id %>"> <%-title %></a></div>
	<blockquote class="icon link"><%- item.category_name %></blockquote>
	<div class="p-followers-bx clearfix">
	<div class="p-followers">
		<div class="imgMembers">
			<a href="<%- siteurl %><%- item.reply_username %>"><img src="<%- siteurl %>images/user-images/25x25/<%- item.profile_pic %>" alt="p1"><b><%- toTitleCase(item.display_name.split(' ')[0]) %></b></a>
      <% if(item.user_id != myprofile.id) { %>
      <% } %>
			<% if(item.user_id != item.login_user_id) { %>			
			
			<span class="status market_user_<%-item.user_id%>">&nbsp;</span>
			<% 
				if ($.browser.msie  && parseInt($.browser.version, 10) === 8) {
				} else {
							jqcc.cometchat.getUser(item.user_id,'checkstatusmarket');
				}
			}
			%>
			<% if(myprofile.id != item.user_id) {%>
<p><span><a class="fancybox report_view" id="<%- item.id %>" href="#pop-win-market"><small>Report this item</small></a></span></p>
			<% }else{ %>
			<p><span>
			   <% if(item.item_type == 1 || item.item_type == 3){
      %>
<a class="fancybox addItemFan" id="<%- item.id %>" href="#add_item"><small>Edit this Market Item</small></a> &#8226;
      <%
         }
         else if(item.item_type == 2)
         {
      %>
<a class="fancybox addRequestFan" id="<%- item.id %>" href="#add_request"><small>Edit this Market Item</small></a>&#8226;
      <%
         } %>
         <a class="marketDeleteBt" id="<%- item.id %>" href="#add_request"><small>Delete</small></a>
        </span></p>
         <%
      }%>
		</div>
	</div>
	<div class="cp-btn-follow"></div>
	</div>
	</div>

	<% }); %>
<%
    $('.report_view').fancybox({
  beforeLoad : function() {
     //console.log(this.element[0].id);
     $('#txtmarketid').val(this.element[0].id);
     $('#txtarcomments-market').val('');
     $(".error-msg").html('');
    }
});%>
</script>