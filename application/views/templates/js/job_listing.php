<script type="text/template" id="joblisting_temp">
<% _.each( rc.jobs, function( job ){
	var description = JSON.parse(job.description);
	%>
	<dl class="job-listing clearfix">
		<dt><%- moment(job.date_posted).format("Do MMM YY") %></dt>
		<dd>
		        <h2><a href="<%- siteurl %>jobs/job/<%- job.id %>"><%- job.title %></a></h2>
		        <small><%- job.concept %> &#8226; <%- job.region %>, <%- job.territory %></small>
		        <p><% if( _.size(description.ul.li[0]) > 1){ %>
				<%- description.ul.li[0] %>
				<% } else { %>
				<%- description.ul.li %>
				 <% } %></p>
		        <div class="apply_share">
		            <a class="btn-sm apply_now_link apply_job_code_self apply_link" href="#refer_friend-box" rel="<%- job.code %>">Apply Now</a> &nbsp;
		            <a href="#refer_friend-box" class="apply_job_code grey-link refer_friend_link" rel="<%- job.code %>"><small>Refer a friend</small></a>
		        </div>
		</dd>
	</dl>
<% }); %>
</script>