<script type="text/template" id="people_list">
<%
var counter = 0;
_.each( rc, function( item ){
	var even = (counter % 2 == 0) ? ' col-even' : ' col-odd';
	var name =  item.first_name + (item.middle_name == '' ? '' : ' '+ item.middle_name) + (item.last_name == '' ? '' : ' '+ item.last_name);
	counter++;
	%>
<div class="row<%- even %>">
	<div class="col1">
		<ul class="basic-info">
			<li><a href="<%- siteurl %><%- item.reply_username %>"><img class="avatar shadow-3 cs-clip" src="<%- siteurl %>images/user-images/25x25/<%- item.profile_pic %>" /></a></li>
			<li><h2><a href="<%- siteurl %><%- item.reply_username %>"><%- name.replace(/\\/g, '') %></a><span class="status">&nbsp;</span></h2>
			<span><%- item.designation %>, <%- item.concept %>, <%- item.country %></span>
			</li>
			<li>
				<% if (item.email != '')  {%>
				<a href="mailto:<%- item.email %>" class="email" title="<%- item.email %>"><span>email</span></a>
				<% } %>
				<a class="chat" href="javascript:void(0)" onclick="javascript:jqcc.cometchat.chatWith(<%- item.id %>);"><span>chat</span></a>
			</li>
		</ul>
	</div>
	<div class="col2">
		<ul class="contact-info">
			<li>
			<%
            if (item.phone != '') { %>
            	<span class="iconTelephone"><a href=""><%- item.phone %></a></span>
            <% } %>
			<% if (item.mobile != '')  {%>
            	<span class="iconMobile"><a href=""><%- item.mobile %></a></span>
            <% } %>
			</li>
			<li class="ext"><%- item.extension %></li>
			<li>
			<% if(item.id != item.loged_user_id) { %>
			<% if(item.follow_user > 0) { %>
    			<input type="button" class="btn-sm btn-follow btn-grn" name="submit" value="Following" user_id="<%-  item.id %>"/>
    		<% } else { %>
    			<input type="button" class="btn-sm btn-follow" name="submit" value="Follow" user_id="<%-  item.id %>"/>
    		<% } %>
			<% } %>
    		</li>
		</ul>
	</div>
</div>
<% 
	if ($.browser.msie  && parseInt($.browser.version, 10) === 8) {
} else {
	jqcc.cometchat.getUser(item.id,'checkstatus');
	}
}); %>
</script>