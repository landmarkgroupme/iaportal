<script type="text/template" id="new_upload_image">
<%
replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
replacedText = response.title;
replacedText = replacedText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>'); 
if(replacedText == response.title)
{
	replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
	replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
}
replacePattern3 = /@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/gim;
replacedText = replacedText.replace(replacePattern3, '<a href="'+siteurl+'$1">$2</a>')
%>
      <dl class="block-2 clearfix" id="<%- response.id %>" object_id="<%- myprofile.id %>" object_type="User" content_type = "Photo">
	  <% if(response.owner_type == "Concept") {%>
				<dt><a href="<%- siteurl %>concepts/<%- concept_slug %>"><img src="<%- siteurl %>images/concepts/thumbs/<%- concept_thumb %>" class="shadow-3 cs-clip" width="55" height="55" alt="<%- concept_name %>"/></a></dt>
				<% } else { %>
				<dt><a href="<%- siteurl %><%- myprofile.reply_username %>"><img src="<%- siteurl %>images/user-images/105x101/<%- myprofile.profile_pic %>" class="shadow-3 cs-clip" width="55" height="55" alt="<%- myprofile.display_name %>"/></a></dt>
				<% } %>
				<dd>
					<div class="rp2-contents clearfix">
					<% if(response.owner_type == "Concept") {%>
						<h4><a href="<%- siteurl %>concepts/<%- concept_slug %>"><%- concept_name %></a></h4>
						<% } else { %>
						<h4><a href="<%- siteurl %><%- myprofile.reply_username %>"><%- myprofile.display_name %></a></h4>
						<% } %>
						<div>	 
               <a class="fancybox" rel="fancybox-thumb" href="<%- siteurl %><%- response.big_image_url %>"><img class="lazy" src="<%- siteurl %><%- response.url %>" alt="" /></a>
							<div class = "readmore"><p><%= replacedText %></p></div>
							<div class="rp-social"> <a href="#" class="bLike">Like</a> &#8226; <a href="#" class="pComment">Comment</a> &#8226; <a href="#" class="pDelete">Delete</a>&#8226; <a href="#" class="featured">Mark as featured</a><span><a href="<%- siteurl %>photo/<%- response.id %>"> a few seconds ago</a></span></div>

						</div>
					</div>

				</dd>
				<dd class="comments-block">
					<div class="rp-social-like clearfix d-n">
					</div>
				</dd>
				<dd class="comments-block">

					<div class="rp-social-ext clearfix">
					<span class="top-arrow">&nbsp;</span>
					<ul class="post-cmntlike post-cmntlike-block d-n">
					    <li class="cmntlike-icon"><img src="<%- siteurl %>media/images/icons/thumbs_up.png" width="15" height="15" alt=""></li>
						<li class="no-cmntlike">
						</li>
					</ul>
					<ul class="post-cmntlike last-ul" id="check-last-ul-<%- response.id %>">
					<% if(typeof concept_manager != 'undefined' && concept_manager != 0 && typeof concept_thumb != 'undefined') { %>
					      <li class="picture rp-ext-lft"><a href="#"><img src="<%- siteurl %>/images/concepts/thumbs/<%-concept_thumb%>" width = "25px" height = "25px" alt=""></a></li>
					<% } else { %>
					<li class="picture rp-ext-lft"><a href="#"><img src="<%- siteurl %>images/user-images/25x25/<%-myprofile.profile_pic%>" width="25" height="25" alt=""></a></li>
					<% } %>
						  <li class="add-comment">
						      <form id="fComment" name="fComment" action="ssi/ajax_response.php" method="post">
								  <fieldset>
						  <div class="rp-social-comment-box textC">
							<textarea id="txtComment_<%- response.id %>" name="txtComment" class="tagged_text_comment rp-comment" style="height: 25px;" placeholder="Add a comment..."></textarea>
						  </div>
						  <fieldset>
							  </form>
					      </li>
					 </ul> 
					</div>

				</dd>

			</dl>
</script>