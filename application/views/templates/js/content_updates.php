<script type="text/template" class="template">
<% if(rc.feed.length > 0){
%>
<% _.each( rc.feed, function( item ){ %>
	<% if(item.category == 'status updates' || item.category == 'photo' || item.category == 'album' ||( _.indexOf(['news', 'offers', 'announcement'], item.category) > -1 && item.concept != null && item.concept_url != null )|| item.category == 'marketplace' || item.category == 'widgets') { %>
	<%if(item.category == 'status updates')
	{ %>
		<dl class="block-2 clearfix <% if(item.featured==1){print("feature");}%>" data-post="<%- %>" id="<%- item.id %>" object_id="<%- item.object_id %>" object_type="User" target_id="<%- item.target_id %>" target_type ="<%- item.target_type %>" content_type = "Status">
		<dt>
            <a href="<%- siteurl %><%- item.username %>"><img
                src="<%- siteurl %><%- item.profile_pic %>?<%- rc.current_time %>" class="shadow-3 cs-clip" width="55"
                height="55" alt="<%- item.display_name %> this"/></a>
        <h4 class="media-name">
				<a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
				<% if(item.target_name) { %>
					<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
				<% } %>
			</h4>
		</dt>
				<dd>
					<div class="rp2-contents clearfix">
						<h4 class="profile-name">
							<a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
							<% if(item.target_name) { %>
								<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
							<% } %>
						</h4>

						<div>
						<div class = "readmore">
							<p><%= $('<div/>').html(item.message).text() %> </p>
						</div>
						
							<div class="rp-social"><a href="#" class="bLike active"><%
						if(_.isElement(item.you_like) || item.you_like > 0) {
							print('Unlike');
						} else {
							print('Like');
						}
						%></a> &#8226; <a href="#" class="bComment">Comment</a>
						<% if(item.object_id == myprofile.id && item.object_type == 'User') { %>
							&#8226; <a href="#" class="bDelete">Delete</a>
						<% } else if ( typeof concept_manager != 'undefined' && concept_manager == 1 && item.object_type == 'Concept' ) { %>
						&#8226; <a href="#" class="bDelete">Delete</a>
						<% } %>
            <% if(item.object_id == myprofile.id || item.object_type == "Concept") { } else {%>

             &#8226;<a class="report_view" id="<%- item.id %>" href="#pop-win">Report</a>
            <% } 
			if(myprofile.role_id==1){ %>
			 &#8226; <a href="#" class="featured"><%
						if(item.featured==1) {
							print('Featured');
						} else {
							print('Mark as featured');
						}
						%></a>
			<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>

						<span><a href="<%- siteurl %>status/<%- item.id %>"><%- item.time_ago %></a></span>
            </div>
						</div>
					</div>
				</dd>
	<%}
else if(item.category == 'photo' )
{%>
  <dl class="block-2 clearfix <% if(item.featured==1){print("feature");}%>" id="<%- item.id %>" object_id="<%- item.object_id %>" object_type="User" content_type = "Photo" >
				<dt>
                    <a href="<%- siteurl %><%- item.username %>"><img
                        src="<%- siteurl %><%- item.profile_pic %>?<%- rc.current_time %>" class="shadow-3 cs-clip"
                        width="55" height="55" alt="<%- item.display_name %>"/></a>
                <h4 class="media-name"><a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
					<% if(item.target_name) { %>
							<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
						<% } %>
					</h4>
			  </dt>
				<dd>
					<div class="rp2-contents clearfix">
						<h4 class="profile-name"><a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
						<% if(item.target_name) { %>
								<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
							<% } %>
						</h4>
						<div>
              <a class="fancybox" rel="fancybox-thumb" href="<%- siteurl %><%- item.album_id %>"><img class="lazy" src="<%- siteurl %><%- item.thumbnail %>" alt="<%- item.title %>" /></a>
							<div>
					<div class = "readmore">		
              <p><%= $('<div/>').html(item.title).text() %></p>
			  </div>
              <div class="rp-social"> <a href="#" class="bLike active"><%
						if(_.isElement(item.you_like) || item.you_like > 0) {
							print('Unlike');
						} else {
							print('Like');
						}
						%></a> &#8226;<a href="#" class="pComment">Comment</a>

              <% if(item.object_id == myprofile.id || ( typeof concept_manager != 'undefined' && concept_manager == 1 && item.object_type == 'Concept' )) { %>
              &#8226; <a href="#" class="pDelete">Delete</a>
              <% } 
			  if(myprofile.role_id==1){ %>
			&#8226; <a href="#" class="featured"><%
						if(item.featured==1) {
							print('Featured');
						} else {
							print('Mark as featured');
						}
						%></a>
			<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
              <span><a href="<%- siteurl %>photo/<%- item.id %>"><%- item.time_ago %></a></span></div>
              </div>
              
						</div>
					</div>

				</dd>
<%} else if(item.category == 'album' ) { %>
<dl class="block-1 block-2 clearfix <% if(item.featured==1){print("feature");}%>" id="<%- item.id %>" object_id="<%- item.id %>" object_type="User" content_type = "Albums" >
		<dt>
		  <a href="<%- siteurl %><%- item.username %>"><img src="<%- siteurl %>images/concepts/thumbs/<%- item.thumbnail %>" class="shadow-3 cs-clip" width="55" height="55" alt="<%- item.concept_name %>"/></a>
			<h4 class="media-name">
				<a href="<%- siteurl %>about/view_photos/<%- item.id %>"><%- item.title %></a>
			</h4>
		</dt>
				<dd>
		        <div class="rp-contents clearfix">
		            <h4 class="profile-name">
						<a href="<%- siteurl %>about/view_photos/<%- item.id %>"><%- item.title %></a>
					</h4>
		            <p><%- item.description.substring(0, 100) %>... </p>
		            <div class="img-blk-main">
		            	<%
						_.each( item.photos, function( photo ){ %>
		            	<a href="<%- siteurl %>about/view_photos/<%- item.id %>"><img alt="WOW Mailer" src="<%- siteurl %>phpalbum/albums/<%- photo.filepath + '/thumb_' + photo.filename %>" width="150"/></a>
		                <% });%>
		            </div>
		            <div class="rp-social clearfix">
					<a href="#" class="bLike active">
					<% if(_.isElement(item.you_like) || item.you_like > 0) {
							print('Unlike');
						} else {
							print('Like');
						}
					%>
					</a> 
					&#8226; <a href="#" class="bComment">Comment</a> 
					<% if( typeof concept_manager != 'undefined' && concept_manager == 1 ) { %>
					&#8226; <a href="#" class="paDelete">Delete</a> 
					<% } %>
					<% if( myprofile.role_id == 1 ){ %>
					&#8226; <a href="#" class="featured">
						<% if( item.featured == 1 ) {
							print('Featured');
						} else {
							print('Mark as featured');
						} %>
					</a>
					<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
					<span><%- item.time_ago %></span>    
		            </div>
		      </div>
			  </dd>
<% } else if(item.category == 'widgets' ) { %>
<dl class="block-1 block-2 clearfix <% if(item.featured==1){print("feature");}%>" id="<%- item.id %>" object_id="<%- item.id %>" object_type="User" content_type = "Widget" >
    <dt>
      <a href="<%- siteurl %><%- item.username %>"><img src="<%- siteurl %>images/concepts/thumbs/<%- item.thumbnail %>" class="shadow-3 cs-clip" width="55" height="55" alt="<%- item.concept_name %>"/></a>
    <h4 class="media-name">
      <a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
    </h4>
    </dt>
    <dd>
      <div class="rp2-contents clearfix">
        <h4 class="profile-name">
          <a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
        </h4>
        <p><a href="<%- siteurl %><%- item.username %>/pages/<%- item.id %>"><%- item.title %></a></p>
        <div class="rp-social clearfix"><a href="#" class="bLike active"><%
						if(_.isElement(item.you_like) || item.you_like > 0) {
							print('Unlike');
						} else {
							print('Like');
						}
						%></a> &#8226; <a href="#" class="bComment">Comment</a> <% if( typeof concept_manager != 'undefined' && concept_manager == 1 ) { %>
              &#8226; <a href="#" class="paDelete">Delete</a>
              <% } if(myprofile.role_id==1){ %>
			&#8226; <a href="#" class="featured"><%
						if(item.featured==1) {
							print('Featured');
						} else {
							print('Mark as featured');
						}
						%></a>
			<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
			<span> <%- item.time_ago %></span>

		            </div>
      </div>
    </dd>
<% } else if(_.indexOf(['news', 'offers', 'announcement'], item.category) > -1) { %>
		<% 
		content_type = item.category;
		content_type = content_type.slice(0,1).toUpperCase() + content_type.slice(1);
		if(item.concept != null && item.concept_url != null)
		{
		%>
		<dl class="block-2 clearfix news <% if(item.featured==1){print("feature");}%>" id="<%- item.id %>" object_type="User" object_type="<%- item.category %>" content_type="<%- content_type %>">
				<dt>
				<a href="<%- siteurl %>concepts/<%- item.concept_url.toLowerCase().replace(' ', '-') %>"><img src="<%- siteurl %>images/concepts/thumbs/<%- item.profile_pic ? item.profile_pic : 'default/1.png' %>" class="shadow-3 cs-clip" width="55" height="55" alt="<%- item.concept %>" /></a>
				
			<h4 class="media-name">
				<a href="<%- siteurl %>concepts/<%- item.concept_url.toLowerCase().replace(' ', '-') %>"><%- item.concept %></a>
			</h4>
			</dt>
				<dd>
					<div class="rp2-contents clearfix">
						<h4 class="profile-name">
						<a href="<%- siteurl %>concepts/<%- item.concept_url.toLowerCase().replace(' ', '-') %>"><%- item.concept %></a>
						
						</h4>
						<!-- <span class="ph"><a href="#"><img alt="<%- item.concept %>" src="<%- siteurl %>media/images/imgFunCity_sm.png" /></a></span> -->
						<div>
							<h1><a href="<%- siteurl %><%- item.category %>/<%- item.id %>"><%- item.title %></a></h1>
							<p><%- item.description.substring(0, 100) %>...</p>
							
			<div class="rp-social">
			  <a href="#" class="bLike active"><%
						if(_.isElement(item.you_like) || item.you_like > 0) {
							print('Unlike');
						} else {
							print('Like');
						}
						%></a> &#8226; <a href="#" class="bComment">Comment</a>
						&#8226;<a href="<%- siteurl %><%- item.category %>"><%- content_type %></a> 
						<% if( typeof concept_manager != 'undefined' && concept_manager == 1 ) { %>
              &#8226; <a href="#" class="noaDelete">Delete</a>&#8226;<a  href="#upload_file-box" class="editnoabox" id="noa_<%- item.id %>" >Edit</a> 
              <% } %> <% if(myprofile.role_id==1){ %>
			&#8226; <a href="#" class="featured"><%
						if(item.featured==1) {
							print('Featured');
						} else {
							print('Mark as featured');
						}
						%></a>
		<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
			<% if(item.offer_expired == 'yes') { %>
								&#8226; <a class="expired">Expired</a>
							<% } %>
							<span><%- item.time_ago %></span>
							</div>
						</div>
					</div>

				</dd>
			<% 
		} 
		%>	

	<% } %>
	
	<%if(item.category == 'marketplace'){ 
		if(item.title.length >= 35) { 
	title = item.title.substring(0,35);
	title = title+'...';
	 } else {
	 title = item.title;
	 }
	%>
	<dl class="block-1 block-2 clearfix <% if(item.featured==1){print("feature");}%>" data-post="<%- %>" id="<%- item.id %>" object_id="<%- item.object_id %>" object_type="User" target_id="<%- item.target_id %>" target_type ="<%- item.target_type %>" content_type = "Marketplace">
		<dt>
            <a href="<%- siteurl %><%- item.username %>"><img
                src="<%- siteurl %><%- item.profile_pic %>?<%- rc.current_time %>" class="shadow-3 cs-clip" width="55"
                height="55" alt="<%- item.display_name %>"/>
			</a>
            <h4 class="media-name">
				<a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
				<% if(item.target_name) { %>
					<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
				<% } %>
			</h4>
		</dt>

		<dd>
			<div class="rp-contents market-box-feed clearfix">
				<h4 class="profile-name"><a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a> <a class="arw-drop"></a>
				</h4>
				<div class="rp-l2 clearfix market-home">
					<div class="rp-img-block"><a href="<%- siteurl %>market/items_details/<%- item.id %>"><img class="lazy" src="<%- item.thumbnail %>"  alt="<%- item.title %>" /></a>    
					</div>
					<div class="rp-txt-block">
						<h2><a href="<%- siteurl %>market/items_details/<%- item.id %>"><%- title %></a> <span class="gt"><img src="media/images/arr.jpg" alt="" width="10"/> </span>
						<% if(item.item_max_price && item.item_max_price != ''){ %>
							<span class="m-price"><span><%- item.item_max_price %> </span><%- item.currency %></span>
						<% } else { %>
							<span class="m-price"><span> Free </span></span>
						<% } %>
						</h2>
						<p class="type"><%- item.item_category %></p>
					</div> 
				</div> 
				<div class="rp-social"><a href="#" class="bLike active"><%
					if(_.isElement(item.you_like) || item.you_like > 0) {
						print('Unlike');
					} else {
						print('Like');
					}%></a> &#8226; <a href="#" class="bComment">Comment</a>
          <% if(myprofile.id != item.object_id) { %>
		    &#8226;<a href="<%- siteurl %>market">Marketplace</a>
		  &#8226;<a class="fancybox report_view" id="<%- item.id %>" href="#pop-win-market">Report</a> 
		  <% } else{ %>
		    &#8226;<a href="<%- siteurl %>market">Marketplace</a>
		  &#8226;<a class="marketDeleteBt" data-id="<%- item.id %>" href="#">Delete</a>
		  <% } %>
         <% if(myprofile.role_id==1){ %>
			&#8226; <a href="#" class="featured"><%
						if(item.featured==1) {
							print('Featured');
						} else {
							print('Mark as featured');
						}
						%></a>
		<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
			 <span><%- item.time_ago %></span>

				</div>				
			</div>
		</dd>
	<% } %>
				
				
				<% if(item.total_likes == 0)
						{
							show1 = "d-n";
						} else {
							show1 = "";
						} %>
				<dd class="comments-block">

					<div class="rp-social-ext clearfix">
					<span class="top-arrow">&nbsp;</span>
					<ul class="post-cmntlike post-cmntlike-block  <%- show1 %>">
					    <li class="cmntlike-icon"><img src="<%- siteurl %>media/images/icons/thumbs_up.png" width="15" height="15" alt=""></li>
						
						<li class="no-cmntlike"><%
						if(_.isElement(item.you_like) || item.you_like > 0) {
							if(item.total_likes == 1)
								print('You like this');
							else if(item.total_likes == 2)
							{
							_.each( item.likes, function( like_user ){ 
								%>
								You and <a href="<%- siteurl %><%- like_user.reply_username%>"><var><%- like_user.first_name %></var></a> like this.
								<%
								   });
							}
							else if(item.total_likes == 3)
							{
								
								user = 'You';
								_.each( item.likes, function( like_user ){ 
								
								user = user+', '+'<a href="'+ siteurl + like_user.reply_username+'"><var>'+like_user.first_name+'</var></a>';
								
								 });
								print(user); %> like this. <%
								  
							}
							else
							{
								
								user = 'You';
								_.each( item.likes, function( like_user ){ 
								
								user = user+', '+'<a href="'+ siteurl + like_user.reply_username+'"><var>'+like_user.first_name+'</var></a>';
								
								 });
								print(user); %> and <%-(item.total_likes -3 )%> <a href="#others_view_list" id="others_<%- item.id %>" class="other_users">others</a> like this <%
								  
							}

						} else {
							if(item.total_likes == 1)
							{
								_.each( item.likes, function( like_user ){ 
								%>
								<a href="<%- siteurl %><%- like_user.reply_username%>"><var><%- like_user.first_name %></var></a> likes this.
								<%
								   });
							}	
							else if(item.total_likes == 2)
							{
								user = '';
								_.each( item.likes, function( like_user ){ 
									if(user == '')
									{
										sep = '';
									}
									else
									{
										sep = ', ';
									}
								user = user+sep+'<a href="'+ siteurl +like_user.reply_username+'"><var>'+like_user.first_name+'</var></a>';
								
								 });
								print(user); %> like this. <%
							}
							else if(item.total_likes > 2)
							{
								user = '';
								_.each( item.likes, function( like_user ){ 
									if(user == '')
									{
										sep = '';
									}
									else
									{
										sep = ', ';
									}
								
								user = user+sep+'<a href="'+siteurl+like_user.reply_username+'"><var>'+like_user.first_name+'</var></a>';
								
								 });
								print(user); %> and <%-(item.total_likes -2 )%> <a href="#others_view_list" id="others_<%- item.id %>" class="other_users">others</a> like this <%
							}
						}
						%></li>
					</ul>
					<% if(item.total_comments > 5) { %>
					<ul class="post-cmntlike">
					    <li class="cmntlike-icon"><img src="<%- siteurl %>media/images/icons/comments.png" width="15" height="15" alt=""></li>
						<li class="no-cmntlike"><a href="#" class="more-comments" status_id="<%- item.id %>" user_id="<%- item.object_id %>">View <%- (item.total_comments-5) %> more comments</a></li>
					</ul>
					<% } %>
						<% if(_.size(item.comments) >= 5) { %>
							<ul class="rp-social-ext-text  <%- _.size(item.comments) < 5 ? 'd-n' : 'd-n' %>">
								<li class="icon_comment">icon</li>
								<% if(_.size(item.comments) == 0) { %>
									<li><a href="#">Be the first to Comment</a></li>
								<% } else if(_.size(item.comments) >= 5) { %>
									<li><a href="#">Load more comments</a></li>
								<% } %>
							</ul>
						<% } %>
						 
					<%
					var counter = 0;
					_.each( item.comments.reverse(), function( comment ){ %>
						<% if(counter  < (_.size(item.comments)-5))
						{
							dis = "d-n";
						} else {
							dis = "";
						} %>
                      <ul class="post-cmntlike <%- dis %> cmntlike-<%- item.id %>" id="<%- comment.id %>" object_id="<%- item.object_id %>" object_type="User" >
                          <li class="picture"><img src="<%- siteurl %><%- comment.profile_pic %>" width="25" height="25" alt="icon small"></li>
                         <li class="comnt-info"><a href="<%- siteurl %><%- comment.reply_username %>"><%- comment.display_name %></a> <%= $('<div/>').html(comment.message).text() %>
							<br/><span><%- comment.time_ago %></span>
							&#8226; <a href="#" class="cmLike"><%
						if(_.isElement(comment.you_like) || comment.you_like > 0) {
							print('Unlike');
						} else {
							print('Like');
						}
						%></a>  <a  href="#others_comment_view_list" class="other_users_comment" id="other_comment_<%- comment.id %>" ><% if(comment.total_likes > 0) { print(' -'+comment.total_likes);} %></a> 
						</li>
						<% if(comment.object_id == myprofile.id && comment.object_type == 'User') { %>
							
							 <li class="hide"><a href="#" class="cDelete">x
							<div class="hide-tooltip"><span class="text">Remove this comment</span><span class="arrow"></span></div>
					    </a></li>
						<% }  else if ( typeof concept_manager != 'undefined' && concept_manager && comment.object_type == 'Concept' ) { %>
						
						 <li class="hide"><a href="#" class="cDelete">x
							<div class="hide-tooltip"><span class="text">Remove this comment</span><span class="arrow"></span></div>
					    </a></li>
						<% } %>
                        </li>
                      </ul>
                      <% counter++;
					  });%>
					<ul class="post-cmntlike last-ul" id="check-last-ul-<%- item.id %>">
					<% if(typeof concept_manager != 'undefined' && concept_manager != 0 && typeof concept_thumb != 'undefined') { %>
					      <li class="picture rp-ext-lft"><a href="#"><img src="<%- siteurl %>/images/concepts/thumbs/<%-concept_thumb%>" width = "25px" height = "25px" alt=""></a></li>
					<% } else { %>

                        <li class="picture rp-ext-lft"><a href="#"><img
                            src="<%- siteurl %>images/user-images/25x25/<%-myprofile.profile_pic%>?<%- rc.current_time %>"
                            width="25" height="25" alt=""></a></li>

                                            <% } %>
                                                  <li class="add-comment">
						      <form id="fComment" name="fComment" action="ssi/ajax_response.php" method="post">
								  <fieldset>
						  <div class="rp-social-comment-box textC">
							<textarea id="txtComment_<%- item.id %>" name="txtComment" class="tagged_text_comment rp-comment" style="height: 25px;" placeholder="Add a comment..."></textarea>
						  </div>
						  <fieldset>
							  </form>
					      </li>
					  </ul>
						<ul class="post-cmnterrormsg fCMsg msg d-n"></ul>
					</div>
					
				</dd>
				
			</dl>

	<% } %>	
	<% if(item.category == 'files') { %>
		<div class="block-1 block-2 clearfix <% if(item.featured==1){print("feature");}%>" id="<%- item.id %>">
            <div class="clearfix">
            	<div class="rp-avtar"><span>
				<%  if(item.view_path.indexOf('.jpg')!= -1 || item.view_path.indexOf('.png')!= -1 || item.view_path.indexOf('.jpeg')!= -1 || item.view_path.indexOf('.gif')!= -1 || item.view_path.indexOf('.bmp')!= -1){ %>
				<a class="pDelete fancybox image_view" href="<%- item.view_path %>"><img src="<%- siteurl %>media/images/iconFile.jpg" alt="File" title="File"/></a>
				<% } else if(item.view_path.indexOf('.zip')!= -1 || item.view_path.indexOf('.rar')!= -1) { %> 
					<a class="fancybox" href="<%- siteurl %>files/files_download/<%- item.file_unique_id %>"><img src="<%- siteurl %>media/images/iconFile.jpg" alt="File" title="File"/></a>
				<%} else if(item.view_path.indexOf('.3gp')!= -1 || item.view_path.indexOf('.avi')!= -1 || item.view_path.indexOf('.flv')!= -1 || item.view_path.indexOf('.m4v')!= -1 || item.view_path.indexOf('.mov')!= -1 || item.view_path.indexOf('.mp4')!= -1 || item.view_path.indexOf('.mpg')!= -1 || item.view_path.indexOf('.vob')!= -1 || item.view_path.indexOf('.wmv')!= -1 || item.view_path.indexOf('.mpeg')!= -1 || item.view_path.indexOf('.mpe')!= -1 ){ %>
					<a class="fancybox video_view" data-fancybox-type="iframe" href="<%- siteurl %>lib/video.php?url=<%- item.view_path %>"><img src="<%- siteurl %>images/files_links/tnVideo.png" width="52" alt="Video" title="Video"/></a>
				<%} else { %>
					<a class="fancybox doc_view" target="_blank" href="//docs.google.com/viewer?url=<%- item.view_path %>&embedded=true"><img src="<%- siteurl %>media/images/iconFile.jpg" alt="File" title="File"/></a>
				<% } %>
				</span></div>
                <div class="rp-contents">
                    <h2>
					<%  if(item.view_path.indexOf('.jpg')!= -1 || item.view_path.indexOf('.png')!= -1 || item.view_path.indexOf('.jpeg')!= -1 || item.view_path.indexOf('.gif')!= -1 || item.view_path.indexOf('.bmp')!= -1){ %>
						<a class="fancybox image_view" href="<%- item.view_path %>"><%- item.title %></a>
					<% } else if(item.view_path.indexOf('.3gp')!= -1 || item.view_path.indexOf('.avi')!= -1 || item.view_path.indexOf('.flv')!= -1 || item.view_path.indexOf('.m4v')!= -1 || item.view_path.indexOf('.mov')!= -1 || item.view_path.indexOf('.mp4')!= -1 || item.view_path.indexOf('.mpg')!= -1 || item.view_path.indexOf('.vob')!= -1 || item.view_path.indexOf('.wmv')!= -1 || item.view_path.indexOf('.mpeg')!= -1 || item.view_path.indexOf('.mpe')!= -1 ){ %>
						<a class="fancybox video_view" data-fancybox-type="iframe" href="<%- siteurl %>lib/video.php?url=<%- item.view_path %>"><%- item.title %></a>
					<%} else if(item.view_path.indexOf('.zip')!= -1 || item.view_path.indexOf('.rar')!= -1) { %>
						<a class="fancybox" href="<%- siteurl %>files/files_download/<%- item.file_unique_id %>"><%- item.title %></a>
					<% } else { %>
						<a class="fancybox doc_view" target="_blank" href="//docs.google.com/viewer?url=<%- item.view_path %>&embedded=true"><%- item.title %></a>
					<% } %>
					</h2>
                    <div class="rp-social clearfix">
                          <a class="in-path" href="<%- siteurl %>files"> In Files</a>
				<% if(myprofile.id == item.object_id){ %>
					 &#8226;<a class="file_delete" id="<%- item.id %>" href="javascript:void(0)">Delete</a>
				<%  } else if(myprofile.role_id == 1) { %>
					 &#8226;<a class="file_delete" id="<%- item.id %>" href="javascript:void(0)">Delete</a>
				<% } else if(typeof concept_manager != 'undefined') { 
						if(concept_manager==1) { %>
							 &#8226;<a class="file_delete" id="<%- item.id %>" href="javascript:void(0)">Delete</a>
				<% 		} 
					} %>	
				<% if(item.object_id == myprofile.id || item.concept_name != 0) { } else {%>
                     &#8226;<a class="fancybox report_file" id="<%- item.id %>" href="#pop-win-file">Report</a>
                <% } %>
				<% if(myprofile.role_id==1){ %>
					&#8226;<a href="#" class="featured_file"><%
					if(item.featured==1) {
					print('Featured');
					} else {
					print('Mark as featured');
					}
					%></a>
				<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
				<span><%- item.time_ago %></span>
                    </div>
              </div>
        	</div>
        </div>
	<% } %>
		<% if(item.category == 'polls') { %>
			<dl class="block-1 block-2 clearfix <% if(item.featured==1){print("feature");}%>" id="<%- item.id %>">
            	<dt>
            <a href="<%- siteurl %><%- item.username %>"><img
                src="<%- siteurl %><%- item.profile_pic %>?<%- rc.current_time %>" class="shadow-3 cs-clip" width="55"
                height="55" alt="<%- item.display_name %> this"/></a>
        <h4 class="media-name">
				<a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
				<% if(item.target_name) { %>
					<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
				<% } %>
			</h4>
		</dt>
		<dd>
                <div class="rp-contents">
				<h4 class="profile-name">
							<a href="<%- siteurl %><%- item.username %>"><%- item.display_name %></a>
							<% if(item.target_name) { %>
								<img src="<%- siteurl %>media/images/icons/posted-arrow.png" alt="posted to"/> <a href="<%- siteurl %><%- item.target_url %>"><%- item.target_name %></a>
							<% } %>
						</h4>
					<div class="poll-box">
					
				
						<ul>
						<%	if(item.voted != 1) { %>
							<li class="question"><b><%- item.title %></b></li>
							<form method="post" class="vPoll" id="vPoll_<%- item.id %>">      
							<input type="hidden" id="poll_id" value="<%- item.id %>" />
							<li> <% _.each( item.options, function( poll ){ %>
								<label for="options"><input type="radio" value="<%- poll.option_id %>" name="options"> <%- poll.title %></label>
								<% });%>  
							</li>
							<li> <label><input type="button" class="btn-sm submit" name="submit" value="Vote" id="vpoll_submit" /></label></li>
							</form>
							
							<% } else {  %>
						
	                    	<li class="question"><b><%- item.title %></b></li>
								<% 
								temp = 0;
								_.each( item.options, function( poll1 ){ %>
							 <li>
	                        	<label><%- poll1.title %></label>
	                            <div id="progressBar<%- temp %>" class="progressBar">
	                            	<div style="width: <%- poll1.percentage * 202 / 100 %>px;"> <%- Math.round(Number(poll1.percentage)) %>%</div>
	                            </div>
	                         </li>
			            	
							<% temp++; });%>
						
						<% } %>
						
						</ul>
						<ul id="vPollResponse_<%- item.id %>" class="PollResponse"></ul>
						 <div class="rp-social"><% if(item.object_id == myprofile.id && item.object_type == 'User') { %>
							<a href="#" class="pollDelete">Delete</a>
						<% } else if ( typeof concept_manager != 'undefined' && concept_manager == 1 && item.object_type == 'Concept' ) { %>
						<a href="#" class="pollDelete">Delete</a>		
						<% } %>
					<% if(myprofile.role_id==1){ %>
					 <span><a href="#" class="featured_polls"><%
						if(item.featured==1) {
							print('Featured');
						} else {
							print('Mark as featured');
						}
						%></a></span> 
			<% } else { %> 
			&#8226; <%	if(item.featured==1) {
							print('Featured');
						} 
						%>
			 <% } %>
						<span><%- item.time_ago %></span></div>
					</div>
             
				  
           	  	</div></dd>
</dl> 
	<% } %>
		<% if(item.category == 'photos') { %>
		<div class="profilePhoto clearfix">
		<div class="cp-photo-gallery">
			<a class="fancybox-thumb" rel="fancybox-thumb" href="<%- item.thumbnail %>"><img src="<%- item.thumbnail %>" alt="<%- item.title %>"></a>
		</div>
		</div>
	<% } %>
	<% if(item.category == 'profilePhotos') { %>
	<div class="cp-photo-gallery-box">
		<div class="cp-photo-gallery">
			<a class="fancybox-thumb" rel="fancybox-thumb" href="<%- item.image %>"><img src="<%- item.thumbnail %>" alt="<%- item.title %>"></a>
		</div>
	</div>
	
	<% } %>
<% }); %>
<%}%>
<%$('.report_view').fancybox({
  beforeLoad : function() {
     
     $('#txtmessageid').val(this.element[0].id);
     $('#txtarcomments').val('');
     $(".error-msg").html('');
    }
});%>   
<%$('.report_file').fancybox({
  beforeLoad : function() {
    
     $('#txtfileid').val(this.element[0].id);
     $('#txtarcomments-file').val('');
     $(".error-msg").html('');
    }
});%>
<%
  $('.image_view').fancybox({
		maxHeight: 500
 });
  $('.doc_view').fancybox({
		'width'	: '75%',
		'height' : '95%',
    'autoScale' : false,
    'transitionIn' : 'none',
		'transitionOut'	: 'none',
		'type' : 'iframe'
	});
	$('.video_view').fancybox({
		width	: '435',
	});	
	
	
%>
</script>