<script type="text/template" id="photo_album_card">
<%
_.each( rc, function( item ){
	%>
              <div class="album">
                <a href="<%-  item.link %>"><div class="album-container">
				<img src="<%- item.thumbnail %>" alt="album"/></div>
                </a>
                <div class="album-title"><%- item.title.substr(0, 15) %>...</div>
                <div class="album-attr"><span class="album-date"><%- item.ctime %></span>  <%= item.total_pic %>  </div>
              </div>
         
	<% }); %>
</script>