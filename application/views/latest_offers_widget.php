<?php
$this->load->model('Offer');
$latest_news = $this->Offer->getOffers(3, 0);

?>
<div class="widget announcements clearfix news">
          	<div class="widget-head latest-offer">
                <h3>Latest Offers</h3>
            </div>
            <div class="box shadow-2">
                <ul>
				<?php foreach($latest_news as $news_item): ?>
                    <li><a class="announc clearfix" href="<?php echo site_url(); ?>offer/<?php echo $news_item['entry_id']; ?>"><?php echo $news_item['entry_title'];?></a></li>
				<?php endforeach;?>
              </ul>
          </div>
        </div>