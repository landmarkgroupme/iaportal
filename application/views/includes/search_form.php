<?php
/* Search form below logout, search with respect to
 * People
 * All Items
 * Items for Sale
 * Items Wanted
 * Freebies
 * Your Items
 */
?>
<form action="<?php echo site_url('people/search')?>" class="search-form" name="searchform" id="quick_search_frm" method="post" onsubmit="javascript:searchPage();">
  <fieldset>
    <input accesskey="4" name="term" autocomplete="off" tabindex="23" type="text" class="text" id="txt_general_search" value="Search by name.."/>
    <input name="search" type="hidden" value="search"/>
    <input name="search_from" type="hidden" value="<?php echo isset($search_from) ? $search_from : ''; ?>" />
  </fieldset>
  <div id ="search_element">
    <div id='search_listing'>
      <div id="src-loader" ><img src="<?php echo base_url()?>images/loader.gif" /></div>
      <ul id='src_user_list'>
        
      </ul>
      <p class='src_more_res'><a id="submit_src" href="#"></a></p>
    </div>
    <div id='search_listing_f'></div>
  </div>
  <img src="<?php echo base_url()?>images/gnr_src_bg-f.jpg" style="display: none;" alt="bg-preload" />
  <img src="<?php echo base_url()?>images/user_list_bg.png" style="display: none;" alt="bg-preload" />
  <img src="<?php echo base_url()?>images/user_list_f_bg.png" style="display: none;" alt="bg-preload" />
</form>