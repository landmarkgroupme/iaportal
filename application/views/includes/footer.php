<?php
/* Footer Div */
?>
<div id="footer">

  <ul>
    <li><a href="<?= site_url("home/code_of_conduct") ?>">Code of Conduct</a></li>
    <li><a href="<?= site_url("home/feedback") ?>">Feedback</a></li>
    <li><a href="<?= site_url("home/release_notes") ?>">Release Notes</a></li>
  </ul>

  <p>&copy; <?= date('Y') ?> Landmark Group. <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>"/></p>
</div>


<div class="useful-link-box" id="show-sso-pass-box">
  <div class="head-text"> <a href="#" class="close-overlay"><img src="<?php echo base_url(); ?>images/sso_close.png" /></a><img src="<?php echo base_url(); ?>images/sso_login.png" /> Login to HR Konnekt</div>
  <div id="sso-pass" class='sso-dialog'>
      <p>You can now access all the functions of HR Konnekt via the intranet. You will only have to login once by entering your password below:</p>
      <input type="password" class="useful-link-input" id="sso-pass-in" name="sso-pass-in" value="" />
      <input type="submit" value="Login" class="useful-link-bt" id="sso-login-submit-bt" name="fgt-submit-bt"> &nbsp;   <a href="#" class="forgot-sso-pass">Forgot Password</a>
  </div>
  <div id="sso-webmail" class='sso-dialog'>
      <p>You can now access your Landmark Email straight from the Intranet. You will only have to login once by entering your password below:</p>
      <input type="password" class="useful-link-input" id="sso-pass-in-webmail" name="sso-pass-in" value="" />
      <input type="submit" value="Login" class="useful-link-bt" id="sso-webmail-submit-bt" name="fgt-submit-bt"> &nbsp;
  </div>
  <div id="sso-pass-forgot-pass-confirm" class='sso-dialog'>
      <p>We will be sending you an email with a link to retrieve your password.</p>
      <input type="submit" value="Confirm" class="useful-link-bt" id="confirm-fgt-submit-bt" name="fgt-submit-bt"> &nbsp;<a href="#" class="close-overlay">or cancel</a>
  </div>
  <div id="sso-pass-forgot-pass" class='sso-dialog'>
      <p>Please check your email for instructions on retrieving your password.</p>
  </div>
  <div id="sso-loader" class='sso-dialog'>
      <p>Please wait while we connect you to HR Konnekt:</p>
      <img src="<?php echo base_url(); ?>images/indicator.gif" />
  </div>
  
 <div id="sso-wrong" style="display:none;">
      <p style="color:red; padding:8px 13px 20px; font-size:20px; font-weight:bold; ">oops,</p>
      <p>Your password/ username combo is incorrect. Please try again or use the <a href="#" class="forgot-sso-pass">Forgot Password</a> link below to get a reminder.</p>
      <input type="password" class="useful-link-input" id="sso-pass-in" name="sso-pass-in" onFocus="if(this.value=='pass here') this.value='';" onBlur="if(this.value=='') this.value='pass here';" value="pass here" />
      <input type="submit" value="Login" class="useful-link-bt grad" id="sso-login-submit-bt" name="fgt-submit-bt"> &nbsp;   <a href="#" class="forgot-sso-pass">Forgot Password</a>
</div>

</div>
 <!-- Skin change    div style="background: url('<?= base_url() ?>images/special.png') no-repeat;height: 145px;margin-left: 591px;position: absolute;top: -25px;width: 366px; z-index: 0;"></div-->
<div class='overlay'></div>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16731150-2']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
