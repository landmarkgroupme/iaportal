<?php 
/* My Interest Groups in sidebar */
?>
<div class="box open">
	<div class="heading">
		<h2>My Interest Groups</h2>
		<a href="#" class="open-close">Open Close</a>
		<!--a href="#" class="edit">Edit</a-->
	</div>
	<div class="box-content">
		<!-- my groups list -->
		<ul class="my-groups">
			<?php if(count($gn_interest_groups_list)):?>
				<?php foreach($gn_interest_groups_list as $details):?>
				<li><a href="<?=base_url();?>index.php/interest_groups/group_page/<?=$details['id']?>" class="group-name"><?=$details['name']?></a><a href="<?=base_url();?>index.php/interest_groups/group_page/<?=$details['id']?>" class="number"><?=$details['total_members']?> people</a></li>
				<?php endforeach;?>
				<?php else:?>
				<li>No Groups</li>
				<?php endif;?>
		</ul>
                <?php if(count($gn_interest_groups_list) > 3 ):?>
                <div class="ig-group-see-more"><a class="show-all" href="#">See more</a></div>
                <?php else:?>
                <br />
                <?php endif;?>

	</div>
</div>