<?php 
/* User Box sidebar top right */
?>
<div class="user-box user-box-type2">
	<div class="photo-box">
		<span><img class="profile_pic" src="<?=base_url()?>images/user-images/105x101/<?=$myprofile['profile_pic']?>" alt="image description" width="89" height="86" /></span>
		<a rel="pop_win" href="<?=site_url("profile/change_photo")?>">Change your photo</a>
	</div>
	<div class="info">
		<span>Welcome,</span>
		<a href="<?php echo site_url('profile/view_profile/' . $myprofile['id']); ?>" class="name"><?php echo ucfirst(strtolower($myprofile['first_name'])). ' ' . ucfirst(strtolower($myprofile['last_name'])) ; ?></a>
		<ul>
			<li><a href="<?php echo site_url('profile/myprofile/'); ?>">View your Profile</a></li>
			<li><a href="<?=site_url("profile/edit_profile#skills")?>">Add your specialities</a></li>
			<li><a rel="pop_win" href="<?=site_url("profile/fav_links")?>">Add your favorite links</a></li>
		</ul>
		<!-- progress bar -->
		<span class="progress"><span></span></span>
		<em>Profile is <strong>0%</strong> complete</em>
	</div>
</div>