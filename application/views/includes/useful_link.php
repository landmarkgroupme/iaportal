<div class="sidebar-w">
    <div class="s-heading-box">
        <div class="s-heading"><a href="#">Useful Links <img src="<?php echo base_url() ?>images/useful-link.png" alt="useful link" /></a></div>
        <span class="s-heading-img" id="useful-link"><img src="<?php echo base_url() ?>images/bullet9.jpg" alt="bullet" /></span>
    </div>
    <div class="s-content" id="useful-link-content">
        <p><strong>Note:</strong> You’ll need to enter your Web Mail or HR Konnekt details just once on clicking the links below.</p>
        <ul class="useful_link">
            <li><a class="webmail show_overlay" href="#">Web Mail</a></li>
            <li><a class="leave show_overlay" href="#">Leave Application</a></li>
            <li><a class="assistance show_overlay" href="#">Request Assistance</a></li>
            <li><a class="qualification show_overlay" href="#">Edit Qualifications</a></li>
            <li><a class="personal-info show_overlay" href="#">Personal Information</a></li>
            <li><a  class="performance show_overlay" href="#">Performance Appraisal</a></li>
        </ul>

    </div>
</div>
