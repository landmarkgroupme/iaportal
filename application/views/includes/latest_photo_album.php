<?php
  $this->load->model('about/about_model');
  $album_category = "c.cid <> '' ";
  $album_list = $this->about_model->alb_get_all_album($album_category, 2, 0);
  //echo "<xmp>" . print_r($album_list, 1) . "</xmp>";
  $latest_albums = null ;
  for ($i = 0; $i < count($album_list); $i++) {
    $thumbnail = $album_list[$i]['image_path'] . $album_list[$i]['sub_dir'] . $album_list[$i]['filepath'] . $album_list[$i]['thumb'] . $album_list[$i]['filename'];
    $thumbnail = str_replace(" ", "%20", $thumbnail);
    if (!@fopen($thumbnail, "r")) {
      $thumbnail = base_url() . "images/album.jpg";
    }
    $latest_albums .= '<li>
      <div class="album">
        <a class="pic" href="'.site_url('about/view_photos/'.$album_list[$i]['aid']).'"><img width="126" alt="album" src="'.$thumbnail.'" alt="'.$album_list[$i]['title'].'" />
        </a>
        <p class="album_name"><a href="'.site_url('about/view_photos/'.$album_list[$i]['aid']).'">'.$album_list[$i]['title'].'</a></p>
        <p class="photo_count">' . $album_list[$i]['total_pic'] . ' Photos</p>
        <p class="photo_date">'. date("d F", $album_list[$i]['ctime']) .'</p>
      </div>
    </li>';
  }

?>
<div class="sidebar-w">
          <div class="s-heading-box">
            <div class="s-heading"><a class="sub-head-link" href="<?php echo site_url('about/photos');?>">View all photos</a><a href="#">Recent Photos</a></div>
            <span class="s-heading-img" id="useful-link"><img src="<?php echo base_url()?>images/bullet9.jpg" alt="bullet" /></span>
          </div>
          <div class="s-content">
            <ul class="recent_photos">
              <?php echo $latest_albums; ?>
            </ul>
          </div>
        </div>