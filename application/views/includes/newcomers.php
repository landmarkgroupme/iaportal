<?php 
/* Newcomers in sidebar */
?>
<div class="box open">
	<div class="heading">
		<h2>Newcomers</h2>
		<a href="#" class="open-close">Open Close</a>
		<!-- a href="#" class="edit">Edit</a -->
	</div>
	<div class="box-content">
		<ul class="newcomers">
			<?php foreach($newcommer_list as $list): ?>
			<li>
				<div class="visual">
					<img src="<?=base_url();?>images/user-images/105x101/<?=$list['profile_pic']?>" alt="image description" width="51" height="51" />
				</div>
				<div class="text vcard">
					<a href="<?=site_url("profile/view_profile/".$list['id'])?>" class="name fn url"><?=ucfirst(strtolower($list['first_name']))." ".ucfirst(strtolower($list['last_name']))?></a>
					<span><?=$list['designation']?>, <?=$list['concept']?></span>
					<ul>
						<li><a href="mailto:<?=$list['email']?>">Email</a></li>
						<li><a class="remove-contact">Add or Remove</a></li>
					</ul>
				</div>
			</li>
			<?php endforeach;?>
		</ul>
	</div>
</div>