<ul id="nav">
	<li <?=$home_active ?>><a href="<?=site_url("home")?>" class="home" tabindex="2" accesskey="1"><span>Home</span><em></em></a></li>
	<li <?=$people_active ?>><a href="<?=base_url()?>profile/" tabindex="3"><span>People</span><em></em></a>
		<div class="drop">
			<span class="t"></span>
			<ul>
				<li><a href="<?=base_url()?>people/" tabindex="9">Company Directory</a></li>
				<li><a href="<?=base_url()?>status_update/" tabindex="10">Status updates</a></li>
				<li><a href="<?=base_url()?>interest_groups/" tabindex="13">Interest Groups</a></li>
			</ul>
			<span class="b"></span>
		</div>
	</li>
	<li <?=$files_active ?>><a href="<?=base_url()?>files/" tabindex="5"><span>Files &amp; Links</span><em></em></a>
    <div class="drop">
      <span class="t"></span>
      <ul>
        <li><a href="<?=base_url()?>files/show_files/" tabindex="9">All Files and Links</a></li>
        <li><a href="<?=base_url()?>files/files_dropbox/" tabindex="10">Dropbox</a></li>
        <li><a href="<?=base_url()?>files/your_files/" tabindex="11">Your Files</a></li>
        <li><hr class="nav-sep" /></li>
        <li><a href="<?=site_url("files/share_files/")?>" tabindex="9">Share a File/Link</a></li>
        <li><a href="<?=site_url("files/files_dropbox/")?>" tabindex="9">Share a File Privately</a></li>
      </ul>
      <span class="b"></span>
    </div>
  </li>
  <li <?=$news_update_active ?>><a href="<?=base_url()?>news/" tabindex="8" accesskey="2"><span>News &amp; Updates</span><em></em></a>
    <div class="drop">
      <span class="t"></span>
      <ul>
        <li><a href="<?=base_url()?>news/" tabindex="9">News</a></li>
        <li><a href="<?=base_url()?>announcement/" tabindex="10">Announcements</a></li>
        <li><a href="<?=base_url()?>offers/" tabindex="13">Offers</a></li>
      </ul>
      <span class="b"></span>
    </div>
  </li>
	<li <?=$events_active ?>><a href="<?=base_url()?>events/" class="home" tabindex="6"><span>Events</span><em></em></a></li>
	<li <?=$photos_active ?>><a href="<?=base_url()?>about/photos/" class="home" tabindex="6"><span>Photos</span><em></em></a></li>
	<li <?=$market_place_active ?>><a href="<?=base_url()?>market_place/" tabindex="4"><span>Marketplace</span><em></em></a>
		<div class="drop">
			<span class="t"></span>
			<ul>
				<li><a href="<?=base_url()?>market_place/all_items/" tabindex="9">All Items</a></li>
				<li><a href="<?=base_url()?>market_place/items_sale/" tabindex="10">Items for Sale</a></li>
				<li><a href="<?=base_url()?>market_place/items_wanted/" tabindex="11">Items Wanted</a></li>
				<li><a href="<?=base_url()?>market_place/items_freebies/" tabindex="12">Freebies</a></li>
				<li><a href="<?=base_url()?>market_place/your_items/" tabindex="13">Your Items</a></li>
				<li><hr class="nav-sep" /></li>
				<li><a href="<?=base_url()?>market_place/add_item/" tabindex="13">Add an Item</a></li>
				<li><a href="<?=base_url()?>market_place/request_item/" tabindex="13">Request an Item</a></li>
			</ul>
			<span class="b"></span>
		</div>
	</li>
	<!--li <?=$about_lmg_active ?>><a href="<?=base_url()?>about/" tabindex="7"><span>About Landmark</span><em></em></a>
		<div class="drop">
			<span class="t"></span>
			<ul>
				<li><a href="<?=base_url()?>about/photos/" tabindex="9">Photos</a></li>
				<li><a href="<?=base_url()?>about/policies/" tabindex="10">Policies</a></li>
			</ul>
			<span class="b"></span>
		</div>
	</li-->
</ul>