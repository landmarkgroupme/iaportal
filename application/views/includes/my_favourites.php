<?php 
/* my favourites in sidebar */
?>
<div class="box open">
	<div class="heading">
		<h2>My Favourites</h2>
		<a href="#" class="open-close">Open Close</a>
		<!--a href="#" class="edit">Edit</a-->
	</div>
	<div class="box-content">
		<ul class="favourites">
			<?php if(count($gn_favourite_links_list)): foreach($gn_favourite_links_list as $link_details):?>
			<li>
			  <a target="_blank" href="<?=$link_details['link']?>"><?=$link_details['link_name']?></a> 
			  <a class="remove-link" id="<?=$link_details['id']?>" href="#"><img src="<?=base_url()?>images/ico-22.jpg" alt="Remove link" /></a>
			</li>
			<?php endforeach; else:?>
			  <li>No Saved Links</li>
      <?php endif;?>
		</ul>
		<div class="container add-link"><a rel="pop_win" href="<?=site_url("profile/fav_links")?>">Add a Link</a></div>
	</div>
</div>