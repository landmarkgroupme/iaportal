<?php 
/* My Reminders in sidebar */
?>
<div class="box open">
	<div class="heading">
		<h2>My Reminders</h2>
		<a href="#" class="open-close">Open Close</a>
		<!--a href="#" class="edit">Edit</a-->
	</div>
	<div class="box-content">
		<ul>
			<li><a href="<?=site_url("events/cal_view")?>" class="events">Upcoming Events (<?=($gn_my_reminders['count_event_reminders'])?$gn_my_reminders['count_event_reminders']:0?>)</a></li>
			<li><a href="<?=site_url("market_place/your_items")?>" class="market">Marketplace Items Listed (<?=$gn_my_reminders['count_item_sale']?>)</a></li>
			<li><a href="<?=site_url("market_place/your_items#requested")?>" class="market">Marketplace Items Wanted (<?=$gn_my_reminders['count_item_requested']?>)</a></li>
		</ul>
	</div>
</div>