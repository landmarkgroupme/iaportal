<?php
//permission validation for urls
$permissions = $this->session->userdata('permissions');

/* Review notification count */
    $news_review_count = $this->manage_model->get_review_count_mt_entries('news');
    $news_review_count = $news_review_count['total_entries'];
    
    $announcement_review_count = $this->manage_model->get_review_count_mt_entries('announcement');
    $announcement_review_count = $announcement_review_count['total_entries'];
    
    $offers_review_count = $this->manage_model->get_review_count_mt_entries('offers');
    $offers_review_count = $offers_review_count['total_entries'];
?>
<div class="top-header">
    <div class="left-header">
      <a class="logo" href="<?=site_url();?>"></a>
      <!--div class="separator"></div>
      <div class="pagetitle">Manage</div-->
    </div>
    <div class="right-header">
      <div class="logininfo">Logged in as: <strong>
        <?=$user_name?>
        </strong></div>
      <a href="<?=base_url();?>login/logout">
      <div class="logout"></div>
      </a> </div>
  </div>

  <div class="menu">
	   <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>" />
    
    <ul class="main">
		<?php $user_id =$this->session->userdata('userid'); if($permissions['moderate']):?>
      <li><a href="<?=base_url();?>backend/moderate/show_all/">Moderate</a></li>
		<?php endif;?>
      <li class="active"><a href="<?=base_url();?>manage">Manage</a></li>
	   <li><a href="<?=base_url();?>manage/admin">Admin</a></li>
      <li class="email-queue"><a href="<?=site_url("manage/email_queue");?>">Notification E-Mail Queue</a></li>
    </ul>
    <ul class="submenu">
      <?php if($permissions['news']):?>
      <li <?php if($active_tab == 'news'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/news">News <?php echo ($news_review_count)? '<span class="review-count">'.$news_review_count.'</span>':''; ?></a></li>
      <?php endif;?>
      
      <?php if($permissions['announcements']):?>
      <li <?php if($active_tab == 'announcement'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/announcement">Announcements <?php echo ($announcement_review_count)? '<span class="review-count">'.$announcement_review_count.'</span>':''; ?></a></li>
      <?php endif;?>
     
      <?php if($permissions['offers']):?>
      <li <?php if($active_tab == 'offers'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/offers">Offers <?php echo ($offers_review_count)? '<span class="review-count">'.$offers_review_count.'</span>':''; ?></a></li>
      <?php endif;?>
     
      <?php if($permissions['photos']):?>
      <li <?php if($active_tab == 'photos'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/photos">Photos</a></li>
      <?php endif;?>
      
      <?php if($permissions['users']):?>
      <li <?php if($active_tab == 'permissions'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/permissions">People &amp; Permissions</a></li>
      <?php endif;?>
      <?php if(isset($permissions['employees'])):?>
      <li <?php if($active_tab == 'employees'){ echo 'class="active"';}?>><a href="<?=base_url();?>mprofile/edit_profile">Employees Info</a></li>
      <?php endif;?>
	  <li <?php if($active_tab == 'group'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/group">Groups</a></li>
	  <li <?php if($active_tab == 'concept'){ echo 'class="active"';}?>><a href="<?=base_url();?>manage/concept">Concepts</a></li>
	  <li style="background:#1E5886; margin-top: -3px;">
	   <select id="choose_create" name="choose_create" class="form-text-dropdown">
          <option value="create_post">Activity</option>
			<option value="post_concept">Concept</option>
			<option value="post_group">Group</option>
          </select>
	  </li>
      <li ><a id="createNew" class="createNewButton" href="<?php echo site_url('manage/create_post');?>">Create</a></li>
	  
    </ul>
  </div>