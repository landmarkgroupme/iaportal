<?php
          $this->load->model('home/home_model');
          $mt_news_data = $this->home_model->get_data_from_mt('news');
          //Customizing the Database field
          for ($i = 0; $i < count($mt_news_data); $i++) {
            $entry_time = $mt_news_data[$i]['entry_created_on'];
            $entry_id = $mt_news_data[$i]['entry_id'];
            $entry_title = $mt_news_data[$i]['entry_title'];
          
            //Custom Field
            $entry_time = strtotime($entry_time);
          
            $news_content['entry_id'] = $entry_id;
            $news_content['entry_time'] = time_ago($entry_time);
            $news_content['entry_title'] = $entry_title;
            $news_data[$entry_time] = $news_content; //main News Data Store
          
            $general_time_check[] = $entry_time; //  Time Store for general sorting
          }
          $mt_announcement_data = $this->home_model->get_data_from_mt('announcement');
          //Customizing the Database field
          for ($i = 0; $i < count($mt_announcement_data); $i++) {
            $entry_time = $mt_announcement_data[$i]['entry_created_on'];
            $entry_id = $mt_announcement_data[$i]['entry_id'];
            $entry_title = $mt_announcement_data[$i]['entry_title'];
          
            //Custom Field
            $entry_time = strtotime($entry_time);
          
            $announcement_content['entry_id'] = $entry_id;
            $announcement_content['entry_time'] = time_ago($entry_time);
            $announcement_content['entry_title'] = $entry_title;
            $announcement_data[$entry_time] = $announcement_content; //main News Data Store
            $general_time_check[] = $entry_time; //  Time Store for general sorting
          }
          $mt_offer_data = $this->home_model->get_data_from_mt('offers');
          
          //Customizing the Database field
          for ($i = 0; $i < count($mt_offer_data); $i++) {
            $entry_time = $mt_offer_data[$i]['entry_created_on'];
            $entry_id = $mt_offer_data[$i]['entry_id'];
            $entry_title = $mt_offer_data[$i]['entry_title'];
          
            //Custom Field
            $entry_time = strtotime($entry_time);
          
            $offer_content['entry_id'] = $entry_id;
            $offer_content['entry_time'] = time_ago($entry_time);
            $offer_content['entry_title'] = $entry_title;
            $offer_data[$entry_time] = $offer_content; //main News Data Store
            $general_time_check[] = $entry_time; //  Time Store for general sorting
          }
          
          $ci_events_data = $this->home_model->events_get_all();
          //Customizing the Database field
          for ($i = 0; $i < count($ci_events_data); $i++) {
            $added_on = $ci_events_data[$i]['added_on'];
            $ci_events_data[$i]['added_on'] = time_ago($added_on);

            $events_data[$added_on] = $ci_events_data[$i]; //main Files Data Store
            $general_time_check[] = $added_on; // Time Store for general sorting
          }
          rsort($general_time_check, SORT_NUMERIC); //Reverse Sort
          //echo '<xmp>' .print_r($general_time_check,1) . '</xmp>';exit; 
          $file_output = '';
          for ($i = 0; $i < 5; $i++) {
            $timestamp = $general_time_check[$i];
            /* Event */
              if (@array_key_exists($timestamp, $events_data)) {
                $tmp_events = '<li><a class="event-update" href="' . site_url("events/event_detail/" . $events_data[$timestamp]['id']) . '">' . $events_data[$timestamp]['title'] . '</a><p class="update-info">In <a href="' . site_url("events/") . '">Events</a> ' . $events_data[$timestamp]['added_on'] . '</p></li>';
                $file_output .= $tmp_events;
              }
            /* News */
              if (@array_key_exists($timestamp, $news_data)) {
                $tmp_news = '<li><a class="news-update" href="' . site_url("news/news_details/" . $news_data[$timestamp]['entry_id']) . '">' . $news_data[$timestamp]['entry_title'] . '</a><p class="update-info">In <a href="' . site_url("news/") . '">News</a> ' . $news_data[$timestamp]['entry_time'] . '</p></li>';
                $file_output .= $tmp_news;
              }
            /* Announcement */
              if (@array_key_exists($timestamp, $announcement_data)) {
                $tmp_announcement = '<li><a class="announcement-update" href="' . site_url("announcement/announcement_details/" . $announcement_data[$timestamp]['entry_id']) . '">' . $announcement_data[$timestamp]['entry_title'] . '</a><p class="update-info">In <a href="' . site_url("announcement/") . '">Announcement</a> ' . $announcement_data[$timestamp]['entry_time'] . '</p></li>';
                $file_output .= $tmp_announcement;
              }
            /* Offers */
              if (@array_key_exists($timestamp, $offer_data)) {
                $tmp_offers = '<li><a class="offers-update" href="' . site_url("offers/offers_details/" . $offer_data[$timestamp]['entry_id']) . '">' . $offer_data[$timestamp]['entry_title'] . '</a><p class="update-info">In <a href="' . site_url("offers/") . '">Offers</a> ' . $offer_data[$timestamp]['entry_time'] . '</p></li>';
                $file_output .= $tmp_offers;
              }
          }
        ?>
        <div class="sidebar-w">
          <div class="s-heading-box">
            <div class="s-heading"><a class="sub-head-link" href="<?php echo site_url('news/'); ?>">View all updates</a><a href="#">Latest Updates</a></div>
            <span class="s-heading-img" id="useful-link"><img src="<?php echo base_url()?>images/bullet9.jpg" alt="bullet" /></span>
          </div>
          <div class="s-content">
            <ul class="updates_list">
              <?php echo $file_output; ?>
            </ul>
          </div>
        </div>
