<?php 
/* Footer Div*/
?>
		<div id="footer">
			
			<ul>
				<li><a href="<?=site_url("home/code_of_conduct")?>">Code of Conduct</a></li>
				<li><a href="<?=site_url("home/feedback")?>">Feedback</a></li>
				<li><a href="<?=site_url("home/release_notes")?>">Release Notes</a></li>
			</ul>
			
			<p>&copy; 2012 Landmark Group.<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>"/></p>
		</div>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16731150-2']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(66435807); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66435807ns.gif" /></p></noscript>
<!--div style="background: url('<?=base_url()?>images/special.png') no-repeat;height: 145px;margin-left: 591px;position: absolute;top: -25px;width: 366px; z-index: 0;"></div-->