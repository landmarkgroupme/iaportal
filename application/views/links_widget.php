<div class="widget clearfix">
          	<div class="widget-head useful-links">
                <h3>Useful links</h3>
            </div>
            <div class="box shadow-2">
                <!-- <div class="note"><strong>Note:</strong> You'll need to enter your web mail or HR Konnekt details just once on clicking the link below.</div> -->
                <ul>
                    <li><a class="webmail show_overlay" href="https://mail.cplmg.com/redirect.nsf" target="_blank">Web Mail</a></li>
                    <li><a class="leave show_overlay" href="https://www.hrkonnekt.com/OA_HTML/RF.jsp?function_id=12250" target="_blank">Leave Application</a></li>
                    <li><a class="assistance show_overlay" href="https://www.hrkonnekt.com/OA_HTML/RF.jsp?function_id=27489" target="_blank">Request Assistance</a></li>
                    <li><a class="qualification show_overlay" href="https://www.hrkonnekt.com/OA_HTML/RF.jsp?function_id=12217" target="_blank">Edit Qualifications</a></li>
                    <li><a class="personal-info show_overlay" href=" https://www.hrkonnekt.com/OA_HTML/RF.jsp?function_id=11232" target="_blank">Personal Information</a></li>
                    <li><a  class="performance show_overlay" href="https://www.hrkonnekt.com/OA_HTML/RF.jsp?function_id=20553" target="_blank">Performance Appraisal</a></li>
                </ul>
          </div>
        </div>