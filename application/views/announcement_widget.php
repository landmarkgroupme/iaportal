<?php
if(isset($announcements) && !empty($announcements)):

?>
<div class="widget announcements clearfix news">
          	<div class="widget-head announcements">
                <h3>Announcements</h3>
            </div>
            <div class="box shadow-2">
                <ul>
				<?php foreach($announcements as $announcement): ?>
                    <li><a class="announc clearfix" href="<?php echo site_url(); ?>announcement/<?php echo strtolower($announcement['entry_id']); ?>"><?php echo $announcement['entry_title'];?></a></li>
				<?php endforeach;?>
              </ul>
          </div>
        </div>
<?
endif;
?>