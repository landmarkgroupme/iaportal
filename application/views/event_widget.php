<?php
if(isset($events) && !empty($events)):

?>
<div class="widget upcoming-events clearfix">
          	<div class="widget-head upcoming-event">
                <h3>Upcoming Events</h3>
            </div>
            <div class="box shadow-2">
                <ul>
				<?php foreach($events as $event): ?>
                    <li><a class="event clearfix" href="/events/view/<?php echo strtolower($event['id']); ?>"><img src="media/images/imgSplash_20130919_22.png" width="60" height="40" alt="<?php echo $event['title'];?>" /><?php echo $event['title'];?></a></li>
					<?php endforeach;?>

              </ul>
          </div>
        </div>
<?
endif;
?>