<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title><?php echo $title_for_layout; ?></title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">

	<?php $this->load->view('global_header'); ?>

<div class="section wrapper clearfix">
	<h2><?php echo $title_for_layout; ?></h2>
</div>

<div class="section wrapper clearfix events-page">
	<div class="left-contents">
		<!-- your content goes here -->
		<?php echo $content_for_layout ?>
	</div>
			

</div> <!-- section -->

<?php $this->load->view('global_footer'); ?>

<?php $this->load->view('partials/js_footer'); ?>
</body>
</html>