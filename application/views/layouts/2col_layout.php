<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title>{title}</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>



</head>
<body class="full-width">
	
	<?php $this->load->view('global_header'); ?>

<div class="section wrapper clearfix">
	<h2>{title}</h2>
</div>

<div class="section wrapper clearfix">
	<div class="left-contents">
		<!-- your content goes here -->
		{content}
	</div>
	  <div class="right-contents media-widget">
    {widgets}
  </div>
</div> <!-- section -->

<?php $this->load->view('global_footer'); ?>

<?php $this->load->view('partials/js_footer'); ?>
</body>
</html>