<?php
$this->load->model('ObjectMember');
$concepts = $this->ObjectMember->getFollowingConcepts($profile_id);
if(isset($concepts) && !empty($concepts)):
?>
    	<div class="widget clearfix">
          	<div class="widget-head concepts">
                <span class="view-all">&#8226; <a href="#view-all-the-concepts" id="concepts-all-view" class="fancybox">View All</a></span>
                <h3>Concepts</h3>
            </div>
            <div class="box shadow-2">
				<ul>
				<?php
				$count= 0;
				foreach($concepts as $concept): ?>
					<li class="clearfix">
						<a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>" class="gr-tn"><img class="round-c" alt="Fun City" src="<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>"><div class="gr-title"><?php echo ucwords(strtolower($concept['name'])); ?></div></a>
					</li>
					<?php
				$count++;
				if($count == 4)
				{
					break;
				}
				endforeach; ?>
              </ul>
          </div>
        </div>

	<div id="view-all-the-concepts" class="bd-all-box fb-container-box d-n">

	<?php foreach($concepts as $concept): ?>
		<div class="pop-bd clearfix">
			<a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>" class="normal-text"><img src="<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>" width="25" height="25" alt="Jesse Lander" class="round-c"/><?php echo ucwords(strtolower($concept['name'])); ?></a>
		</div>
		<?php if ($concept !== end($concepts)): ?>
        	<hr class="dotted"/>
		<?php endif; ?>
	<?php endforeach; ?>
	</div>
<?php endif; ?>