<?php
$limit =1; 
$offset=0;
$my_user = $this->session->userdata("logged_user");
if(isset($created_by))
{
    $latest_poll = $this->poll_lib->getPollCreatedBy($created_by, $created_by_type); 
} else{
    $latest_poll = $this->poll_lib->getPollCreatedByType('Concept',$limit, $offset); 
}

if(!empty($latest_poll)):
?>



<?php
$poll_count = $this->poll_lib->num_polls_created_by_type('Concept');
if($poll_count > 10)
{
$poll_count =10;
}
$already_voted = $this->poll_lib->has_previously_voted($latest_poll['poll_id'], $my_user['userid']);

if($already_voted):
?>
<script type="text/javascript">
var poll_results = <?php echo json_encode($latest_poll['options']); ?>;
</script>
<?php endif; ?>
<div class="widget clearfix">
          	<div class="widget-head poll-of-month">
                <span class="view-all">&#8226; <a href="#poll-view-all-box" id="poll-view-all" class="fancybox">View all polls (<?php echo $poll_count; ?>)</a></span>
                <h3>Latest polls.</h3>
	            <div class="poll-question"><strong><?php echo $latest_poll['title']; ?></strong></div>
            </div>
            <div class="box shadow-2">
                <ul>
                    <li>
                        <form id="vPoll_wid_<?php echo $latest_poll['poll_id']; ?>" class="vPoll" method="post">
                        	<input type="hidden" id="poll_id" value="<?php echo $latest_poll['poll_id']; ?>" />
                        	<?php if(! $already_voted){ ?>
	                        	<?php foreach($latest_poll['options'] as $option): ?>
	                            	<label for="poll_yes"><input name="options" type="radio" value="<?php echo $option['option_id']; ?>"> <?php echo $option['title']; ?></label><br />
	                        	<?php endforeach; ?>
	                            <label><input type="button" class="btn-sm submit" name="submit" value="Vote" /></label>
                       	 	<?php } ?>
                        </form>
                        <ul id="vPollResponse_wid_<?php echo $latest_poll['poll_id']; ?>" class="PollResponse widpoll"></ul>
                    </li>
                </ul>
          </div>
        </div>
<?php endif; ?>

<div id="poll-view-all-box" class="poll-all-box fb-container-box d-n">

	<?php $polls = $this->poll_lib->all_polls(10, 0, 'Concept'); ?>
	<?foreach($polls as $poll){
	$alreadyvoted = $this->poll_lib->has_previously_voted($poll['poll_id'], $myprofile['id']);
	?>
		<ul>
					<li><b><?php echo $poll['title']; ?></b></li>
		</ul>
		<?php if(!$alreadyvoted) { ?>
					<form method="post" class="vPoll" id="vPoll_home_<?php echo $poll['poll_id']; ?>">
					<input type="hidden" id="poll_id" value="<?php echo $poll['poll_id']; ?>" />
					<fieldset>
					<ul>
					<li>
					<?php foreach($poll['options'] as $option){ ?>
					<label for="options"><input type="radio" value="<?php echo $option['option_id']; ?>" name="options"> <?php echo $option['title']; ?></label>
					<?php } ?>
					</li>
					<li><label><input type="button" value="Vote" name="submit" class="btn-sm submit"></label></li>
					</ul>
					
		</fieldset>
		</form>

		<?php } 
		else
		{
		?>
				<ul class="PollResponse">
				
				<li>
				<?php 
				$i = 0;
				foreach($poll['options'] as $option){ ?>
				<label><?php echo $option['title']; ?></label>
				<div id="progressBar_home_<?php echo $poll['poll_id'];?>_<?php echo $i;?>" class="progressBar">
				<div style="width: <?php echo $progress = $option['percentage'] * 152 / 100;?>px;"><?php echo round(floatval($option['percentage']));?>%&nbsp;</div>
				</div>
				<?php } ?>
				</li>
				</ul>
		<?php
		}
		?>
		
		<ul id="vPollResponse_home_<?php echo $poll['poll_id']; ?>" class="PollResponse"></ul>
		<hr class="dotted sp-25" />
	<?}
	//exit;
	?>

</div>

