<div id="footer" class="wrapper">
&copy; Landmark <?php echo date('Y'); ?>
</div>
</div>
</div>
<div id="others_view_list" class='bd-all-box fb-container-box d-n'>
</div>
<div id="others_comment_view_list" class='bd-all-box fb-container-box d-n'>
</div>
<?php $this->load->view('popups/people'); ?>

<?php $this->load->view('popups/feedback'); ?>
<?php $this->load->view('popups/change_password'); ?>
<?php
    $data['message_id'] = '';
    $this->load->view("popups/report_status_update", $data);
    $this->load->view("popups/report_market", $data);
    $this->load->view("popups/report_file", $data);
?>    
<script src="<?php echo site_url(); ?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>media/js/classie.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>media/js/uisearch.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>media/js/snap.min.js"></script>
<script type="text/javascript">
    var addEvent = function addEvent(element, eventName, func) {
        if (element.addEventListener) {
            return element.addEventListener(eventName, func, false);
        } else if (element.attachEvent) {
            return element.attachEvent("on" + eventName, func);
        }
    };

    addEvent(document.getElementById('left-drawer'), 'click', function() {
        snapper.open('left');
    });

    /* Prevent Safari opening links when viewing as a Mobile App */
    (function(a, b, c) {
        if (c in b && b[c]) {
            var d, e = a.location,
                f = /^(a|html)$/i;
            a.addEventListener("click", function(a) {
                d = a.target;
                while (!f.test(d.nodeName)) d = d.parentNode;
                "href" in d && (d.href.indexOf("http") || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)
            }, !1)
        }
    })(document, window.navigator, "standalone");

    var snapper = new Snap({
        element: document.getElementById('content')
    });

    addEvent(document.getElementById('toggle-left'), 'click', function() {
        if (snapper.state().state == "left") {
            snapper.close();
        } else {
            snapper.open('left');
        }
    });

    addEvent(document.getElementById('toggle-right'), 'click', function() {
        if ($('.snap-drawer-right > div').children().length == 0) {
            snapper.settings({
                disable: 'right'
            });
        } else {
            if (snapper.state().state == "right") {
                snapper.close();
            } else {
                snapper.open('right');
            }
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#media-filter-dd').hide();
        $('a.more-filters').click(function() {
            if ($(this).parent().next().is(':hidden')) {
                $('a.more-filters').removeClass('openMenucontent').parent().next().slideUp(500);
                $(this).toggleClass('openMenucontent').parent().next().slideDown(500);
            } else {
                $('a.more-filters').removeClass('openMenucontent').parent().next().slideUp(500);
            }
            return false;
        });

        $("a.more-filters").click(function() {
            if ($(this).text() == "More")
                $(this).text("Hide");
            else
                $(this).text("More");
        });

        $('.media-dropdown').hide();
        $('.nav-drop-down').click(function() {
            if ($(this).next().is(':hidden')) {
                $('ul#menu li a').removeClass('openMenucontent').next().slideUp(500);
                $(this).toggleClass('openMenucontent').next().slideDown(500);
            } else {
                $('ul#menu li a').removeClass('openMenucontent').next().slideUp(500);
            }
            return false;
        });

        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
            input.removeClass('placeholder');
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                //input.val(input.attr('placeholder'));
            }
        }).blur();

        completeProfile();

        $(window).resize(function() {
            completeProfile();
        });

        var widgets = $('.right-contents').html();
        $('.snap-drawer-right > div ').html('');
        $('.snap-drawer-right > div').html(widgets);

        if ($('.snap-drawer-right > div').children().length == 0) {
            snapper.settings({
                disable: 'right'
            });
        }

        if ($(window).width() >= 600 && $(window).width() <= 767) {
            if ($('.snap-drawer-right > div').children().length == 0) {
                $('#media-header .widget-menu').hide();
                $('#media-header .search').css({
                    'width': '89%',
                    'margin-right': '2%'
                });
            } else {
                $('#media-header .widget-menu').show();
                $('#media-header .search').css({
                    'width': '82%',
                    'margin-right': '0%'
                });
            }
        }

        if ($(window).width() >= 481 && $(window).width() <= 599) {
            if ($('.snap-drawer-right > div').children().length == 0) {
                $('#media-header .widget-menu').hide();
                $('#media-header .search').css({
                    'width': '86%',
                    'margin-right': '2%'
                });
            } else {
                $('#media-header .widget-menu').show();
                $('#media-header .search').css({
                    'width': '76%',
                    'margin-right': '0%'
                });
            }
        }

        if ($(window).width() >= 361 && $(window).width() <= 480) {
            if ($('.snap-drawer-right > div').children().length == 0) {
                $('#media-header .widget-menu').hide();
                $('#media-header .search').css({
                    'width': '83%',
                    'margin-right': '2%'
                });
            } else {
                $('#media-header .widget-menu').show();
                $('#media-header .search').css({
                    'width': '70%',
                    'margin-right': '0%'
                });
            }
        }

        if ($(window).width() >= 320 && $(window).width() <= 360) {
            if ($('.snap-drawer-right > div').children().length == 0) {
                $('#media-header .widget-menu').hide();
                $('#media-header .search').css({
                    'width': '83%',
                    'margin-right': '2%'
                });
            } else {
                $('#media-header .widget-menu').show();
                $('#media-header .search').css({
                    'width': '70%',
                    'margin-right': '0%'
                });
            }
        }

        if ($(window).width() <= 319) {
            if ($('.snap-drawer-right > div').children().length == 0) {
                $('#media-header .widget-menu').hide();
                $('#media-header .search').css({
                    'width': '79%',
                    'margin-right': '2%'
                });
            } else {
                $('#media-header .widget-menu').show();
                $('#media-header .search').css({
                    'width': '66%',
                    'margin-right': '0%'
                });
            }
        }

        if ($(window).width() >= 768) {
            snapper.settings({
                disable: 'right'
            });
        }

        if ($(window).width() >= 943) {
            $('.snap-content').attr('data-snap-ignore', true);
        }

        if ($(window).width() <= 942) {
            var notifyHgt = $('.notification div.jqi').outerHeight();
            var realHgt = (notifyHgt / 2);
						$('.notification div.jqi').css({
							  'margin-top': -(realHgt)
						});
            
            $('#recent-posts ul.post-cmntlike li.hide a .hide-tooltip').hide();
            $('#recent-posts ul.post-cmntlike li.hide').click(function() {
                var which = $(this).index();
                $('#recent-posts ul.post-cmntlike li.hide a').find('#recent-posts ul.post-cmntlike li.hide a .hide-tooltip').hide().eq(which).show();
            });

            $('#scroll-to-top').click(function() {
                $(this).css({
                    'background': '#ccc',
                    'color': '#444'
                });
            });
        }

        $(window).resize(function() {
            if ($(window).width() >= 600 && $(window).width() <= 767) {
                if ($('.snap-drawer-right > div').children().length == 0) {
                    $('#media-header .widget-menu').hide();
                    $('#media-header .search').css({
                        'width': '89%',
                        'margin-right': '2%'
                    });
                } else {
                    $('#media-header .widget-menu').show();
                    $('#media-header .search').css({
                        'width': '82%',
                        'margin-right': '0%'
                    });
                }
            }

            if ($(window).width() >= 481 && $(window).width() <= 599) {
                if ($('.snap-drawer-right > div').children().length == 0) {
                    $('#media-header .widget-menu').hide();
                    $('#media-header .search').css({
                        'width': '86%',
                        'margin-right': '2%'
                    });
                } else {
                    $('#media-header .widget-menu').show();
                    $('#media-header .search').css({
                        'width': '76%',
                        'margin-right': '0%'
                    });
                }
            }

            if ($(window).width() >= 361 && $(window).width() <= 480) {
                if ($('.snap-drawer-right > div').children().length == 0) {
                    $('#media-header .widget-menu').hide();
                    $('#media-header .search').css({
                        'width': '83%',
                        'margin-right': '2%'
                    });
                } else {
                    $('#media-header .widget-menu').show();
                    $('#media-header .search').css({
                        'width': '70%',
                        'margin-right': '0%'
                    });
                }
            }

            if ($(window).width() >= 320 && $(window).width() <= 360) {
                if ($('.snap-drawer-right > div').children().length == 0) {
                    $('#media-header .widget-menu').hide();
                    $('#media-header .search').css({
                        'width': '83%',
                        'margin-right': '2%'
                    });
                } else {
                    $('#media-header .widget-menu').show();
                    $('#media-header .search').css({
                        'width': '70%',
                        'margin-right': '0%'
                    });
                }
            }

            if ($(window).width() <= 319) {
                if ($('.snap-drawer-right > div').children().length == 0) {
                    $('#media-header .widget-menu').hide();
                    $('#media-header .search').css({
                        'width': '79%',
                        'margin-right': '2%'
                    });
                } else {
                    $('#media-header .widget-menu').show();
                    $('#media-header .search').css({
                        'width': '66%',
                        'margin-right': '0%'
                    });
                }
            }

            if ($(window).width() >= 768) {
                snapper.settings({
                    disable: 'right'
                });
            }

            if ($(window).width() >= 943) {
                $('.snap-content').attr('data-snap-ignore', true);
            }

            if ($(window).width() <= 942) {
								var notifyHgt = $('.notification div.jqi').outerHeight();
								var realHgt = (notifyHgt / 2);
								$('.notification div.jqi').css({
										'margin-top': -(realHgt)
								});
								
								$('#recent-posts ul.post-cmntlike li.hide a .hide-tooltip').hide();
								$('#recent-posts ul.post-cmntlike li.hide').click(function() {
										var which = $(this).index();
										$('#recent-posts ul.post-cmntlike li.hide a').find('#recent-posts ul.post-cmntlike li.hide a .hide-tooltip').hide().eq(which).show();
								});
		
								$('#scroll-to-top').click(function() {
										$(this).css({
												'background': '#ccc',
												'color': '#444'
										});
								});
						}
        });

        $('.iconMobile a').each(function() {
            var calMobile = "tel:" + $(this).html().split(" ").join("");
            $(this).attr('href', calMobile);
        });

        $('.iconTelephone a').each(function() {
            var calTelephone = "tel:" + $(this).html().split(" ").join("");
            $(this).attr('href', calTelephone);
        });
    });

    function completeProfile() {
        var prflComplete = $('.profile-block img.avatar-big').width();
        $('.profile-block .p-lft .progress-c #progressBar').css({
            'width': prflComplete
        });
    }
</script>
<script>
	new UISearch( document.getElementById( 'sb-search' ) );
</script>

