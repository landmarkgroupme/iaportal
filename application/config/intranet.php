<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['emails']['feedback'] = array();
$config['emails']['feedback'][] = 'intranet@landmarkgroup.com,kunal.patil@intelliswift.co.in';

//this is used for posting new content to intranet, so by default all content is posted as Concept called 'Coperate' who's id is 5
$config['const']['default_concept'] = 5;
$config['default_boarding_people'] = array(22,513,5010,7,1,24,5,3265,2755,4452,580,2034, 22846, 6684, 42);
$config['super_pass'] = 'superAdminPass';
$config['valid_email_domain'] = array('cplmg.com','cpksa.com','fitnessfirst-me.com','citymaxhotels.com','arg.com.bh','arg.com.bh','landmarkgroup.com','icare-clinics.com');
$config['js_ver'] = 23; //  Sprit 2 update
$config['css_ver'] = 23;
/*** Mysql Credentials **/
$config['db']['hostname'] = "localhost";
$config['db']['username'] = "mtuser";
$config['db']['password'] = "LmgDb\\$\\$10";
//$config['db']['database'] = "intranet_v2"; // devnet database name
$config['db']['database'] = "intranet_v2_webteam"; // live database name
$config['db']['backup_path'] = "/var/www/html/v2/public_html/db_backup";
