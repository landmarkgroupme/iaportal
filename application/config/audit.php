<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

#$config['audit']['self_compliance_portal_group_id'] = 695;
$config['audit']['internal_audit_plan_group_id'] = 715;
#$config['audit']['internal_audit_plan_group_id'] = 702;
$config['audit']['knowledge_center_group_id'] = 36;
#$config['audit']['knowledge_center_group_id'] = 701;
$config['audit']['user_guide_group_id'] = 716;
#$config['audit']['user_guide_group_id'] = 700;
$config['audit']['outlet_audit_reports_group_id'] = 717;
#$config['audit']['outlet_audit_reports_group_id'] = 703;
$config['audit']['process_audit_reports_group_id'] = 718;
#$config['audit']['process_audit_reports_group_id'] = 704;

/*$config['audit']['outlet_audit_report_tag_id'] = 201;
$config['audit']['process_audit_report_tag_id'] = 203;
$config['audit']['user_guide_tag_id'] = 200;
$config['audit']['knowledge_base_tag_id'] = 199;*/

/** Tags Title as per the group name **/
$config['audit']['internal_audit_plan_tag_title'] = 'Audit Plan';
$config['audit']['knowledge_center_tag_title'] = 'File Type';
$config['audit']['user_guide_tag_title'] = 'User Guide';
$config['audit']['outlet_audit_reports_tag_title'] = 'Outlets';
$config['audit']['process_audit_reports_tag_title'] = 'Processes';
