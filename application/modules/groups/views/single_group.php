<!DOCTYPE html>
<html>
  <head>
    <link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
    <meta name="robots" content="index, nofollow">
    <title><?php echo $group['name']; ?> - Landmark Group</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <?php $this->load->view('include_files/common_includes_new'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>css/facebox_manage.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>css/moderation/calendrical.css" />
    <style>
   
    </style>
	<script>
	var group = <?php echo isset($group['id'])? $group['id'] : 0; ?>;
	var group_page = true;	
	var filter_user = <?php echo isset($statuses_filter) ? $statuses_filter : 0; ?>;
</script>
  </head>
<body class="profile">

  <?php $this->load->view('templates/js/people'); ?>

  <?php $this->load->view('templates/js/people_list'); ?>

  <?php $this->load->view('global_header');
  
  //echo "<pre>".print_r($myprofile,"\n")."</pre>";exit;
   ?>

  <div class="section wrapper clearfix">
  	<h2>Groups</h2>
	<ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="#">Groups</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><?php echo $group['name']; ?></li>
		</ul>
  </div>
<div class="section wrapper clearfix">
  	<div class="left-contents">

          <div class="profile-block clearfix">
          	<div class="p-lft"><img class="avatar-big shadow-3" src="<?php echo site_url(); ?>images/groups/thumbs/<?php echo $group['thumbnail']; ?>" alt="<?php echo $group['name']; ?>" /></div>
              <div class="p-rgt profile-intro">
              	<h1><?php echo $group['name']; ?></h1>
                  <div class="x-members"><span><?php echo $group['total_members']; ?></span> Members</div>
                  <ul>
                  	<li>
                    <?php if($group['member'])
                    	{
						if($group['id'] == $audit_constant['internal_audit_plan_group_id'] || $group['id'] == $audit_constant['knowledge_center_group_id'] || $group['id'] == $audit_constant['user_guide_group_id'] || $group['id'] == $audit_constant['outlet_audit_reports_group_id'] || $group['id'] == $audit_constant['process_audit_reports_group_id']) { } else { ?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Group" object_id="<?php echo $group['id']; ?> name="btnFollow" value="Leave" /></div>
                    <?php
							}
                    	} else {
                    	   if($group['is_open'] == 'yes' || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member']))):
                    	?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow" object_type="Group" object_id="<?php echo $group['id']; ?> name="btnFollow" value="Join" /></div>
                    	<?php
                         endif;
                    	}
                	?>
  					         </li>
  				        </ul>
                  <?php echo $group['description']; ?>
  			     </div>
          </div>
          <?php if($group['member'] || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA']))
            {
				$this->load->view('partials/group_post_status', array('target_id' => $group['id'], 'target_type' => 'Group','created_by' => $myprofile['id'], 'created_by_type' => 'User'));
		  }
		  ?>
		  
          <?php if(isset($filetags) && !empty($filetags)): ?>
          <div class="upload-share-block for_tags">
    		    <div class="tags"><strong> Tags: </strong>
  						<?php
  							$last_key = end(array_keys($filetags)); 
  							foreach($filetags as $key => $filetag) { 
  								?>
  								<label class="lbl"><span value="<?php echo $filetag['id']; ?>"><?php echo ucwords($filetag['name']).' ('.$filetag['total'].')'; ?></span></label>
  								<?php 
  							} 
  						?>
  					</div>
          </div>
          <?php endif; ?>
          <?php if($group['member'] || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA']))
            {
			$this->load->view('partials/group_content_updates' ); 
			
			}
			?>

     </div> <!-- left-contents -->
     <div class="right-contents media-widget">
     <?php if($group['id'] == $audit_constant['internal_audit_plan_group_id'] || $group['id'] == $audit_constant['knowledge_center_group_id'] || $group['id'] == $audit_constant['user_guide_group_id'] || $group['id'] == $audit_constant['outlet_audit_reports_group_id'] || $group['id'] == $audit_constant['process_audit_reports_group_id']): ?>    
    <div class="widget members clearfix">
      <div class="widget-head useful-links">
        <h3>Important Links</h3>
      </div>
      <div class="box shadow-2">
        <ul>
          <li><a href="<?php print base_url(); ?>surveys">Back to Main Page</a></li>        
        </ul>
      </div>
    </div>
    <?php endif; ?>
	   <?php
	    if($group['member'] || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA']))
      {
      $this->load->view('widgets/groups_document_widget');
      }
      $this->load->view('group_file_upload');
      $this->load->view('widgets/group_members');
	    $this->load->view('widgets/groups_widget');
     ?>
     </div> <!-- right-contents -->
</div> <!-- section -->

<?php $this->load->view('global_footer'); ?>

<?php $this->load->view('partials/js_footer'); ?>

<?php $this->load->view('templates/js/content_updates'); ?>

<?php $this->load->view('templates/js/new_status'); ?>
<?php $this->load->view('templates/js/new_status_poll'); ?>
<?php $this->load->view('templates/js/new_upload_image'); ?>
<?php $this->load->view('partials/interaction_js'); ?>
<?php
if(in_array($user_id,$uploader) || in_array($user_id,$groupMember))
{
  $this->load->view('popups/group_file_popup');
  $this->load->view('popups/group_invite_user');
}
$this->load->view('popups/group_create_popup');
?>
<script type="text/javascript">
var limit = 10;
var start = 0;
var nearToBottom = 250;
var concept_id = <?php echo $group['id']; ?>;
var feedtype = 'group';
var feedtype_id = <?php echo $group['id']; ?>;
var group_id = <?php echo $group['id']; ?>;
var tag_id = '';
var group_end = false;
$(document).ready(function(){

var progressbox     = $('#progressbox');
var progressbar     = $('#fileprogressbar');
var statustxt       = $('#statustxt');
var completed       = '0%';

	$('.loader-more').show();
	$('#groups-all-view').fancybox({
	maxHeight	: 500
	})
  
  //loading this for members popup
	loadGroupMember({'noclear': 0, 'group': <?php echo $group['id']; ?>});
  
  //Focus on tags changes default text
  $("#txt_tags").focus(function(){
    var text = $("#txt_tags").val();
    if(text == "Start typing.."){
      $("#txt_tags").val("");
    }
  })
  $("#txt_tags").blur(function(){
    var text = $.trim($("#txt_tags").val());
    if(text == ""){
      $("#txt_tags").val("Start typing..");
    }
  })
  $("#txt_tags").autocomplete({
    source: function(request, response){
      $.ajax({
        url: siteurl+"files/autocomplete_files_tag",
        dataType: 'json',
        type:"post",
        data: {
          "term":extractLast(request.term),
          "group_id":group_id
        },
        success: response
      });
    },
    search: function(){
      // custom minLength
      var term = extractLast(this.value);
      if(term=="Start typing.."){
        return false;
      }
      if (term.length < 2) {
        return false;
      }
    },
    focus: function(){
      // prevent value inserted on focus
      return false;
    },
    select: function(event, ui){
      var terms = split(this.value);
      // remove the current input
      terms.pop();
      // add the selected item
      terms.push(ui.item.value);
      // add placeholder to get the comma-and-space at the end
      terms.push("");
      this.value = terms.join(", ");
      return false;
    }
  });
  
 	$('.lbl span').click(function(){
 	  tag_id = $(this).attr('value');
    start = 0;
    $("div#recent-posts .block-2.clearfix").remove();
		$("div#recent-posts .block-1.clearfix").remove();  
    loadUpdates('','');
  });
  //loading this for data feed
  <?php
  $tagGet = $this->input->get('tag'); 
  if(isset($tagGet) && !empty($tagGet))
  {
  ?>
  tag_id = <?php print $tagGet; ?>;
  $('span[value="<?php print $tagGet; ?>"]').parent('label').addClass('active');
  <?php } ?>
	loadUpdates('', $('#recent-posts ul.filter li:first'));

  $(".autocomplete_user").tokenInput("<?php echo base_url();?>manage/group_member_autocomplete?group_id=<?php echo $group['id']; ?>",{
      theme: "facebook",
      preventDuplicates: true
  });
  
  $(document).ready(function() {
	  $("#invite_members").tokenInput("<?=base_url();?>manage/auto_nongroup_users?group_id=<?php echo $group['id']; ?>",{
	    theme: "facebook",
      preventDuplicates: true
	  });
	});

	$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			if(loadProgress == false){
			var selected = $('#recent-posts ul.filter li[class=active] a');
			start += 10;
		 	loadUpdates($(selected).attr('type'), $('#recent-posts ul.filter li[class=active]'));
			}
		}
	});

	$('a.load_members').fancybox({
		maxHeight	: 700,
		width		: 943,
		autoSize	: false,
    afterLoad : function() {
			$('.fancybox-inner').attr('id','newfancy');
			$('#newfancy').on('scroll',function(){
				if ($('#newfancy').scrollTop() + $('#newfancy').height() >= $('#people_popup').offset().top + $('#people_popup').height() ) {
					//ajax
					start += 10;
					loadGroupMember({'noclear': 1, 'group': <?php echo $group['id']; ?>}); 
				}
			});
		}
	});

	$('.editProfile').fancybox({
		maxHeight: 500
	});
	$(".fancybox").fancybox({
		maxHeight	: 500
		
	}); 

	if(typeof poll_results !== 'undefined')
	{
		$.each(poll_results, function(index, item){
			_html	= '<li><label>'+item['title']+'</label><div id="progressBar'+index+'" class="progressBar"><div style="width:'+item['percentage']+'%">'+Math.round(Number(item['percentage']))+'%&nbsp;</div></div></li>';
			$('#vPollResponse').append(_html);
			//progress(item['percentage'], $('#progressBar'+index));
		});
	}
  $('.group_files').fancybox({
		maxHeight: 500,
    afterClose : function(){
      $('.error-msg').hide().html('');
      $("#frm_add_item")[0].reset();
      $(".autocomplete_user").tokenInput('clear');
	   progressbox.hide();
    },
	});
  $('.group_create').fancybox({
    maxHeight: 500,
    afterClose  : function() {
      $("#frm_add_group")[0].reset();
      $('.error-msg').html('');
	  $('#group_create .msg').html('');
	  $('#group_create .msg').hide();
	  $('#group_create .msg').removeClass('error');
	  $('#group_create .msg').removeClass('success');
    },
    beforeLoad : function() {
     var groupId = $(this)[0].element.context.attributes[1]['nodeValue'];
     if(!isNaN(groupId))
     {  
        LoadeditGroup(groupId);
        $('#form_popup_title').html('Edit Group');
        $('#bt_add_group').val('Edit Group');
     }
     else{
      $('#render_thumbnail').html('');
      $('#form_popup_title').html('Create a Group');
      $('#bt_add_group').val('Create a Group');
     }
    },
  })
  $('.group_invite').fancybox({
    maxHeight: 500,
    beforeLoad : function() {
      $("#invite_members").tokenInput('clear');
      
    },
    afterClose : function(){
      console.log("Closed");
      $('#group_invite .msg').html('');
	    $('#group_invite .msg').hide();
	    $('#group_invite .msg').removeClass('error');
      $('#group_invite .msg').removeClass('success');
    },
  }); 
  $('#bt_add_group').live('click',function(){
    if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
      if($('#frm_add_group #txt_title').val() == "Group Name") {
        $("#frm_add_group #error-txt_title").html("Required");
        $("#frm_add_group #txt_title").focus();
        return false;
      }
    }
      if(!$.trim($('#frm_add_group #txt_title').val())){
        $("#frm_add_group #error-txt_title").html("Required");
        $("#frm_add_group #txt_title").focus();
        return false;
      }
      else
      {
        $("#frm_add_group #error-txt_title").html("");
      }
      if(!$.trim($('#frm_add_group #txt_open').val())){
        $("#frm_add_group #error-txt_open").html("Required");
        $("#frm_add_group #txt_open").focus();
        return false;
      }
      else
      {
        $("#frm_add_group #error-txt_open").html("");
      }
     //   var formData = new FormData($('#frm_add_group')[0]);
    var options = {
    beforeSend: function() {
    			},
    complete: function() {
    			},
    type	: 'POST',
    cache	: false,
    url		: $('#frm_add_group').attr('action'),
    dataType: "json",
    success	: function(data) {
      if(data.status == true)
      {
        $('#frm_add_group')[0].reset();
        $('#group_create .msg').show().addClass('success').html('Group created successfully.');
        $.fancybox.close(); 
        if(data.reload == true)
        {
          window.location.reload();
        }
		else
		{
			window.location.href = siteurl+'groups/'+data.group_id
		}
        return true;
      }
      else
      {
	  if(data.duplicate == true)
        {
           $('#group_create .msg').show().addClass('error').html('Group name all ready exist.');
        }
		else{
        $('#group_create .msg').show().addClass('error').html('Something went wrong.');
		}
        return false;
      }
    }, 

    cache: false,
    contentType: false,
    processData: false
    };
    $('#frm_add_group').ajaxSubmit(options);
  });

  $('#send_group_invite').live('click',function(){
      //var formData = new FormData($('#group_invite_form')[0]);
      if($('#invite_members').val() == '')
      {
        $('#group_invite .msg').show().addClass('error').html('Please enter the people\'s name.');
        return false;
      }
      var options = {
				beforeSend: function() {
							},
				complete: function() {
							},
				type	: 'POST',
				url		: $('#group_invite_form').attr('action'),
				dataType: "json",
				success	: function(data) {
				  if(data.status == true)
          {
            $('#group_invite_form')[0].reset();
            $('#group_invite .msg').show().addClass('success').html('Invitation send successfully.');
            $.fancybox.close(); 
            return true;
          }
          else
          {
            $('#group_invite .msg').show().addClass('error').html('Something went wrong.');
            return false;
          }
				},
			};
      $('#group_invite_form').ajaxSubmit(options);
  });
  
 
$('#bt_add_item').live('click',function(e) {
  	e.preventDefault();
		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;
		inactive_form();

		var base_url = $("#base_url").val();
		var cat = $.trim($("#cmb_cat").val());
    var concept = $.trim($("#file_concept").val());
		var file_share = $.trim($("#file_share").val());
		var title = $.trim($("#txt_title11").val());
    var group = $.trim($("#group").val());
    
		if(!cat){
      $("#error-cmb_cat").html("Required");
  		$("#cmb_cat").focus();
  		$('#group_upload_file .msg').removeClass('success');
  		active_form();
  		return false;
		}
    /*else if(!concept)
    {
      $("#error-concept").html("Required");
  		$("#file_concept").focus();
  		$('.msg').removeClass('success');
  		active_form();
  		return false;
    }*/
		else
		{
		  $("#error-cmb_cat").html("");
      $("#error-concept").html("");
		  $('.msg').removeClass('success');
		}


		if(file_share){
		  $("#error-files_upload").html("");
		  $('#group_upload_file .msg').removeClass('success');
		}
		else if((file_share == "")){

			$("#error-files_upload").html("Required");
			$("#error-files_upload").focus();
			$('#group_upload_file .msg').removeClass('success');
			
			active_form();
			return false;
		}
		else{
		$("#error-files_upload").html("");
		$('#group_upload_file .msg').removeClass('success');
		}
    if(!title){
			$("#error-txt_title11").html("Required");
			$("#txt_title11").focus();
			$('#group_upload_file .msg').removeClass('success');
			
			active_form();
			return false;
		}
		else{
			//var formData = new FormData($('#frm_add_item')[0]);
      var options = {
					beforeSend: function() {
								inactive_form();
								progressbox.show(); //show progressbar
								progressbar.width(completed); //initial value 0% of progressbar
								statustxt.html(completed); //set status text
								statustxt.css('color','#000'); //initial color of status text
								},
					uploadProgress: OnProgress,
					complete: function() {
								active_form();
								start = 0;
								},
					type	: 'POST',
					cache	: false,
					url		: $('#frm_add_item').attr('action'),
					success	: function(data2) {
					obj = JSON.parse(data2);
						if(obj.done === 'yes' && obj.samename === 'no')
						{
						  request_data = {'group_id': group, 'file_id' : obj.file_id};
						  $.ajax({
								url: siteurl+'files/ajax_group_file',
								data: request_data,
								dataType: "json",
								type: "POST",
								success: function(response){
								 if(response.status)
								 {
									$("input[type=text], textarea , select ").val("");
									$("input[type=file]").val("");
									$('#group_upload_file .msg').addClass('success');
									$('#group_upload_file .msg').append('<li/>').html('Your file has been uploaded successfully!');
									$('#group_upload_file .msg').hide();
									$("div#recent-posts .block-2.clearfix").remove();
									$("div#recent-posts .block-1.clearfix").remove();  
									loadUpdates('','');
									setTimeout(function(){$.fancybox.close();}, 500);
								  }
								  },
							});
						}
						else if(obj.samename === 'yes')
						{
							$('#group_upload_file .msg').removeClass('success');
							$('#group_upload_file .msg').addClass('error');
							$('#group_upload_file .msg').append('<li/>').html('A file with this name already exists. Please change name to continue.');
							$('#group_upload_file .msg').hide();
						}
						else
						{
							$('#group_upload_file .msg').removeClass('success');
							$('#group_upload_file .msg').addClass('error');
							$('#group_upload_file .msg').append('<li/>').html('Please retry, enter data');
							$('#group_upload_file .msg').hide();
						}


					},
					cache: false,
					contentType: false,
					processData: false
			};
      $('#frm_add_item').ajaxSubmit(options);
		}
		

		return false;

	});
function OnProgress(event, position, total, percentComplete)
{
	//Progress bar
	progressbar.width(percentComplete + '%') //update progressbar percent complete
	statustxt.html(percentComplete + '%'); //update status text
	if(percentComplete>50)
		{
			statustxt.css('color','#fff'); //change status text to white after 50%
		}
}
function LoadeditGroup(group_id){
  var data = {'group_id' : group_id}; 
  var service_url = siteurl+"manage/load_group_ajax";
  $.ajax({
    url: service_url,
    data: data,
    async: false,
    dataType: "json",
    type: "POST",
    success: function(data){
      if(data.status == true)
      {
        $('#txt_title').val(data.record.name);
        $('#txt_body').val(data.record.description);
        $('#txt_open').val(data.record.is_open);
        $('#group_id').val(data.record.id);
        if(data.record.thumbnail.indexOf('default') == -1)
        {                
          var image_url = siteurl+"images/groups/thumbs/"+data.record.thumbnail;
        }
        else
        {
          var image_url = siteurl+"images/groups/"+data.record.thumbnail;
         
        }                                                                                            
        $('#render_thumbnail').html('<img height="100" width="100" alt="'+data.record.name+'" src="'+image_url+'">');    
        if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
          $("#txt_title").removeClass("ie-placeholder");
          $("#txt_title").removeAttr('data-placeholder');
        }    
      }
    }
  });
}
  
function loadGroupMember(prop)
{
  $('.loader-more').show();
  var searched = ($('#txtSearch').length > 0 && $('#txtSearch').val() != '') ? true : false;
  //console.log(searched);
  if(!prop.noclear)
  {
    $('.people-grid').html('');
    $('.people-list .row:not(.head)').remove();
  }
  var service_url = siteurl+"people/people_ajax";
  if(searched)
  {
    var search = $('#txtSearch').val();
    var data = {'limit' : limit, 'start' : start, 'search_term': search};
  } 
  else 
  {
    var concept = prop.concept ? prop.concept : $('#ddConcepts').val();
    var department = $('#ddDepartments').val();
    var country = $('#ddCountry').val();
    var group = prop.group ? prop.group : 0;
    var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country,'group':group};
  }
  console.log(group_end);
  if(group_end == false)
  {
    $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "GET",
      success: function(data){
        console.log(_.size(data));
        if(_.size(data) > 0)
        {
          $('.no-result-block').addClass('d-n');
          
          _.templateSettings.variable = "rc";
          
          // Grab the HTML out of our template tag and pre-compile it.
          var template = _.template(
          $( "#people_card" ).html()
          );
          
          
          $('div.people-grid').append(template(data));
          
          var template = _.template(
          $( "#people_list" ).html()
          );
          
          $('div.people-list').append(template(data));
          
          if($('.l-list-view').hasClass('active')) {
          $('.people-list').css('display', 'table');
          $('.people-grid').css('display', 'none');
          } else {
          $('.people-list').css('display', 'none');
          $('.people-grid').css('display', 'table');
          }
          $('.people-grid-view').removeClass('d-n');
        
        } else {
          group_end = true;
          if(!prop.scroll && start == 0)
          {
          $('.no-result-block').text('No Users found for specified criteria');
          $('.no-result-block').removeClass('d-n');
          $('.people-list .row:not(.head)').remove();
          $('.people-list').css('display', 'none');
          $('.people-grid').css('display', 'none');
          $('.people-grid-view').addClass('d-n');
          }
        }
        $('.loader-more').hide();
      }
    });
  }
}
  

function active_form()
{
$.fancybox.hideLoading();
$('#bt_add_item').removeClass('inactive');
$('#bt_add_item').removeAttr('disabled');

}
function split(val){
  return val.split(/,\s*/);
}
function extractLast(term){
  return split(term).pop();
}
function inactive_form()
{
$.fancybox.showLoading();
$('#bt_add_item').addClass('inactive');
$('#bt_add_item').attr('disabled','disabled');
}  
});

</script>

</body>
</html>


