    <!DOCTYPE html>
<html>
<head>
<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Intranet User Groups - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.load_more_group
{
    margin-left: 801px;
}

</style>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
	<h2>Intranet User Groups</h2>
	 <ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Groups</li>
    </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

<?
	//include_once "ssi/comp_jobs_filter.php";
  //echo "<pre>".print_r($myprofile,"\n")."</pre>";
?>

<div id="media-group">
    <div class="concept-grid clearfix">

            <div class="concept-intro">
            	<h1>Follow Your Favourite Groups</h1>
                Meet like-minded people with similar interests.
            </div>

              <?php
            	$counter = 0;

               /*  $top_groups = array_slice($groups, 0, 5);
                $other_groups = array_slice($groups, 5);
               

                
                $others = array();
                foreach($other_groups as $group)
                {
                    if($group['is_open'] == 'yes' || (isset($group['valid_members']) && !empty($group['valid_members']) && in_array($user_id,$group['valid_members'])) || ($myprofile['role_id'] == $permission_roles['SA']) || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member']))):
					$count = 0;
					if (isset($others[strtoupper(substr($group['name'], 0, 1))])) {
						$count = count($others[strtoupper(substr($group['name'], 0, 1))]);
					}
					
					 if ($count < 3) {
						 $others[strtoupper(substr($group['name'], 0, 1))][] = $group;
					  } 
					  
						//echo "<pre>"; print ;
                    endif;
                }

                ksort($others); */
				 //echo "<pre>".print_r($others,"\n")."</pre>"; exit;
              $excludeGroup = array();
            	foreach($top_groups as $group){
         	      $excludeGroup[] = $group['id'];
                if($group['is_open'] == 'yes' || (isset($group['valid_members']) && !empty($group['valid_members']) && in_array($user_id,$group['valid_members'])) || ($myprofile['role_id'] == $permission_roles['SA']) || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member']))):
            	?>




           <div class="concept-box media-hide <?php echo $counter % 3 == 0 ? 'mid' : ''; ?>">

				<a href="<?php echo site_url(); ?>groups/<?php echo $group['id']; ?>">
					<span class="cp-span-big cp-img-big" style="background: url('<?php echo site_url(); ?>images/groups/thumbs/<?php echo $group['thumbnail']; ?>') no-repeat scroll center center #CCC;">
						<span class="cp-span-text"><div><?php echo ucwords(strtolower($group['name'])); ?></div></span>
					</span>
				</a><br/>

				<?php if($group['total_members'] > 0) { ?>
                <div class="x-members"><span><?php echo $group['total_members']; ?></span> Members</div>
				<?php } else { ?>
				<div class="x-members"><span>Be the first to Join</div>
				<?php } ?>
                <div class="cp-followers-bx clearfix">
                	<div class="cp-followers">
                    	<div class="imgMembers">
                    		<?php 
							$mem_count = 0;
							foreach ($group['members'] as $member) {
                    			?>
                    			<a href="http://devnet.landmarkgroup.com/<?php echo $member['reply_username']; ?>" title="<?php echo $member['fullname']; ?>"><img src="http://devnet.landmarkgroup.com/images/user-images/105x101/<?php echo $member['profile_pic']; ?>" alt="<?php echo $member['fullname']; ?>"></a>
                    			<?php
								$mem_count++;
								if($mem_count==8) break;
                    		}
                    		?>

                        </div>
                    </div>
                    <?php if($group['member'])
                    	{
                    	?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Group" object_id="<?php echo $group['id']; ?> name="btnFollow" value="Leave" /></div>
                    	<?php
                    	} else {
                    	   if($group['is_open'] == 'yes' || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member'])))
                         {
                    	?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow" object_type="Group" object_id="<?php echo $group['id']; ?> name="btnFollow" value="Join" /></div>
                    	<?php
                         }
                    	}
                	?>
                </div>
        	</div>
			
			            <!-- Shows in responsive view -->
            
            <div class="concept-box media-show">
               <a href="<?php echo site_url(); ?>groups/<?php echo $group['id']; ?>">
                  <span class="cp-span-big cp-img-big" style="background: url('<?php echo site_url(); ?>images/groups/thumbs/<?php echo $group['thumbnail']; ?>') no-repeat scroll center center #CCC;"></span>
               </a>
               <span class="cp-span-text">
                  <div><?php echo ucwords(strtolower($group['name'])); ?></div>
               </span>
               
               <div class="cp-followers-bx clearfix">
                  <div class="x-members"><span><?php echo $group['total_members']; ?></span></div>
                  <?php if($group['member'])
                     {
                     ?>
                  <div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Group" object_id="<?php echo $group['id']; ?> name="btnFollow" value="Leave" /></div>
                  <?php
                     } else {
                        if($group['is_open'] == 'yes' || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member'])))
                         {
                     ?>
                  <div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow" object_type="Group" object_id="<?php echo $group['id']; ?> name="btnFollow" value="Join" /></div>
                  <?php
                     }
                     }
                     ?>
               </div>
            </div>
            <!-- End Shows in responsive view -->
        	
            
            <?php  

        	$counter++;
          endif;
        	} ?>


            <div class="concept-table">
                <div class="view-head">
                    <span>all Groups a-z</span>
                </div>
                <?php
                foreach($others as $letter => $groups)
                {
                  
                  ?>
                <div class="concept-row">
                   <div class="alphabet"><?php echo strtoupper($letter); ?></div>
                   <div class="group-data" id="container-<?php echo strtoupper($letter); ?>">
                        <ul>
                            <?php foreach($groups as $group)
                            {
                            ?>
                                <li>
                                    <span class="name"><a href="<?php echo site_url(); ?>groups/<?php echo $group['id']; ?>"><?php echo ucwords(strtolower($group['name']));?></a></span>
									<?php if($group['total_members'] > 0) { ?>
										<span class="count"><?php echo $group['total_members']; ?> members</span>
									<?php } else { ?>
										<span class="count">Be the first to Join</span>
									<?php } ?>
                                <?php 
                                if($group['member']):
                                ?>
                                    <input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Group" object_id="<?php echo $group['id']; ?>" name="btnFollow" value="Leave" />
                                <?php else: 
                                    if($group['is_open'] == 'yes' || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member']))):
                                ?>
                                    <input type="button" class="btn-sm btn-object-follow" object_type="Group" object_id="<?php echo $group['id']; ?>" name="btnFollow" value="Join" />
                                <?php 
                                    endif;
                                endif; 
                                ?>
                                </li>
                            <?php
                              
                            } ?>                            
                        </ul>
                   </div>
                </div>
                <div id="media-more"><a href="#" class="load_more_group" id="<?php echo strtoupper($letter); ?>">More ..</a></div>
                <?php 
                
                } ?>
                
               
            </div>













    </div>	<!-- concept-grid -->

</div>
	<!-- <div class="loader-more clearfix">&nbsp;</div> -->


	</div> <!-- container -->


    </div>

	<div class="right-contents media-widget">
  </div> <!-- right-contents -->
</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>
<script type="text/javascript">
$(window).load(function(){
        var tourSubmitFunc = function(e,v,m,f){
            if(v === -1){
                $.prompt.prevState();
                return false;
            }
            else if(v === 1){
                $.prompt.nextState();
                return false;
            }
            else if(v === 0){
                $.prompt.goToState(0);
                return false;
            }
},
tourStates = [
    {
        title: 'Join Groups',
        html: 'Join Groups, create events - have fun with your co-workers',
        buttons: { Next: 1 },
        focus: 0,
        position: { container: '.cp-btn-follow', x: 80, y: -10, width: 300, arrow: 'lt' },
        submit: tourSubmitFunc
    },
    {
        title: 'Audio/Video Chat',
        html: 'Chat with anyone on your list',
        buttons: { Prev: -1, Next: 1 },
        focus: 1,
        position: { container: '#cometchat_userstab', x: -125, y: 30, width: 300, arrow: 'bc' },
        submit: tourSubmitFunc
    },
    {
        title: 'Chat rooms',
        html: 'Create your own public or private chat room, and invite co-workers to be part of discussion',
        buttons: { Prev: -1, Next: 1 },
        focus: 1,
        position: { container: '#cometchat_trayicon_chatrooms', x: 0, y: 30, width: 300, arrow: 'bl' },
        submit: tourSubmitFunc
    },
    {
        title: 'Controls',
        html: 'Control your chat settings',
        buttons: { Prev: -1, First: 0, Done: 2 },
        focus: 0,
        position: { container: '#cometchat_optionsbutton_icon', x: -300, y: 30, width: 300, arrow: 'br' },
        submit: tourSubmitFunc
    }
];
if(<?php echo $user_prompt;?>)
{
    $.prompt(tourStates);
	var data1 = {'user_id' : myprofile.id, 'feature' : 'Group'};
	var url = siteurl+"status_update/add_prompt";
	$.ajax({
		url: url,
		data: data1,
		async: false,
		dataType: "json",
		type: "POST",
		success: function(data){

		}
	});
}
});
$(document).ready(function(){
  $('.load_more_group').on('click',function(e){
    e.preventDefault();
    var alpha = $(this).attr('id');
    var service_url = siteurl+"groups/ajax_load_all_group";
    var data = {'alpha' : alpha,'exclude':'<?php echo implode(',',$excludeGroup);?>'};
    $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "POST",
      beforeSend : function(){
        $.fancybox.showLoading();
      },
      complete: function(){
        
      },
      success: function(data){
        if(data.status == true)
        {
          var html = '<ul>';
          $.each(data.records,function(a,record) {
            html+= '<li>';
            html+= '<span class="name"><a href="'+siteurl+'groups/'+record.id+'">'+record.name+'</a></span>';
			if(record.total_members > 0){
				html+='<span class="count">'+record.total_members+' members</span>';
			} else {
				html+='<span class="count">Be the first to Join</span>';
			}
            if(record.is_member == 1)
            {
            html+='<input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Group" object_id="'+record.id+'" name="btnFollow" value="Leave" />';
            }
            else
            {
            html+='<input type="button" class="btn-sm btn-object-follow" object_type="Group" object_id="'+record.id+'" name="btnFollow" value="Join" />';
            }
            html+='</li>';
          });
          html+='</ul>';
          $('#container-'+alpha).html(html);
          $('#'+alpha).hide(1000,function(){
            $.fancybox.hideLoading();
          });
        }
        else{
          $.fancybox.hideLoading();
        }
      }
    });
  });
});

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
</script>

</body>
</html>