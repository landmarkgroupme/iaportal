<!DOCTYPE html>
<html>
  <head>
    <link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
    <meta name="robots" content="index, nofollow">
    <title><?php echo $group['name']; ?> - Landmark Group</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <?php $this->load->view('include_files/common_includes_new'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>css/facebox_manage.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>css/moderation/calendrical.css" />
   <style>
    #token-input-invite_members{
    margin-bottom: 11px;
    margin-left:3px;
    outline: 1px solid #CCCCCC !important;
    width: 376px;
    }
    /*#filter-search .filter-box{
    width: 556px !important;
    padding-bottom: 15px;
    position: relative;
    top: 20px;
    }*/
  #filter-search .search{
      /*width: 212px !important;
      top: 329px;
      position: absolute;
      right: 502px;*/
    }
    .textC #txtSearch{
      width: 85%;
    }
    </style>
	<script>
	var group = <?php echo isset($group['id'])? $group['id'] : 0; ?>;
	var group_page = true;	
	</script>
  </head>
<body class="profile">

  <?php $this->load->view('templates/js/people'); ?>

  <?php $this->load->view('templates/js/people_list'); ?>

  <?php $this->load->view('global_header'); ?>

  <div class="section wrapper clearfix">
  	<h2>Groups Document</h2>
	<ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="#">Groups</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="<?php print site_url();?>groups/<?php print $group['id']; ?>"><?php echo $group['name']; ?></a></li>
		</ul>
  </div>
<div class="section wrapper clearfix">
  	<div class="left-contents">

          <div class="profile-block clearfix">
          	<div class="p-lft"><img class="avatar-big shadow-3" src="<?php echo site_url(); ?>images/groups/thumbs/<?php echo $group['thumbnail']; ?>" alt="<?php echo $group['name']; ?>" /></div>
              <div class="p-rgt profile-intro">
              	<h1><?php echo $group['name']; ?></h1>
                  <div class="x-members"><span><?php echo $group['total_members']; ?></span> Members</div>
                  <ul>
                  	<li>
                   	<?php if($group['member'])
                    	{
						if($group['id'] == $audit_constant['internal_audit_plan_group_id'] || $group['id'] == $audit_constant['knowledge_center_group_id'] || $group['id'] == $audit_constant['user_guide_group_id'] || $group['id'] == $audit_constant['outlet_audit_reports_group_id'] || $group['id'] == $audit_constant['process_audit_reports_group_id']) { } else { ?>
                   		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Group" object_id="<?php echo $group['id']; ?>" name="btnFollow" value="Leave" /></div>
                    <?php
						}
                    	} else {
                    	   if($group['is_open'] == 'yes' || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($myprofile['id'],$group['invited_member']))):
                    	?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow" object_type="Group" object_id="<?php echo $group['id']; ?>" name="btnFollow" value="Join" /></div>
                    	<?php
                         endif;
                    	}
                	?>
  					         </li>
  				        </ul>
                  <?php echo $group['description']; ?>
  			     </div>
          </div>
          
          <script type="text/javascript">
            var filter_user = <?php echo isset($statuses_filter) ? $statuses_filter : 0; ?>;
          </script>
          
          <?php $this->load->view('partials/group_files_list',$files); ?>

     </div> <!-- left-contents -->
     <div class="right-contents media-widget">
    <?php if($group['id'] == $audit_constant['internal_audit_plan_group_id'] || $group['id'] == $audit_constant['knowledge_center_group_id'] || $group['id'] == $audit_constant['user_guide_group_id'] || $group['id'] == $audit_constant['outlet_audit_reports_group_id'] || $group['id'] == $audit_constant['process_audit_reports_group_id']): ?>    
    <div class="widget members clearfix">
      <div class="widget-head useful-links">
        <h3>Important Links</h3>
      </div>
      <div class="box shadow-2">
        <ul>
          <li><a href="<?php print base_url(); ?>surveys">Back to Main Page</a></li>        
        </ul>
      </div>
    </div>
    <?php endif; ?>
	   <?php
      if($group['member'] || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA']))
      {
      $this->load->view('widgets/groups_document_widget');
      }
      $this->load->view('group_file_upload');
     	$this->load->view('widgets/group_members');
		  $this->load->view('widgets/groups_widget');
      
     ?>
    
     </div> <!-- right-contents -->
</div> <!-- section -->

<?php $this->load->view('global_footer'); ?>

<?php $this->load->view('partials/js_footer'); ?>

<?php $this->load->view('templates/js/content_updates'); ?>

<?php $this->load->view('templates/js/new_status'); ?>
<?php $this->load->view('templates/js/new_upload_image'); ?>
<?php $this->load->view('partials/interaction_js'); ?>
<?php
if(in_array($user_id,$uploader) || in_array($user_id,$groupMember))
{
  $this->load->view('popups/group_file_popup');
  $this->load->view('popups/group_invite_user');
}
$this->load->view('popups/group_create_popup');
?>

<script type="text/javascript" src="<?php echo site_url(); ?>js/custom_js/files/share_files.js"></script>
<script type="text/javascript">
var limit = 10;
var start = 0;
var Flimit = 10;
var Fstart = 0;
var Fend = false;
var group_end = false;
var nearToBottom = 250;
var concept_id = <?php echo $group['id']; ?>;
var feedtype = 'group';
var feedtype_id = <?php echo $group['id']; ?>;
var group_id = <?php echo $group['id']; ?>;
var uploader = new Array();
uploader = <?php echo json_encode($uploader); ?>;
//alert(uploader);
<?php if(isset($files['value_tags']) && !empty($files['value_tags'])):?>
var tag_id = <?php print $files['value_tags'];?>;
$(document).ready(function(){
$('span[value="<?php print $files['value_tags'];?>"]').parent('label').addClass('active');
});
<?php endif; ?>
var moderator = <?php print (in_array($user_id,$uploader))?1:0; ?>;
$(document).ready(function(){

var progressbox     = $('#progressbox');
var progressbar     = $('#fileprogressbar');
var statustxt       = $('#statustxt');
var completed       = '0%';
	
  <?php //if($changes_filter == true): ?>
  $('#ddConcepts').change(function(){
     var _html = '';
     $.ajax({
        url: siteurl+"manage/fetch_concept_group_tags",
        dataType: 'json',
        type:"post",
        data: {
          "concept_id":$(this).val(),
          "group_id":group_id
        },
        success: function(data){
          if(_.size(data) > 0)
          {
            <?php $group_tags_title = strtolower(str_replace(' ','_',$group['name'])); ?>
            <?php if(isset($audit_constant[$group_tags_title.'_tag_title']) && !empty($audit_constant[$group_tags_title.'_tag_title'])): ?>
            _html+= '<option value="0"><?php print $audit_constant[$group_tags_title.'_tag_title']; ?></option>';    
            <?php endif; ?>
            $.each(data, function(index, item){
              _html+= '<option value="'+item.id+'">'+item.name+'</option>';
            });
            $('#ddTags').html(_html);
          }
        },
      });
	  $(this).blur();
  });
  <?php //endif; ?>
  $('.loader-more').show();
	$('#groups-all-view').fancybox({
	maxHeight	: 500
	});
  
  $('.video_view').fancybox({
	width	: '435',
  height : '350'  
	});
  
  //loading this for members popup
	loadPeople({'noclear': 0, 'group': <?php echo $group['id']; ?>});
  
  //Focus on tags changes default text
  $("#txt_tags").focus(function(){
    var text = $("#txt_tags").val();
    var terms = split(text);
    terms = getUnique(terms);
    $("#txt_tags").val(terms.join(", "));
    if(text == "Start typing.."){
      $("#txt_tags").val("");
    }
  })
  $("#txt_tags").blur(function(){
    var text = $.trim($("#txt_tags").val());
    var terms = split(text);
    terms = getUnique(terms);
    $("#txt_tags").val(terms.join(", ")); 
    if(text == ""){
      $("#txt_tags").val("Start typing..");
    }
  })
  $("#txt_tags").autocomplete({
    source: function(request, response){
      $.ajax({
        url: siteurl+"files/autocomplete_files_tag",
        dataType: 'json',
        type:"post",
        data: {
          "term":extractLast(request.term),
          "group_id":group_id
        },
        success: response
      });
    },
    search: function(){
      // custom minLength
      var term = extractLast(this.value);
      if(term=="Start typing.."){
        return false;
      }
      if (term.length < 2) {
        return false;
      }
    },
    focus: function(event, ui){
      // prevent value inserted on focus
      return false;
    },
    select: function(event, ui){
      var terms = split(this.value);
      // remove the current input
      terms.pop();
      // add the selected item
      var idx = $.inArray(ui.item.value, terms);
      if (idx == -1) {
      terms.push(ui.item.value);
      }
      // add placeholder to get the comma-and-space at the end
      terms.push("");
      this.value = terms.join(", ");
      return false;
    }
  });
  
 	//loading this for data feed
  //loadUpdates('', $('#recent-posts ul.filter li:first'));
  

  $(".autocomplete_user").tokenInput("<?php echo base_url();?>manage/group_member_autocomplete?group_id=<?php echo $group['id']; ?>",{
      theme: "facebook",
      preventDuplicates: true
  });
  
  $(document).ready(function() {
	  $("#invite_members").tokenInput("<?=base_url();?>manage/auto_nongroup_users?group_id=<?php echo $group['id']; ?>",{
	    theme: "facebook",
      preventDuplicates: true
	  });
	});
	$('a.load_members').fancybox({
		maxHeight	: 700,
		width		: 943,
		autoSize	: false,
    afterLoad : function() {
			$('.fancybox-inner').attr('id','newfancy');
			$('#newfancy').on('scroll',function(){
				if ($('#newfancy').scrollTop() + $('#newfancy').height() >= $('#people_popup').offset().top + $('#people_popup').height() ) {
					//ajax
					start += 10;
					loadGroupMember({'noclear': 1, 'group': <?php echo $group['id']; ?>}); 
				}
			});
		}
	});

	$('.editProfile').fancybox({
		maxHeight: 500
	});
	$(".fancybox").fancybox({
		maxHeight	: 500
		
	}); 

	if(typeof poll_results !== 'undefined')
	{
		$.each(poll_results, function(index, item){
			_html	= '<li><label>'+item['title']+'</label><div id="progressBar'+index+'" class="progressBar"><div style="width:'+item['percentage']+'%">'+Math.round(Number(item['percentage']))+'%&nbsp;</div></div></li>';
			$('#vPollResponse').append(_html);
			//progress(item['percentage'], $('#progressBar'+index));
		});
	}
  $('.group_files').fancybox({
		maxHeight: 500,
    afterClose : function(){
      $('.error-msg').html('');
      $("#frm_add_item")[0].reset();
      $(".autocomplete_user").tokenInput('clear');
      $('#group_upload_file .msg').removeClass('success');
      $('#group_upload_file .msg').removeClass('error');
      $('#group_upload_file .msg').html('');
      $('#group_upload_file .msg').hide();
	   progressbox.hide();
    },
	});
  $('.group_create').fancybox({
    maxHeight: 500,
    afterClose  : function() {
      $("#frm_add_group")[0].reset();
      $('.error-msg').html('');
	  $('#group_create .msg').html('');
	  $('#group_create .msg').hide();
	  $('#group_create .msg').removeClass('error');
	  $('#group_create .msg').removeClass('success');
    },
    beforeLoad : function() {
     var groupId = $(this)[0].element.context.attributes[1]['nodeValue'];
     if(!isNaN(groupId))
     {  
        LoadeditGroup(groupId);
        $('#form_popup_title').html('Edit Group');
        $('#bt_add_group').val('Edit Group');
     }
     else{
      $('#render_thumbnail').html('');
      $('#form_popup_title').html('Create a Group');
      $('#bt_add_group').val('Create a Group');
     }
    },
  })
  $('.group_invite').fancybox({
    maxHeight: 500,
    beforeLoad : function() {
      $("#invite_members").tokenInput('clear');
      
    },
    afterClose : function(){
      console.log("Closed");
      $('#group_invite .msg').html('');
	    $('#group_invite .msg').hide();
	    $('#group_invite .msg').removeClass('error');
      $('#group_invite .msg').removeClass('success');
    },
  }); 
  
  $('#bt_add_group').live('click',function(){
    if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
      if($('#frm_add_group #txt_title').val() == "Group Name") {
        $("#frm_add_group #error-txt_title").html("Required");
        $("#frm_add_group #txt_title").focus();
        return false;
      }
    }
      if(!$.trim($('#frm_add_group #txt_title').val())){
        $("#frm_add_group #error-txt_title").html("Required");
        $("#frm_add_group #txt_title").focus();
        return false;
      }
      else
      {
        $("#frm_add_group #error-txt_title").html("");
      }
      if(!$.trim($('#frm_add_group #txt_open').val())){
        $("#frm_add_group #error-txt_open").html("Required");
        $("#frm_add_group #txt_open").focus();
        return false;
      }
      else
      {
        $("#frm_add_group #error-txt_open").html("");
      }
     //   var formData = new FormData($('#frm_add_group')[0]);
    var options = {
    beforeSend: function() {
    			},
    complete: function() {
    			},
    type	: 'POST',
    cache	: false,
    url		: $('#frm_add_group').attr('action'),
    dataType: "json",
    success	: function(data) {
      if(data.status == true)
      {
        $('#frm_add_group')[0].reset();
        $('#group_create .msg').show().addClass('success').html('Group created successfully.');
        $.fancybox.close(); 
        if(data.reload == true)
        {
          window.location.reload();
        }
		else
		{
			window.location.href = siteurl+'groups/'+data.group_id
		}
        return true;
      }
      else
      {
        if(data.duplicate == true)
        {
           $('#group_create .msg').show().addClass('error').html('Group name all ready exist.');
        }
		else{
        $('#group_create .msg').show().addClass('error').html('Something went wrong.');
		}
        return false;
      }
    }, 

    cache: false,
    contentType: false,
    processData: false
    };
    $('#frm_add_group').ajaxSubmit(options);
  });

  $('#send_group_invite').live('click',function(){
      //var formData = new FormData($('#group_invite_form')[0]);
      if($('#invite_members').val() == '')
      {
        $('#group_invite .msg').show().addClass('error').html('Please enter the people\'s name.');
        return false;
      }
      var options = {
				beforeSend: function() {
							},
				complete: function() {
							},
				type	: 'POST',
				url		: $('#group_invite_form').attr('action'),
				dataType: "json",
				success	: function(data) {
				  if(data.status == true)
          {
            $('#group_invite_form')[0].reset();
            $('#group_invite .msg').show().addClass('success').html('Invitation send successfully.');
            $.fancybox.close(); 
            return true;
          }
          else
          {
            $('#group_invite .msg').show().addClass('error').html('Something went wrong.');
            return false;
          }
				},
			};
      $('#group_invite_form').ajaxSubmit(options);
  });
  
 
$('#bt_add_item').live('click',function(e) {
  	e.preventDefault();
		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;
		inactive_form();

		var base_url = $("#base_url").val();
		var cat = $.trim($("#cmb_cat").val());
    var concept = $.trim($("#file_concept").val());
		var file_share = $.trim($("#file_share").val());
		var title = $.trim($("#txt_title11").val());
    var group = $.trim($("#group").val());
    
		if(!cat){
      $("#error-cmb_cat").html("Required");
  		$("#cmb_cat").focus();
  		$('#group_upload_file .msg').removeClass('success');
  		active_form();
  		return false;
		}
    /*else if(!concept)
    {
      $("#error-concept").html("Required");
  		$("#file_concept").focus();
  		$('.msg').removeClass('success');
  		active_form();
  		return false;
    }*/
		else
		{
		  $("#error-cmb_cat").html("");
      $("#error-concept").html("");
		  $('#group_upload_file .msg').removeClass('success');
		}


		if(file_share){
		  $("#error-files_upload").html("");
		  $('#group_upload_file .msg').removeClass('success');
		}
		else if((file_share == "")){

			$("#error-files_upload").html("Required");
			$("#error-files_upload").focus();
			$('#group_upload_file .msg').removeClass('success');
			
			active_form();
			return false;
		}
		else{
		$("#error-files_upload").html("");
		$('#group_upload_file .msg').removeClass('success');
		}
    if(!title){
			$("#error-txt_title11").html("Required");
			$("#txt_title11").focus();
			$('#group_upload_file .msg').removeClass('success');
			
			active_form();
			return false;
		}
		else{
			//var formData = new FormData($('#frm_add_item')[0]);
      var options = {
					beforeSend: function() {
								inactive_form();
								progressbox.show(); //show progressbar
								progressbar.width(completed); //initial value 0% of progressbar
								statustxt.html(completed); //set status text
								statustxt.css('color','#000'); //initial color of status text
								},
					uploadProgress: OnProgress,
					complete: function() {
								active_form();
								Fstart = 0;
                    loadFiles({'noclear': 0});                
								},
					type	: 'POST',
					cache	: false,
					url		: $('#frm_add_item').attr('action'),
					success	: function(data2) {
					obj = JSON.parse(data2);
						if(obj.done === 'yes' && obj.samename === 'no')
						{
							  request_data = {'group_id': group, 'file_id' : obj.file_id};
							  $.ajax({
								url: siteurl+'files/ajax_group_file',
								data: request_data,
								dataType: "json",
								type: "POST",
								success: function(response){
								 if(response.status)
								 {
												$("input[type=text], textarea , select ").val("");
												$("input[type=file]").val("");
												$('#group_upload_file .msg').addClass('success');
												$('#group_upload_file .msg').append('<li/>').html('Your file has been uploaded successfully!');
												$('#group_upload_file .msg').hide();
									$("div#recent-posts .block-2.clearfix").remove();
										$("div#recent-posts .block-1.clearfix").remove();  
									loadUpdates('','');
									setTimeout(function(){$.fancybox.close();}, 500);
								  }
							},
							});
						}
						else if(obj.samename == 'yes')
						{
							$('#group_upload_file .msg').removeClass('success');
							$('#group_upload_file .msg').addClass('error');
							$('#group_upload_file .msg').append('<li/>').html('A file with this name already exists. Please change name to continue.');
							$('#group_upload_file .msg').show();
						}
						else
						{
							$('#group_upload_file .msg').removeClass('success');
							$('#group_upload_file .msg').addClass('error');
							$('#group_upload_file .msg').append('<li/>').html('Please retry, enter data');
							$('#group_upload_file .msg').show();
						}


					},
					cache: false,
					contentType: false,
					processData: false
			};
      $('#frm_add_item').ajaxSubmit(options);
		}
		

		return false;

	});
function OnProgress(event, position, total, percentComplete)
{
	//Progress bar
	progressbar.width(percentComplete + '%') //update progressbar percent complete
	statustxt.html(percentComplete + '%'); //update status text
	if(percentComplete>50)
		{
			statustxt.css('color','#fff'); //change status text to white after 50%
		}
}
function LoadeditGroup(group_id){
  var data = {'group_id' : group_id}; 
  var service_url = siteurl+"manage/load_group_ajax";
  $.ajax({
    url: service_url,
    data: data,
    async: false,
    dataType: "json",
    type: "POST",
    success: function(data){
      if(data.status == true)
      {
        $('#txt_title').val(data.record.name);
        $('#txt_body').val(data.record.description);
        $('#txt_open').val(data.record.is_open);
        $('#group_id').val(data.record.id);
        if(data.record.thumbnail.indexOf('default') == -1)
        {                
          var image_url = siteurl+"images/groups/thumbs/"+data.record.thumbnail;
        }
        else
        {
          var image_url = siteurl+"images/groups/"+data.record.thumbnail;
         
        }                                                                                            
        $('#render_thumbnail').html('<img height="100" width="100" alt="'+data.record.name+'" src="'+image_url+'">');        
      }
    }
  });
}

function loadGroupMember(prop)
{
  $('.loader-more').show();
  var searched1 = ($('#txtSearch').length > 0 && $('#txtSearch').val() != '' && $('#txtSearch').val() != 'Search for files by keyword') ? true : false;
  //console.log(searched1);
  if(!prop.noclear)
  {
    $('.people-grid').html('');
    $('.people-list .row:not(.head)').remove();
  }
  var service_url = siteurl+"people/people_ajax";
  if(searched1)
  {
    var search = $('#txtSearch').val();
    var data = {'limit' : limit, 'start' : start, 'search_term': search};
  } 
  else 
  {
    var concept = prop.concept ? prop.concept : $('#ddConcepts').val();
    var department = $('#ddDepartments').val();
    var country = $('#ddCountry').val();
    var group = prop.group ? prop.group : 0;
    var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country,'group':group};
  }
  console.log(group_end);
  if(group_end == false)
  {
    $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "GET",
      success: function(data){
        console.log(_.size(data));
        if(_.size(data) > 0)
        {
          $('.no-result-block').addClass('d-n');
          
          _.templateSettings.variable = "rc";
          
          // Grab the HTML out of our template tag and pre-compile it.
          var template = _.template(
          $( "#people_card" ).html()
          );
          
          
          $('div.people-grid').append(template(data));
          
          var template = _.template(
          $( "#people_list" ).html()
          );
          
          $('div.people-list').append(template(data));
          
          if($('.l-list-view').hasClass('active')) {
          $('.people-list').css('display', 'table');
          $('.people-grid').css('display', 'none');
          } else {
          $('.people-list').css('display', 'none');
          $('.people-grid').css('display', 'table');
          }
          $('.people-grid-view').removeClass('d-n');
        
        } else {
          group_end = true;
          if(!prop.scroll && start == 0)
          {
          $('.no-result-block').text('No Users found for specified criteria');
          $('.no-result-block').removeClass('d-n');
          $('.people-list .row:not(.head)').remove();
          $('.people-list').css('display', 'none');
          $('.people-grid').css('display', 'none');
          $('.people-grid-view').addClass('d-n');
          }
        }
        $('.loader-more').hide();
      }
    });
  }
}

  
function split(val){
  return val.split(/,\s*/);
}
function extractLast(term){
  return split(term).pop();
}


var end = false;
var nearToBottom = 250;
	
$(document).ready(function() {

		$( "#txtSearch" ).autocomplete({
	      source: '<?php echo site_url(); ?>files/autosearch_ajax?group='+group_id+"&moderator="+moderator,
		  position: { my: "left-11 top", at: "left bottom", collision: "none" },
		  appendTo: "#filter-search .search",
			select: function (a, b) {
				$(this).val(b.item.label);

				//window.location.href = '/files/files_download/' + b.item.value;
				Fstart = 0;
				$('#ddCategories').focus();
				loadFiles({'noclear': 0, 'searched': 1});
				
				//e.preventDefault();
		    	return false;
			}
	    });

	$('.editProfile').fancybox({
		maxHeight: 500,
		afterClose  : function() {
           $("#frm_add_item")[0].reset();
           $('#upload_file-box .msg').removeClass('success');
           $('#upload_file-box .msg').removeClass('error');
           $('#upload_file-box .msg').html('');
		   $('#upload_file-box .msg').fadeIn();
		   $('.error-msg').html('');
		   }
	});
  
  
	$('.loader-more').show();
	loadFiles({'noclear': 0});

	$('#ddfiles, #ddCategories, #ddConcepts,#ddMonth,#ddYear').change(function(){
	  tag_id = 0;
		Fstart = 0;
		$('#txtSearch').val('');
		$('#ddTags').val('0');
		loadFiles({'noclear': 0});
		$(this).blur();
	});
	
	$('#ddTags').change(function(){
	  tag_id = 0;
		Fstart = 0;
		$('#txtSearch').val('');
		//$('#ddTags').val('0');
		loadFiles({'noclear': 0});
		$(this).blur();
	});

	$('.lbl span').click(function(){ 

		Fstart = 0;
		
		$('#txtSearch').val('');
		$('#ddCategories').focus();
		$('#ddCategories').val('0');

		tag_id = $(this).attr('value');
		loadFiles({'noclear': 0});
		//console.log($(this).attr('value'))
	});


	$('#clear').on( "click", function() {
		Fstart = 0;
		$('#ddCategories').val('0');
		$('#ddConcepts').val('0');
		$('#ddYear').val('0');
		$('#ddMonth').val('0');
		$('#ddTags').val('0');
    tag_id = 0;
    $('.lbl').removeClass('active');
		loadFiles({'noclear': 0});

	});

	$('#cancel').on( "click", function() {
		$('.msg').hide();
		$.fancybox.close();

	});
	$('#upload_file_edit').on( "click", function() {
		$('.msg').hide();
		//$.fancybox.close();

	});

	$('#txtSearch').enterKey(function(){
		Fstart = 0;
		$('#ddCategories').focus();
		$('#ddCategories').val('0');
		$('#ddConcepts').val('0');
		loadFiles({'noclear': 0, 'searched': 1});
		
		//e.preventDefault();
    	return false;
	});
	$('#go').on( "click", function() {
		Fstart = 0;
		loadFiles({'noclear': 0, 'searched': 1});
		//e.preventDefault();
    	return false;
	});
	
	$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			if(Fend == false)
			{
			Fstart += 10;
		 	loadFiles({'noclear': 1});
			}
		}
	});
	
	//delete file ajax
	$('.file_delete').live('click',function(){
		var r=confirm("Do you really want to delete the file");
		if (r==true)
		{
			var parent_div = $(this).closest('.fl-contents');
			var file_id = $(this).attr('id');
			var service_url = siteurl+"files/delete_file";
			 $.ajax({
			  url: service_url,
			  data: {'file':file_id},
			  async: false,
			  dataType: "json",
			  type: "POST",
			  success: function(msg){
				if(msg.status == 1){
				  //Upadate if Tabs are All or Status update

					$(parent_div).slideUp('fast');
					//console.log(msg);
					return;
				}
			  },
			});
		}
		
	});
});
function getUnique(a) {
  var b = [a[0]], i, j, tmp;
  for (i = 1; i < a.length; i++) {
    tmp = 1;
    for (j = 0; j < b.length; j++) {
      if (a[i] == b[j]) {
        tmp = 0;
        break;
      }
    }
    if (tmp) {
      b.push(a[i]);
    }
  }
  return b;
}
function active_form()
{
$.fancybox.hideLoading();
$('#bt_add_item').removeClass('inactive');
$('#bt_add_item').removeAttr('disabled');
$('#cancel').removeClass('inactive');
$('#cancel').removeAttr('disabled');
}

function inactive_form()
{
$.fancybox.showLoading();
$('#bt_add_item').addClass('inactive');
$('#bt_add_item').attr('disabled','disabled');
$('#cancel').addClass('inactive');
$('#cancel').attr('disabled','disabled');
}
function url_validation(url) {
  var v = new RegExp();
  v.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");
  if (!v.test(url)) {
    return false;
  }else{
    return true;
  }
}
function remove_tags(html)
  {
       return html.replace(/<(?:.|\n)*?>/gm, '');
  }
function loadFiles(prop)
{
  <?php if(!in_array($myprofile['id'],$groupMember) && !in_array($myprofile['id'],$uploader)):?>
  $('.loader-more').hide();
  $('.container div.no-result-block').show();
  var noResult = 'No result found.';
  $('.container div.no-result-block').html(noResult);
  return false;
  <?php endif; ?>
  
	var searched1 = ($('#txtSearch').val() != '' && $('#txtSearch').val() != 'Search for files by keyword')? true : false;

	if(!prop.noclear)
		$('div.files-links-block').html('');
		$('.container div.no-result-block').html('');
		$('.container div.no-result-block').hide();

	var service_url = siteurl + "files/files_ajax";



	if(searched1)
	{

		var search = remove_tags($('#txtSearch').val());
		var data = {'limit' : Flimit, 'start' : Fstart, 'search_term': search,'group':group_id,'moderator':moderator};
	}  else {
		var files = $('#ddfiles').val();
    var month = $('#ddMonth').val();
    var year = $('#ddYear').val();
		var category = $('#ddCategories').val();
		var concept = $('#ddConcepts').val();
    if(tag_id == 0)
    {
      tag_id = $('#ddTags').val();
    }
    
    
		var data = {'limit' : Flimit, 'start' : Fstart, 'files': files, 'category' : category, 'concept': concept, 'tag': tag_id,'group':group_id,'moderator':moderator,'year':year,'month':month};
	}
	$.ajax({
		beforeSend: function() {
			$('.loader-more').show();
		},
		complete: function() {
			$('.loader-more').hide();
		},
		url: service_url,
		data: data,
		async: false,
		dataType: "json",
		type: "GET",
		success: function(data){
			var counter = 2;
			if(data.not_found != true)
			{

				$.each(data, function() {
					if(this[0]){

						var html='<dl class="files-links clearfix">';
						html +='<dt>'+this[0].date+'</dt><dd>';
			        	$.each(this, function() {
							len = '';
							if(this.description.length > 1)
							{
								len='...';
							}
							html +='<div class="fl-contents clearfix">';
							//html +='<blockquote class="icon '+this.icon+'">'+this.icon+'</blockquote>';
							html +='<span class="ph"><a '+this.target+' href="'+this.download_path+'"><img src="'+this.file_image_thumb+'" width="97" alt="'+this.title+'"/></a></span>';
							html +='<div class="f1-c2"><h2><a '+this.target+' href="'+this.download_path+'">'+this.title+'</a><!-- <label class="lbl"><span>Label</span></label> --></h2>';
							if(this.user_status == 'active')
							{
								html +='<p class="dgrey">'+this.description.substring(0,150)+len+' by <strong><a href="'+this.reply_username +'">'+this.display_name+'</a></strong></p>';	
							} else {
								html +='<p class="dgrey">'+this.description.substring(0,150)+len+' by <strong>'+this.display_name.replace(/\\/g, '')+'</strong></p>';
							}
							//var extArr =  [ "video/3gp", "video/avi", "video/flv", "video/m4v", "video/mov", "video/mp4", "video/mpg", "video/vob", "video/wmv", "video/mpeg", "video/mpe" ];
              var extArr =  [ "video/mp4", "video/mpg", "video/mpeg"];
              html +='<div class="rp-social">';
							html +='<small><a class="pDelete" '+this.target+' href="'+this.download_path+'">Download</a></small>&nbsp;';
							
				if(myprofile.role_id == 1 || jQuery.inArray(myprofile.id, uploader ) != -1){
					html +='&nbsp;<small><a class="pDelete file_delete" id="'+this.file_id+'" href="javascript:void(0)">Delete</a></small>&nbsp;';
				}			
							if(this.view_path!=''){
                if(this.view_path.indexOf('.jpg')!= -1){
                html +='&nbsp;<small><a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a></small>&nbsp;';  
                }
                else if(this.view_path.indexOf('.png')!= -1){
                html +='&nbsp;<small><a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a></small>&nbsp;';  
                }
                else if(this.view_path.indexOf('.jpeg')!= -1){
                html +='&nbsp;<small><a class="pDelete fancybox" href="'+this.view_path+'">View</a></small>&nbsp;';  
                }
                else if(this.view_path.indexOf('.gif')!= -1){
                html +='&nbsp;<small><a class="pDelete fancybox" href="'+this.view_path+'">View</a></small>&nbsp;';  
                }
                else if(this.view_path.indexOf('.png')!= -1){
                html +='&nbsp;<small><a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a></small>&nbsp;';  
                }
                else if(this.view_path.indexOf('.bmp')!= -1){
                html +='&nbsp;<small><a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a></small>&nbsp;';  
                }
                else if(jQuery.inArray( this.file_mime_type, extArr ) != -1)
                {

                  html +='&nbsp;<small><a class="pDelete fancybox video_view" data-fancybox-type="iframe" href="'+siteurl+'lib/video.php?url='+this.view_path+'">Watch</a></small>&nbsp;';
                }
                else{
                  html += '&nbsp;<small><a class="pDelete fancybox doc_view" target="_blank" href="https://docs.google.com/viewer?url=' + this.view_path + '&embedded=true">View</a></small>&nbsp;';
                }
                html +='&nbsp;<small><a class="pDelete fancybox report_file" id="'+this.file_id+'" href="#pop-win-file"><small>Report</small></a></small>';
              }
							html +='</div></div></div>';
			    		});
						html +='</dd></dl>';
						$('div.files-links-block').append(html);
			    		counter++;
					}
				});
			}
			else
			{
				if($('div.files-links-block').html() == '')
				{
					$('.container div.no-result-block').show();
					var noResult = 'No result found for <b>"'+data.search_term+'"</b>.';
					$('.container div.no-result-block').append(noResult);
				}
			}

			if(data.no_result == true && Fstart == 0)
			{
				Fend = true;
				$('.loader-more').hide();
				$('.container div.no-result-block').show();
				var noResult = 'No results found.';
				$('.container div.no-result-block').html(noResult);
			}
		}
	});
  $('.image_view').fancybox({
		maxHeight: 500
  });
  $('.doc_view').fancybox({
		'width'	: '75%',
		'height' : '95%',
    'autoScale' : false,
    'transitionIn' : 'none',
		'transitionOut'	: 'none',
		'type' : 'iframe'
	});
  $('.report_file').fancybox({
  beforeLoad : function() {
     //console.log(this.element[0].id);
     $('#txtfileid').val(this.element[0].id);
     $('#txtarcomments-file').val('');
     $(".error-msg").html('');
    }
  });
}
});


</script>

</body>
</html>