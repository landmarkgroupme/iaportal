<?php

class Groups extends General {

  function __construct() {
    parent::__construct();
    $this->load->model('Group');
    $this->load->model('ObjectMember'); 
    $this->load->model('FileCategory');  
  	$this->load->model('Prompt');	
    $this->load->model('Invite');
    $this->config->load('audit');
    $this->load->model('Survey');
    $this->load->model('Outlets');
	$this->load->model('Role');
	$this->load->model('Concept');
	$this->load->model('File');
  $this->load->helper('usort_compare');
    
  }

//Default Offers page
function index() {
	$data = array();
	$my_user = $this->session->userdata("logged_user");
	$groups = $this->Group->getGroups(null, 0,  array('total_members' => 'DESC'), $my_user['userid']);
  //echo "<pre>";print_r($groups);exit;
  $data['audit_constant'] = $this->config->item('audit');
	$groups = (array)$groups;
	$end_time = microtime();
	$data['user_id']=$my_user['userid'];	
	$permission_roles = array();
	foreach($this->Role->getRoleKeys() as $role)
	{
		$permission_roles[$role['key']] = $role['id'];
	}
   $top_groups = array_slice($groups, 0, 5);
   $other_groups = array_slice($groups, 5);
   $others = array();
   foreach($top_groups as $index => $group)
	{
		$members = array();
		$members = $this->ObjectMember->getMembers('Group', $group['id']);
		$top_groups[$index]['members'] = $members;
		if($group['is_open'] == 'no')
		{
		  
		  foreach($members as $member)
		  {
			$top_groups[$index]['valid_members'][] = $member['user_id'];
		  }
		  $top_groups[$index]['invited_member'] = $this->Invite->fetchAllInvitation($group['id']);
		} 
	}
				
	foreach($other_groups as $index => $group)
	{	//$other_groups[$index]['members'] = $this->ObjectMember->getMembers('Group', $group['id'], 8);
		if($group['is_open'] == 'no')
		{
		  foreach($members as $member)
		  {
			$other_groups[$index]['valid_members'][] = $member['user_id'];
		  }
		  $other_groups[$index]['invited_member'] = $this->Invite->fetchAllInvitation($group['id']);
		} 
	
		if($group['is_open'] == 'yes' || (isset($group['valid_members']) && !empty($group['valid_members']) && in_array($user_id,$group['valid_members'])) || ($my_user['permissions']['id'] == $permission_roles['SA']) || (isset($group['invited_member']) && !empty($group['invited_member']) && in_array($my_user['id'],$group['invited_member']))):
		$count = 0;
		if (isset($others[strtoupper(substr($group['name'], 0, 1))])) {
			$count = count($others[strtoupper(substr($group['name'], 0, 1))]);
		}
		
		 if ($count < 3) {
			 $others[strtoupper(substr($group['name'], 0, 1))][] = $group;
		  } 
		  
			//echo "<pre>"; print ;
		endif;
	}

    ksort($others);
	
	$data['others'] = $others;
	$data['top_groups'] = $top_groups;
	$filters = array(
	'feature' => 'Group',
	'user_id' => $my_user['userid']
	);
	$prompt_data =  $this->Prompt->getByFilters($filters);
	
	if(isset($prompt_data) &&  $prompt_data!= '')
	{
		$data['user_prompt'] = 0;
	}
	else
	{
		$data['user_prompt'] = 1;
	}
  
  	//$data['groups'] = $groups;
	//echo "<pre>";print_r($data);exit;
    $this->load->view("groups", $data);
  }
  
  public function view($slug) {
   // $user_id = $this->session->userdata('userid');
    #echo "<pre>".print_r($user_id,"\n")."</pre>";
  //  $my_user = $this->session->userdata("logged_user");

    $this->load->helper('cookie');

    $cookieData = get_cookie("intranet_ck");
    //echo "<pre>";
    //print_r($cookieData);
    //echo "<br>";
    $session_array = unserialize( $cookieData );

    $my_user['userid'] = $session_array['userid'];
    $user_id = $session_array['userid'];

    $group = $this->Group->getGroup($slug , $my_user['userid']);
    
		if(empty($group))
		{
		  redirect('locator/notfound');
		}
		else
		{
		  
      /** Fetching detail of ids for audit groups **/
      $data['audit_constant'] = $this->config->item('audit');
      /** Fetching detail of ids for audit groups **/
      
      $this->load->model('FileTag');
      
      $group['invited_member'] = $this->Invite->fetchAllInvitation($group['id']);
      $group['members'] = $this->ObjectMember->getMembers('Group', $group['id'], 12);
      $group['managers']=$this->Group->getGroupManagers(intval($group['id']));
      $group['managers_count']=$this->Group->getCountGroupManagers(intval($group['id']));
			$sortby = array(
			  'total_members' => 'DESC',
			);
      
      $concepts_manager = $this->Survey->getSurveyConcept();
      if(isset($concepts_manager) && !empty($concepts_manager))
      {
        foreach($concepts_manager as $concept)
        {
          $data['concept_manager'][] = $concept->user_id;
          $data['concept_manager_id'][$concept->user_id] = $concept->concept_id;
        }
      }
      $outlets = $this->Outlets->getOutletbyFilter(array());
      if(isset($outlets) && !empty($outlets))
      {
        foreach($outlets as $outlet)
        {
          $data['outlet_manager'][] = $outlet->user_id;
          $data['concept_manager_id'][$outlet->user_id] = $outlet->concept_id;
        }
      }
      
      /** Remove other group if survey group page is viewed **/
      $data['changes_filter'] = false;
      if($group['id'] == $data['audit_constant']['outlet_audit_reports_group_id'] || $group['id'] == $data['audit_constant']['process_audit_reports_group_id'] )
      {
        $data['changes_filter'] = true;  
      }
      $data['url_suffix'] = '';
      if($group['id'] == $data['audit_constant']['internal_audit_plan_group_id'] || $group['id'] == $data['audit_constant']['knowledge_center_group_id'] || $group['id'] == $data['audit_constant']['user_guide_group_id'] || $group['id'] == $data['audit_constant']['outlet_audit_reports_group_id'] || $group['id'] == $data['audit_constant']['process_audit_reports_group_id'])
      {
        $data['url_suffix'] = '/files';
        $data['groups'] = $this->Group->getGroups(null, $offset = null,$sortby);
        $SurveyGroups = array();
        foreach($data['audit_constant'] as $SurveyGroup)
        {
          if(is_numeric($SurveyGroup))
          {
            if(isset($data['outlet_manager']) && !empty($data['outlet_manager']))
            {
              if(in_array($user_id,$data['outlet_manager']))
              {
                if($SurveyGroup == $data['audit_constant']['knowledge_center_group_id'] || $SurveyGroup == $data['audit_constant']['user_guide_group_id'])
                {
                  $SurveyGroups[] = $SurveyGroup;
                }
              }
              else
              {
                $SurveyGroups[] = $SurveyGroup;
              }
            }
            else
            {
              $SurveyGroups[] = $SurveyGroup;
            }
          }
        }
        
        foreach($data['groups'] as $i=>$Sgroup)
        {
          if(!in_array($Sgroup['id'],$SurveyGroups))
          {
            unset($data['groups'][$i]);
          }
        }
      }
      else
      {
       $data['groups'] = $this->Group->getGroups(10, $offset = null,$sortby); 
      }
			
      
      
			foreach($data['groups'] as $j=>$gp)
			{
        if($gp['id'] == $group['id'])
			  {
				 unset($data['groups'][$j]);
			  }
			  
			}
      
      $uploader = array();
      $groupMembers = array();
      $AllMembers = $this->ObjectMember->getMembers('Group', $group['id'], null, null);
      foreach($AllMembers as $member)
      {
        $groupMembers[] = $member['user_id'];
      }
      $data['groupMember'] = $groupMembers;
      foreach($group['managers'] as $mem)
      {
        $uploader[] = $mem['id'];
      }
      if(!in_array($group['created_by'],$uploader))
      {
        $uploader[] = $group['created_by'];
      }
      $data['uploader'] = $uploader;
      $data['user_id']=$my_user['userid'];
			$data['group'] = $group;
      
      $this->load->model('Concept');
      $data['concepts'] = $this->Concept->getAll();
      
      $data['file_categories'] = $this->FileCategory->getFileCategories(null, 'active');
      #echo "<pre>".print_r($data,"\n")."</pre>";
      $haveFiles = $this->uri->segment(3,0); 	
      $haveTags = $this->uri->segment(4,0);
      $tagid = $this->uri->segment(5,0);
      
      
      if(isset($haveFiles) && !empty($haveFiles) && $haveFiles == 'files')
      {
        $data['files']['concepts'] = $this->Concept->getAll();
        $data['files']['concepts_feed'] = $this->Concept->getConceptByGroup($group['id']);
        $filters = array();
		 $filters['group_id'] = $group['id'];
		 $filters['group_file'] = 1;
		 $filelisting = $this->File->getFiles(null,null,$filters);
		 $retail_concept= array();
		 foreach($filelisting as $list)
		 {
			$retail_concept[] = $list['concept_id'];
		 }
		$data['files']['retail_concept'] = $retail_concept;
        $this->load->model('FileCategory');
        $data['files']['file_categories'] = $this->FileCategory->getFileCategories(null, 'active');
        $data['files']['file_categories_filter'] = $this->FileCategory->getFileCategoriesByGroup(null, 'active',$group['id']);
        if(in_array($user_id,$uploader))
        {
          $user_id = null;
        }
        $managerConcept = 0;
        
        if(isset($data['concept_manager']) && !empty($data['concept_manager']) || isset($data['outlet_manager']) && !empty($data['outlet_manager']))
        {
          if((isset($data['concept_manager']) && in_array($user_id,$data['concept_manager'])) ||( isset($data['outlet_manager']) && in_array($user_id,$data['outlet_manager'])))
          {
            $managerConcept =  $data['concept_manager_id'][$user_id];
          }
        }
        $data['files']['filetags_filter'] = $this->FileTag->getPopularTags('',$group['id'],$user_id,$managerConcept);
        $data['files']['filetags'] = $this->FileTag->getPopularTags(10,$group['id'],$user_id,$managerConcept);
        
        $data['files']['value_title'] = "";
        $data['files']['value_desc'] = "";
        $data['files']['value_category'] = "";
        $data['files']['value_link'] = "";
        $data['files']['value_tags'] = "";
        if(isset($haveTags) && !empty($haveTags))
        {
          if(isset($tagid) && !empty($tagid))
          {
            $data['files']['value_tags'] = $tagid;
          }
        }
        $this->load->view('single_group_files', $data);
      }
      else
      {
			 $this->load->view('single_group', $data);
      }
		}
	}
  function ajax_load_all_group(){
    $alpha = $this->input->post('alpha');
    $exclude = $this->input->post('exclude');
    $user_id = $this->session->userdata('userid');
    $filter = array(
      'alpha' => $alpha,
      'exclude' => explode(',',$exclude),
      'sort' => array('total_members' => 'DESC'),
    );
    $FGroup = $this->Group->getGroupByFilter($filter);
	//echo "<pre>";print_r($FGroup);exit;
    foreach($FGroup as $key => $group)
    {
      $members = $this->ObjectMember->getMembers('Group', $group['id']);
      $validUser = array();
      foreach($members as $member)
      {
        $validUser[] = $member['user_id'];
      }
      if(in_array($user_id,$validUser))
      {
        $FGroup[$key]['is_member'] = 1;
      }
      else
      {
        $FGroup[$key]['is_member'] = 0;
      }
      if($group['is_open'] == 'no')
      {
        $invitees = $this->Invite->fetchAllInvitation($group['id']);
		
        if(!in_array($user_id,$invitees))
        {
          if($group['created_by'] != $user_id) unset($FGroup[$key]);
        }
      }
    }
     if(isset($FGroup) && !empty($FGroup))
    {
      $result = array('status'=>true,'records'=>$FGroup);
    }
    else
    {
      $result = array('status'=>false);
    }
	//echo"<pre>";print_r($result);exit;
    $data['json'] = json_encode($result);
    $this->load->view("print_json", $data);
  }
}

/* End of file concepts.php */
/* Location: ./system/application/modules/concepts/controllers/concepts.php */