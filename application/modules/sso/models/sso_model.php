<?php

Class User extends Model {

  function User() {
    parent::Model();
  }

  function authenticated($email, $pass) {
    $userdata = array(
        'email' => $email,
        'password' => md5($pass)
    );
    $query = $this->db->get_where('ci_users', $userdata);
    return $query->num_rows();
  }

  function id($email) {
    $userdata = array(
        'email' => $email
    );
    $query = $this->db->get_where('ci_users', $userdata);
    if ($query->num_rows()) {
      $row = $query->row();
      return $row->id;
    } else {
      return 'Not Available';
    }
  }

  function usertype($email) {
    $userdata = array(
        'email' => $email
    );
    $query = $this->db->get_where('ci_users', $userdata);
    if ($query->num_rows()) {
      $row = $query->row();
      return $row->user_type;
    } else {
      return 'Not Available';
    }
  }

// Added as a part of the SSO
  function userhrms($email) {
    $userdata = array(
        'email' => $email
    );
    $query = $this->db->get_where('ci_users', $userdata);
    if ($query->num_rows()) {
      $row = $query->row();
      return $row->hrms_id;
    } else {
      return 'Not available';
    }
  }
// End addition

  function fullname($email) {
    $userdata = array(
        'email' => $email
    );
    $query = $this->db->get_where('ci_users', $userdata);
    if ($query->num_rows()) {
      $row = $query->row();
      return $row->first_name . " " . $row->last_name;
    } else {
      return 'Not Available';
    }
  }

  function location($email) {
    $userdata = array(
        'email' => $email
    );
    $query = $this->db->get_where('ci_users', $userdata);
    if ($query->num_rows()) {
      $row = $query->row();
      return $row->location_id;
    } else {
      return 'Not Available';
    }
  }
  function update_last_login_time($userdata,$userid){
    $this->db->update('ci_users', $userdata, "id = $userid");
    return $this->db->affected_rows();
  }
  
  function last_login_time($email) {
    $userdata = array(
        'email' => $email
    );
    $query = $this->db->get_where('ci_users', $userdata);
    if ($query->num_rows()) {
      $row = $query->row();
      return $row->last_login_time;
    } else {
      return 'Not Available';
    }
  }

  function reset_password($data, $user_id) {
    $this->db->where('id', $user_id);
    $this->db->update('ci_users', $data);
    return $this->db->affected_rows();
  }

  function permissions($user_id) {
    $sql = "SELECT
              announcements,
              news,
              offers,
              photos,
              events,
              users,
              review,
              moderate
            FROM
              ci_permissions
            WHERE user_id = ?";
    $result = $this->db->query($sql, array($user_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function check_commetchat_entry($user_id){
    $userdata = array(
        'userid' => $user_id
    );
    $query = $this->db->get_where('cometchat_status', $userdata);
    return $query->num_rows();
  }

  //Add user to comet chat
  function addto_cometchat($data){
    $this->db->insert('cometchat_status', $data);
  }
  function login_tracker($tracker_data){
    $this->db->insert('ci_login_tracker', $tracker_data);
  }
}
