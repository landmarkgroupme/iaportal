  <?php

class Surveys extends General {

  function __construct() {
    parent::__construct();

    $this->load->model('Concept');
   // $this->load->model('Announcement');
    $this->load->model('GroupConceptPermission');
    $this->load->model('ObjectMember'); //generic table holding members for both concepts and groups
    $this->load->model('manage/manage_model');
    $this->load->model('includes/general_model'); //General Module
    $this->load->model('backend/moderate_model');
    $this->load->model('SurveyQuestionCategory');
    $this->load->model('SurveyQuestion');
    $this->load->model('Survey');
    $this->load->model('SurveyUser');
    $this->load->model('SurveyAnswer');
    $this->load->model('Outlets');
    $this->load->model('ConceptManager');
    $this->load->model('Territory');
    $this->load->model('ManagementActionPlan');
    $this->config->load('audit');
    $this->load->model('Group');
  }

  //Default Offers page
  function mysurveys() {
    $data = array();
    $my_user = $this->session->userdata("logged_user");
    $data['audit_constant'] = $this->config->item('audit');
    $outlets = $this->Outlets->getOutletbyFilter(array());
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
      }
    }
    $data['concepts'] = $this->Survey->getSurveyConceptNames();
    $data['surveys'] = $this->Survey->getAll(null,array('create_dttm'=>'DESC'));
    $data['user_surveys'] = $this->Survey->getSurveyByUser($my_user['userid']);
    
    foreach($data['user_surveys'] as $key => $survey)
    {
      $data['user_surveys'][$key]->questions_count = $this->SurveyQuestion->getSurveyQuestionsCount($survey->id);
      $data['user_surveys'][$key]->answers_count = $this->SurveyAnswer->getSurveyAnswersCount($survey->id,$my_user['userid']);
      $data['user_surveys'][$key]->user_status = $this->SurveyUser->getSurveyByUser($survey->id,$my_user['userid']);
    }
    #echo "<pre>".print_R($data['user_surveys'],"\n")."</pre>";
    $this->load->view("surveys_list", $data);
  }

  function editbk($id) {
    $data = array();
    $data['survey_id'] = $id; 
    require_once('formcraft/function.php');
    $this->load->view('survey_fill', $data);
  }

  function create() {
    $data = array();
    $data['audit_constant'] = $this->config->item('audit');
    $data['concepts'] = $this->Survey->getSurveyConceptNames();

    $this->load->model('SurveyQuestionCategory');

    $data['question_categories'] = $this->SurveyQuestionCategory->getAll(null, array('title' => 'ASC'));

    //$data['survey_id'] = $id; 
    $this->load->view('survey_create', $data);
  }

  function edit() {

    $data = array();
    $id = (int)$this->uri->segment(3);

    $data['survey'] = $this->Survey->get($id);
    $data['survey'] = $data['survey'][0];
    $data['audit_constant'] = $this->config->item('audit');
    $data['users'] = $this->SurveyUser->getUserSurveys($id);

    //print_r($data['survey']);
    
    $data['concepts'] = $this->Survey->getSurveyConceptNames();

    $data['question_categories'] = $this->SurveyQuestionCategory->getAll(null, array('title' => 'ASC'));

    //$data['survey_id'] = $id; 
    
    /**/
    $this->load->view('survey_create', $data);
  }

  function preview() {
    $this->submit();
  }
  function delete($id) {
    $this->Survey->removeSurvey($id);
    header('location:/surveys/mysurveys');
  }
  function submit() {
    
    $data = array();
    $id = (int)$this->uri->segment(3);
    $my_user = $this->session->userdata("logged_user");
    $data['audit_constant'] = $this->config->item('audit');
    $data['survey'] = $this->Survey->get($id);
    $data['survey'] = $data['survey'][0];
    $answers = $this->SurveyAnswer->getSurveyDraftAnswers($data['survey']->id,$my_user['userid']);
    foreach($answers as $answer)
    {
     $data['draftanswer'][$answer->question_id] = $answer;
    }
    $questions = $this->SurveyQuestion->getSurveyQuestions($id);

    $survey_questions = array();

    foreach($questions as $question)
    {
      $survey_questions[$question->subcategory_id]['category'] = $question->category_name;
      $survey_questions[$question->subcategory_id]['subcategory_name'] = $question->subcategory_name;
      $survey_questions[$question->subcategory_id]['questions'][] = $question;
    }

    $data['category'] = $survey_questions;
    $this->load->view('survey_fill_new', $data);
  }

  function submit_survey()
  {
    $my_user = $this->session->userdata("logged_user");
    
    $survey_id= $this->input->post('survey_id');
    $answers  = $this->input->post('question');
    $data['audit_constant'] = $this->config->item('audit');
    $action_plans = $this->input->post('action_plan');
    $action_plan_dates = $this->input->post('action_plan_date');
    $responsibility = $this->input->post('responsibility');
    $NAReason = $this->input->post('na_reason');
    $surveyget_id  = $this->input->get('survey_id');
    $post = $this->input->post();
    
    if(isset($answers) && !empty($answers) && (!isset($surveyget_id) || empty($surveyget_id)))
    {
      foreach($answers as $question_id => $answer)
      {
        $data = array();
        $data['user_id'] = $my_user['userid'];
        $data['question_id'] = $question_id;
        $data['answer'] = $answer;
        
        if(isset($action_plans) && !empty($action_plans))
        {
          $data['actionplan'] = in_array($question_id, array_keys($action_plans)) ? $action_plans[$question_id] : '';
        }
        else{
          $data['actionplan'] = '';
        }
        if(isset($action_plan_dates) && !empty($action_plan_dates))
        {
          $data['actionplan_dttm'] = in_array($question_id, array_keys($action_plan_dates)) ? $action_plan_dates[$question_id] : '';
        }
        else
        {
          $data['actionplan_dttm'] = '';
        }
        if(isset($responsibility) && !empty($responsibility))
        {
          $data['responsibility'] = in_array($question_id, array_keys($responsibility)) ? $responsibility[$question_id] : '';
        }
        else
        {
          $data['responsibility'] = '';
        }
        if(isset($NAReason) && !empty($NAReason))
        {
          $data['na_reason'] = in_array($question_id, array_keys($NAReason)) ? $NAReason[$question_id] : '';
        }
        else
        {
          $data['na_reason'] = '';
        }
        if($answer == 'No')
        {
          $data['answered_nodate'] = date("Y-m-d H:i:s",time());
          $data['answered_yesdate'] = '';
        }
        else if($answer == 'Yes')
        {
          $data['answered_nodate'] = date("Y-m-d H:i:s",time());
          $data['answered_yesdate'] = date("Y-m-d H:i:s",time());
        }
        else
        {
          $data['answered_nodate'] = '';
          $data['answered_yesdate'] = '';
        }
        $data['status'] = 1;
        $this->SurveyAnswer->saveSurveyAnswer($data);
        $MAPanswers = $this->ManagementActionPlan->getManagementActionPlanAnswers(array('question_id'=>$question_id,'user_id'=>$my_user['userid']));
        if(count($MAPanswers) == 0)
        {
          $this->ManagementActionPlan->createManagementActionPlanAnswers($data);
        }
      }
      $this->SurveyUser->changeStatus('completed',$survey_id,$my_user['userid']);
    }
    
    
    if(isset($surveyget_id) && !empty($surveyget_id))
    {
      $actionplan = $this->input->post('action_plan');
      $actionplan_dttm = $this->input->post('action_plan_date');
      $responsibility = $this->input->post('responsibility');
      $yes = $this->input->post('yes');
      $answers = $this->input->post('answers');
      if(isset($answers) && !empty($answers))
      {
        foreach($answers as $id)
        {
          $data['id'] = $id;
          $data['actionplan'] = $actionplan[$id];
          $data['actionplan_dttm'] = $actionplan_dttm[$id];
          $data['responsibility'] = $responsibility[$id];
          if(isset($yes) && !empty($yes))
          {
            if(in_array($id, array_keys($yes)))
            {
              $data['yes'] = $id;
            }
            else
            {
              $data['no'] = $id;
            }
          }
          else
          {
            $data['yes'] = '';
            $data['no'] = $id;
          }
          if(isset($data['yes']) && !empty($data['yes']))
          {
            $data['answered_yesdate'] = date("Y-m-d H:i:s",time());
          }
          else
          {
            $data['answered_yesdate'] = '';
          }
          $this->SurveyAnswer->saveSurveyActionPlan($data);
          if(isset($data['yes']) && !empty($data['yes']))
          {
            $this->SurveyAnswer->updateSurveyAnswer($data['yes'],array('answer'=>'Yes'));
          }
          if(isset($data['no']) && !empty($data['no']))
          {
            $this->SurveyAnswer->updateSurveyAnswer($data['no'],array('answer'=>'No'));
          }
        }            
      }
    }
    $data['survey_id'] = (isset($survey_id) && !empty($survey_id))?$survey_id:$surveyget_id;
    if(!isset($surveyget_id) || empty($surveyget_id))
    {
      $surveyget_id = $survey_id;
    }
    $results = $this->SurveyAnswer->surveyResult($surveyget_id,$my_user['userid']);
    $data['questions'] = $this->SurveyAnswer->getSurveyAnswers($surveyget_id,$my_user['userid']);
    
    $MAPData = array();
    if(isset($results) && !empty($results))
    {
      foreach($results as $record)
      {
        $MAPfilter = array();
        $MAPfilter['survey_id'] = $surveyget_id;
        $MAPfilter['user_id'] = $my_user['userid'];
        $MAPfilter['category_id'] = $record->category_id;
        $Maprecord = $this->ManagementActionPlan->getManagementActionPlan($MAPfilter);  
        if(count($Maprecord) == 0)
        {
          $MAPData['category_id'] = $record->category_id;
          $MAPData['total_controls'] = $record->total_controls;
          $MAPData['total_no'] = $record->total_no;
          $MAPData['total_yes'] = $record->total_yes;
          $MAPData['survey_id'] = $record->survey_id;
          $MAPData['user_id'] = $record->user_id;
          $MAPData['answered_date'] = time();
          $this->ManagementActionPlan->createManagementActionPlan($MAPData);
        }
      }
    }
    $MAPfilter = array();
    $MAPfilter['survey_id'] = $surveyget_id;
    $MAPfilter['user_id'] = $my_user['userid'];
    $MAPresults = $this->ManagementActionPlan->getManagementActionPlan($MAPfilter);
    
    $data['results'] = $MAPresults;
    $this->load->view('survey_result', $data);

  }
  
  function submit_survey_ajax() {
    #echo "<pre>".print_r($_POST,"\n")."</pre>";exit;
    $my_user = $this->session->userdata("logged_user");
    
    $survey_id = $this->input->post('survey_id');
    $answers  = $this->input->post('question');
    $action_plans = $this->input->post('action_plan');
    $action_plan_dates = $this->input->post('action_plan_date');
    $responsibility = $this->input->post('responsibility');
    $NAReason = $this->input->post('na_reason');
    #echo "<pre>".print_r($_POST,"\n")."</pre>";exit;
    if(isset($answers) && !empty($answers))
    {
      foreach($answers as $question_id => $answer)
      {
        $data = array();
        $data['user_id'] = $my_user['userid'];
        $data['question_id'] = $question_id;
        $data['answer'] = $answer;
        
        if(isset($action_plans) && !empty($action_plans))
        {
          $data['actionplan'] = in_array($question_id, array_keys($action_plans)) ? $action_plans[$question_id] : '';
        }
        else{
          $data['actionplan'] = '';
        }
        if(isset($action_plan_dates) && !empty($action_plan_dates))
        {
          $data['actionplan_dttm'] = in_array($question_id, array_keys($action_plan_dates)) ? $action_plan_dates[$question_id] : '';
        }
        else
        {
          $data['actionplan_dttm'] = '';
        }
        if(isset($responsibility) && !empty($responsibility))
        {
          $data['responsibility'] = in_array($question_id, array_keys($responsibility)) ? $responsibility[$question_id] : '';
        }
        else
        {
          $data['responsibility'] = '';
        }
        if(isset($NAReason) && !empty($NAReason))
        {
          $data['na_reason'] = in_array($question_id, array_keys($NAReason)) ? $NAReason[$question_id] : '';
        }
        else
        {
          $data['na_reason'] = '';
        }
        if($answer == 'No')
        {
          $data['answered_nodate'] = date("Y-m-d H:i:s",time());
          $data['answered_yesdate'] = '';
        }
        else if($answer == 'Yes')
        {
          $data['answered_nodate'] = date("Y-m-d H:i:s",time());
          $data['answered_yesdate'] = date("Y-m-d H:i:s",time());
        }
        else
        {
          $data['answered_nodate'] = '';
          $data['answered_yesdate'] = '';
          
        }
        $data['status'] = 0;
        $this->SurveyAnswer->saveSurveyAnswer($data);
      }
    }
    $this->SurveyUser->changeStatus('inprogress',$survey_id,$my_user['userid']);
    echo json_encode(array('status'=> 'success'));
    exit();
}    



  function get_categories() {

    $this->load->model('SurveyQuestionCategory');
    $data = array();
    $parent_id = $this->input->get('parent_id');
    $question_categories = $this->SurveyQuestionCategory->getCategories($parent_id);
    $data['json'] = json_encode(array('question_categories' => $question_categories));
    $this->load->view("print_json", $data);
  }

  function add_question_category()
  {
    $details = array();
    $details['parent_id'] = $this->input->post('parent_id');
    $details['title'] = mysql_real_escape_string($this->input->post('title'));
    $details['status'] = 'active';
    $id = $this->SurveyQuestionCategory->add($details);
    echo json_encode(array('status'=> 'success', 'id' => $id));
    exit();
  }

  function remove_question_category()
  {
    $id = $this->input->post('id');
    $this->SurveyQuestionCategory->remove($id);
    //echo $this->db->last_query();
    echo json_encode(array('status'=> 'success'));
    exit();
  }


  

  function save_survey()
  {
    #echo "<pre>".print_r($this->input->post(),"\n")."</pre>";exit;
    $id = $this->input->post('survey_id');

    $details = array();
    if($id == '')
    {
      //Create Survey
      $details['name'] = mysql_real_escape_string($this->input->post('title'));
      $details['concept_id'] = $this->input->post('concept');
      $details['status'] = 'active';
      $survey_id = $this->Survey->add($details);
      echo json_encode(array('status'=> 'success', 'id' => $survey_id));
      exit();

    } else {
      //Save Survey 
      $details['name'] = mysql_real_escape_string($this->input->post('title'));
      $survey_users = $this->input->post('survey_users');
      $survey_users = explode(',', $survey_users);
      // $this->SurveyUser->removeSurveyUsers($id);
      foreach($survey_users as $user)
      {
        $total = $this->SurveyUser->checksurveyforuser($id,$user);
        if($total == 0) {
            $this->SurveyUser->add(array('survey_id' => $id, 'user_id' => $user));
        }
        
      }
      $previous_survey_users = $this->SurveyUser->getAllUsersOfSurvey($id);

      foreach($previous_survey_users as $key =>$value){ 
        $preuserid =  $previous_survey_users[$key]->user_id;
        if(!in_array($preuserid, $survey_users)) {

          $this->SurveyUser->removeSurveyUser($id,$preuserid);

        }
       
      }

      $date = $this->input->post('created_date');
      $details['create_dttm'] = date("Y-m-d H:i:s",strtotime($date));
      $this->Survey->update($details, $id);

      $category = $this->input->post('subcategory');

      $existing = $this->input->post('existing');
      if(!empty($existing))
      {
        $existing = array_keys($existing);
      } else {
        $existing = array();
      }
      

      $questions = $this->input->post('question');
      
      $questions_tips = $this->input->post('question_tips');
      
      foreach($questions as $qid => $question)
      {
        #echo $question.' -- '.$questions_tips[$qid].'---'.$qid;

        $question = trim(mysql_real_escape_string($question));
        if(in_array($qid, $existing))
        {
          //echo 'here';
          if($question == '')
          {
            //its delete
            //echo $question.' delte';
            $this->SurveyQuestion->remove($qid);
          } else {
            #echo $question.' update';
            $data = array('title' => $question,'question_tips'=>mysql_real_escape_string($questions_tips[$qid]));
            $this->SurveyQuestion->update($data, $qid);
          }

          
        } elseif($question != '')
        {
          $data = array('survey_id' => $id, 'category_id' => $category, 'status' => 'active', 'title' => $question,'question_tips'=>mysql_real_escape_string($questions_tips[$qid]));
          $this->SurveyQuestion->add($data);
        }
          
      }

      echo json_encode(array('status'=> 'success'));
      exit();

    }

    
    //$details['category'] = $this->input->post('subcategory');

    
  }

  function get_questions() {

    $this->load->model('SurveyQuestion');
    $data = array();
    $survey_id = $this->input->get('survey_id');
    $category_id = $this->input->get('category_id');
    $question_categories = $this->SurveyQuestion->getQuestions($survey_id, $category_id);
  //print "<pre>";
  //print_r($question_categories);
  //exit;
    $data['json'] = json_encode(array('questions' => $question_categories));
    $this->load->view("print_json", $data);
  }

  function index() {
    $data = array();
    $data['audit_constant'] = $this->config->item('audit');
    $concepts = $this->Survey->getSurveyConcept();
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
      }
    }
    $outlets = $this->Outlets->getOutletbyFilter(array());
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
      }
    }
    $this->load->view('user_view', $data);
  }

  function report(){
    $my_user = $this->session->userdata("logged_user");
    $this->load->model('Concept');
    $data['audit_constant'] = $this->config->item('audit');
    $data['concepts'] = $this->Survey->getSurveyConceptNames();
//$data['countries'] = $this->Territory->getAll();
$list[]=array();    
foreach ($data['concepts'] as $value) {
  //echo $value->id;
   if(count($data['concepts'])>0){
      $territory[] = $this->Territory->getTerritoryByConcept($value->id);
        foreach($territory as $k=>$territories){
         //echo '<pre>';print_r($territories);
         $list=array_merge($list,$territories);
        }
      }
} 
  unset($list[0]);
 $territory = array_unique($list, SORT_REGULAR);
  $data['countries']=$territory;
  // echo '<pre>';print_r($territory);exit;
   

    $data['outlets'] = $this->Outlets->getOutletbyFilter(array());
    $data['categories'] = $this->SurveyQuestionCategory->getCategories(0);
    $filter['user_id'] = $my_user['userid'];
    $concepts = $this->Survey->getSurveyConcept();
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
      }
    }
    $outlets = $this->Outlets->getOutletbyFilter(array());
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
        $data['outlet_manager_id'][$outlet->user_id] = $outlet->id;
      }
    }
    
    $myConcept = $this->ConceptManager->getConceptbyFilter($filter);

    if(isset($myConcept) && !empty($myConcept))
    {
      $data['myconcept'] = $myConcept[0]->concept_id;
      $user_concept_details = $this->Concept->getConcept($myConcept[0]->concept_id,$filter['user_id']);
      $data['myconcept_name'] = $user_concept_details['name'];
    } 
    else{
      $data['myconcept'] = '';
      $data['myconcept_name'] = '';
    }   
    //echo '<pre>';print_r($data);exit;
    $this->load->view("survey_report", $data);
  }
  function positive_assurance(){
    $data = array();
    $concepts = $this->Survey->getSurveyConcept();
    $data['audit_constant'] = $this->config->item('audit');
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
      }
    }
    $outlets = $this->Outlets->getOutletbyFilter(array());
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
      }
    }
    $this->load->view('positive_assurance', $data);
  }
  function audit_report()
  {
    $data = array();
    $concepts = $this->Survey->getSurveyConcept();
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
      }
    }
    $data['audit_constant'] = $this->config->item('audit');
    $this->load->view('audit_report', $data);
  }
  function report_ajax(){
    $type = $this->input->get('type');
    $year = $this->input->get('year');
    $export = $this->input->get('export');
    $export_type = $this->input->get('export_type');
    
    $startYear = ((int)$year-1);
    $endYear = (int)$year;
    $territory_array = array();
    $category_array = array();
    $total = array();
    switch($type)
    {
      case '1':
        $territory_id = $this->input->get('territory');   
        $concept_id = $this->input->get('concept');
        $outlet_set = $this->input->get('outlet_set');
        $outlet_id = '';
        if(isset($outlet_set) && !empty($outlet_set))
        {
          $outlet_id = $this->input->get('outlet');
        }
        
        $MidstartDate =  $startYear.'-01-01 00:00:00';
        $MidendDate =  $startYear.'-06-30 00:00:00';
        $dateFilter = array();
        $dateFilter['startDate'] = $MidstartDate;
        $dateFilter['endDate'] = $MidendDate;
        $dateFilter['type'] = 'mid';
        $surveyResult[] = $this->SurveyAnswer->surveyResultbyConcept($concept_id,$territory_id,$dateFilter,$outlet_id);
        
        $AnnualstartDate =  $startYear.'-07-01 00:00:00';
        $AnnualendDate =  $endYear.'-01-01 00:00:00';
        $dateFilter = array();
        $dateFilter['startDate'] = $AnnualstartDate;
        $dateFilter['endDate'] = $AnnualendDate;
        $dateFilter['type'] = 'annual';
        $surveyResult[] = $this->SurveyAnswer->surveyResultbyConcept($concept_id,$territory_id,$dateFilter,$outlet_id);
        foreach($surveyResult as $key => $results)
        {
          foreach($results as $k => $report)
          {
            $final_record[$report->id] = (object)array('id' => $report->id);
            if(isset($report->mid_percentage))
            {
              $final_record[$report->id]->mid_percentage = $report->mid_percentage;
            }
            
            if(isset($report->annual_percentage))
            {
              $final_record[$report->id]->annual_percentage = $report->annual_percentage;
            }
            
            $final_record[$report->id]->outlet_name = $report->outlet_name;
          } 
        }
        #echo "<pre>".print_r($final_record,'\n')."</pre>";
      break;
      case '1.1':
        $outlet_id = $this->input->get('outlet');
        $yearType =  $this->input->get('yearType');
        $dateFilter = array();
        if($yearType == 'mid')
        {
          $dateFilter['startDate'] =  $startYear.'-01-01 00:00:00';
          $dateFilter['endDate'] =  $startYear.'-06-30 00:00:00';
        }
        elseif($yearType == 'annual')
        {
          $dateFilter['startDate'] =  $startYear.'-07-01 00:00:00';
          $dateFilter['endDate'] =  $endYear.'-01-01 00:00:00';
        }
        
        $final_record = $this->SurveyAnswer->surveyResultbyOutlets($outlet_id,false,$dateFilter);
      break;
      case '1.2':
        $total_no = 0;
        $percentage = 0;
        $infoArray = array();
        $outlet_id = $this->input->get('outlet'); 
        $category_id = $this->input->get('category');        
        $yearType =  $this->input->get('yearType');
        $dateFilter = array();
        if($yearType == 'mid')
        {
          $dateFilter['startDate'] =  $startYear.'-01-01 00:00:00';
          $dateFilter['endDate'] =  $startYear.'-06-30 00:00:00';
        }
        elseif($yearType == 'annual')
        {
          $dateFilter['startDate'] =  $startYear.'-07-01 00:00:00';
          $dateFilter['endDate'] =  $endYear.'-01-01 00:00:00';
        }
        $surveyResult = $this->SurveyAnswer->getRiskControlMatrixByOutlet($outlet_id,$category_id,$dateFilter);
        $expectedControl = 0;
        foreach($surveyResult as $key=>$record)
        {
          $infoArray['outlet_name'] = $record->outlet_name;
          $infoArray['manager_name'] = $record->manager_name;
          $infoArray['risk'] = $record->risk_impact;
          $infoArray['assessment_date'] = $record->assessment_date;
          
          if($record->answer == 'No')
          {
            $total_no++;
          }
          
          if($record->answer == 'No' || $record->answer == 'Yes')
          {
            $expectedControl++;
          }
          if($expectedControl != 0) {
            $percentage = round($total_no/$expectedControl*100,0);
          }
          else {
            $percentage = 0;
          }
          
         // echo $total_no;
          //echo $expectedControl;
         // exit;
          $infoArray['expected_control'] = $expectedControl;
          $infoArray['risk_exposure'] = $percentage;
          $final_record['info'] = $infoArray;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['risk_description'] = $record->risk_desc;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['questions'] = $record->questions;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['na_reason'] = $record->na_reason;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['response'] = $record->answer;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['responsible'] = $record->responsibility;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['actionplan'] = $record->actionplan;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['timeline'] = date('Y-m-d',strtotime($record->actionplan_dttm));
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['question_count'] = count($final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions']);
        }
        
        #echo "<pre>".print_r($final_record,"\n")."</pre>";
      break;
      case '1.3':
        $total_no = 0;
        $outletId = $this->input->get('outlet');
        $yearType =  $this->input->get('yearType');
        $dateFilter = array();
        if($yearType == 'mid')
        {
          $dateFilter['startDate'] =  $startYear.'-01-01 00:00:00';
          $dateFilter['endDate'] =  $startYear.'-06-30 00:00:00';
        }
        elseif($yearType == 'annual')
        {
          $dateFilter['startDate'] =  $startYear.'-07-01 00:00:00';
          $dateFilter['endDate'] =  $endYear.'-01-01 00:00:00';
        } 
        $surveyResult = $this->SurveyAnswer->getRiskControlMatrix($outletId,$dateFilter);
        $total_question = 0;  
        $c = 0;
        $d = 0;
        $expectedControl = 0;
        $percentage='';
        foreach($surveyResult as $key=>$record)
        {
          if($outletId != 0)
          {
            $final_record['outlet_name'] = $record->outlet_name;
          }
          
          $infoArray['outlet_name'] = $record->outlet_name;
          $infoArray['manager_name'] = $record->manager_name;
          $infoArray['risk'] = $record->risk_impact;
          
          if($record->answer == 'No')
          {
            $total_no++;
          }
        
          if($record->answer == 'No' || $record->answer == 'Yes')
          {
            $expectedControl++;
          }
          if($expectedControl>0)
          {
          $percentage = round($total_no/$expectedControl*100,0);
          }
          $infoArray['expected_control'] = $expectedControl;
          $infoArray['risk_exposure'] = $percentage;
          $infoArray['assessment_date'] = $record->assessment_date;
          $final_record['info'] = $infoArray;
          //echo "<pre>".print_r($record,"\n")."</pre>";exit;
          $final_record['records'][$record->risk_id]['risk_impact'] = $record->risk_impact;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['risk_description'] = $record->risk_desc;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['questions'] = $record->questions;
          if(!empty($record->na_reason)) {
            $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['na_reason'] = $record->na_reason; 
          }
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['response'] = $record->answer;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['responsible'] = $record->responsibility;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['actionplan'] = $record->actionplan;
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions'][$key]['timeline'] = date('Y-m-d',strtotime($record->actionplan_dttm));
          
                      
          $final_record['records'][$record->risk_id]['risk_count'] = (count($final_record['records'][$record->risk_id]['risk']));
          $final_record['records'][$record->risk_id]['risk'][$record->desc_id]['question_count'] = count($final_record['records'][$record->risk_id]['risk'][$record->desc_id]['questions']); 
        }
        $f_record = array_filter($final_record['records']);
        $final_count = 0;
        foreach($f_record as $key => &$counter)
        { 
          $final_count = $b=0;
          foreach($counter['risk'] as $risk) {
            $final_count += $risk['question_count'];
          }
          $b =  $final_count + $counter['risk_count'];
          $final_record['records'][$key]['risk_count'] = $b;
        }
        
      break;
      case '2':
        $territory_id = $this->input->get('territory');   
        $concept_id = $this->input->get('concept');
        $outlet_set = $this->input->get('outlet_set');
        $outlet_id = '';
        if(isset($outlet_set) && !empty($outlet_set))
        {
          $outlet_id = $this->input->get('outlet');
        }
        $dateFilter = array();
        $dateFilter['startDate'] =  $startYear.'-01-01 00:00:00';
        $dateFilter['endDate'] =  $endYear.'-01-01 00:00:00';
        
        $final_record = array();
        $surveyResult = $this->SurveyAnswer->surveyResultbyTerritory($territory_id,$concept_id,$dateFilter,$outlet_id);
        
        foreach($surveyResult as $record)
        {
          $final_record[$record->qustion_category][$record->territory_id]=$record;
          $territory_array[$record->territory_id] = $record->territory_name;
        }
        #echo "<pre>".print_r($final_record,"\n")."</pre>";
      break;
      case '2.1':
        $territory_id = $this->input->get('territory');   
        $category_id = $this->input->get('category');
        $concept_id = $this->input->get('concept');
        $outlet_set = $this->input->get('outlet_set');
        $outlet_id = '';
        if(isset($outlet_set) && !empty($outlet_set))
        {
          $outlet_id = $this->input->get('outlet');
        }
        $dateFilter = array();
        $dateFilter['startDate'] =  $startYear.'-01-01 00:00:00';
        $dateFilter['endDate'] =  $endYear.'-01-01 00:00:00';
        $allTerritory = 1;
        if(isset($territory_id) && !empty($territory_id))
        {
          $allTerritory = 0;
        }
        $surveyResult = $this->SurveyAnswer->surveyResultbyCategoryTerritory($category_id,$territory_id,$concept_id,$dateFilter,$outlet_id);
        
        foreach($surveyResult as $key => $results)
        {
            $results->all = $allTerritory;
            $final_record[] = $results;
        }
        #echo "<pre>".print_r($final_record,"\n")."</pre>";
      break;
    }
   
    if(!$export || !isset($export))
    {
      if(isset($final_record) && !empty($final_record))
      {
        $data['json'] = json_encode(array('report'=>$final_record,'status'=>true,'territory'=>$territory_array,'category'=>$category_array));  
      }
      else
      {
        $data['json'] = json_encode(array('status'=>false));
      }    
      $this->load->view("print_json", $data);
    }
    else
    {
      $data = array('report'=>$final_record,'status'=>true,'territory'=>$territory_array,'category'=>$category_array,'get'=>$this->input->get());
      $this->load->view('export_report',$data);
    }
  }
  function outlets($values = null){
    #echo $values;
    $data = array();
    $result = array();
    $status = array();
    $this->load->model('Concept');
    $data['concepts'] = $this->Concept->getAll();
    $data['audit_constant'] = $this->config->item('audit');
    $this->load->model('Territory');
    $data['countries'] = $this->Territory->getAll();
    $audit_constant = $this->config->item('audit');
    switch($values)
    {
      case 'edit':
          $outlet_id = $this->input->post('outlet_id');
          if(isset($outlet_id) && !empty($outlet_id))
          {
            $filter['id'] = $outlet_id;
            $outlet = $this->Outlets->getOutletbyFilter($filter);
            $userDetail = $this->User->get((int)$outlet[0]->user_id);
            $outlet[0]->username = $userDetail[0]->reply_username;
          }
          $result['json'] = json_encode($outlet[0]);
          $this->load->view("print_json", $result);
      break;
      case 'save':
          $outlet = $this->input->post('outlet');
          $outlet_id = $this->input->post('outlet_id');
          if(isset($outlet) && !empty($outlet))
          {
            $userValue = $this->User->getByUsername(trim($this->input->post('username')));
            $saveOutlet = array(
              'user_id' => $userValue['id'],
              'concept_id' => trim($this->input->post('concept')),
              'outlet_name' => trim($this->input->post('outlet')),
              'territory_id' => trim($this->input->post('territory')),
              'created_on' => date('Y-m-d H:i:s',time()),
            );
            
            
    
            
            if(isset($outlet_id) && !empty($outlet_id))
            {
              if($this->Outlets->update($saveOutlet,$outlet_id))
              {
                $id = $outlet_id;
              }
            }
            else
            {
              $id = $this->Outlets->addOutlet($saveOutlet);
            }
            if(is_numeric($id))
            {
             $status = array('status'=>true,'exist' => false);
              /** Adding outlet manager to the self compliance portal **/
              if(!$this->Group->checkGroupMember($audit_constant['knowledge_center_group_id'],$userValue['id']))
              {
              $details['object_id'] = $userValue['id'];
              $details['object_type'] = 'User';
              $details['parent_type'] = 'Group';
              $details['parent_id']  = $audit_constant['knowledge_center_group_id'];
              $this->ObjectMember->followObject($details);
              }
              if(!$this->Group->checkGroupMember($audit_constant['user_guide_group_id'],$userValue['id']))
              {
              $details['object_id'] = $userValue['id'];
              $details['object_type'] = 'User';
              $details['parent_type'] = 'Group';
              $details['parent_id']  = $audit_constant['user_guide_group_id'];
              $this->ObjectMember->followObject($details);
              }
              
              /*$details['object_id'] = $userValue['id'];
              $details['object_type'] = 'User';
              $details['parent_type'] = 'Group';
              $details['parent_id']  = $audit_constant['internal_audit_plan_group_id'];
              $this->ObjectMember->followObject($details);
              
              $details['object_id'] = $userValue['id'];
              $details['object_type'] = 'User';
              $details['parent_type'] = 'Group';
              $details['parent_id']  = $audit_constant['outlet_audit_reports_group_id'];
              $this->ObjectMember->followObject($details);
              
              $details['object_id'] = $userValue['id'];
              $details['object_type'] = 'User';
              $details['parent_type'] = 'Group';
              $details['parent_id']  = $audit_constant['process_audit_reports_group_id'];
              $this->ObjectMember->followObject($details);*/
              
              
            }
            else
            {
              $status = array('status'=>false);
              if($id === false)
              {
                $status['exist'] = true;
              }
            }
          }
          $result['json'] = json_encode($status);
          $this->load->view("print_json", $result);
      break;
      case 'delete':
        $details = array();
        $filter['id'] = $this->input->get('id');
        $outlet = $this->Outlets->getOutletbyFilter($filter);
        $details['object_id'] = $outlet[0]->user_id;
        $details['object_type'] = 'User';
        $details['parent_type'] = 'Group';
        $details['parent_id']  = $audit_constant['self_compliance_portal_group_id'];
        $affected_row = $this->ObjectMember->unFollowObject($details);
        $this->Outlets->remove($this->input->get('id'));
        header("location:/surveys/outlets/manage");
      break;
      case 'ajax':
        if($this->input->get('concept') != 0 || $this->input->get('country') != 0)
        {
            $filter = array();
            if($this->input->get('concept') != 0)
            {
              $filter['concept_id'] = $this->input->get('concept');
            }
            if($this->input->get('country') != 0)
            {
              $filter['territory_id'] = $this->input->get('country');
            }
            $result = $this->Outlets->getOutletbyFilter($filter);
        }
        else
        {
          $result = $this->Outlets->getAll(null,array('id'=>'ASC'));
        }
        foreach($data['concepts'] as $concept)
        {
          $concepts[$concept->id] = $concept->name;
        }
        foreach($data['countries'] as $country)
        {
          $countries[$country->id] = $country->name;
        }
        $this->load->model('User');
        
        if(isset($result) && !empty($result))
        {
          foreach($result as $key => $outlet)
          {
            $outlets['result'][$key]['id'] = $outlet->id;
            $outlets['result'][$key]['concept'] = $concepts[$outlet->concept_id];
            $outlets['result'][$key]['territory'] = $countries[$outlet->territory_id];
            $outlets['result'][$key]['outlet_name'] = $outlet->outlet_name;
            $Username = $this->User->getFullName((int)$outlet->user_id);
            $outlets['result'][$key]['user_name'] = $Username;
            $outlets['status'] = true;
          }
        } 
        else
        {
          $outlets['status'] = false;
        }
        $result['json'] = json_encode($outlets);
        $this->load->view("print_json", $result);
      break;
      case 'manage':
      default:
        $this->load->view('manage_outlet',$data);
      break;
    }
  }
  
  function concepts($values = null){
    #echo $values;
    $data = array();
    $result = array();
    $status = array();
    $this->load->model('Concept');
    $data['concepts'] = $this->Concept->getAll();
    $data['audit_constant'] = $this->config->item('audit');
    $audit_constant = $this->config->item('audit');
    switch($values)
    {
      case 'edit':
          $concept_id = $this->input->post('concept_id');
          if(isset($concept_id) && !empty($concept_id))
          {
            $filter['id'] = $concept_id;
            $concept = $this->ConceptManager->getConceptbyFilter($filter);
            $userDetail = $this->User->get((int)$concept[0]->user_id);
            $concept[0]->username = $userDetail[0]->reply_username;
          }
          $result['json'] = json_encode($concept[0]);
          $this->load->view("print_json", $result);
      break;
      case 'save':
          $concept = $this->input->post('concept');
          $concept_id = $this->input->post('concept_id');
          if(isset($concept) && !empty($concept))
          {
            $userValue = $this->User->getByUsername(trim($this->input->post('username')));
            $saveConcept = array(
              'user_id' => $userValue['id'],
              'concept_id' => trim($this->input->post('concept')),
              'created_on' => date('Y-m-d H:i:s',time()),
            );
            if(isset($concept_id) && !empty($concept_id))
            {
              if($this->ConceptManager->update($saveConcept,$concept_id))
              {
                $id = $concept_id;
              }
            }
            else
            {
              $id = $this->ConceptManager->addConcept($saveConcept);
            }
            if(is_numeric($id))
            {
             $status = array('status'=>true,'exist' => false);
             /** Adding outlet manager to the self compliance portal **/
              if(!$this->Group->checkGroupMember($audit_constant['knowledge_center_group_id'],$userValue['id']))
              {
                $details['object_id'] = $userValue['id'];
                $details['object_type'] = 'User';
                $details['parent_type'] = 'Group';
                $details['parent_id']  = $audit_constant['knowledge_center_group_id'];
                $this->ObjectMember->followObject($details);
              }
              if(!$this->Group->checkGroupMember($audit_constant['user_guide_group_id'],$userValue['id']))
              {
                $details['object_id'] = $userValue['id'];
                $details['object_type'] = 'User';
                $details['parent_type'] = 'Group';
                $details['parent_id']  = $audit_constant['user_guide_group_id'];
                $this->ObjectMember->followObject($details);
              }
              if(!$this->Group->checkGroupMember($audit_constant['internal_audit_plan_group_id'],$userValue['id']))
              {
                $details['object_id'] = $userValue['id'];
                $details['object_type'] = 'User';
                $details['parent_type'] = 'Group';
                $details['parent_id']  = $audit_constant['internal_audit_plan_group_id'];
                $this->ObjectMember->followObject($details);
              }
              if(!$this->Group->checkGroupMember($audit_constant['outlet_audit_reports_group_id'],$userValue['id']))
              {
                $details['object_id'] = $userValue['id'];
                $details['object_type'] = 'User';
                $details['parent_type'] = 'Group';
                $details['parent_id']  = $audit_constant['outlet_audit_reports_group_id'];
                $this->ObjectMember->followObject($details);
              }
              if(!$this->Group->checkGroupMember($audit_constant['process_audit_reports_group_id'],$userValue['id']))
              {
                $details['object_id'] = $userValue['id'];
                $details['object_type'] = 'User';
                $details['parent_type'] = 'Group';
                $details['parent_id']  = $audit_constant['process_audit_reports_group_id'];
                $this->ObjectMember->followObject($details);
              }
            }
            else
            {
              $status = array('status'=>false);
              if($id === false)
              {
                $status['exist'] = true;
              }
            }
          }
          $result['json'] = json_encode($status);
          $this->load->view("print_json", $result);
      break;
      case 'delete':
        $details = array();
        $filter['id'] = $this->input->get('id');
        $concept = $this->ConceptManager->getConceptbyFilter($filter);
        $details['object_id'] = $concept[0]->user_id;
        $details['object_type'] = 'User';
        $details['parent_type'] = 'Group';
        $details['parent_id']  = $audit_constant['self_compliance_portal_group_id'];
        $affected_row = $this->ObjectMember->unFollowObject($details);
        
        $this->ConceptManager->remove($this->input->get('id'));
        header("location:/surveys/concepts/manage");
      break;
      case 'ajax':
        if($this->input->get('concept') != 0)
        {
            $filter = array();
            if($this->input->get('concept') != 0)
            {
              $filter['concept_id'] = $this->input->get('concept');
            }
            $result = $this->ConceptManager->getConceptbyFilter($filter);
        }
        else
        {
          $result = $this->ConceptManager->getAll(null,array('concept_id'=>'ASC'));
        }
        foreach($data['concepts'] as $concept)
        {
          $concepts[$concept->id] = $concept->name;
        }
        $this->load->model('User');
        
        if(isset($result) && !empty($result))
        {
          foreach($result as $key => $concept)
          {
            $ConceptM['result'][$key]['id'] = $concept->id;
            $ConceptM['result'][$key]['concept'] = $concepts[$concept->concept_id];
            $Username = $this->User->getFullName((int)$concept->user_id);
            $ConceptM['result'][$key]['user_name'] = $Username;
            $ConceptM['status'] = true;
          }
        } 
        else
        {
          $ConceptM['status'] = false;
        }
        $result['json'] = json_encode($ConceptM);
        $this->load->view("print_json", $result);
      break;
      case 'manage':
      default:
        $this->load->view('manage_survey_concept',$data);
      break;
    }
  }
  
  function auto_users()
  {
    if(isset($_GET["q"]))
    {
      $name = mysql_real_escape_string($_GET["q"]);
    }
    else
    {
      $name='';
    }
    $limit = 10;
    $obj = $this->Outlets->getAllManager($name);
    $arr[] = $obj;
    foreach($obj as $key => $user)
    {
      $obj[$key]->name =$user->first_name." ".$user->last_name;
      unset($obj[$key]->last_name);
      unset($obj[$key]->first_name);
    }

    # JSON-encode the response
    $json_response = json_encode($obj);

    # Optionally: Wrap the response in a callback function for JSONP cross-domain support

    if(isset($_GET["callback"])) {
      $json_response = $_GET["callback"] . "(" . $json_response . ")";
    }

    # Return the response
    echo $json_response;
  }
  function ajax_list_survey()
  {
    $data = array();
    $my_user = $this->session->userdata("logged_user");
    $myprofile = $this->User->getByEmail($my_user['useremail']);
    $this->load->model('Role');
    $permission_roles = array();
    foreach($this->Role->getRoleKeys() as $role)
    {
      $permission_roles[$role['key']] = $role['id'];
    }
    $filter['isAdmin'] = false;
    if((isset($permission_roles['AM']) && !empty($permission_roles['AM']) && $myprofile['role_id'] == $permission_roles['AM']) || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA']))
    {
      $filter['isAdmin'] = true;
    }
    $filter['user_id'] = $my_user['userid']; 
    $filter['date'] = $this->input->post('ddDate');
    $filter['concept'] = $this->input->post('ddConcepts'); 
    $surveys = $this->Survey->getSurveybyFilter($filter);
    if(isset($surveys) && !empty($surveys))
    {
      foreach($surveys as $key=>$survey)
      {
        $answers = $this->SurveyAnswer->getSurveyAnswers($survey->id,$myprofile['id']);
        if(isset($answers) && !empty($answers))
        {
          $surveys[$key]->answered = 1;
        }
        else
        {
          $surveys[$key]->answered = 0;
        }
      }
      # JSON-encode the response
      $result['json'] = json_encode(array('status'=>true,'result'=>$surveys,'isAdmin'=>$filter['isAdmin']));
    }
    else
    {
      $result['json'] = json_encode(array('status'=>false));
    }
    $this->load->view("print_json", $result);
  }
  function managment_action_plan(){
    $data = array();
    $my_user = $this->session->userdata("logged_user");
    $this->load->model('Concept');
    $data['audit_constant'] = $this->config->item('audit');
    $data['concepts'] = $this->Survey->getConcepts();
    //$data['countries'] = $this->Territory->getAll();
    $list[]=array();    
   // echo '<pre>';print_r($data['concepts']);exit;
foreach ($data['concepts'] as $value) {
 // echo $value->id;exit;
   if(count($data['concepts'])>0){
      $territory[] = $this->Territory->getTerritoryByConcept($value['id']);
        foreach($territory as $k=>$territories){
         //echo '<pre>';print_r($territories);
         $list=array_merge($list,$territories);
        }
      }
} 
  unset($list[0]);
 $territory = array_unique($list, SORT_REGULAR);
  $data['countries']=$territory;
    $data['outlets'] = $this->Outlets->getOutletbyFilter(array());

    $filtern['user_id'] = $my_user['userid'];

    $myConcept = $this->ConceptManager->getConceptbyFilter($filtern);
    if(isset($myConcept) && !empty($myConcept))
    {
      $data['myconcept'] = $myConcept[0]->concept_id;
      $user_concept_details = $this->Concept->getConcept($myConcept[0]->concept_id,$filtern['user_id']);
      $data['myconcept_name'] = $user_concept_details['name'];
    } 
    else{
      $data['myconcept'] = '';
      $data['myconcept_name'] = '';
    }   

    $filter = array();
    $outlets = $this->Outlets->getOutletbyFilter(array());
    $outletManager = array();
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
        $data['surveyoutlets'][$outlet->user_id] = $outlet->id;
      }
    }
    $concepts = $this->Survey->getSurveyConcept();
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
        $data['surveyconcepts'][$concept->user_id] = $concept->concept_id;
      }
    }
    $MidstartDate =  date('Y',time()).'-01-01 00:00:00';
    $MidendDate =  date('Y',time()).'-06-30 00:00:00';
    $filter['start_date'] = $MidstartDate;
    $filter['end_date'] = $MidendDate;
    $questions_mid = $this->SurveyAnswer->getSurveyManagementActionPlanQuestion($filter);
    $data['category_mid'] = $questions_mid;
    
    $AnnualstartDate =  date('Y',time()).'-07-01 00:00:00';
    $AnnualendDate =  (date('Y',time())+1).'-01-01 00:00:00';
    $filter['start_date'] = $AnnualstartDate;
    $filter['end_date'] = $AnnualendDate;
    $questions_annual = $this->SurveyAnswer->getSurveyManagementActionPlanQuestion($filter);
    $data['category_annual'] = $questions_annual;
    #echo "<pre>".print_r($questions_mid,"\n")."</pre>";
    #echo "<pre>".print_r($questions_annual,"\n")."</pre>";
    $this->load->view('management_action_plan',$data);
  }
  function ajax_manangement_action_plan(){
    $year = $this->input->get('year');
    $outlet = $this->input->get('outlet');
    $concept = $this->input->get('concept');
    $territory = $this->input->get('territory');
    $filter = array();
    $question = array();
    
    
    $filter['outlet'] = $outlet;
    $filter['concept'] = $concept;
    $filter['territory'] = $territory;
    
    if(!isset($year) || empty($year))
    {
      $year = date('Y',time());
    }
      $MidstartDate = $year.'-01-01 00:00:00';
      $MidendDate =  $year.'-06-30 00:00:00';
      $filter['start_date'] = $MidstartDate;
      $filter['end_date'] = $MidendDate;
    
    $questions_mid = $this->SurveyAnswer->getSurveyManagementActionPlanQuestion($filter);
    
      $AnnualstartDate =  $year.'-07-01 00:00:00';
      $AnnualendDate =  ($year+1).'-01-01 00:00:00';
      $filter['start_date'] = $AnnualstartDate;
      $filter['end_date'] = $AnnualendDate;
    
    $questions_annual = $this->SurveyAnswer->getSurveyManagementActionPlanQuestion($filter);
    
    #echo "<pre>".print_r($questions_mid,"\n")."</pre>";
    #echo "<pre>".print_r($questions_annual,"\n")."</pre>";
    
    if(isset($questions_mid) && !empty($questions_mid))
    {
      $info['manager_name'] = $questions_mid[0]->manager_name;
      $info['outlet_name'] = $questions_mid[0]->outlet_name;
      $info['assessment_date'] = $questions_mid[0]->assessment_date;
      
    }
    elseif(isset($questions_annual) && !empty($questions_annual))
    {
      $info['manager_name'] = $questions_annual[0]->manager_name;
      $info['outlet_name'] = $questions_annual[0]->outlet_name;
      $info['assessment_date'] = $questions_annual[0]->assessment_date;
      
    }
    
    
    if(isset($questions_mid) && !empty($questions_mid))
    {
      $question['records_mid'] = $questions_mid;
    }
    if(isset($questions_annual) && !empty($questions_annual))
    {
      $question['records_annual'] = $questions_annual;
    }
    
    if((isset($questions_mid) && !empty($questions_mid)) || (isset($questions_annual) && !empty($questions_annual)))
    {
      $question['info'] = $info;
      $question['status'] = true;
      $result['json'] = json_encode($question);
    }
    else
    {
      $result['json'] = json_encode(array('status'=>false));
    }
    $this->load->view("print_json", $result);
  }
  function fetch_outlet_by_territory_ajax()
  {
    $territory = $this->input->get('territory');
  $concept = $this->input->get('concept');
    $outlets = array();
    $status = false;
    $outlets = $this->Outlets->fetchOutletByTerritory($territory,$concept);
    if(isset($outlets) && !empty($outlets))
    {
      $status = true;
    }
    //print_r($outlets);
    //exit;
    $result['json'] = json_encode(array('status'=>$status,'outlets'=>$outlets));
    $this->load->view("print_json", $result);
  }
  
  function fetch_territory_by_concept_ajax()
  {
    $concept = $this->input->get('concept');
    $outlets = array();
    $status = false;
  $my_user = $this->session->userdata("logged_user");
  $filtern['user_id'] = $my_user['userid'];
  if($concept==0){
    //$conceptsList = $this->ConceptManager->getConceptbyFilter($filtern);
    $conceptNames = $this->Survey->getSurveyConceptNames();
//$data['countries'] = $this->Territory->getAll();
$list[]=array();    
foreach ($conceptNames as $value) {
  //echo $value->id;
   if(count($conceptNames)>0){
      $territory[] = $this->Territory->getTerritoryByConcept($value->id);
        foreach($territory as $k=>$territories){
         //echo '<pre>';print_r($territories);
         $list=array_merge($list,$territories);
        }
      }
    } 
      unset($list[0]);
      $territory = array_unique($list, SORT_REGULAR);
      $territory=$territory; 
      }
      else {
      $territory = $this->Territory->getTerritoryByConcept($concept);
      $territory = array_unique($territory, SORT_REGULAR);
      }

   
  //print "<pre>";
  //print_r($territory);
  //exit;
    //$outlets = $this->Outlets->fetchOutletByTerritory($concept);
    if(isset($territory) && !empty($territory))
    {
      $status = true;
    }
    $result['json'] = json_encode(array('status'=>$status,'outlets'=>$territory));
    $this->load->view("print_json", $result);
  }
}

/* End of file surveys.php */
/* Location: ./system/application/modules/surveys/controllers/surveys.php */