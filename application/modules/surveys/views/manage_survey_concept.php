<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Survey Concept Management - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm.submit
{
    float: right;
}
.concept .ui-autocomplete{
  z-index:9999;
}
#add_concept_tab{
  float: right;
  margin: 5px 29px 5px 0;
}
</style>
</head>
<body class="full-width concept">

	<?php $this->load->view('global_header.php'); 
  if((isset($permission_roles['AM']) && !empty($permission_roles['AM']) && $myprofile['role_id'] == $permission_roles['AM']) || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA'])):
  ?>

	<div class="section wrapper clearfix">
 	<h2>Survey Concept Management</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Manage Concept Managers</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">
        	<?php
					$this->load->view('partials/survey_concept_filter');
				?>
        <a href="#add_concept" id="add_concept_tab" class="btn-sm fancybox" >Add Concept Manager</a>
      <div class="result">
      
      </div>
    <!-- div class="loader-more">&nbsp;</div -->
	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>
<?php $this->load->view('partials/add_survey_concept'); ?>
<script type="text/javascript">
var limit = 6;
var start = 0;
var type = 0;
var end = false;

var nearToBottom = 250;
$(document).ready(function() {
  
	$('#cancel').on( "click", function() {
		$('#add_concept .msg').hide();
		$.fancybox.close();
	});
  $('#delete_concept').live('click',function(){
    if(confirm("Are you sure to delete this concept?") === false)
    {
      return false;
    }
  });
	$('#add_concept_tab,.fancybox').fancybox({
		maxHeight: 500,
		minWidth: 230,
    afterClose  : function() {
      $("#frm_add_concept")[0].reset();
    },
    beforeLoad : function() {
     var conceptid = $(this)[0].element.context.attributes[1]['nodeValue'];
     if(!isNaN(conceptid))
     {
        loadEditOutlet(conceptid);
     }
    },
  });

	$('.loader-more').show();
	loadConcept({'noclear': 0});
  $('#clear_concept').live('click',function(){
    $('#fFilter')[0].reset();
    loadConcept({'noclear': 0});
  });
	$('#ddConcepts').change(function(){
		end = false;
		start = 0;
		loadConcept({'noclear': 0});
	});
  
  $( "#txt_name" ).autocomplete({
    source: siteurl + 'people/autocomplete_ajax',
	 position: { my: "left-11 top", at: "left bottom", collision: "none" },
	 appendTo: "#filter-search .search",
    select: function (a, b) {
      $(this).val(b.item.link);
      return false;
    }
  });
  
  $('#bt_add_concept').live('click',function(){
    var name = $('#txt_name').val();
    var concept = $('#txt_concept').val();
    var concept_id = $('#concept_id').val();
    if(!name)
    {
      $("#error-txt_name").html("Required");
      $("#txt_name").focus();
      return false;
    }
    else if(!concept)
    {
      $("#error-txt_concept").html("Required");
      $("#txt_concept").focus();
      return false;
    }
    else
    {
      $("#error-txt_name").html("");
      $("#error-txt_concept").html("");
    }
    
   var data = {'username' : name, 'concept': concept,'concept_id':concept_id}; 
   $.ajax({
      url: $('#frm_add_concept').attr('action'),
      data: data,
      async: false,
      dataType: "json",
      type: "POST",
      success: function(data){
        if(data.status == true)
        {
          loadConcept({'noclear': 1});
          $('#add_concept .msg').show();
          $('#add_concept .msg').addClass('success').html('Concept Manager added successfully');
          $("#frm_add_concept")[0].reset();
          $('#add_concept .msg').delay(5000).hide();
          $.fancybox.close();
        }
        else
        {
          if(data.exist == true)
          {
            $('#add_concept .msg').show();
            $('#add_concept .msg').addClass('error').removeClass('success');
            $('#add_concept .msg').html('Record already exist.');
          }
        }
      }
   });
      
  });
});
function loadEditOutlet(concept_id)
{
   var data = {'concept_id' : concept_id}; 
   var service_url = siteurl+"surveys/concepts/edit";
   $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "POST",
      success: function(data){
        if(data)
        {
          $('#txt_name').val(data.username);
          $('#txt_concept').val(data.concept_id);
          $('#concept_id').val(data.id);
        }
      }
   });
}
function loadConcept(prop)
{
  $('div.outlet-grid-view').show();
  if(!prop.noclear)
	{
  	var concept = $('#ddConcepts').val();
	}
  
	var service_url = siteurl+"surveys/concepts/ajax";
  
	var data = {'concept': concept};
	if(end == false)
	{
     $('.loader-more').show();
     $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "GET",
      success: function(data){
          if(data.status == true)
          {
            var html ='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
            html +='<tr>';
            html +='<th width="20%">User Name</th>';
            html +='<th width="15%">Concept Name</th>';
            html +='<th width="15%" colspan="2">Action</th>';
            html +='</tr>';
            $.each(data.result, function() {
            html +='<tr>';
            html +='<td>'+this.user_name+'</td>';
            html +='<td>'+this.concept+'</td>';
            html +='<td><a href="#add_concept" id="'+this.id+'" class="btn-sm fancybox">Edit</a></td>';
            html +='<td><a href="'+siteurl+'surveys/concepts/delete?id='+this.id+'" class="btn-sm" id="delete_concept">Delete</a></td>';
             
            html +='</tr>';  
            })
            html +='</table>';
            $('div.result').html(html);
          }
          else{
            $('div.result').html('No Record.');
          }
      }
    });
	}
}
function remove_tags(html)
  {
       return html.replace(/<(?:.|\n)*?>/gm, '');
  }
function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

</script>
<?php else: 
header('location:'.site_url().'surveys/mysurveys');
?>
<?php endif; ?>
</body>
</html>
