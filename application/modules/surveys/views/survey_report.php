<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Internal Audit portal - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm.submit
{
    float: right;
}

.territory_click,.outlet_click,.category_click,.total_click,.territory_category_click,.total_category_click{
    cursor: pointer;
}
#ddReporttype {
  width: 177px !important;
}
.introtable{
  margin-bottom: 16px;
}
.no-result-block{
  margin-top: 54px;
  margin-bottom: 0px;
}
.territory{
  display:none;
}
.report_back{
  position: relative;
  top: -4px;
  margin-right: 14px;
}
#report span.qTip div.tooltip {
width: auto;
height:auto;
}
</style>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); 
  if((isset($permission_roles['AM']) && !empty($permission_roles['AM']) && $myprofile['role_id'] == $permission_roles['AM']) || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA'])|| (isset($permission_roles['ARV']) && !empty($permission_roles['ARV']) && $myprofile['role_id'] == $permission_roles['ARV']) || (isset($concept_manager) && !empty($concept_manager) && in_array($myprofile['id'],$concept_manager)) || (isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager))):
  ?>

	<div class="section wrapper clearfix">
 	<h2>Internal Audit portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Positive Assurance Reports</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">
	<div class="sur-holder last-child">
    <div class="checklist">
        	<?php
					$filters_data = array('search_placeholder' => 'Search for files by keyword');
					$this->load->view('partials/survey_filters', $filters_data);
				?>
<div class="result">
</div>
    <div class="loader-more">&nbsp;</div>
	</div>
</div>
	</div> <!-- container -->


    </div>

</div> <!-- section -->
<div id="report-popup"></div>
<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>
<script type="text/javascript" src="/media/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/media/js/jquery.ui.datepicker.js"></script>
<script src="/media/js/jquery.fancybox.js?v=2.1.5" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/media/js/main.js"></script> 

<script>
var outlet_id = 0;
var outlet_set = 0;
<?php if(isset($outlet_manager) && !empty($outlet_manager)):
        if(in_array($myprofile['id'],$outlet_manager)):
?>
        outlet_id = <?php print $outlet_manager_id[$myprofile['id']];?>;
        outlet_set = 1;
<?php 
        endif;
      endif;
?>
var type = 0;
var yearType = '';
var category_id = 0;
var territory = 0;
var back = 0;
var Getdata = {};
var PathA = [1,1.1,1.2,1.3];
var PathB = [2,2.1,1.2,1.3];
$(document).ready(function() {
  $('.loader-more').hide();
  /** Loading First Report By Default **/
  loadReport()
  /** Loading First Report By Default **/
  
  if($('#ddConcepts').val() != 0 && $('#ddConcepts').val() >0)
  {
	loadTerritories();
  }
  $('#clear').on( "click", function(e) {
    e.preventDefault();
    var d = new Date();
		$('#ddConcepts').val('0');
    $('#ddYear').val((d.getFullYear()+1));
	loadTerritories();
    $('#ddTerritory').val('0');
    $('#ddReporttype').val('1');
    loadReport();
  });
  $('#bt_report_filter').on('click',function(){
    if(outlet_set == 0)
    {
      outlet_id = 0;
    }
    type = 0;
    yearType = '';
    category_id = 0;
    territory = 0;
    back = 0;
    loadReport();
  });
  $('.result .territory_click').live('click',function(){
    var territory_id = $(this).attr('id');
    $('#ddTerritory').val(territory_id);
    $('#ddReporttype').val(3);
    loadReport();
  });
  $('#report_back').live('click',function(e){
    e.preventDefault();
    back = 1;
    loadReport();
  });
  $('.result .outlet_click').live('click',function(){
    var id = $(this).attr('id');
    if (id.indexOf("-") >= 0)
    {
      var rawid = id.split("-");
      outlet_id = rawid[0];
      yearType =  rawid[1];
    }
    else
    {
      outlet_id = id;
    }
    type = 1.1;
    loadReport();
  });
  $('.result .category_click').live('click',function(){
    var ids = $(this).attr('id');
    if (ids.indexOf("-") >= 0)
    {
      var rawid = ids.split("-");
      category_id =  rawid[0];
      outlet_id = rawid[1];
      yearType =  rawid[2];
    }
    else
    {
      outlet_id = ids;
    }
    type = 1.2;
    loadReport();
  });
  $('.result .total_click').live('click',function(){
    var ids = $(this).attr('id');
    if (ids.indexOf("-") >= 0)
    {
      var rawid = ids.split("-");
      outlet_id = rawid[0];
      yearType =  rawid[1];
    }
    else
    {
      outlet_id = ids;
    }
    type = 1.3;
    loadReport();
  });
  $('.result .territory_category_click,.result .total_category_click').live('click',function(){
    var ids = $(this).attr('id');
    if (ids.indexOf("-") >= 0)
    {
      var rawid = ids.split("-");
      territory = rawid[0];
      category_id =  rawid[1];
    }
    else
    {
      category_id = $(this).attr('id');
    }
    type = 2.1;
    loadReport();
  });
  $('.territory').hide();
  $('.ter-btn').live('click',function(){
    $('.territory').toggle();
  }); 
  
  $('#ddConcepts').live('change',function(){
  loadTerritories();
});

});

function loadTerritories(){
var service_url = siteurl+'surveys/fetch_territory_by_concept_ajax'    
  var Getdata = {'concept':$('#ddConcepts').val()};
  $.ajax({
    url: service_url,
    data: Getdata,
    dataType: "json",
    type: "GET",
    success: function(data){
      if(data.status === true)
      {
        var options = '<option value="0">All Territory</option>';
        $.each(data.outlets,function(index,value){
		console.log(index);
		console.log(value);
           options += '<option value="' + value.id + '">' + value.name + '</option>';
        });
        $('#ddTerritory').html(options);
        $('#ddTerritory').show();
      }
      else
      {
        $('#ddTerritory').val(0);
        $('#ddTerritory').hide();
      }
    },
  });
}
var storedUrl = '';
function loadReport()
{
    var service_url = siteurl + "surveys/report_ajax";
    var concept = $('#ddConcepts').val();
    if(territory == 0)
    {
      territory = $('#ddTerritory').val();
    }
    if(type == 0)
    {
      type = $('#ddReporttype').val();
    }
    
    var year = $('#ddYear').val();
    /** Back Feature Code **/
    if($('#ddReporttype').val() == 1)
    {
      switch(type)
      {
        case 1.1:
          backType =  PathA[0];
        break
        case 1.2:
          backType =  PathA[1];
        break;
        case 1.3:
          backType =  PathA[1];
        break;
      }
    }
    else if($('#ddReporttype').val() == 2)
    {
      switch(type)
      {
        case 2.1:
          backType = PathB[0];
        break;
        case 1.2:
          backType =  PathB[1];
        break;
        case 1.3:
          backType =  PathB[1];
        break;
      }      
    }
    if(back == 1)
    {
      type = backType;
    }
    /** Back Feature Code **/
    Getdata = {'concept': concept,'type':type,'territory':territory,'year':year,'outlet':outlet_id,'yearType':yearType,'category':category_id,'outlet_set':outlet_set};
    $.ajax({
      beforeSend: function() {
        $('.loader-more').show();
      },
      complete: function() {
        $('.loader-more').hide();
      },
      url: service_url,
      data: Getdata,
      async: false,
      dataType: "json",
      type: "GET",
      success: function(data){
        if(data.status == true)
        {
            var html = '';
            var exportUrl = service_url+'?concept='+concept+'&type='+type+'&territory='+territory+'&year='+year+'&outlet='+outlet_id+'&yearType='+yearType+'&category='+category_id+'&export=1';
            var fullscreen = '';
            var backButton = '';
            if(type === 1.2 || type === 1.3)
            {
              fullscreen = ' | <a href="#" title="Full screen" class="fullscreen"><img src="/media/images/full-screen.jpg" alt="Full screen"></a>';
            }
            if(type != 1 && type!= 2)
            {
              backButton = '<a href="#" class="report_back" id="report_back"><input name="bt_report_filter" id="bt_report_filter" class="btn-sm" value="&lt;&lt; Back" type="button"></a>';
            }
            html+= '<div style="float:right; margin:20px 0;">'+backButton+'<a href="'+exportUrl+'&exportType=excel" title="Excel"><img src="/media/images/excel-ico.png" alt="Excel"></a> | <a href="'+exportUrl+'&exportType=pdf" title="PDF"  target="_blank"><img src="/media/images/pdf-ico.png" alt="PDF"></a>'+fullscreen+'</div>';
            html+='<div id="report">'+GenerateReport(data,type)+'</div>';
            html+= '<div style="float:right; margin:20px 0;">'+backButton+'<a href="'+exportUrl+'&exportType=excel" title="Excel"><img src="/media/images/excel-ico.png" alt="Excel"></a> | <a href="'+exportUrl+'&exportType=pdf" title="PDF"  target="_blank"><img src="/media/images/pdf-ico.png" alt="PDF"></a>'+fullscreen+'</div>';
            $('.result').html(html);
        }
        else if(data.status == false)
        {
          $('.result').html('<div class="no-result-block d-n" style="display: block;">No records found.</div>');
        }
      }
    });
    // Reseting the variable.
    /*outlet_id = 0;  
    type = 0;
    category_id = 0
    yearType = '';*/
    back = 0;
}
function GenerateReport(data,type)
{
    var html = '';
    var htmlTable = '';
    if(type == 1)
    {
      
      var total_controls = 0;
      var total_no = 0;
      var i = 0
      console.log(data);
      var annual_non_compliance_flag = 0;
      var mid_non_compliance_flag = 0; 
      $.each(data.report, function(a,report) {
       
        i++;
        var mid_non_compliance = this.mid_percentage;
        var annual_non_compliance = this.annual_percentage;
          htmlTable +='<tr>';
          htmlTable +='<td>'+i+'</td>';
          htmlTable +='<td class="first-child outlet_click" id="'+this.id+'" style="text-align:left!important;">'+report.outlet_name+'</td>';
          if(mid_non_compliance)
          {
            if(mid_non_compliance > 30){
            htmlTable +='<td class="red outlet_click" id="'+this.id+'-mid"><img src="/media/images/high-ico.png">High</td>';
            }
            if(mid_non_compliance >= 15 && mid_non_compliance <= 30){
            htmlTable +='<td class="yellow outlet_click" id="'+this.id+'-mid"><img src="/media/images/med-ico.png">Medium</td>';
            }
            if(mid_non_compliance < 15){
            htmlTable +='<td class="green outlet_click" id="'+this.id+'-mid"><img src="/media/images/low-ico.png">Low</td>';
            }
            mid_non_compliance_flag = 1;
          }
          
          
          if(annual_non_compliance)
          {
            if(annual_non_compliance > 30){
            htmlTable +='<td class="red outlet_click" id="'+this.id+'-annual"><img src="/media/images/high-ico.png">High</td>';
            }
            if(annual_non_compliance >= 15 && annual_non_compliance <= 30){
            htmlTable +='<td class="yellow outlet_click" id="'+this.id+'-annual"><img src="/media/images/med-ico.png">Medium</td>';
            }
            if(annual_non_compliance < 15){
            htmlTable +='<td class="green outlet_click" id="'+this.id+'-annual"><img src="/media/images/low-ico.png">Low</td>';
            }
            annual_non_compliance_flag = 1;
          }
          
          htmlTable +='</tr>';
      });
      html='<h3>Risk Exposure By Outlet</h3>';
      html +='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
      html +='<tr>';
      html +='<th width="10%">Sr.no</th>';
      html +='<th width="40%" style="text-align:left!important;padding-left: 10px;">Outlet Reference</th>';
      if(mid_non_compliance_flag == 1)
      {
        html +='<th width="15%">First Review</th>';
      }
      if(annual_non_compliance_flag == 1)
      {
       // html +='<th width="15%">Second Review</th>'; 25 Aug 2014
		html +='<th width="15%">Review</th>';
      }
      html +='</tr>';
      html += htmlTable;
      html +='</table>';
      
    }
    else if(type == 1.1)
    {
      html +='<h3>Outlet Risk Exposure</h3>';
      
      html +='<table class="introtable" border="0" cellpadding="1" cellspacing="1" width="100%">';
      html +='<tbody>';
      html +='<tr>';
      html +='<td width="20%">Outlet Name</td>';
      html +='<td>'+data.report[0].outlet_name+'</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td width="20%">Assessment Date</td>';
      html +='<td>'+data.report[0].assessment_date+'</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Outlet Manager</td>';
      html +='<td>'+data.report[0].manager_name+'</td>';
      html +='</tr>';
      html +='</tbody>';
      html +='</table>';
      
      html +='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
      html +='<tr>';
      html +='<th width="7%">Sr.no</th>';
      html +='<th width="30%">Outlet Reference</th>';
      html +='<th width="15%">Risk Rating</th>';
      html +='<th width="15%">Expected Controls</th>';
      html +='<th width="15%">Control Gaps</th>';
      html +='<th width="18%">Non Compliance %</th>';
      html +='</tr>';
      var total_controls = 0;
      var total_no = 0;
      var i = 0
      $.each(data.report, function() {
        i++;
        var non_compliance = (parseInt(this.total_no)/parseInt(this.total_controls)) * 100;
        total_controls = (parseInt(total_controls)+parseInt(this.total_controls));
        total_no = (parseInt(total_no)+parseInt(this.total_no));
          html +='<tr>';
          html +='<td>'+i+'</td>';
          html +='<td class="first-child category_click" id="'+this.category_id+'-'+this.id+'-'+yearType+'" style="text-align:left!important;">'+this.qustion_category+'</td>';
            if(non_compliance > 30){
            html +='<td class="red"><img src="/media/images/high-ico.png">High</td>';
            }
            if(non_compliance >= 15 && non_compliance <= 30){
            html +='<td class="yellow"><img src="/media/images/med-ico.png">Medium</td>';
            }
            if(non_compliance < 15){
            html +='<td class="green"><img src="/media/images/low-ico.png">Low</td>';
            }
            html +='<td>'+this.total_controls+'</td>';
            html +='<td>'+this.total_no+'</td>';
            html +='<td>'+Math.round(non_compliance)+'</td>';
          html +='</tr>';
      });
      var non_compliance_total = (parseInt(total_no)/parseInt(total_controls)) * 100;
      html +='<tr class="res">';
      html +='<td class="first-child total_click" colspan="2" id="'+data.report[0].id+'-'+yearType+'">Total Risk Exposure</td>';
      if(non_compliance_total > 30){
      html +='<td class="red"><img src="/media/images/high-ico.png">High</td>';
      }
      if(non_compliance_total >= 15 && non_compliance_total <= 30){
      html +='<td class="yellow"><img src="/media/images/med-ico.png">Medium</td>';
      }
      if(non_compliance_total < 15){
      html +='<td class="green"><img src="/media/images/low-ico.png">Low</td>';
      }
      html +='<td>'+total_controls+'</td>';
      html +='<td>'+total_no+'</td>';
      html +='<td>'+Math.round(non_compliance_total)+'%</td>';
      html +='</tr>';
      html +='</table>';
    }
    if(type == 1.2)
    {
      var outletname = '';
      if(data.report.info.hasOwnProperty('outlet_name'))
      {
        outletname = data.report.info.outlet_name+' - ';
      }
      html='<h3>Detailed Report</h3>';
      
      html +='<table border="0" cellpadding="0" cellspacing="0" width="100%" class="introtable Outlet-main">';
      html +='<tbody>';
      html +='<tr>';
      html +='<td width="25%">Outlet Name</th>';
      html +='<td width="46%">'+data.report.info.outlet_name+'</td>';
      html +='<td class="comp-head">Non Compliance %</th>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Manager Name</td>';
      html +='<td>'+data.report.info.manager_name+'</td>';
      var css = '';
      if(data.report.info.risk_exposure > 30){
        css = 'style="color:#C00000"';
      }
      if(data.report.info.risk_exposure >= 15 && data.report.info.risk_exposure <= 30){
        css = 'style="color:#584707"';
      }
      if(data.report.info.risk_exposure < 15){
        css = 'style="color:#467345"';
      }      
      html +='<td rowspan="4" class="comp-res" '+css+'>'+data.report.info.risk_exposure+'%</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Assessment Date</td>';
      html +='<td>'+data.report.info.assessment_date+'</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Business Risk / Impact</td>';
      html +='<td>'+data.report.info.risk+'</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Expected Controls</td>';
      html +='<td>'+data.report.info.expected_control+'</td>';
      html +='</tr>';
      html +='</table>';
      
      
      html +='<br />';
      html +='<br />';
      html +='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
      html +='<tr>';
      html +='<th colspan="3">'+outletname+' Contol Gaps</th>';
      html +='<th colspan="3" class="mcp">Management Action Plans</th>';
      html +='</tr>';
      html +='<tr>';
      html +='<th width="20%">Risk Description / Root Cause</th>';
      html +='<th width="30%" style="padding-left: 4px;">Self Assessment Question</th>';
      html +='<th width="10%">Responses</th>';
      html +='<th class="mcp" width="15%">Action Plan</th>';
      html +='<th class="mcp" width="15%">Responsibility</th>';
      html +='<th class="mcp" width="10%">Timeline</th>';
      html +='</tr>';
      var ftr = false;
      var str = false;
      
      $.each(data.report.records,function(a,report) {
        html +='<tr>';
        var risk_count = (report.risk_count == 1)?0:report.risk_count;
        $.each(report.risk,function(b,risk) {
          if(risk.question_count <= 1)
          {
            html +='<td>'+risk.risk_description+'</td>';
            str = false;
          }
          else
          {
            html +='<td rowspan="'+(risk.question_count+1)+'">'+risk.risk_description+'</td>';
            html +='</tr>';
            str = true;
          }
          $.each(risk.questions,function(b,questions) {
              if(str == true)
              {
                html +='<tr>';
              }
              if(questions.response != "" && (questions.response == 'No' || questions.response == 'Yes')){
                var responses = questions.response;
              }
              else
              {
                var responses = 'N/A <span class="qTip" title="'+questions.na_reason+'">?</span>';
              }
              var dateTimeline = '';
              if(questions.timeline == '1970-01-01' || questions.timeline == '-0001-11-30')
              {
               dateTimeline = ' - ';
              }
              else
              {
               dateTimeline = questions.timeline;
              }
              html +='<td style="text-align:left;">'+questions.questions+'</td>';
              html +='<td>'+responses+'</td>';
              html +='<td class="mcp" style="text-align:left;"><div style="width: 225px;word-wrap: break-word;">'+questions.actionplan+'</div></td>';
              html +='<td class="mcp">'+questions.responsible+'</td>';
              html +='<td class="mcp">'+dateTimeline+'</td>';
          });
          html +='</tr>';
        });
        html +='</tr>';
      });
      html +='</table>';
    }
    else if(type == 1.3)
    {
      var outletname = '';
      if(data.report.hasOwnProperty('outlet_name'))
      {
        outletname = data.report.outlet_name+' - ';
      }
      html='<h3>Complete RCM (Risk Control Matrix)</h3>';
      
      html +='<table border="0" cellpadding="0" cellspacing="0" width="100%" class="introtable Outlet-main">';
      html +='<tbody>';
      html +='<tr>';
      html +='<td width="25%">Outlet Name</th>';
      html +='<td width="46%">'+data.report.info.outlet_name+'</td>';
      html +='<td class="comp-head">Non Compliance %</th>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Manager Name</td>';
      html +='<td>'+data.report.info.manager_name+'</td>';
      html +='<td rowspan="3" class="comp-res">'+data.report.info.risk_exposure+'%</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Date of Assessment</td>';
      html +='<td>'+data.report.info.assessment_date+'</td>';
      html +='</tr>';
      html +='<tr>';
      html +='<td>Expected Controls</td>';
      html +='<td>'+data.report.info.expected_control+'</td>';
      html +='</tr>';
      html +='</tbody>';
      html +='</table>';
      html +='<br />';
      html +='<br />';
      html +='<table border="0" cellpadding="0" cellspacing="0" width="100%" class="rcm">';
      html +='<tbody>';
      html +='<tr>';
      html +='<th colspan="4">'+outletname+' Risk Control Matrix</th>';
      html +='<th colspan="3" class="mcp">Management Action Plans</th>';
      html +='</tr>';
      html +='<tr>';
      html +='<th width="15%">Business Risk / Impact</th>';
      html +='<th width="20%">Risk Description / Root Cause</th>';
      html +='<th width="13%">Self Assessment Question</th>';
      html +='<th width="12%">Responses</th>';
      
      html +='<th class="mcp">Action Plan</th>';
      html +='<th class="mcp">Responsibility</th>';
      html +='<th class="mcp">Timeline</th>';
      html +='</tr>';
      var ftr = false;
      var str = false;
      
      $.each(data.report.records,function(a,report) {
        html +='<tr>';
        var risk_count = (report.risk_count == 1)?0:report.risk_count;
        if(report.risk_count <= 1)
        {
          html +='<td>'+report.risk_impact+'</td>';
          ftr = false;
        }
        else
        {
          html +='<td rowspan="'+(report.risk_count+1)+'">'+report.risk_impact+'</td>';
          html +='</tr>';
          ftr = true;
        }
        $.each(report.risk,function(b,risk) {
          if(ftr === true)
          {
            html +='<tr>';
          }
          if(risk.question_count <= 1)
          {
            html +='<td>'+risk.risk_description+'</td>';
            str = false;
          }
          else
          {
            html +='<td rowspan="'+(risk.question_count+1)+'">'+risk.risk_description+'</td>';
            html +='</tr>';
            str = true;
          }
          $.each(risk.questions,function(b,questions) {
              if(str == true)
              {
                html +='<tr>';
              }
              var dateTimeline = '';
              if(questions.timeline == '1970-01-01' || questions.timeline == '-0001-11-30')
              {
               dateTimeline = ' - ';
              }
              else
              {
               dateTimeline = questions.timeline;
              }
              html +='<td>'+questions.questions+'</td>';
             // html +='<td>'+questions.response+'</td>';
                   if(questions.response != "" && (questions.response == 'No' || questions.response == 'Yes')){
                var responses = questions.response;
              }
              else
              {
                var responses = 'N/A <span class="qTip" title="'+questions.na_reason+'">?</span>';
              }
              html +='<td>'+responses+'</td>';
              html +='<td class="mcp">'+questions.actionplan+'</td>';
              html +='<td class="mcp">'+questions.responsible+'</td>';
              html +='<td class="mcp">'+dateTimeline+'</td>';
          });
          html +='</tr>';
        });
        html +='</tr>';
      });
      html +='</tbody>';
      html +='</table>';
      yearType = '';
    }
    else if(type == 2)
    {
      html='<h3>Risk Exposure by Category</h3>';
      html +='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
      html +='<tr>';
	  html +='<th width="10%">Sr.no</th>';
      html +='<th width="40%" style="text-align:left!important;padding-left: 10px;">Risk reference</th>';
      html +='<th width="15%"><div class="ter-head">';
      html +='<a class="ter-btn"><img src="/media/images/territory.jpg" alt=""></a>';
      html +='Total</div>';
      html +='</th>';
      $.each(data.territory,function(j,territory) {
	   //  ii++;
	  //html +='<td>'+ii+'</td>';
      html +='<th width="15%" class="territory">'+territory+'</th>';
      });
      //html +='<th class="arrow-slider" width="1px">&nbsp;</th>';
      html +='</tr>';
      var non_compliance = [];
      var category_id = [];
	   
      $.each(data.report,function(i,report) {
        var total_controls = 0;
        var total_no = 0;
        $.each(data.territory,function(j,territory) {
          if(report[j])
          {
            total_controls = (parseInt(total_controls)+parseInt(report[j].total_controls));
            total_no = (parseInt(total_no)+parseInt(report[j].total_no));
            non_compliance[i] = (parseInt(total_no)/parseInt(total_controls)) * 100;
            category_id[i] =  report[j].category_id;
          }
        });
      });
      
var ii = 0
      $.each(data.report,function(i,report) {
	    ii++;
          html +='<tr>';
		  html +='<td>'+ii+'</td>';
          html +='<td class="first-child" style="text-align:left!important;">'+i+'</td>';
          if(non_compliance[i] > 30){
          html +='<td class="red total_category_click" id="'+category_id[i]+'"><img src="/media/images/high-ico.png">High</td>';
          }
          if(non_compliance[i] >= 15 && non_compliance[i] <= 30){
          html +='<td class="yellow total_category_click" id="'+category_id[i]+'"><img src="/media/images/med-ico.png">Medium</td>';
          }
          if(non_compliance[i] < 15){
          html +='<td class="green total_category_click" id="'+category_id[i]+'"><img src="/media/images/low-ico.png">Low</td>';
          }
          $.each(data.territory,function(j,territory) {
            if(report[j])
            {
              if(report.hasOwnProperty(j))
              {
                if(report[j].percentage > 30){
                html +='<td class="red territory_category_click territory" id="'+report[j].territory_id+'-'+report[j].category_id+'"><img src="/media/images/high-ico.png">High</td>';
                }
                if(report[j].percentage >= 15 && report[j].percentage <= 30){
                html +='<td class="yellow territory_category_click territory" id="'+report[j].territory_id+'-'+report[j].category_id+'"><img src="/media/images/med-ico.png">Medium</td>';
                }
                if(report[j].percentage < 15){
                html +='<td class="green territory_category_click territory" id="'+report[j].territory_id+'-'+report[j].category_id+'"><img src="/media/images/low-ico.png">Low</td>';
                }
              }
              else
              {
                html +='<td class="territory">N/A</td>';
              }
            }
            else
            {
              html +='<td class="territory">N/A</td>';
            }
          });
          //html +='<th class="arrow-slider">&nbsp;</th>';
          html +='</tr>';
      });
      html +='<tr>';
      html +='</tr>';
      html +='</table>';
    }
    else if(type == 2.1)
    {
      if(data.report[0].all == 1)
      {
        html='<h3>Category Risk Exposure for all territories</h3>';
      }
      else
      {
        html='<h3>Category Risk Exposure for '+data.report[0].territory_name+'</h3>';
      }
      
      html +='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
      html +='<tr>';
      html +='<th colspan="3">'+data.report[0].title+'</th>';
      html +='</tr>';
      html +='<tr>';
      html +='<th width="10%">Sr.no</th>';
      html +='<th width="40%" style="text-align:left!important;padding-left: 10px;">Outlet Reference</th>';
      html +='<th width="15%">Risk Rating</th>';
      html +='</tr>';
      var total_controls = 0;
      var total_no = 0;
      var i = 0
      $.each(data.report, function(a,report) {
        i++;
          html +='<tr>';
          html +='<td>'+i+'</td>';
          html +='<td class="first-child category_click" id="'+this.category_id+'-'+this.id+'" style="text-align:left!important;">'+this.outlet_name+'</td>';
          if(this.percentage > 30){
          html +='<td class="red"><img src="/media/images/high-ico.png">High</td>';
          }
          if(this.percentage >= 15 && this.percentage <= 30){
          html +='<td class="yellow"><img src="/media/images/med-ico.png">Medium</td>';
          }
          if(this.percentage < 15){
          html +='<td class="green"><img src="/media/images/low-ico.png">Low</td>';
          }
          html +='</tr>';
      });
      html +='</table>';
      territory = 0;
    }
    else if(type == 3)
    {
      html='<h3>Outlet Wise Summary</h3>';
      html +='<table width="100%" border="0" cellspacing="0" cellpadding="0">';
      html +='<tr>';
      html +='<th>Risk Impact</th>';
      $.each(data.category,function(j,category) {
      html +='<th>'+category+'</th>';
      });
      html +='<th>Total Expected Controls</th>';
      html +='<th>Control Gaps Noted</th>';
      html +='</tr>';
      
      var total_controls = 0;
      var total_no = 0;
      $.each(data.report,function(i,report) {
          html +='<tr>';
          html +='<td class="first-child outlet_click" id="'+report.outlet_id+'" style="text-align:left!important;">'+i+'</td>';
          $.each(data.category,function(j,category) {
            if(report.record.hasOwnProperty(j))
            {
              if(report.record[j].percentage > 30){
              html +='<td class="red"><img src="/media/images/high-ico.png">High</td>';
              }
              if(report.record[j].percentage >= 15 && report.record[j].percentage <= 30){
              html +='<td class="yellow"><img src="/media/images/med-ico.png">Medium</td>';
              }
              if(report.record[j].percentage < 15){
              html +='<td class="green"><img src="/media/images/low-ico.png">Low</td>';
              }
            }
            else
            {
              html +='<td>N/A</td>';
            }
          });
          html +='<td>'+report.total_controls+'</td>';
          html +='<td>'+report.total_no+'</td>';
          html +='</tr>';
      });
      html +='</table>';
    }
    
    
    return html;
}
</script>
<?php else: 
header('location:'.site_url().'surveys/mysurveys');
?>
<?php endif; ?>
</body>
</html>