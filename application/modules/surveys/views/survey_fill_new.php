<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Concepts - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm.submit
{
    float: right;
}
</style>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
 	<h2>Internal Audit portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Positive Assurance Questionnaire</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">
        
        <form action="<?php echo site_url(); ?>/surveys/submit_survey" class="frm-sur" method="POST" id="survey_submit">
        <div style="display:block" id="message_place" class="msg"></div>
        <input type="hidden" name="survey_id" value="<?php echo $survey->id; ?>" />
<?php foreach($category as $question_category) { ?>
<div class="sur-holder last-child">

    <div class="sur-structure ">
            <div class="row">
                <label for="title">Potential Risk Impact</label>
                <span><?php echo $question_category['category']; ?></span>
            </div>
            <div class="row">
                <label for="title">Risk Description</label>
                <span><?php echo $question_category['subcategory_name']; ?></span>
            </div>
            <div class="row" style="display:none">
                <label for="title">Expected Controls</label>
                <span>All games to played trough EMBED swipers</span>
            </div>
    </div>
    
    <div class="checklist">
        <h3>Positive Assurance Questionnaire</h3>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="10%">Sr No</th>
            <th width="60%">Question</th>
            <th width="30%">Answers</th>
          </tr>
          <?php
          $i = 1;
          #echo "<pre>".print_r($draftanswer,"\n")."</pre>";
          foreach($question_category['questions'] as $question) {
          ?>
          <tr>
            <td class="first-child"><?php print $i;?></td>
            <td><?php echo $question->title; ?><?php if(isset($question->question_tips) && !empty($question->question_tips)): ?><span class="qTip" title="<?php print $question->question_tips?>">?</span><?php endif;?></td>
            <td class="third-child" data-qid="<?php echo $question->id; ?>">
              
              <input type="radio" id="yes1" value="Yes" name="question[<?php echo $question->id; ?>]" class="remove_ap" <?php if((isset($draftanswer[$question->id]->answer) && !empty($draftanswer[$question->id]->answer)) && (trim($draftanswer[$question->id]->answer) === 'Yes')){ print 'checked="checked"';}?> /><label for="yes1">Yes</label>
              <input type="radio" id="no1" value="No" name="question[<?php echo $question->id; ?>]" class="no" <?php if((isset($draftanswer[$question->id]->answer) && !empty($draftanswer[$question->id]->answer)) && (trim($draftanswer[$question->id]->answer) === 'No')){ print 'checked="checked"';}?> /><label for="no1">No</label>
              <input type="radio" id="na1" value="N/A" name="question[<?php echo $question->id; ?>]" class="na" <?php if((isset($draftanswer[$question->id]->answer) && !empty($draftanswer[$question->id]->answer)) && (trim($draftanswer[$question->id]->answer) === 'N/A')){ print 'checked="checked"';}?> /><label for="na1">N / A</label>
              <?php if((isset($draftanswer[$question->id]->answer) && !empty($draftanswer[$question->id]->answer)) && (trim($draftanswer[$question->id]->answer) === 'No')): ?>
              <div class="ans-no"><div class="textC plan"><input type="text" id="action" name="action_plan[<?php echo $question->id; ?>]" placeholder="Action Plan" value="<?php print $draftanswer[$question->id]->actionplan;?>" /></div><div class="textC resp"><input type="text" name="responsibility[<?php echo $question->id; ?>]" placeholder="Responsibility" id="responsibility" value="<?php print $draftanswer[$question->id]->responsibility;?>"  /></div><div class="textC date"><input type="text" name="action_plan_date[<?php echo $question->id; ?>]" placeholder="Date" class="cal-date timeline"  value="<?php print date('Y-m-d',strtotime($draftanswer[$question->id]->actionplan_dttm));?>"  /></div></div>
              <?php endif; ?>
              <?php if((isset($draftanswer[$question->id]->answer) && !empty($draftanswer[$question->id]->answer)) && (trim($draftanswer[$question->id]->answer) === 'N/A')): ?>
                <div class="ans-na"><div class="textC plan"><input type="text" id="reason" name="na_reason[<?php echo $question->id; ?>]" placeholder="Reason" value="<?php print $draftanswer[$question->id]->na_reason;?>" /></div></div>
              <?php endif; ?>
            </td>
          </tr>
          <?php
            $i++;
           } ?>
        </table>
    </div>
</div>
<?php } ?>
<?php if((isset($permission_roles['AM']) && !empty($permission_roles['AM']) && $myprofile['role_id'] != $permission_roles['AM']) && (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] != $permission_roles['SA'])):?>
<input type="submit" class="btn-sm" value="Submit" />
<?php endif; ?> 
</form>


	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>
<script type="text/javascript" src="/media/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/media/js/jquery.ui.datepicker.js"></script>

<script>

    var questions = [];
    var survey_id = <?php echo isset($survey) ? $survey->id : 'null'; ?>;

    $(window).bind("load", function() { 
        var timeout = setTimeout(function() {
        $("img.lazy").trigger("sporty")}, 5000);
        $("#Assign").tokenInput("http://devnet.landmarkgroup.com/manage/auto_group", {
                theme: "facebook",
                zindex: 99999
                /*,prePopulate: [
                    
                        {id: 123, name: "Slurms MacKenzie"},
                        {id: 555, name: "Bob Hoskins"},
                        {id: 9000, name: "Kriss Akabusi"}
                    ]
                [{"id":"29","name":"Cricket"}]*/
            });
        });

    $(window).load(function(){
        $('input[type=submit]').unbind( "click" );
        $('#survey_submit').submit(function(){
          var names = [];
          $('input[type="radio"]').each(function() {
            names[$(this).attr('name')] = true;
          });
          var go = true;
          for (name in names) {
            var radio_buttons = $("input[name='" + name + "']");
            if (radio_buttons.filter(':checked').length == 0) {
              go = false;
            } 
          }
          if(go)
          {
            $('#message_place').show().html('Thank you for answering all the questionnaire.').addClass('success');
            return true;
          }
          else
          {
            $('#message_place').show().html('Please answer all the questionnaire.').addClass('error');
            return false;  
          }
        });
        
        $('.no').change(function(){
            var qid = $(this).closest('td').data('qid');


            var plan = '<div class="ans-no"><div class="textC plan"><input type="text" id="action" name="action_plan[' + qid + ']" placeholder="Action Plan"></div><div class="textC resp"><input type="text" name="responsibility[' + qid + ']" placeholder="Responsibility" id="responsibility"></div><div class="textC date"><input type="text" name="action_plan_date[' + qid + ']" placeholder="Date" class="cal-date timeline"></div></div>';
            $(plan).insertAfter($(this).next().next().next()).fadeIn();
            bindDatePicker();
        });
        $('.na').change(function(){
          var qid = $(this).closest('td').data('qid');
          var plan = '<div class="ans-na"><div class="textC plan"><input type="text" id="reason" name="na_reason[' + qid + ']" placeholder="Reason"></div></div>';
          $(plan).insertAfter($(this).next()).fadeIn();
          bindDatePicker();
        });
        $('.remove_ap,.na').change(function(){
            $(this).closest('td').find('div.ans-no').remove();
            saveAnswerData(); // Saving Unsaved answers
            bindDatePicker();
        });
        $('.remove_ap,.no').change(function(){
          $(this).closest('td').find('div.ans-na').remove();
          saveAnswerData(); // Saving Unsaved answers
          bindDatePicker();
        });
        $('#action,#responsibility,.timeline,#reason').live('blur',function(){
          saveAnswerData(); // Saving Unsaved answers
          bindDatePicker();
        });
        
    }); 

    function saveAnswerData() {
      $.ajax({
          beforeSend: function() {
              //inactive_form();
          },
          complete: function() {
      
          },
          type    : 'POST',
          cache   : false,
          async   : false,
          url     : siteurl+'surveys/submit_survey_ajax',
          data    : $('#survey_submit').serialize(),
          dataType: "json",
          success : function(response) {
              /*if(!survey_id)
              {
                  //creating a survey
                  if(response.status == 'success')
                  {
                      window.location.href = siteurl+'surveys/edit/' + response.id + '/';
                  }
              } else {
                  loadQuestions($('#subcategory').val());
              }
              $('#message_place').show().html('Survey Saved Successfully.').addClass('success').delay( 1000 ).hide( 'slow' );*/
          },
      });
    }
    function bindDatePicker() {
        $( ".cal-date" ).datepicker({
            buttonImage: "/media/images/date-picker.png",
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd',
            minDate: new Date(),
        });
    }
    

    

</script>
</body>
</html>