<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Concepts - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm
{
  float: right;
  margin-right: 31px;
  margin-bottom: 14px;
  margin-top: -15px;
}

</style>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
 	<h2>Internal Audit portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Self Assurance Questionnaire</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">

        <div class="result">
    <h3>Summary</h3>
    <?php 
    $postSuveryid = '';
    $postSuveryid = $this->input->post('survey_id');
    if(isset($postSuveryid) && !empty($postSuveryid)) :?>
    <div style="display:block" id="message_place" class="msg success">Questionnaire submitted</div>
    <?php endif;?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="40%">Risk Impact</th>
    <th width="15%">Risk Exposure</th>
    <th width="15%">Applicable Controls</th>
    <th width="15%">Control Gaps</th>
    <th width="15%">Non Compliance %</th>
  </tr>
  <?php 
    $total_controls = 0;
    $total_no = 0;
    foreach($results as $result) { 
    $non_compliance = ($result->total_no/$result->total_controls) * 100;
    $total_controls += $result->total_controls;
    $total_no += $result->total_no;
    ?>
  <tr>
    <td class="first-child"><?php echo $result->qustion_category; ?></td>
    <?php if($non_compliance > 30): ?>
    <td class="red"><img src="/media/images/high-ico.png" />High</td>
    <?php endif; ?>
    <?php if($non_compliance >= 15 && $non_compliance <= 30): ?>
    <td class="yellow"><img src="/media/images/med-ico.png" />Medium</td>
    <?php endif; ?>
    <?php if($non_compliance < 15): ?>
    <td class="green"><img src="/media/images/low-ico.png" />Low</td>
    <?php endif; ?>
    <td><?php echo $result->total_controls; ?></td>
    <td><?php echo $result->total_no; ?></td>
    <td><?php echo round($non_compliance); ?></td>
  </tr>
  <?php } ?>
  <tr class="res">
    <td class="first-child">Total</td>
    <td>&nbsp;</td>
    <td><?php echo $total_controls; ?></td>
    <td><?php echo $total_no; ?></td>
    <td>&nbsp;</td>
  </tr>
</table>
    </div>
    <?php if(isset($questions) && !empty($questions)):?>
    <form action="<?php echo site_url(); ?>surveys/submit_survey?survey_id=<?php print $survey_id; ?>" class="frm-sur-result" method="POST">
      <input type="hidden" name="survey_id" value="<?php print $survey_id; ?>" />
      <div class="action-plan">
          <h3>Management Action Plan</h3>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th></th>
              <th>Action Plan</th>
              <th>Responsibility</th>
              <th style="text-align: left;">Date</th>
            </tr>
            <?php foreach($questions as $result): ?>
            <tr>
              <td width="40%"><?php print $result->questions; ?></td>
              <td colspan="3">
                  <?php 
                  if(isset($survey_id) && !empty($survey_id)):?>
                  <div class="plan-check">
                    <input type="checkbox" value="1" name="yes[<?php print $result->id; ?>]" class="yes_checkbox" <?php if($result->answer == 'Yes'):?> checked="checked" <?php endif;?>/>
                  </div>
                  <?php endif; ?>
                  <div class="textC plan">
                          <input type="hidden" value="<?php print $result->id; ?>" name="answers[]" />
                          <input type="text" placeholder="Action Plan" name="action_plan[<?php print $result->id; ?>]" value="<?php print $result->actionplan;?>" >
                  </div>
                  <div class="textC resp">
                          <input type="text" placeholder="Responsibility" value="<?php print $result->responsibility;?>" name="responsibility[<?php print $result->id; ?>]" />
                  </div>
                  <?php
                  $resultDate = '';                  
                  if(date("Y-m-d",strtotime($result->actionplan_dttm)) != '1970-01-01' && date("Y-m-d",strtotime($result->actionplan_dttm)) != '-0001-11-30')
                  {
                    $resultDate = date("Y-m-d",strtotime($result->actionplan_dttm));
                  }
                  ?>
                  <div class="textC date">
                      <input type="text" placeholder="Date" class="cal-date" value="<?php print $resultDate;?>" name="action_plan_date[<?php print $result->id; ?>]"/>
                  </div></td>
               </td>
            </tr>
            <?php endforeach; ?>
          </table>
      </div>
      <input type="submit" class="btn-sm" value="Submit"> 
    </form>
    <?php endif;?>


	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>
<script type="text/javascript" src="/media/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/media/js/jquery.ui.datepicker.js"></script>

<script>

    var questions = [];
    var survey_id = <?php echo isset($survey) ? $survey->id : 'null'; ?>;

    $(window).bind("load", function() { 
        var timeout = setTimeout(function() {
        $("img.lazy").trigger("sporty")}, 5000);
        $("#Assign").tokenInput("http://devnet.landmarkgroup.com/manage/auto_group", {
                theme: "facebook",
                zindex: 99999
                /*,prePopulate: [
                    
                        {id: 123, name: "Slurms MacKenzie"},
                        {id: 555, name: "Bob Hoskins"},
                        {id: 9000, name: "Kriss Akabusi"}
                    ]
                [{"id":"29","name":"Cricket"}]*/
            });
        });

    $(window).load(function(){
        $('input[type=submit]').unbind( "click" );
        bindDatePicker();
    }); 


    function bindDatePicker() {
        $( ".cal-date" ).datepicker({
            buttonImage: "/media/images/date-picker.png",
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd',
            minDate: new Date()
        });
    }
    

    

</script>
</body>
</html>