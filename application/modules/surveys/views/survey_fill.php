<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Concepts - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
 	<h2>Self compliance portal</h2>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <iframe src="<?php echo site_url(); ?>/formcraft/form.php?preview=true&id=<?php echo $survey_id; ?>" width="100%" height="900" style="border:0px"></iframe>

    <?php 
    /*
    formcraft(1); 
    */
    ?>

	<!-- <div class="loader-more clearfix">&nbsp;</div> -->


	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>


</body>
</html>