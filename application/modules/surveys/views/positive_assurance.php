<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Internal Audit Portal - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.survey .concept-box {
    width: 300px;
    height: 100px;
    margin-right: 25px;
}
.portal-main ul {
margin: 0 auto;
width: 70%;
}
</style>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".inline").colorbox({inline:true, width:"50%"});
        });
    </script>

</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
	<h2>Internal Audit portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Positive Assurance</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="portal-main clearfix">
        <h1>Positive Assurance</h1>
        <?php if(($myprofile['role_id'] == $permission_roles['US'] or (isset($concept_manager) && !empty($concept_manager) && in_array($myprofile['id'],$concept_manager)) or (isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager))) && ($myprofile['role_id'] != $permission_roles['ARV'])):
        ?>
        <ul>
            <?php if(isset($concept_manager) && !empty($concept_manager) && !in_array($myprofile['id'],$concept_manager)): ?>
            <li><a href="<?php echo site_url(); ?>surveys/mysurveys/"><img  src="<?php echo site_url(); ?>media/images/concepts/questionnaire.jpg" /><div class="text">Positive Assurance Questionnaire</div></a></li>
            <?php endif;?>
            <li><a href="<?php echo site_url(); ?>surveys/report"><img  src="<?php echo site_url(); ?>media/images/concepts/positive_assurance.jpg" /><div class="text">Positive Assurance Reports</div></a></li>
            
            <li><a href="<?php echo site_url(); ?>surveys/managment_action_plan"><img  src="<?php echo site_url(); ?>media/images/concepts/managment_action_plan.jpg" /><div class="text">Management Action Plan</div></a></li>
       </ul>
        <?php endif; ?>
        <?php if(
                ($myprofile['role_id'] == $permission_roles['AM'] or
                $myprofile['role_id'] == $permission_roles['SA']) && ($myprofile['role_id'] != $permission_roles['ARV'])):
        ?>
        <ul>
          <li><a href="<?php echo site_url(); ?>surveys/create"><img  src="<?php echo site_url(); ?>media/images/concepts/create-questionaire.jpg" /><div class="text">Create Positive Assurance Questionnaire</div></a></li>
        
          <li><a href="<?php echo site_url(); ?>surveys/outlets/manage"><img  src="<?php echo site_url(); ?>media/images/concepts/outlet.jpg" /><div class="text">Manage Outlet</div></a></li>
         
          <li><a href="<?php echo site_url(); ?>surveys/concepts/manage"><img  src="<?php echo site_url(); ?>media/images/concepts/concept_management.jpg" /><div class="text">Manage Concept Manager</div></a></li>
         
          <li><a href="<?php echo site_url(); ?>surveys/mysurveys"><img  src="<?php echo site_url(); ?>media/images/concepts/questionnaire.jpg" /><div class="text">Positive Assurance Questionnaire</div></a></li>
         
          <li><a href="<?php echo site_url(); ?>surveys/report"><img  src="<?php echo site_url(); ?>media/images/concepts/positive_assurance.jpg" /><div class="text">Positive Assurance Reports</div></a></li>
      
          <li><a href="<?php echo site_url(); ?>surveys/managment_action_plan"><img src="<?php echo site_url(); ?>media/images/concepts/managment_action_plan.jpg" /><div class="text">Management Action Plan</div></a></li>
        </ul>
        <?php endif; ?>
        <?php if(
                $myprofile['role_id'] == $permission_roles['ARV']):
        ?>
        <ul>
          <li><a href="<?php echo site_url(); ?>surveys/report"><img  src="<?php echo site_url(); ?>media/images/concepts/positive_assurance.jpg" /><div class="text">Positive Assurance Reports</div></a></li>
          <li><a href="<?php echo site_url(); ?>surveys/managment_action_plan"><img src="<?php echo site_url(); ?>media/images/concepts/managment_action_plan.jpg" /><div class="text">Management Action Plan</div></a></li>
        </ul>
        <?php endif; ?>
    </div>	<!-- concept-grid -->


	<!-- <div class="loader-more clearfix">&nbsp;</div> -->


	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>


</body>
</html>