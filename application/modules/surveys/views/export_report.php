<?php
  $type =  $get['type'];
  $html = '';
  switch($type)
  {
    case '1':
      $fileName = 'Risk Exposure By Outlet';
       $html = '';
       $htmlTable = '';
      $total_controls = 0;
      $total_no = 0;
      $i = 0;
      $annual_non_compliance_flag = 0;
      $mid_non_compliance_flag = 0;

      foreach($report as $record)
      {
        $i++;
        if(array_key_exists('mid_percentage', $record))
        {
          $mid_non_compliance = $record->mid_percentage;
          $mid_non_compliance_flag = 1;
        } 
       
         if(array_key_exists('annual_percentage', $record))
        {
           $annual_non_compliance = $record->annual_percentage;
           $annual_non_compliance_flag = 1;
        } 
      
     // echo $mid_non_compliance;

     // echo $annual_non_compliance;
         $htmlTable.='<tr>';
         $htmlTable.='<td>'.$i.'</td>';
         $htmlTable.='<td class="first-child outlet_click" align="left">'.$record->outlet_name.'</td>';
         // if($mid_non_compliance > 30){ 
         //  $htmlTable.='<td class="red outlet_click">High</td>';
         // } 
         // if($mid_non_compliance >= 15 && $mid_non_compliance <= 30){
         //  $htmlTable.='<td class="yellow outlet_click">Medium</td>';
         // }
         // if($mid_non_compliance < 15){
         //  $htmlTable.='<td class="green outlet_click">Low</td>';
         // }
         // if($annual_non_compliance > 30){
         //  $htmlTable.='<td class="red outlet_click">High</td>';
         // }
         // if($annual_non_compliance >= 15 && $annual_non_compliance <= 30){
         //  $htmlTable.='<td class="yellow outlet_click">Medium</td>';
         // }
         // if($annual_non_compliance < 15){
         //  $htmlTable.='<td class="green outlet_click">Low</td>';
         // } 


          if(isset($mid_non_compliance))
          {
            if($mid_non_compliance > 30){
            $htmlTable.='<td class="red outlet_click">High</td>';
            }
            if($mid_non_compliance >= 15 && $mid_non_compliance <= 30){
            $htmlTable.='<td class="yellow outlet_click">Medium</td>';
            }
            if($mid_non_compliance < 15){
            $htmlTable.='<td class="green">Low</td>';
            }
            
         }
          
          
          if(isset($annual_non_compliance))
          {
            if($annual_non_compliance > 30){
            $htmlTable.='<td class="red outlet_click">High</td>';
            }
            if($annual_non_compliance >= 15 && $annual_non_compliance <= 30){
            $htmlTable.='<td class="yellow outlet_click">Medium</td>';
            }
            if($annual_non_compliance < 15){
            $htmlTable.='<td class="green">Low</td>';
            }
            
          }
          $htmlTable.='</tr>';
      } 
      $html.='<h3>Risk Exposure By Outlet</h3>';
      $html.='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th width="10%">Sr.no</th>';
      $html.='<th width="40%">Outlet Reference</th>';
      if($mid_non_compliance_flag == 1)
      {
        $html.='<th width="15%">First Review</th>';
      }
      if($annual_non_compliance_flag == 1)
      {
       // html +='<th width="15%">Second Review</th>'; 25 Aug 2014
        $html.='<th width="15%">Review</th>';
      }
      $html.='</tr>';
      $html.= $htmlTable;


      $html.='</table>';
    break;
    case '1.1':
      $fileName = 'Outlet Risk Exposure';
      $html.='<h3>Outlet Risk Exposure</h3>';
      $html.='<table width="30%" border="1" cellspacing="1" cellpadding="1" class="introtable">';
      $html.='<tr>';
      $html.='<td>Outlet Name</td>';
      $html.='<td>'.$report[0]->outlet_name.'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<td>Assessment Date</td>';
      $html.='<td>'.$report[0]->assessment_date.'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<td>Outlet Manager</td>';
      $html.='<td>'.$report[0]->manager_name.'</td>';
      $html.='</tr>';
      $html.='</table>';
      
  
      
      $html.='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th width="10%">Sr.no</th>';
      $html.='<th width="40%">Outlet Reference</th>';
      $html.='<th width="15%">Risk Rating</th>';
      $html.='<th width="15%">Expected Controls</th>';
      $html.='<th width="15%">Control Gaps</th>';
      $html.='<th width="15%">Non Compliance %</th>';
      $html.='</tr>';
      $total_controls = 0;
      $total_no = 0;
      $i = 0;
      foreach($report as $record)
      {
        $i++;
        $non_compliance = ((int)$record->total_no/(int)$record->total_controls) * 100;
        $total_controls = ((int)$total_controls+(int)$record->total_controls);
        $total_no = ((int)$total_no+(int)$record->total_no);
        $html.='<tr>';
        $html.='<td>'.$i.'</td>';
        $html.='<td class="first-child category_click" >'.$record->qustion_category.'</td>';
          if($non_compliance > 30){
          $html.='<td class="red">High</td>';
          }
          if($non_compliance >= 15 && $non_compliance <= 30){
          $html.='<td class="yellow">Medium</td>';
          }
          if($non_compliance < 15){
          $html.='<td class="green">Low</td>';
          }
          $html.='<td>'.$record->total_controls.'</td>';
          $html.='<td>'.$record->total_no.'</td>';
          $html.='<td>'.round($non_compliance).'</td>';
        $html.='</tr>';
      }
      $non_compliance_total = ((int)$total_no/(int)$total_controls) * 100;
      $html.='<tr class="res">';
      $html.='<td class="first-child total_click" colspan="2">Total Risk Exposure</td>';
      if($non_compliance_total > 30){
      $html.='<td class="red">High</td>';
      }
      if($non_compliance_total >= 15 && $non_compliance_total <= 30){
      $html.='<td class="yellow">Medium</td>';
      }
      if($non_compliance_total < 15){
      $html.='<td class="green">Low</td>';
      }
      $html.='<td>'.$total_controls.'</td>';
      $html.='<td>'.$total_no.'</td>';
      $html.='<td>'.round($non_compliance_total).'%</td>';
      $html.='</tr>';
      $html.='</table>';
    break;
    case '1.2':
      $fileName = 'Detailed Report';
      #echo "<pre>".print_r($report,"\n")."</pre>";
      $outletname = '';
      if(isset($report['info']['outlet_name']) && !empty($report['info']['outlet_name']))
      {
        $outletname = $report['info']['outlet_name'].' - ';
      }
      $html='<h3>Detailed Report</h3>';
      
      $html.='<table width="50%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th>Outlet Name</th>';
      $html.='<td>'.$report['info']['outlet_name'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Manager Name</th>';
      $html.='<td>'.$report['info']['manager_name'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Assessment Date</th>';
      $html.='<td>'.$report['info']['assessment_date'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Business Risk / Impact</th>';
      $html.='<td>'.$report['info']['risk'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Expected Controls</th>';
      $html.='<td>'.$report['info']['expected_control'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Non Compliance %</th>';
      $html.='<td>'.$report['info']['risk_exposure'].'%</td>';
      $html.='</tr>';
      $html.='</table>';
      $html.='<br />';
      $html.='<br />';
      $html.='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th colspan="4">'.$outletname.' Contol Gaps</th>';
      $html.='<th colspan="3">Management Action Plans</th>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Risk Description / Root Cause</th>';
      $html.='<th>Self Assessment Question</th>';
      $html.='<th>Responsibility</th>';
      $html.='<th>Responses</th>';
      
      $html.='<th>Action Plan</th>';
      $html.='<th>Responsibility</th>';
      $html.='<th>Timeline</th>';
      $html.='</tr>';
      $ftr = false;
      $str = false;
      
      
      foreach($report['records'] as $records){
        foreach($records['risk'] as $risk){
          $html.='<tr>';
          if($risk['question_count'] <= 1)
          {
            $html.='<td>'.wordwrap($risk['risk_description'],35,'<br />').'</td>';
            $str = false;
          }
          else
          {
            $html.='<td rowspan="'.($risk['question_count']+1).'">'.wordwrap($risk['risk_description'],35,'<br />').'</td>';
            $html.='</tr>';
            $str = true;
          }
          foreach($risk['questions'] as $question)
          {
              if($str == true)
              {
                $html.='<tr>';
              }
              if($question['response'] != ""){
                $responses = $question['response'];  
              }
              else
              {
                $responses = 'N/A';
              }
              $dateTimeline = '';
              if($question['timeline'] == '1970-01-01' || $question['timeline'] == '-0001-11-30')
              {
               $dateTimeline = ' - ';
              }
              else
              {
               $dateTimeline = $question['timeline'];
              }
              $html.='<td>'.$question['questions'].'</td>';
              $html.='<td>'.$question['responsible'].'</td>';
              $html.='<td>'.$responses.'</td>';
              $html.='<td>'.$question['actionplan'].'</td>';
              $html.='<td>'.$question['responsible'].'</td>';
              $html.='<td>'.$dateTimeline.'</td>';
              if($str == true)
              {
                $html.='</tr>';
              }
          }
        }
        if($str == false)
        {
          $html.='</tr>';
        }
      }
      $html.='</table>';
    break;
    case '1.3':
      $fileName = 'Complete RMC';
      $outletname = '';
      if(isset($report['outlet_name']) && !empty($report['outlet_name']))
      {
        $outletname = $report['outlet_name'].' - ';
      }
      $html='<h3>Complete RMC (Risk Control Matrix)</h3>';
      
      $html.='<table width="50%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th>Outlet Name</th>';
      $html.='<td>'.$report['info']['outlet_name'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Manager Name</th>';
      $html.='<td>'.$report['info']['manager_name'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Assessment Date</th>';
      $html.='<td>'.$report['info']['assessment_date'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Expected Controls</th>';
      $html.='<td>'.$report['info']['expected_control'].'</td>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Non Compliance %</th>';
      $html.='<td>'.$report['info']['risk_exposure'].'%</td>';
      $html.='</tr>';
      $html.='</table>';
      $html.='<br />';
      $html.='<br />';
      
      $html.='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th colspan="5">'.$outletname.' Risk Control Matrix</th>';
      $html.='<th colspan="3">Management Action Plans</th>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th>Business Risk / Impact</th>';
      $html.='<th>Risk Description / Root Cause</th>';
      $html.='<th>Self Assessment Question</th>';
      $html.='<th>Responsibility</th>';
      $html.='<th>Responses</th>';
      
      $html.='<th>Action Plan</th>';
      $html.='<th>Responsibility</th>';
      $html.='<th>Timeline</th>';
      $html.='</tr>';
      $ftr = false;
      $str = false;
      
      
      foreach($report['records'] as $records){
        $html.='<tr>';
        $risk_count = ($records['risk_count'] == 1)?0:$records['risk_count'];
        if($records['risk_count'] <= 1)
        {
          $html.='<td class="category_click">'.wordwrap($records['risk_impact'],35,'<br />').'</td>';
          $ftr = false;
        }
        else
        {
          $html.='<td rowspan="'.($records['risk_count']+1).'">'.wordwrap($records['risk_impact'],35,'<br />').'</td>';
          $html.='</tr>';
          $ftr = true;
        }
        foreach($records['risk'] as $risk){
          if($ftr === true)
          {
            $html.='<tr>';
          }
          if($risk['question_count'] <= 1)
          {
            $html.='<td>'.wordwrap($risk['risk_description'],35,'<br />').'</td>';
            $str = false;
          }
          else
          {
            $html.='<td rowspan="'.($risk['question_count']+1).'">'.wordwrap($risk['risk_description'],35,'<br />').'</td>';
            $html.='</tr>';
            $str = true;
          }
          foreach($risk['questions'] as $questions){
              if($str == true)
              {
                $html.='<tr>';
              }
              $dateTimeline = '';
              if($questions['timeline'] == '1970-01-01' || $questions['timeline'] == '-0001-11-30')
              {
               $dateTimeline = ' - ';
              }
              else
              {
               $dateTimeline = $questions['timeline'];
              }
              $html.='<td>'.$questions['questions'].'</td>';
              $html.='<td>'.$questions['responsible'].'</td>';
              $html.='<td>'.$questions['response'].'</td>';
              $html.='<td>'.$questions['actionplan'].'</td>';
              $html.='<td>'.$questions['responsible'].'</td>';
              $html.='<td>'.$dateTimeline.'</td>';
              if($str == true)
              {
                $html.='</tr>';
              }
          }
          if($str == false)
          {
            $html.='</tr>';
          }
        }
        if($ftr == false)
        {
          $html.='</tr>';
        }
      }
      $html.='</table>';
    break;
    case '2':
      $fileName = 'Risk Exposure by Category';
      $html='<h3>Risk Exposure by Category</h3>';
      $html.='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th width="40%">Outlet Reference</th>';
      $html.='<th width="15%">Total</th>';
      
      foreach($territory as $territory_value){
        $html.='<th width="15%">'.$territory_value.'</th>';
      }
      $html.='</tr>';
      $non_compliance = array();
      $category_id = array();
      foreach($report as $k=>$record)
      {
        $total_controls = 0;
        $total_no = 0;
        foreach($territory as $key=>$terval){
          if(isset($record[$key]) && !empty($record[$key]))
          {
            $total_controls = ($total_controls+$record[$key]->total_controls);
            $total_no = ($total_no+$record[$key]->total_no);
            $non_compliance[$k] = ($total_no/$total_controls) * 100;
            $category_id[$k] =  $record[$key]->category_id;
          }
        }
      }
      foreach($report as $i=>$record){
          $html.='<tr>';
          $html.='<td class="first-child">'.$i.'</td>';
          if($non_compliance[$i] > 30){
          $html.='<td class="red total_category_click">High</td>';
          }
          if($non_compliance[$i] >= 15 && $non_compliance[$i] <= 30){
          $html.='<td class="yellow total_category_click">Medium</td>';
          }
          if($non_compliance[$i] < 15){
          $html.='<td class="green total_category_click">Low</td>';
          }
          foreach($territory as $j=>$terr){
            if($record)
            {
              if(isset($record[$j]) && !empty($record[$j]))
              {
                if($record[$j]->percentage > 30){
                $html.='<td class="red territory_category_click">High</td>';
                }
                if($record[$j]->percentage >= 15 && $record[$j]->percentage <= 30){
                $html.='<td class="yellow territory_category_click">Medium</td>';
                }
                if($record[$j]->percentage < 15){
                $html.='<td class="green territory_category_click">Low</td>';
                }
              }
              else
              {
                $html.='<td>N/A</td>';
              }
            }
            else
            {
              $html.='<td>N/A</td>';
            }
          }
          $html.='</tr>';
      }
      $html.='<tr>';
      $html.='</tr>';
      $html.='</table>';
    break;
    case '2.1':
      
      if($report[0]->all == 1)
      {
        $html='<h3>Category Risk Exposure for all territories</h3>';
        $fileName = 'Category Risk Exposure for all territories';
      }
      else
      {
        $html='<h3>Category Risk Exposure for '.$report[0]->territory_name.'</h3>';
        $fileName = 'Category Risk Exposure for '.$report[0]->territory_name;
      }
      $html.='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
      $html.='<tr>';
      $html.='<th colspan="3">'.$report[0]->title.'</th>';
      $html.='</tr>';
      $html.='<tr>';
      $html.='<th width="10%">Sr.no</th>';
      $html.='<th width="40%">Outlet Reference</th>';
      $html.='<th width="15%">Risk Rating</th>';
      $html.='</tr>';
      $total_controls = 0;
      $total_no = 0;
      $i = 0;
      foreach($report as $a=>$record)
      {
          $i++;
          $html.='<tr>';
          $html.='<td>'.$i.'</td>';
          $html.='<td class="first-child category_click">'.$record->outlet_name.'</td>';
          if($record->percentage > 30){
          $html.='<td class="red">High</td>';
          }
          if($record->percentage >= 15 && $record->percentage <= 30){
          $html.='<td class="yellow">Medium</td>';
          }
          if($record->percentage < 15){
          $html.='<td class="green">Low</td>';
          }
          $html.='</tr>';
      }
      $html.='</table>';
    break;
  }
  //echo $get['exportType'];exit;
  $filetype = '';
  if($get['exportType'] == 'excel')
  {
    $filetype = 'vnd.ms-excel';
    $ext ='.xls';
    header("Content-type: application/$filetype");
    //header("Content-Disposition: attachment; filename=$fileName$ext");
    header("Content-Disposition: attachment; filename=\"" . $fileName.$ext . "\"");
    echo $html;
  }
  elseif($get['exportType'] == 'pdf')
  {
    require_once('html2pdf/html2pdf.class.php');
    $format = 'P';
    if($type == '1.2' || $type == '1.3')
    {
      $format = 'L';
    }
    $html2pdf = new HTML2PDF($format,'A4','en');
    $html2pdf->WriteHTML($html);
    $filetype = 'pdf';
    $ext = '.pdf';
    $html2pdf->Output($fileName.$ext,'D');
  }
?>