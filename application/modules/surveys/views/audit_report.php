<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Internal Audit portal - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.survey .concept-box {
    width: 300px;
    height: 100px;
    margin-right: 25px;
}
</style>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".inline").colorbox({inline:true, width:"50%"});
        });
    </script>

</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
	<h2>Internal Audit portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Internal Audit Reports</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="portal-main clearfix">
        <h1>Internal Audit Reports</h1>
        
        <?php if(
                $myprofile['role_id'] == $permission_roles['ARV'] or
                $myprofile['role_id'] == $permission_roles['AM'] or
                $myprofile['role_id'] == $permission_roles['SA'] or 
                (isset($concept_manager) && !empty($concept_manager) && in_array($myprofile['id'],$concept_manager))):
        ?>
        <ul>
        <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['outlet_audit_reports_group_id'];?>/files"><img  src="<?php echo site_url(); ?>media/images/concepts/outlet_audit_reports.jpg" /><div class="text">Outlet Audit Reports</div></a></li>
        
        <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['process_audit_reports_group_id'];?>/files"><img  src="<?php echo site_url(); ?>media/images/concepts/process.jpg" /><div class="text">Process Audit Reports</div></a></li>
        </ul>
        <?php endif; ?>
        

    </div>	<!-- concept-grid -->


	<!-- <div class="loader-more clearfix">&nbsp;</div> -->


	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>


</body>
</html>