<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title>Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

<link type="text/css" rel="stylesheet" href="<?php echo site_url(); ?>media/css/jquery.fancybox.css?v=2.1.5"media="screen" />
<style>
ul.ui-autocomplete {
    z-index: 55000;
}
.ddDate{
  border: 1px solid #ccc !important;
  padding: 4px !important;
  border-radius: 1px !important;
}
</style>

</head>
<body class="profile">



<?php $this->load->view('global_header.php'); ?>

<div class="section wrapper clearfix">
	<h2>Internal Audit portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Positive Assurance Questionnaire</li>
  </ul>
</div>

<div class="section wrapper clearfix">
	<div class="left-contents">
    <?php
					$this->load->view('partials/survey_list_filters');
    ?>
    <div class="survey-listing">
        

      <?php   if((isset($permission_roles['AM']) && !empty($permission_roles['AM']) && $myprofile['role_id'] == $permission_roles['AM']) || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA'])):
                    if(isset($surveys) && !empty($surveys)):
                      foreach($surveys as $survey):
                  ?>
								  <div class="listing-block">
                  	<h4><a title="Edit Questionnaire" href="<?=site_url();?>surveys/edit/<?php print $survey->id; ?>"><?php print $survey->name;?></a></h4>
                      <ul>
                        <li><a title="Preview Questionnaire" href="<?=site_url();?>surveys/preview/<?php print $survey->id; ?>"><img src="<?php echo base_url();?>media/images/surveys/review.png" border="0" alt=""></a></li>
                        <li><a title="Edit Questionnaire" href="<?=site_url();?>surveys/edit/<?php print $survey->id; ?>"><img src="<?php echo base_url();?>media/images/surveys/edit.png" border="0" alt=""></a></li>
                        <li><a title="Delete Questionnaire" href="<?=site_url();?>surveys/delete/<?php print $survey->id; ?>"><img src="<?php echo base_url();?>media/images/surveys/delete.png" border="0" alt=""></a></li>
                      </ul>
                  </div>
						    	<?php
                      endforeach;
                    else:
                  ?>
                    <div class="listing-block"><h4>No Surveys Found.</h4></div>
                  <?php
                    endif;
                   elseif((isset($permission_roles['US']) && !empty($permission_roles['US']) && $myprofile['role_id'] == $permission_roles['US']) or (isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager))):
                    if(isset($user_surveys) && !empty($user_surveys)):
                      foreach($user_surveys as $survey):
                      $answersVar = 0;
                      if(($survey->answers_count == $survey->questions_count) && ($survey->user_status->status != 'inprogress' && $survey->user_status->status != 'pending'))
                      {
                        $answersVar = 1;
                      }
                   ?>

                  <div class="listing-block">
                  	<h4><a href="<?php if($answersVar == 1): ?><?=site_url();?>surveys/submit_survey?survey_id=<?php echo $survey->id; ?><?php else: ?><?=site_url();?>surveys/submit/<?php print $survey->id; ?><?php endif; ?>"><?php print $survey->name;?></a></h4>
                      <ul>
                        <li><a href="<?php if($answersVar == 1): ?><?=site_url();?>surveys/submit_survey?survey_id=<?php echo $survey->id; ?><?php else: ?><?=site_url();?>surveys/submit/<?php print $survey->id; ?><?php endif; ?>"><img src="<?php echo base_url();?>media/images/surveys/review.png" border="0" alt=""></a></li>
                      </ul>
                  </div>
						    	<?php
                      endforeach;
                    else:
                  ?>
                    <div class="listing-block"><h4>No Surveys Found.</h4></div>
                  <?php
                    endif;
                  endif; ?>



    </div>

  </div>

	<div class="right-contents media-widget">
  </div> <!-- right-contents -->

    <div style='display:none'>

	<div id='people_box' style='padding:10px; background:#fff;'>
		 <form name="form" id= "target" method="post" action="<?=site_url("manage/concept_group_managers")?>">

				 <h2>Assign survey to:</h2>
					<div>

						<!-- <input type="text" id="demo-input-people" name="blah" /> -->
						<div class="textC"><input type="text" id="people_list" name="people_list"  class="txt_title ui-autocomplete-input" autocomplete="off" role="textbox"aria-autocomplete="list" aria-haspopup="true" placeholder="Start typing..."  value=""></div>
						<input type="submit" value="Submit" />

					</div>
			</form>
		</div>
	</div>


</div> <!-- section -->


<?php $this->load->view('global_footer.php'); ?>


<?php $this->load->view('partials/js_footer'); ?>



<script type="text/javascript">
$(document).ready(function() {
	//$('.permissions').fancybox();
	$('.permissions').fancybox({
		maxHeight: 500,
		afterClose  : function() {
           /*$("#frm_add_item")[0].reset();
           $('#upload_file-box .msg').removeClass('success');
           $('#upload_file-box .msg').removeClass('error');
           $('#upload_file-box .msg').html('');
		   $('#upload_file-box .msg').fadeIn();
		   $('.error-msg').html('');*/
		   }
	});
  $( "#cal-date" ).datepicker({
    buttonImage: "/media/images/date-picker.png",
    buttonImageOnly: true,
    dateFormat: 'yy-mm-dd'
  });
  $('#surveyFilter #clear').click(function(e){
      e.preventDefault();
      $('#ddDate').val(0);
      $('#ddConcepts').val(0);
      loadSurvey();
  });
	var availableTags = [
      "Arun Kumaran",
      "Aqeel Ahmad",
      "Parves Shahid",
      "Chinmaya Rout"
    ];
    $( "#people_list" ).autocomplete({
      source: availableTags,
	   position: { my: "left-11 top", at: "left bottom", collision: "none" },
      select: function(event, ui){
        var terms = split(this.value);
        // remove the current input
        terms.pop();
        // add the selected item
        terms.push(ui.item.value);
        // add placeholder to get the comma-and-space at the end
        terms.push("");
        this.value = terms.join(", ");
        return false;
      },
      search: function(){
        // custom minLength
        var term = extractLast(this.value);
        if(term=="Start typing.."){
          return false;
        }
        if (term.length < 2) {
            return false;
        }
      },
      focus: function(){
        // prevent value inserted on focus
        return false;
      },
    });
    $('#bt_survey_filter').live('click',function(){
      loadSurvey();
    });
    
});

function extractLast(term){
  return split(term).pop();
}
function split(val){
  return val.split(/,\s*/);
}
function loadSurvey()
{
    var service_url = siteurl + "surveys/ajax_list_survey";
    $.ajax({
      beforeSend: function() {
        $('.loader-more').show();
      },
      complete: function() {
        $('.loader-more').hide();
      },
      url: service_url,
      data: $('#surveyFilter').serialize(),
      async: false,
      dataType: "json",
      type: "POST",
      success: function(data){
        if(data.status == true)
        {
          var html = '';
          $.each(data.result, function(a,result) {
            if(data.isAdmin == true)
            {
              html+= '<div class="listing-block">';
              html+= '<h4><a title="Edit Questionnaire" href="/surveys/edit/'+result.id+'">'+result.name+'</a></h4>';
              html+= '<ul>';
              html+= '<li><a title="Preview Questionnaire" href="'+siteurl+'surveys/preview/'+result.id+'"><img src="'+siteurl+'media/images/surveys/review.png" border="0" alt=""></a></li>';
              html+= '<li><a title="Edit Questionnaire" href="'+siteurl+'surveys/edit/'+result.id+'"><img src="'+siteurl+'media/images/surveys/edit.png" border="0" alt=""></a></li>';
              html+= '<li><a title="Delete Questionnaire" href="'+siteurl+'surveys/delete/'+result.id+'"><img src="'+siteurl+'media/images/surveys/delete.png" border="0" alt=""></a></li>';
              html+= '</ul>';
              html+= '</div>';
            }
            else
            {
              if(result.answered == 1)
              {
                var survey_url = siteurl+'surveys/submit_survey?survey_id='+result.id;
              }
              else
              {
                var survey_url = siteurl+'surveys/submit/'+result.id;
              }
              html+= '<div class="listing-block">';
              html+= '<h4><a href="'+survey_url+'">'+result.name+'</a></h4>';
              html+= '<ul>';
              html+= '<li><a href="'+survey_url+'"><img src="'+siteurl+'media/images/surveys/review.png" border="0" alt=""></a></li>';
              html+= '</ul>';
              html+= '</div>';
            }
          });
          
          $('.survey-listing').html(html);
        }
        else if(data.status == false)
        {
          $('.survey-listing').html('<div class="no-result-block d-n" style="display: block;">No records found.</div>');
        }
      }
    });
}
</script>
</body>
</html>