<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Outlet Management - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm.submit
{
    float: right;
}
.outlet .ui-autocomplete{
  z-index:9999;
}
#add_outlet_tab{
  float: right;
  margin: 5px 29px 5px 0;
}
</style>
</head>
<body class="full-width outlet">

	<?php $this->load->view('global_header.php'); 
  if((isset($permission_roles['AM']) && !empty($permission_roles['AM']) && $myprofile['role_id'] == $permission_roles['AM']) || (isset($permission_roles['SA']) && !empty($permission_roles['SA']) && $myprofile['role_id'] == $permission_roles['SA'])):
  ?>

	<div class="section wrapper clearfix">
 	<h2>Outlet Management</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Manage Outlets</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">
        	<?php
					$this->load->view('partials/outlet_filters');
				?>
        <a href="#add_outlet" id="add_outlet_tab" class="btn-sm fancybox" >Add Outlet</a>
      <div class="result">
      
      </div>
    <!-- div class="loader-more">&nbsp;</div -->
	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>
<?php $this->load->view('partials/add_outlet'); ?>
<script type="text/javascript">
var limit = 6;
var start = 0;
var type = 0;
var end = false;

var nearToBottom = 250;
$(document).ready(function() {
  
	$('#cancel').on( "click", function() {
		$('#add_outlet .msg').hide();
		$.fancybox.close();
	});
  $('#delete_outlet').live('click',function(){
    if(confirm("Are you sure to delete this outlet?") === false)
    {
      return false;
    }
  });
	$('#add_outlet_tab,.fancybox').fancybox({
		maxHeight: 500,
		minWidth: 230,
    afterClose  : function() {
      $("#frm_add_outlet")[0].reset();
      $('#add_outlet .msg').hide();
      $('.error-msg').show().html('');
    },
    beforeLoad : function() {
     var outletid = $(this)[0].element.context.attributes[1]['nodeValue'];
     if(!isNaN(outletid))
     {
        loadEditOutlet(outletid);
     }
    },
  });

	$('.loader-more').show();
	loadOutlet({'noclear': 0});
  $('#clear_outlet').live('click',function(){
    $('#fFilter')[0].reset();
    loadOutlet({'noclear': 0});
  });
	$('#ddConcepts, #ddCountry').change(function(){
		end = false;
		start = 0;
		loadOutlet({'noclear': 0});
	});
  
  $( "#txt_name" ).autocomplete({
    source: siteurl + 'people/autocomplete_ajax',
	 position: { my: "left-11 top", at: "left bottom", collision: "none" },
	 appendTo: "#filter-search .search",
    select: function (a, b) {
      $(this).val(b.item.link);
      return false;
    }
  });
  
  $('#bt_add_outlet').live('click',function(){
    var name = $('#txt_name').val();
    var concept = $('#txt_concept').val();
    var territory = $('#txt_territory').val();
    var outlet = $('#txt_outlet').val();
    var outlet_id = $('#outlet_id').val();
    if(!name){
      $("#error-txt_name").html("Required");
      $("#txt_name").focus();
      return false;
    }
    else if(!outlet)
    {
      $("#error-txt_outlet").html("Required");
      $("#txt_outlet").focus();
      return false;
    }
    else if(!concept)
    {
      $("#error-txt_concept").html("Required");
      $("#txt_concept").focus();
      return false;
    }
    else if(!territory)
    {
      $("#error-txt_territory").html("Required");
      $("#txt_territory").focus();
      return false;
    }
    else
    {
      $("#error-txt_name").html("");
      $("#error-txt_outlet").html("");
      $("#error-txt_concept").html("");
      $("#error-txt_territory").html("");
    }
    var data = {'username' : name, 'outlet' : outlet, 'concept': concept,'territory' :territory,'outlet_id':outlet_id}; 
   $.ajax({
      url: $('#frm_add_outlet').attr('action'),
      data: data,
      async: false,
      dataType: "json",
      type: "POST",
      success: function(data){
        if(data.status == true)
        {
          loadOutlet({'noclear': 1});
          $('#add_outlet .msg').show();
          $('#add_outlet .msg').addClass('success').html('Outlet Manager added successfully');
          $("#frm_add_outlet")[0].reset();
          $.fancybox.close();
        }
        else
        {
          if(data.exist == true)
          {
            $('#add_outlet .msg').show();
            $('#add_outlet .msg').addClass('error').removeClass('success');
            $('#add_outlet .msg').html('Record already exist.');
          }
        }
      }
   });
      
  });
});
function loadEditOutlet(outlet_id)
{
   var data = {'outlet_id' : outlet_id}; 
   var service_url = siteurl+"surveys/outlets/edit";
   $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "POST",
      success: function(data){
        if(data)
        {
          $('#txt_name').val(data.username);
          $('#txt_concept').val(data.concept_id);
          $('#txt_territory').val(data.territory_id);
          $('#txt_outlet').val(data.outlet_name);
          $('#outlet_id').val(data.id);
        }
      }
   });
}
function loadOutlet(prop)
{

	$('div.outlet-grid-view').show();
  if(!prop.noclear)
	{
  	var concept = $('#ddConcepts').val();
    var country = $('#ddCountry').val();  
	}
  
	var service_url = siteurl+"surveys/outlets/ajax";
  
	var data = {'concept': concept,'country': country};
	if(end == false)
	{
     $('.loader-more').show();
     $.ajax({
      url: service_url,
      data: data,
      async: false,
      dataType: "json",
      type: "GET",
      success: function(data){
          if(data.status == true)
          {
            var html ='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
            html +='<tr>';
            html +='<th width="20%">User Name</th>';
            html +='<th width="15%">Outlet Name</th>';
            html +='<th width="15%">Concept Name</th>';
            html +='<th width="15%">Territory Name</th>';
            html +='<th width="15%" colspan="2">Action</th>';
            html +='</tr>';
            $.each(data.result, function() {
            html +='<tr>';
            html +='<td>'+this.user_name+'</td>';
            html +='<td>'+this.outlet_name+'</td>';
            html +='<td>'+this.concept+'</td>';
            html +='<td>'+this.territory+'</td>';
            html +='<td><a href="#add_outlet" id="'+this.id+'" class="btn-sm fancybox">Edit</a></td>';
            html +='<td><a href="'+siteurl+'surveys/outlets/delete?id='+this.id+'" class="btn-sm" id="delete_outlet">Delete</a></td>';
             
            html +='</tr>';  
            })
            html +='</table>';
            $('div.result').html(html);
          }
          else{
            $('div.result').html('No Record.');
          }
      }
    });
	}
}
function remove_tags(html)
  {
       return html.replace(/<(?:.|\n)*?>/gm, '');
  }
function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

</script>
<?php else: 
header('location:'.site_url().'surveys/mysurveys');
?>
<?php endif; ?>
</body>
</html>
