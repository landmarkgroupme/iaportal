<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Concepts - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm.submit
{
    float: right;
}
</style>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
 	<h2>Self compliance portal</h2>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">

        <div class="assess-form">
            <form action="" id="survey">
                <input type="hidden" value="<?php echo isset($survey) ? $survey->id : ''; ?>" name="survey_id" />
                <div class="row" id="message_place" style="display:none">
                  <h3 style="
                    text-align: center;
                ">Survey Saved</h3>
                </div>
                <div class="row">
                    <label for="title">Title</label>
                    <div class="textC">
                        <input type="text" id="title" name="title" class="" value="<?php echo isset($survey) ? $survey->name : ''; ?>">
                    </div>
                </div>
                <div class="row" <?php echo isset($survey) ? 'style="display:none"' : ''; ?>>
                    <label for="Concept" >Concept</label>
                    <select id="Concept" name="concept">
                        <?php foreach($concepts as $concept): ?>
                            <option value="<?php echo $concept->id; ?>"><?php echo ucwords(strtolower($concept->name)); ?></option>
                        <?php endforeach; ?>
                        
                    </select>
                </div>
                <div class="row" <?php echo !isset($survey) ? 'style="display:none"' : ''; ?>>
                    <label for="Assign">Assign to</label>
                    <div class="textC">
                        <input type="text" id="Assign" name="survey_users" class="autocomplete_user">
                    </div>
                </div>
                <div class="row" <?php echo !isset($survey) ? 'style="display:none"' : ''; ?>>
                    <label for="category">Risk Impact</label>
                    <select id="category" name="category" style="float: left"></select>
                    <a href="#" class="indented_icons add_category"><img src="<?php echo site_url(); ?>media/images/surveys/plus.png" width="16" alt="Add a Risk Impact"/></a>
                    <a href="#" class="indented_icons remove_category"><img src="<?php echo site_url(); ?>media/images/surveys/minus.png" width="16" alt="Remove a Risk Impact"/></a>
                </div>
                <div class="row save_category" style="display:none">
                    <label for="newcategory">Risk Impact Name</label>
                    <div class="textC">
                        <input type="text" id="newcategory" class="">
                    </div>
                    <a href="#" class="indented_hyperlink submit">Save Risk Impact</a>
                </div>
                <div class="row category" <?php echo !isset($survey) ? 'style="display:none"' : ''; ?>>
                    <label for="Category">Risk Description</label>
                    <select id="subcategory" name="subcategory" style="float: left">
                    </select>
                    <a href="#" class="indented_icons add_sub_category"><img src="<?php echo site_url(); ?>media/images/surveys/plus.png" width="16" alt="Add a Risk Description"/></a>
                    <a href="#" class="indented_icons remove_sub_category"><img src="<?php echo site_url(); ?>media/images/surveys/minus.png" width="16" alt="Remove a Risk Description"/></a>
                </div>
                <div class="row save_sub_category" style="display:none">
                    <label for="newsubcategory">Risk Description Name</label>
                    <div class="textC">
                        <input type="text" id="newsubcategory" class="">
                    </div>
                    <a href="#" class="indented_hyperlink submit">Save Risk Description</a>
                </div>
                <div class="row quest">
                    <input type="button" id="" class="btn-sm submit" value="<?php echo isset($survey) ? 'Save Survey' : 'Create Survey'; ?>">
                </div>
            </form>
        </div>


	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>

<script>

    var questions = [];
    var survey_id = <?php echo isset($survey) ? $survey->id : 'null'; ?>;

    $(window).bind("load", function() { 
        var timeout = setTimeout(function() {
        $("img.lazy").trigger("sporty")}, 5000);
            /*$("#Assign").tokenInput("http://devnet.landmarkgroup.com/manage/auto_group", {
                theme: "facebook",
                zindex: 99999
                ,prePopulate: [
                    
                        {id: 123, name: "Slurms MacKenzie"},
                        {id: 555, name: "Bob Hoskins"},
                        {id: 9000, name: "Kriss Akabusi"}
                    ]
                [{"id":"29","name":"Cricket"}]
            });*/
        });

    $(window).load(function(){
        $('#more-quest').live('click',function(){   
            var more = '<div class="row quest"><div class="textC"><input type="text" name="question[]" class="question" placeholder="Question"></div></div><div class="row quest"><div class="textC"><input type="text" name="question[]" class="" placeholder="Question"></div></div><div class="row quest"><div class="textC"><input type="text" name="question[]" class="" placeholder="Question"></div><a href="#" id="more-quest">+More</a></div>';
            $(this).hide().parent().after(more);
        });

        if(survey_id)
        {
            loadCategories('#category', 0);
        }

        $('div.row.save_category').hide();

        $('div.row.save_sub_category').hide();

        $('#message_place').hide();

        /*$('div.row.quest .textC').on('blur', 'input.question', function(){
            console.log('here');
        });*/

        $('input.question').live('blur', function(){
            saveSurvey();
        });

        

        $('a.add_category').click(function(){
            $('div.row.save_category').show();
        });

        $('a.remove_category').click(function(){
            removeCategory('#category', $('#category').val(),0);
        });

        $('a.remove_sub_category').click(function(){
            removeCategory('#subcategory', $('#subcategory').val(),$('#category').val());
        });
        

        $('a.add_sub_category').click(function(){
            $('div.row.save_sub_category').show();
        })

        $('div.row.save_category a.submit').click(function(){
            addCategory('#category', $('#newcategory').val(), 0);
            $('#newcategory').val('');
            $('div.row.save_category').hide();
        });

        $('div.row.save_sub_category a.submit').click(function(){
            addCategory('#subcategory', $('#newsubcategory').val(), $('#category').val());
            $('#newsubcategory').val('');
            $('div.row.save_sub_category').hide();
        });


        $(".autocomplete_user").tokenInput("<?php echo base_url();?>manage/auto_users",{
            theme: "facebook"
            <?php if(isset($survey)): ?>
            ,prePopulate: 
                [
                <?php foreach($users as $user): 
                echo '{"id":"'. $user->id. '","name":"'.$user->display_name.'"},';
                endforeach;
                ?>
                ]
            <?php endif; ?>

        });


        $('#category').change(function(){
            loadCategories('#subcategory', $(this).val());
        });
        $('#subcategory').change(function(){
            loadQuestions($(this).val());
        });

        $('#survey input.submit').click(function(){
            //console.log($('div.row.quest.question'));

            //return false;
            //var formData = new FormData($('#survey')[0]);
            saveSurvey();
        });

    });

    function saveSurvey() {
        $.ajax({
                beforeSend: function() {
                    //inactive_form();
                },
                complete: function() {
                        
                },
                type    : 'POST',
                cache   : false,
                async   : false,
                url     : siteurl+'surveys/save_survey',
                data    : $('#survey').serialize(),
                dataType: "json",
                success : function(response) {
                    if(!survey_id)
                    {
                        //creating a survey
                        if(response.status == 'success')
                        {
                            window.location.href = siteurl+'surveys/edit/' + response.id + '/';
                        }
                    } else {
                        loadQuestions($('#subcategory').val());
                    }
                    $('#message_place').show().delay( 1000 ).hide( 'slow' );
                    //$('#message_place').hide('slow');
                    //delay(2000).hide();
                },
            });
    }


    function loadCategories(node, parent_id) {
        $.ajax({
            beforeSend: function() {
                //inactive_form();
            },
            complete: function() {
                if(node == '#category')
                {
                    loadCategories('#subcategory', $(node).val());
                }
                if(node == '#subcategory')
                {
                    loadQuestions($(node).val());
                }
                    
            },
            type    : 'GET',
            cache   : false,
            async   : false,
            url     : siteurl+'surveys/get_categories',
            data    : {'parent_id': parent_id},
            dataType: "json",
            success : function(response) {
                $(node).find('option').remove(); //removing exising options with node
                if(response.question_categories)
                {
                    $.each(response.question_categories, function( index, category ) {
                        $(node).append($("<option></option>")
                            .attr("value",category.id)
                            .attr('name', 'question[]')
                            .text(category.title)); 
                    });
                }
                
            },
        });
        return true;
    }

    function loadQuestions(category_id) {
        console.log('here');
        $('div.row.quest.question').remove();
        $.ajax({
            beforeSend: function() {
                //inactive_form();
            },
            complete: function() {
                /*if(node == '#category')
                {
                    loadCategories('#subcategory', $(node).val());
                }*/
                //var more = '<div class="row quest question"><div class="textC"><input type="text" id="Assign" class="" placeholder="Question"></div><a href="#" id="more-quest">+More</a></div>';
                var more = '<div class="row quest question"><div class="textC"><input type="text" name="question[]" class="question" placeholder="Question"></div></div><div class="row quest question"><div class="textC"><input type="text" name="question[]" class="question" placeholder="Question"></div></div><div class="row quest question"><div class="textC"><input type="text" name="question[]" class="question" placeholder="Question"></div><a href="#" id="more-quest">+More</a></div>';
                $('div.row.quest:last').before(more);
                
                    
            },
            type    : 'GET',
            cache   : false,
            async   : false,
            url     : siteurl+'surveys/get_questions',
            data    : {'category_id': category_id, 'survey_id': survey_id},
            dataType: "json",
            success : function(response) {
                 //removing exising options with node
                 //console.log(response);
                if(response.questions)
                {
                    $.each(response.questions, function( index, question ) {
                        
                        var question_html = '<div class="row quest question"><div class="textC"><input type="hidden" name="existing['+question.id+']" /><input name="question['+question.id+']" data-id="' + question.id + '" type="text" class="question" value="' + question.title + '" placeholder="Question"></div>';
                        $('div.row.save_sub_category').after(question_html); 
                    });
                    
                    
                }
                
            },
        });
    }

    function addCategory(node,title, parent_id) {
        $.ajax({
            beforeSend: function() {
                //inactive_form();
            },
            complete: function() {
                    
            },
            type    : 'POST',
            cache   : false,
            url     : siteurl+'surveys/add_question_category',
            data    : {'parent_id': parent_id, 'title': title},
            dataType: "json",
            success : function(response) {
                if(loadCategories(node, parent_id))
                {
                    $(node).val(response.id);
                }
                
            },
        });
    }

    function removeCategory(node,id,parent_id) {
        $.ajax({
            beforeSend: function() {
                //inactive_form();
            },
            complete: function() {
                    
            },
            type    : 'POST',
            cache   : false,
            url     : siteurl+'surveys/remove_question_category',
            data    : {'id': id},
            dataType: "json",
            success : function(response) {
                if(loadCategories(node, parent_id))
                {
                    $(node).val(response.id);
                }
                
            },
        });
    }

</script>
</body>
</html>