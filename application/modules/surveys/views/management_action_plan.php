<!doctype html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Management Action Plan - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.btn-sm.submit
{
    float: right;
}
</style>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
 	<h2>Management Action Plan</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys">Internal Audit portal</a></li>
   <li><span>&gt;&gt;</span></li>
   <li><a href="<?php echo site_url(); ?>surveys/positive_assurance">Positive Assurance</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Management Action Plan</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

    <div class="container">
        

<div class="sur-holder last-child">

 
    
    <div class="checklist">
        <h3>Management Action Plan</h3>
        <?php 
        $this->load->view('partials/action_plan_filter');
        ?>
        <div class="result">
          <div class="no-result-block d-n" style="display:block">Please select the filters for the record.</div>
        </div>
    </div>
</div>



	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>
<script type="text/javascript" src="/media/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/media/js/jquery.ui.datepicker.js"></script>
<script>

$(document).ready(function() {
  //loadManagementActionPlan();
   if($('#ddConcepts').val() != 0 && $('#ddConcepts').val() >0)
    {
      loadTerritories();
      loadOutlets();
    }

});
$('#clear').live('click',function(e){
  e.preventDefault();
  
    var d = new Date();
	$('#ddConcepts').val('0');
    $('#ddYear').val('0');
	loadTerritories();
    $('#ddTerritories').val('0');
    $('#ddOutlet').val('0');
    loadManagementActionPlan();
});

/*
$('#ddTerritories,#ddOutlet,#ddConcepts').live('change',function(){
  loadManagementActionPlan();
});*/
$('#ddOutlet').live('change',function(){
  loadManagementActionPlan();
});

$('#ddTerritories').live('change',function(){
 loadOutlets();
});

$('#ddConcepts').live('change',function(){
  loadTerritories();
  loadOutlets();
});

<?php if(isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager)):?>
$('#ddYear').live('change',function(){
 loadManagementActionPlan();
});
<?php endif; ?>

var concept = 0;
var outlet = 0;
<?php if(isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager)):?>
outlet = <?php print $surveyoutlets[$myprofile['id']]; ?>;
<?php endif; ?>

<?php if(isset($concept_manager) && !empty($concept_manager) && in_array($myprofile['id'],$concept_manager)):?>
concept = <?php print $surveyconcepts[$myprofile['id']]; ?>;
<?php endif; ?>
function loadTerritories(){
var service_url = siteurl+'surveys/fetch_territory_by_concept_ajax'    
  var Getdata = {'concept':$('#ddConcepts').val()};
  $.ajax({
    url: service_url,
    data: Getdata,
    dataType: "json",
    type: "GET",
    success: function(data){
      if(data.status === true)
      {
        var options = '<option value="0">All Territories</option>';
        $.each(data.outlets,function(index,value){
		console.log(index);
		console.log(value);
           options += '<option value="' + value.id + '">' + value.name + '</option>';
        });
        $('#ddTerritories').html(options);
        $('#ddTerritories').show();
      }
      else
      {
        $('#ddTerritories').val(0);
        $('#ddTerritories').hide();
        $('#ddOutlet').hide();
      }
    },
  });
}

function loadOutlets(){
 var service_url = siteurl+'surveys/fetch_outlet_by_territory_ajax'    
  var Getdata = {'territory':$('#ddTerritories').val(),'concept':$('#ddConcepts').val()};
  $.ajax({
    url: service_url,
    data: Getdata,
    dataType: "json",
    type: "GET",
    success: function(data){
      if(data.status === true)
      {
        var options = '<option value="0">Outlets</option>';
        $.each(data.outlets,function(index,value){
           options += '<option value="' + index + '">' + value + '</option>';
        });
        $('#ddOutlet').html(options);
        $('#ddOutlet').show();
      }
      else
      {
        $('#ddOutlet').val(0);
        $('#ddOutlet').hide();
      }
    },
  });
} 
function loadManagementActionPlan(){
  var year = $('#ddYear').val();
  var territory = $('#ddTerritories').val();
  if(outlet == 0)
  {
    outlet = $('#ddOutlet').val();
  }
  if(concept == 0)
  {
    concept = $('#ddConcepts').val();
  }
  var Getdata = {"year":year,"territory":territory,"outlet":outlet,"concept":concept};
  var service_url = siteurl + "surveys/ajax_manangement_action_plan";
      $.ajax({
      beforeSend: function() {
        $('.loader-more').show();
      },
      complete: function() {
        $('.loader-more').hide();
      },
      url: service_url,
      data: Getdata,
      async: false,
      dataType: "json",
      type: "GET",
      success: function(data){
        if(data.status == true)
        {
          var html = '';
            
          if(outlet != 0)
          {
            html +='<table border="0" cellpadding="0" cellspacing="0" width="100%" class="introtable Outlet-main">';
            html +='<tbody>';
            html +='<tr>';
            html +='<td>Outlet Name</th>';
            html +='<td>'+data.info.outlet_name+'</td>';
            html +='</tr>';
            html +='<tr>';
            html +='<td>Manager Name</td>';
            html +='<td>'+data.info.manager_name+'</td>';
            html +='</tr>';
            html +='<tr>';
            html +='<td>Date of Assessment</td>';
            html +='<td>'+data.info.assessment_date+'</td>';
            html +='</tr>';
            html +='</tbody>';
            html +='</table>';
          }
          if(data.hasOwnProperty('records_mid'))
          {
            html+='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
            html+='<tr><th colspan="6">First Review</th></tr>';
            html+='<tr>';
            html+='<th width="10%">Sr No</th>';
            html+='<th width="30%">Question</th>';
            html+='<th width="20%">Action Plan</th>';
            html+='<th width="10%">Responsibility</th>';
            html+='<th width="10%">Date</th>';
            html+='<th width="10%">Implemetation Status</th>';
            html+='</tr>';
            $.each(data.records_mid, function(a,record) {
            html+='<tr>';
            html+='<td class="first-child">'+(a+1)+'</td>';
            html+='<td style="text-align:left;padding-left:10px;">'+record.title;
            html+='</td>';
            html+='<td  style="text-align:left;" class="third-child" data-qid="">'+record.actionplan+'<div></td>';
            html+='<td><div>'+record.responsibility+'</div></td>';
            html+='<td>';
            if(record.actionplan_dttm != '1970-01-01 00:00:00' && record.actionplan_dttm != '-0001-11-30 00:00:00' && record.actionplan_dttm != '0000-00-00 00:00:00')
            {
			apdate = record.actionplan_dttm.substring(0,10);
            html+='<div>'+apdate+'</div>';
            }
            html+='</td>';
            if(record.answer == 'Yes')
            {
            html+='<td>YES</td>';
            }
            else if(record.answer == 'No')
            {
            html+='<td>NO</td>';
            }
            html+'</tr>';
            });
            html+='</table>';
          }
          html+='<br /><br />';
          if(data.hasOwnProperty('records_annual'))
          {
            html+='<table width="100%" border="1" cellspacing="0" cellpadding="0">';
            // html+='<tr><th colspan="6">Second Review</th></tr>'; 25 Aug 2014
			 html+='<tr><th colspan="6">Review</th></tr>';
            html+='<tr>';
            html+='<th width="10%">Sr No</th>';
            html+='<th width="30%">Question</th>';
            html+='<th width="20%">Action Plan</th>';
            html+='<th width="10%">Responsibility</th>';
            html+='<th width="10%">Date</th>';
            html+='<th width="10%">Implemetation Status</th>';
            html+='</tr>';
            $.each(data.records_annual, function(a,record) {
            html+='<tr>';
            html+='<td class="first-child">'+(a+1)+'</td>';
            html+='<td style="text-align:left;padding-left:10px;">'+record.title;
            html+='</td>';
            html+='<td style="text-align:left;" class="third-child" data-qid=""><div style="width: 225px;word-wrap: break-word;">'+record.actionplan+'</div></td>';
            html+='<td><div>'+record.responsibility+'</div></td>';
            html+='<td>';
            if(record.actionplan_dttm != '1970-01-01 00:00:00' && record.actionplan_dttm != '-0001-11-30 00:00:00' && record.actionplan_dttm != '0000-00-00 00:00:00')
            {
            apdate = record.actionplan_dttm.substring(0,10);
            html+='<div>'+apdate+'</div>';
            }
            html+='</td>';
            if(record.answer == 'Yes')
            {
            html+='<td>YES</td>';
            }
            else if(record.answer == 'No')
            {
            html+='<td>NO</td>';
            }
            html+'</tr>';
            });
            html+='</table>';
          }
            $('.result').html(html);
        }
        else if(data.status == false)
        {
          $('.result').html('<div class="no-result-block d-n" style="display: block;">No records found.</div>');
        }
        <?php if(isset($outlet_manager) && !empty($outlet_manager) && !in_array($myprofile['id'],$outlet_manager)):?>
        outlet = 0;
        <?php endif; ?>
        <?php if(isset($concept_manager) && !empty($concept_manager) && !in_array($myprofile['id'],$concept_manager)):?>
        concept = 0;
        <?php endif; ?>
        
      }
    });
}
</script>
</body>
</html>