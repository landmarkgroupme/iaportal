<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Internal Audit Portal - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.survey .concept-box {
    width: 300px;
    height: 100px;
    margin-right: 25px;
}
</style>

</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
	<h2>Internal Audit Portal</h2>
  <ul class="breadcrumb">
   <li><a href="<?php echo site_url(); ?>">Home</a></li>
   <li><span>&gt;&gt;</span></li>
   <li>Internal Audit portal</li>
  </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="portal-main clearfix">
        <h1>Internal Audit Portal</h1>
       
        
        <?php
        #echo "<pre>".print_r($myprofile,"\n")."</pre>";
         if(($myprofile['role_id'] == $permission_roles['US'] or $myprofile['role_id'] == $permission_roles['CONCM'] or $myprofile['role_id'] == $permission_roles['CONTM']) && ((!isset($concept_manager) && empty($concept_manager) || !in_array($myprofile['id'],$concept_manager)) && (!isset($outlet_manager) || empty($outlet_manager) || !in_array($myprofile['id'],$outlet_manager)))):
        ?>
     <ul>
          <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['user_guide_group_id'];?>/files"><img src="<?php echo site_url(); ?>media/images/concepts/User-Guide.jpg" alt="User Guide" /><div class="text">User Guide</div></a></li>
     </ul>
        
        <?php endif; ?>
        <?php if($myprofile['role_id'] == $permission_roles['AM'] or
                $myprofile['role_id'] == $permission_roles['SA'] or (isset($concept_manager) && !empty($concept_manager) && in_array($myprofile['id'],$concept_manager)) or (isset($outlet_manager) && !empty($outlet_manager) && in_array($myprofile['id'],$outlet_manager)) or $myprofile['role_id'] == $permission_roles['ARV']):
        ?>
         <ul>
          <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['user_guide_group_id'];?>/files"><img src="<?php echo site_url(); ?>media/images/concepts/User-Guide.jpg" alt="User Guide" /><div class="text">User Guide</div></a></li>
          <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['knowledge_center_group_id'];?>/files"><img src="<?php echo site_url(); ?>media/images/concepts/Knowledge-Base.jpg" alt="Knowledge Center" /><div class="text">Knowledge Center</div></a></li>
          <li><a href="<?php echo site_url(); ?>surveys/positive_assurance"><img src="<?php echo site_url(); ?>media/images/concepts/Positive-assurance-icon.jpg" alt="Positive Assurance" /><div class="text">Positive Assurance</div></a></li>
          <?php if(!isset($outlet_manager) || empty($outlet_manager) || !in_array($myprofile['id'],$outlet_manager)): ?>          
          <li><a href="<?php echo site_url(); ?>surveys/audit_report"><img src="<?php echo site_url(); ?>media/images/concepts/AuditReport-Icon.jpg" alt="Audit Reports Internal" /><div class="text">Internal Audit Reports</div></a></li>
          <?php
          endif;          
          if((!isset($concept_manager) || empty($concept_manager) || (isset($outlet_manager) && !in_array($myprofile['id'],$outlet_manager))) && (!isset($outlet_manager) || empty($outlet_manager) || (isset($outlet_manager) && !in_array($myprofile['id'],$outlet_manager)))): ?>
          <li><a href="<?php echo site_url(); ?>groups/<?php print $audit_constant['internal_audit_plan_group_id'];?>/files"><img src="<?php echo site_url(); ?>media/images/concepts/Audit-Plan.jpg" alt="Internal Audit Plan" /><div class="text">Internal Audit Plan</div></a></li>      
          <?php endif; ?>
         </ul>
         <?php endif; ?>

    </div>	<!-- concept-grid -->
	</div> <!-- container -->


    </div>

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>


</body>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".inline").colorbox({inline:true, width:"50%"});
        });
    </script>

</html>