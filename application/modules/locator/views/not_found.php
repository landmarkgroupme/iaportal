<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Page Not Found</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">

<?php $this->load->view('global_header.php'); ?>

<div class="section wrapper clearfix">
	<h2>Page Not found</h2>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">




	</div> <!-- container -->
		<p class="not-found">We could not find the requested Page.</p>

    </div>

<div class="right-contents media-widget">
  </div> <!-- right-contents -->
</div> <!-- section -->



<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
$(function() {
    $("img:below-the-fold").lazyload({
        event : "sporty"
    });
});
$(window).bind("load", function() {
    var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
});



</script>

</body>
</html>
