<?php

class Locator extends General {

  function __construct() {
    parent::__construct();
    $this->load->helper('url');
  }

  function lookup()
  {

    $urlname = $this->uri->segment(1);
    //Make sure if the urlname is a username
    $this->load->model('User');
    if($user = $this->User->getByUsername($urlname))
    {
      if($user['first_name'])
      {
        $profile = $this->load->module('profile/profile');
        echo modules::run('profile/profile/view_profile', $user['id']);
      }
      else
      {
        $this->notfound();
      }

    } else
    {
    	$this->notfound();
    }



  }

  function notfound()
  {
  	header("HTTP/1.1 404 Not Found");
  	$this->load->view('not_found');
  }
}
