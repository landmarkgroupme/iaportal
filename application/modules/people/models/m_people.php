<?php

class M_people extends CI_Model {

  function M_people() {

    parent::__construct();
  }

  function get_all_count($src_attr="") {

    $sql = "select
                    count(user.id) as total_users
            from
                    ci_users user,
                    ci_master_designation designation,
                    ci_master_concept concept,
                    ci_master_department department,
                    ci_master_location location,
                    ci_master_territory territory
            where
                    user.designation_id = designation.id and
                    user.concept_id = concept.id and
                    user.department_id = department.id and
                    user.location_id = location.id and
                    user.territory_id = territory.id {$src_attr}";

    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data = $row['total_users'];
      }
    }

    $qry->free_result();
    return $data;
  }

  function get_all($user_id, $limit, $offset,$src_attr="") {

    $sql = "select
					user.id,
					user.first_name,
					user.middle_name,
					user.last_name,
					user.profile_pic,
					designation.name designation,
					department.name department,
					concept.name concept,
					location.name location,
					territory.name territory,
					user.extension extension,
					user.phone phone,
					user.mobile mobile,
					user.email email,
					(SELECT following_user_id FROM ci_users_following_users WHERE user_id = ? AND follow_status = 1 AND following_user_id = user.id) as following_user_id,
					(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = user.id  AND contact_status = 1 AND user_id = ?)as contact_user
				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory
				where
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id {$src_attr}
					order by TRIM(user.first_name)
				limit $limit
				offset $offset";

    $qry = $this->db->query($sql, array($user_id, $user_id));

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }

    $qry->free_result();
    return $data;
  }

  function get_allchar($user_id, $char, $limit, $offset) {

    $sql = "select
					user.id,
					user.first_name,
					user.middle_name,
					user.last_name,
					user.profile_pic,
					designation.name designation,
					department.name department,
					concept.name concept,
					location.name location,
					territory.name territory,
					user.extension extension,
					user.phone phone,
					user.mobile mobile,
					user.email email,
					(SELECT following_user_id FROM ci_users_following_users WHERE user_id = ? AND follow_status = 1 AND following_user_id = user.id) as following_user_id,
					(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = user.id  AND contact_status = 1 AND user_id = ?)as contact_user
				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory
				where
					lower(user.first_name) like '$char%' and
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id
				order by 2
				limit $limit
				offset $offset";

    $qry = $this->db->query($sql, array($user_id, $user_id));

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  function count_char($char) {

    $sql = "select
					count(*) count
				from
					ci_users user
				where
					lower(user.first_name) like '$char%'";

    $qry = $this->db->query($sql);
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data[0]['count'];
  }

  function concepts($rows) {

    $sql = "select
					id,
					name
				from
					ci_master_concept
				order by
					2";

    $qry = $this->db->query($sql);
    if ($qry->num_rows() > 0) {

      $count = 0;
      foreach ($qry->result_array() as $row) {

        ++$count;
        $data[] = $row;

        if (($count % $rows) == 0) {
          $rowdata[] = $data;
          $data = array();
        }
      }
      if ($data) {
        $rowdata[] = $data;
      }
    }
    $qry->free_result();
    return $rowdata;
  }

  function territories($rows) {

    $sql = "select
					id,
					name
				from
					ci_master_territory
				order by
					2";

    $qry = $this->db->query($sql);
    if ($qry->num_rows() > 0) {

      $count = 0;
      foreach ($qry->result_array() as $row) {

        ++$count;
        $data[] = $row;

        if (($count % $rows) == 0) {
          $rowdata[] = $data;
          $data = array();
        }
      }
      if ($data) {
        $rowdata[] = $data;
      }
    }
    $qry->free_result();
    return $rowdata;
  }

  function search($user_id, $term, $concept, $country, $limit, $offset) {

    $sql = "select
					user.id,
					user.first_name,
					user.middle_name,
					user.last_name,
					user.profile_pic,
					designation.name designation,
					department.name department,
					concept.name concept,
					location.name location,
					territory.name territory,
					user.extension extension,
					user.phone phone,
					user.mobile mobile,
					user.email email,
					(SELECT following_user_id FROM ci_users_following_users WHERE user_id = ? AND follow_status = 1 AND following_user_id = user.id) as following_user_id,
					(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = user.id  AND contact_status = 1 AND user_id = ?)as contact_user
				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory
				where
					(user.first_name like '%$term%' or
					user.last_name like '%$term%') and
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id";

    if ($concept) {
      $sql .= ' and concept_id in (' . implode(', ', $concept) . ')';
    }
    if ($country) {
      $sql .= ' and territory_id in (' . implode(', ', $country) . ')';
    }

    $sql .= " order by 2 limit $limit offset $offset";
    $qry = $this->db->query($sql, array($user_id, $user_id));

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  function search_count($term, $concept, $country) {

    $sql = "select
					count(*) count
				from
					ci_users user
				where
					user.first_name like '%$term%' or
					user.last_name like '%$term%'";

    if ($concept) {
      $sql .= ' and concept_id in (' . implode(', ', $concept) . ')';
    }
    if ($country) {
      $sql .= ' and territory_id in (' . implode(', ', $country) . ')';
    }

    $qry = $this->db->query($sql);

    $row = $qry->result_array();

    $qry->free_result();
    return $row[0]['count'];
  }

  /* My Contacts */

  //Count My Contacts
  function count_my_contact($user_id) {
    $sql = "select
                    count(*) count
            from
                    ci_users_contacts
            where
                    user_id = ? AND contact_status = 1";
    $qry = $this->db->query($sql, $user_id);
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data[0]['count'];
  }

  // My contacts List
  function get_all_my_contact($user_id, $limit, $offset) {
    $sql = "select
					user.id,
					user.first_name,
					user.middle_name,
					user.last_name,
					user.profile_pic,
					designation.name designation,
					department.name department,
					concept.name concept,
					location.name location,
					territory.name territory,
					user.extension extension,
					user.phone phone,
					user.mobile mobile,
					user.email email,
					(SELECT following_user_id FROM ci_users_following_users WHERE user_id = ? AND follow_status = 1 AND following_user_id = user.id) as following_user_id,
					(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = user.id  AND contact_status = 1 AND user_id = ?)as contact_user
				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory,
					ci_users_contacts my_contacts
				where
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id AND
					my_contacts.user_id = ? AND
					my_contacts.contact_users_id = user.id AND
					my_contacts.contact_status = 1
					order by 2
				limit $limit
				offset $offset";

    $qry = $this->db->query($sql, array($user_id, $user_id, $user_id));

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  //Count My contacts, Alphabets
  function count_char_my_contacts($char, $user_id) {
    $sql = "select
							count(*) count
						from
							ci_users user,
							ci_users_contacts my_contacts
						where
							lower(user.first_name) like '$char%' AND
							my_contacts.user_id = ? AND
							my_contacts.contact_users_id = user.id AND
							my_contacts.contact_status = 1";
    $qry = $this->db->query($sql, $user_id);
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data[0]['count'];
  }

  //Alphabet sorting for My contacts
  function get_allchar_my_contacts($user_id, $char, $limit, $offset) {

    $sql = "select
							user.id,
							user.first_name,
							user.middle_name,
							user.last_name,
							user.profile_pic,
							designation.name designation,
							department.name department,
							concept.name concept,
							location.name location,
							territory.name territory,
							user.extension extension,
							user.phone phone,
							user.mobile mobile,
							user.email email,
							(SELECT following_user_id FROM ci_users_following_users WHERE user_id = ? AND follow_status = 1 AND following_user_id = user.id) as following_user_id,
							(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = user.id  AND contact_status = 1 AND user_id = ?)as contact_user
						from
							ci_users user,
							ci_master_designation designation,
							ci_master_concept concept,
							ci_master_department department,
							ci_master_location location,
							ci_master_territory territory,
							ci_users_contacts my_contacts
						where
							lower(user.first_name) like '$char%' and
							user.designation_id = designation.id and
							user.concept_id = concept.id and
							user.department_id = department.id and
							user.location_id = location.id and
							user.territory_id = territory.id AND
							my_contacts.user_id = ? AND
							my_contacts.contact_users_id = user.id AND
							my_contacts.contact_status = 1
						order by 2
						limit $limit
						offset $offset";

    $qry = $this->db->query($sql, array($user_id, $user_id, $user_id));

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  //Get all concepts name from Id's
  function get_concepts_names($concept_id_arr) {
    $sql = "select name from ci_master_concept WHERE id IN({$concept_id_arr})";
    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  //Get all Location name from Id's,
  function get_territory_names($location_id_arr) {
    $sql = "select name from ci_master_territory WHERE id IN({$location_id_arr})";
    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  function get_profile($id) {

    $sql = "select
          user.id,
          user.first_name,
          user.middle_name,
          user.last_name,
          designation.name designation,
          department.name department,
          concept.name concept,
          location.name location,
          territory.name territory,
          user.extension extension,
          user.phone phone,
          user.mobile mobile,
          user.email email,
          user.about about,
          user.skills skills
          from
          ci_users user,
          ci_master_designation designation,
          ci_master_concept concept,
          ci_master_department department,
          ci_master_location location,
          ci_master_territory territory
        where
          user.id = $id and
          user.designation_id = designation.id and
          user.concept_id = concept.id and
          user.department_id = department.id and
          user.location_id = location.id and
          user.territory_id = territory.id";

    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data[0];
  }

  function get_src_user_info($id) {

    $sql = "select
          user.id,
          user.first_name,
          user.middle_name,
          user.last_name,
          user.profile_pic,
          designation.name designation,
          concept.name concept,
          user.extension extension,
          user.phone phone,
          user.mobile mobile
          from
          ci_users user,
          ci_master_designation designation,
          ci_master_concept concept,
          ci_master_department department,
          ci_master_location location,
          ci_master_territory territory
        where
          user.id = $id and
          user.designation_id = designation.id and
          user.concept_id = concept.id and
          user.department_id = department.id and
          user.location_id = location.id and
          user.territory_id = territory.id";

    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data[0];
  }


   /* My Contacts, new search*/

  //Count My Contacts
  function search_count_my_contact($user_id,$src_attr) {
    $sql = "select
					count(user.id) as total_users

				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory,
					ci_users_contacts my_contacts
				where
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id AND
					my_contacts.user_id = ? AND
					my_contacts.contact_users_id = user.id AND
					my_contacts.contact_status = 1 {$src_attr}";
    $qry = $this->db->query($sql, $user_id);
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data = $row['total_users'];
      }
    }
    $qry->free_result();
    return $data;
  }

  // My contacts List
  function search_all_my_contact($user_id, $limit, $offset,$src_attr) {
    $sql = "select
					user.id as id 
				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory,
					ci_users_contacts my_contacts
				where
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id AND
					my_contacts.user_id = ? AND
					my_contacts.contact_users_id = user.id AND
					my_contacts.contact_status = 1 {$src_attr}
					order by TRIM(user.first_name)
				limit $limit
				offset $offset";

    $qry = $this->db->query($sql, array($user_id));

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }




}