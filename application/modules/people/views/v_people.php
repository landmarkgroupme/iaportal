<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
$active_tab['home_active'] = '';
$active_tab['people_active'] = 'class="active"';
$active_tab['market_place_active'] = '';
$active_tab['files_active'] = '';
$active_tab['events_active'] = '';
$active_tab['about_lmg_active'] = '';
$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Landmark Intranet</title>
    <?php $this->load->view("include_files/common_files"); ?>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/people/company_dir.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/people/people.js"></script>

    <script>
      /* Global array for Dept,Concepts, Territories */
      var arr_dept = new Array();
      var arr_concepts = new Array();
      var arr_territories = new Array();
      var show_only_contacts = 0;
      var src_term = '';
      $(function(){
        //Hover show
        $('.list-content').live('mouseover',function(){
          $('.pple-info').hide();
          $(this).find('.pple-info').show();
        })

        $('.list-content').live('mouseout',function(){
          $('.pple-info').hide();
        })

        $('.check').click(function(){
          //alert('sdf');
          //return false;
          var value = $(this).attr('value');// Filter value
          var filter = $(this).attr('title'); //Filter option
          var offset =0;
          if($(this).attr('checked')){
            $('.side-bar label[for=' + this.id +']').removeClass("chk_checked");
            $('.side-bar label[for=' + this.id +']').addClass("chk_unchecked");
            //$(this).next().children('.checked').show();
            //$(this).next().children('.unchecked').hide();
            /* Set Filters */
            switch (filter) {
              case 'contacts': show_only_contacts = 1; break;
              case 'department': arr_dept.push(value); break;
              case 'concept': arr_concepts.push(value); break;
              case 'territory': arr_territories.push(value); break;
              //default: result = 'unknown';
            }

          }else{
            $('.side-bar label[for=' + this.id +']').removeClass("chk_unchecked");
            $('.side-bar label[for=' + this.id +']').addClass("chk_checked");
            //$(this).next().children('.checked').hide();
            //$(this).next().children('.unchecked').show();
            /* Set Filters */
            switch (filter) {
              case 'contacts': show_only_contacts = 0; break;
              case 'department': clear_filter_array(arr_dept,value); break;
              case 'concept': clear_filter_array(arr_concepts,value); break;
              case 'territory': clear_filter_array(arr_territories,value); break;
              //default: result = 'unknown';
            }

          }
          serach_users(offset);
        });
        $('.all').click(function(){
          $(this).parent().next().children().children('.check').attr('checked', true);
          var filter = $(this).attr('title');
          /* Set Filters */
          switch (filter) {
            case 'department': arr_dept = $(this).parent().next().children().children('.check').map(function(){return this.value;}).get(); break;
            case 'concept': arr_concepts = $(this).parent().next().children().children('.check').map(function(){return this.value;}).get(); break;
            case 'territory': arr_territories = $(this).parent().next().children().children('.check').map(function(){return this.value;}).get(); break;
            //default: result = 'unknown';
          }

          //$(this).parent().next().children().children('.check').next().children('.checked').show();
          //$(this).parent().next().children().children('.check').next().children('.unchecked').hide();

          $(this).parent().next().children().children('.chk_checked').addClass('chk_unchecked');
          $(this).parent().next().children().children('.chk_checked').removeClass('chk_checked');


          offset = 0;
          serach_users(offset);
          return false;
        });
        $('.none').click(function(){
          var filter = $(this).attr('title');
          /* Set Filters */
          switch (filter) {
            case 'department': arr_dept = new Array(); break;
            case 'concept': arr_concepts = new Array(); break;
            case 'territory': arr_territories = new Array(); break;
            //default: result = 'unknown';
          }

          $(this).parent().next().children().children('.check').attr('checked', false);
          //$(this).parent().next().children().children('.check').next().children('.checked').hide();
          //$(this).parent().next().children().children('.check').next().children('.unchecked').show();
          $(this).parent().next().children().children('.chk_unchecked').addClass('chk_checked');
          $(this).parent().next().children().children('.chk_unchecked').removeClass('chk_unchecked');
          offset = 0;
          serach_users(offset);

          return false;
        });

        //Search term
        $('#search-contacts').keyup(function(e) {
          if(e.keyCode != 13){
            return false;
          }
          src_term = $('#search-contacts').val();
          offset = 0;
          serach_users(offset);

        });
        $('#search-contacts').focus(function(){
          if($.trim($('#search-contacts').val()) == $('#search-contacts').attr('title')){
            $('#search-contacts').val('');
          }
        })
        //On Blur
        $('#search-contacts').blur(function(){
          if($.trim($('#search-contacts').val()) == ''){
            $('#search-contacts').val($('#search-contacts').attr('title'));
          }
        })

        //Paging option
        $('.paging').click(function(){
          var total = $('.show_total_cnt').html()*1;
          var end_cnt = $('.show_end_cnt').html()*1;

          offset = $(this).attr('rel');
          if(offset <0){
             return false;
          }

          serach_users(offset);
          return false;
        })



        //Add to Fav list
        $(".add-fav").live("click",function(){
          var e = this;
          var service_url = "<?php echo site_url('interest_groups/add_contact')?>";
          var add_contact = this.id;

          //Loading img code
          var img = $('.img_'+add_contact).attr('src');
          var loader = img.replace('non-favourite-b.png','loader.gif')
          $('.img_'+add_contact).attr('src',loader)
          $('.img_'+add_contact).attr('width','18')
          $('.img_'+add_contact).css('margin-top','5px')

          $.ajax({
            url: service_url,
            data: {
              "add_contact":add_contact
            },
            async: true,
            dataType: "json",
            type: "POST",
            success: function(msg){
              if(msg.affected_row >= 1){
                $(e).removeClass("add-fav");
                $(e).addClass("remove-fav");
                var img = $('.img_'+add_contact).attr('src');
                var remove_fav = img.replace('loader.gif','favourite-b.png');
                $('.img_'+add_contact).attr('src',remove_fav);
                $('.img_'+add_contact).css('margin-top','0px')
              };
            }
          });
          return false;
        })

        //Remove from Fav list
        $(".remove-fav").live("click",function(){
          var e = this;
          var service_url = "<?php echo site_url('interest_groups/remove_contact')?>";
          var remove_contact = this.id;
           //Loading img code
          var img = $('.img_'+remove_contact).attr('src');
          var loader = img.replace('favourite-b.png','loader.gif');
          $('.img_'+remove_contact).attr('src',loader)
          $('.img_'+remove_contact).attr('width','18');
          $('.img_'+remove_contact).css('margin-top','5px');
          $.ajax({
            url: service_url,
            data: {
              "remove_contact":remove_contact
            },
            async: true,
            dataType: "json",
            type: "POST",
            success: function(msg){
              if(msg.affected_row >= 1){
                $(e).removeClass("remove-fav");
                $(e).addClass("add-fav");
                var img = $('.img_'+remove_contact).attr('src');
                var remove_fav = img.replace('loader.gif','non-favourite-b.png');
                $('.img_'+remove_contact).attr('src',remove_fav);
                $('.img_'+remove_contact).css('margin-top','0px');
              };
            }
          });
          return false;
        })

				//Email show
				$('.email a').live('mouseover',function(){
					var email_id = $(this).attr('rel');
          $('.cp-email-show').hide();
					var img = $("a[rel ='"+email_id+"'] img").attr('src');
					var ori_img = img.replace('email-ico.png','email-h-ico.png')
					$("a[rel ='"+email_id+"'] img").attr('src',ori_img);
					$('#'+email_id).show();
        })
        $('.email a').live('mouseout',function(){
					var email_id = $(this).attr('rel');
					var img = $("a[rel ='"+email_id+"'] img").attr('src');
					var ori_img = img.replace('email-h-ico.png','email-ico.png')
					$("a[rel ='"+email_id+"'] img").attr('src',ori_img);
          $('.cp-email-show').hide();
        })

				//Name show
				$('.person-name a').live('mouseover',function(){
					var name = $(this).attr('rel');
          $('.cp-name-show').hide();
					$('#'+name).show();
        })
        $('.person-name a').live('mouseout',function(){
          $('.cp-name-show').hide();
        })




       });

      /* Common search logic */
      function serach_users(offset){

       $('#pb_loading_mask').show();
       $('#pb-loading-img').css('margin-top',$(window).scrollTop()+"px")
       $('#loading-img').show();

       if($.trim($('#search-contacts').val()) == $('#search-contacts').attr('title')){
          src_term = '';
        }else{
          src_term = $.trim($('#search-contacts').val());
        }
        $.ajax({
          url: base_url+"people/phone_book_src",
          data: {"show_only_contacts":show_only_contacts,'dept[]':arr_dept,'concept[]':arr_concepts,'territory[]':arr_territories,'offset':offset,'src_term':src_term},
          async: true,
          dataType: "json",
          type: "POST",
          success: function(data){
            $('#pb_loading_mask').hide();
            if(data['total_count']){
              $('#show_user_list').html(data['user_list']);
              $('.show_total_cnt').html(data['total_count']);
              $('.show_start_cnt').html(data['show_start_cnt']);
              $('.show_end_cnt').html(data['show_end_cnt']);
              if(data['pg_nxt'] < data['total_count'] ){
                $('.show_next').attr('rel',data['pg_nxt']);
              }
              $('.show_prev').attr('rel',data['pg_prev']);
							$('.paging-bar').show();
            }else{
              $('#show_user_list').html(data['user_list']);
              $('.show_total_cnt').html('0');
              $('.show_start_cnt').html('0');
              $('.show_end_cnt').html('0');
              $('.show_next').attr('rel','0');
              $('.show_prev').attr('rel','0');
							$('.paging-bar').hide();
            }

          }
        });
      }
      /*Clearing Filter array*/
      function clear_filter_array(arrayName,arrayElement){
        for(var i=0; i<arrayName.length;i++ ){
            if(arrayName[i]==arrayElement){
              arrayName.splice(i,1);
            }
          }
        }
    </script>
  </head>
  <body>
    <!-- wrapper -->
    <div id="wrapper" class="phonebook">
    <?php $this->load->view("includes/admin_nav"); ?>
      <!-- logo -->
      <h1 class="logo vcard"><a href="#" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
      <!-- main -->
      <div id="main">
        <div class="search-div">
          <h1>Find contacts</h1>
          <input type="text" name="search-contacts" id="search-contacts" title="Search by name, designation, concept, territory" value="<?php if(trim($src_term)){echo $src_term;}else{echo 'Search by name, designation, concept, territory';}?>"/>
          <img src="<?php echo base_url()?>images/search-back-f.png" style="display: none;" alt="bg-preload" />
          <div class="paging-bar">
              <span class="previous"><a class="show_prev paging" rel="<?php echo $pg_prev; ?>" href="#"><img src="<?php echo base_url()?>images/prev-back.png" /></a></span>
              <span class="results"><strong><?php echo "<span class='show_start_cnt'>$first</span> &ndash; <span class='show_end_cnt'>$last</span>"; ?></strong><span id="of"> of</span> <strong><span class='show_total_cnt'><?php echo $total; ?></span></strong></span>
              <span id="next"><a class="show_next paging" rel="<?php echo $pg_nxt;?>" href="#"><img src="<?php echo base_url();?>images/next-back.png"/></a></span>
            </div>
        </div>
        <div class="main-content">
          <div class="side-bar">
            <div class="filter" >
            Filter your results:
            </div>
            <div class="side-div" id="first-div">
              <div class="category"><img src="<?php echo base_url() ?>images/favourite-s.png"/><span style="margin-left:5px;">Favourites:</span></div>
              <ul>
                <li><input type="checkbox" title="contacts" value="show" id="show_fav" class="check"/><label class='chk_checked' for="show_fav"><span>Show only favourites</span></label></li>
              </ul>
            </div>
            <div class="side-div">
              <div class="category" style="float:left;margin-top:2px;margin-right:-4px;">Departments: &nbsp;</div><span><a href="#" title="department" class="all">All</a>, <a href="" title="department" class="none">None</a></span>
              <ul>
                <?php foreach($departments as $dept):?>
                  <li><input type="checkbox" title="department" value="<?php echo $dept['id'];?>" id="<?php echo $dept['name'];?>" class="check"/><label class='chk_checked' for="<?php echo $dept['name'];?>"><span><?php echo $dept['name'];?></span></label></li>
                <?php endforeach;?>
              </ul>
            </div>
            <div class="side-div">
              <div class="category" style="float:left;margin-top:2px;margin-right:-4px;">Concepts: &nbsp;</div><span><a href="#" title="concept" class="all">All</a>, <a href="#" title="concept" class="none">None</a></span>
              <ul>
                <?php foreach($concepts as $concept):?>
                <li><input type="checkbox" title="concept" id="<?php echo $concept['name'];?>" value="<?php echo $concept['id'];?>" class="check"/><label  class='chk_checked' for="<?php echo $concept['name'];?>"><span><?php echo $concept['name'];?></span></label></li>
               <?php endforeach;?>
              </ul>
            </div>
            <div class="side-div">
              <div class="category" style="float:left;margin-top:2px;margin-right:-4px;">Territories: &nbsp;</div><span><a href="#" title="territory" class="all">All</a>, <a href="#" title="territory" class="none">None</a></span>
              <ul>
                <?php foreach($countries as $territory):?>
                <li><input type="checkbox" title="territory" id="<?php echo $territory['name'];?>" value="<?php echo $territory['id'];?>" class="check"/><label class='chk_checked'  for="<?php echo $territory['name'];?>"><span><?php echo $territory['name'];?></span></label></li>
                <?php endforeach;?>
              </ul>
            </div>
          </div>
          <div class="contact-list-content">
            <div class="list-header">
              <div class="name-desig">Name | Designation | Concept | Territory</div>
              <div class="email">&nbsp;</div>
              <div class="board">Board</div>
              <div class="mobile">Mobile</div>
              <div class="extn">Extn.</div>

            </div>
            <div id="pb_loading_mask"><img id='pb-loading-img' src="<?php echo base_url()?>images/loading-pb.gif" alt='loader' /></div>
            <div id="show_user_list">

            <?php $this->load->view('v_src_list');?>
            </div>

              <div class="search-div" style="border-top:1px solid #bebebe;">
              <div class="paging-bar" style="margin-right:-6px;">
                <span class="previous"><a class="show_prev paging" rel="<?php echo $pg_prev; ?>" href="#"><img src="<?php echo base_url()?>images/prev-back.png" /></a></span>
                <span class="results"><strong><?php echo "<span class='show_start_cnt'>$first</span> &ndash; <span class='show_end_cnt'>$last</span>"; ?></strong><span id="of"> of</span> <strong><span class='show_total_cnt'><?php echo $total; ?></span></strong></span>
                <span id="next"><a class="show_next paging" rel="<?php echo $pg_nxt;?>" href="#"><img src="<?php echo base_url();?>images/next-back.png"/></a></span>
              </div>

            </div>

          </div>

        </div>

      </div>
      <!-- header -->
      <div id="header">
        <div class="header-holder">
          <!-- navigation -->
          <?php $this->load->view("includes/navigation", $active_tab); ?>
          <div class="head-bar">
            <!-- sub navigation -->
            <ul class="subnav">
              <li class="active"><a href="<?= base_url() ?>index.php/people" tabindex="15"><span>Company Directory</span></a></li>
              <li><a href="<?= base_url() ?>index.php/status_update" tabindex="16"><span>Status Updates</span></a></li>
              <li><a href="<?= base_url() ?>index.php/interest_groups" tabindex="17"><span>Interest Groups</span></a></li>
            </ul>
            <!-- header search form -->

          </div>
        </div>
      </div>
      <input type="hidden" value="<?php echo $page_limit;?>" id="page_limit" />
      <!-- footer -->
      <?php $this->load->view("includes/footer"); ?>
    </div>
  </body>
</html>