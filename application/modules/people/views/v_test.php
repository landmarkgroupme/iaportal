<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" href="<?php echo base_url() ?>css/blueprint/screen.css" type="text/css" media="screen, projection" />  
		<link rel="stylesheet" href="<?php echo base_url() ?>css/blueprint/print.css" type="text/css" media="print" />  
		<!--[if lt IE 8]><link rel="stylesheet" href="<?php echo base_url() ?>css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->  
		<link rel="stylesheet" href="<?php echo base_url() ?>css/ecards.css" type="text/css" media="screen" />  
		<script type="text/javascript" src="<?php echo base_url() ?>js/jquery/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>js/autocomplete/jquery.autocomplete.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>js/ecards_autocomplete.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>js/nicedit/nicEdit.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>js/ecards_nicedit.js"></script>
		<title>CodeIgniterTest Page</title>
    </head>
	<body>
		<?php echo $this->pagination->create_links(); ?>
	</body>
</html>	