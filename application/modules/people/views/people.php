<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title>People - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width" id="people-page">

<?php $this->load->view('global_header'); ?>

<div class="section wrapper clearfix">
	<h2>People</h2>
	<ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li>People</li>
		</ul>
</div>

<div class="section wrapper clearfix"> 

<div class="left-contents">

	<div class="container">

		<?php
		$filters_data = array('search_placeholder' => 'Search by name', 'searched' => $term);
		$this->load->view('partials/filters', $filters_data);
		?>

		<div class="people-grid-view clearfix">
			<ul class="sort">
	    		<li><a href="#">A</a></li>
	    		<li><a href="#">B</a></li>
	    		<li><a href="#">C</a></li>
	    		<li><a href="#">D</a></li>
	    		<li><a href="#">E</a></li>
	    		<li><a href="#">F</a></li>
	    		<li><a href="#">G</a></li>
	    		<li><a href="#">H</a></li>
	    		<li><a href="#">I</a></li>
	    		<li><a href="#">J</a></li>
	    		<li><a href="#">K</a></li>
	    		<li><a href="#">L</a></li>
	    		<li><a href="#">M</a></li>
	    		<li><a href="#">N</a></li>
	    		<li><a href="#">O</a></li>
	    		<li><a href="#">P</a></li>
	    		<li><a href="#">Q</a></li>
	    		<li><a href="#">R</a></li>
	    		<li><a href="#">S</a></li>
	    		<li><a href="#">T</a></li>
	    		<li><a href="#">U</a></li>
	    		<li><a href="#">V</a></li>
	    		<li><a href="#">W</a></li>
	    		<li><a href="#">X</a></li>
	    		<li><a href="#">Y</a></li>
	    		<li><a href="#">Z</a></li>
    		</ul>
			
        	<!-- <span class="qTip" title="Test 2222">?</span> -->
			<ul class="view">
				<li class="l-grid-view"><a id="l-grid-view" href="#" title="Show people in a grid view">&nbsp;</a></li>
				<li class="l-list-view active"><a id="l-list-view" href="#" title="Show people in a list view">&nbsp;</a></li>
			</ul>
		</div>
		<div class="no-result-block d-n">No Users found.</div>

		<div class="table people-list">

			<div class="row head">
				<div class="col1">
					<ul class="basic-info">
						<li class="head">Name<br/><span>Designation, Concept, Territory</span></li>
					</ul>
				</div>
				<div class="col2">
					<ul class="contact-info">
						<li>Board / Mobile</li>
						<li class="ext">Ext. No.</li>
						<li><span class="qTip">?</span></li>
					</ul>
				</div>
			</div>
			<!-- ajax based people list data loads here -->
		</div>

    	<div class="people-grid clearfix d-n">
			<!-- ajax based people card data loads here -->
    	</div>


	<div class="loader-more clearfix">&nbsp;</div>


	</div> <!-- container -->


    </div>

	<div class="right-contents media-widget">

    </div> <!-- right-contents -->
</div> <!-- section -->


<?php $this->load->view('templates/js/people'); ?>

<?php $this->load->view('templates/js/people_list'); ?>

<?php $this->load->view('global_footer'); ?>

<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
	var limit = 10;
	var start = 0;
	var nearToBottom = 250;
	var character = '';
$(document).ready(function() {
	//console.log('here');

    $( "#txtSearch" ).autocomplete({
      	source: siteurl + 'people/autocomplete_ajax?page=people',
		html: true,
		position: { my: "left-11 top", at: "left bottom", collision: "none" },
		appendTo: "#filter-search .search",
		select: function (a, b) {
			$(this).val(b.item.id);
			//window.location.href = siteurl + b.item.value;
			start = 0;
			loadPeople({'noclear': 0});

			return false;
		},
    });
	$( "#txtSearchMobile" ).autocomplete({
      	source: siteurl + 'people/autocomplete_ajax?page=people',
		html: true,
		position: { my: "left-11 top", at: "left bottom", collision: "none" },
		appendTo: "#filter-searchMobile .search",
		select: function (a, b) {
			$(this).val(b.item.id);
			window.location.href = siteurl + b.item.link;
			start = 0;
			loadPeople({'noclear': 0});

			return false;
		},
    });

	$('.loader-more').show();
	loadPeople({'noclear': 0});

	$('#ddConcepts, #ddDepartments, #ddCountry').change(function(){
		start = 0;
		character = '';
		$('#txtSearch').val('');
		loadPeople({'noclear': 0});


	});
	$('#txtSearch').enterKey(function(){
		start = 0;
		character = '';
		//$('#ddConcepts').focus();
		loadPeople({'noclear': 0, 'searched': 1});
		$(this).blur();
		//e.preventDefault();
    	$('#ddConcepts, #ddDepartments, #ddCountry').val('0');

    	return false;

	});

	$('ul.sort li a').click(function(a){
		start = 0;
		//$('#ddConcepts').focus();
		$('#txtSearch').val('');
		loadPeople({'noclear': 0, 'searched': 1, 'character': $(this).text()});
	  //e.preventDefault();
  	//$('#ddConcepts, #ddDepartments, #ddCountry').val('0');
  	$('ul.sort li a').removeClass('active');
  	$(this).addClass('active');
    return false;
	});

	
	$('#go').on( "click", function() {		
		start = 0;
		character = '';
		//$('#ddConcepts').focus();
		loadPeople({'noclear': 0, 'searched': 1});
		//e.preventDefault();
    	$('#ddConcepts, #ddDepartments, #ddCountry').val('0');
		$(this).blur();
    	return false;
	});
	$('#goMobile').on( "click", function() {
		var searched = $('#txtSearchMobile').val();
		if(searched == '')
		{
			$("#txtSearchMobile").attr("placeholder", "Please enter keywords").placeholder();
			return false;
		}
		start = 0;
		character = '';
		//$('#ddConceptsMobile').focus();
		loadPeopleMobile({'noclear': 0, 'searched': 1});
		//e.preventDefault();
    	$('#ddConceptsMobile, #ddDepartmentsMobile, #ddCountryMobile').val('0');

    	return false;
	});
	$('#ddConceptsMobile, #ddDepartmentsMobile, #ddCountryMobile').change(function(){
		start = 0;
		character = '';
		$('#txtSearchMobile').val('');
		loadPeopleMobile({'noclear': 0});
		$(this).blur();
		 return false;
	});
		$('#clearMobile').on( "click", function() {
		start = 0;
		$('#ddDepartmentsMobile').val('0');
		$('#ddConceptsMobile').val('0');
		$('#ddCountryMobile').val('0');
		loadPeopleMobile({'noclear': 0});

	});
	
	$('#clear').on( "click", function() {
		start = 0;
		$('#ddDepartments').val('0');
		$('#ddConcepts').val('0');
		$('#ddCountry').val('0');
		loadPeople({'noclear': 0});

	});

	$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			start += 10;
		if($('.media-show').is(':visible'))
		{
			loadPeopleMobile({'noclear': 1, 'scroll': true});
		} else {
			loadPeople({'noclear': 1, 'scroll': true});
		}
		 	
		}
	});
});

function loadPeopleMobile(prop)
{
	//console.log(prop);
	//return;
	$('.loader-more').show();
	var searched = ($('#txtSearchMobile').val() == '' || $('#txtSearchMobile').val() == 'Search by name') ? false : true;

	if(!prop.noclear)
	{
		$('.people-grid').html('');
		$('.people-list .row:not(.head)').remove();
	}


	var service_url = siteurl+"people/people_ajax";

	
	if(prop.character)
	{
		character = prop.character
	} 
	else if(character && character != '')
	{
		
	}
	else {
	character = '';
		$('ul.sort li a').removeClass('active');
		
	}
	//console.log(character);
		

	if(searched)
	{
		var search = $('#txtSearchMobile').val();
		var data = {'limit' : limit, 'start' : start, 'search_term': search};
		
	} else if(character && character != '')
	{
        var concept = $('#ddConceptsMobile').val();
		var department = $('#ddDepartmentsMobile').val();
		var country = $('#ddCountryMobile').val();
		var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country,'search_char': character};
	}
	else {
		var concept = $('#ddConceptsMobile').val();
		var department = $('#ddDepartmentsMobile').val();
		var country = $('#ddCountryMobile').val();
		var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country};
	}



     $.ajax({
      url: service_url,
      data: data,
      async: false,
	  cache: false,
      dataType: "json",
      type: "GET",
      success: function(data){

      	if(_.size(data) > 0)
      	{
      		$('.no-result-block').addClass('d-n');

      		_.templateSettings.variable = "rc";

	        // Grab the HTML out of our template tag and pre-compile it.
	        var template = _.template(
	            $( "#people_card" ).html()
	        );


	        $('div.people-grid').append(template(data));

	        var template = _.template(
	            $( "#people_list" ).html()
	        );

	        $('div.people-list').append(template(data));

	        if($('.l-list-view').hasClass('active')) {
	        	$('.people-list').css('display', 'table');
	      		$('.people-grid').css('display', 'none');
	        } else {
	        	$('.people-list').css('display', 'none');
	      		$('.people-grid').css('display', 'table');
	        }
	        $('.people-grid-view').removeClass('d-n');

      	} else {
      		if(!prop.scroll)
      		{
      			$('.no-result-block').text('No Users found for specified criteria');
	      		$('.no-result-block').removeClass('d-n');
	      		$('.people-list .row:not(.head)').remove();
	      		$('.people-list').css('display', 'none');
	      		$('.people-grid').css('display', 'none');
	      		//$('.people-grid-view').addClass('d-n');
      		}



      	}

		$('.loader-more').hide();
      }
    });
}
function loadPeople(prop)
{
	//console.log(prop);
	//return;

	 var pstorageview = sessionStorage.getItem("people_grid_view");
	if (pstorageview != null){
		$('.l-grid-view').addClass('active');	
		$('.l-list-view').removeClass('active');			
	}
	
	$('.loader-more').show();
	var searched = ($('#txtSearch').val() == '' || $('#txtSearch').val() == 'Search by name' || $('#txtSearch').val() == 'Please enter keywords') ? false : true;

	if(!prop.noclear)
	{
		$('.people-grid').html('');
		$('.people-list .row:not(.head)').remove();
	}


	var service_url = siteurl+"people/people_ajax";

	
	if(prop.character)
	{
		character = prop.character
	} 
	else if(character && character != '')
	{
		
	}
	else {
	character = '';
		$('ul.sort li a').removeClass('active');
		
	}
	//console.log(character);
		

	if(searched)
	{
		var search = $('#txtSearch').val();
		var data = {'limit' : limit, 'start' : start, 'search_term': search};
		
	} else if(character && character != '')
	{
    var concept = $('#ddConcepts').val();
		var department = $('#ddDepartments').val();
		var country = $('#ddCountry').val();
		var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country,'search_char': character};
	}
	else {
		var concept = $('#ddConcepts').val();
		var department = $('#ddDepartments').val();
		var country = $('#ddCountry').val();
		var data = {'limit' : limit, 'start' : start, 'concept': concept, 'department': department, 'country': country};
	}



     $.ajax({
      url: service_url,
      data: data,
      async: false,
	  cache: false,
      dataType: "json",
      type: "GET",
      success: function(data){

      	if(_.size(data) > 0)
      	{
      		$('.no-result-block').addClass('d-n');

      		_.templateSettings.variable = "rc";

	        // Grab the HTML out of our template tag and pre-compile it.
	        var template = _.template(
	            $( "#people_card" ).html()
	        );


	        $('div.people-grid').append(template(data));

	        var template = _.template(
	            $( "#people_list" ).html()
	        );

	        $('div.people-list').append(template(data));

	        if($('.l-list-view').hasClass('active')) {
	        	$('.people-list').css('display', 'table');
	      		$('.people-grid').css('display', 'none');
	        } else {
	        	$('.people-list').css('display', 'none');
	      		$('.people-grid').css('display', 'table');
	        }
	        $('.people-grid-view').removeClass('d-n');
	        var pstorageview = sessionStorage.getItem("people_grid_view");
			if (pstorageview != null){
				$('.people-grid').css("display" , "block");
				$('.people-list').css("display" , "none");	
			}

      	} else {
      		if(!prop.scroll)
      		{
      			$('.no-result-block').text('No Users found for specified criteria');
	      		$('.no-result-block').removeClass('d-n');
	      		$('.people-list .row:not(.head)').remove();
	      		$('.people-list').css('display', 'none');
	      		$('.people-grid').css('display', 'none');
	      		//$('.people-grid-view').addClass('d-n');
      		}



      	}

		$('.loader-more').hide();
      }
    });
}


$(window).load(function(){
		var tourSubmitFunc = function(e,v,m,f){
			if(v === -1){
				$.prompt.prevState();
				return false;
			}
			else if(v === 1){
				$.prompt.nextState();
				return false;
			}
			else if(v === 0){
				$.prompt.goToState(0);
				return false;
			}
			
			
			
},
tourStates = [
	{
		title: 'Follow a colleague',
		html: 'Follow a colleague to get their updates right in your feed',
		buttons: { Next: 1 },
		focus: 0,
		position: { container: '.f-action', x: 650, y: 274, width: 300, arrow: 'rt' },
		submit: tourSubmitFunc
	},
	{
		title: 'Filter',
		html: 'Filter to find the right person!',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '#fFilter', x: 0, y: 30, width: 300, arrow: 'tl' },
		submit: tourSubmitFunc
	},
	{
		title: 'View type',
		html: 'View in either list or grid view',
		buttons: { Prev: -1, First: 0, Done: 2 },
		focus: 0,
		position: { container: '.view', x: -270, y: 35, width: 300, arrow: 'tr' },
		submit: tourSubmitFunc
	}
];
if(<?php echo $user_prompt;?>)
{
	$.prompt(tourStates);
	var data1 = {'user_id' : myprofile.id, 'feature' : 'People'};
	var url = siteurl+"status_update/add_prompt";
	$.ajax({
		url: url,
		data: data1,
		async: false,
		dataType: "json",
		type: "POST",
		success: function(data){

		}
	});
}
});


</script>

</body>
</html>