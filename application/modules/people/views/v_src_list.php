<?php $zeb_strip = 0; if(count($people)):
foreach ($people as $person) : ?>
  <div class="list-content <?php if ($zeb_strip == 0) {
    echo 'alter1';
    $zeb_strip = 1;
  } else {
    echo 'alter2';
    $zeb_strip = 0;
  } ?>">
  <?php
  $person = array_map('stripslashes', $person) ;
  /* Shortening lenthy info */
  $allowed_length = 60;
  $info_name = ucwords(strtolower($person['first_name'])) . ' ' . ucwords(strtolower($person['last_name']));
	$formated_name = (strlen($info_name)>25)? substr($info_name,0,25).'..':$info_name;
	
	
 // $info_name = stripslashes($info_name);
 	//$person['designation'] = (strlen($person['designation'])>25)? substr($person['designation'],0,25).'..':$person['designation'];
  $info_details = ucwords(strtolower($person['designation'])) . ', ' . ucwords(strtolower($person['concept'])) . ', ' . $person['territory'];
  $current_len = strlen($info_name) + strlen($info_details);
	if ($current_len > $allowed_length) {
    if (strlen($info_details) > ($allowed_length - strlen($info_name))) {
			$designation = ucwords(strtolower($person['designation']));
			$info_details =  substr($designation,0,15). '.., ' . ucwords(strtolower($person['concept'])) . ', ' . $person['territory'];
		} 
  }



  if(strpos($person['mobile'], '00') === 0){
    $person['mobile'] = '+'.substr($person['mobile'],2,strlen($person['mobile']));
  }
  if(strpos($person['phone'], '00') === 0){
    $person['phone'] = '+'.substr($person['phone'],2,strlen($person['phone']));
  }
  if(strlen($person['extension']) > 4){
    $person['extension'] = '';
  }

  //echo $current_len = strlen($info_name)+strlen($info_details);
  //exit;
  ?>
  <div class="name-desig">
  	<a style="float:left;margin-top:-2px;" class="<?= ($person['contact_user']) ? "remove-fav" : "add-fav" ?>" id="<?php echo $person['id'];?>" href="#"><img class="img_<?php echo $person['id'];?>" src="<?php echo base_url() ?>images/<?= ($person['contact_user']) ? "favourite-b.png" : "non-favourite-b.png" ?>" /></a><img class="cp-user-pic" height="23" width="23" src="<?php echo base_url()?>images/user-images/105x101/<?php echo $person['profile_pic'];?>" alt="img" />
  	<?php if(strlen($info_name)>25):?>
    <span id="show_name_<?php echo $person['id'];?>" class="cp-name-show"><span><?php echo $info_name;?></span></span>
    <?php endif;?>
  	<span class="person-name"><a rel="show_name_<?php echo $person['id'];?>"  href="<?php echo site_url('profile/view_profile/'.$person['id']);?>"><strong><?php echo $formated_name; ?></a> </strong></span><span id="name-hover-<?php echo $person['id'];?>" class="name-hover"><?php // echo $info_name; ?></span>
  	<span class="desig-con"><?php echo $info_details; ?></span> 
  </div>
  <div class="email"><span id="show_email_<?php echo $person['id'];?>" class="cp-email-show"><span><?php echo $person['email'];?></span></span><a rel="show_email_<?php echo $person['id'];?>" href="mailto:<?php echo $person['email'];?>"><img src="<?php echo base_url()?>images/email-ico.png" alt="img" /></a></div>
  <div class="board"><?php echo (trim($person['phone'])) ? $person['phone'] : '&nbsp;'; ?></div>
  <div class="mobile"><?php echo (trim($person['mobile'])) ? $person['mobile'] : '&nbsp;'; ?></div>
  <div class="extn"><strong><?php echo (trim($person['extension'])) ? $person['extension'] : '&nbsp;'; ?></strong></div>
  <!--div style="clear:both;">  
    <div class="pple-info">
      <div class='pple-info-h'>
        <div class='pple-info-rep'>
          <div class='pple-info-pic'><img height="81" src="<?php echo base_url()?>images/user-images/105x101/<?php echo $person['profile_pic'];?>" alt="img" /></div>
          <div class="pple-info-desc">
            <div> <span class="ph-ext"><?php echo (trim($person['extension'])) ? $person['extension'] : '&nbsp;'; ?></span> <span class="person_name"><a class="person_name" href="<?php echo site_url('profile/view_profile/'.$person['id']);?>"><?php echo (strlen($info_name) > 25)? substr($info_name,0,25).'..':$info_name; ?></a></span> <a class="<?= ($person['contact_user']) ? "remove-fav" : "add-fav" ?>" id="<?php echo $person['id'];?>" href="#"><img class="img_<?php echo $person['id'];?>" src="<?php echo base_url() ?>images/<?= ($person['contact_user']) ? "favourite-b.png" : "non-favourite-b.png" ?>" /></a> </div>
            <div><span class="person_contact"><?php echo (trim($person['mobile'])) ? 'M: '.$person['mobile'] : '&nbsp;'; ?></span><span class="person_info"><?php echo (strlen($person['designation']) > 25)? substr($person['designation'],0,25).'..':$person['designation']; ?>, <?php echo ucfirst(strtolower($person['concept']));?> </span></div>
            <div><span class="person_contact"><?php echo (trim($person['phone'])) ? 'B: '.$person['phone'] : '&nbsp;'; ?></span><span class="person_info"><?php echo (strlen($person['department']) > 25)? substr($person['department'],0,25).'..':$person['department']; ?>, <?php echo ucfirst(strtolower($person['concept']));?></span></div>
            <div><span class="person_contact"><a href="mailto:<?php echo $person['email'];?>"><?php echo $person['email'];?></a></span><span class="person_info"><?php echo (strlen($person['location']) > 20)? substr($person['location'],0,20).'..': $person['location']; ?>, <?php echo $person['territory'];?> </span></div>
          </div>
        </div>
      </div>
      <div class='pple-info-f'></div>
    </div>
  </div -->
</div>
<?php endforeach;  else: ?>
  <div class="list-content alter1">No user found</div>
 <?php endif; ?>