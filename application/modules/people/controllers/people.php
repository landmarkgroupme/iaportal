<?php
use com\google\i18n\phonenumbers\PhoneNumberUtil;
use com\google\i18n\phonenumbers\PhoneNumberFormat;
use com\google\i18n\phonenumbers\NumberParseException;

require_once(APPPATH . 'modules/libphone/PhoneNumberUtil.php');

function phoneNumber($territory)
{
    $cc =   & get_instance();
    $cc->db->select('name');
    $cc->db->from('ci_master_territory');
    $query = $cc->db->get();
    $ccq = $query->result();
// print_r($ccq);
    $countryName = array();
    foreach ($ccq as $key => $value) {
        if ($value->name == "UAE") {
            $countryName['AE'] = $value->name;
        }
        if ($value->name == "KSA") {
            $countryName['SA'] = $value->name;
        }
        if ($value->name == "Kuwait") {
            $countryName['KW'] = $value->name;
        }
        if ($value->name == "Bahrain") {
            $countryName['BH'] = $value->name;
        }
        if ($value->name == "Oman") {
            $countryName['OM'] = $value->name;
        }
        if ($value->name == "Qatar") {
            $countryName['QA'] = $value->name;
        }
        if ($value->name == "Jordan") {
            $countryName['JO'] = $value->name;
        }
        if ($value->name == "Lebanon") {
            $countryName['LB'] = $value->name;
        }
        if ($value->name == "Egypt") {
            $countryName['EG'] = $value->name;
        }
    }

    // print_r($profile['territory']);exit;
    $countryKey = array_search($territory, $countryName);
    return $countryKey;
}

Class People extends General
{

    private $per_page = 45;
    private $page_conf = array();

//Constructor function
    function __construct()
    {

        parent::__construct();

        $this->load->model('Concept');
        $this->load->model('Territory');
        $this->load->model('Department');
        $this->load->model('UserFollow');
        $this->load->model('Prompt');
        $this->load->library('intranet');


        $this->load->model('Survey');
        $this->load->model('Outlets');
        $this->config->load('audit');

        $data = array();

        $data['concepts'] = $this->Concept->getAll(null, array('name' => 'ASC'));
        $data['countries'] = $this->Territory->getAll();
        $data['departments'] = $this->Department->getAll();
        $user_id = $this->session->userdata('userid');
        $filters = array(
            'feature' => 'People',
            'user_id' => $user_id
        );
        $prompt_data = $this->Prompt->getByFilters($filters);
        if (isset($prompt_data) && $prompt_data != '') {
            $data['user_prompt'] = 0;
        } else {
            $data['user_prompt'] = 1;
        }
        $this->load->vars($data);
    }

    /* Private function for Newcommer */

    function _newcommer_list()
    {
        $newcommer_show_limit = 3;
        $user_id = $this->session->userdata('userid');
        $db_newcommer = $this->general_model->gn_newcomers($user_id, $newcommer_show_limit); //General Model function for New Commers
        for ($i = 0; $i < count($db_newcommer); $i++) {
            $contact_user = $db_newcommer[$i]['contact_user'];
            $profile_pic = base_url() . "images/user-images/105x101/" . $db_newcommer[$i]['profile_pic'];
            if (!$db_newcommer[$i]['profile_pic']) {
                $db_newcommer[$i]['profile_pic'] = "default.jpg";
            } else if (!@fopen($profile_pic, "r")) {
                $db_newcommer[$i]['profile_pic'] = "default.jpg";
            }
            $id = $db_newcommer[$i]['id'];
            if ($contact_user) {
                $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="remove-contact">Remove from my contacts</a>';
            } else {
                $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="add-contact">Add to My Contacts</a>';
            }
        }
        return $db_newcommer;
    }

    /* Private function for Interest Groups */

    function _interest_group_list()
    {
        $show_limit = 3;
        $user_id = $this->session->userdata('userid');
        $db_interest_groups = $this->general_model->gn_intrest_groups($user_id, $show_limit); //General Model function for Interest Groups
        if (!count($db_interest_groups)) {
            $db_interest_groups = array();
        }
        return $db_interest_groups;
    }

    /* Private function for My Reminders */

    function _my_reminders()
    {
        $user_id = $this->session->userdata('userid');
        $time = time();
        $this->load->model('market_place/market_place_model');
        $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($user_id);
        $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($user_id);
        $db_event_reminders = $this->general_model->gn_my_event_reminders($user_id, $time);
        $my_reminders['count_item_sale'] = count($db_item_sale);
        $my_reminders['count_item_requested'] = count($db_item_requested);
        $my_reminders['count_event_reminders'] = count($db_event_reminders);
        return $my_reminders;
    }

//Default page for people, basically Phonebook page


    function people_ajax()
    {

        $user_id = $this->session->userdata('userid');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('start');
        $filters = array();
        $search_term = $this->input->get('search_term');
        $search_char = $this->input->get('search_char');
        $concept = $this->input->get('concept');
        $department = $this->input->get('department');
        $country = $this->input->get('country');
        $group = $this->input->get('group');
        if ($concept != '0')
            $filters['concept'] = $concept;
        if ($department != '0')
            $filters['department'] = $department;
        if ($country != '0')
            $filters['country'] = $country;
        if ($search_term != '')
            $filters['search_term'] = $search_term;
        if ($search_char != '')
            $filters['search_char'] = $search_char;
        if (isset($group) && !empty($group) && $group != 0)
            $filters['group'] = $group;

        $users = $this->User->getAllUsers($user_id, $limit, $offset, $filters);

        foreach ($users as $key => $user) {

            $users[$key]['loged_user_id'] = $user_id;
            $users[$key]['first_name'] = ucwords(strtolower($user['first_name']));
            $users[$key]['middle_name'] = ucwords(strtolower($user['middle_name']));
            $users[$key]['last_name'] = ucwords(strtolower($user['last_name']));
            $users[$key]['fullname'] = ucwords(strtolower($users[$key]['fullname']));
            if ($users[$key]['country'] == "UAE") {
                $countryKey = 'AE';
            } else if ($users[$key]['country'] == "KSA") {
                $countryKey = 'SA';
            } else if ($users[$key]['country'] == "Kuwait") {
                $countryKey = 'KW';
            } else if ($users[$key]['country'] == "Bahrain") {
                $countryKey = 'BH';
            } else if ($users[$key]['country'] == "Oman") {
                $countryKey = 'OM';
            } else if ($users[$key]['country'] == "Qatar") {
                $countryKey = 'QA';
            } else if ($users[$key]['country'] == "Jordan") {
                $countryKey = 'JO';
            } else if ($users[$key]['country'] == "Lebanon") {
                $countryKey = 'LB';
            } else if ($users[$key]['country'] == "Egypt") {
                $countryKey = 'EG';
            } else {
                $countryKey = phoneNumber($users[$key]['country']);
            }
            $phoneUtil = PhoneNumberUtil::getInstance();
            if (isset($users[$key]['phone']) && !empty($users[$key]['phone'])) {
                $mobileNumberStr = $users[$key]['phone'];
                $users[$key]['phone'] = $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
            }
            if (isset($users[$key]['mobile']) && !empty($users[$key]['mobile'])) {
                $mobileNumberStr = $users[$key]['mobile'];
                $users[$key]['mobile'] = $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
            }
        }
        $data['json'] = json_encode($users);
        $this->load->view("print_json", $data);
    }

    function people_concept_ajax()
    {
        $user_id = $this->session->userdata('userid');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('start');
        $filters = array();
        $concept = $this->input->get('concept');
        if ($concept != '0')
            $filters['concept'] = $concept;
        $users = $this->User->getConceptUsers($user_id, $limit, $offset, $filters);

        $phoneUtil = PhoneNumberUtil::getInstance();
        foreach ($users as $key => $user) {
            $users[$key]['loged_user_id'] = $user_id;
            $users[$key]['first_name'] = ucwords(strtolower($user['first_name']));
            $users[$key]['middle_name'] = ucwords(strtolower($user['middle_name']));
            $users[$key]['last_name'] = ucwords(strtolower($user['last_name']));
            $users[$key]['fullname'] = ucwords(strtolower($users[$key]['fullname']));
            if (isset($user['phone']) && $user['phone'] != '' && strlen($user['country']) <= 3) {
                $mobileNumberStr = $users[$key]['phone'];
                $countryKey = phoneNumber($users[$key]['country']);
                $users[$key]['phone'] = $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
            }
            if (isset($user['mobile']) && $user['mobile'] != '' && strlen($user['country']) <= 3 && $user['country'] != '') {
                $mobileNumberStr = $users[$key]['mobile'];
                $countryKey = phoneNumber($users[$key]['country']);
                $users[$key]['mobile'] = $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
            }
        }
        $data['json'] = json_encode($users);
        $this->load->view("print_json", $data);
    }

    function autocomplete_ajax()
    {
		$users = array();
        $page_name = $this->input->get('page');
        $user_id = $this->session->userdata('userid');
        $search_term = $this->input->get('term');
		if(isset($search_term) && trim($search_term) != '')
		{
        $users = $this->User->searchUsers(5, 0, $search_term, $page_name);
		}
        foreach ($users as $key => $user) {

            if ($user['label'] == '') {
                unset($users[$key]);
            }
        }
        $data['json'] = json_encode($users);
        $this->load->view("print_json", $data);
    }
	
	function global_autocomplete_ajax()
    {
        $page_name = $this->input->get('page');
        $user_id = $this->session->userdata('userid');
        $search_term = $this->input->get('term');
        $users = $this->User->searchUsers(5, 0, $search_term, $page_name);
        foreach ($users as $key => $user) {

            if ($user['label'] == '') {
                unset($users[$key]);
            }
        }
        $data['json'] = json_encode($users);
        $this->load->view("print_json", $data);
    }

    function autocomplete_ajax_tags()
    {
        $search_term = $this->input->post('query');
        $users = $this->User->searchUsersTags(10, 0, $search_term);
        $data['json'] = json_encode($users);
        $this->load->view("print_json", $data);
    }

    function index()
    {
	$data = array();
	$data['term']='';
    $data['audit_constant'] = $this->config->item('audit');
		if($this->input->get('txtSearch'))
		{
			$term = $this->input->get('txtSearch');
			$data['term'] = $term;
		} 
		$this->load->view('people',$data);
    }

//alphabetical view of company directory
    function alphabetical()
    {
        $user_id = $this->session->userdata('userid');
        $this->load->library('pagination');

        $this->load->model('m_people');
        $char = $this->uri->segment(3);
        $people_count = $this->m_people->count_char($char);

        $this->page_conf['base_url'] = site_url('people/alphabetical/' . $char);
        $this->page_conf['total_rows'] = $people_count;
        $this->page_conf['uri_segment'] = 4;
        $this->pagination->initialize($this->page_conf);


        $offset = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
        $limit = $this->per_page;
        $user_email = $this->session->userdata('useremail');
        $data = array(
            'gn_newcommer_list' => $this->_newcommer_list(),
            'gn_interest_groups_list' => $this->_interest_group_list(),
            'gn_my_reminders' => $this->_my_reminders(),
            'myprofile' => $this->m_profile->get_profile_by_email($user_email),
            'people' => $this->m_people->get_allchar($user_id, $char, $limit, $offset),
            'first' => ($offset + 1),
            'last' => min($offset + $limit, $people_count),
            'total' => $people_count,
            'concepts' => $this->m_people->concepts(8),
            'countries' => $this->m_people->territories(8),
            'active_all_people' => 'class="active"',
            'active_my_contacts' => '',
            'alphabetical_filter_link' => 'people/alphabetical',
            'src_msg' => "filtered on names starting with '{$char}'",
            'src_term' => "",
            'src_concept_id_arr' => array(),
            'src_country_id_arr' => array()
        );

        $alpha_filter['active_class_all'] = '';
        $alpha_filter['active_class_a'] = '';
        $alpha_filter['active_class_b'] = '';
        $alpha_filter['active_class_c'] = '';
        $alpha_filter['active_class_d'] = '';
        $alpha_filter['active_class_e'] = '';
        $alpha_filter['active_class_f'] = '';
        $alpha_filter['active_class_g'] = '';
        $alpha_filter['active_class_h'] = '';
        $alpha_filter['active_class_i'] = '';
        $alpha_filter['active_class_j'] = '';
        $alpha_filter['active_class_k'] = '';
        $alpha_filter['active_class_l'] = '';
        $alpha_filter['active_class_m'] = '';
        $alpha_filter['active_class_n'] = '';
        $alpha_filter['active_class_o'] = '';
        $alpha_filter['active_class_p'] = '';
        $alpha_filter['active_class_q'] = '';
        $alpha_filter['active_class_r'] = '';
        $alpha_filter['active_class_s'] = '';
        $alpha_filter['active_class_t'] = '';
        $alpha_filter['active_class_u'] = '';
        $alpha_filter['active_class_v'] = '';
        $alpha_filter['active_class_w'] = '';
        $alpha_filter['active_class_x'] = '';
        $alpha_filter['active_class_y'] = '';
        $alpha_filter['active_class_z'] = '';

        foreach ($alpha_filter as $active_class => $filter) {
            if (!$char) {
                $char = "all";
            }
            if ("active_class_" . $char == $active_class) {
                $data[$active_class] = 'class="active"';
            } else {
                $data[$active_class] = '';
            }
        }


        $this->load->view('v_people', $data);
    }

    function follow_user()
    {
        $user_id = $this->session->userdata('userid');
        $follow_user = $this->input->post('user');
        $affected_row = $this->UserFollow->followUser($user_id, $follow_user);
        $user_name = $this->session->userdata('logged_user');
        //print_r($user_name);exit;
        //$notification_html = '<a href="'.site_url().'groups/'.$this->input->post('group_id').'">'.ucfirst($this->input->post('group_name')).'</a>';
        $notification_html = $user_name['fullname'] . ' is following you';
        $notification = array(
            'object_id' => $follow_user,
            'object_type' => 'User',
            'parent_id' => $user_id,
            'parent_type' => 'User',
            'activity_type' => 'Follow',
            'dttm' => date('Y-m-d H:i:s', time()),
            'status' => 'unviewed',
            'notification' => $notification_html,
        );
        $this->Activities->groupUpdateNotification($notification);


        $data['json'] = json_encode(array('affected_row' => $affected_row, 'status' => 'success', 'action' => 'Follow'));
        $this->load->view("print_json", $data);
    }

    //Unfollow user json output
    function unfollow_user()
    {
        $user_id = $this->session->userdata('userid');
        $unfollow_user = $this->input->post('user');
        $affected_row = $this->UserFollow->unfollowUser($user_id, $unfollow_user);
        $data['json'] = json_encode(array('affected_row' => $affected_row, 'status' => 'success', 'action' => 'Following'));
        $this->load->view("print_json", $data);
    }

//Searchin company directory
    function search($searchstr = '', $offset = 0)
    {
        $user_id = $this->session->userdata('userid');
        $user_email = $this->session->userdata('useremail');

        $limit = $this->per_page;

        if ($this->input->post('search') == 'search') {
            $country = $this->input->post('country');
            $concept = $this->input->post('concept');
            $term = $this->input->post('term');

            if ($term == "Search by name..") {
                $term = "";
            }
            if ($term) {
                $src_msg = "Search results for '{$term}'";
            } else {
                redirect('people');
            }

            if (is_array($concept)) {
                $concept_id_arr = @implode(", ", $concept);
                $concept_name_arr = $this->m_people->get_concepts_names($concept_id_arr);
                foreach ($concept_name_arr as $name) {
                    $concept_name[] = ucwords(strtolower($name['name']));
                }
                $src_msg .= " filtered on Concept: " . @implode(", ", $concept_name);
                $src_concept_id_arr = $concept;
            } else {
                $src_concept_id_arr = array();
                $concept = array();
            }

            if (is_array($country)) {
                $territory_id_arr = @implode(", ", $country);
                $territory_name_arr = $this->m_people->get_territory_names($territory_id_arr);
                foreach ($territory_name_arr as $name) {
                    $territory_name[] = ucwords(strtolower($name['name']));
                }
                $src_msg .= " filtered on Country: " . @implode(", ", $territory_name);
                $src_country_id_arr = $country;
            } else {
                $country = array();
                $src_country_id_arr = array();
            }

            $searchobj = array(
                'concept' => $concept,
                'country' => $country,
                'term' => $term
            );

            $searchstr = base64_encode(serialize($searchobj));
        } elseif ($searchstr) {

            $searchobj = unserialize(base64_decode($searchstr));

            if (!$searchobj) {
                redirect('people/search');
            }

            $country = $searchobj['country'];
            $concept = $searchobj['concept'];
            $term = $searchobj['term'];

            if ($term == "Search by name..") {
                $term = "";
            }
            if ($term) {
                $src_msg = "Search results for '{$term}'";
            } else {
                redirect('people');
            }

            if (is_array($concept) && count($concept)) {
                $concept_id_arr = @implode(", ", $concept);
                $concept_name_arr = $this->m_people->get_concepts_names($concept_id_arr);
                foreach ($concept_name_arr as $name) {
                    $concept_name[] = ucwords(strtolower($name['name']));
                }
                $src_msg .= " filtered on Concept: " . @implode(", ", $concept_name);
                $src_concept_id_arr = $concept;
            } else {
                $src_concept_id_arr = array();
                $concept = array();
            }

            if (is_array($country) && count($country)) {
                $territory_id_arr = @implode(", ", $country);
                $territory_name_arr = $this->m_people->get_territory_names($territory_id_arr);
                foreach ($territory_name_arr as $name) {
                    $territory_name[] = ucwords(strtolower($name['name']));
                }
                $src_msg .= " filtered on Country: " . @implode(", ", $territory_name);
                $src_country_id_arr = $country;
            } else {
                $country = array();
                $src_country_id_arr = array();
            }
        } else {
            redirect('people/');
        }
//sphinx search
        $sp = new SphinxClient();
        $sp->SetServer('localhost', 3312);
        $sp->SetMatchMode(SPH_MATCH_EXTENDED2);
        $sp->SetArrayResult(true);
        $sp->SetSortMode(SPH_SORT_EXTENDED, "@relevance DESC");
        $sp->SetLimits((int)$offset, $limit, 1000);

        if ($country)
            $sp->SetFilter('territory_id', $country, false);

        if ($concept)
            $sp->SetFilter('concept_id', $concept, false);

        $result = $sp->Query($term . "*", 'test5');
        if (!$result) {
            echo $sp->GetLastError();
            exit;
        }
//    echo '<pre>'; print_r($offset); print_r($limit); print_r($result); echo '</pre>';

        $this->load->model('m_people');
        $this->load->library('pagination');
        if (!array_key_exists('matches', $result)) {
//$user_list = '';
            $data = array(
                'searchresults' => 0,
                'people' => array(),
                'first' => 0,
                'last' => 0,
                'total' => 0,
                'concepts' => $this->general_model->gn_get_concepts(),
                'countries' => $this->general_model->gn_get_territory(),
                'departments' => $this->general_model->gn_get_department(),
                'active_all_people' => 'class="active"',
                'active_my_contacts' => '',
                'alphabetical_filter_link' => 'people/my_contacts_sort',
                'src_msg' => $src_msg,
                'src_term' => $term,
                'src_concept_id_arr' => $src_concept_id_arr,
                'src_country_id_arr' => $src_country_id_arr,
                'myprofile' => $this->m_profile->get_profile_by_email($user_email),
                'gn_newcommer_list' => $this->_newcommer_list(),
                'gn_interest_groups_list' => $this->_interest_group_list(),
                'gn_my_reminders' => $this->_my_reminders(),
                'pg_nxt' => 0,
                'pg_prev' => $limit
            );
        } else {

            $people = array();
            $this->load->model("m_profile");
            foreach ($result['matches'] as $row) {
                //$people[] = $this->m_people->get_profile($row['id']);
                $people[] = $this->m_profile->get_profile($row['id'], $user_id);
            }

            $people_count = $result['total_found'];

            $page_conf = array(
                'base_url' => site_url("people/search/$searchstr"),
                'total_rows' => $people_count,
                'per_page' => $limit,
                'uri_segment' => 4,
                'full_tag_open' => '<ul>',
                'full_tag_close' => '</ul>',
                'num_tag_open' => '<li>',
                'num_tag_close' => '</li>',
                'cur_tag_open' => '<li><strong>',
                'cur_tag_close' => '</strong></li>',
                'next_link' => 'next',
                'next_tag_open' => '<li class="next">',
                'next_tag_close' => '</li>',
                'prev_link' => 'prev',
                'prev_tag_open' => '<li class="prev">',
                'next_tag_close' => '</li>',
                'first_link' => 'First',
                'first_tag_open' => '<li>',
                'first_tag_close' => '</li>',
                'last_link' => 'Last',
                'last_tag_open' => '<li>',
                'last_tag_close' => '</li>'
            );
            $this->pagination->initialize($page_conf);
            $this->load->model('m_people');
            $data = array(
                'searchresults' => 1,
                'people' => $people,
                'first' => ($offset + 1),
                'last' => min($offset + $limit, $people_count),
                'total' => $people_count,
                'concepts' => $this->general_model->gn_get_concepts(),
                'countries' => $this->general_model->gn_get_territory(),
                'departments' => $this->general_model->gn_get_department(),
                'active_all_people' => 'class="active"',
                'active_my_contacts' => '',
                'alphabetical_filter_link' => 'people/my_contacts_sort',
                'src_msg' => $src_msg,
                'src_term' => $term,
                'src_concept_id_arr' => $src_concept_id_arr,
                'src_country_id_arr' => $src_country_id_arr,
                'myprofile' => $this->m_profile->get_profile_by_email($user_email),
                'gn_newcommer_list' => $this->_newcommer_list(),
                'gn_interest_groups_list' => $this->_interest_group_list(),
                'gn_my_reminders' => $this->_my_reminders(),
                'pg_nxt' => 0,
                'pg_prev' => $limit
            );
        }
        //Alphabet filters
        $data['active_class_all'] = 'class="active"';
        $data['active_class_a'] = '';
        $data['active_class_b'] = '';
        $data['active_class_c'] = '';
        $data['active_class_d'] = '';
        $data['active_class_e'] = '';
        $data['active_class_f'] = '';
        $data['active_class_g'] = '';
        $data['active_class_h'] = '';
        $data['active_class_i'] = '';
        $data['active_class_j'] = '';
        $data['active_class_k'] = '';
        $data['active_class_l'] = '';
        $data['active_class_m'] = '';
        $data['active_class_n'] = '';
        $data['active_class_o'] = '';
        $data['active_class_p'] = '';
        $data['active_class_q'] = '';
        $data['active_class_r'] = '';
        $data['active_class_s'] = '';
        $data['active_class_t'] = '';
        $data['active_class_u'] = '';
        $data['active_class_v'] = '';
        $data['active_class_w'] = '';
        $data['active_class_x'] = '';
        $data['active_class_y'] = '';
        $data['active_class_z'] = '';

        $this->load->view('v_people', $data);
    }

    //My Contacts
    function my_contacts()
    {
        $user_id = $this->session->userdata('userid');
        $this->load->library('pagination');

        $this->load->model('m_people');
        $people_count = $this->m_people->count_my_contact($user_id);

        $this->page_conf['base_url'] = site_url('people/my_contact');
        $this->page_conf['total_rows'] = $people_count;
        $this->pagination->initialize($this->page_conf);

        $offset = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $limit = $this->per_page;
        $user_email = $this->session->userdata('useremail');
        $data = array(
            'gn_newcommer_list' => $this->_newcommer_list(),
            'gn_interest_groups_list' => $this->_interest_group_list(),
            'gn_my_reminders' => $this->_my_reminders(),
            'myprofile' => $this->m_profile->get_profile_by_email($user_email),
            'people' => $this->m_people->get_all_my_contact($user_id, $limit, $offset),
            'first' => ($offset + 1),
            'last' => min($offset + $limit, $people_count),
            'total' => $people_count,
            'concepts' => $this->m_people->concepts(8),
            'countries' => $this->m_people->territories(8),
            'active_all_people' => '',
            'active_my_contacts' => 'class="active"',
            'alphabetical_filter_link' => 'people/my_contacts_sort',
            'src_msg' => "All People in My Contact'",
            'src_term' => "",
            'src_concept_id_arr' => array(),
            'src_country_id_arr' => array()
        );
        //Alphabet filters
        $data['active_class_all'] = 'class="active"';
        $data['active_class_a'] = '';
        $data['active_class_b'] = '';
        $data['active_class_c'] = '';
        $data['active_class_d'] = '';
        $data['active_class_e'] = '';
        $data['active_class_f'] = '';
        $data['active_class_g'] = '';
        $data['active_class_h'] = '';
        $data['active_class_i'] = '';
        $data['active_class_j'] = '';
        $data['active_class_k'] = '';
        $data['active_class_l'] = '';
        $data['active_class_m'] = '';
        $data['active_class_n'] = '';
        $data['active_class_o'] = '';
        $data['active_class_p'] = '';
        $data['active_class_q'] = '';
        $data['active_class_r'] = '';
        $data['active_class_s'] = '';
        $data['active_class_t'] = '';
        $data['active_class_u'] = '';
        $data['active_class_v'] = '';
        $data['active_class_w'] = '';
        $data['active_class_x'] = '';
        $data['active_class_y'] = '';
        $data['active_class_z'] = '';
        $this->load->view('v_people', $data);
    }

//My contacts sorting for company directory
    function my_contacts_sort()
    {
        $user_id = $this->session->userdata('userid');
        $this->load->library('pagination');

        $this->load->model('m_people');
        $char = $this->uri->segment(3);
        $people_count = $this->m_people->count_char_my_contacts($char, $user_id);

        $this->page_conf['base_url'] = site_url('people/my_contacts_sort/' . $char);
        $this->page_conf['total_rows'] = $people_count;
        $this->pagination->initialize($this->page_conf);


        $offset = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
        $limit = $this->per_page;
        $user_email = $this->session->userdata('useremail');
        $data = array(
            'gn_newcommer_list' => $this->_newcommer_list(),
            'gn_interest_groups_list' => $this->_interest_group_list(),
            'gn_my_reminders' => $this->_my_reminders(),
            'myprofile' => $this->m_profile->get_profile_by_email($user_email),
            'people' => $this->m_people->get_allchar_my_contacts($user_id, $char, $limit, $offset),
            'first' => ($offset + 1),
            'last' => min($offset + $limit, $people_count),
            'total' => $people_count,
            'concepts' => $this->m_people->concepts(8),
            'countries' => $this->m_people->territories(8),
            'active_all_people' => '',
            'active_my_contacts' => 'class="active"',
            'alphabetical_filter_link' => 'people/my_contacts_sort',
            'src_msg' => "filtered on names in My Contacts, starting with '{$char}'",
            'src_term' => "",
            'src_concept_id_arr' => array(),
            'src_country_id_arr' => array()
        );

        $alpha_filter['active_class_all'] = '';
        $alpha_filter['active_class_a'] = '';
        $alpha_filter['active_class_b'] = '';
        $alpha_filter['active_class_c'] = '';
        $alpha_filter['active_class_d'] = '';
        $alpha_filter['active_class_e'] = '';
        $alpha_filter['active_class_f'] = '';
        $alpha_filter['active_class_g'] = '';
        $alpha_filter['active_class_h'] = '';
        $alpha_filter['active_class_i'] = '';
        $alpha_filter['active_class_j'] = '';
        $alpha_filter['active_class_k'] = '';
        $alpha_filter['active_class_l'] = '';
        $alpha_filter['active_class_m'] = '';
        $alpha_filter['active_class_n'] = '';
        $alpha_filter['active_class_o'] = '';
        $alpha_filter['active_class_p'] = '';
        $alpha_filter['active_class_q'] = '';
        $alpha_filter['active_class_r'] = '';
        $alpha_filter['active_class_s'] = '';
        $alpha_filter['active_class_t'] = '';
        $alpha_filter['active_class_u'] = '';
        $alpha_filter['active_class_v'] = '';
        $alpha_filter['active_class_w'] = '';
        $alpha_filter['active_class_x'] = '';
        $alpha_filter['active_class_y'] = '';
        $alpha_filter['active_class_z'] = '';

        foreach ($alpha_filter as $active_class => $filter) {
            if (!$char) {
                $char = "all";
            }
            if ("active_class_" . $char == $active_class) {
                $data[$active_class] = 'class="active"';
            } else {
                $data[$active_class] = '';
            }
        }
        $this->load->view('v_people', $data);
    }

//custom FB like search for General pages -- Old function
    function quick_search()
    {
        $src_term = $this->input->post('src_term');
        $user_id = $user_id = $this->session->userdata('userid');
        $offset = 0;
        $limit = 5;
        $user_list = '';
        $show = 0;
        $show_count = '';

        $result = $this->m_people->search($user_id, $src_term, '', '', $limit, $offset);
        $show_count = $this->m_people->search_count($src_term, '', '');
        //print_r($result); exit;
        if (count($result)) {
            foreach ($result as $info) {
                $user_list .= "<li>
                      <div>
                        <img src='" . base_url() . "images/user-images/105x101/" . $info['profile_pic'] . "' height='47' />
                        <p class='name'><a href='" . site_url('profile/view_profile/' . $info['id']) . "'>" . ucfirst(strtolower($info['first_name'])) . ' ' . ucfirst(strtolower($info['last_name'])) . "</a></p>
                        <p class='info'>" . $info['designation'] . ", " . $info['concept'] . "</p>
                        <p class='contact'>" . $info['extension'] . " <span>&middot;</span>" . $info['phone'] . " (B)<span>&middot;</span> " . $info['mobile'] . "</p>
                      </div>
                    </li>";
            }
            $show = 1;
            $show_count = 'See all ' . $show_count . ' matching people';
        } else {
            $user_list = "<li>
                      <div>
                        <p class='no-users'>No users found</p>
                      </div>
                    </li>";
        }


        $data = array(
            'show' => count($result),
            'result_count' => $show_count,
            'result' => $user_list
        );
        echo json_encode($data);
    }

//custom FB like sphinx search for General pages
    function quick_search_sphinx()
    {
        $src_term = $this->input->post('src_term');
        $src_from = $this->input->post('src_from');

        //adding search in edit employee detail in management, need different url


        $offset = 0;
        $limit = 5;
        $user_list = '';
        $show = 0;
        $show_count = '';

        $this->load->helper('sphinx_helper');

        $sp = new SphinxClient();
        $sp->SetServer('localhost', 9312);
        $sp->SetMatchMode(SPH_MATCH_EXTENDED2);
        $sp->SetArrayResult(true);
        $sp->SetSortMode(SPH_SORT_EXTENDED, "@relevance DESC");
        $sp->SetFieldWeights(array('first_name' => 200, 'last_name' => 10));
        $sp->SetLimits((int)$offset, $limit, 1000);
        $result = $sp->Query($src_term . "*", 'quick_search_index'); //Results
        if (isset($result['matches'])) {
            foreach ($result['matches'] as $user_info) {
                $info = $this->m_people->get_src_user_info($user_info['id']);


                if (trim($info['mobile'])) {
                    if (strpos($info['mobile'], '00') === 0) {
                        $info['mobile'] = '+' . substr($info['mobile'], 2, strlen($info['mobile']));
                    }
                    $info['mobile'] = " M: " . $info['mobile'];
                }

                if (trim($info['phone'])) {
                    if (strpos($info['phone'], '00') === 0) {
                        $info['phone'] = '+' . substr($info['phone'], 2, strlen($info['phone']));
                    }
                    $info['phone'] = " B: " . $info['phone'];
                }


                if (trim($info['mobile'])) {
                    if (strlen($info['extension']) > 4) {
                        $info['extension'] = '';
                    }
                    $info['extension'] = (trim($info['extension'])) ? 'E: ' . $info['extension'] : '';
                }
                if (strlen($info['designation']) > 25) {
                    $info['designation'] = substr($info['designation'], 0, 25) . '..';
                }

                $info['concept'] = ucfirst(strtolower($info['concept']));

                //adding search in edit employee detail in management, need different url
                //print_r($src_from);
                $target_url = $src_from == '' ? 'profile/view_profile/' : 'mprofile/edit_profile/';
                //echo $target_url;
                //die();
                $user_list .= "<li id='" . site_url($target_url . $info['id']) . "'>
                      <div>
                        <img src='" . base_url() . "images/user-images/105x101/" . $info['profile_pic'] . "' height='47' />
                        <p class='name'><a href='" . site_url($target_url . $info['id']) . "'>" . ucfirst(strtolower($info['first_name'])) . ' ' . ucfirst(strtolower($info['last_name'])) . "</a></p>
                        <p class='info'>" . $info['designation'] . ", " . $info['concept'] . "</p>
                        <p class='contact'>" . $info['extension'] . $info['phone'] . $info['mobile'] . "</p>
                      </div>
                    </li>";
            }
            $show = 1;
            $show_count = 'See all ' . $result['total_found'] . ' matching people';
        } else {
            $user_list = "<li>
                      <div>
                        <p class='no-users'>No users found</p>
                      </div>
                    </li>";
        }

        $data = array(
            'show' => count($result),
            'result_count' => $show_count,
            'result' => $user_list
        );
        echo json_encode($data);
    }

//Main phone book search page
    function phone_book_src()
    {
        $dept = $this->input->post('dept');
        $concept = $this->input->post('concept');
        $territory = $this->input->post('territory');
        $show_only_contacts = $this->input->post('show_only_contacts');
        $user_id = $this->session->userdata('userid');
        $term = $this->input->post('src_term');
        if (!trim($term)) {
            $term = '';
        }
        //$offset = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $offset = $this->input->post('offset');
        $limit = $this->per_page;

        $pg_nxt = $offset + $limit;
        $pg_prev = $offset - $limit;


        if ($show_only_contacts) {
            $filter = '';
            if ($term) {
                $filter[] = 'first_name LIKE "%' . $term . '%"';
            }
            if (is_array($dept)) {
                $filter[] = "department_id IN (" . @implode(",", $dept) . ")";
            }
            if (is_array($concept)) {
                $filter[] = "concept_id IN (" . @implode(",", $concept) . ")";
            }
            if (is_array($territory)) {
                $filter[] = "territory_id IN (" . @implode(",", $territory) . ")";
            }

            if (is_array($filter)) {
                $src_attr = ' AND ' . @implode(' AND ', $filter);
            } else {
                $src_attr = '';
            }


            $result['total_found'] = $this->m_people->search_count_my_contact($user_id, $src_attr);
            if ($result['total_found']) {
                $result['matches'] = $this->m_people->search_all_my_contact($user_id, $limit, $offset, $src_attr);
            }
        } elseif (!$term) {
            if (is_array($dept)) {
                $filter[] = "department_id IN (" . @implode(",", $dept) . ")";
            }
            if (is_array($concept)) {
                $filter[] = "concept_id IN (" . @implode(",", $concept) . ")";
            }
            if (is_array($territory)) {
                $filter[] = "territory_id IN (" . @implode(",", $territory) . ")";
            }

            if (is_array($filter)) {
                $src_attr = ' AND ' . @implode(' AND ', $filter);
            } else {
                $src_attr = '';
            }
            $result['total_found'] = $this->m_people->get_all_count($src_attr);
            if ($result['total_found']) {
                $result['matches'] = $this->m_people->get_all($user_id, $limit, $offset, $src_attr);
            }
        } else {
            $sp = new SphinxClient();
            $sp->SetServer('localhost', 3312);
            $sp->SetMatchMode(SPH_MATCH_EXTENDED2);
            $sp->SetArrayResult(true);
            //$sp->SetSortMode(SPH_SORT_ATTR_ASC, "first_name");
            //$sp->SetSortMode(SPH_SORT_EXTENDED, "@weight DESC");
            //$sp->SetSortMode ( SPH_SORT_ATTR_DESC, "first_name" );
            $sp->SetLimits((int)$offset, $limit, 1000);
            $sp->SetFieldWeights(array('first_name' => 200, 'last_name' => 150));
            if (is_array($dept)) {
                $sp->SetFilter('department_id', $dept, false);
            }
            if (is_array($concept)) {
                $sp->SetFilter('concept_id', $concept, false);
            }
            if (is_array($territory)) {
                $sp->SetFilter('territory_id', $territory, false);
            }

            //$sp->AddQuery("$term", "test5");
            //$result = $sp->RunQueries();
            //Test 5 is the sphinx index function
            $result = $sp->Query($term . '*', 'test5');
            //print_r($result);
        }


        //print_r($result[0]); exit;
        if (!array_key_exists('matches', $result)) {
            $user_list = '<div class="list-content alter1">No user found</div>';
            $total_count = 0;
        } else {
            $people = array();
            $this->load->model("m_profile");
            foreach ($result['matches'] as $row) {
                $people[] = $this->m_profile->get_profile($row['id'], $user_id);
            }
            $total_count = $result['total_found'];
            $data['people'] = $people;
            $user_list = $this->load->view('v_src_list', $data, true);
        }


        $json_arr = array();
        $json_arr['user_list'] = $user_list;
        $json_arr['total_count'] = $total_count;
        $json_arr['pg_nxt'] = $pg_nxt;
        $json_arr['pg_prev'] = $pg_prev;
        $json_arr['show_start_cnt'] = ($offset + 1);
        $json_arr['show_end_cnt'] = min($offset + $limit, $total_count);

        //$json_arr['data'] = $output_data;
        echo json_encode($json_arr);
    }
}