<?php

class Auth extends MX_Controller {
	
	function __constrcut() {
		parent::__construct();
	}

	function index() {
		
		$this->load->view('login_form');
	}
		
	function validate_user() {
		
		//date_default_timezone_set('Asia/Calcutta');
		date_default_timezone_set("Asia/Baghdad"); //default timezone setting
		$this->load->model('user');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if ($this->user->authenticate($username, $password)) {


			$userdata = array(
				'username' => $username,
				'fullname' => $this->user->fullname($username),
				'is_logged_in' => true,
				'userid' => $this->user->id($username),
				'useremail' => $this->user->email($username),
				'userlocation' => $this->user->department($username),
				'userlastlogin' => strftime('%Y/%m/%d %H:%M:%S')
			);
			$this->session->set_userdata($userdata);
			
			if ($comingfrom = get_cookie('comingfrom')) {

				// delete_cookie('comingfrom');
				setcookie('comingfrom', '', time() - 3600, '/ci');
				header("Location: $comingfrom");
			}
			elseif ($this->session->userdata('requested_uri')) {

				redirect($this->session->userdata('requested_uri'));
			}
			else {

				redirect('ecards');
			}
		}
		else {

			$data['login_failed'] = true;
			$this->load->view('login_form', $data);
		}
	}
	
	function logout() {
		
		$userid = $this->session->userdata('userid');

		$userdata = array(
			'lastlogin' => $this->session->userdata('userlastlogin')
		);
		
		$this->db->update('user', $userdata, "id = $userid");
		
		$this->session->sess_destroy();
		redirect('auth');
	}
}
