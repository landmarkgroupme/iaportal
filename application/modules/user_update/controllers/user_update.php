<?php

class User_update extends MX_Controller {

  function User_update() {
    parent::__construct();
    $this->load->model('user_update_model');
    $this->load->model('backend/moderate_model');
    $this->load->model('new_user_update/new_user_update_model');
    $this->config->load('intranet');
  }

  function import() {
    $this->load->model('User');
    $this->load->model('Designation');
    $exist_designation = $this->Designation->getNamesArray();

    $this->load->model('Department');
    $exist_departs = $this->Department->getNamesArray();

    $this->load->model('Concept');
    $exist_concepts = $this->Concept->getNamesArray();


    
    /*print_r($exist_designation);
    die();*/
    //$designations = $this->Designation->getAll(null, null, null, null, 'array');

    $this->db->select('user.id, user.hrms_id, user.first_name, user.last_name, 
                      hrms.full_name as hrms_name, 
                      des.name as user_designation, hrms.position_name, 
                      dept.name as depart_name, 
                      hrms.department as hrms_depart,
                      con.name as user_concept, 
                      hrms.concept as hrms_concept,
                      user.mobile as user_mobile,
                      hrms.mobile as hrms_mobile,
                      user.phone as user_phone,
                      hrms.phone as hrms_phone');
    $this->db->from('ci_users user');
    $this->db->join('ci_master_designation des', 'des.id = user.designation_id');
    $this->db->join('ci_master_department dept', 'dept.id = user.department_id');
    $this->db->join('ci_master_concept con', 'con.id = user.concept_id');
    //$this->db->join('ci_master_location loc', 'loc.id = user.location_id');
    $this->db->join('hrms_users hrms', 'hrms.EMPLOYEE_NUMBER = user.hrms_id');
    $this->db->where('des.name <> hrms.position_name');
    $this->db->or_where('dept.name <> hrms.department');
    $this->db->or_where('con.name <> hrms.concept');
    $this->db->or_where('user.mobile <> hrms.mobile');
    $this->db->or_where('user.phone <> hrms.phone');


    $query = $this->db->get();
    $data = $query->result_array();

    echo '<pre>';
    foreach($data as $user_data) 
    {
      $update_data = array();
      if(strcmp($user_data['user_designation'], $user_data['position_name']) != 0)
      {
        $update_data['designation_id'] = array_search($user_data['position_name'], $exist_designation);
      }
      if(strcmp($user_data['depart_name'], $user_data['hrms_depart']) != 0)
      {
        $update_data['department_id'] = array_search($user_data['hrms_depart'], $exist_departs); 
      }

      if(strcmp($user_data['user_concept'], $user_data['hrms_concept']) != 0)
      {
        $update_data['concept_id'] = array_search($user_data['hrms_concept'], $exist_concepts); 
        if($update_data['concept_id'] == '')
        {
          unset($update_data['concept_id']);
        }
      }

      if(strcmp($user_data['user_mobile'], $user_data['hrms_mobile']) != 0)
      {
        $update_data['mobile'] = $user_data['hrms_mobile'];
      }

      if(strcmp($user_data['user_phone'],$user_data['hrms_phone']) != 0)
      {
        $update_data['phone'] = $user_data['hrms_phone'];
      }

      if(!empty($update_data))
      {
        $this->db->where('id',$user_data['id']);
        $this->db->update('ci_users', $update_data);
      }
    }
    print_r($data);
  }

//Accepts filepath and data
  function _create_log_file($file_path, $data) {
    $this->load->helper('file');
    if (write_file($file_path, $data, 'w')) {
      echo 'Log file created at ', $file_path, '<br />';
    } else {
      echo 'Log file creation failed at ', $file_path, '<br />';
    }
  }
//deactivate ex-emp
function remove_ex_employess($file_path) {
  $file = fopen($file_path, "r") or exit("Unable to open file!");
  $emp_id = array();
  $result = array();
  if(($handle = fopen($file_path, "r")) !== false){
    while (($data = fgetcsv($handle)) !== false) {
      $emp_id[] = $data[0];
      unset($data);
    }
  }
  if (!empty($emp_id)) {
    $result = $this->user_update_model->deactivate_ex_emp($emp_id);
  }
  return $result;
} 
//User update list pre check
 function _user_update_data_check($file_path) {

    $file = fopen($file_path, "r") or exit("Unable to open file!");
    $row_cnt = 1;
    $row_validation = array();
    $unique_emails = array();
    $valid_emails = 0;
    $processed_user_data = array();
  if(($handle = fopen($file_path, "r")) !== false){
    while (($data = fgetcsv($handle)) !== false) {
      $row = fgets($file);
      $user_data = array();
      $user_data = $data;//explode(",", $row);
      //print_r($user_data); exit;
      if (!$user_data[0]) {
        continue;
      }
      $error_cnt = 0;
      //Row Header validation
      if ($row_cnt == 1) {
        $error_array = array();
        if (trim($user_data[0]) != 'BUSINESS_GROUP_ID') {
          $error_array[] = 'Business group ID missing(BUSINESS_GROUP_ID)';
        }
        if (trim($user_data[1]) != 'TERRITOTY_NAME') {
          $error_array[] = 'Territory Name missing(TERRITOTY_NAME)';
        }
        if (trim($user_data[2]) != 'PERSON_TYPE_ID') {
          $error_array[] = 'Person ID missing(PERSON_TYPE_ID)';
        }
        if (trim($user_data[3]) != 'EMPLOYEE_NUMBER') {
          $error_array[] = 'Employee number missing(EMPLOYEE_NUMBER)';
        }
        if (trim($user_data[4]) != 'TITLE') {
          $error_array[] = 'Title missing(TITLE)';
        }
        if (trim($user_data[5]) != 'FIRST_NAME') {
          $error_array[] = 'First name missing(FIRST_NAME)';
        }
        if (trim($user_data[6]) != 'MIDDLE_NAMES') {
          $error_array[] = 'Middle name missing(MIDDLE_NAMES)';
        }
        if (trim($user_data[7]) != 'LAST_NAME') {
          $error_array[] = 'Last name missing(LAST_NAME)';
        }
        if (trim($user_data[8]) != 'ORIGINAL_DATE_OF_HIRE') {
          $error_array[] = 'Hire date missing(ORIGINAL_DATE_OF_HIRE)';
        }
        if (trim($user_data[9]) != 'GDOJ') {
          $error_array[] = 'GDOJ missing(GDOJ)';
        }
        if (trim($user_data[10]) != 'DATE_OF_BIRTH') {
          $error_array[] = 'Date of birth missing(DATE_OF_BIRTH)';
        }
        if (trim($user_data[11]) != 'FULL_NAME') {
          $error_array[] = 'Full name missing(FULL_NAME)';
        }
        if (trim($user_data[12]) != 'POSITION_NAME') {
          $error_array[] = 'Position missing(POSITION_NAME)';
        }
        if (trim($user_data[13]) != 'DEPARTMENT') {
          $error_array[] = 'Department missing(DEPARTMENT)';
        }
        if (trim($user_data[14]) != 'CONCEPT') {
          $error_array[] = 'Concept missing(CONCEPT)';
        }
        if (trim($user_data[15]) != 'ORGANISATION_NAME') {
          $error_array[] = 'Organisation name missing(ORGANISATION_NAME)';
        }
        if (trim($user_data[16]) != 'ORGANIZATION_ID') {
          $error_array[] = 'Organisation id missing(ORGANIZATION_ID)';
        }
        if (trim($user_data[17]) != 'LOCATION') {
          $error_array[] = 'Location missing(LOCATION)';
        }
        if (trim($user_data[18]) != 'EMAIL_ADDRESS') {
          $error_array[] = 'Email missing(EMAIL_ADDRESS)';
        }
        if (trim($user_data[19]) != 'SUPERVISOR') {
          $error_array[] = 'Supervisor missing(SUPERVISOR)';
        }
        if (trim($user_data[20]) != 'SUPERVISOR_ID') {
          $error_array[] = 'Supervisor id missing(SUPERVISOR_ID)';
        }
        if (trim($user_data[21]) != 'BAND') {
          $error_array[] = 'Band missing(BAND)';
        }
        if (trim($user_data[22]) != 'Mobile Phone') {
          $error_array[] = 'Mobile missing(Mobile Phone)';
        }
        if (trim($user_data[23]) != 'Board Number') {
          $error_array[] = 'Board number missing(Board Number)';
        }
        if (trim($user_data[24]) != 'EXTN') {
          $error_array[] = 'Extension missing(EXTN)';
        }
        if (trim($user_data[25]) != 'PREVIOUS_LAST_NAME') {
          $error_array[] = 'Previous last name is missing(PREVIOUS_LAST_NAME)';
        }
        if (trim($user_data[26]) != 'ACTUAL_WRK_PLACE') {
          $error_array[] = 'Actual work place is missing(ACTUAL_WRK_PLACE)';
        }

        if (count($error_array)) {
          //echo '<xmp>' . print_r($error_array, 1) . '</xmp>';
          //exit;
          $check_result['proceed'] = 0;
          $check_result['error'] = $error_array;
          $check_result['data'] = array();
          return $check_result;
        }
        $row_cnt++;
        continue;
      }
      if (count($user_data) != 27) {
        $row_validation[] = 'Row count missmatch, didnt find 27 columns, Total columns: ' . count($user_data) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }
      if (!ctype_digit(trim($user_data[0]))) {
        $row_validation[] = 'Business group ID should be a integer value:' . trim($user_data[0]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }

      if (!ctype_digit(trim($user_data[2]))) {
        $row_validation[] = 'Person ID should be a integer value: ' . trim($user_data[2]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }
      if (!ctype_digit(trim($user_data[3]))) {
        $row_validation[] = 'Employee number should be a integer value: ' . trim($user_data[3]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }

      if (!ctype_digit(trim($user_data[16]))) {
        $row_validation[] = 'Organisation id should be a integer value: ' . trim($user_data[16]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }
       /*
        *  Allowing User to create account with HRMS ID- IN-1095
        *  Email validation is added in _process_user_list()
        ***/
       
      //if (!filter_var(trim($user_data[18]), FILTER_VALIDATE_EMAIL)) {
      //  $row_validation[] = 'Invalid email address: ' . trim($user_data[18]) . ', Error at row no.: ' . $row_cnt;
      //  $error_cnt++;
      //}
      //if (filter_var($user_data[18], FILTER_VALIDATE_EMAIL)) {
          //$valid_emails++;
          //$email = strtolower($user_data[18]);
          //$emails = explode("@", $email); // extract the email username - the part before @
          //$email1 = $emails[0];
          $hrmsid = $user_data[3];
          
          //$emailpoints = array();
          //$fname = strtolower(trim($user_data[5]));
          //$mname = strtolower(trim($user_data[6]));
          //$lname = strtolower(trim($user_data[7]));
          //$emailpoints[$hrmsid][$email] = 0;
          //$points = 0;
          //
          //$email_name = str_replace('.','',$email1);
          //$email_name = str_replace('_','',$email_name);
          //$email_name = str_replace('-','',$email_name);
          
          //if(!stripos($email, "@cplmg.com") && !stripos($email, "@cpksa.com") && !stripos($email, "@fitnessfirst-me.com")
          //    && !stripos($email, "@citymaxhotels.com") && !stripos($email, "@arg.com.bh") && !stripos($email, "@landmarkgroup.com")) {
          //    $user['name'] = $fname .'&nbsp; '. $mname .'&nbsp; '. $lname;
          //    $user['points'] = $points;
          //    $user['email'] = $email;
          //    $user['hrmsid'] = $hrmsid;
          //     
          //    $black_list_array[] = $user;
          //    continue;
          //}
          //
          //
          //$f_name_arr = explode(' ',$fname);
          //$m_name_arr = explode(' ',$mname);
          //$l_name_arr = explode(' ',$lname);
          //
          //$f_count = 0;
          //$m_count = 0;
          //$l_count = 0;
          //$percent = 0;
          //$percent1 = 0;
          //$percent2 = 0;
          //$points = 0;
          //
          //$is_match = false;
          //$email_count = strlen($email_name);
          //
          //foreach($f_name_arr as $f_name) {
          //  if (empty($f_name)) {
          //    continue;
          //  }
          //  $f_count = strlen($f_name);
          //  $percent = $this->LevenshteinDistance($email_name, $f_name);
          //  $diff = $email_count - $percent;
          //
          //  if (($diff == $f_count  || ($diff > ($f_count/2))) && $f_count > 2) {
          //
          //    $is_match = true;
          //    $points = 100;
          //  }
          //}
          //
          //foreach($m_name_arr as $m_name) {
          //  if (empty($m_name)) {
          //    continue;
          //  }
          //  $m_count = strlen($m_name);
          //  $percent1 = $this->LevenshteinDistance($email_name, $m_name);
          //  $diff = $email_count - $percent1;
          //
          //  if (($diff == $m_count || ($diff > ($m_count/2))) && $m_count > 2) {
          //    $is_match = true;
          //    $points += 100;
          //  }
          //}
          //
          //foreach($l_name_arr as $l_name) {
          //  if (empty($l_name)) {
          //    continue;
          //  }
          //  $l_count = strlen($l_name);
          //  $percent2 = $this->LevenshteinDistance($email_name, $l_name);
          //  $diff = $email_count - $percent2;
          //
          //  if (($diff == $l_count || ($diff > ($l_count/2))) && $l_count > 2) {
          //    $is_match = true;
          //    $points += 100;
          //  }
          //}
          //
          //$emailpoints[$hrmsid][$email] = $points;
          //$white_list_email = $this->user_update_model->get_whitelist_email($hrmsid);
          //$black_list_email = $this->user_update_model->get_blacklist_email($hrmsid); // Removed as it was not used in code
           //$user = array();
          if ($error_cnt == 0) {
            $processed_user_data[] = $user_data;
          }
          unset($data);
     // }
        
      /* if ($error_cnt == 0) {
        $processed_user_data[] = $user_data;
      } */

      $row_cnt++;
    }
    fclose($file);
  } 
    $check_result['proceed'] = 1;
    $check_result['error'] = $row_validation;
    $check_result['data'] = $processed_user_data;
    
    return $check_result;
  }
  
  function LevenshteinDistance($s1, $s2) { 
    $sLeft = (strlen($s1) > strlen($s2)) ? $s1 : $s2; 
    $sRight = (strlen($s1) > strlen($s2)) ? $s2 : $s1; 
    $nLeftLength = strlen($sLeft); 
    $nRightLength = strlen($sRight); 
    if ($nLeftLength == 0) 
      return $nRightLength; 
    else if ($nRightLength == 0) 
      return $nLeftLength; 
    else if ($sLeft === $sRight) 
      return 0; 
    else if (($nLeftLength < $nRightLength) && (strpos($sRight, $sLeft) !== FALSE)) 
      return $nRightLength - $nLeftLength; 
    else if (($nRightLength < $nLeftLength) && (strpos($sLeft, $sRight) !== FALSE)) 
      return $nLeftLength - $nRightLength; 
    else { 
      $nsDistance = range(1, $nRightLength + 1); 
      for ($nLeftPos = 1; $nLeftPos <= $nLeftLength; ++$nLeftPos) 
      { 
        $cLeft = $sLeft[$nLeftPos - 1]; 
        $nDiagonal = $nLeftPos - 1; 
        $nsDistance[0] = $nLeftPos; 
        for ($nRightPos = 1; $nRightPos <= $nRightLength; ++$nRightPos) 
        { 
          $cRight = $sRight[$nRightPos - 1]; 
          $nCost = ($cRight == $cLeft) ? 0 : 1; 
          $nNewDiagonal = $nsDistance[$nRightPos]; 
          $nsDistance[$nRightPos] = 
            min($nsDistance[$nRightPos] + 1, 
                $nsDistance[$nRightPos - 1] + 1, 
                $nDiagonal + $nCost); 
          $nDiagonal = $nNewDiagonal; 
        } 
      } 
      return $nsDistance[$nRightLength]; 
    } 
  }
  
  function _process_user_list($users_array) {
    
    $userInsertFailedCount = 1;
    $userInsertCount = 1;
    $userUpdateDetailsCount = 1;
    $userInsertFailed = "<table border='1' bordercolor='black' cellpadding='10'><tr><th>SR.</th><th>HRMS ID</th><th>EMAIL ID</th><th>USER NAME</th></tr>";
    $userInsert = "<table border='1' bordercolor='black' cellpadding='10'><tr><th>SR.</th><th>HRMS ID</th><th>EMAIL ID</th><th>USER NAME</th></tr>";
    $userUpdateDetails = "<table border='1' bordercolor='black' cellpadding='10'><tr><th>SR.</th><th>HRMS ID</th><th>EMAIL ID</th></tr>";
    
    $new_user_arr = array();
    $territory_arr = array();
    $concept_arr = array();
    $designation_arr = array();
    $department_arr = array();
    $location_arr = array();
    $band_arr = array();
    $log = array();
    $user_update_log = array();
    $ins_cnt = 0;
    $up_cnt = 0;
    
    $dup_email_cnt = 0;
    $dup_user = array();
    $dup_email = array();
    
    foreach ($users_array as $user_info) {

      $email = strtolower(trim(addslashes($user_info[18])));
      $hrms_id = addslashes($user_info[3]);
      $title = addslashes($user_info[4]);
      $first_name = ucwords(strtolower(trim(addslashes($user_info[5]))));
      $middle_name = ucwords(strtolower(trim(addslashes($user_info[6]))));
      $last_name = ucwords(strtolower(trim(addslashes($user_info[7]))));
      $hiredon = date('Y-m-d', strtotime(addslashes($user_info[8])));
      $joinedon = date('Y-m-d', strtotime(addslashes($user_info[9])));
      $dob = date('Y-m-d', strtotime(addslashes($user_info[10])));
      $supervisor_id = addslashes($user_info[20]);
      $concept = addslashes($user_info[14]);
      $department = addslashes($user_info[13]);
      $designation = addslashes($user_info[12]);
      $location = addslashes($user_info[17]);
      $territory = addslashes($user_info[1]);
      $extension = addslashes($user_info[24]);
      $phone = addslashes($user_info[23]);
      $mobile = addslashes($user_info[22]);
      $band = addslashes($user_info[21]);
      $territory = str_replace("Landmark ", "", $territory);
      $territory = str_replace("HR Business Group", "", $territory);
      $user_id = NULL;
      
      // New User Check based on HRMS ID
      $new_user_check = $this->user_update_model->new_user_check($hrms_id);
      
      if (!count($new_user_check)) {
        $new_user_arr[$email] = $first_name . " " . $last_name . ", " . $email;
      } else {
        $user_id = $new_user_check[0]['id'];
      }

      //New Territory Check
      $new_territory_check = $this->user_update_model->new_territory_check($territory);
      if (!count($new_territory_check)) {
        $territory_arr[$territory] = $territory;
        $data_territory = array('name' => $territory);
        $territory_id = $this->user_update_model->new_territory_insert($data_territory);
      } else {
        $territory_id = $new_territory_check[0]['id'];
      }

      //New Concept Check
      $new_concept_check = $this->user_update_model->new_concept_check($concept);
      if (!count($new_concept_check)) {
        $concept_arr[$concept] = $concept;
        $data_concept = array('name' => $concept, 'db_concept_name' => $concept);
        $concept_id = $this->user_update_model->new_concept_insert($data_concept);
      } else {
        $concept_id = $new_concept_check[0]['id'];
      }

      //New Designation Check
      $new_designation_check = $this->user_update_model->new_designation_check($designation);
      if (!count($new_designation_check)) {
        $designation_arr[$designation] = $designation;
        $data_designation = array('name' => $designation);
        $designation_id = $this->user_update_model->new_designation_insert($data_designation);
      } else {
        $designation_id = $new_designation_check[0]['id'];
      }

      //New Department Check
      $new_department_check = $this->user_update_model->new_department_check($department);
      if (!count($new_department_check)) {
        $department_arr[$department] = $department;
        $data_department = array('name' => $department);
        $department_id = $this->user_update_model->new_department_insert($data_department);
      } else {
        $department_id = $new_department_check[0]['id'];
      }

      //New Location Check
      $new_location_check = $this->user_update_model->new_location_check($location);
      if (!count($new_location_check)) {
        $location_arr[$location] = $location;
        $data_location = array('name' => $location);
        $location_id = $this->user_update_model->new_location_insert($data_location);
      } else {
        $location_id = $new_location_check[0]['id'];
      }

      //New Band Check
      $new_band_check = $this->user_update_model->new_band_check($band);
      if (!count($new_band_check)) {
        $band_arr[$band] = $band;
        $data_band = array('name' => $band);
        $band_id = $this->user_update_model->new_band_insert($data_band);
      } else {
        $band_id = $new_band_check[0]['id'];
      }
      
      $user_insert_array = array();
      $user_update_array = array();
      $display_name = '';
      if(!empty($first_name)) {
       $display_name.= $first_name;
      }
      if(!empty($last_name)) {
        $display_name.= ' '.$last_name;
      }
      
      $validDomains = array();
      $validDomains = $this->config->item('valid_email_domain');
      
      if ($user_id) {  
        $hrmsDetails = $this->user_update_model->getHrmsDetailsById($user_id);
        $sendWelcomeMail = $hrmsDetails->welcome_mail;
        if(!empty($email)) {  
          $emailExist = $this->user_update_model->check_email_exist($email);
          if(empty($emailExist)) {
            /** Email filtering processs **/
            $user_email = '';
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
              list($uname,$domain) = @explode('@',$email);
              if(in_array($domain,$validDomains)){
                
                // Check the email id with first name and last name
                  $email_points = $this->validateEmailId($email, $first_name, $middle_name, $last_name);
                  if($email_points >= 100 ) {
                    $user_email = $email;
                    if($hrmsDetails->last_login_time == 0) {
                      $sendWelcomeMail = 0;
                    }
                  }
              }
            }
            $updateEmail = $user_email;
          }else{
            $updateEmail = $hrmsDetails->email;
          }
        }else{
          $updateEmail = "";
        }
        
        $user_update_array = array(
            'email' => $updateEmail,
            'hrms_id' => $hrms_id,
            'title' => $title,
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'last_name' => $last_name,
            'hiredon' => $hiredon,
            'joinedon' => $joinedon,
            'dob' => $dob,
            'supervisor_id' => $supervisor_id,
            'concept_id' => $concept_id,
            'department_id' => $department_id,
            'designation_id' => $designation_id,
            'location_id' => $location_id,
            'territory_id' => $territory_id,
            'band_id' => $band_id,
            'extension' => $extension,
            'phone' => $phone,
            'mobile' => $mobile,
            'welcome_mail'=>$sendWelcomeMail
        );
        
        $up_cnt++;
        $userUpdateDetails.= "<tr><td>$userUpdateDetailsCount</td><td>$hrms_id</td><td>$user_email</td></tr>";
        $userUpdateDetailsCount++;
        $this->user_update_model->update_user($user_update_array, $user_id);
        $user_update_log[] = "Update Done for: $email";
        
      } else {
        // New User Details
        $user_email = '';
        $new_pass = '';
        $random_no = rand(1,35);
        $profile_pic = "default/$random_no.png";
        
        $dob1 = date('dmy', strtotime(addslashes($user_info[10])));
        if(!empty($dob1)) {
         $new_pass = $hrms_id.'_'.$dob1; 
        }else{
          $new_pass = $hrms_id;
        }
        
        /** Email filtering processs **/
        $user_email = '';
        if( !empty($email) && (filter_var($email, FILTER_VALIDATE_EMAIL)) ) {
          list($uname,$domain) = @explode('@',$email);
          if(in_array($domain,$validDomains)){
            // Remove the email ids which are already exist like supervisiour emails ids and store email ids
            $email_exist = $this->user_update_model->check_email_exist($email);
            if(empty($email_exist)) {
              // Check the email id with first name and last name
              $email_points = $this->validateEmailId($email, $first_name, $middle_name, $last_name);
              if($email_points >= 100 ) {
                $user_email = $email;
              }
            }else{
              $user_email = '';
            }
          }else{
            // Dont store the email ids from non-landmark domian
            $user_email = '';  
          }
        }
        /**
         * create the reply_username
         * reply_username = first_name.last_name
         * reply_username is unique value
         **/
        $reply_uname = "";          
        if(!empty($first_name)) {
          $reply_uname.=strtolower(str_replace(" ","",$first_name));
        }
        if(!empty($first_name) && !empty($last_name) ) {
          $reply_uname.='.';
        }
        if(!empty($last_name)) {
          $reply_uname.=strtolower(str_replace(" ","",$last_name));
        }
        $reply_uname = str_replace("'","",stripslashes($reply_uname));
        if(empty($reply_uname)) { continue; } 
        $reply_username_exist = $this->new_user_update_model->check_replyusername_exist($reply_uname);
        if(empty($reply_username_exist)) {
          $username = $reply_uname;
        }else{
          $username = $this->genrate_username($reply_uname);
        }
        
        $user_insert_array = array(
            'id' => NULL,
            'email' => $user_email,
            'reply_username' => $username,
            'password' => md5($new_pass),
            'hrms_id' => $hrms_id,
            'title' => $title,
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'last_name' => $last_name,
            'display_name' => ucwords(strtolower($display_name)),
            'hiredon' => $hiredon,
            'joinedon' => $joinedon,
            'dob' => $dob,
            'supervisor_id' => $supervisor_id,
            'concept_id' => $concept_id,
            'department_id' => $department_id,
            'designation_id' => $designation_id,
            'location_id' => $location_id,
            'territory_id' => $territory_id,
            'band_id' => $band_id,
            'extension' => $extension,
            'phone' => $phone,
            'mobile' => $mobile,
            'skills' => '',
            'about' => '',
            'profile_pic' => $profile_pic,
            'user_type' => 1,
            'welcome_mail' => 0,
            'mt_login_user' => '',
            'mt_login_password' => '',
            'last_login_time' => 0,
            'create_dttm' => date('Y-m-d h:i:s', time()),
        );
        
        if ($this->user_update_model->new_user_insert($user_insert_array)) {
          $userInsert.= "<tr><td>$userInsertCount</td><td>$hrms_id</td><td>$user_email</td><td>$username</td></tr>";
          $userInsertCount++;
          $user_update_log[] = "Insert Done for: $email";
        } else {
          $userInsertFailed.= "<tr><td>$userInsertFailedCount</td><td>$hrms_id</td><td>$user_email</td><td>$username</td></tr>";
          $userInsertFailedCount++;
          $user_update_log[] = "Failed Insert for: $email";
        }
        $ins_cnt++;
      }
    }
    
    $userInsert.= "</table>";
    $userInsertFailed.= "</table>";
    $userUpdateDetails.= "</table>";
    
    /* Pre update log */
    //Printing All New Users
    $new_usr_log = array();
    $new_user_cnt = 0;
    foreach ($new_user_arr as $val) {
      $new_usr_log[] = $val;
      $new_user_cnt = $new_user_cnt + 1;
    }
    $log[] = "<h2>New User: $new_user_cnt</h2>";
    $log = array_merge($log, $new_usr_log);
    //Printing All New Territories
    $territory_cnt = 0;
    $territory_log = array();
    foreach ($territory_arr as $val) {
      if ($val) {
        $territory_log[] = $val;
        $territory_cnt = $territory_cnt + 1;
      }
    }
    $log[] = "<h2>New Territory: $territory_cnt</h2>";
    $log = array_merge($log, $territory_log);

    //Printing All New Concepts
    $concept_cnt = 0;
    $concept_log = array();
    foreach ($concept_arr as $val) {
      if ($val) {
        $concept_log[] = $val;
        $concept_cnt = $concept_cnt + 1;
      }
    }
    $log[] = "<h2>New Concept: $concept_cnt</h2>";
    $log = array_merge($log, $concept_log);

    //Printing All New Designation
    $designation_cnt = 0;
    $designation_log = array();
    foreach ($designation_arr as $val) {
      if ($val) {
        $designation_log[] = $val;
        $designation_cnt = $designation_cnt + 1;
      }
    }
    $log[] = "<h2>New Designation: $designation_cnt</h2>";
    $log = array_merge($log, $designation_log);

    //Printing All New Department
    $department_cnt = 0;
    $dept_log = array();
    foreach ($department_arr as $val) {
      if ($val) {
        $dept_log[] = $val;
        $department_cnt = $department_cnt + 1;
      }
    }
    $log[] = "<h2>New Department: $department_cnt</h2>";
    $log = array_merge($log, $dept_log);

    //Printing All New Location
    $location_cnt = 0;
    $loc_log = array();
    foreach ($location_arr as $val) {
      if ($val) {
        $loc_log[] = $val;
        $location_cnt = $location_cnt + 1;
      }
    }
    $log[] = "<h2>New Location: $location_cnt</h2>";
    $log = array_merge($log, $loc_log);

    //Printing All New Band
    $band_cnt = 0;
    $band_log = array();
    foreach ($band_arr as $val) {
      if ($val) {
        $band_log[] = $val;
        $band_cnt = $band_cnt + 1;
      }
    }
    $log[] = "<h2>New Band: $band_cnt</h2>";
    $log = array_merge($log, $band_log);

    $user_output['pre_update_log'] = $log;
    $user_output['post_update_log'] = $user_update_log;
    $user_output['user_update_cnt'] = $up_cnt;
    $user_output['user_insert_cnt'] = $ins_cnt;
    //Email Content
    $user_output['email_content'] = 'Swati,<br /><br />
                                     Below are the user update details for ' . date('jS F') . ' update.<br /><br />
                                     Summary:
                                      <ul>
                                        <li>No. of users whose information was updated: ' . $up_cnt . '</li>
                                        <li>No. of new users added: ' . $ins_cnt . '</li>
                                        <li>No. of welcome mails to be sent in database : 0</li>
                                        <li>No. of welcome mails failed : 0</li>
                                        <li>No. of welcome mails sent : 0</li>
                                        <li>Duplicate users in the db: ' . $dup_email_cnt . '</li>
                                        <li>New designations imported: ' . $designation_cnt . '</li>
                                        <li>New Concepts imported: ' . $concept_cnt . '</li>
                                        <li>New bands imported :' . $band_cnt . '</li>
                                        <li>New locations imported :' . $location_cnt . '</li>
                                      </ul>
                                      Logs are attached.<br /><br />
                                      Regards<br />
                                      Raghunath
                                      ';
    $user_output['user_import_data']['insert'] = $userInsert;
    $user_output['user_import_data']['update'] = $userUpdateDetails;
    $user_output['user_import_data']['insertfail'] = $userInsertFailed;
    return $user_output;
    /* ------------------------------Updating Users--------------------------------------- */
  }

//User Purge list pre check
  function _user_purge_data_check($file_path) {

    $file = fopen($file_path, "r") or exit("Unable to open file!");
    $row_cnt = 1;
    $row_validation = array();
    $new_user_arr = array();
    $territory_arr = array();
    $concept_arr = array();
    $designation_arr = array();
    $department_arr = array();
    $location_arr = array();
    $band_arr = array();
    $processed_user_data = array();
    while (!feof($file)) {
      $row = fgets($file);
      $user_data = array();
      $user_data = explode(",", $row);
      //print_r($user_data); exit;
      if (!$user_data[0]) {
        continue;
      }

      //Row Header validation
      if ($row_cnt == 1) {
        $error_array = array();
        if (trim($user_data[0]) != 'POSITION_ID') {
          $error_array[] = 'Position Id missing(POSITION_ID)';
        }
        if (trim($user_data[1]) != 'EMPLOYEE_NUMBER') {
          $error_array[] = 'Employee number missing(EMPLOYEE_NUMBER)';
        }
        if (trim($user_data[2]) != 'FULL_NAME') {
          $error_array[] = 'Full Name is missing(FULL_NAME)';
        }
        if (trim($user_data[3]) != 'BUSINESS_GROUP_ID') {
          $error_array[] = 'Business group ID missing(BUSINESS_GROUP_ID)';
        }
        if (trim($user_data[4]) != 'USER_PERSON_TYPE') {
          $error_array[] = 'User person type is missing(USER_PERSON_TYPE)';
        }
        if (trim($user_data[5]) != 'NAME') {
          $error_array[] = 'Name is missing(NAME)';
        }
        if (trim($user_data[6]) != 'ACTUAL_TERMINATION_DATE') {
          $error_array[] = 'Actual termination date missing(ACTUAL_TERMINATION_DATE )';
        }
        if (trim($user_data[7]) != 'EMAIL_ADDRESS') {
          $error_array[] = 'Email address missing(EMAIL_ADDRESS)';
        }
        if (trim($user_data[8]) != 'REASON') {
          $error_array[] = 'Reason missing(REASON)';
        }

        if (count($error_array)) {
          //echo '<xmp>' . print_r($error_array, 1) . '</xmp>';
          //exit;
          $check_result['proceed'] = 0;
          $check_result['error'] = $error_array;
          $check_result['data'] = array();
          return $check_result;
        }
        $row_cnt++;
        continue;
      }
      $error_cnt = 0;
      if (count($user_data) != 9) {
        $row_validation[] = 'Row count missmatch, didnt find 8 columns, Total columns: ' . count($user_data) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }
      if (!ctype_digit(trim($user_data[0]))) {
        $row_validation[] = 'Position Id should be a integer value:' . trim($user_data[0]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }

      if (!ctype_digit(trim($user_data[1]))) {
        $row_validation[] = 'Employee number should be a integer value: ' . trim($user_data[1]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }
      if (!ctype_digit(trim($user_data[3]))) {
        $row_validation[] = 'Business group ID should be a integer value: ' . trim($user_data[3]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }

      if (!filter_var(trim($user_data[7]), FILTER_VALIDATE_EMAIL)) {
        $row_validation[] = 'Invalid email address: ' . trim($user_data[7]) . ', Error at row no.: ' . $row_cnt;
        $error_cnt++;
      }
      if ($error_cnt == 0) {
        $processed_user_data[] = $user_data;
      }

      $row_cnt++;
    }
    fclose($file);
    //echo '<xmp>' . print_r($row_validation, 1) . '</xmp>';
    $check_result['proceed'] = 1;
    $check_result['error'] = $row_validation;
    $check_result['data'] = $processed_user_data;
    return $check_result;
  }

  function _process_purge_list($user_data) {
    if (!count($user_data)) {
      return false;
    }
    $log = array();
    $found_cnt = 0;
    $not_found_cnt = 0;
    foreach ($user_data as $user) {
      $emp_id = $user[1];
      $email = $user[7];
      $reason = trim($user[8]);

      if ($reason == 'Employee Transfer') {
        $log[] = 'Employee transfer for user with Employee number: ' . $emp_id . ' and Email: ' . $email;
      } else {
        $user_check = $this->user_update_model->user_db_check($emp_id, $email);
        if (count($user_check)) {
          $user_id = $user_check['id'];
          $this->user_update_model->delete_user_trace($user_id);
          $log[] = 'User deleted with Employee number: ' . $emp_id . ' and Email: ' . $email;
          $found_cnt++;
        } else {
          $log[] = 'User not found with Employee number: ' . $emp_id . ' and Email: ' . $email;
          $not_found_cnt++;
        }
      }
    }
    $purge_output['log'] = $log;
    $purge_output['del_user'] = $found_cnt;
    $purge_output['not_found_user'] = $not_found_cnt;

    return $purge_output;
  }
  function _send_welcome_kit() {

    $site_url = site_url();
    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    /*
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
     */
    /*
    $config['smtp_host'] = '83.111.79.200';
    $config['mailtype'] = "html";
    */

    $user_list = $this->moderate_model->get_welcome_users();
    if (!count($user_list)) {
      redirect("backend/moderate");
    }
    foreach ($user_list as $user_details) {
      if(!empty($user_details['email'])) {
        $user_id = $user_details['id'];
        $name = $user_details['first_name'];
        $email = $user_details['email'];
        
        //random password -- Old code of pass
        //$length = 8;
        //$level = 3;
        //list($usec, $sec) = explode(' ', microtime());
        //srand((float) $sec + ((float) $usec * 100000));
        //
        //$validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
        //$validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //$validchars[3] = "0123456789_!@#$%&*()abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()";
        //
        //$password = "";
        //$counter = 0;
        //
        //while ($counter < $length) {
        //  $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level]) - 1), 1);
        //
        //  // All character must be different
        //  if (!strstr($password, $actChar)) {
        //    $password .= $actChar;
        //    $counter++;
        //  }
        //}
        
        $this->load->library('intranet');
       // $password = $this->intranet->generateUniqueToken(8);
        
        //$update_pass = array(
        //  'password' => md5($password)
        //);
        //$result = $this->moderate_model->update_user_data($update_pass, $user_id);
        $this->load->library('email');
        
        $sender_email = "intranet@landmarkgroup.com";
        $sender_name = "Landmark Intranet";
        //$email = "intranet.lmg@gmail.com";
        //$to_email = $email;
        //$to_email = "raghu111@gmail.com";
        //$to_email = "thomson.george@cplmg.com";
        $to_email = $email;
        
        
        $subject = "Login to the Landmark Intranet";
        $message = '<table width="520" border="0" style="font-family:Arial;">
        <tr>
        <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
          <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">Welcome to the new Landmark Intranet</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;"><strong>Dear ' . $name . ',</strong> </p>
          <p style="color:#3e3e3e;font-size:14px; margin:5px 0 0 0; line-height:20px;">The all new Landmark Intranet allows you to collaborate with colleagues across various concepts and keep abreast with the latest in the company.</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Your account on the Intranet has been created. You can login using the following details.</p>
          <table  style="color:#3e3e3e;font-size:14px;margin-top:10px;margin-left:20px;">
            <tr>
              <td>Visit:</td>
              <td><a style="color:#1e5886;font-size:16px;" href="'.$site_url.'">'.$site_url.'</a></td>
            </tr>
            <tr>
              <td>Username:</td>
              <td>' . $email . '</td>
            </tr>
            <tr> <td>Password Format:</td> <td>EmployeeNumber_DOB (DDMMYY format)</td>  </tr>
          </table>
          <p style="color:#3e3e3e;font-size:14px;margin:13px 0 0 0">Eg: For Employee Number (12345) and Date of Birth (01-Feb-1980), then password would be : 12345_010280</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">As soon as you login please change your password and complete your profile.</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Thanks,<br />
          Landmark Intranet</p>
        </td>
        </tr>
        
        
        </table>';
        
        $this->email->initialize($smtp_config);
        $this->email->from($sender_email, $sender_name);
        $this->email->set_newline("\r\n");
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($to_email);
        if ($this->email->send()) {
          $update_welcome_status = array(
              'welcome_mail' => 1,
			  'welcome_mail_time' => date("Y-m-d H:i:s")
          );
          $this->moderate_model->update_user_data($update_welcome_status, $user_id);
          echo "Done: $email<br/>";
          
        } else {
          echo "<div style='color:#ff0000;'>Fail: $email</div>";
        }
      }
    }
    echo "<h1>All Mail send</h1>";
  }
  function sftp_connection() {
    
    //$this->load->helper('sftp_lib');
    $log_path = '/var/www/html/sftp_reports/';
    $local_directory = "/var/www/html/v2/intranet_users_csv/";

    $current_date = date("Ymd");
    $user_list_name = "diff_$current_date.csv";
    $ex_emp_list = "ex_emp_$current_date.csv";
    $intranet_user_csv = "intranet_users_$current_date.csv";
    
    if ((!file_exists($local_directory.$intranet_user_csv)) && (@filesize($local_directory.$intranet_user_csv) <= 0) && (@filesize($local_directory.$ex_emp_list) <= 0) && (@filesize($local_directory.$user_list_name) <= 0)) {
      echo "User Import CSV files missing";
      exit;
    }
    
    echo 'User file name: ' . $local_directory . $user_list_name;
    //echo '<br />Purge file name: ' . $purge_list_name;

    $user_update = $this->_user_update_data_check($local_directory . $user_list_name);
    //$user_update = $this->_user_update_data_check("importer/in/diff_20140714.csv");
    
    if ($user_update['proceed'] == 1) {
      $user_output = $this->_process_user_list($user_update['data']);
      $remove_ex_emp = $this->remove_ex_employess($local_directory . $ex_emp_list);
      
      echo '------------------------------------------Error------------------------------------<br/>';
      //echo @implode('<br />', $user_update['error']);
      $user_data_error_log = '<h1 style="text-align:center;">User Data Error Report (' . date('Y-m-d') . ')</h1>' . @implode('<br />', $user_update['error']);
      $this->_create_log_file($log_path . 'intranet_user_data_error_report_' . date('Y-m-d') . '.docx', $user_data_error_log);

      echo '<br/>------------------------------------------Pre Update log------------------------------------<br/>';
      //echo @implode('<br />', $user_output['pre_update_log']);
      $user_data_pre_log = '<h1 style="text-align:center;">User Data Report (' . date('Y-m-d') . ')</h1>' . @implode('<br />', $user_output['pre_update_log']);
      $this->_create_log_file($log_path . 'intranet_pre_update_user_report_' . date('Y-m-d') . '.docx', $user_data_pre_log);

      
       echo '<br/>------------------------------------------Pre Update log------------------------------------<br/>';
      //echo @implode('<br />', $user_output['pre_update_log']);
      $user_deactivate = '<h1 style="text-align:center;">Deactivated users </h1> : '.$remove_ex_emp.'';
      $this->_create_log_file($log_path . 'intranet_deactivate_user_report_' . date('Y-m-d') . '.docx', $user_deactivate);
      
      
      echo '<br/>------------------------------------------Post Update log------------------------------------<br/>';
      //echo @implode('<br />', $user_output['post_update_log']);

      $user_data_post_log = "Updating User Data Report (" . date('Y-m-d') . ")\n\n" . @implode("\n", $user_output['post_update_log']);
      $user_data_post_log .= "\n\nUsers updated: " . $user_output['user_update_cnt'];
      $user_data_post_log .= "\nUsers added: " . $user_output['user_insert_cnt'];

      $this->_create_log_file($log_path . 'intranet_post_update_user_report_' . date('Y-m-d') . '.log', $user_data_post_log);

      echo '<br/>------------------------------------------Email Content------------------------------------<br/>';
      echo $user_output['email_content'];
    } else {
      echo @implode('<br />', $user_update['error']);
    }
    if (site_url() == 'https://intranet.landmarkgroup.com/') {
      $this->_send_welcome_kit();
    }
    $this->_send_admin_mail($user_output);
  }
  
  function _send_admin_mail($user_output) {

    $site_url = site_url();
    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    $this->load->library('intranet');
    $this->load->library('email');
    
    $sender_email = "intranet@landmarkgroup.com";
    $sender_name = "Landmark Intranet";
    //$email = "intranet.lmg@gmail.com";
    //$to_email = $email;
    //$to_email = "raghu111@gmail.com";
    //$to_email = "thomson.george@cplmg.com";
    $to_email = 'ashish.dalvi@intelliswift.co.in';
    $userInsert = $user_output['user_import_data']['insert'];
    $userUpdate = $user_output['user_import_data']['update'];
    $userInsertFails = $user_output['user_import_data']['insertfail'];
    
    $date = date('d-M-Y',time());
    $subject = "User Import process Report for $date";
    $message = '<table width="520" border="0" style="font-family:Arial;">
    <tr>
    <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
      <p>Dear Admin</p>
      <p> User Import process Report for '.$date.'</p>
      <p>New Users List : </p>'.$userInsert.'
      <p>Updated Users List : </p>'.$userUpdate.'
      <p>User Insertion Fails List : </p>'.$userInsertFails.'
    </td>
    </tr>
    </table>';
    $this->email->initialize($smtp_config);
    $this->email->from($sender_email, $sender_name);
    $this->email->set_newline("\r\n");
    $this->email->subject($subject);
    $this->email->message($message);
    $this->email->to($to_email);
    if ($this->email->send()) {
      echo "Done: $to_email<br/>";
    } else {
      echo "<div style='color:#ff0000;'>Fail: $to_email</div>";
    }
  }
  
  function send_welcome_kit() {

    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    /*
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
     */
    /*
    $config['smtp_host'] = '83.111.79.200';
    $config['mailtype'] = "html";
    */


    $user_list = $this->moderate_model->get_welcome_users_bulk_import();
    if (!count($user_list)) {
      redirect("backend/moderate");
    }
    foreach ($user_list as $user_details) {
      $user_id = $user_details['id'];
      $name = $user_details['first_name'];
      $email = $user_details['email'];

      //random password
      $length = 8;
      $level = 3;
      list($usec, $sec) = explode(' ', microtime());
      srand((float) $sec + ((float) $usec * 100000));

      $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
      $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      $validchars[3] = "0123456789_!@#$%&*()abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()";

      $password = "";
      $counter = 0;

      while ($counter < $length) {
        $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level]) - 1), 1);

        // All character must be different
        if (!strstr($password, $actChar)) {
          $password .= $actChar;
          $counter++;
        }
      }

      $update_pass = array(
          'password' => md5($password)
      );
      $result = $this->moderate_model->update_user_data($update_pass, $user_id);
      $this->load->library('email');

      $sender_email = "intranet@landmarkgroup.com";
      $sender_name = "Landmark Intranet";
      //$email = "intranet.lmg@gmail.com";
      //$to_email = $email;
      //$to_email = "raghu111@gmail.com";
      //$to_email = "thomson.george@cplmg.com";
      //$to_email = "kunal.patil@intelliswift.co.in";
	  $to_email = $email;


      $subject = "Login to the Landmark Intranet";
      $message = '<table width="520" border="0" style="font-family:Arial;">
       <tr>
        <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
          <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">Welcome to the new Landmark Intranet</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;"><strong>Dear ' . $name . ',</strong> </p>
          <p style="color:#3e3e3e;font-size:14px; margin:5px 0 0 0; line-height:20px;">The all new Landmark Intranet allows you to collaborate with colleagues across various concepts and keep abreast with the latest in the company.</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Your account on the Intranet has been created. You can login using the following details.</p>
          <table  style="color:#3e3e3e;font-size:14px;margin-top:10px;margin-left:20px;">
            <tr>
              <td>Visit:</td>
              <td><a style="color:#1e5886;font-size:16px;" href="http://intranet.landmarkgroup.com/">http://intranet.landmarkgroup.com/</a></td>
            </tr>
            <tr>
              <td>Username:</td>
              <td>' . $email . '</td>
            </tr>
            <tr>
              <td>Password:</td>
              <td>' . $password . '</td>
            </tr>
          </table>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">As soon as you login please change your password and complete your profile.</p>

          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Thanks,<br />
          Landmark Intranet</p>
        </td>
      </tr>


    </table>';

      $this->email->initialize($smtp_config);
      $this->email->from($sender_email, $sender_name);
      $this->email->set_newline("\r\n");
      $this->email->subject($subject);
      $this->email->message($message);
      $this->email->to($to_email);
      if ($this->email->send()) {
        $update_welcome_status = array(
            'welcome_mail' => 1
        );
        $this->moderate_model->update_user_data($update_welcome_status, $user_id);
        echo "Done: $email<br/>";
      } else {
        echo "<div style='color:#ff0000;'>Fail: $email</div>";
      }
    }
    echo "<h1>All Mail send</h1>";
  }
  
  function genrate_username($uname) {
    $count = 0;
    $i = 1;
    $new_name = '';
    while(true){
      $new_name = $uname.$i;
      $reply_username_exist = $this->new_user_update_model->check_replyusername_exist($new_name);
      
      if(empty($reply_username_exist)) {
        break;
      }
      $i++;
    }
    return $new_name;
   }
   
   function validateEmailId($email, $fname, $mname, $lname) {
    
    $emails = explode("@", $email); // extract the email username - the part before @
    $email1 = $emails[0];
    
    $emailpoints = array();
    $fname = strtolower(trim($fname));
    $mname = strtolower(trim($mname));
    $lname = strtolower(trim($lname));
    
    $points = 0;
    
    $email_name = str_replace('.','',$email1);
    $email_name = str_replace('_','',$email_name);
    $email_name = str_replace('-','',$email_name);
    
    $f_name_arr = explode(' ',$fname);
    $m_name_arr = explode(' ',$mname);
    $l_name_arr = explode(' ',$lname);
    
    $f_count = 0;
    $m_count = 0;
    $l_count = 0;
    $percent = 0;
    $percent1 = 0;
    $percent2 = 0;
    $points = 0;
    
    $is_match = false;
    $email_count = strlen($email_name);
    
    foreach($f_name_arr as $f_name) {
      if (empty($f_name)) {
        continue;
      }
      $f_count = strlen($f_name);
      $percent = $this->LevenshteinDistance($email_name, $f_name);
      $diff = $email_count - $percent;
    
      if (($diff == $f_count  || ($diff > ($f_count/2))) && $f_count > 2) {
    
        $is_match = true;
        $points = 100;
      }
    }
    
    foreach($m_name_arr as $m_name) {
      if (empty($m_name)) {
        continue;
      }
      $m_count = strlen($m_name);
      $percent1 = $this->LevenshteinDistance($email_name, $m_name);
      $diff = $email_count - $percent1;
    
      if (($diff == $m_count || ($diff > ($m_count/2))) && $m_count > 2) {
        $is_match = true;
        $points += 100;
      }
    }
    
    foreach($l_name_arr as $l_name) {
      if (empty($l_name)) {
        continue;
      }
      $l_count = strlen($l_name);
      $percent2 = $this->LevenshteinDistance($email_name, $l_name);
      $diff = $email_count - $percent2;
    
      if (($diff == $l_count || ($diff > ($l_count/2))) && $l_count > 2) {
        $is_match = true;
        $points += 100;
      }
    }  
    return $points;
   }
   
   function test_send_welcome_kit() {

    $site_url = site_url();
    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    
        $user_id = '';
        $name = 'ashish';
        $email = 'ashish.dalvi@intelliswift.co.in';
        
        $this->load->library('intranet');
        
        $this->load->library('email');
        
        $sender_email = "intranet@landmarkgroup.com";
        $sender_name = "Landmark Intranet";
        //$email = "intranet.lmg@gmail.com";
        //$to_email = $email;
        //$to_email = "raghu111@gmail.com";
        //$to_email = "thomson.george@cplmg.com";
        $to_email = $email;
        
        
        $subject = "Login to the Landmark Intranet";
        $message = '<table width="520" border="0" style="font-family:Arial;">
        <tr>
        <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
          <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">Welcome to the new Landmark Intranet</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;"><strong>Dear ' . $name . ',</strong> </p>
          <p style="color:#3e3e3e;font-size:14px; margin:5px 0 0 0; line-height:20px;">The all new Landmark Intranet allows you to collaborate with colleagues across various concepts and keep abreast with the latest in the company.</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Your account on the Intranet has been created. You can login using the following details.</p>
          <table  style="color:#3e3e3e;font-size:14px;margin-top:10px;margin-left:20px;">
            <tr>
              <td>Visit:</td>
              <td><a style="color:#1e5886;font-size:16px;" href="'.$site_url.'">'.$site_url.'</a></td>
            </tr>
            <tr>
              <td>Username:</td>
              <td>' . $email . '</td>
            </tr>
            <tr>
              <tr> <td>Password Format:</td> <td>EmployeeNumber_DOB (DDMMYY format)</td>  </tr>
            </tr>
          </table>
          <p style="color:#3e3e3e;font-size:14px;margin:13px 0 0 0">Eg: For Employee Number (12345) and Date of Birth (01-Feb-1980), then password would be : 12345_010280</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">As soon as you login please change your password and complete your profile.</p>
        
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Thanks,<br />
          Landmark Intranet</p>
        </td>
        </tr>
        
        
        </table>';
        
        $this->email->initialize($smtp_config);
        $this->email->from($sender_email, $sender_name);
        $this->email->set_newline("\r\n");
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($to_email);
        if ($this->email->send()) {
          echo "Done: $email<br/>";
          
        } else {
          echo "<div style='color:#ff0000;'>Fail: $email</div>";
        }
      
    echo "<h1>All Mail send</h1>";
  }

}
