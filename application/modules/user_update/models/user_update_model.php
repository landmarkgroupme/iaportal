<?php

Class User_update_model extends CI_Model {

  function User_update_model() {
    parent::__construct();
  }

  function user_db_check($emp_id, $email) {
    $result = $this->db->get_where('ci_users', array('hrms_id' => $emp_id, 'email' => $email));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* //New user check
  function new_user_check($email) {
    $result = $this->db->get_where('ci_users', array('email' => $email));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  } */
  //New user check
  function new_user_check($hrmsid) {
    $result = $this->db->get_where('ci_users', array('hrms_id' => $hrmsid));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //territory check
  function new_territory_check($territory) {
    $result = $this->db->get_where('ci_master_territory', array('name' => $territory));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Concept check
  function new_concept_check($concept) {
    $result = $this->db->get_where('ci_master_concept', array('db_concept_name' => $concept));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Designation check
  function new_designation_check($designation) {
    $result = $this->db->get_where('ci_master_designation', array('name' => $designation));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Department check
  function new_department_check($department) {
    $result = $this->db->get_where('ci_master_department', array('name' => $department));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//Location check
  function new_location_check($location) {
    $result = $this->db->get_where('ci_master_location', array('name' => $location));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Band check
  function new_band_check($band) {
    $result = $this->db->get_where('ci_master_band', array('name' => $band));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function delete_user_trace($id) {
    //Event reminder
    $sql = 'DELETE FROM `ci_event_reminder` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //Fav link
    $sql = 'DELETE FROM `ci_fav_links` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //Map interest group
    $sql = 'DELETE FROM `ci_map_intrest_users` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //Market Items
    $sql = 'DELETE FROM `ci_market_items` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //My Replies
    $sql = 'DELETE FROM `ci_my_replies` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //Offer pref
    $sql = 'DELETE FROM `ci_offers_users` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //User delete
    $sql = 'DELETE FROM `ci_users` WHERE `id` = ?';
    $result = $this->db->query($sql, array($id));

    //Following user
    $sql = 'DELETE FROM `ci_users_following_users` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //followers
    $sql = 'DELETE FROM `ci_users_following_users` WHERE `following_user_id` = ?';
    $result = $this->db->query($sql, array($id));

    //Status update
    $sql = 'DELETE FROM `ci_users_status_updates` WHERE `user_id` = ?';
    $result = $this->db->query($sql, array($id));

    return $this->db->affected_rows();
  }

  //insert functions
  function new_territory_insert($data) {
    $str = $this->db->insert('ci_master_territory', $data);
    return $this->db->insert_id();
  }

  function new_concept_insert($data) {
    $str = $this->db->insert('ci_master_concept', $data);
    return $this->db->insert_id();
  }

  function new_designation_insert($data) {
    $str = $this->db->insert('ci_master_designation', $data);
    return $this->db->insert_id();
  }

  function new_department_insert($data) {
    $str = $this->db->insert('ci_master_department', $data);
    return $this->db->insert_id();
  }

  function new_location_insert($data) {
    $str = $this->db->insert('ci_master_location', $data);
    return $this->db->insert_id();
  }

  function new_band_insert($data) {
    $str = $this->db->insert('ci_master_band', $data);
    return $this->db->insert_id();
  }

  function new_user_insert($data) {
    $str = $this->db->insert('ci_users', $data);
    return $this->db->insert_id();
  }

  function update_user($data, $id) {
    $this->db->where('id', $id);
    $this->db->update('ci_users', $data);
    return $this->db->affected_rows();
  }

  /*
    function notification_log($data) {
    $str = $this->db->insert('ci_notify_email_log', $data);
    return $this->db->insert_id();
    }
   */
  function get_whitelist_email($hrmsid) {
    $this->db->select('iw.hrmsid');
    $this->db->from('import_whitelist iw');
    $this->db->where('iw.hrmsid',$hrmsid);
    $query = $this->db->get();
    $ret = $query->row();
    if (!empty($ret)) {
      return $ret->hrmsid;
    } else {
      return $query->result();
    }
    
   
  }
  
  function get_blacklist_email($hrmsid) {
    $this->db->select('ib.hrmsid');
    $this->db->from('import_blacklist ib');
    $this->db->where('ib.hrmsid',$hrmsid);
    $query = $this->db->get();
    $ret = $query->row();
      
    if (!empty($ret)) {
      return $ret->hrmsid;
    } else {
     return $query->result();
    }
   
  }
  function deactivate_ex_emp($emp_ids) {
    $data = array(
      'status' => 'inactive'
    );
    $this->db->where_in('hrms_id', $emp_ids);
    $this->db->update('ci_users', $data);
    return $this->db->affected_rows();
  }
  /*
   *  Check Email id exist
   **/
  function check_email_exist($email) {
    $this->db->select('cu.id');
    $this->db->from('ci_users cu');
    $this->db->where('cu.email',$email);
    $query = $this->db->get();
    return $query->row();
  }
  /*
   *  Get HRMS Details by ID
   **/
  function getHrmsDetailsById($id) {
    $this->db->select('cu.email,cu.last_login_time, cu.welcome_mail');
    $this->db->from('ci_users cu');
    $this->db->where('cu.id',$id);
    $query = $this->db->get();
    return $query->row();
  } 
}

