<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Concepts - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
	<h2>Concepts</h2>
	 <ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Concepts</li>
    </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="concept-grid clearfix">

            <div class="concept-intro">
            	<h1>Follow Your Favourite Concepts</h1>
                Get the latest updates, best deals and offers and events.
            </div>

            <?php
            	$counter = 0;
            	foreach($concepts as $concept){

            	?>

           <div class="concept-box media-hide <?php echo $counter % 3 == 0 ? 'mid' : ''; ?>">
				<?php if($concept['icon'] == '') { ?>
				<a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>">
					<span class="cp-span-big cp-img-big" style="background: url('<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>') no-repeat scroll center center #CCC;">
						<span class="cp-span-text"><div><?php echo ucwords(strtolower($concept['name'])); ?></div></span>
					</span>
				</a><br/>
				<?php } else { ?>
            	<img class="cp-img-logo" src="<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['icon']; ?>" alt="Splash" />
			  		<a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>"><img class="cp-img-big" src="<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>" alt="Splash" /><br/></a>
				<?php } ?>
                <div class="x-members"><span><?php echo $concept['total_members']; ?></span> Members</div>
                <div class="cp-followers-bx clearfix">
                	<div class="cp-followers">
                    	<div class="imgMembers">
                    		<?php foreach ($concept['members'] as $member) {
                    			?>
                    			<a href="<?php echo $member['reply_username']; ?>" title="<?php echo $member['fullname']; ?>"><img src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $member['profile_pic']; ?>" alt="<?php echo $member['fullname']; ?>"></a>
                    			<?php
                    		}
                    		?>

                        </div>
                    </div>
                    <?php
					if(!in_array($myprofile['id'],$concept['managers']))
					{

                    	if($concept['member'])
                    	{
                    	?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Concept" object_id="<?php echo $concept['id']; ?>" name="btnFollow" value="Following" /></div>
                    	<?php
                    	} else {
                    	?>
                    		<div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow" object_type="Concept" object_id="<?php echo $concept['id']; ?>" name="btnFollow" value="Follow" /></div>
                    	<?php
                    	}
                    
					}
                	?>
                </div>
        	</div>
			          <!-- Shows in responsive view -->
          <div class="concept-box media-show">
             <?php if($concept['icon'] == '') { ?>
             <a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>">
                <span class="cp-span-big cp-img-big" style="background: url('<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>') no-repeat scroll center center #CCC;"></span>
             </a>
             <?php } else { ?>
             <a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>"><img class="cp-img-big" src="<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>" alt="Splash" /></a>
						 <?php } ?>
             <span class="cp-span-text">
                <div><?php echo ucwords(strtolower($concept['name'])); ?></div>
             </span>
             <div class="cp-followers-bx clearfix">
                <div class="x-members"><span><?php echo $concept['total_members']; ?></span>+</div>
                <?php
                   if(!in_array($myprofile['id'],$concept['managers']))
                   {
										 if($concept['member'])
										 {
									?>
                  <div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Concept" object_id="<?php echo $concept['id']; ?>" name="btnFollow" value="Following" /></div>
									<?php
                  } else {
										?>
                    <div class="cp-btn-follow"><input type="button" class="btn-sm btn-object-follow" object_type="Concept" object_id="<?php echo $concept['id']; ?>" name="btnFollow" value="Follow" /></div>
										<?php
                   }
									}
								?>
             </div>
          </div>
          <!-- Shows in responsive view  -->
        	<?php
        	$counter++;
        	} ?>














    </div>	<!-- concept-grid -->


	<!-- <div class="loader-more clearfix">&nbsp;</div> -->


	</div> <!-- container -->


    </div>

	<div class="right-contents media-widget">
    </div> <!-- right-contents -->
</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
$(window).load(function(){
        var tourSubmitFunc = function(e,v,m,f){
            if(v === -1){
                $.prompt.prevState();
                return false;
            }
            else if(v === 1){
                $.prompt.nextState();
                return false;
            }
            else if(v === 0){
                $.prompt.goToState(0);
                return false;
            }
			
},
tourStates = [
    {
        title: 'Follow Concept',
        html: 'Follow a concept to get later offers and take in part in competitions',
        buttons: { Next: 1 },
        focus: 0,
        position: { container: '.cp-btn-follow', x: -130, y: 40, width: 300, arrow: 'tc' },
        submit: tourSubmitFunc
    },
    {
        title: 'Follow Members',
        html: 'Your follow members within a group',
        buttons: { Prev: -1, First: 0, Done: 2 },
        focus: 0,
        position: { container: '.cp-followers', x: -100, y: 70, width: 300, arrow: 'tc' },
        submit: tourSubmitFunc
    }
];
if(<?php echo $user_prompt;?>)
{
    $.prompt(tourStates);
		var data1 = {'user_id' : myprofile.id, 'feature' : 'Concept'};
			var url = siteurl+"status_update/add_prompt";
				$.ajax({
					url: url,
					data: data1,
					async: false,
					dataType: "json",
					type: "POST",
					success: function(data){

					}
				});
}	
});
</script>


</body>
</html>