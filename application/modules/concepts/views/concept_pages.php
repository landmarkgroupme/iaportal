<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
	<meta name="robots" content="index, nofollow">
	<title>Widgets - Landmark Group</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">
<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
		<h2>
			<?php echo $concept['name']; ?> Widgets
		</h2>
		<ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="<?php echo site_url(); ?>concepts">Concepts</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a id="concept_url_reload" href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>"><?php echo $concept['name']; ?></a></li>
			<li><span>&gt;&gt;</span></li>
			<li><?php echo $widget_data['name']; ?></li>
		</ul>
	</div>
	<div class="section wrapper clearfix">
		<div class="left-contents">
			<div class="container">
			    <div id ="recent-widget" class="job_details clearfix widget_body">
				<?php if(isset($concept_manager['id']) && $concept_manager['id'] > 0) {?>
				<ul class="clearfix btn-items"><li><a href="#update_document" id="edit_widget" class="btn-sm fancybox add_file_save">Edit</a> &nbsp;</li><li><a href="#" id="delete_widget" class="btn-sm ">Delete</a> &nbsp;</li></ul>
				<?php } ?>
				 <h3><?php echo $widget_data['name']; ?></h3>
								<?php echo $widget_data['widget']; ?>
				</div>
			</div>
			 <div id="recent-posts" class="new-posts"></div>
		</div>
	<div class="right-contents media-widget">
    </div> <!-- right-contents -->
	</div>
	<?php
$attributes1 = array('class' => '', 'id' => 'frm_edit_widget');
?>
<div id="update_document" class="fb-container-box d-n">
<h3>Add Pages</h3>
<ul class="msg d-n"></ul>
	<!--<form action="<?//=base_url();?>files/submit_share_files"  id="frm_add_item" enctype="multipart/form-data" method="post"> -->
	<?php echo form_open_multipart('widgets/ajax_update_widget',$attributes1);?>
	<div class="form-fields">

	
	<dl class="files-box-holder">
		<dd>
			<div class="block">
				<label for="txt_title">Title<span class="required">*</span></label>
				<span id="error-txt-title-update" class="error-msg"></span><br>
				<div class="textC">
				<input type="text" id="txt_title_widget_update" name="txt_title" placeholder="Title" />
				</div>
			</div>
		</dd>
		<dl class="cols-two">
		<dd><label for="cmb_cat">Category<span class="required">*</span></label> <span class="error-msg" id="error-cmb-cat-widget"> </span><br />
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="cmb_cat" id="cmb_cat_widget">
					<option value="0">Select a category</option>
					<?php	foreach($widget_select as $categories_select):?>
					 <optgroup label="<?php echo $categories_select['name'];?>">
					 <?php	foreach($categories_select['option'] as $sub):?>
					<option value="<?php echo $sub['id']; ?>"><?php echo $sub['name'];?></option>
					<?php endforeach?>
					 </optgroup>
					<?php endforeach?>
					</select>
				</label>
			</div>
		</div>
		</dd>
	</dl>
		<dd>
			<div class="block">
				<label for="txtar_description">Description </label>
				<span id="error-txtar-descr-update" class="error-msg"></span><br>
				<div class="textC"><textarea cols="55" rows="10" id="txtar_descr_update" style="width:600px;" name="txtar_descr" ></textarea></div>
			</div>
		</dd>
		
		
	</dl>
	<dl class="cols-two">
	<dd><label for="status">status<span class="required">*</span></label><span class="error-msg" id="error-cmb_cat"></span><br>
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="status" id="status">
					<option value="0">Hide</option>
					<option value="1">Show</option>
					</select>
				</label>
			</div>
		</div>
		</dd>
		
		</dl>
		<dl class="files-box-holder">
		<dd>
			<input type="hidden" value="<?php echo $concept['id']; ?>" name="object_id">
			<input type="hidden" name="widget_id" id="widget_id" value="<?php echo $widget_data['id']; ?>">
			<div class="submit-div">
				<span id="progress-status"></span>
				<input type="button" id="update_widget" class="btn-sm edit fancybox" value="Update" />
				<input type="button" id="cancel_widget_update" class="btn-sm" value="Cancel" />
			</div>
		</dd>
		</dl>
</div>

</form>
</div>
	<!-- section -->
	
<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>
<?php $this->load->view('templates/js/widget_updates'); ?>
<?php $this->load->view('partials/interaction_js'); ?> 
<script type="text/javascript" src="<?php echo site_url(); ?>js/nicEdit.js"></script>
<script src="//cdn.ckeditor.com/4.4.4/full/ckeditor.js"></script> 
   
   <script type="text/javascript">
   	   CKEDITOR.disableAutoInline = true;
       CKEDITOR.on( 'dialogDefinition', function( ev )
       {
          // Take the dialog name and its definition from the event data.
          var dialogName = ev.data.name;
          var dialogDefinition = ev.data.definition;
      
          // Check if the definition is from the dialog we're
          // interested in (the 'image' dialog). This dialog name found using DevTools plugin
          if ( dialogName == 'image' )
          {
             // Remove the 'Link' and 'Advanced' tabs from the 'Image' dialog.
             
             dialogDefinition.removeContents( 'advanced' );
             dialogDefinition.removeContents( 'link' );
      
             // Get a reference to the 'Image Info' tab.
             var infoTab = dialogDefinition.getContents( 'info' );
      
             // Remove unnecessary widgets/elements from the 'Image Info' tab.        
             infoTab.remove( 'txtHSpace');
             infoTab.remove( 'txtVSpace');
          }
       });

      
	   
      bkLib.onDomLoaded(function() {
        //nicEditors({iconsPath : }).allTextAreas()
		  
		 //   new nicEditor({
   //          iconsPath : '<?= base_url(); ?>images/nicEditorIcons.gif',
   //          uploadURI : '<?= base_url() ?>lib/nicUpload.php',
			// fullPanel : 'true'
   //        }).panelInstance('txtar_descr_update');
		
			
         });
    </script>
<script type="text/javascript">
var widget_id = <?php echo $widget_data['id']; ?>;
 $(document).ready(function(){
 $('.widget_body img').each(function() {
	var ori_src = $(this).attr("src");
	var res = ori_src.replace("../images/news_offers_announcement", "/images/news_offers_announcement");
	if(ori_src != res)
	{
	$(this).attr("src",res);
	}
 });
 
 $('.add_file_save').fancybox({
		maxHeight	: 700,
		minWidth	: 400,
		afterClose  : function() {
		   $("#frm_add_widget input[type=text], #frm_add_widget textarea , #frm_add_widget select ").val("");
		   $("#frm_add_widget .nicEdit-main").html("");
		   $("#frm_add_widget .error-msg").html("");
		   return window.location.reload();
		   },
		    afterLoad:function(){
		    	

		      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ||
         (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.platform))) {

          // // some code..

          //  setTimeout(function(){
          //   if (CKEDITOR.instances.txtar_descr) {
          //           CKEDITOR.instances.txtar_descr.destroy();
          //           delete CKEDITOR.instances['txtar_descr'];
          //       }
          //       CKEDITOR.replace('txtar_descr', {
          //           toolbar: [
          //             { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'RemoveFormat'  ] }
          //             //{ name: 'links', items: [ 'Link', 'Unlink' ] },
          //            // { name: 'insert', items: [ 'Image' ] }
                      
          //           ],
          //         "filebrowserImageUploadUrl": '<?= base_url() ?>lib/ckeditorUpload.php',
          //         height: '100px',

          //       });
          //   }, 4);

      } else {

      setTimeout(function(){
        if (CKEDITOR.instances.txtar_descr) {
                CKEDITOR.instances.txtar_descr.destroy();
                delete CKEDITOR.instances['txtar_descr'];
            }
            CKEDITOR.replace('txtar_descr', {
                toolbar: [
                  { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat'  ] },
                  { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                  { name: 'links', items: [ 'Link', 'Unlink' ] },
                  { name: 'insert', items: [ 'Image' ] },
                  { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                  { name: 'colors', items: [ 'TextColor'] }
                ],
              "filebrowserImageUploadUrl": '<?= base_url() ?>lib/ckeditorUpload.php',
              height: '100px',

            });
        }, 4);
  
      }
    }
	});
	
	$('#edit_widget').live( "click", function() {
		
		/*$("div#hp-action").html('');
		$("div#hp-action").hide();
		$("div#recent-posts").html('');
		$("div#recent-widget").html('');
		$("div#recent-widget").show();*/
		var service_url = siteurl+"widgets/getWidget";
		$.ajax({
			url: service_url,
			async: false,
			 data: {
						"id":widget_id
					},
			dataType: "json",
			type: "POST",
			success: function(response){
			/*if(concept_manager == 1)
			{
				$('div#recent-widget').append('<ul class="clearfix btn-items"><li><a href="#update_document" class="btn-sm fancybox add_file_save">Edit</a> &nbsp;</li><li><a href="#" id="delete_widget" class="btn-sm ">Delete</a> &nbsp;</li></ul>');
			}
			$('div#recent-widget').append('<h3>'+response.name+'</h3>');
			$('div#recent-widget').append(response.widget);*/
			var ori_widget = response.widget;
			var res_widget = ori_widget.replace("../images/news_offers_announcement", "/images/news_offers_announcement");
			$('#widget_id').val(widget_id);
			//$("#frm_edit_widget .nicEdit-main").html(res_widget);
			$('#txtar_descr_update').val(res_widget);
			if (CKEDITOR.instances['txtar_descr_update']) {
				setTimeout(function(){CKEDITOR.instances.txtar_descr_update.setData(res_widget); } ,10);
			}
			$("#txt_title_widget_update").val(response.name);
			$("#status").val(response.show);
			$('#cmb_cat_widget').val(response.category);
			}
		});
		
		//$.fancybox.close();
		return false;
});
	
 $('#update_widget').click(function(e) {
		e.preventDefault();
		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;
		var base_url = $("#base_url").val();
		var title = $.trim($("#txt_title_widget_update").val());
		//var desc = $.trim($("#frm_edit_widget .nicEdit-main").html());
		if (CKEDITOR.instances['txtar_descr_update']) {
			var desc = CKEDITOR.instances.txtar_descr_update.getData();
		}
		else {
			var desc = $("#txtar_descr_update").val();
		}

		if(desc )

		var category = $("#cmb_cat_widget").val();
		$("#txtar_descr_update").val(desc);
		if(title == '' || title == 'Title'){
			$("#error-txt-title-update").html("Required");
			$("#txt_title_widget_update").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else if(desc == '' || desc == '<br>' || desc == '<BR>'){
			$("#error-txtar-descr-update").html("Required");
			$("#txtar_descr_update").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else if(category == '' || category == '0' || category == 0 ){
			$("#error-cmb-cat-widget").html("Required");
			$("#cmb_cat_widget").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else{
			//var formData = new FormData($('#frm_edit_widget')[0]);

				var options = {
					beforeSend: function() {
								$.fancybox.showLoading();
								$('#update_widget').addClass('inactive');
								$('#update_widget').attr('disabled','disabled');
								$('#cancel_widget_update').addClass('inactive');
								$('#cancel_widget_update').attr('disabled','disabled');
								},
					complete: function() {
								$.fancybox.hideLoading();
								$('#update_widget').removeClass('inactive');
								$('#update_widget').removeAttr('disabled');
								$('#cancel_widget_update').removeClass('inactive');
								$('#cancel_widget_update').removeAttr('disabled');
								},
					type	: 'POST',
					cache	: false,
					url		: $('#frm_edit_widget').attr('action'),
					dataType: "json",
					success	: function(response) {
					if(response.status === 'done')
						{
							$("div#recent-posts").html('');
							$("div#recent-widget").html('');

								$('div#recent-widget').append('<ul class="clearfix btn-items"><li><a href="#update_document" class="btn-sm fancybox add_file_save">Edit</a> &nbsp;</li><li><a href="#" id="delete_widget" class="btn-sm">Delete</a> &nbsp;</li></ul>');
							
							$('div#recent-widget').append('<h3>'+response.name+'</h3>');
							$('div#recent-widget').append(response.widget);
							$('#widget_id').val(response.id);
							$('#widget_id_'+response.id).html(response.name);
							$("#status").val(response.show);
							$.fancybox.close();
							setSuccess('.msg', 'Your widget has been updated successfully!');
							
						}
						else
						{
								setError('#frm_edit_widget .msg', 'Please retry, enter data.');
						}
					},
					cache: false,
					contentType: false,
					processData: false
			};
			$('#frm_edit_widget').ajaxSubmit(options);
		}
		return false;
	});
	
	$('#cancel_widget_update').on( "click", function() {
		$('.msg').hide();
		$.fancybox.close();

	});
	
	$('#delete_widget').live( "click", function() {
			var r = confirm('Are you sure you want to delete?');
			if (r == true)
			{
				var service_url = siteurl+"widgets/deleteWidget";
				$.ajax({
					url: service_url,
					async: false,
					data: {
						'id':$('#widget_id').val()
					},
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.status === 'yes')
						{
							
							window.location=$('#concept_url_reload').attr('href');
						}
						else
						{
							return false;
						}
						
					}
				});
			}
			else
			{
				return false;
			}
	});
	
	 loadWidgetUpdates();
	 $('#recent-posts .rp-contents .rp-social').css({'margin': '20px 0 0 50px'});

  });
  
    function loadWidgetUpdates() {
    $('#recent-posts div.loader-more').show();
    var service_url = siteurl + "status_update/widget_load_ajax_data";
    var request_data = {};
    request_data = {'item_id': widget_id};
    $.ajax({
      url: service_url,
      data: request_data,
      async: false,
      dataType: "json",
      type: "POST",
      cache: false,
      success: function (response) {
        if (response.feed.length == 0 && start == 0) {
          $('#recent-posts div.loader-more').before('<dl class="block-2 clearfix no-feed-found"><dt><dd><h2>No feed found!</h2></dd></dt></dl>');
        }
        console.log(response);
        _.templateSettings.variable = "rc";

        // Grab the HTML out of our template tag and pre-compile it.
        var template = _.template(
          $("script.template").html()
        );
        //$('#recent-posts ul.filter li').removeClass('active');
        //$(parent).addClass('active');
        //$('#recent-posts ul.filter').append(template(response));
        $('#recent-posts').append(template(response));

      },
      beforeSend: function () {
        loadProgress = true;
      },
      complete: function (response) {
        loadProgress = false;
        $('#recent-posts div.loader-more').hide();
      }


    });

  }
    $(function() {
		$("img:below-the-fold").lazyload({
			event : "sporty"
		});
	});
	
	$(window).bind("load", function() {
		var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
	}); 
</script>
</body>
</html>
