<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
	<meta name="robots" content="index, nofollow">
	<title>Widget order - Landmark Group</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">
<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
		<h2>
			Update widget order
		</h2>
		<ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="<?php echo site_url(); ?>concepts">Concepts</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>"><?php echo $concept['name']; ?></a></li>
			<li><span>&gt;&gt;</span></li>
			<li>Widget order</li>
		</ul>
	</div>
	<div class="section wrapper clearfix">
		<div class="left-contents">
			<div class="container">
				<div class="widget clearfix">
					<div class="box shadow-2">
						<?php if($concept_manager)
						{ ?>
						<ul class = "widget-page dragable">
						<?php 
						$count= 0;
						foreach($widget as $value): ?>
						<li><a href="#" rel="<?php echo $value->id; ?>" id = "widget_id_<?php echo $value->id; ?>"><?php echo ucwords(strtolower(stripcslashes($value->name))); ?></a></li>
						<?php if ($value !== end($widget)): ?>

						<?php endif; 
						endforeach; } ?>
						</ul>
					</div>
				</div>
			</div>	
		</div>
		 <div class="right-contents media-widget">
         </div> <!-- right-contents -->
	</div>
	<!-- section -->
	
<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?> 
<script type="text/javascript">
 $(document).ready(function(){
  if ($('.dragable').exists()) {
	    $( ".dragable" ).sortable({
			revert: true,
			axis: "y",
			stop: function(event,ui) {
			var myArray = [];
			$('.dragable li').each(function( index ) {
				console.log( index + ": " + $( this ).children('a').attr('rel') );
				myArray[ index] = $( this ).children('a').attr('rel');
			});
			 $.ajax({
                type: 'POST',
                url: '<?php echo site_url('manage/widget_sort');?>',
                data: {widgets : myArray},
                async: false,
                dataType: 'json',
                 success: function(result){
                 }
              });
			}
		});
  }
  });
    $(function() {
		$("img:below-the-fold").lazyload({
			event : "sporty"
		});
	});
	
	$(window).bind("load", function() {
		var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
	}); 
</script>
</body>
</html>
