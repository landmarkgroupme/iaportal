<!DOCTYPE html>
<html>
  <head>
    <meta name="robots" content="index, nofollow">
    <title><?php echo $concept['name']; ?> - Landmark Group</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <?php $this->load->view('include_files/common_includes_new'); ?>
	<script>
	var concept_manager = <?php echo isset($concept_manager['id'])? 1 : 0; ?>;
	var concept = <?php echo isset($concept['id'])? $concept['id'] : 0; ?>;
	var filter_user = <?php echo isset($statuses_filter) ? $statuses_filter : 0; ?>;
	var concept_thumb = "<?php echo isset($concept['thumbnail']) ? $concept['thumbnail'] : 0; ?>";
	var concept_slug = "<?php echo isset($concept['slug']) ? $concept['slug'] : 0; ?>";
	var concept_name = "<?php echo isset($concept['name']) ? $concept['name'] : 0; ?>";
	var concept_page = true;
	</script>
  </head>
<body class="profile concept_page">
  <?php $this->load->view('templates/js/people'); ?>
  <?php $this->load->view('templates/js/people_list'); ?>
  <?php $this->load->view('global_header');
  $this->load->helper('html');
  ?>

  <div class="section wrapper clearfix">
  	<h2>Concepts</h2>
	<ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="<?php echo site_url(); ?>concepts">Concepts</a></li>
			<li><span>&gt;&gt;</span></li>
			<li><a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>"><?php echo $concept['name']; ?></a></li>
		</ul>
  </div>

<div class="section wrapper clearfix">
  	<div class="left-contents">

          <div class="profile-block clearfix">
          	<div class="p-lft"><a href="<?php echo site_url(); ?>concepts/<?php echo $concept['slug']; ?>"><img class="avatar-big shadow-3" src="<?php echo site_url(); ?>images/concepts/thumbs/<?php echo $concept['thumbnail']; ?>" alt="Splash Fashion" /></a></div>
              <div class="p-rgt profile-intro">
              	<h1><?php echo $concept['name']; ?></h1>
                  <div class="x-members"><span><?php echo $concept['total_members']; ?></span> Members</div>
                  <ul>
				  	<?php if(isset($concept['phone']) && !empty($concept['phone'])):?>
				   <li class="iconTelephone">&nbsp;<a href=""><?php echo $concept['phone']; ?></a></li>
				   <?php endif;
				   if(isset($concept['email']) && !empty($concept['email'])):
				   ?>
				   <li><a href="mailto:<?php echo $concept['email']; ?>"><?php echo $concept['email']; ?></a></li>
				   <?php endif; ?>
                  	<li>

                    	<?php
						if(!$concept_manager)
						{
						if($concept['member']):?>
                        <input type="button" class="btn-sm btn-object-follow btn-grn" object_type="Concept" object_id="<?php echo $concept['id']; ?>" name="btnFollow" value="Following" />
            					<?php else: ?>
            					  <input type="button" class="btn-sm btn-object-follow" object_type="Concept" object_id="<?php echo $concept['id']; ?>" name="btnFollow" value="Follow" />
            					<?php endif;
						}?>

  					         </li>
  				        </ul>
						<?php if(strlen($concept['description']) > 50)
						{ ?>
						<div class="profile-block-description">
						<?php echo $concept['description']; ?>
						</div>
						<a href="#" class="more-less">More</a>
						<?php } else
						{
						echo $concept['description'];
						}?>
  			     </div>
          </div>

          <?php
          if($concept_manager)
          {
            $concept_admin = true;
            $photos_allow = true;
          } else {
            $concept_admin = false;
          }

          if($concept['member'])
          {
            $this->load->view('partials/post_status', array('target_id' => $concept['id'], 'target_type' => 'Concept', 'concept_admin' => $concept_admin));
            $this->load->view('partials/con_content_updates');
          } ?>


     </div> <!-- left-contents -->

	   <div class="right-contents media-widget">

        <?php
       
	//   Remove the Concept Winners
	//   if($concept['id'] == 9)
       // {        	//home centre
        	//$this->load->view('widgets/competition_winner.php');
      //  }
	  //if($concept['slug']== 'corporate' && $concept['id'] == 5){
	  // $this->load->view('widgets/compitition_winner_widget');
	  //}
	  // Ramdaan Cookbook widget
	if($concept['slug']== 'corporate' && $concept['id'] == 5){
	   $this->load->view('widgets/ramadan_cookbook_widget');
	  }
	  
        if($concept_manager)
        {
           $this->load->view('concept_edit_tools');
		   $this->load->view('widgets/concept_page_widget');
        }
		else
		{
			$this->load->view('widgets/concept_page_widget_user');
		}
          $this->load->view('widgets/concept_members');
          //$this->load->view('links_widget');
          $this->load->view('announcement_widget');
		  $this->load->view('news_widget');
          $this->load->view('poll_widget', array('created_by' => $concept['id'], 'created_by_type' => 'Concept'));

          //$this->load->view('widgets/message', array('title' => 'Restricted Area', 'message' => 'Only Members are allowed'));



        ?>

    </div> <!-- right-contents -->
</div> <!-- section -->
<?php $this->load->view('global_footer'); ?>
<?php $this->load->view('partials/js_footer'); ?>
<?php $this->load->view('templates/js/content_updates'); ?>
<?php $this->load->view('templates/js/new_status'); ?>
<?php $this->load->view('templates/js/new_status_poll'); ?>
<?php $this->load->view('templates/js/new_upload_image'); ?>
<?php $this->load->view('templates/js/new_file'); ?>
<?php $this->load->view('partials/interaction_js'); ?>
<?php
		if($concept_manager)
		{
		  $this->load->view('popups/concept_create_content');
			$this->load->view('popups/concept_info');

	  ?>
    <script type="text/javascript" src="<?php echo site_url(); ?>js/jquery.limit.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>js/custom_js/manage/manage_concept_content.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>js/nicEdit.js"></script>

    <script type="text/javascript" language="javascript" src="<?php echo site_url(); ?>js/swfobject.js"></script>
  	<script src="<?php echo site_url(); ?>media/js/jquery.Jcrop.min.js"></script>
  	<script type="text/javascript" src="<?php echo site_url(); ?>js/custom_js/files/share_files.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo site_url(); ?>js/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
    <script src="//cdn.ckeditor.com/4.4.4/full/ckeditor.js"></script> 
    <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>


    <!-- Includes for uploadify ends -->
      <script type="text/javascript">
     

      bkLib.onDomLoaded(function() {
        //nicEditors({iconsPath : }).allTextAreas()
        new nicEditor({
            iconsPath : '<?= base_url(); ?>images/nicEditorIcons.gif',
            buttonList : ['image','upload','link','unlink'],
            uploadURI : '<?= base_url() ?>lib/nicUpload.php',
            maxHeight : 100
          }).panelInstance('txt_body');
		 //  new nicEditor({
   //          iconsPath : '<?= base_url(); ?>images/nicEditorIcons.gif',
   //          uploadURI : '<?= base_url() ?>lib/nicUpload.php',
			// fullPanel : 'true',
			// maxHeight : 100
   //        }).panelInstance('txtar_descr');

		   new nicEditor({
            iconsPath : '<?= base_url(); ?>images/nicEditorIcons.gif',
            uploadURI : '<?= base_url() ?>lib/nicUpload.php',
			fullPanel : 'true',
            maxHeight : 100
          }).panelInstance('txtar_descr_update');


         });
    </script>
    <!-- Includes for uploadify -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>js/uploadify/uploadify.css" />

   <script type="text/javascript" language="javascript">
      var ajaxUploadifyProcessing = false;
      var uploadifyTimer = null;
      function guessUploadify(){
        if(!ajaxUploadifyProcessing) {
          //alert('done all');
          clearTimeout(uploadifyTimer);
         // window.location = 'photos';
        } else {
          uploadifyTimer = setTimeout(guessUploadify, 1000);
        }
      }
    $(document).ready(function(){
      
	// Setting the Current timestamp as the random foldername and id for the album
	var tmpFolderName = <?php echo date('dmHis',time()); ?>
	//$('body').append('<input type="hidden" name="tmpfoldername" id="tmpfoldername" value="1404124568796">');
	$('#frm_post').prepend('<input type="hidden" id="tmpfoldername" name="tmpfoldername" value="'+tmpFolderName+'">');
	$.post('<?php echo site_url('manage/create_temp_folder');?>',{tmpfolder:tmpFolderName},function(data) {
	  folderExist = true;
	});

        $("#upload").uploadify({
	       'auto':true,
          'uploader': '<?php echo site_url(); ?>js/uploadify/uploadify.swf',
          'script': '<?php echo site_url(); ?>js/uploadify/uploadify.php',
          'cancelImg': '<?php echo site_url(); ?>js/uploadify/cancel.png',
          'folder': '/phpalbum/albums/'+tmpFolderName,
          'fileDesc': 'Image Files',
          'fileExt': '*.jpg;*.jpeg;*.gif;*.png',
          'multi': true,
	  'sizeLimit': '1024000',
          'removeCompleted' : false,
          'buttonText': 'Select photos',
          buttonImg: '<?php echo base_url(); ?>images/select-photo.png',
          onError : function (a, b, c, d) {
            if (d.status == 404)
              alert('Could not find upload script.');
            else if (d.type === "HTTP")
              alert('error '+d.type+": "+d.status);
            else if (d.type ==="File Size")
              alert(c.name+' '+d.type+' Limit: '+Math.round(d.info/1000)+'KB');
            else
              alert('error '+d.type+": "+d.text);
          },
          onInit: function (instance) {
          },
          onComplete   : function (event, queueID, fileObj, response, data) {
            var entry_type = $("input[name='entry_category']:checked").val();
	    if (entry_type == 'photos') {
	        tmp_name = $('#tmpfoldername').val();
	      
          //Creating the folder with tmp name
          var folderExist = false;
          if (!folderExist) {
          setTimeout(function() {
          $.post('<?php echo site_url('manage/create_temp_folder');?>',{tmpfolder:tmp_name},function(data) {
          folderExist = true;
          });
		}, 5000);
	      }
	      var album_folder =  tmp_name;
	      var album_id = tmp_name;
	    }else{
	      var album_folder =  $('#album_folder').val();
	      var album_id = $('#album_id').val();
	    }
            ajaxUploadifyProcessing = true;
            $.post('<?php echo site_url('manage/uploadify');?>',{filearray: response,album_folder: album_folder,album_id : album_id},function(info){
                //$('#uploaded-photos-title').show();
                //$("#target").append(info);  //Add response returned by controller
                //alert(info)
                ajaxUploadifyProcessing = false;
            });
          },
          onAllComplete: function(){
            album_id = $('#album_id').val();
            publish_status = $('#alb_publish_status').val();
            txt_title = $('#txt_title').val();
            news_create_flag =  $('#news_create_flag').val();
            var btn_text = $.trim($("#submit_content").val()); //general
	    if (entry_type != 'photos') {
	      if(publish_status == 0 ){
		$.ajax({
		  type: 'POST',
		  url: '<?php echo site_url('manage/publish_album');?>',
		  data: {album_id : album_id},
		  async: false,
		  dataType: 'json',
		  success: function(result){
		  alert('ajax call')
		  //window.location ='photos';
		    //location.reload();
		  }
		});
	      }	      
	    }
          if(news_create_flag == 'yes'){
            $('#category_news').attr('checked','checked');
            $('.custom-form-element').hide();
            $('#title-box').show();
            $('#excerpt-box').show();
            $('#body-box').show();
            $('#album-box').show();
            $('#email-notify-box').show();
            $('#save_content').show();
            $('.help-info').hide();
            $('#help-text').show();
            $('#news-help').show();
            $('#selected-content-type').html('News');
            $('#txt_album').append('<option value = "'+album_id+'" selected="selected">'+txt_title+'</option>');
            $('#news-ablum-created-status').show();

            $("#submit_content").css("background","url(<?php echo base_url()?>images/moderation/btn-bg-hover.gif) repeat-x");
            $("#submit_content").css("border",'1px solid #000000');
            $("#submit_content").css("width","auto");
            $(".submit_entry").removeAttr("disabled");
            $('#submit_content').val($('#submit_content').attr('title'));

            $("#save_content").css("background","url(<?php echo base_url()?>images/save_draft.jpg) repeat-x");
            $("#save_content").css("border",'1px solid #939393');
            $("#submit_content").css("width","auto");
            $('#save_content').val('Save as draft');
            return false;
          }else{
            guessUploadify();
          }
         }

        });


FiletoUploader=document.getElementById("uploadUploader");

    $('#cancel').on( "click", function() {
    $('.msg').hide();
    $.fancybox.close();});

   $('#cancel_file').on( "click", function() {
    $('.msg').hide();
    $.fancybox.close();
  });
	$("#albums-list").change(function(){
		var base_location = '';
		var album_folder =  $("#albums-list option:selected").attr('id');
		var upload_folder = base_location + album_folder;
		$('#upload').uploadifySettings('folder',upload_folder);
	});
	$('#clear_queue').click(function(){
		$('#upload').uploadifyClearQueue();
		return false;
	});

    });

    </script>
  	<?php
  	}
  	?>


<script type="text/javascript">
	var limit = 10;
	var start = 0;
	var concept_start = 0;
	var concept_end = false;
	var concept_limit = 10;
	var nearToBottom = 250;
	var feedtype = 'concept';
	var feedtype_id = <?php echo $concept['id']; ?>;

$(document).ready(function(){

var progressbox     = $('#progressbox');
var progressbar     = $('#fileprogressbar');
var statustxt       = $('#statustxt');
var completed       = '0%';
	
if(typeof poll_results !== 'undefined')
	{
		$.each(poll_results, function(index, item){
		_html	= '<li><label>'+item['title']+'</label><div id="progressBarWid'+index+'" class="progressBar"><div></div></div></li>';
		$('.widpoll').append(_html);
		progress(item['percentage'], $('#progressBarWid'+index));
		});
	}

$("#update_concept").click(function(){

	$('#upload_concept_info .msg').removeClass('success');
	$('#upload_concept_info .msg').removeClass('error');
	$('#upload_concept_info .msg').html('');
	var txt_title = $.trim($("#txt_title_concept").val());//General
    var txt_body = $.trim($(".nicEdit-main").html());//General
	var txt_email = $.trim($("#txt_email_concept").val());//General
	var txt_phone = $.trim($("#txt_phone_concept").val());//General
	$("#txt_body").val(txt_body);
	var txt_status = $.trim($("#txt_status").val());//General
	var txt_open = $.trim($("#txt_open").val());//


	if(!txt_title){
      $("#error-txt_title_concept").html("required");
      $("#txt_title_concept").focus();
      return false;
    }else{
      $("#error-txt_title_concept").html("");
    }


	if(txt_email != "")
	{
	if(validateEmail(txt_email))
			{
				$("#error-txt_email_concept").html("");
			}
			else
			{
				$("#error-txt_email_concept").html("Please Enter valid email.");
				$("#txt_email_concept").focus()
				return false;
			}
	}
	else
	{
		$("#error-txt_email_concept").html("");
	}
	if(txt_phone != "")
	{
	if(IsPhone(txt_phone))
			{
				$("#error-txt_phone_concept").html("");
			}
			else
			{
				$("#error-txt_phone_concept").html("Please Enter valid Phone Number.");
				$("#txt_phone_concept").focus()
				return false;
			}
	}
	else
	{
		$("#error-txt_phone_concept").html("");
	}

	update_concept_info();
		return false;
});
	$('.loader-more').show();

  //loading this for data feed
	loadUpdates('', $('#recent-posts ul.filter li:first'));

  //loading this for members popup
	loadConceptPeople({'noclear': 0, 'concept': <?php echo $concept['id']; ?>,'concept_start': concept_start, 'concept_limit':concept_limit });


	$('.snap-content').scroll(function() {
		if ($('.snap-content').scrollTop() + $('.snap-content').height() >=
		    $('.left-contents').height() ) {
			if(loadProgress == false){
			var selected = $('#recent-posts ul.filter li[class=active] a');
			start += 10;
		 	loadUpdates($(selected).attr('type'), $('#recent-posts ul.filter li[class=active]'));
			}
		}
	});

	$('a.load_members').fancybox({
		maxHeight	: 700,
		width		: 943,
		autoSize	: false,
		afterLoad : function() {
			$('.fancybox-inner').attr('id','newfancy');
			$('#newfancy').scroll(function(){
				if ($('#newfancy').scrollTop() + $('#newfancy').height() >=
				$('#people_popup').offset().top + $('#people_popup').height() ) {
					//ajax
					concept_start += 10;
					loadConceptPeople({'noclear': 1, 'concept': <?php echo $concept['id']; ?>, 'concept_start': concept_start, 'concept_limit':concept_limit });
				}
			});
		}
	});

	$('.editProfile').fancybox({
		maxHeight: 500,
		minWidth: 669,
    autoSize  : false,
		afterClose  : function() {
           //$("#frm_post")[0].reset();
			  $("#frm_post input[type=text], #frm_post textarea, #frm_post select").val("");
			  if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
			   $("#txt_date_start").val("Start date");
			   $("#txt_date_end").val("End date");
			   }
			  $("#frm_post select").val('0');
			  $("#frm_post .nicEdit-main").html('');
			  $('#frm_post .error-msg').html('');
		   }
	});
	
	$(".editnoabox").fancybox({
	beforeLoad : function() {
	
		var others_id = this.element[0].id;
		/*parent_div = $(object).closest('dl.block-2');
		noa_id = $(parent_div).attr('id');
	*/
		parent_div = $("#"+others_id).closest('dl.block-2');
		if(parent_div.length==0){
			parent_div = $("#"+others_id).closest('.block-2');
		}
		noa_id = $(parent_div).attr('id');
		category_type = $(parent_div).attr('object_type');
		if(category_type == 'news'){
		$('#category_news').prop( "checked", true );
		$('#category_news').trigger( "click");
		}
		else if(category_type == 'offers'){
		$('#category_offers').prop( "checked", true );
		$('#category_offers').trigger( "click");
		}
		else if(category_type == 'announcement'){
		$('#category_announcements').prop( "checked", true );
		$('#category_announcements').trigger( "click");
		}
		
		
		console.log(noa_id);
		 $('#category_photos').attr('disabled', 'true');
		
		post_type = 'Status';
		object_id = $(parent_div).attr('object_id');
		object_type = $(parent_div).attr('object_type');
		//console.log(post_id);
		//console.log(post_type);
		//console.log(object_id);
		//console.log(object_type);
		var service_url = siteurl+"manage/get_mt_entry";
		var str = '';
		$.ajax({
			url: service_url,
			data: {"entry_id":noa_id},
			async: false,
			dataType: "json",
			type: "POST",
			success: function(data){
				console.log(data);
				$('#txt_title').val(data.entry_title);
				$('#txt_excerpt').val(data.entry_text_more);
				$('#frm_post .nicEdit-main').html(data.entry_text);
				var offers_start = data.offers_start_date;
				if(category_type == 'offers')
				{
				var arr = offers_start.split('-');
				$('#txt_date_start').val(arr[2]+'/'+arr[1]+'/'+arr[0]);
				var offers_end = data.offers_end_date;
				var arr1 = offers_end.split('-');
				$('#txt_date_end').val(arr1[2]+'/'+arr1[1]+'/'+arr1[0]);
				}
				if(category_type == 'news')
				{
					$('#txt_album').val(data.entry_excerpt);
				}
				$('#entry_id').val(data.entry_id);
			}
		});	
		
		//$("#others_comment_view_list").html(str);
		
	},
	afterClose  : function() {
	 $('#category_photos').attr('disabled', false);
	 $('#category_news').prop( "checked", true );
	 $('#category_news').trigger( "click");
	 $("#frm_post input[type=text], #frm_post textarea, #frm_post select").val("");
			  $("#frm_post select").val('0');
			  $("#frm_post .nicEdit-main").html('');
			  $('#frm_post .error-msg').html('');
	 
	}
	
	
	});
	

	
	
	$('#poll-view-all').fancybox({
		maxHeight	: 500,
		minWidth	: 400
	});

	$('#pages-view-all').fancybox({
		maxHeight	: 500,
		minWidth	: 400
	})

	$('.edit_concept').fancybox({
		maxHeight: 600,
		afterClose  : function() {

          $('#upload_concept_info.msg').removeClass('success');
          $('#upload_concept_info .msg').removeClass('error');
          $('#upload_concept_info .msg').html('');
		      $('#upload_concept_info .msg').fadeIn();
		      $('.error-msg').html('');
          window.location.href = window.location.href;

		   },
		    afterLoad: function(){ 
              $("#upload_concept_info").css({'overflow-x':'hidden'}); 

            } 

	});
$(".fancybox").fancybox({
		maxHeight	: 500,
		autoSize	: false,
		openEffect	: 'none',
		closeEffect	: 'none'

});
$('.edit_file_save').fancybox({
	maxHeight: 500,
	afterClose  : function() {
		$("#frm_add_item input[type=text], #frm_add_item textarea").val("");
		$("#frm_add_item select ").val('0');
		if($.browser.msie){
			$("#frm_add_item input[type='file']").replaceWith($("#frm_add_item input[type='file']").clone(true));
		} else {
			$("input[type='file']").val('');
		}
		$('.error-msg').html('');
		 progressbox.hide();
	},
	afterLoad: function(){ 
              $("#upload_file").css({'overflow-x':'hidden'}); 

            } 
});
	$('.add_file_save').fancybox({
		maxHeight	: 700,
		scrolling	:'auto',
		autoDimensions: true,
		afterClose  : function() {
		   $("#frm_add_widget input[type=text], #frm_add_widget textarea , #frm_add_widget select ").val("");
		   $("#frm_add_widget .nicEdit-main").html("");
		   $("#frm_add_widget .error-msg").html("");
    return window.location.reload();
		},
    afterLoad:function(){

 setTimeout(function(){
        if (CKEDITOR.instances.txtar_descr) {
                CKEDITOR.instances.txtar_descr.destroy();
                delete CKEDITOR.instances['txtar_descr'];
            }
            CKEDITOR.replace('txtar_descr', {
        toolbar: [
          { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat'  ] },
          { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
  { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
  { name: 'insert', items: [ 'Image' ] },
  { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
  { name: 'colors', items: [ 'TextColor'] }
        ]
      });
        }, 1000);
  

    }

	});

			$('#bt_add_item').click(function(e) {
			e.preventDefault();

		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;
		inactive_form();

		var base_url = $("#base_url").val();
		var cat = $.trim($("#cmb_cat").val());
		var file_share = $.trim($("#file_share").val());
		var txt_link = $.trim($("#txt_link").val());
		var title = $.trim($("#txt_title11").val());
		//var desc = $.trim($("#txtar_description").val());
		//var txtfilestag = $.trim($("#txt_tags").val());

		if(!cat){

		$("#error-cmb_cat").html("Required");
		$("#cmb_cat").focus();
		$('.msg').removeClass('success');

		active_form();
		return false;
		}
		else
		{
		$("#error-cmb_cat").html("");
		$('.msg').removeClass('success');
		}


		if(file_share){
		$("#error-files_upload").html("");
		$('.msg').removeClass('success');
		}
		else if((txt_link == "") || (txt_link.length < 2) &&(file_share == "")){

			$("#error-files_upload").html("Required");
			$("#error-files_upload").focus();
			$('.msg').removeClass('success');

			active_form();
			return false;
		}
		else if(txt_link){

			if(url_validation(txt_link)){
				$("#error-files_upload").html("");
			}else{
				$("#error-files_upload").html("Not a valid link");
				$('.msg').removeClass('success');

				active_form();
				return false;
			}
		}
		else{
		$("#error-files_upload").html("");
		$('.msg').removeClass('success');
		}

		if(!title){
			$("#error-txt_title").html("Required");
			$("#txt_title11").focus();
			$('.msg').removeClass('success');

			active_form();
			return false;
		}
		else{
		file_upload_submit();
		}


		return false;

	});


	$('#add_widget').click(function(e) {

	 	e.preventDefault();
		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;

		var base_url = $("#base_url").val();
		var title = $.trim($("#txt_title_widget").val());
		var desc = $.trim($("#frm_add_widget .nicEdit-main").html());
		var category = $("#cmb_cat_widget").val();
		$("#txtar_descr").val(desc);
		if(title == '' || title== 'Title' ){
			$("#error-txt-title").html("Required");
			$("#txt_title_widget").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else if(desc == '' || desc == '<br>' || desc == '<BR>'){
			$("#error-txtar-description").html("Required");
			$("#txtar_descr").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else if(category == '' || category == '0' || category == 0 ){
			$("#error-cmb-cat-widget").html("Required");
			$("#cmb_cat_widget").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else{
			//var formData = new FormData($('#frm_add_widget')[0]);

			var options = {
					beforeSend: function() {
								inactive_widget_form();
								},
					complete: function() {
								active_widget_form();
								start = 0;
								///loadFiles({'noclear': 0});
								},
					type	: 'POST',
					cache	: false,
					url		: $('#frm_add_widget').attr('action'),
					//data	: formData,
					dataType: "json",
					success	: function(data2) {
					if(data2.status === 'done')
						{
							$('.widget-page').append('<li><a href="" rel="'+data2.id+'" id = "widget_id_'+data2.id+'">'+data2.name+'</a></li>');
							$("#frm_add_widget input[type=text], textarea , select ").val("");
							$("#frm_add_widget .nicEdit-main ").html("");
							$.fancybox.close();
							setSuccess('.msg', 'Your widget has been uploaded successfully!');

						}
						else
						{
							setError('#frm_edit_widget .msg', 'Please retry, enter data.');
						}


					},
					cache: false,
					contentType: false,
					processData: false
			};
      $('#frm_add_widget').ajaxSubmit(options);
		}
		return false;

	});
	$('#update_widget').click(function(e) {
		e.preventDefault();
		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;
		var base_url = $("#base_url").val();
		var title = $.trim($("#txt_title_widget_update").val());
		var desc = $.trim($("#frm_edit_widget .nicEdit-main").html());
		$("#txtar_descr_update").val(desc);
		if(title == '' || title== 'Title'){
			$("#error-txt-title-update").html("Required");
			$("#txt_title_widget_update").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else if(desc == '' || desc == '<br>' || desc == '<BR>'){
			$("#error-txtar-descr-update").html("Required");
			$("#txtar_descr_update").focus();
			$('.msg').removeClass('success');
			return false;
		}
		else{
			//var formData = new FormData($('#frm_edit_widget')[0]);

				var options = {
					beforeSend: function() {
								$.fancybox.showLoading();
								$('#update_widget').addClass('inactive');
								$('#update_widget').attr('disabled','disabled');
								$('#cancel_widget_update').addClass('inactive');
								$('#cancel_widget_update').attr('disabled','disabled');
								},
					complete: function() {
								$.fancybox.hideLoading();
								$('#update_widget').removeClass('inactive');
								$('#update_widget').removeAttr('disabled');
								$('#cancel_widget_update').removeClass('inactive');
								$('#cancel_widget_update').removeAttr('disabled');
								},
					type	: 'POST',
					cache	: false,
					url		: $('#frm_edit_widget').attr('action'),
					dataType: "json",
					success	: function(response) {
					if(response.status === 'done')
						{
							$("div#recent-posts").html('');
							$("div#recent-widget").html('');
							if(concept_manager == 1)
							{
								$('div#recent-widget').append('<ul class="clearfix btn-items"><li><a href="#update_document" class="btn-sm fancybox add_file_save">Edit</a> &nbsp;</li><li><a href="#" id="delete_widget" class="btn-sm">Delete</a> &nbsp;</li></ul>');
							}
							$('div#recent-widget').append('<h3>'+response.name+'</h3>');
							$('div#recent-widget').append(response.widget);
							$('#widget_id').val(response.id);
							$('#widget_id_'+response.id).html(response.name);
							$("#status").val(response.show);
							$.fancybox.close();
							setSuccess('.msg', 'Your widget has been updated successfully!');

						}
						else
						{
								setError('#frm_edit_widget .msg', 'Please retry, enter data.');
						}
					},
					cache: false,
					contentType: false,
					processData: false
			};
			$('#frm_edit_widget').ajaxSubmit(options);
		}
		return false;
	});

   $('#cancel_concept').on( "click", function() {
		$('.msg').hide();
		$.fancybox.close();

	});
	$('#cancel_widget').on( "click", function() {
		$('.msg').hide();
		$.fancybox.close();

	});
	$('.video_view').fancybox({
		width	: '435',
	});
	$('#cancel_widget_update').on( "click", function() {
		$('.msg').hide();
		$.fancybox.close();

	});

	$('#delete_widget').live( "click", function() {
			var r = confirm('Are you sure you want to delete?');
			if (r == true)
			{
				var service_url = siteurl+"widgets/deleteWidget";
				$.ajax({
					url: service_url,
					async: false,
					data: {
						'id':$('#widget_id').val()
					},
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.status === 'yes')
						{
							location.reload();
						}
						else
						{
							return false;
						}

					}
				});
			}
			else
			{
				return false;
			}
	});




/*	$('.widget-page > li > a').live( "click", function() {
		var id = $(this).attr('rel');
		$("div#hp-action").html('');
		$("div#hp-action").hide();
		$("div#recent-posts").html('');
		$("div#recent-widget").html('');
		$("div#recent-widget").show();
		var service_url = siteurl+"widgets/getWidget";
		$.ajax({
			url: service_url,
			async: false,
			 data: {
						"id":$(this).attr('rel')
					},
			dataType: "json",
			type: "POST",
			success: function(response){
			if(concept_manager == 1)
			{
				$('div#recent-widget').append('<ul class="clearfix btn-items"><li><a href="#update_document" class="btn-sm fancybox add_file_save">Edit</a> &nbsp;</li><li><a href="#" id="delete_widget" class="btn-sm ">Delete</a> &nbsp;</li></ul>');
			}
			$('div#recent-widget').append('<h3>'+response.name+'</h3>');
			$('div#recent-widget').append(response.widget);
			$('#widget_id').val(id);
			$("#frm_edit_widget .nicEdit-main").html(response.widget);
			$("#txt_title_widget_update").val(response.name);
			$("#status").val(response.show);
			}
		});

		$.fancybox.close();
		return false;
}); */








	function file_upload_submit()
	{
		var options = {
					url     : $('#frm_add_item').attr('action'),
					type    : $("#frm_add_item").attr('method'),
					dataType: 'json',
					beforeSubmit: function() { 
					inactive_form();
					progressbox.show(); //show progressbar
					progressbar.width(completed); //initial value 0% of progressbar
					statustxt.html(completed); //set status text
					statustxt.css('color','#000'); //initial color of status text
					},
					uploadProgress: OnProgress,
					success:function( data ) {
					if(data.done === 'yes' && data.samename === 'no')
						{
							 active_form();
							$("input[type=text], textarea , select ").val("");
							if($.browser.msie){
								$("#frm_add_item input[type='file']").replaceWith($("#frm_add_item input[type='file']").clone(true));

								} else {
									$("input[type='file']").val('');
								}
							setSuccess('#upload_file .msg', 'Your file has been uploaded successfully!');
							updatefileDiv(data);
							setTimeout(function(){$.fancybox.close();}, 500);
						}
						else if(data.samename === 'yes')
						{
							active_form();
							setError('#upload_file .msg', 'A file with this name already exists. Please change name to continue.');

						}
						else
						{
							active_form();
							setError('#upload_file .msg', 'Please retry, enter data.');
						}


					},
			};
		$('#frm_add_item').ajaxSubmit(options);
		return false;
	}
function OnProgress(event, position, total, percentComplete)
{
	//Progress bar
	progressbar.width(percentComplete + '%') //update progressbar percent complete
	statustxt.html(percentComplete + '%'); //update status text
	if(percentComplete>50)
		{
			statustxt.css('color','#fff'); //change status text to white after 50%
		}
}
	function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
	}

	function IsPhone(sText){
		var filter = /^[0-9-+]+$/;
		if (filter.test(sText)) {
			return true;
		}else {
			return false;
		}
	}


	function update_concept_info()
	{
	var options2 = {
                url     : $('#frm_edit_concept').attr('action'),
                type    : $("#frm_edit_concept").attr('method'),
                dataType: 'json',
				  beforeSubmit: function() { inactive_form1();
				  },
                  success: function( data ) {
				  active_form1();
                    //var msg = data.msg;

                    if(data.status == 'yes'){
					$("#txt_title_concept").focus();
							$('#upload_concept_info .msg').addClass('success');
							$('#upload_concept_info .msg').append('<li/>').html('Concept has been updated!');
							$('#upload_concept_info .msg').fadeIn();
								location.reload();

                    }else{

                        $("#txt_title_concept").focus();
							$('#upload_concept_info .msg').removeClass('success');
							$('#upload_concept_info .msg').addClass('error');
							$('#upload_concept_info .msg').append('<li/>').html('Please retry, enter data');
							$('#upload_concept_info .msg').fadeIn();

                    }
                },
            };

            $('#frm_edit_concept').ajaxSubmit(options2);
                return false;
	}
	function active_form()
	{
	$.fancybox.hideLoading();
	$('#bt_add_item').removeClass('inactive');
	$('#bt_add_item').removeAttr('disabled');
	$('#cancel_file').removeClass('inactive');
	$('#cancel_file').removeAttr('disabled');
	}

	function active_widget_form()
	{
	$.fancybox.hideLoading();
	$('#add_widget').removeClass('inactive');
	$('#add_widget').removeAttr('disabled');
	$('#cancel_widget').removeClass('inactive');
	$('#cancel_widget').removeAttr('disabled');
	}
	function inactive_widget_form()
	{
	$.fancybox.showLoading();
	$('#add_widget').addClass('inactive');
	$('#add_widget').attr('disabled','disabled');
	$('#cancel_widget').addClass('inactive');
	$('#cancel_widget').attr('disabled','disabled');
	}


	function inactive_form()
	{
	$.fancybox.showLoading();
	$('#bt_add_item').addClass('inactive');
	$('#bt_add_item').attr('disabled','disabled');
	$('#cancel_file').addClass('inactive');
	$('#cancel_file').attr('disabled','disabled');
	}

	function active_form1()
	{
	$.fancybox.hideLoading();
	$('#update_concept').removeClass('inactive');
	$('#update_concept').removeAttr('disabled');

	}

	function inactive_form1()
	{
	$.fancybox.showLoading();
	$('#update_concept').addClass('inactive');
	$('#update_concept').attr('disabled','disabled');
	}

	function updatefileDiv(response)
	{
		_.templateSettings.variable = "response";

		var template = _.template(
            $( "#new_file" ).html()
        );
		$('#recent-posts ul.filter').after(template(response));
		$('#recent-posts dl.block-2:first').fadeIn('slow');
		//$('#recent-posts').append('added...');
	//	$('#txtPost').val('');
	}

	function loadConceptPeople(prop)
	{
		$('.loader-more').show();
		var searched = ($('#txtSearch').length > 0 && $('#txtSearch').val() != '') ? true : false;
		//console.log(searched);
		if(!prop.noclear)
		{
			$('.people-grid').html('');
			$('.people-list .row:not(.head)').remove();
		}
		var service_url = siteurl+"people/people_concept_ajax";
		var data = {'limit' : concept_limit, 'start' : concept_start, 'concept': concept};
		if(concept_end == false){
	     $.ajax({
	      url: service_url,
	      data: data,
	      async: false,
	      dataType: "json",
	      type: "GET",
	      success: function(data){

	      	if(_.size(data) > 0)
	      	{
	      		$('.no-result-block').addClass('d-n');
	      		_.templateSettings.variable = "rc";
		        // Grab the HTML out of our template tag and pre-compile it.
		        var template = _.template(
		            $( "#people_card" ).html()
		        );

		        $('div.people-grid').append(template(data));
		        var template = _.template(
		            $( "#people_list" ).html()
		        );
		        $('div.people-list').append(template(data));
		        if($('.l-list-view').hasClass('active')) {
		        	$('.people-list').css('display', 'table');
		      		$('.people-grid').css('display', 'none');
		        } else {
		        	$('.people-list').css('display', 'none');
		      		$('.people-grid').css('display', 'table');
		        }
		        $('.people-grid-view').removeClass('d-n');

	      	} else {
				concept_end = true;
			if(!prop.scroll && concept_start == 0)
	      		{
	      			$('.no-result-block').text('No Users found for specified criteria');
		      		$('.no-result-block').removeClass('d-n');
		      		$('.people-list .row:not(.head)').remove();
		      		$('.people-list').css('display', 'none');
		      		$('.people-grid').css('display', 'none');
		      		$('.people-grid-view').addClass('d-n');
	      		}
	      	}
			$('.loader-more').hide();
	      }
	    });
		}
	}
});
</script>

</body>
</html>

