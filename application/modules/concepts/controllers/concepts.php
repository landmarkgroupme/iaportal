<?php

class Concepts extends General {

  function __construct() {
    parent::__construct();

    $this->load->model('Concept');
    $this->load->model('Announcement');
    $this->load->model('NewsEntries');
    $this->load->model('GroupConceptPermission');
    $this->load->model('ObjectMember'); //generic table holding members for both concepts and groups
    $this->load->model('manage/manage_model');
    $this->load->model('includes/general_model'); //General Module
    $this->load->model('backend/moderate_model');
    $this->load->model('FileCategory');
    $this->load->model('Prompt');
    $this->load->model('Widget');
    $this->load->model('WidgetCategories');
  }

  //Default Offers page
  function index() {
  	$data = array();
  	$my_user = $this->session->userdata("logged_user");

    
  	$concepts = $this->Concept->getConcepts(null, array('total_members' => 'DESC'), $my_user['userid'], 'active');
  	$concepts = (array)$concepts;

  	foreach($concepts as $index => $concept)
  	{
		$manager_data = array();
  		$concepts[$index]['members'] = $this->ObjectMember->getMembers('Concept', $concept['id'], 8);
		$managers=$this->Concept->getConceptManagers(intval($concept['id']));
		foreach($managers as $key => $manager)
		{
			$manager_data[] = $manager['id'];
		}
		$concepts[$index]['managers']= $manager_data;
  	}
  	$data['concepts'] = $concepts;
  	$my_user = $this->session->userdata("logged_user");
  	$this->load->model('User');
    $data['myprofile'] = $this->User->getByEmail($my_user['useremail']);
	$filters = array(
	'feature' => 'Concept',
	'user_id' => $my_user['userid']
	);
	$prompt_data =  $this->Prompt->getByFilters($filters);
	if(isset($prompt_data) &&  $prompt_data!= '')
	{
		$data['user_prompt'] = 0;
	}
	else
	{
		$data['user_prompt'] = 1;
	}
    $this->load->vars($data);
    //added the below lines as the total notifications were not visible for the concept page
    $data['myprofile']['total_notifications'] = $this->Activities->countUserActivities($my_user['userid']);
		$data['myprofile']['has_surveys'] = $this->SurveyUser->countUserSurveys($my_user['userid']);
    
    $this->load->view("concepts", $data);
  }

  public function view($slug) {
    $offer_concepts="";
    $my_user = $this->session->userdata("logged_user");
    $concept = $this->Concept->getConceptBySlug($slug, $my_user['userid']);
	if(empty($concept) && !isset($concept['id']))
	  {
		redirect('locator/notfound');
	  }

    
    $concept['members'] = $this->ObjectMember->getMembers('Concept', $concept['id'], 12);
    $concept['managers']=$this->Concept->getConceptManagers(intval($concept['id']));
    $concept['managers_count']=$this->Concept->getCountConceptManagers(intval($concept['id']));
    $data['announcements'] = $this->Announcement->getAnnouncements(3, 0, (int)$concept['id']);
	$data['latest_news'] =$this->NewsEntries->getNewsList(5, 0, (int)$concept['id']);
    $albums = $this->manage_model->get_album_list();
    $data['albums'] = $albums;
    


    $data['concept']  =  $concept;
    
    $manager_data = array();
    $data['user_name'] = $this->session->userdata('fullname');
    


    //TODO-- remove code dupilication here
    $user_details = $this->moderate_model->get_user_details($my_user['userid']);
    if(count($user_details)){
      $concept_id = $user_details[0]['concept_id'];
    }
    //If concept Corporate then show offer posting dropdown
    if($concept_id == 5){
      $offer_concepts = $this->general_model->gn_get_concepts();

    }else{
      $offer_concepts = '';
    }
    $user_permission = $this->session->userdata('permissions');
    $territory = $this->manage_model->get_territory_names();
    $concepts = $this->manage_model->get_concept_names();
    $grades = $this->manage_model->get_grade_names();
    $data['offer_concepts'] = $offer_concepts;
    $data['location'] = $territory;
    $data['cmb_location'] = $territory;
    $data['concepts'] = $concepts;
    $data['cmb_concepts'] = $concepts;
    $data['cmb_album_category'] = $this->manage_model->get_album_categories();
    $data['grades'] = $grades;
    
  	$data['user_permissions'] = $user_permission;
  	$data['user_details'] = $user_details[0];

    
    $data['concept_manager'] = $this->Concept->isConceptManager($concept['id'], $my_user['userid']);
	if(!empty($data['concept_manager']))
	{
		$filters1 = array(
		'parent_type' => 'Concept',
		'parent_id' => $concept['id']
		);
	}
	else
	{
		$filters1 = array(
		'parent_type' => 'Concept',
		'parent_id' => $concept['id'],
		'status' => 1
		);
	}
    $data['file_categories'] = $this->FileCategory->getFileCategories(null, 'active');
	$data['widget'] = $this->Widget->getByFilters($filters1);
	$widget_categories = $this->WidgetCategories->getByFilters(array('parent_category_id' => 0));
	$widgets_data = array();
	$widget_select = array();

      foreach ($widget_categories as $key => $categories)
	{
	$widget_select[$categories['id']]['name'] = $categories['name'];
	$widget_select[$categories['id']]['id'] = $categories['id'];
	$widgets = array();
		$widget_sub_categories = $this->WidgetCategories->getByFilters(array('parent_category_id' => $categories['id']));
        $this->array_sort_by_column($widget_sub_categories, 'name');
        foreach($widget_sub_categories as $sub_categories){
		$filters1 = array(
		'parent_type' => 'Concept',
		'parent_id' => $concept['id'],
		'category_id' => $sub_categories['id']
		);
		$widget_select[$categories['id']]['option'][$sub_categories['id']]['name'] = $sub_categories['name'];
		$widget_select[$categories['id']]['option'][$sub_categories['id']]['id'] = $sub_categories['id'];
		$widgets[$sub_categories['name']] =  $this->Widget->getByFilters($filters1);
		}
		
	$widgets_data[$categories['name']] = $widgets;	
	}


      $data['widgets_data'] = $widgets_data;
	$data['widget_select'] = $widget_select;
	$page_type = $this->uri->segment(1,0); 	
	$concepts_name = $this->uri->segment(2,0);
	$edit_order = $this->uri->segment(3,0);
	
	if(isset($edit_order) && !empty($edit_order) && $edit_order == "sort_widgets")
	{
		$this->load->view('widget_sort', $data);
	}
	else if(isset($edit_order) && !empty($edit_order) && $edit_order == "pages" )
	{
		$page = $this->uri->segment(4,0);
		if($page != '' && is_numeric($page))
		{
			if(isset($data['concept_manager']['id']) && $data['concept_manager']['id'] > 0)
			{
			$details = array(
                'id' => intval($page)
                ); //Array of data
			} else {
			$details = array(
                'id' => intval($page),
                "status" => 1
                ); //Array of data
			}
			$widget = $this->Widget->getByFilters($details);
			if(isset($widget[0]))
			{
			$widget = (array)$widget[0];
			$data['widget_data'] = $widget;
			$this->load->view('concept_pages', $data);
			} else {
			 $this->notfound();
			}
			
			
		}
	}
	else
	{
		$this->load->view('single_concept', $data);
	}
  }
  
  function notfound()
  {
  	header("HTTP/1.1 404 Not Found");
  	$this->load->view('not_found');
  }

    /**
     * @param $arr Array to be sorted
     * @param $col Coloumn Name
     * @param int $dir
     */
    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC)
    {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

}

/* End of file concepts.php */
/* Location: ./system/application/modules/concepts/controllers/concepts.php */