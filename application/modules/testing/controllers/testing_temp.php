<?php

class Testing_temp extends MX_Controller {

  function Testing_temp() {
    parent::__construct();
    $this->load->model('testing_temp_model');
  }
//Accepts filepath and data
  function _create_log_file($file_path,$data){
    $this->load->helper('file');
    if (write_file($file_path, $data,'w')){
      echo 'Log file created at ',$file_path,'<br />';
    }else{
      echo 'Log file creation failed at ',$file_path,'<br />';
    }
  }




//User Purge list pre check
  function _user_purge_data_check($file_path) {

    $file = fopen($file_path, "r") or exit("Unable to open file!");
    $row_cnt = 1;
    $row_validation = array();
    $new_user_arr = array();
    $territory_arr = array();
    $concept_arr = array();
    $designation_arr = array();
    $department_arr = array();
    $location_arr = array();
    $band_arr = array();
    $processed_user_data = array();
    while (!feof($file)) {
      $row = fgets($file);
      $user_data = array();
      $user_data = explode(",", $row);
      //print_r($user_data); exit;
      if (!$user_data[0]) {
        continue;
      }

      //Row Header validation
      if ($row_cnt == 1) {
        if (trim($user_data[0]) != 'EMAIL_ADDRESS') {
          $error_array[] = 'Email address missing(EMAIL_ADDRESS)';
        }
        
        if (count($error_array)) {
          //echo '<xmp>' . print_r($error_array, 1) . '</xmp>';
          //exit;
          $check_result['proceed'] = 0;
          $check_result['error'] = $error_array;
          $check_result['data'] = array();
          return $check_result;
        }
        $row_cnt++;
        continue;
      }

      $error_cnt = 0;
  
      if (!filter_var(trim($user_data[0]), FILTER_VALIDATE_EMAIL)) {
        $row_validation[] = 'Invalid email address: ' . trim($user_data[0]);
        $error_cnt++;
      }
      if ($error_cnt == 0) {
        $processed_user_data[] = $user_data;
      }

      $row_cnt++;
    }
    fclose($file);
    //echo '<xmp>' . print_r($row_validation, 1) . '</xmp>';
    $check_result['proceed'] = 1;
    $check_result['error'] = $row_validation;
    $check_result['data'] = $processed_user_data;
    return $check_result;
  }

  function _process_purge_list($user_data) {
    if (!count($user_data)) {
      return false;
    }
    $log = array();
    $found_cnt = 0;
    $not_found_cnt = 0;
    foreach ($user_data as $user) {
      $email = $user[0];

      if ($reason == 'Employee Transfer') {
        $log[] = 'Employee transfer for user with Employee number: ' . $emp_id . ' and Email: ' . $email;
      } else {
        $user_check = $this->testing_temp_model->user_db_check(strtolower(trim($email)));
        if (count($user_check)) {
          $user_id = $user_check['id'];
          $this->testing_temp_model->delete_user_trace($user_id);
          $log[] = 'User deleted with Email: ' . $email;
          $found_cnt++;
        } else {
          $log[] = 'User not found with Email: ' . $email;
          $not_found_cnt++;
        }
      }
    }
    $purge_output['log'] = $log;
    $purge_output['del_user'] = $found_cnt;
    $purge_output['not_found_user'] = $not_found_cnt;

    return $purge_output;
  }

  function sftp_connection() {
    $this->load->helper('sftp_lib');

    $sftp_host = '83.111.79.197';
    $sftp_user = 'dotahead';
    $sftp_pass = 'd0t@heaD';
    $remote_directory = '/in/';
    $local_directory = '/var/www/html/sftp_files/';

    //$make

    $log_path = '/var/www/landmark/sftp_reports/';

    //echo '<xmp>' . print_r($user_update, 1) . '</xmp>';
    $purge_data = $this->_user_purge_data_check($local_directory . 'x-emp.csv');
    //echo '<xmp>' . print_r($purge_data, 1) . '</xmp>';
    //exit;
    if ($purge_data['proceed'] == 1) {
      $purge_output = $this->_process_purge_list($purge_data['data']);
      //echo '<xmp>' . print_r($purge_data['error'], 1) . '</xmp>';
      echo '------------------------------------------Error------------------------------------<br/>';
      //echo @implode('<br />', $purge_data['error']);
      $user_purge_error_log = "\nPurge Data Error Report (".date('Y-m-d').")\n\n".@implode("\n", $purge_data['error']);
      $this->_create_log_file($log_path.'intranet_purge_data_error_report_'.date('Y-m-d').'.log',$user_purge_error_log);

      echo '<br/>------------------------------------------Log------------------------------------<br/>';
      //echo @implode('<br />', $purge_output['log']);
      $user_purge_log = "Purge Report(".date('Y-m-d').")\n\n".@implode("\n", $purge_output['log']);
      $user_purge_log .= "\n\nNumber of users  deleted: " . $purge_output['del_user'];
      $user_purge_log .= "\nNumber of users not found: " . $purge_output['not_found_user'];
      
      $this->_create_log_file($log_path.'intranet_purge_report_'.date('Y-m-d').'.log',$user_purge_log);

      //echo '<xmp>' . print_r($purge_output, 1) . '</xmp>';
    } else {
      echo @implode('<br />', $purge_data['error']);
    }
  }

}
