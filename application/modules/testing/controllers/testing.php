<?php

class Testing extends MX_Controller {

  function __construct() {
    parent::__construct();
    $this->load->library('unit_test');
  }

  function index()
  {
    //get all method names automatically
    $methods = get_class_methods($this);
    $methods = array_diff($methods, array('__construct', 'index', '__get'));
    foreach($methods as $method)
    {
      //call every method to run all test cases
      $this->$method();
    }
  }

  function user()
  {
    $this->load->model('User');

    echo $this->unit->run($this->User->get(1), 'is_array', 'Returns a User');
    echo $this->unit->run($this->User->getLastLoginByEmail('aristo.bhupal@landmarkgroup.com'), 'is_int', 'Returns User Last Login Time');
    echo $this->unit->run($this->User->getAll(5, array('first_name' => 'Asc')), 'is_array', 'Returns All Users');
    $user_array = array('email' => 'aqeel@yahoo.com', 'designation_id' => 5);
    //echo $this->unit->run($this->User->add($user_array), 'is_int', 'adds a User');

    echo $this->unit->run($this->User->removeUserTrace(25086), 'is_true', 'deletes an existing user');
    $user_array = array('reply_username' => 'aqeel');
    echo $this->unit->run($this->User->update($user_array,25092), 'is_true', 'updates an existing user');
    echo $this->unit->run($this->User->countWelcomeUsers(), 'is_int', 'counts the Users Pending Welcome Email');
    echo $this->unit->run($this->User->getWelcomeUsers(), 'is_array', 'Returns All Pending welcome users');
    echo $this->unit->run($this->User->getByEmailHRMS('manu.jeswani@landmarkgroup.com', '31'), 'is_array', 'Returns a user by filters');
    echo $this->unit->run($this->User->getByEmail('manu.jeswani@landmarkgroup.com'), 'is_array', 'Returns a user profile by email');
    echo $this->unit->run($this->User->getIdByEmail('aristo.bhupal@landmarkgroup.com'), 'is_string', 'Returns User Id by Email');
    echo $this->unit->run($this->User->getTypeByEmail('aristo.bhupal@landmarkgroup.com'), 'is_string', 'Returns User Type by Email');
    echo $this->unit->run($this->User->getHrmsIdByEmail('aristo.bhupal@landmarkgroup.com'), 'is_int', 'Returns User HRMS Id by Email');
    echo $this->unit->run($this->User->getFullName(9), 'is_string', 'Returns User Full Name by Id');
    echo $this->unit->run($this->User->getFullNameByEmail('aristo.bhupal@landmarkgroup.com'), 'is_string', 'Returns User Full Name by email');
    echo $this->unit->run($this->User->getLocationByEmail('aristo.bhupal@landmarkgroup.com'), 'is_int', 'Returns User Full Name by email');
    echo $this->unit->run($this->User->authenticateProfile('aristo.bhupal@landmarkgroup.com', 'password'), 'is_int', 'Authenticate User Profile');
    echo $this->unit->run($this->User->getUserProfile(1, 2), 'is_array', 'Get User Profile, user contacts count and user status');
    echo $this->unit->run($this->User->getUserProfileByEmail('aristo.bhupal@landmarkgroup.com'), 'is_array', 'Get User Profile and user status by User Email');

  }

  function designation()
  {
    $this->load->model('Designation');
    
    echo $this->unit->run($this->Designation->get(1), 'is_array', 'Returns a Designation');
    echo $this->unit->run($this->Designation->getAll(5), 'is_array', 'Returns All Designation');
    echo $this->unit->run($this->Designation->getByName('Test Designation'), 'is_array', 'Returns a Designation By Name');
    $data = array('name' => 'Test Designation');
    echo $this->unit->run($this->Designation->add($data), 'is_int', 'adds a Designation');
    echo $this->unit->run($this->Designation->remove(1223), 'is_true', 'deletes an existing Designation');
    $data = array('name' => 'Test Designation2');
    echo $this->unit->run($this->Designation->update($data,1229), 'is_true', 'updates an existing Designation');

  }

  function concept()
  {
    $this->load->model('Concept');
    
    echo $this->unit->run($this->Concept->get(1), 'is_array', 'Returns a Concept');
    echo $this->unit->run($this->Concept->getByName('Test Concept'), 'is_array', 'Returns a Concept By Name');
    echo $this->unit->run($this->Concept->getAll(5), 'is_array', 'Returns All Concept');
    $data = array('name' => 'Test Concept', 'db_concept_name' => 'Test Concept');
    echo $this->unit->run($this->Concept->add($data), 'is_int', 'adds a Concept');
    echo $this->unit->run($this->Concept->remove(100), 'is_true', 'deletes an existing Concept');
    $data = array('name' => 'Test Concept2', 'db_concept_name' => 'Test Concept2');
    echo $this->unit->run($this->Concept->update($data,47), 'is_true', 'updates an existing Concept');

  }

  function department()
  {
    $this->load->model('Department');
    
    echo $this->unit->run($this->Department->get(1), 'is_array', 'Returns a Department');
    echo $this->unit->run($this->Department->getByName('Test Department'), 'is_array', 'Returns a Department By Name');
    echo $this->unit->run($this->Department->getAll(5), 'is_array', 'Returns All Department');
    $data = array('name' => 'Test Department');
    echo $this->unit->run($this->Department->add($data), 'is_int', 'adds a Department');
    echo $this->unit->run($this->Department->remove(100), 'is_true', 'deletes an existing Department');
    $data = array('name' => 'Test Department2');
    echo $this->unit->run($this->Department->update($data,47), 'is_true', 'updates an existing Department');

  }

  function group()
  {
    $this->load->model('Group');
    
    echo $this->unit->run($this->Group->get(1), 'is_array', 'Returns a Group');
    echo $this->unit->run($this->Group->getAll(5), 'is_array', 'Returns All Group');
    $data = array('name' => 'Test Group', 'group_status' => 0, 'created_by' => 24254, 'created_on' =>time());
    echo $this->unit->run($this->Group->add($data), 'is_int', 'adds a Group');
    echo $this->unit->run($this->Group->remove(660), 'is_true', 'deletes an existing Group');
    $data = array('name' => 'Test Group22');
    echo $this->unit->run($this->Group->update($data,660), 'is_true', 'updates an existing Group');
    echo $this->unit->run($this->Group->getIdByName('Test Group22'), 'is_int', 'Returns a Group id by Name');
    echo $this->unit->run($this->Group->findGroupsByName('cri'), 'is_array', 'find groups matching Name query');

  }

  function albumpicture()
  {
    $this->load->model('AlbumPicture');
    echo $this->unit->run($this->AlbumPicture->countAlbumPictures(1), 'is_int', 'counts pictures in an Album');
    echo $this->unit->run($this->AlbumPicture->getAlbumPictures(1, 20, 0), 'is_array', 'get pictures in an Album');
    
  }

  function album()
  {
    $this->load->model('Album');
    echo $this->unit->run($this->Album->get(1), 'is_array', 'gets an Album');
  }

  function announcementbar()
  {
    $this->load->model('AnnouncementBar');

    echo $this->unit->run($this->AnnouncementBar->getAll(), 'is_array', 'get a list of AnnouncementBars');

    $data = array(
        'announcement_bar_text' => 'sample announcement',
        'posted_by_user_id' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'expiry_date' => '2013-11-02',
        'status' => 1
    );
    echo $this->unit->run($this->AnnouncementBar->add($data), 'is_int', 'adds a AnnouncementBar');
  }

  function entry()
  {

    $this->load->model('Entry');

    echo $this->unit->run($this->Entry->get(15), 'is_array', 'gets an Entry Details');
    echo $this->unit->run($this->Entry->getEntryTitlesByCategory('offers'), 'is_array', 'gets an Entry Titles by Category name');
  }

  function userpermission()
  {

    $this->load->model('UserPermission');

    echo $this->unit->run($this->UserPermission->getUserPermissions(20718), 'is_array', 'get User Permissions');

  }

  function chatstatus()
  {

    $this->load->model('ChatStatus');

    echo $this->unit->run($this->ChatStatus->CheckUserChatStatus(20718), 'is_int', 'Check User Comet Chat');

    $data = array('userid' => 24478, 'message' => 'my test message', 'status' => 'available', 'typingto' => 24126, 'typingtime' => 173738383);
    echo $this->unit->run($this->ChatStatus->add($data), 'is_int', 'adds to Comet Chat');
    
  }

  function logintracker()
  {

    $this->load->model('LoginTracker');

    $data = array('user_id' => 20718, 'login_time' => date('Y-m-d h:i:s'));
    echo $this->unit->run($this->LoginTracker->add($data), 'is_int', 'add to login tracker');
    
  }

  function file()
  {
    $this->load->model('File');
    echo $this->unit->run($this->File->countUserFiles(20716), 'is_int', 'count user files');
    $filters = array('user_id' => 20716);
    echo $this->unit->run($this->File->getFiles(50, 0, $filters), 'is_array', 'get user files');

    echo $this->unit->run($this->File->getLatestFiles(16), 'is_array', 'get latest files by file category');
    echo $this->unit->run($this->File->getFiles(3, 0), 'is_array', 'get all files by limit, offset');
    echo $this->unit->run($this->File->getFileDetails(189), 'is_array', 'get file details');
    
    $filters = array('is_link' => true);
    echo $this->unit->run($this->File->getFiles(2, 0, $filters), 'is_array', 'get all links');
    $filters = array('is_file' => true);
    echo $this->unit->run($this->File->getFiles(2, 0, $filters), 'is_array', 'get files which are not links');
    //print_r($this->File->getFiles(2, 0, $filters));
    echo $this->unit->run($this->File->getFilesByTag(5, 0, 1), 'is_array', 'get files by a tag id');


    echo '<pre>';

    print_r($this->File->files_get_all_files_tags(1, 5, 0));
    print_r($this->File->getFilesByTag(5, 0, 1));
    //$filters = array('is_link' => true);
    
  }

  function filetag()
  {
    $this->load->model('FileTag');
    echo $this->unit->run($this->FileTag->getFileTags(11), 'is_array', 'get tags for a given file');

  }

  function filecategory()
  {
    $this->load->model('FileCategory');
    echo $this->unit->run($this->FileCategory->getFileCategories(), 'is_array', 'get file categories');

   /* echo '<pre>';
    print_r($this->FileCategory->getFileCategories());*/
    
  }

  function groupuser()
  {
    $this->load->model('GroupUser');
    echo $this->unit->run($this->GroupUser->getUserGroups(20716), 'is_array', 'get groups for a given user');
    echo $this->unit->run($this->GroupUser->removeUserFromGroups(10000), 'is_bool', 'removes a user from all User Groups');
    $data = array('user_id' => 10000, 'intrest_groups_id' => 7);
    echo $this->unit->run($this->GroupUser->add($data), 'is_int', 'adds a User to Group');

  }

  function favoritelink()
  {
    $this->load->model('FavoriteLink');
    
    echo $this->unit->run($this->FavoriteLink->get(1), 'is_array', 'Returns a Favorite Link');
    
    //echo $this->unit->run($this->FavoriteLink->getAll(5), 'is_array', 'Returns All Group');
    
    $data = array('link_name' => 'Test Link', 'link' => 'http://www.abc.com', 'user_id' => 24254, 'status' => 1);
    echo $this->unit->run($this->FavoriteLink->add($data), 'is_int', 'adds a Favorite Link');

    //echo $this->unit->run($this->Group->remove(660), 'is_true', 'deletes an existing Group');

    $data = array('link_name' => 'Test link2');
    echo $this->unit->run($this->FavoriteLink->update($data,218), 'is_true', 'updates an favorite link');

  }

  function offer()
  {
    $this->load->model('Offer');
    
    echo $this->unit->run($this->Offer->getOffers(1, 10), 'is_array', 'Returns Offers List');
    echo $this->unit->run($this->Offer->getOfferDetails(979), 'is_array', 'Returns Offer details');

  }

  function useroffer()
  {
    $this->load->model('UserOffer');
    
    echo $this->unit->run($this->UserOffer->getUserOfferConcepts(8269), 'is_array', 'Returns User Offer Subscribed Concept IDs');
    $data = array('user_id' => 10000, 'concept_id' => 22);
    echo $this->unit->run($this->UserOffer->add($data), 'is_int', 'adds a User Offer Subscription');
    echo $this->unit->run($this->UserOffer->removeUserOffers(10000), 'is_bool', 'Remove a User from All offers');
    
  }

  function news()
  {
    $this->load->model('News');
    
    echo $this->unit->run($this->News->getNewsList(1, 10), 'is_array', 'Returns News List');
    echo $this->unit->run($this->News->getNewsDetails(979), 'is_array', 'Returns News details');
  }

  function announcement()
  {
    $this->load->model('Announcement');
    
    echo $this->unit->run($this->Announcement->getAnnouncements(1, 10), 'is_array', 'Returns Announcement List');
    echo $this->unit->run($this->Announcement->getAnnouncementDetails(979), 'is_array', 'Returns Announcement details');

  }

  function itemimage()
  {
    $this->load->model('ItemImage');

    echo $this->unit->run($this->ItemImage->getItemImages(56), 'is_array', 'get Item Images');
    $data = array(
              'original_image' => 'apple-iphone-3g.jpg',
              'market_items_id' => 672
            );
    echo $this->unit->run($this->ItemImage->add($data), 'is_int', 'adds an image to item');
    echo $this->unit->run($this->ItemImage->remove(165), 'is_true', 'removes an item image');
    
  }

  function item()
  {
    $this->load->model('Item');
    
    echo $this->unit->run($this->Item->getItem(13), 'is_array', 'Returns item details');
    echo $this->unit->run($this->Item->getItemsByCategory(12, 1, 2, 10), 'is_array', 'Returns items matching a category and item type');
    echo $this->unit->run($this->Item->getFreebies(null, 2,0), 'is_array', 'Returns no of freebies in market place');
    echo $this->unit->run($this->Item->getFreebies(21253, 2,0), 'is_array', 'Returns no of freebies by a user in market place');
    echo $this->unit->run($this->Item->getItemsforSale(null, 2,0), 'is_array', 'Returns no of items for sale in market place');
    echo $this->unit->run($this->Item->getItemsforSale(22008, 2,0), 'is_array', 'Returns no of items for sale by a user in market place');
    echo $this->unit->run($this->Item->getRecentItemsforSale(), 'is_array', 'Returns 5 most recent items for sale in market place');
    echo $this->unit->run($this->Item->getLatestFreebies(), 'is_array', 'Returns 10 latest freebies in market place');
    echo $this->unit->run($this->Item->getLatestWanted(), 'is_array', 'Returns 10 latest wanted items in market place');
    echo $this->unit->run($this->Item->countWantedItems(), 'is_int', 'count number of wanted items');
    echo $this->unit->run($this->Item->countFreebies(), 'is_int', 'count number of freebies');
    echo $this->unit->run($this->Item->countItems(3), 'is_int', 'count number of items by type');
    echo $this->unit->run($this->Item->countItemsforSale(), 'is_int', 'count number of items for sale in market place');
    echo $this->unit->run($this->Item->countItems(1, 6), 'is_int', 'count number of items by type and category');
    echo $this->unit->run($this->Item->getCategoryList('category', 1), 'is_array', 'get items counts by category and type');
    $data = array(
            'title' => 'My Playstation',
            'description'=> 'very good playstation',
            'user_id'=> 4021,
            'item_min_price'=> 0,
            'item_max_price'=> 1000,
            'item_condition'=> 1,
            'item_type'=> 2,
            'category_id'=> 6,
            'items_status'=> 2
            );
    //echo $this->unit->run($this->Item->add($data), 'is_int', 'adds an item to market place');
    $data = array(
            'title'=> 'my plastation old'
            );
    echo $this->unit->run($this->Item->update($data, 672), 'is_true', 'update an item in market place');

    echo $this->unit->run($this->Item->deleteItem(673), 'is_true', 'deletes an item from market place');
    echo $this->unit->run($this->Item->updateStatus(672, 2), 'is_true', 'update an item status in market place');
  }


}
