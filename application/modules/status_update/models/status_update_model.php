<?php
/*
 * "su" stands for Status Update
 *
 * */
Class Status_update_model extends CI_Model{
	function Status_update_model(){
		parent::__construct();
	}

	function su_show_following_people($pagelimit){
		$userId = $this->session->userdata('userid');
		$limit = (int)$pagelimit;
		$offset = (int)$this->session->userdata('cfgpagelimit');
		$sql = 'SELECT
							a.following_user_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM
							ci_users_following_users a ,
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							a.user_id = ? AND
							a.follow_status = 1 AND
							b.user_id = a.following_user_id AND
							b.status = 1 AND
							c.id = a.following_user_id AND
							d.id = designation_id AND
							e.id = concept_id
						ORDER BY b.id DESC  LIMIT ?,?';
		$result= $this->db->query($sql,array($userId,$limit,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}

	function su_get_like_view_all($status_id , $content_type){
		$statusId = (int)$status_id;
		$sql = 'SELECT u.reply_username, u.display_name, u.profile_pic
				FROM ci_likes l
				INNER JOIN ci_users u ON l.user_id = u.id
				WHERE l.status_id =? AND l.status_type=?';
		$result= $this->db->query($sql,array($statusId,$content_type));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
	
	function su_show_my_contacts($pagelimit){
		$userId = $this->session->userdata('userid');
		$limit = (int)$pagelimit;
		$offset = (int)$this->session->userdata('cfgpagelimit');
		$sql = 'SELECT
							(SELECT following_user_id FROM ci_users_following_users WHERE user_id = ? AND follow_status = 1 AND following_user_id = a.contact_users_id) as following_user_id,
							a.contact_users_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM
							ci_users_contacts a ,
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							a.user_id = ? AND
							b.user_id = a.contact_users_id AND
							b.status = 1 AND
							a.contact_status = 1 AND
							c.id = a.contact_users_id AND
							d.id = designation_id AND
							e.id = concept_id
						GROUP BY b.id
						ORDER BY b.id DESC  LIMIT ?, ?';
		$result= $this->db->query($sql,array($userId,$userId,$limit,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}

	function su_show_all_people($pagelimit){
		$userId = (int)$this->session->userdata('userid');
		$limit = (int)$pagelimit;
		$offset = (int)$this->session->userdata('cfgpagelimit');
		$sql = 'SELECT
						(SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.user_id ) as following_user_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							c.id = b.user_id AND
							b.status = 1 AND
							d.id = designation_id AND
							e.id = concept_id
						ORDER BY b.id DESC LIMIT ?, ?';
		$result= $this->db->query($sql,array($userId,$limit,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
	function su_show_my_replies($pagelimit){
		$userId = (int)$this->session->userdata('userid');
    $user_email = $this->session->userdata('useremail');

    list($reply_user_get,$domain) = @explode("@",$user_email);//user name is the first part of the email address
    $reply_user_get = "@".$reply_user_get; //Checking for username in the message

		$limit = (int)$pagelimit;
		$offset = (int)$this->session->userdata('cfgpagelimit');
		$sql = 'SELECT
						(SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.user_id ) as following_user_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							c.id = b.user_id AND
							b.status = 1 AND
							d.id = designation_id AND
							e.id = concept_id AND
							b.message like "%'.$reply_user_get.'%"
						ORDER BY b.id DESC LIMIT ?, ?';
		$result= $this->db->query($sql,array($userId,$limit,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
	function su_show_my_updates($pagelimit){
		$userId = $this->session->userdata('userid');
		$limit = (int)$pagelimit;
		$offset = (int)$this->session->userdata('cfgpagelimit');
		$sql = 'SELECT
							0 as following_user_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							c.id = ? AND
							b.status = 1 AND
							b.user_id = c.id AND
							d.id = designation_id AND
							e.id = concept_id
						ORDER BY b.id DESC  LIMIT ?, ?';
		$result= $this->db->query($sql,array($userId,$limit,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}

/* Add Status Message to Database */
	function su_submit_status_message($data){
		$str = $this->db->insert('ci_status_updates', $data);
		return $this->db->insert_id();
	}
/* Last posted message of the user */
	function su_your_status(){
		$userId = $this->session->userdata('userid');
		$sql = 'SELECT
						 	message
						FROM
							ci_users_status_updates
						WHERE
							user_id = ?
						ORDER BY id DESC LIMIT 1';
		$result= $this->db->query($sql,array($userId));
		$data = array();
		if($result->num_rows() > 0){
			$data = $result->row();
			$message =$data->message;
		}else{
			$message = "";
		}
		$result->free_result();
		return $message;
	}

	function su_delete_status_message($message_id){
		$userId = $this->session->userdata('userid');
		$sql = "DELETE FROM
							`ci_users_status_updates`
						WHERE
							`user_id` = ? AND `id` = ?
						LIMIT 1";
		$result= $this->db->query($sql,array($userId,$message_id));
		return $this->db->affected_rows();
	}
	function su_delete_status_replies($message_id){
		$sql = "DELETE FROM
							`ci_my_replies`
						WHERE
							`status_msg_id` = ? ";
		$result= $this->db->query($sql,array($message_id));
		return $this->db->affected_rows();
	}



  /* Status Update Ajax call*/
  function su_get_updates_by_id($msg_id){
    $userId = $this->session->userdata('userid');
    $sql = "SELECT
              0 as following_user_id,
              b.id as message_id,
              b.message,
              b.object_id as user_id,
              b.create_dttm,
              CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
              c.profile_pic,
              c.email,
              d.name as designation,
              e.name as concept
            FROM
              ci_status_updates b,
              ci_users c,
              ci_master_designation d,
              ci_master_concept e
            WHERE
              c.id = ? AND
              b.status = 1 AND
              b.object_id = c.id AND b.object_type = 'User' AND
              d.id = designation_id AND
              e.id = concept_id AND
              b.id = ?
            ";
    $result= $this->db->query($sql,array($userId,$msg_id));
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }


  /* user id from email username(@) explode */
  function su_get_user_id_reply_username($reply_username){
    $sql = 'SELECT
              id
            FROM
              ci_users
            WHERE
              reply_username = ? ';

    $result= $this->db->query($sql,array($reply_username));
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data = $row['id'];
      }
    }
    $result->free_result();
    return $data;
  }

 /* Add Replied status message to Database */
  function su_submit_replied_details($data){
    $str = $this->db->insert('ci_my_replies', $data);
    return $this->db->insert_id();
  }
 /* my reply count*/
  function su_get_my_reply_count(){
    $userId = $this->session->userdata('userid');
    $sql = 'SELECT count(id) as total FROM ci_my_replies WHERE read_status = 1 AND user_id = ? ';

    $result= $this->db->query($sql,array($userId));
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data = $row['total'];
      }
    }
    $result->free_result();
    return $data;
  }

 /* check my reply message */
  function su_check_my_reply_message($msg_id){
    $userId = $this->session->userdata('userid');
    $sql = 'SELECT id FROM ci_my_replies WHERE read_status = 1 AND user_id = ? AND status_msg_id  = ?';

    $result= $this->db->query($sql,array($userId,$msg_id));
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data = $row['id'];
      }
    }
    $result->free_result();
    return $data;
  }

  function su_update_my_reply_status($reply_id){
    $userId = $this->session->userdata('userid');
    $sql = 'UPDATE ci_my_replies SET read_status = 2 WHERE user_id = ? AND id  = ?';
    $this->db->query($sql,array($userId,$reply_id));
    return true;
  }
}