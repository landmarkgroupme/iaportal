<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/status_update.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
	  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="#">People</a></li>
					<li>Status Updates</li>
					<li><?=$breadcrumbs?></li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>Status Updates</h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li id="su_show_following_people" <?=$following_active?>><a href="following_people"><span><em>Following</em></span></a></li>
							<li id="su_show_my_contacts" <?=$mycontacts_active?>><a href="mycontacts"><span><em>My Contacts</em></span></a></li>
							<li id="su_show_all_people" <?=$all_people_active?>><a href="all_people"><span><em>All People</em></span></a></li>
							<li id="su_show_my_updates" <?=$my_updates_active?>><a href="my_updates"><span><em>My Updates</em></span></a></li>
							<li id="su_show_my_replies" <?=$my_replies_active?>><a href="my_replies"><span><em><?=$my_replies?></em></span></a></li>
						</ul>
					</div>
				</div>
				<!-- status box -->
				<form action="<?=base_url()?>index.php/status_update/submit_status_message" method="post" class="share-form-textarea">
					<fieldset>
						<textarea class="textarea" name="txtarShare" id="txtarShare" rows="4" cols="4">What are you working on ?</textarea>
						<input type="image" id="btn-ar-share" src="<?=base_url();?>images/btn-share-txtar.jpg" alt="Share!"/>
                                                <div id="status-msg-count">160</div>
					</fieldset>
				</form>
				<div id="status-msg-status" class="msg-ok"></div>
				<br />
			<!--	<div class="your-status"><strong>Your Status:</strong> <span id="your-status-msg"><?=$yourstatus?></span></div>-->
				<!-- peoples list -->
				<div class="posts-box">
					<ul id="show-list" class="posts-list hfeed">
					<?php foreach($messagelist as $msg):?>
						<li class="type2 hentry <?=$msg['highlight_status_message']?> <?=$highlight_status_message?>">
								<div class="frame">
									<span class="visual">
										<img src="<?=base_url();?>images/user-images/105x101/<?=$msg['profile_pic']?>" alt="<?=$msg['first_name']." ".$msg['last_name']?>" width="53" height="53" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="<?=site_url('profile/view_profile/' . $msg['user_id'])?>"><?=ucfirst(strtolower($msg['first_name']))." ".ucfirst(strtolower($msg['last_name']))?></a><em><?=$msg['designation'].", ".$msg['concept']?></em></strong>
										<p><?=$msg['message']?></p>
										<span><?=$msg['message_time'];?></span>
										<ul class="actions">
											<li><a class="msg-reply" id="<?=$msg['reply_user'];?>" href="#">Reply</a></li>
											<li><a href="mailto:<?=$msg['email']?>">Email</a></li>
											
											<?php if($this->session->userdata('userid') == $msg['user_id']):?>
											<li><a class="msg-delete" id="<?=$msg['message_id']?>" href="#">Delete</a></li>
											<?php elseif($this->session->userdata('userid') != $msg['user_id']): ?>
											<li><a rel="pop_win" class="msg-report" href="<?=site_url("profile/report_status_update/".$msg['message_id'])?>" class="msg-report" href="#">Report</a></li>
											<li><a class="<?=($msg['following_user_id'])? "msg-unfollow":"msg-follow"?>" rel="<?=$msg['user_id']?>" href="#"><?=($msg['following_user_id'])? "Unfollow":"Follow"?></a></li>
											<?php endif;?>
										</ul>
									</div>
								</div>
						</li>
					<?php $highlight_status_message=""; endforeach;?>
					</ul>
					<?php if($show_load_more):?>
					<a href="#" id="<?=$this->session->userdata('cfgpagelimit')?>" class="more-stories">View More</a>
					<?php endif; ?>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- user box -->
				<?php $this->load->view("includes/useful_link");?>
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/latest_update");?>
				<!-- sidebar box, My Interest Groups -->
				<?php $this->load->view("includes/my_interest_groups");?>
				<!-- sidebar box, Newcomers -->
				<?php $this->load->view("includes/newcomers");?>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li><a href="<?=base_url()?>index.php/people/" tabindex="15"><span>Company Directory</span></a></li>
						<li class="active"><a href="<?=base_url()?>index.php/status_update/" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=base_url()?>index.php/interest_groups/" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>		
	</div>
</body>
</html>