<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title>Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
</head>
<body class="<?php echo isset($default_content) ? 'profile' : 'profile' ; ?>">



<?php $this->load->view('global_header.php'); ?>
<div class="section wrapper clearfix">
	<ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Status</li>
    </ul>
	<div class="left-contents">
	 <div id="recent-posts">
	 <?php if ($exits) { ?>
	 <div class="loader-more">&nbsp;</div>
	 <?php  } else { ?>
	 <div class="no-result-block d-n" style="display: block;">The status was removed.</div>
	 <?php } ?>
	 </div>
	</div>
	<div class="right-contents media-widget">
  </div> <!-- right-contents -->
</div> <!-- section -->


<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>
<?php $this->load->view('templates/js/content_updates'); ?>
<?php $this->load->view('partials/interaction_js'); ?>
<script src="media/js/jquery.carouFredSel-6.2.0.js" type="text/javascript"></script>
<!-- <script src="js/custom_js/general_js.js" type="text/javascript"></script> -->
<script type="text/javascript">
var limit = 10;
var start = 0;
var nearToBottom = 250;
var filter_user = <?php echo isset($statuses_filter) ? $statuses_filter : 0; ?>;
var status_id = <?php echo $status_id;?>;
$(document).ready(function(){
if(<?php echo $exits; ?>)
{
loadStatus();
}
});
$(function() {
    $("img:below-the-fold").lazyload({
        event : "sporty"
    });
});
$(window).bind("load", function() {
    var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
});

function loadStatus()
	{
		$('#recent-posts div.loader-more').show();
		var service_url = siteurl+"status_update/ajax_status";
		var request_data = {};
		//filter_user is used to filter status on user profile page.
			request_data = {'limit': limit, 'start' : start, 'filter_user' : filter_user,'status_id':status_id};
		//filter by concept
		if (typeof feedtype != 'undefined')
		{
			request_data.feedtype = feedtype;
			request_data.feedtype_id = feedtype_id;
		}
		$.ajax({
			url: service_url,
			data: request_data,
			async: false,
			dataType: "json",
			type: "POST",
			success: function(response){
			if(response.feed.length == 0 &&  start == 0)
			{
					$('#recent-posts div.loader-more').before('<dl class="block-2 clearfix no-feed-found"><dt><dd><h2>No feed found!</h2></dd></dt></dl>');        
			}
				console.log(response);
				_.templateSettings.variable = "rc";

		        // Grab the HTML out of our template tag and pre-compile it.
		        var template = _.template(
		            $( "script.template" ).html()
		        );
		        //$('#recent-posts ul.filter li').removeClass('active');
		       // $(parent).addClass('active');
		        //$('#recent-posts ul.filter').append(template(response));
		        $('#recent-posts div.loader-more').before(template(response));

				},
			complete: function(response){
				$('#recent-posts div.loader-more').hide();
			}
		});

	}

</script>
</body>
</html>