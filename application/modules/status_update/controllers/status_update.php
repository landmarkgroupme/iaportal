<?php

/*
 * "su" stands for Status Update
 * "gn" stands for General
 * */

class Status_update extends General {
//Constructor
  function __construct() {
    parent::__construct();
    $this->load->model('status_update_model');
    $this->load->model('Album');
    $this->load->model('profile/m_profile');
    $this->load->model('includes/general_model'); //General Module
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');
    $this->load->model('Activities');
    $this->load->model('Statuses');
    $this->load->model('manage/manage_model');
    $this->load->model('Photo');
	$this->load->model('Prompt');
	$this->load->model('Entry');
	$this->load->model('AlbumPicture');
	$this->load->model('Polls');
	$this->load->model('User');
	$this->load->helper('time_ago_helper');
	$this->load->model('NewsEntries');
	$this->load->model('Item');
	$this->load->model('File');
	$this->load->model('Widget');
    
  }

  function notifications()
  {
  	$my_user = $this->session->userdata("logged_user");
  	$activities = $this->Activities->getUserActivitiesUnviewed($my_user['userid']);
    $data['json'] = json_encode(array('notifications' => $activities));
    //User has viewed the notifications so lets reset them.
    $this->Activities->resetUserNotifications($my_user['userid']);
    $this->load->view("print_json", $data);
  }
  
  function photo_view()
  {
  	$my_user = $this->session->userdata("logged_user");
	$status_id  = $this->uri->segment(2, 0);
	$data['status_id']= $status_id;
	$this->load->view("photo", $data);
  }
  
  function ajax_photo()
  {
  	$items = array();                      
  	$my_user = $this->session->userdata("logged_user");
  	$status_id = $this->input->post('status_id');
  	///print $status_id;                   
  	$items1 = $this->Photo->get_photo((int)$status_id, $my_user['userid']);
	/*
  	$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
	$replace = '<a href="'.site_url().'$1">$2</a>';
	if(isset($items[0]))
	{
		//$items[0]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $items[0]['message'])));
		$items[0]['strip_message'] = strip_tags($items[0]['message']);
		$items[0]['message'] = preg_replace($pattern, $replace, $items[0]['message']);
		
		$items[0]['message'] = stripslashes(htmlentities($items[0]['message']));
		$items[0]['time_ago'] = time_ago(strtotime($items[0]['publish_date']));
	}*/
	
	foreach($items1 as $item)
	{
	
	if($item['category'] == 'photo')
		{
		 $content_type = 'Photo';
		 $item['comments'] = $this->Statuses->getStatusReplies($item['id'],$content_type , $limit = 0, $offset = 10, $my_user['userid']);
     $totalComment = $this->Statuses->getStatusRepliesCount($item['id'],$content_type);
     $item['total_comments'] = $totalComment;
		 foreach($item['comments'] as $key2 => $comment)
		  {
			$item['comments'][$key2]['likes']=  $this->Statuses->getLike($comment['id'], $my_user['userid']);
			$item['comments'][$key2]['message']=  preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>',$comment['message']);
			$item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));
			
			$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
		  $replace = '<a href="'.site_url().'$1">$2</a>';
		  $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
		  }
		 $item['likes'] = $this->Photo->getLike($item['id'], $my_user['userid']);
		 
		 $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
		  $replace = '<a href="'.site_url().'$1">$2</a>';
		  $item['title'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['title'])));
		}
		
		$items[] = $item;
	}
		
  	$data['json'] = json_encode(array('feed' => $items));
    $this->load->view('print_json', $data);
  }
  
  function view()
  {
    $data['exits'] = 1;
  	$my_user = $this->session->userdata("logged_user");
	$status_id  = $this->uri->segment(2, 0);
	
	$items = $this->Statuses->getByFilters(array('id' =>(int)$status_id, 'parent_status_id' => 0));
	if(empty($items))
	{
	  $data['exits'] = 0;
	}
	$data['status_id']= $status_id;
	$this->load->view("status", $data);
  }
  
  function ajax_status()
  {
  	$items = array();                      
  	$my_user = $this->session->userdata("logged_user");
  	$status_id = $this->input->post('status_id');
  	///print $status_id;                   
  	$items = $this->Statuses->getStatus((int)$status_id, $my_user['userid']);
  	$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
	$replace = '<a href="'.site_url().'$1">$2</a>';
	if(isset($items[0]))
	{
		//$items[0]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $items[0]['message'])));
		$items[0]['strip_message'] = strip_tags($items[0]['message']);
		$items[0]['message'] = preg_replace($pattern, $replace, $items[0]['message']);
		
		$items[0]['message'] = stripslashes(htmlentities($items[0]['message']));
		$items[0]['time_ago'] = time_ago(strtotime($items[0]['publish_date']));
	}
  	$data['json'] = json_encode(array('feed' => $items));
    $this->load->view('print_json', $data);
  }
  
  function ajax_fetch_comment(){
    $items = array();
    $user_id = $this->input->post('user_id');
    if(!isset($user_id) || empty($user_id))
    {
      $user_id = '';
    }
    $status_id = $this->input->post('status_id');
    $content_type = $this->input->post('content_type');
    $items = $this->Statuses->getStatusReplies($status_id,$content_type, $limit = 0, $offset = 0, $user_id);
    if(isset($items) && !empty($items))
    {
      $data['json'] = json_encode(array('comments' => $items,'status'=>true));
    }
    else
    {
      $data['json'] = json_encode(array('status'=>false));
    }
    $this->load->view('print_json', $data);
  }

  /* Private function for Newcommer */

  function _newcommer_list() {
    $newcommer_show_limit = 3;
    $user_id = $this->session->userdata('userid');
    $db_newcommer = $this->general_model->gn_newcomers($user_id, $newcommer_show_limit); //General Model function for New Commers
    for ($i = 0; $i < count($db_newcommer); $i++) {
      $contact_user = $db_newcommer[$i]['contact_user'];
      $profile_pic = base_url() . "images/user-images/105x101/" . $db_newcommer[$i]['profile_pic'];
      if (!$db_newcommer[$i]['profile_pic']) {
        $db_newcommer[$i]['profile_pic'] = "default.jpg";
      } else if (!@fopen($profile_pic, "r")) {
        $db_newcommer[$i]['profile_pic'] = "default.jpg";
      }
      $id = $db_newcommer[$i]['id'];
      if ($contact_user) {
        $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="remove-contact">Remove from my contacts</a>';
      } else {
        $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="add-contact">Add to My Contacts</a>';
      }
    }
    return $db_newcommer;
  }

  /* Private function for Interest Groups */

  function _interest_group_list() {
    $show_limit = 3;
    $user_id = $this->session->userdata('userid');
    $db_interest_groups = $this->general_model->gn_intrest_groups($user_id, $show_limit); //General Model function for Interest Groups
    if (!count($db_interest_groups)) {
      $db_interest_groups = array();
    }
    return $db_interest_groups;
  }

  /* Private function for My Reminders */

  function _my_reminders() {
    $user_id = $this->session->userdata('userid');
    $time = time();
    $this->load->model('market_place/market_place_model');
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($user_id);
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($user_id);
    $db_event_reminders = $this->general_model->gn_my_event_reminders($user_id, $time);
    $my_reminders['count_item_sale'] = count($db_item_sale);
    $my_reminders['count_item_requested'] = count($db_item_requested);
    $my_reminders['count_event_reminders'] = count($db_event_reminders);
    return $my_reminders;
  }

//Default page for Status update
  function index() {
    redirect("status_update/following_people");
  }

  /* Prints all the users status message in the contact list of logged in a user */

  function mycontacts() {
    $user_email = $this->session->userdata('useremail');
    $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email);
    $data['gn_newcommer_list'] = $this->_newcommer_list(); //Private function Newcommer list
    $data['gn_interest_groups_list'] = $this->_interest_group_list(); //Private function Interest Group list
    $data['gn_my_reminders'] = $this->_my_reminders();
    $pagelimit = 0;
    //Active Tab
    $data['mycontacts_active'] = 'class="active"';
    $data['following_active'] = '';
    $data['all_people_active'] = '';
    $data['my_updates_active'] = '';
    $data['my_replies_active'] = '';
    //Breadcrumbs
    $data['breadcrumbs'] = 'My Contacts';
    $data['highlight_status_message'] = ""; //To check if the status message is new used only in My Updates
    $status_msg_list = $this->status_update_model->su_show_my_contacts($pagelimit);

    $data['messagelist'] = $status_msg_list;
    $db_msg_count = count($status_msg_list);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }

//Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $message = $data['messagelist'][$i]['message'];

      $formated_time = time_ago(strtotime($message_time)); //calling time ago helper
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $data['messagelist'][$i]['message_time'] = $formated_time;

      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }

    $my_reply_count = $this->status_update_model->su_get_my_reply_count();
    if ($my_reply_count) {
      $data['my_replies'] = '<span style="font-weight:bold;">My Replies (' . $my_reply_count . ')</span>';
    } else {
      $data['my_replies'] = '<span>My Replies</span>';
    }

    $your_status_msg = strip_tags($this->status_update_model->su_your_status());
    $data['yourstatus'] = (strlen($your_status_msg) > 70) ? substr($your_status_msg, 0, 70) . ".." : $your_status_msg;
    $this->load->view('template', $data);
  }

  /* Prints all the users status message whom the logged in a user is following */

  function following_people() {
    $user_email = $this->session->userdata('useremail');
    $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email);
    $data['gn_newcommer_list'] = $this->_newcommer_list(); //Private function Newcommer list
    $data['gn_interest_groups_list'] = $this->_interest_group_list(); //Private function Newcommer list
    $data['gn_my_reminders'] = $this->_my_reminders();
    $pagelimit = 0;
    //Active Tab
    $data['mycontacts_active'] = '';
    $data['following_active'] = 'class="active"';
    $data['all_people_active'] = '';
    $data['my_updates_active'] = '';
    $data['my_replies_active'] = '';
    //Breadcrumbs
    $data['breadcrumbs'] = 'Following';
    $data['highlight_status_message'] = ""; //To check if the status message is new used only in My Updates
    $status_msg_list = $this->status_update_model->su_show_following_people($pagelimit);
    $data['messagelist'] = $status_msg_list;
    $db_msg_count = count($status_msg_list);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }

    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $message = $data['messagelist'][$i]['message'];
      $formated_time = time_ago(strtotime($message_time)); //calling time ago helper
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }

    $my_reply_count = $this->status_update_model->su_get_my_reply_count();
    if ($my_reply_count) {
      $data['my_replies'] = '<span style="font-weight:bold;">My Replies (' . $my_reply_count . ')</span>';
    } else {
      $data['my_replies'] = '<span>My Replies</span>';
    }

    $your_status_msg = strip_tags($this->status_update_model->su_your_status());
    $data['yourstatus'] = (strlen($your_status_msg) > 70) ? substr($your_status_msg, 0, 70) . ".." : $your_status_msg;
    $this->load->view('template', $data);
  }

  /* Prints all the users status message in the system */

  function all_people() {
    $user_email = $this->session->userdata('useremail');
    $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email);
    $data['gn_newcommer_list'] = $this->_newcommer_list(); //Private function Newcommer list
    $data['gn_interest_groups_list'] = $this->_interest_group_list(); //Private function Newcommer list
    $data['gn_my_reminders'] = $this->_my_reminders();
    $pagelimit = 0;
    //Active Tab
    $data['mycontacts_active'] = '';
    $data['following_active'] = '';
    $data['all_people_active'] = 'class="active"';
    $data['my_updates_active'] = '';
    $data['my_replies_active'] = '';
    //Breadcrumbs
    $data['breadcrumbs'] = 'All People';
    $data['highlight_status_message'] = ""; //To check if the status message is new used only in My Updates
    $status_msg_list = $this->status_update_model->su_show_all_people($pagelimit);
    $data['messagelist'] = $status_msg_list;
    $db_msg_count = count($status_msg_list);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }
    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $message = $data['messagelist'][$i]['message'];
      $formated_time = time_ago(strtotime($message_time)); //calling time ago helper
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }

    $my_reply_count = $this->status_update_model->su_get_my_reply_count();
    if ($my_reply_count) {
      $data['my_replies'] = '<span style="font-weight:bold;">My Replies (' . $my_reply_count . ')</span>';
    } else {
      $data['my_replies'] = '<span>My Replies</span>';
    }

    $your_status_msg = strip_tags($this->status_update_model->su_your_status());
    $data['yourstatus'] = (strlen($your_status_msg) > 70) ? substr($your_status_msg, 0, 70) . ".." : $your_status_msg;
    $this->load->view('template', $data);
  }
//My updates page
  function my_updates() {
    $user_email = $this->session->userdata('useremail');
    $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email);
    $data['gn_newcommer_list'] = $this->_newcommer_list(); //Private function Newcommer list
    $data['gn_interest_groups_list'] = $this->_interest_group_list(); //Private function Newcommer list
    $data['gn_my_reminders'] = $this->_my_reminders();
    $pagelimit = 0;
    //Active Tab
    $data['mycontacts_active'] = '';
    $data['following_active'] = '';
    $data['all_people_active'] = '';
    $data['my_updates_active'] = 'class="active"';
    $data['my_replies_active'] = '';
    //Breadcrumbs
    $data['breadcrumbs'] = 'My Updates';
    $data['highlight_status_message'] = $this->session->userdata('new_status_message'); //If any updates are done highlight the updated status message.
    $this->session->unset_userdata('new_status_message');
    $status_msg_list = $this->status_update_model->su_show_my_updates($pagelimit);

    $data['messagelist'] = $status_msg_list;
    $db_msg_count = count($status_msg_list);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }
    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $message = $data['messagelist'][$i]['message'];
      $formated_time = time_ago(strtotime($message_time)); //calling time ago helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }

    $my_reply_count = $this->status_update_model->su_get_my_reply_count();
    if ($my_reply_count) {
      $data['my_replies'] = '<span style="font-weight:bold;">My Replies (' . $my_reply_count . ')</span>';
    } else {
      $data['my_replies'] = '<span>My Replies</span>';
    }

    $your_status_msg = strip_tags($this->status_update_model->su_your_status());
    $data['yourstatus'] = (strlen($your_status_msg) > 70) ? substr($your_status_msg, 0, 70) . ".." : $your_status_msg;
    $this->load->view('template', $data);
  }

  /* Prints all replies of the users status message */

  function my_replies() {
    $user_email = $this->session->userdata('useremail');

    $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email);
    $data['gn_newcommer_list'] = $this->_newcommer_list(); //Private function Newcommer list
    $data['gn_interest_groups_list'] = $this->_interest_group_list(); //Private function Newcommer list
    $data['gn_my_reminders'] = $this->_my_reminders();
    $pagelimit = 0;
    //Active Tab
    $data['mycontacts_active'] = '';
    $data['following_active'] = '';
    $data['all_people_active'] = '';
    $data['my_updates_active'] = '';
    $data['my_replies_active'] = 'class="active"';
    //Breadcrumbs
    $data['breadcrumbs'] = 'All People';
    $data['highlight_status_message'] = ""; //To check if the status message is new used only in My Updates
    $status_msg_list = $this->status_update_model->su_show_my_replies($pagelimit);
    //$data['messagelist'] = $status_msg_list;
    $db_msg_count = count($status_msg_list);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }
    //Customizing the Database field
    $data['messagelist'] = array();
    for ($i = 0; $i < count($status_msg_list); $i++) {

      $email = $status_msg_list[$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address

      $message_time = $status_msg_list[$i]['message_time'];
      $message = $status_msg_list[$i]['message'];

      $data['messagelist'][$i]['profile_pic'] = $status_msg_list[$i]['profile_pic'];
      $data['messagelist'][$i]['first_name'] = ucfirst(strtolower($status_msg_list[$i]['first_name']));
      $data['messagelist'][$i]['last_name'] = ucfirst(strtolower($status_msg_list[$i]['last_name']));
      $data['messagelist'][$i]['user_id'] = $status_msg_list[$i]['user_id'];
      $data['messagelist'][$i]['designation'] = $status_msg_list[$i]['designation'];
      $data['messagelist'][$i]['concept'] = $status_msg_list[$i]['concept'];
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $data['messagelist'][$i]['message_time'] = time_ago(strtotime($message_time)); //calling time ago helper
      $data['messagelist'][$i]['reply_user'] = $reply_user;
      $data['messagelist'][$i]['email'] = $status_msg_list[$i]['email'];
      $data['messagelist'][$i]['message_id'] = $status_msg_list[$i]['message_id'];
      $data['messagelist'][$i]['following_user_id'] = $status_msg_list[$i]['following_user_id'];

      $reply_id = $this->status_update_model->su_check_my_reply_message($status_msg_list[$i]['message_id']);
      if ($reply_id) {
        $data['messagelist'][$i]['highlight_status_message'] = 'highlight_status_message';
        $this->status_update_model->su_update_my_reply_status($reply_id);
      } else {
        $data['messagelist'][$i]['highlight_status_message'] = "";
      }
    }

    $my_reply_count = $this->status_update_model->su_get_my_reply_count();
    if ($my_reply_count) {
      $data['my_replies'] = 'My Replies (' . $my_reply_count . ')';
    } else {
      $data['my_replies'] = 'My Replies';
    }

    $your_status_msg = strip_tags($this->status_update_model->su_your_status());
    $data['yourstatus'] = (strlen($your_status_msg) > 70) ? substr($your_status_msg, 0, 70) . ".." : $your_status_msg;
    $this->load->view('my_reply_template', $data);
  }

  //Share button Action
  function submit_status_message() {
    if ($this->input->post('txtarShare')) {
      $status_message = $this->input->post('txtarShare');
      $status_message = substr($status_message, 0, 160);
      $status_message = nl2br($status_message);
      $this->load->library('form_validation');
      $this->form_validation->set_rules('txtarShare', 'Status Message', 'required|trim');
      if ($this->form_validation->run()) {
        $userId = $this->session->userdata('userid');
        $data = array('message' => $status_message, 'user_id' => $userId, 'message_time' => time(), "parent_message_id" => 0, "status" => 1); //Array of data
        $lastId = $this->status_update_model->su_submit_status_message($data);
        if ($lastId) {
          $this->session->set_userdata('new_status_message', "highlight_status_message");
          redirect("status_update/my_updates");
        }
      } else {
        redirect("status_update");
      }
    } else {
      redirect("status_update");
    }
  }
  function ajax_submit_photo() 
	{
  	$this->load->library('image_lib'); 
  	$user_id = $this->session->userdata('userid');
  		 //User_Photos_userid
	//setlocale(LC_ALL, "en_US.utf8")
	if ($this->input->post('photos') && $this->input->post('photos') != '' )
	{
		//$addPhoto = $this->input->post('photos');
        $addPhoto = $this->input->post('photos');
		$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
		//$replace = '<a href="'.site_url().'$1">$2</a>';
		preg_match_all($pattern, $addPhoto,$matches, PREG_SET_ORDER);
		if(isset($matches))
		{
			$tag_users = $matches;
		}
	} else {	 
        $addPhoto = $this->input->post('addPhoto');
	}
	$addPhoto = $this->intranet->filterInput($addPhoto);
	setlocale(LC_ALL, "en_US.utf8");
    $addPhoto = iconv("utf-8", "ascii//TRANSLIT",nl2br($addPhoto));
	  
  	$owner_id = isset($_POST['object_id'])? $this->input->post('object_id') : $user_id;
  	$owner_type = isset($_POST['object_type'])? $this->input->post('object_type') : 'User';
  	$target_id = isset($_POST['target_id'])? $this->input->post('target_id') : 0;
  	$target_type = isset($_POST['target_type'])? $this->input->post('target_type') : '';
  	if($target_type == 'Group'){
		$album_name = 'Group_Photos_'.$target_id;
  	} else if($target_type == 'Concept'){
		$album_name = 'Concept_Photos_'.$target_id;
  	} else {
		$album_name = 'User_Photos_'.$user_id;
	}
  	$count_album_slug = $this->manage_model->get_album_slug_count($album_name);
  	$album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath'));
  	$create_album_directory = str_replace(base_url(), '', $album_config['ecards_more_pic_target']) . $album_config['fullpath'];
  	$create_album_directory .= $album_name;
  	$alb_dir_path = str_replace("/system/", '', BASEPATH);
      //$create_album_directory = $alb_dir_path . '/public_html/phpalbum/albums/' . $album_name;
  	$create_album_directory = 'phpalbum/albums/' . $album_name;
  	
    $album = array(
      'title' => $album_name,
      'description' => $addPhoto,
      'visibility' => 1, //Always publish as daraft mode unless photos are uploaded
      'category' => 1,
      'owner' => $this->session->userdata('userid'),
      'keyword' => $album_name
    );
  			  
  	
  
  	if(is_dir($create_album_directory))
  	{
      $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));
      /* $this->session->set_userdata($album_config);
      $thumb_pfx = $album_config['thumb_pfx'];
      $thumb_height = $album_config['thumb_height'];
      $thumb_width = $album_config['thumb_width'];*/
      
      // $album_id = $this->manage_model->insert_album($album);
      $album_id = $this->Album->getIdByTitle($album_name);
      
      $config['upload_path'] = $create_album_directory;
      $config['max_size'] = '10240';
      $config['remove_spaces'] = true;
      $config['overwrite'] = false;
      
      $this->load->library('upload', $config);
      if($this->upload->do_upload('fileUploadFile'))
      {
      	$data = array('upload_data' => $this->upload->data());
      	 //$album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));
      		$picture = array(
      	'pid' => NULL,
      	'aid' => $album_id,
      	'filepath' => $album_name . '/',
      	'filename' => $data['upload_data']['file_name'],
      	'filesize' => $data['upload_data']['file_size'],
      	'owner_id' => $owner_id,
      	'owner_type' => $owner_type,
      	'target_id' => $target_id,
      	'target_type' => $target_type,
      	'ctime' =>  time(),
      	'mtime' =>date('Y-m-d H:i:s'),
      	'title' =>  $addPhoto
      );
      $picture_id = $this->manage_model->insert_picture($picture);
      
      $upload['status']= 'yes';
      $upload['upload'] = 'yes';
	  $upload['owner_type'] = $owner_type;
	  $upload['image_size'] = 'no';
      } else {
		$upload['status']= 'no';
      	$upload['upload'] = 'no';
		$upload['image_size'] = 'yes';
	  
	  }
  	}
  	else
  	{
      //print "not exist";
      $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));
      if (@mkdir($create_album_directory, 0777, true)) {
      //   print "dir created";
      	$album_id = $this->manage_model->insert_album($album);
      	$config['upload_path'] = $create_album_directory;
      	$config['max_size'] = '10240';
      	$config['remove_spaces'] = true;
      	$config['overwrite'] = false;
      
      	$this->load->library('upload', $config);
      	if($this->upload->do_upload('fileUploadFile'))
      	{
      	$data = array('upload_data' => $this->upload->data());
      	$picture = array(
      	'pid' => NULL,
      	'aid' => $album_id,
      	'filepath' => $album_name . '/',
      	'filename' => $data['upload_data']['file_name'],
      	'filesize' => $data['upload_data']['file_size'],
      	'owner_id' => $owner_id,
      	'owner_type' => $owner_type,
      	'target_id' => $target_id,
      	'target_type' => $target_type,
      	'ctime' => time(),
      	'mtime' =>date('Y-m-d H:i:s'),
      	'title' =>  $addPhoto
      	);
      	$picture_id = $this->manage_model->insert_picture($picture);
      	$upload['status']= 'yes';
		$upload['upload'] = 'yes';
		$upload['owner_type'] = $owner_type;
		$upload['image_size'] = 'no';
      } else {
		$upload['status']= 'no';
      	$upload['upload'] = 'no';
		$upload['image_size'] = 'yes';
	  }
		
      }
      else
      {
      	$upload['status']= 'no';
      	$upload['upload'] = 'no';
		$upload['image_size'] = 'no';
      //echo "dir not created";
      }
  	}
	
	
	if(isset($tag_users) && !empty($tag_users))
		{
			foreach($tag_users as $tag_user)
			{
				$tag_user_id = $tag_user[1];	
				$user_data = $this->User->getByUsername($tag_user_id);
					$this->Activities->userTagStatus($owner_id, $owner_type, 'Photo', $picture_id, $user_data['id']);
			}
		}
		
  	if(isset($data) && !empty($data))
    {
        $FileFullPath = site_url().$create_album_directory.'/'.$data['upload_data']['file_name'];
        $handlers = array(
          'jpg'  => 'imagecreatefromjpeg',
          'jpeg' => 'imagecreatefromjpeg',
          'png'  => 'imagecreatefrompng',
          'gif'  => 'imagecreatefromgif'
        );
        $imageCreators = array(
          'jpg'  => 'imagejpeg',
          'jpeg' => 'imagejpeg',
          'png'  => 'imagepng',
          'gif'  => 'imagegif'
        );
		$config2['image_library'] = 'gd2';
  			$config2['source_image']	= $data['upload_data']['full_path'];
  			$config2['maintain_ratio'] = TRUE;
  			/* $config['width']	 = $data['upload_data']['image_width'];
  			$config['height']	= $data['upload_data']['image_height']; */
            $config2['width']	 = 1024;//$thumb_width;
  			$config2['height']	= 768;//$thumb_height;
  			$this->image_lib->initialize($config2);
  			$this->image_lib->resize();
  			$this->image_lib->clear();
			
        $extension = $data['upload_data']['image_type'];
        if (($handler = $handlers[$extension]) && ($imageCreator = $imageCreators[$extension])){
          $im = $handler($FileFullPath);
          imageinterlace($im, true);
          $imageCreator($im, $create_album_directory.'/'.$data['upload_data']['file_name']);  
        }
        
            
  			
  
        if($data['upload_data']['image_width'] > 184 )
        {
        		$config1['image_library'] = 'gd2';
        		$config1['source_image']	= $data['upload_data']['full_path'];
        		$config1['maintain_ratio'] = TRUE;
        		$config1['create_thumb'] = TRUE;
        		$config1['width']	 = 184;
        		$config1['height']	= 122;
        		$config1['new_image']	= $album_config['thumb_pfx'].$data['upload_data']['file_name'];
        		$this->image_lib->initialize($config1);
        		$this->image_lib->resize();
        		$this->image_lib->clear();
        }
        else
        {
        		$config1['image_library'] = 'gd2';
        		$config1['source_image']	= $data['upload_data']['full_path'];
        		$config1['maintain_ratio'] = TRUE;
        		$config1['create_thumb'] = TRUE;
        		$config1['width']	 = $data['upload_data']['image_width'];
        		$config1['height']	= $data['upload_data']['image_height'];
        		$config1['new_image']	= $album_config['thumb_pfx'].$data['upload_data']['file_name'];
        		$this->image_lib->initialize($config1);
        		$this->image_lib->resize();
        		$this->image_lib->clear();
        }
        $upload['url'] = $create_album_directory.'/'.$album_config['thumb_pfx'].$data['upload_data']['file_name'];
        $upload['big_image_url'] = $create_album_directory.'/'.$data['upload_data']['file_name'];
        $upload['id'] = $picture_id;
    }
    else
    {
      $upload['status']= 'no';
      $upload['upload'] = 'no';
    }
	$upload['title'] = $addPhoto;
    $temp['json'] = json_encode($upload);
    echo $temp['json'];
	}

  //Share button Action Ajax function Calls -- the dynamic show of status message without page reloading and highlighting new status message etc.
  function ajax_submit_status_message() {
    if ($this->input->post('txtPost')) {

     /* print_r($_POST);
      die();*/

	if ($this->input->post('tag_text') && $this->input->post('tag_text') != '' )
	{
		$status_message = $this->input->post('tag_text');
		$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
		//$replace = '<a href="'.site_url().'$1">$2</a>';
		preg_match_all($pattern, $status_message,$matches, PREG_SET_ORDER);
		if(isset($matches))
		{
			$tag_users = $matches;
		}
	} else {
		$status_message = $this->input->post('txtPost');
	}
     $status_message = $this->intranet->filterInput($status_message);
      $parent_status_id = $this->input->post('parent_status_id');
	  $parent_type = $this->input->post('parent_type');
	  if($parent_type == '')
	  {
		$parent_type = 'Status';
	  }

      $commented_id = $this->input->post('object_id');
      $object_type = $this->input->post('object_type') == '' ? 'User' : $this->input->post('object_type');
      $target_id = $this->input->post('target_id');
      $target_type = $this->input->post('target_type');
	  $concept = $this->input->post('concept');
	  $concept_manager = $this->input->post('concept_manager');

      //$status_message = substr($status_message, 0, 160);
	  setlocale(LC_ALL, "en_US.utf8");
      $status_message = iconv("utf-8", "ascii//TRANSLIT",nl2br($status_message));
      $this->load->library('form_validation');
      $this->form_validation->set_rules('txtPost', 'Status Message', 'required|trim');
      if ($this->form_validation->run()  or 1) {
        
        $userId = $this->session->userdata('userid');

        if($parent_status_id == '' && $commented_id != '')
        {
          $object_id = $commented_id;
        } else {
				if($concept_manager == 1 && $concept != 0)
				{
					$object_id = $concept;
					$object_type = 'Concept';
				}
				else
				{
					$object_id = $userId;
				}
        }
        
        $data = array(
                'message' => $status_message, 
                'object_type' => $object_type, 
                'object_id' => $object_id, 
                'create_dttm' => date('Y-m-d H:i:s'), 
                "parent_status_id" => $parent_status_id, 
				"parent_type" => $parent_type,
                "status" => 1
                ); //Array of data

        if($target_id != '')
        {
          $data['target_id'] = $target_id;
          $data['target_type'] = $target_type;
          //$this->Activities->commentUserStatus($userId, $commented_id, $parent_status_id);
        }


        /*print_r($data);
        die();*/

        if($parent_status_id != '' && $userId != $commented_id)
        {
        	//this is a comment but not a wall post
        	//So adding a notification
        	$this->Activities->commentUserStatus($userId, $commented_id, $parent_status_id);
        }


        /*print_r($data);
        die();*/


        $lastId = $this->status_update_model->su_submit_status_message($data);
		
		if(isset($tag_users) && !empty($tag_users))
		{
			foreach($tag_users as $tag_user)
			{
				$tag_user_id = $tag_user[1];	
				$user_data = $this->User->getByUsername($tag_user_id);
				if(isset($parent_status_id) && $parent_status_id != '')
				{
					$this->Activities->userTagStatus($object_id, $object_type, $parent_type, $parent_status_id, $user_data['id']);
				}
				else
				{
					$this->Activities->userTagStatus($object_id, $object_type, $parent_type, $lastId, $user_data['id']);
				}
			}
		}
        $json_arr = array();
        $json_arr['your_status'] = $status_message;
        $json_arr['time'] = time_ago(time());
        $json_arr['id'] = $lastId;
        $json_arr['data'] = "";
        $json_arr['update'] = 1;
		$last_data = $this->Statuses->get($lastId);
		if(isset($last_data[0]))
		{
			$json_arr['details']= $last_data[0];
		}else
		{
			$json_arr['details']= $last_data;
		}
        $data['json'] = json_encode($json_arr);
      } else {
        $json_arr = array();
        $json_arr['your_status'] = "";
        $json_arr['data'] = "";
        $json_arr['update'] = 0;
        $data['json'] = json_encode($json_arr);
      }
    } else {
      $json_arr = array();
      $json_arr['your_status'] = "";
      $json_arr['data'] = "";
      $json_arr['update'] = 0;
      $data['json'] = json_encode($json_arr);
    }
    $this->load->view("print_json", $data);
  }
  
    function ajax_featured_post() {	
	$response = array('status' => 'failed');
	$val=0;
		if ($this->input->post('post_id')) {
		$post_id = $this->input->post('post_id');
		$post_type = $this->input->post('post_type');
		$value = $this->input->post('value');
		//echo $value;exit;
		if($value == 'Mark as featured')
		{	 
		$val=1;
		 if($this->Statuses->setFeaturedComment($post_id,$post_type,$val))
		 {        
		  $response['value'] = 'Featured';
		  $response['status'] = 'success';
		 }

		}
		else
		{
		$val=0;		
		if( $this->Statuses->setFeaturedComment($post_id,$post_type,$val))
		{
		$response['value'] = 'Mark as featured';
		$response['status'] = 'success';
		}

		}
	  
		$data['json'] = json_encode($response);
		$this->load->view("print_json", $data);		  
		  }	  
	  }

  function ajax_like_post() {
    $response = array('status' => 'failed');
    if ($this->input->post('post_id')) {
      $post_id = $this->input->post('post_id');
	  $post_type = $this->input->post('post_type');
      $value = $this->input->post('value');
      $liker_user_id = $this->session->userdata('userid');
      $liked_user_id = $this->input->post('object_id');
      if($value == 'Like')
      {
        if($this->Statuses->addLike($post_id, $post_type, $liker_user_id) )
        {
        	if($liker_user_id != $liked_user_id)
        	{
        		//no notifications if you like your status yourself
            	$this->Activities->likeUserStatus($liker_user_id, $liked_user_id, $post_id, $post_type);
            }
          $response['value'] = 'Unlike';
          $response['status'] = 'success';
        }

      }
      else
      {
        if( $this->Statuses->removeLike($post_id, $post_type, $liker_user_id) )
        {
          $response['value'] = 'Like';
          $response['status'] = 'success';
        }

      }
	  if($post_type == 'Status')
	  {
		$likes = $this->Statuses->get((int)$post_id);
		$response['likes'] = $likes[0]->total_likes;
	  }
	  else if($post_type == 'Photo')
	  {
		$likes = $this->Photo->get((int)$post_id);
		$response['likes'] = $likes[0]['total_likes'];
	  }
	  else if($post_type == 'Albums')
	  {
		$likes = $this->Album->get((int)$post_id);
		$response['likes'] = $likes['total_likes'];
	  }
	  else if($post_type == 'Announcement' || $post_type == 'Offers' || $post_type == 'News')
	  {
		$likes = $this->NewsEntries->get_all_data((int)$post_id);
		$response['likes'] = $likes[0]->total_likes;
	  }
	  else if($post_type == 'Marketplace')
	  {
		$likes = $this->Item->get((int)$post_id);
		$response['likes'] = $likes[0]->total_likes;
	  }
	  else if($post_type == 'Files')
	  {
		$likes = $this->File->get((int)$post_id);
		$response['likes'] = $likes['total_likes'];
	  }
	  else if($post_type == 'Widget')
	  {
		$likes = $this->Widget->getWidget((int)$post_id);
		$response['likes'] = $likes[0]['total_likes'];
	  }

    }
    $data['json'] = json_encode($response);
    $this->load->view("print_json", $data);
  }
  
	//view all users liked
	function other_users_liked(){
		if ($this->input->post('post_id')) {
			$post_id = $this->input->post('post_id');
			$post_type = $this->input->post('post_type');
			$value = $this->input->post('value');
			$liker_user_id = $this->session->userdata('userid');
			$liked_user_id = $this->input->post('object_id');
			
			$su_get_like_view_all = $this->status_update_model->su_get_like_view_all($post_id,$post_type);
			$data['json'] = json_encode($su_get_like_view_all);
			$this->load->view("print_json", $data);
		}
	}

//Following user json output
  function follow_user() {
    $follow_user = $this->input->post('follow_user');
    $affected_row = $this->status_update_model->su_follow_user($follow_user);
    $data['json'] = '{affected_row : "' . $affected_row . '"}';
    $this->load->view("print_json", $data);
  }
//Unfollow user json output
  function unfollow_user() {
    $unfollow_user = $this->input->post('unfollow_user');
    $affected_row = $this->status_update_model->su_unfollow_user($unfollow_user);
    $data['json'] = '{affected_row : "' . $affected_row . '"}';
    $this->load->view("print_json", $data);
  }
//Delete status message
  function delete_status_message() {
    $my_user = $this->session->userdata("logged_user");
    $status_id = $this->input->post('status_id');
    $affected_row = $this->Statuses->deleteStatus($status_id, $my_user['userid']);
    $response = array('status' => 'failed');
    if($affected_row > 0)
    {
      $response['status'] = 'success';
    } 
    $data['json'] = json_encode($response);
    $this->load->view("print_json", $data);
  }
  
  function delete_poll() {
    $my_user = $this->session->userdata("logged_user");
    $poll_id = $this->input->post('poll_id');
    $affected_row = $this->Polls->delete_poll($poll_id);
    $response = array('status' => 'failed');
    if($affected_row)
    {
      $response['status'] = 'success';
    } 
    $data['json'] = json_encode($response);
    $this->load->view("print_json", $data);
  }
  
  function delete_status_photo(){
    $my_user = $this->session->userdata("logged_user");
    $photo_id = $this->input->post('photo_id');
    $photo_data = $this->Photo->get((int)$photo_id);
    $affected_row = $this->Photo->deletePhoto($photo_id,$my_user['userid']);
    if($affected_row > 0)
    {
      $response['status'] = 'success';
      @unlink('phpalbum/albums/'.$photo_data[0]['filepath'].$photo_data[0]['filename']);
    }
    $data['json'] = json_encode($response);
    $this->load->view("print_json", $data);
  }
  
  function delete_photo_album(){
    $my_user = $this->session->userdata("logged_user");
    $album_id = $this->input->post('album_id');
    $photo_data = $this->AlbumPicture->getAlbumPictures((int)$album_id);
	foreach($photo_data as $album)
	{
		@unlink('phpalbum/albums/'.$album['filepath'].$album['filename']);
		@unlink('phpalbum/albums/'.$album['filepath'].'thumb_'.$album['filename']);
		$this->Photo->deletePhoto((int)$album['pid'],$my_user['userid']);
	}
	$album_dir = substr($album['filepath'], 0, -1);
	@rmdir('phpalbum/albums/'.$album_dir);
    $affected_row = $this->Album->remove($album_id);
    if($affected_row > 0)
    {
      $response['status'] = 'success';
    }
    $data['json'] = json_encode($response);
    $this->load->view("print_json", $data);
  }
  
  function delete_status_noa(){
    $my_user = $this->session->userdata("logged_user");
    $noa_id = $this->input->post('noa_id');
    $noa_data = $this->Entry->get((int)$noa_id);
	if(!empty($noa_data))
	{
		$affected_row = $this->Entry->remove((int)$noa_id);
		if($affected_row > 0)
		{
		  $response['status'] = 'success';
		}
	}
	else
	{
		$response['status'] = 'error';
	}
    $data['json'] = json_encode($response);
    $this->load->view("print_json", $data);
  }

  function add_prompt() {
   $details = array(
        'id' => NULL,
        'user_id' => $_POST['user_id'],
        'feature' => $_POST['feature'],
        'status' => 1
    );
     $prompt_id = $this->Prompt->add($details);
    
    /*$follow_user = $this->input->post('follow_user');
    $affected_row = $this->status_update_model->su_follow_user($follow_user);
    $data['json'] = '{affected_row : "' . $affected_row . '"}';
    $this->load->view("print_json", $data);*/
  }
  //Load More pagination
  function pagination() {
    $load_page = $this->input->post("page");
    $offset = $this->input->post("pagesize");
    //$data['messagelist'] = $this->status_update_model->$load_page($offset);
    $status_msg_list = $this->status_update_model->$load_page($offset);
    $data['messagelist'] = $status_msg_list;
    $db_msg_count = count($status_msg_list);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $load_more = true;
    } else {
      $load_more = false;
    }

    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $formated_time = time_ago(strtotime($message_time)); //calling time ago helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }
    $user_msg = "";
    foreach ($data['messagelist'] as $msg) {
      if ($this->session->userdata('userid') == $msg['user_id']) {
        $user_follow = '<li><a class="msg-delete" id="' . $msg['message_id'] . '" href="#">Delete</a></li>';
      } elseif ($this->session->userdata('userid') != $msg['user_id']) {
        $status_class = ($msg['following_user_id']) ? "msg-unfollow" : "msg-follow";
        $status_msg = ($msg['following_user_id']) ? "Unfollow" : "Follow";
        $user_follow = '<li><a rel="pop_win" class="msg-report" href="' . site_url("profile/report_status_update/" . $msg['message_id']) . '">Report</a></li>
        <li><a class="' . $status_class . '" rel="' . $msg['user_id'] . '" href="#">' . $status_msg . '</a></li>';
      }

      if ($load_page == "su_show_my_replies") {
        $reply_id = $this->status_update_model->su_check_my_reply_message($msg['message_id']);
        if ($reply_id) {
          $highlight = 'highlight_status_message';
          $this->status_update_model->su_update_my_reply_status($reply_id);
        } else {
          $highlight = "";
        }
      } else {
        $highlight = "";
      }

      $user_msg .= '<li class="type2 hentry ' . $highlight . '">
                    <div class="frame">
                      <span class="visual">
                        <img src="' . base_url() . 'images/user-images/105x101/' . $msg['profile_pic'] . '" alt="' . $msg['first_name'] . " " . $msg['last_name'] . '" width="53" height="53" />
                      </span>
                      <div class="text">
                        <strong class="entry-title"><a href="' . site_url("profile/view_profile/" . $msg["user_id"]) . '">' . ucfirst(strtolower($msg['first_name'])) . " " . ucfirst(strtolower($msg['last_name'])) . '</a><em>' . $msg['designation'] . ", " . $msg['concept'] . '</em></strong>
                        <p>' . text_to_link($msg['message']) . '</p>
                        <span>' . $msg['message_time'] . '</span>
                        <ul class="actions">
                          <li><a class="msg-reply" id="' . $msg['reply_user'] . '" href="#">Reply</a></li>
                          <li><a href="mailto:' . $msg['email'] . '">Email</a></li>
                          ' . $user_follow . '
                        </ul>
                      </div>
                    </div>
                </li>';
    }

    $my_reply_count = $this->status_update_model->su_get_my_reply_count();
    if ($my_reply_count) {
      $my_replies_cnt = 'My Replies (' . $my_reply_count . ')';
    } else {
      $my_replies_cnt = 'My Replies';
    }



    $json_arr = array();
    $json_arr['load_more'] = $load_more;
    $json_arr['load_page'] = $load_page;
    $json_arr['my_replies_cnt'] = $my_replies_cnt;
    $json_arr['data'] = $user_msg;
    $data['json'] = json_encode($json_arr);


    $this->load->view("print_json", $data);
    //$this->load->view('show_messages',$data);
  }
  
  function MakeUrls($str)
	{
		$find=array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si','`((?<!//)(www\.\S+[[:alnum:]]/?))`si');
		$replace=array('a href="$1" target="_blank">$1</a>','<a href="http://$1"    target="_blank">$1</a>');
		return preg_replace($find,$replace,$str);
	}
	
	function widget_load_ajax_data() {
	$item_id        = $this->input->post('item_id');
	$my_user     = $this->session->userdata("logged_user");
	
	$feed = $this->Widget->getWidgetajax($item_id, $my_user['userid']);
	$items = array();
  	foreach($feed as $key => $item)
  	{
		$content_type = 'Widget';
       
      //  $item['photos'] = $this->AlbumPicture->getAlbumPictures($item['id'], 3, 0);
		$item['comments'] = $this->Statuses->getStatusReplies($item['id'],$content_type , $limit = 0, $offset = 10, $my_user['userid']);
        $totalComment = $this->Statuses->getStatusRepliesCount($item['id'],$content_type);
        $item['total_comments'] = $totalComment;
		foreach($item['comments'] as $key2 => $comment)
		  {
			$item['comments'][$key2]['likes']=  $this->Statuses->getLike($comment['id'], $my_user['userid']);
			$item['comments'][$key2]['message']=  preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>',$comment['message']);
			$item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));
			
			$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
		    $replace = '<a href="'.site_url().'$1">$2</a>';
		    $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
		  }
		  
		  $item['likes'] = $this->Item->getLike($item['id'], $my_user['userid']);
		  $item['time_ago'] = time_ago(strtotime($item['publish_date']));

     // $db_data = $this->Item->getItem($item['id']);
     // $item['item_type'] = $db_data[0]['item_type'];

		  $items[] = $item;
	}
	$data['json'] = json_encode(array('feed' => $items));
    $this->load->view('print_json', $data);
  
  }

}

/* End of file status_update.php */
/* Location: ./system/application/status_update/controllers/status_update.php */