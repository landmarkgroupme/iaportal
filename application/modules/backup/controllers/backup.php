<?php

class backup extends MX_Controller {
  function __construct() {
    parent::__construct();
   // $this->load->model('backup_model');
  $this->load->model('User');
  $this->load->model('Role');
  }
  
  function index() {
    
  }
  function backup_listing() {
    
    $user_id = $this->session->userdata("userid");
    $profile = $this->User->getUserProfile($user_id);
    $this->load->model('UserPermission');
    
    if(empty($_GET['cron'])) {
      if($profile['role_id'] != 1) {
        redirect(site_url());
      }    
      if (!$user_id) { 
        redirect(site_url());
      } 
    }
    $errror_list = array();
    $sucess_list = array();
    $this->config->load('intranet',TRUE);
    $database_details = $this->config->item('db');
    
    define('DATABASE_HOST',$database_details['hostname']);
    define('DATABASE_USERNAME',$database_details['username']);
    define('DATABASE_PASSWORD',$database_details['password']);
    define('DATABASE_NAME',$database_details['database']);
    define('LIVE_BACKUP_PATH',$database_details['backup_path']);
    
    $time = date('d-m-Y',time());
     if(!empty($_GET['del'])) {
      shell_exec('rm '.LIVE_BACKUP_PATH.'/'.$_GET['del'],$result);
      $errror_list[] = 'File Deleted sucessfully';
        
     }elseif(!empty($_GET['bck'])){
      if((empty($database_details['hostname'])) || (empty($database_details['username'])) || (empty($database_details['password'])) || (empty($database_details['database'])) || (empty($database_details['backup_path'])) ) {
        
        $error_list[] = 'Mysql Credentials Missing.';
      }else{
        
        shell_exec('mysqldump -u'. DATABASE_USERNAME .' -p'. DATABASE_PASSWORD .' -h'. DATABASE_HOST . ' ' . DATABASE_NAME .' | gzip -v >'. LIVE_BACKUP_PATH. '/'.$time.'.tar.gz');
          $sucess_list[] = 'Database backup done Sucessfully.';
          //shell_exec('php /home/softwares/rackspace/tests/sync_db.php');
          //$sucess_list[] = 'File Uploaded on Rackspace server';
      }
    }
    
    $file_list_details = array_diff(scandir(LIVE_BACKUP_PATH), array('..', '.'));
    $previos_date = date('d-m-Y', strtotime('-7 days'));

    foreach($file_list_details as $file) {
      if($file < $previos_date) {
        exec('rm '.LIVE_BACKUP_PATH.'/'.$file,$result);
      }
    }
    $file_list = array_diff(scandir(LIVE_BACKUP_PATH), array('..', '.'));
    
    $data['file_list'] = $file_list;
    $data['errors'] = $errror_list;
    $data['sucess'] = $sucess_list;
    $data['username'] = $this->session->userdata('fullname');
    $data['cron'] = (!empty($_GET['cron'])) ? $user_id : '';
    $this->load->view("backup_listing_page",$data);
  }
}
