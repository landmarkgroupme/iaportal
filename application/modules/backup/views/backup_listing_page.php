<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/mprofile/all.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/user_update/misc.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css">
    
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <!--<script type="text/javascript" src="=//base_url();?>js/custom_js/manage/manage.js"></script>-->
    <script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/weekly_export.js"></script>
    <script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    
    <style type="text/css">
    .thumb-span {background:#860e9c;padding:2px 4px;text-transform:uppercase;color:#fff;-moz-border-radius:4px; border-radius: 4px;font-weight:bold;}
    </style>
</head>
<body>
    <?php if((!empty($username))) : ?>
<div class="wrapper">
    
  <?php
        $data['active_tab'] = 'backup_listing';
        $data['user_name'] = $username;
        $this->load->view('includes/admin_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Database Backup Listing</h1>
        </div>
        <?php if(!empty($sucess)):?>
        <div class="msg-ok">
           <ul>
            <?php foreach($sucess as $suc) :?>
            <li>
                <?php print($suc); ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <?php if(!empty($errors)):?>
        <div class="msg-error">
            <ul>
            <?php foreach($errors as $error) :?>
            <li>
                <?php print($error); ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <div class="clear"></div>
        <div>
        </div>
        
        <div class="clear"></div>
          <a href="<?php echo site_url('backup/backup_listing').'/?bck=1'; ?>" class="myButton">Backup Database</a>
          <table id="dbTable" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tsbhead">Name</th>
                    <th class="tsbhead">Download</th>
                    <th class="tsbhead">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($file_list as $file_name): ?>
                <tr>
                    <td><?php print $file_name; ?></td>
                    <td><a href="<?php print site_url().'/db_backup/'.$file_name; ?>" target="_blank" download>Download</a></td>
                    <td><a href="<?php echo site_url('backup/backup_listing').'/?del='.$file_name; ?>">Delete</a></td>
                    <td></td>
                </tr>
                <?php endforeach; ?>
          </table>
    </div>
</div>
<script type="text/javascript">
//General Search Field
var base_url = "/";
$(document).ready(function(){
    
    var dbTable = $('#dbTable').dataTable();
});
//function general_search_form(){
// 
//  //search when clikc on link
//  $('#submit_src').live('click',function(){
//    $('#quick_search_frm').submit();
//  })
//
//  //Hover background change
//  $('#src_user_list li div').live('mouseover',function(){
//    $('#src_user_list li div').removeClass('hover');
//    $(this).addClass('hover')
//    return false;
//  })
//  //Hide search list on main mouse out
//  $('#src_user_list').blur(function(){
//    $('#search_element').hide();
//  })
//
//  //User click
//  $('#src_user_list li').live('click',function(){
//    $('#search_element').show();
//    var user_profile = this.id
//    window.location = user_profile;
//    return false;
//  })
//  //Type event
//  var tmp_cnt = 0;// arrow key navigation count
//  $('#txt_general_search').keyup(function(e){
//
//    if(e.keyCode == 27){
//      $('#search_element').hide();
//      return false;
//    }
//   
//    var src_term =  $('#txt_general_search').val();
//    var src_from =  '';
//    if($('#search_from')) 
//    {
//      src_from = $('#search_from').val(); 
//    }
//    if(src_term.length < 3){
//      return false;
//    }
//   
//
//    var size_li = $("#src_user_list li").size();
//
//    if(e.keyCode == 38){
//      if(tmp_cnt <=0){
//        tmp_cnt =  size_li;
//      }else{
//        tmp_cnt = tmp_cnt-1
//      }
//      //$('#src_user_list li div').css('background-color','inherit')
//      //$('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
//      $('#src_user_list li div').removeClass('hover')
//      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
//      return false;
//    }else if(e.keyCode == 40){
//      if(size_li == tmp_cnt){
//        tmp_cnt = 0;
//      }else{
//        tmp_cnt = tmp_cnt+1
//      }
//      //$('#src_user_list li div').css('background-color','inherit')
//      // $('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
//      $('#src_user_list li div').removeClass('hover')
//      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
//      return false;
//    }
//
//
//
//    $('#search_element').show();
//    $('#src-loader').show();
//
//    $.ajax({
//      url: base_url+"people/quick_search_sphinx",
//      data: {
//        "src_term":src_term,
//        'src_from': src_from
//      },
//      async: true,
//      dataType: "json",
//      type: "POST",
//      success: function(data){
//        $('#src-loader').hide();
//        if(data['show']){
//          //alert(e.keyCode);
//          $('#src_user_list').html(data['result']);
//          $('#submit_src').html(data['result_count']);
//          $('#search_element').show();
//        //alert($('#src_user_list li:first').html());
//        }else{
//          $('#src_user_list').html(data['result']);
//          $('#submit_src').html('');
//          $('#search_element').show();
//        }
//      }
//    });
//
//    return false;
//  })
//
//
//  //Focus
//  $("#txt_general_search").focus(function(){
//    var src_term =  $("#txt_general_search").val();
//    if( $.trim(src_term) == 'Search by name..'){
//      $("#txt_general_search").val('');
//    }
//    $(this).css('color','#000000');
//  })
//  //Blur
//  $("#txt_general_search").blur(function(){
//    var src_term =  $("#txt_general_search").val();
//    if( $.trim(src_term) == ''){
//      $("#txt_general_search").val('Search by name..');
//    }
//    $(this).css('color','#747474');
//  })
//}
//general_search_form();
</script>
<?php endif; ?>
</body>
</html>