<?php

class About extends General {

  private $mt_category = "policies"; //This for MT database "mt_category->category_basename" For Policies category

  function __construct() {
    parent::__construct();

    $this->load->model('about_model');
	 $this->load->model('Album');
	 $this->load->model('Statuses');
	  $this->load->model('AlbumPicture');
  }

  function index() {
    redirect("about/photos");
  }

  //Photo Albums
  function photos() {
    $selected_concept = ($this->uri->segment(3, 0)) ? $this->uri->segment(3, 0) : 0;
    if ($selected_concept <= 0) {
      $album_category = "c.cid <> '' ";
    } else {
      $album_category = "c.cid = '" . $selected_concept . "' ";
    }
    //Concepts
    $db_concept = $this->about_model->alb_get_all_concepts();
    $concept_filter = '';
    $concept_filter = '<option selected="yes" value="0">Landmark galleries</option>';
    foreach ($db_concept as $concept_details) {
      $id = $concept_details['cid'];
      $name = $concept_details['name'];
      $val = $id;
      if ($selected_concept == $id) {
        $concept_filter .= '<option selected="yes" value="' . $val . '">' . $name . '</option>';
      } else {
        $concept_filter .= '<option value="' . $val . '">' . $name . '</option>';
      }
    }
    $data['concept_filters'] = $concept_filter;
    $db_data = $this->about_model->alb_get_all_album($album_category, 20, 0);
    if (count($db_data)) {
      for ($i = 0; $i < count($db_data); $i++) {
		$check_path = $db_data[$i]['sub_dir'] . $db_data[$i]['filepath'] . $db_data[$i]['thumb'] . $db_data[$i]['filename'];
        $thumbnail = $db_data[$i]['image_path'] . $db_data[$i]['sub_dir'] . $db_data[$i]['filepath'] . $db_data[$i]['thumb'] . $db_data[$i]['filename'];
        $thumbnail = str_replace(" ", "%20", $thumbnail);

        if (!@fopen($thumbnail, "r")) {
		//print
          $thumbnail = base_url() . "images/album.jpg";
        }
        $alb_arr[$i]['thumbnail'] = $thumbnail;
        $alb_arr[$i]['link'] = base_url() . "about/view_photos/" . $db_data[$i]['aid'];
        $alb_arr[$i]['title'] = $db_data[$i]['title'];
        $alb_arr[$i]['ctime'] = date("d M, Y", $db_data[$i]['ctime']);
        $alb_arr[$i]['total_pic'] = '<span class="pic-count media-show"> ' . $db_data[$i]['total_pic'] . ' Photos</span><span class="pic-count media-hide">| ' . $db_data[$i]['total_pic'] . ' Photos</span>';
      }
    } else {
      //No Albums found
      $thumbnail = base_url() . "images/album.jpg";
      $alb_arr[0]['thumbnail'] = $thumbnail;
      $alb_arr[0]['link'] = "#";
      $alb_arr[0]['title'] = "No Albums Found";
      $alb_arr[0]['ctime'] = "";
      $alb_arr[0]['total_pic'] = "";
    }
    $data['album'] = $alb_arr;
    //echo "<xmp>".print_r($alb_arr,1)."</xmp>";exit;
    $this->load->view("new_photos_template", $data);
  }
  
  function photos_ajax() { 
	$concept = $this->input->get('concept');
	$limit = $this->input->get('limit');
  	$offset = $this->input->get('start');
	
	if ($concept <= 0) {
      $album_category = "c.cid <> '' ";
    } else {
      $album_category = "c.cid = '" . $concept . "' ";
    }
	$alb_arr = array();
   $db_data = $this->about_model->alb_get_all_album($album_category, $limit, $offset);
    if (count($db_data)) {
      for ($i = 0; $i < count($db_data); $i++) {
		$check_path = $db_data[$i]['sub_dir'] . $db_data[$i]['filepath'] . $db_data[$i]['thumb'] . $db_data[$i]['filename'];
        $thumbnail = $db_data[$i]['image_path'] . $db_data[$i]['sub_dir'] . $db_data[$i]['filepath'] . $db_data[$i]['thumb'] . $db_data[$i]['filename'];
        $thumbnail = str_replace(" ", "%20", $thumbnail);

        if (!@fopen($thumbnail, "r")) {
		//print
          $thumbnail = base_url() . "images/album.jpg";
        }
        $alb_arr[$i]['thumbnail'] = $thumbnail;
        $alb_arr[$i]['link'] = base_url() . "about/view_photos/" . $db_data[$i]['aid'];
        $alb_arr[$i]['title'] = $db_data[$i]['title'];
        $alb_arr[$i]['ctime'] = date("d M, Y", $db_data[$i]['ctime']);
        $alb_arr[$i]['total_pic'] = '<span class="pic-count">| ' . $db_data[$i]['total_pic'] . ' Photos</span>';
      }
    } 
    $data['album'] = $alb_arr;
	$data['json'] = json_encode($data['album']);
  	$this->load->view("print_json", $data);
  }

  //Inside Album
  function view_photos() {
    $alb_id = $this->uri->segment(3, 0);
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'about/view_photos/' . $alb_id;
    $config['total_rows'] = $this->about_model->alb_get_count_all_album_pics($alb_id);
    $config['per_page'] = 12;
    $config['num_links'] = 2;
    $config['uri_segment'] = 4;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class = 'active'><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0)+1 : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);

    $this->pagination->initialize($config);
    //$db_data = $this->about_model->alb_get_all_album_pics($alb_id, $config['per_page'], $this->uri->segment(4, 0));
	//echo "<pre>";print_r($db_data);echo "<br>";
	$all_data = $this->about_model->alb_get_all_album_pics_no_limits($alb_id);
	//echo "<pre>";print_r($all_data);echo "<br>";
	//$diff_data = array_diff_key($all_data,$db_data);
	//echo "<pre>";print_r($diff_data);exit;
	$data['navlinks'] = $this->pagination->create_links();
/*	print_r($db_data);
			die(); */
		if(empty($all_data))
		{
		redirect('locator/notfound');
		}
		else
		{
			//echo "<xmp>".print_r($db_data,1)."</xmp>";exit;
			$j=0;
			foreach($all_data as $i=>$pic) {
			  $thumbnail = $all_data[$i]['image_path'] . $all_data[$i]['sub_dir'] . $all_data[$i]['filepath'] . $all_data[$i]['thumb'] . $all_data[$i]['filename'];
			  $image = $all_data[$i]['image_path'] . $all_data[$i]['sub_dir'] . $all_data[$i]['filepath'] . $all_data[$i]['filename'];
			  $pic_arr[$j]['thumbnail'] = $thumbnail;
			  $pic_arr[$j]['image'] = $image;
			  $pic_arr[$j]['title'] = $all_data[$i]['title'];
			  $album_title = $all_data[$i]['album_title'];
			  $album_desc = $all_data[$i]['album_desc'];
			  $album_time = $all_data[$i]['ctime'];
			  $j++;
			}
			$data['album_title'] = $album_title;
			$data['album_desc'] = $album_desc;
			$data['album_time'] = $album_time;
			$data['album'] = $pic_arr;
			$data['album_id'] = $alb_id;
			//$i=0;$pic_arr=array();
			//echo "<pre>";print_r($diff_data);exit;
/* 			foreach($diff_data as $pic){
				$thumbnail = $pic['image_path'] . $pic['sub_dir'] . $pic['filepath'] . $pic['thumb'] . $pic['filename'];
				$image = $pic['image_path'] . $pic['sub_dir'] . $pic['filepath'] . $pic['filename'];
				$pic_arr[$i]['thumbnail'] = $thumbnail;
				$pic_arr[$i]['image'] = $image;
				$pic_arr[$i]['title'] = $pic['title'];
				$i++;
			} */
			//$data['album_rest'] = $pic_arr;
			/*print "<pre>";
			print_r($data);
			exit;*/
			$this->load->view("new_view_photos_template", $data);
		}
  }
  
  function view_photos_ajax_data() {
  $album_id        = $this->input->post('album_id');
	$my_user     = $this->session->userdata("logged_user");
	
	$feed = $this->Album->getAlbumData($album_id, $my_user['userid']);
	$items = array();
  	foreach($feed as $key => $item)
  	{
		$content_type = 'Albums';
       
      //  $item['photos'] = $this->AlbumPicture->getAlbumPictures($item['id'], 3, 0);
		$item['comments'] = $this->Statuses->getStatusReplies($item['id'],$content_type , $limit = 0, $offset = 10, $my_user['userid']);
        $totalComment = $this->Statuses->getStatusRepliesCount($item['id'],$content_type);
        $item['total_comments'] = $totalComment;
		foreach($item['comments'] as $key2 => $comment)
		  {
			$item['comments'][$key2]['likes']=  $this->Statuses->getLike($comment['id'], $my_user['userid']);
			$item['comments'][$key2]['message']=  preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>',$comment['message']);
			$item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));
			
			$pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
		    $replace = '<a href="'.site_url().'$1">$2</a>';
		    $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
		  }
		  $item['likes'] = $this->Album->getLike($item['id'], $my_user['userid']);
		   $items[] = $item;
	}
	$data['json'] = json_encode(array('feed' => $items));
    $this->load->view('print_json', $data);
  
  }

  function policies() {
    $mt_category = $this->mt_category;
    $db_data = $this->about_model->policies_get_details($mt_category);

    if (count($db_data)) {
      $cnt = 0;
      $policy_array = array();
      foreach ($db_data as $details) {
        $category = $details['category'];
        $category_id = $details['category_id'];
        $entry_id = $details['entry_id'];
        $entry_title = $details['entry_title'];
        $entry_text = $details['entry_text'];
        $entry_modified_time = strtotime($details['entry_modified_on']);
        $blog_main_url = $details['site_url'];
        $blog_url = $details['blog_url'];
        $asset_path = $details['asset_file_path'];

        $blog_url = str_replace("/::/", $blog_main_url, $blog_url);
        $asset_link = str_replace("%r\\", $blog_url, $asset_path);
        $asset_link = str_replace("\\", "/", $asset_link);


        $policy_anchor[$category] = '<a href="#' . $category . '">' . $category . '</a>';

        $category = "<a name='" . $category . "' >" . $category . "</a>"; //Anchor text

        $policy_array[$category][$entry_id]['entry_title'] = $entry_title;
        $policy_array[$category][$entry_id]['entry_text'] = $entry_text;
        $policy_array[$category][$entry_id]['entry_modified_time'] = date("d M Y", $entry_modified_time);
        $policy_array[$category][$entry_id]['asset_link'][] = ($asset_link) ? '<a target="_blank" href="' . $asset_link . '" class="download-link">Download</a>' : "";
      }
    }
    $data['policy_anchor'] = @implode(" | ", $policy_anchor);
    $data['policies'] = $policy_array;
    //echo "<xmp>".print_r($policy_array,1)."</xmp>";exit;
    $this->load->view("policies_template", $data);
  }

}

/* End of file news.php */
/* Location: ./system/application/controllers/news.php */