<?php

/*
 * alb stands for Album
 * */

Class About_model extends CI_Model {

  function About_model() {
    parent::__construct();
  }

  //Get all Concpets from Album category table
  function alb_get_all_concepts() {
    $this->db->order_by("name", "Asc");
    $result = $this->db->get('alb_categories');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//Count Album 	
  function alb_get_count_all_album($arg) {
    $sql = "select
							count(a.aid) as total_album,
							a.aid,
							(select count(b.pid)  FROM alb_pictures b WHERE a.aid = b.aid GROUP BY a.aid) as pic_count
						FROM
							alb_albums a,
							alb_categories c
						WHERE 
							a.visibility = 0 AND
							a.category = c.cid AND
							" . $arg . "
							group by 
							a.aid HAVING count(pic_count)>0";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//All Album Detiails	
  function alb_get_all_album($arg, $limit, $offset) {
    $limit = (int) $limit;
    $offset = (int) $offset;
    $sql = "SELECT
							a.aid,
							a.title,
							b.ctime,
							b.filepath,
							count(pid) as total_pic,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb,
							(SELECT filename FROM alb_pictures WHERE pid = a.thumb) as filename
							
						FROM 
							alb_albums a,
							alb_pictures b,
							alb_categories c
						WHERE 
							visibility = 0 AND
							a.aid = b.aid	AND
							a.category = c.cid AND
							" . $arg . "
						GROUP BY b.aid 
						ORDER BY b.ctime DESC LIMIT ? offset ?";
    $result = $this->db->query($sql, array($limit, $offset));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//Album Detiails	
  function alb_get_album($album_id) {
    $sql = "SELECT
							a.aid,
							a.title,
							b.ctime,
							b.filepath,
							count(pid) as total_pic,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb,
							(SELECT filename FROM alb_pictures WHERE pid = a.thumb) as filename
							
						FROM 
							alb_albums a,
							alb_pictures b,
							alb_categories c
						WHERE 
							visibility = 0 AND
							a.aid = b.aid	AND
							a.category = c.cid AND
							a.aid = '?'
						GROUP BY b.aid ";
    $result = $this->db->query($sql, array($album_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//Count Pics in Album 	
  function alb_get_count_all_album_pics($alb_id) {
    $sql = "SELECT
							count(pid) as total_count 
						FROM 
							alb_pictures
						WHERE 
							aid = ?";
    $result = $this->db->query($sql, $alb_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row['total_count'];
      }
    }
    $result->free_result();
    return $data;
  }

//Get all Pics in Album with limits 	
  function alb_get_all_album_pics($alb_id, $limit, $offset) {
    $alb_id = (int) $alb_id;
    $limit = (int) $limit;
    $offset = (int) $offset;
    $sql = "SELECT
							a.title as album_title,
							a.description as album_desc,
							b.filepath,
							b.filename,
							b.ctime,
							b.pid,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb,
							(SELECT value FROM alb_config WHERE name = 'normal_pfx') as normal,
							b.title
						FROM 
							alb_albums a,
							alb_pictures b						
						WHERE 
							a.visibility = 0 AND
							a.aid = ?	AND
							a.aid = b.aid	
						ORDER BY b.ctime DESC LIMIT ? offset ?";
						
    $result = $this->db->query($sql, array($alb_id, $limit, $offset));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[$row['pid']] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Get all Pics in Album 
  function alb_get_all_album_pics_no_limits($alb_id) {
    $alb_id = (int) $alb_id;
    //$limit = (int) $limit;
    //$offset = (int) $offset;
    $sql = "SELECT
							a.title as album_title,
							a.description as album_desc,
							b.filepath,
							b.filename,
							b.ctime,
							b.pid,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb,
							(SELECT value FROM alb_config WHERE name = 'normal_pfx') as normal,
							b.title
						FROM 
							alb_albums a,
							alb_pictures b						
						WHERE 
							a.visibility = 0 AND
							a.aid = ?	AND
							a.aid = b.aid	
						ORDER BY b.ctime";
						
    $result = $this->db->query($sql, array($alb_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[$row['pid']] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Policies
  function policies_get_details($mt_category) {
    $sql = "SELECT
							b.category_label as category,
							b.category_id as category_id,
							d.entry_id,
							d.entry_title,
							d.entry_text,
							d.entry_modified_on,
							f.asset_file_path,
							g.blog_site_url as blog_url,
							h.blog_site_url as site_url
						FROM 
							mt_category AS a
							LEFT JOIN  mt_category AS b ON b.category_parent = a.category_id
							LEFT JOIN mt_placement AS c ON b.category_id = c.placement_category_id
							LEFT JOIN mt_entry AS d ON d.entry_id = c.placement_entry_id
							LEFT JOIN mt_objectasset AS e ON e.objectasset_object_id = c.placement_entry_id
							LEFT JOIN mt_asset AS f ON f.asset_id = e.objectasset_asset_id
							LEFT JOIN mt_blog AS g ON g.blog_id = b.category_blog_id
							LEFT JOIN mt_blog AS h ON g.blog_parent_id = h.blog_id
						WHERE 
							a.category_basename = ? AND
							
							d.entry_title <> ''
							";

    $result = $this->db->query($sql, array($mt_category));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

}