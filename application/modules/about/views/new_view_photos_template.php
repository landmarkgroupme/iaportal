<!DOCTYPE html>
<html>
  <head>
    <meta name="robots" content="index, nofollow">
    <title>Landmark Group</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <?php $this->load->view('include_files/common_includes_new'); ?>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/colorbox.css" />
    <link rel="stylesheet" media="all" href="<?=base_url()?>media/css/galleria-folio.css">
  </head>
  <body class="full-width">
    
      <?php $this->load->view('global_header.php'); ?>

    <div class="section wrapper clearfix">
      <h2>Photos</h2>
       <ul class="breadcrumb">
          <li><a href="<?php echo site_url(); ?>">Home</a></li>
            <li><span>&gt;&gt;</span></li>
        <li><a href="<?php echo site_url(); ?>photos">Photos</a></li>
            <li><span>&gt;&gt;</span></li>
            <li><?php echo $album_title; ?></li>
        </ul>
    </div>

    <div id="wrapper" class="section wrapper clearfix about-gallery">
      <div class="left-contents">
        <!-- recently added products -->
        <div id="main">
              <!-- content -->
          <div id="content">
              <div class="heading-box">
                  <h2><?=$album_title?></h2>
              </div>
           <div class="container" id="photo-inner">
             <div class="details-attr">
                <div class="media-hide">
                <div class="details-attr-title">Date: </div>
                <div class="details-attr-res"><?=date("l, d M, Y",$album_time)?></div>
                <div class="details-attr-title">Time: </div>
                <div class="details-attr-res"><?=date("H:i",$album_time)?></div>
                </div>
                <div class="media-show">
                <div class="photo-left">
                <div class="details-attr-title">Date: </div>
                <div class="details-attr-res"><?=date("l, d M, Y",$album_time)?></div>
                </div>
                <div class="photo-right">
                <div class="details-attr-title">Time: </div>
                <div class="details-attr-res"><?=date("H:i",$album_time)?></div>
                </div>
                </div>
              </div>
              
              <div class="event-link">
                
              </div>
              <p><?=$album_desc?></p>
              <!--<div class="gallery-top-pagination">
                <div class="gallery-paging-div">
                  <div class="pagination-footer-list">
                    <ul>
                      <?=$navlinks?>
                    </ul>
                  </div>
                </div>
                <span>Showing <strong><?=$limit?></strong> - <strong><?php echo $total_count;// $off = ($offset <= $total_count ? $offset:$total_count); ?></strong> of <strong><?=$total_count?></strong> Photos</span>
              </div>-->
            <div class="section-main gallery-group js-gallery-group" id="js-gallery-rooms">
                <!-- photo container -->
                <div class="gallery-group-photo-container">
                    <div class="gallery-trigger">
                      <?php for($i=0;$i<count($album);$i++){ ?>
                        <a class = "fancybox view_photos" rel ="view_photos" href="<?= $album[$i]['image']?>" title="<?= $album[$i]['title']?>">
                            <img
                                src="<?= $album[$i]['thumbnail']?>",
                                data-big="<?= $album[$i]['image']?>"

                            >
                        </a>
                        <?php } ?>
                    </div>
                </div><!-- // photo container -->
            </div><!-- // gallery group -->
			<div id="recent-posts">

			</div>
            </div>
            <!--  <div class="pagination-footer">
              <div class="pagination-footer-count">Showing <strong><?=$limit?></strong> - <strong><?php echo $total_count;//$off = ($offset <= $total_count ? $offset:$total_count); ?></strong> of <strong><?=$total_count?></strong> Photos</div>
              
              <div class="pagination-footer-list">
                <ul>
                  <?=$navlinks?>
                </ul>
              </div>
              
              <div class="clear"></div>
            </div>-->
          </div>
        </div>	
      </div>
    </div> <!-- section -->

    <?php $this->load->view('global_footer.php'); ?>
    <?php $this->load->view('partials/js_footer'); ?>
	<?php $this->load->view('templates/js/album_updates'); ?>
	<?php $this->load->view('partials/interaction_js'); ?>
    <script type="text/javascript" src="<?=base_url();?>js/colorbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/about/view_photo.js"></script>
    <script src="<?=base_url()?>media/js/galleria/galleria-1.3.5.min.js"></script>
    <script>

        // Load the classic theme
        Galleria.loadTheme('<?=base_url();?>media/js/galleria/galleria.folio.min.js');
        // Initialize Galleria
        Galleria.run('.gallery-trigger');

    </script>
	
	<script type="text/javascript" language="javascript">
		var album_id = <?php echo $album_id; ?>;
		$(document).ready(function(){
		loadUpdates1();
		 $('#recent-posts .rp-contents .rp-social').css({'margin':'20px 0 0 50px'});
  });
  
  function loadUpdates1()
{
	$('#recent-posts div.loader-more').show();
	var service_url = siteurl+"about/view_photos_ajax_data";
	var request_data = {};
console.log(service_url);
		request_data = {'album_id': album_id};
	//filter by concept
	if (typeof feedtype != 'undefined')
	{
		request_data.feedtype = feedtype;
		request_data.feedtype_id = feedtype_id;
	}


	/*console.log(request_data);
	return;*/

	$.ajax({
		url: service_url,
		data: request_data,
		async: false,
		dataType: "json",
		type: "POST",
		cache: false,
		success: function(response){
		if(response.feed.length == 0 &&  start == 0)
		{
				$('#recent-posts div.loader-more').before('<dl class="block-2 clearfix no-feed-found"><dt><dd><h2>No feed found!</h2></dd></dt></dl>');        
		}
			console.log(response);
			_.templateSettings.variable = "rc";

			// Grab the HTML out of our template tag and pre-compile it.
			var template = _.template(
				$( "script.template" ).html()
			);
			//$('#recent-posts ul.filter li').removeClass('active');
			//$(parent).addClass('active');
			//$('#recent-posts ul.filter').append(template(response));
			$('#recent-posts').append(template(response));

			},
		beforeSend: function(){
			loadProgress = true;
			},
		complete: function(response){
			loadProgress = false;
			$('#recent-posts div.loader-more').hide();
			}
		   

	});

}
  
  </script>
  </body>
</html>