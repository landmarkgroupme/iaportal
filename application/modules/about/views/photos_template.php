<?php
//Active Navigation Page
$active_tab['photos_active'] = 'class="active"';
  $active_tab['home_active'] = '';
  $active_tab['people_active'] = '';
  $active_tab['market_place_active'] = '';
  $active_tab['files_active'] = '';
  $active_tab['events_active'] = '';
  $active_tab['about_lmg_active'] = '';
  $active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Landmark Intranet</title>
  <?php $this->load->view("include_files/common_files");?>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/about/photo.js"></script>
</head>
<body>
 
  <!-- wrapper -->
  <div id="wrapper" class="about-gallery">
   <?php $this->load->view("includes/admin_nav");?>
    <!-- logo -->
    <h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
    <!-- main -->
    <div id="main">
      <!-- content -->
      <div id="content">
        <!-- breadcrumbs -->
        <ul class="breadcrumbs">
          <li><a href="<?=site_url("about/photos/")?>">Photos</a></li>
        </ul>
        <!-- heading -->
        <div class="heading-box">
          <h2>Photos</h2>
        </div>
        <!-- Gallery List -->
        <div class="container">
          <div class="gallery-top-pagination">
            <div class="gallery-concept">
              <form id="frm_concept_filter" action="" method="get">
                <label for="cmb_concept">Select Concept</label>
                <select id="cmb_concept" name="cmb_concept">
                  <?=$concept_filters?>
                </select>
              </form>
            </div>
            <span>Showing <strong><?=$limit?></strong> - <strong><?=$offset?></strong> of <strong><?=$total_count?></strong> Photo Albums</span>
          </div>
          <div class="album-holder">
            <?php $cnt=1; for($i=0;$i<count($album);$i++): ?>
             
              <div class="album">
                <a href="<?=$album[$i]['link']?>"><img src="<?= $album[$i]['thumbnail']?>" alt="album" width="184" height="110" />
                </a>
                <div class="album-title"><?=$album[$i]['title']?></div>
                <div class="album-attr"><span class="album-date"><?=$album[$i]['ctime']?></span>  <?=$album[$i]['total_pic']?>  </div>
              </div>
               
             
           
            <?php endfor;?>
                        
          </div><!-- /Album Holder -->
          <div class="pagination-footer">
            <div class="pagination-footer-count">Showing <strong><?=$limit?></strong> - <strong><?=$offset?></strong> of <strong><?=$total_count?></strong> Photo Albums</div>
            <div class="pagination-footer-list">
              <ul>
                <?=$navlinks?>
              </ul>
            </div>
            <div class="clear"></div>
          </div>
          
        </div>
      </div>
      
    </div>
    <!-- header -->
    <div id="header">
      <div class="header-holder">
        <!-- navigation -->
        <?php $this->load->view("includes/navigation",$active_tab);?>
        <div class="head-bar">
          <!-- sub navigation -->
          <ul class="subnav">
            <li class="active"><a href="<?=base_url()?>index.php/about/photos" tabindex="15"><span>Photos</span></a></li>
          </ul>
          <!-- header search form -->
          <?php $this->load->view("includes/search_form");?>
        </div>
      </div>
    </div>
    <!-- footer -->
    <?php $this->load->view("includes/footer");?>
  </div>
</body>
</html>