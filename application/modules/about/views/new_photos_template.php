
<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title>Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">
	
	<?php $this->load->view('global_header.php'); ?>

<div class="section wrapper clearfix">
	<h2>Photos</h2>
	 <ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Photos</li>
    </ul>
</div>

<div class="section wrapper clearfix about-gallery ">
	<div class="left-contents">
		<!-- recently added products -->

		<div class="container">
		    <div class="gallery-top-pagination">
            <div class="gallery-concept">
              <form id="frm_concept_filter" action="" method="get">
                <label for="cmb_concept">Select Concept</label>
                <select id="cmb_concept" name="cmb_concept">
                  <?=$concept_filters?>
                </select>
              </form>
            </div>
          </div>
          <div class="album-holder">
		  
            <?php $cnt=1; for($i=0;$i<count($album);$i++): ?>
              <div class="album">
                <a href="<?=$album[$i]['link']?>"><div class="album-container">
				<img src="<?= $album[$i]['thumbnail']?>" alt="album"/></div>
                </a>
                <div class="album-title"><?=substr($album[$i]['title'], 0, 22).'...';?></div>
                <div class="album-attr"><span class="album-date"><?=$album[$i]['ctime']?></span>  <?=$album[$i]['total_pic']?>  </div>
              </div>
            <?php endfor;?>
                        
          </div><!-- /Album Holder -->
        
			
		</div>
				
	</div>
			

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('templates/js/photo_album'); ?>
<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
var start = 20;
var limit = 10;
var not_end = true;
$(document).ready(function() {

$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			if(not_end)
			{
				start += 10;
				loadPhotos({'noclear': 1, 'scroll': true});
			}
		}
	});
	
	
	$('#cmb_concept').change(function(){
		start = 0;
		not_end = true;
		loadPhotos({'noclear': 0});
	});
	
});

function loadPhotos(prop)
{
	//console.log(prop);
	//return;
	$('.loader-more').show();
	
var searched = ( $('#cmb_concept').val() != 0) ? true : false;

	if(!prop.noclear)
	{
		$('div.album-holder').html('');
		
	}


	var service_url = siteurl+"about/photos_ajax";

	

	if(searched)
	{
		
		var concept = $('#cmb_concept').val();
		var data = {'limit' : limit, 'start' : start, 'concept': concept};
		
	} 
	else {
	
	var search = $('#txtSearch').val();
		var data = {'limit' : limit, 'start' : start};
		
	}



     $.ajax({
      url: service_url,
      data: data,
      async: false,
	  cache: false,
      dataType: "json",
      type: "GET",
      success: function(data){
	  console.log(data);

      	if(_.size(data) > 0)
      	{
      		$('.no-result-block').addClass('d-n');

      		_.templateSettings.variable = "rc";

	        // Grab the HTML out of our template tag and pre-compile it.
	        var template = _.template(
	            $( "#photo_album_card" ).html()
	        );

	        $('div.album-holder').append(template(data));

      	} else {
      		not_end = false;
			if(start == 0)
			{
				$('div.album-holder').append('<div class="no-result-block">No album found.</div>');
			}
      	}

		$('.loader-more').hide();
      }
    });
	
}


</script>

</body>
</html>