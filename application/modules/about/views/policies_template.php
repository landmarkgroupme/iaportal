<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = 'class="active"';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
</head>
<body>
	
	<!-- wrapper -->
	<div id="wrapper" class="about-gallery">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="#">About Landmark</a></li>
					<li>Policies</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>Policies</h2>
				</div>
				<!-- Gallery List -->
				<div class="container">
					<p>The Landmark Group, founded in 1973 with a single store in Bahrain has grown into one of the largest retail conglomerates in the Middle East and is expanding rapidly in India. It currently operates over 900 stores. In addition to its retail sector, the Group has also diversified into leisure, food, hotels and electronics and has created a comprehensive infrastructure including its own logistics and distribution division, to support its retail operations and other businesses.</p>
					<div class="anchor-text">
						<?=$policy_anchor?>
					</div>
					
					<?php foreach($policies as $category=>$policies_articles):?>
					<div class="policies">
						<h3><?=$category?></h3>
						<?php foreach($policies_articles as $policy):?>
						<div class="policy-details">
							<h4><?=$policy['entry_title']?></h4>
							<p><?=$policy['entry_text']?></p>
							<div class="policy-footer">
								<?=@implode(" | ",$policy['asset_link']);?>
								<span><em>Last Updated <?=$policy['entry_modified_time']?></em></span>
							</div>
						</div>
						<?php endforeach;?>
						 
					
					</div>
					<?php endforeach;?>
					 
					
				</div>
			</div>
			
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li><a href="<?=base_url()?>index.php/about/photos" tabindex="15"><span>Photos</span></a></li>
						<li class="active"><a href="<?=base_url()?>index.php/about/policies" tabindex="16"><span>Policies</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>