<?php
//Active Navigation Page
$active_tab['photos_active'] = 'class="active"';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/colorbox.css" />
	<script type="text/javascript" src="<?=base_url();?>js/colorbox/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/about/view_photo.js"></script>
</head>
<body>
	
	<!-- wrapper -->
	<div id="wrapper" class="about-gallery">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="#">About Landmark</a></li>
					<li><a href="<?=base_url()."index.php/about/"?>">Photos</a></li>
					<li><?=$album_title?></li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?=$album_title?></h2>
				</div>
				<!-- Gallery List -->
				<div class="container">
					<div class="details-attr">
						<div class="details-attr-title">Date: </div>
						<div class="details-attr-res"><?=date("l, d M, Y",$album_time)?></div>
						<div class="details-attr-title">Time: </div>
						<div class="details-attr-res"><?=date("H:i",$album_time)?></div>
						<!--div class="details-attr-title">Location: </div>
						<div class="details-attr-res">Jebel Ali, Dubai</div-->
					</div>
					
					<div class="event-link">
						
					</div>
					<p><?=$album_desc?></p>
					<div class="gallery-top-pagination">
						<div class="gallery-paging-div">
							<div class="pagination-footer-list">
								<ul>
									<?=$navlinks?>
								</ul>
							</div>
						</div>
						<span>Showing <strong><?=$limit?></strong> - <strong><?=$offset?></strong> of <strong><?=$total_count?></strong> Photos</span>
					</div>
					<div class="album-holder">
						<?php $cnt=1; for($i=0;$i<count($album);$i++): ?>
						<?php if($cnt==1):?>
						<div class="album-row">
						<?php endif;?>
							<div class="album photo-listing">
								<a rel="view_photos" href="<?= $album[$i]['image']?>" title="<?= $album[$i]['title']?>"><img src="<?= $album[$i]['thumbnail']?>" alt="<?= $album[$i]['title']?>" />
								</a>
							</div>
						<?php  if($cnt==4):  $cnt=0;?>
						</div>
						<?php elseif($i==count($album)-1):$cnt=0;?>
						</div>
						<?php endif; $cnt++;?>
						<?php endfor;?>
												
					</div><!-- /Album Holder -->
					<div class="pagination-footer">
						<div class="pagination-footer-count">Showing <strong><?=$limit?></strong> - <strong><?=$offset?></strong> of <strong><?=$total_count?></strong> Photos</div>
						<div class="pagination-footer-list">
							<ul>
								<?=$navlinks?>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=base_url()?>index.php/about/photos" tabindex="15"><span>Photos</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>