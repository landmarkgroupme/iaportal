<?php $review_date = date("Ymd"); 
      $nl = "\n";

?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>HRMS Admin</title>
    
    <link rel="stylesheet" href="importer/css/screen.css" type="text/css" media="screen" title="default" />
    <!--[if IE]>
    <link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
    <![endif]-->

    <!--  jquery core -->
    <script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <!--  checkbox styling script -->
    <script src="js/jquery/ui.core.js" type="text/javascript"></script>
    <script src="js/jquery/ui.checkbox.js" type="text/javascript"></script>
    <script src="js/jquery/jquery.bind.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function(){
      $('input').checkBox();
      $('#toggle-all').click(function(){
      $('#toggle-all').toggleClass('toggle-checked');
      $('#mainform input[type=checkbox]').checkBox('toggle');
      return false;
      });
    });
    </script>

    <![if !IE 7]>

    <!--  styled select box script version 1 -->
    <script src="js/jquery/jquery.selectbox-0.5.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.styledselect').selectbox({ inputClass: "selectbox_styled" });
    });
    </script>


    <![endif]>

    <!--  styled select box script version 2 -->
    <script src="js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
      $('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
    });
    </script>

    <!--  styled select box script version 3 -->
    <script src="js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" });
    });
    </script>
    <!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
    <script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $(document).pngFix( );
    });
    </script>
  </head>
  <body>
    <!-- Start: page-top-outer --><!-- End: page-top-outer -->

    <form action="<?php echo site_url(); ?>new_user_update/update_user_bwlist" method="POST">

      <div class="clear">&nbsp;</div>

      <!--  start nav-outer-repeat................................................................................................. START --><!--  start nav-outer-repeat................................................... END -->

      <div class="clear"></div>

      <!-- start content-outer ........................................................................................................................START -->
      <div id="content-outer">
      <!-- start content -->
      <div id="content">
        <!--  start page-heading -->
        <div id="page-heading">
          <h1 class="header-01">&nbsp;</h1>
          <h1 class="header-01">High Confidence:</h1>
        </div>
        <!-- end page-heading -->

        <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
          <tr>
            <th rowspan="3" class="sized"><img src="importer/images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
            <th class="topleft"></th>
            <td id="tbl-border-top">&nbsp;</td>
            <th class="topright"></th>
            <th rowspan="3" class="sized"><img src="importer/images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
          </tr>
          <tr>
            <td id="tbl-border-left"></td>
            <td>
              <!--  start content-table-inner ...................................................................... START -->
              <div id="content-table-inner">

                <!--  start table-content  -->
                <div id="table-content">

                  <!--  start product-table ..................................................................................... -->
                  <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                    <tr>
                      <th width="89" class="table-header-repeat line-left minwidth-1"><a href="">HRMS ID</a></th>
                      <th width="230" class="table-header-repeat line-left minwidth-1"><a href=""> Name</a>	</th>
                      <th width="259" class="table-header-repeat line-left"><a href="">Email</a></th>
                      <th width="118" class="table-header-repeat line-left"><a href="">Email Score</a></th>
                      <th width="62" class="table-header-repeat line-left"><a href="">Remove</a></th>
                    </tr>

                    <?php
                    /**************************************************
                              HIGH CONFIDENCE RESULTS
                    **************************************************/
                   
                    $color1 = 1;
                    foreach($whitelist as $email_for_import) {
                      
                        if($color1 % 2) { echo '<tr class="alternate-row">'; } else { echo '<tr>'; }
                        echo $nl;
                        echo '<td>'.$email_for_import['hrmsid'].'</td>';
                        echo $nl;
                        echo '<td>'.$email_for_import['name'].'</td>';
                        echo $nl;
                        echo '<td>'.$email_for_import['email'].'</td>';
                        echo $nl;
                        echo '<td>'.$email_for_import['points'].'</td>';
                        echo $nl;
                        echo '<td><input name="blacklist[]" value="'.urlencode($email_for_import['email']).'|'.$email_for_import['hrmsid'].'" type="checkbox"/></td>';
                        echo $nl;
                        echo '</tr>';
                        echo $nl;

                        $color1++;
                      
                    }
                    ?>
          
                  </table>
                  <!--  end product-table................................... -->
                </div>
                <!--  end content-table  -->
                <!--  start actions-box ............................................... -->
                <!-- end actions-box........... -->


                <div class="clear"></div>

              </div>
              <!--  end content-table-inner ............................................END  -->
            </td>
            <td id="tbl-border-right"></td>
          </tr>
          <tr>
            <th class="sized bottomleft"></th>
            <td id="tbl-border-bottom">&nbsp;</td>
            <th class="sized bottomright"></th>
          </tr>
        </table>
        <div class="clear">&nbsp;</div>
      </div>
      <!--  end content -->

      <!--  start new content -->
      <div id="content">

        <!--  start page-heading -->
        <div id="page-heading">
          <h1 class="header-01">Low Confidence:</h1>
        </div>
        <!-- end page-heading -->

        <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
          <tr>
            <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
            <th class="topleft"></th>
            <td id="tbl-border-top">&nbsp;</td>
            <th class="topright"></th>
            <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
          </tr>
          <tr>
            <td id="tbl-border-left"></td>
            <td>
                <!--  start content-table-inner ...................................................................... START -->
                <div id="content-table-inner2">

                  <!--  start table-content  -->
                  <div id="table-content">

                    <!--  start product-table ..................................................................................... -->
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                    <tr>
                      <th width="83" class="table-header-repeat line-left minwidth-1"><a href="">HRMS ID</a></th>
                      <th width="180" class="table-header-repeat line-left minwidth-1"><a href=""> Name</a>	</th>
                      <th width="231" class="table-header-repeat line-left"><a href="">Email</a></th>
                      <th width="102" class="table-header-repeat line-left"><a href="">Email Score</a></th>
                      <th width="87" class="table-header-repeat line-left"><a href="">Whitelist</a></th>
                      <th width="77" class="table-header-repeat line-left"><a href="">Ignore </a></th>
                      </tr>

                  <?php
                  /**************************************************
                            LOW CONFIDENCE RESULTS
                  **************************************************/
                  
                  $color2 = 1;
                  foreach($blacklist as $email_for_import) {
                      if($color2 % 2) { echo '<tr class="alternate-row">'; } else { echo '<tr>'; }
                      echo $nl;
                      echo '<td>'.$email_for_import['hrmsid'].'</td>';
                      echo $nl;
                      echo '<td>'.$email_for_import['name'].'</td>';
                      echo $nl;
                      echo '<td>'.$email_for_import['email'].'</td>';
                      echo $nl;
                      echo '<td>'.$email_for_import['points'].'</td>';
                      echo $nl;
                      echo '<td><input name="whitelist[]" value="'.urlencode($email_for_import['email']).'|'.$email_for_import['hrmsid'].'" type="checkbox"/></td>';
                      echo $nl;
                      echo '<td><input name="ignore[]" value="'.urlencode($email_for_import['email']).'" type="checkbox"/></td>';
                      echo $nl;
                      echo '</tr>';
                      echo $nl;

                      $color2++;
                  }
                  ?>
                    </table>
                    <!--  end product-table................................... -->
         
                </div>
                <!--  end content-table  -->
                <!--  start paging..................................................... -->
                <div id="actions-box"> <input type="submit" name="action" value="Update" class="action-slider">
                  <div class="clear"></div>
                </div>
                <!--  end paging................ -->

                <div class="clear"></div>

              </div>
              <!--  end content-table-inner ............................................END  -->
            </td>
            <td id="tbl-border-right"></td>
          </tr>
          <tr>
            <th class="sized bottomleft"></th>
            <td id="tbl-border-bottom">&nbsp;</td>
            <th class="sized bottomright"></th>
          </tr>
        </table>
        <div class="clear">&nbsp;</div>

      </div>
     </form>
    <!-- start footer -->
    <div id="footer">
      <!--  start footer-left -->
      <div id="footer-left">HRMS 2.0 &copy; Copyright Landmark Group. All rights reserved.</div>
      <!--  end footer-left -->
      <div class="clear"></div>
    </div>
    <!-- end footer -->
  </body>
</html>