<?php $review_date = date("Ymd"); 
      $nl = "\n";

?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>HRMS Admin</title>
    
    <link rel="stylesheet" href="importer/css/screen.css" type="text/css" media="screen" title="default" />
    <!--[if IE]>
    <link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
    <![endif]-->

    <!--  jquery core -->
    <script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <!--  checkbox styling script -->
    <script src="js/jquery/ui.core.js" type="text/javascript"></script>
    <script src="js/jquery/ui.checkbox.js" type="text/javascript"></script>
    <script src="js/jquery/jquery.bind.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function(){
      $('input').checkBox();
      $('#toggle-all').click(function(){
      $('#toggle-all').toggleClass('toggle-checked');
      $('#mainform input[type=checkbox]').checkBox('toggle');
      return false;
      });
    });
    </script>

    <![if !IE 7]>

    <!--  styled select box script version 1 -->
    <script src="js/jquery/jquery.selectbox-0.5.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.styledselect').selectbox({ inputClass: "selectbox_styled" });
    });
    </script>


    <![endif]>

    <!--  styled select box script version 2 -->
    <script src="js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
      $('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
    });
    </script>

    <!--  styled select box script version 3 -->
    <script src="js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" });
    });
    </script>
    <!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
    <script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $(document).pngFix( );
    });
    </script>
  </head>
  <body>
    <!-- Start: page-top-outer --><!-- End: page-top-outer -->

    <div>
      <div style="text-align: center;">
      <?php
      
        if($user_inserted) {
              echo "<b>User Updated sucessfully</b>";
              
        }else{
            echo "<b>Something went wrong.</b>";
        }
      ?>
      <a href="<?php print site_url();?>new_user_update">Back to User Update page</a>
      </div>
    </div>
    <!-- start footer -->
    <div id="footer">
      <!--  start footer-left -->
      <div id="footer-left">HRMS 2.0 &copy; Copyright Landmark Group. All rights reserved.</div>
      <!--  end footer-left -->
      <div class="clear"></div>
    </div>
    <!-- end footer -->
  </body>
</html>