<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/mprofile/all.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/user_update/misc.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery.ui.autocomplete.html.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
    
    
    <style type="text/css">
    .thumb-span {background:#860e9c;padding:2px 4px;text-transform:uppercase;color:#fff;-moz-border-radius:4px; border-radius: 4px;font-weight:bold;}
    </style>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'create_user';
        $data['user_name'] = $username;
        $this->load->view('includes/admin_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Create Employee</h1>
        </div>
        <?php if(!empty($sucess)):?>
        <div class="msg-ok">
           <ul>
            <?php foreach($sucess as $suc) :?>
            <li>
                <?php print($suc); ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <?php if(!empty($errors)):?>
        <div class="msg-error">
            <ul>
            <?php foreach($errors as $error) :?>
            <li>
                <?php print($error); ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <div class="clear"></div>
        <div>
          <!--<form action="<?php //echo site_url('people/search')?>" class="search-form" name="searchform" id="quick_search_frm" method="post">
            <fieldset>
              <input accesskey="4" name="term" autocomplete="off" tabindex="23" type="text" class="text" id="txt_general_search" value="Search by name.."/>
              <input name="search" type="hidden" value="search"/>
              <input name="search_from" type="hidden" value="" id="search_from"/>
            </fieldset>
            <div id ="search_element">
              <div id='search_listing'>
                <div id="src-loader" ><img src="<?php //echo base_url()?>images/loader.gif" /></div>
                <ul id='src_user_list'>
                  
                </ul>
                <p class='src_more_res'><a id="submit_src" href="#"></a></p>
              </div>
              <div id='search_listing_f'></div>
            </div>
            <img src="<?php //// base_url()?>images/gnr_src_bg-f.jpg" style="display: none;" alt="bg-preload" />
            <img src="<?php// echo base_url()?>images/user_list_bg.png" style="display: none;" alt="bg-preload" />
            <img src="<?php //echo base_url()?>images/user_list_f_bg.png" style="display: none;" alt="bg-preload" />
          </form>-->
        </div>
        
        <div class="clear"></div>
        <form method="post" action="<?php echo site_url('new_user_update/create_new_user'); ?>" id="frm-create-profile" name="frm-create-profile">
            <div class="form-fields">
              <!-- Locked contact details -->
              <div class="contact-detail-box">
                <div class="contact-details">
                
                  <div class="contact-info">
                    <div class="contact-attr">Email:</div>
                    <div><input type="text" name="email" value="<?php print !empty($form_data['email']) ? $form_data['email'] : ''; ?>" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">First Name:</div>
                    <div><input type="text" name="first_name" value="<?php print !empty($form_data['first_name']) ? $form_data['first_name'] : ''; ?>" id="first_name" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Middle Name:</div>
                    <div><input type="text" name="middle_name" value="<?php !empty($form_data['middle_name']) ? print $form_data['middle_name'] : print ''; ?>" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Last Name:</div>
                    <div><input type="text" name="last_name" value="<?php !empty($form_data['last_name']) ? print $form_data['last_name'] : print ''; ?>"  id="last_name"/></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Display Name:</div>
                    <div><input type="text" name="display_name" value="<?php !empty($form_data['display_name']) ? print $form_data['display_name'] : print ''; ?>" id="display_name" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Title:</div>
                    <div>
                      <select name="title">
                        <option value="MR." <?php (!empty($form_data['title']) && $form_data['title'] =='MR.') ? print 'selected="selected"' : ''; ?>>MR.</option>
                        <option value="MRS." <?php (!empty($form_data['title']) && $form_data['title'] =='MRS.') ? print 'selected="selected"' : ''; ?>>MRS.</option>
                        <option value="MS." <?php (!empty($form_data['title']) && $form_data['title'] =='MS.') ? print 'selected="selected"' : ''; ?>>MS.</option>
                        <option value="MISS." <?php (!empty($form_data['title']) && $form_data['title'] =='MISS.') ? print 'selected="selected"' : ''; ?>>MISS.</option>
                        <option value="DR." <?php (!empty($form_data['title']) && $form_data['title'] =='DR.') ? print 'selected="selected"' : ''; ?>>DR.</option>
                      </select>
                    </div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Concept:</div>
                    <div><select name="concept">
                    <option value="">Select Concept</option>
                    <?php
                    if($concept_list) {
                      foreach($concept_list as $con) {
                      ?><option value="<?php echo $con['id']; ?>" <?php (!empty($form_data['concept']) && $con['id'] == $form_data['concept']) ? print 'selected="selected"' : print ''; ?>><?php echo $con['name']; ?></option><?php
                      }  
                    }
                    ?>
                    </select></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Designation:</div>
                    <div><select name="designation">
                    <option value="">Select Designation</option>
                    <?php
                    if($designation_list) {
                     foreach($designation_list as $des) {
                      ?><option value="<?php echo $des['id']; ?>" <?php (!empty($form_data['designation']) && $des['id'] == $form_data['designation']) ? print 'selected="selected"' : print ''; ?>><?php echo $des['name']; ?></option><?php
                     }
                    }
                    ?>
                    </select></div>
                  </div>

                  <div class="contact-info">
                    <div class="contact-attr">Department:</div>
                    <div><select name="department">
                    <option value="">Select Department</option>
                    <?php
                    if(!empty($department_list)) {
                        
                     foreach($department_list as $dep) {
                      ?><option value="<?php echo $dep['id']; ?>" <?php (!empty($form_data['department']) && $dep['id'] == $form_data['department']) ? print 'selected="selected"' : print ''; ?>><?php echo $dep['name']; ?></option>
                      <?php
                      }
                    }
                    ?>
                    </select></div>
                  </div>
                        <div class="contact-info" id="supervisor_names_content">
                    <div class="contact-attr">Supervisor :</div>
                    <div><input type="text" name="supervisor_name" id="supervisor_names" value="<?php !empty($form_data['supervisor_name']) ? print $form_data['supervisor_name'] : print ''; ?>" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Phone:</div>
                    <div><input type="text" name="phone" value="<?php !empty($form_data['phone']) ? print $form_data['phone'] : print ''; ?>" /></div>
                  </div>
                  <div class="clear"></div>

                  <div class="contact-info">
                    <div class="contact-attr">Mobile no:</div>
                    <div><input type="text" name="mobile" value="<?php !empty($form_data['mobile']) ? print $form_data['mobile'] : print ''; ?>" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Has an HRMS ID?</div>
                    <div>
                    <input type="radio" name="lmg_emp" value="yes"  <?php (!empty($form_data['lmg_emp']) && $form_data['lmg_emp'] == 'yes') ? print 'checked' : ''; ?>/>Yes
                    <input type="radio" name="lmg_emp" value="no"  <?php (!empty($form_data['lmg_emp']) && $form_data['lmg_emp'] == 'no') ? print 'checked' : ''; ?>/>No
                    </div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info" id="hrms_id_content">
                    <div class="contact-attr">HRMS ID:</div>
                    <div><input type="text" name="hrms_id" id="hrms_id" value="<?php !empty($form_data['hrms_id']) ? print $form_data['hrms_id'] : print ''; ?>" /></div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Location :</div>
                    <div><select name="location">
                    <option value="">Select Location</option>
                    <?php
                    if($location_list) {
                      foreach($location_list as $loc) {
                      ?><option value="<?php echo $loc['id']; ?>" <?php (!empty($form_data['location']) && $loc['id'] == $form_data['location']) ? print 'selected="selected"' : print ''; ?>><?php echo $loc['name']; ?></option><?php
                      }  
                    }
                    ?>
                    </select></div>
                  </div>
                  <div class="clear"></div>

                  <div class="contact-info">
                    <div class="contact-attr">Territory :</div>
                    <div><select name="territory">
                    <option value="">Select Territory</option>
                    <?php
                    if($territory_list) {
                      foreach($territory_list as $terr) {
                      ?><option value="<?php echo $terr['id']; ?>" <?php (!empty($form_data['territory']) && $terr['id'] == $form_data['territory']) ? print 'selected="selected"' : ''; ?>><?php echo $terr['name']; ?></option><?php
                      }  
                    }
                    ?>
                    </select></div>
                  </div>
                  <div class="clear"></div>
                  
                  </div>
                </div>
              </div>
              
            </div>
            <div class="submit-div user_create_submit"><input type="image" src="<?=base_url()?>images/btn-save.jpg" name="save changes" />
            <a href="#">Cancel</a></div>
          </form>
      
          <div style="display:none">
              <div id="previewBox" style="padding:10px; background:#fff;">
                  <p><strong>This content comes from a hidden element on this page.</strong></p>
                  <p>The inline option preserves bound JavaScript events and changes, and it puts the content back where it came from when it is closed.<br /></p>
              </div>
          </div>
    </div>
</div>
<script type="text/javascript">
//General Search Field
var base_url = "/";
$(document).ready(function(){
    
    $("#first_name").on( "keyup focusout keydown", function() {
        
        var fname = $("#first_name").val();
        var lname = $("#last_name").val();
        fullname = fname+' '+lname;
        $("#display_name").val(fullname);
    });
    
    $("#last_name").on( "keypress focusout keydown", function() {
        var fname = $("#first_name").val();
        var lname = $("#last_name").val();
        fullname = fname+' '+lname;
        $("#display_name").val(fullname);
    });
    
    
    var hrms_id_status = $('input:radio[name=lmg_emp]:checked').val();
    if (hrms_id_status == 'yes') {
        $('#hrms_id_content').css('display','block');
    }else{
        $('#hrms_id_content').css('display','none');
    }
    
    $('input:radio[name=lmg_emp]').click(function () {
        var hrms_id = $(this).val();
        console.log(hrms_id);
        if (hrms_id == 'yes') {
            $('#hrms_id_content').css('display','block');
        }else{
            $('#hrms_id_content').css('display','none');
        }
    });
    
    var sup_names_list = "<?php print $supervisor_names; ?>";
    var sup_names = [];
    sup_names = sup_names_list.split(',');
    
    $( "#supervisor_names" ).autocomplete({
      source: sup_names,
      html: true,
      position: { my: "left top", at: "left bottom", collision: "none" }
    });
  /* General Files */
  //loading base Url of the site
  //base_url = $("#base_url").val()+"index.php/";
});
function general_search_form(){
 
  //search when clikc on link
  $('#submit_src').live('click',function(){
    $('#quick_search_frm').submit();
  })

  //Hover background change
  $('#src_user_list li div').live('mouseover',function(){
    $('#src_user_list li div').removeClass('hover');
    $(this).addClass('hover')
    return false;
  })
  //Hide search list on main mouse out
  $('#src_user_list').blur(function(){
    $('#search_element').hide();
  })

  //User click
  $('#src_user_list li').live('click',function(){
    $('#search_element').show();
    var user_profile = this.id
    window.location = user_profile;
    return false;
  })
  //Type event
  var tmp_cnt = 0;// arrow key navigation count
  $('#txt_general_search').keyup(function(e){

    if(e.keyCode == 27){
      $('#search_element').hide();
      return false;
    }
   
    var src_term =  $('#txt_general_search').val();
    var src_from =  '';
    if($('#search_from')) 
    {
      src_from = $('#search_from').val(); 
    }
    if(src_term.length < 3){
      return false;
    }
   

    var size_li = $("#src_user_list li").size();

    if(e.keyCode == 38){
      if(tmp_cnt <=0){
        tmp_cnt =  size_li;
      }else{
        tmp_cnt = tmp_cnt-1
      }
      //$('#src_user_list li div').css('background-color','inherit')
      //$('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
      $('#src_user_list li div').removeClass('hover')
      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
      return false;
    }else if(e.keyCode == 40){
      if(size_li == tmp_cnt){
        tmp_cnt = 0;
      }else{
        tmp_cnt = tmp_cnt+1
      }
      //$('#src_user_list li div').css('background-color','inherit')
      // $('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
      $('#src_user_list li div').removeClass('hover')
      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
      return false;
    }



    $('#search_element').show();
    $('#src-loader').show();

    $.ajax({
      url: base_url+"people/quick_search_sphinx",
      data: {
        "src_term":src_term,
        'src_from': src_from
      },
      async: true,
      dataType: "json",
      type: "POST",
      success: function(data){
        $('#src-loader').hide();
        if(data['show']){
          //alert(e.keyCode);
          $('#src_user_list').html(data['result']);
          $('#submit_src').html(data['result_count']);
          $('#search_element').show();
        //alert($('#src_user_list li:first').html());
        }else{
          $('#src_user_list').html(data['result']);
          $('#submit_src').html('');
          $('#search_element').show();
        }
      }
    });

    return false;
  })


  //Focus
  $("#txt_general_search").focus(function(){
    var src_term =  $("#txt_general_search").val();
    if( $.trim(src_term) == 'Search by name..'){
      $("#txt_general_search").val('');
    }
    $(this).css('color','#000000');
  })
  //Blur
  $("#txt_general_search").blur(function(){
    var src_term =  $("#txt_general_search").val();
    if( $.trim(src_term) == ''){
      $("#txt_general_search").val('Search by name..');
    }
    $(this).css('color','#747474');
  })
}
general_search_form();
</script>
</body>
</html>