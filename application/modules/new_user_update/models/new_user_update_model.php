<?php
Class New_user_update_model extends CI_Model {

  function User_update_model() {
    parent::__construct();
  }
  
  function get_whitelist_email($hrmsid) {
    $this->db->select('iw.hrmsid');
    $this->db->from('import_whitelist iw');
    $this->db->where('iw.hrmsid',$hrmsid);
    $query = $this->db->get();
    $ret = $query->row();
    if (!empty($ret)) {
      return $ret->hrmsid;
    } else {
      return $query->result();
    }
    
   
  }
  
  function get_blacklist_email($hrmsid) {
    $this->db->select('ib.hrmsid');
    $this->db->from('import_blacklist ib');
    $this->db->where('ib.hrmsid',$hrmsid);
    $query = $this->db->get();
    $ret = $query->row();
      
    if (!empty($ret)) {
      return $ret->hrmsid;
    } else {
     return $query->result();
    }
   
  }
  function get_all_users() {
    $this->db->select('ci.hrms_id');
    $this->db->from('ci_users ci');
    $query = $this->db->get();
    return $query->result_array();   
  }
  /*
   * @method Bulk inert 1st parameter table name, 2nd parameter data
   **/
  function bulk_user_insert($table,$user_details) {
    $this->db->insert_batch($table, $user_details); 
  }
  
  /*
   * @method Get Concept name and id
   **/ 
   function get_concept_list () {
    $this->db->select('cmc.id, cmc.name');
    $this->db->from('ci_master_concept cmc');
    $query = $this->db->get();
    return $query->result_array();
   } 
   
  /*
   * @method Get Concept name and id
   **/ 
   function get_designation_list () {
    $this->db->select('cmd.id, cmd.name');
    $this->db->from('ci_master_designation cmd');
    $query = $this->db->get();
    return $query->result_array();
   }   
  
  /*
   * @method Get Concept name and id
   **/ 
   function get_department_list () {
    $this->db->select('cmde.id, cmde.name');
    $this->db->from('ci_master_department cmde');
    $query = $this->db->get();
    return $query->result_array();
   }  
   
    /*
   * @method Get Concept name and id
   **/ 
   function get_userdetails_by_email($email) {
    $this->db->select('cu.id');
    $this->db->from('ci_users cu');
    $this->db->where('cu.email',$email);
    $query = $this->db->get();
    return $query->row();
   }
   
   /*
   * @method Get Concept name and id
   **/ 
   function get_hrmsid_by_display_name($supervisor) {
    $this->db->select('cu.hrms_id');
    $this->db->from('ci_users cu');
    $this->db->where('cu.display_name',$supervisor);
    $query = $this->db->get();
    return $query->row();
   }

    /*
   * @method Get Concept name and id
   **/ 
   function check_replyusername_exist($reply_username,$id = null) {
    $this->db->select('cu.id');
    $this->db->from('ci_users cu');
    $this->db->where('cu.reply_username',$reply_username);
    if(!empty($id)) {
     $this->db->where('cu.id !=',$id); 
    }
    $query = $this->db->get();
    return $query->row();
   }

   
  /*
   * @method Insert user details and create a new user
   **/ 
   function create_user($data) {
    $str = $this->db->insert('ci_users', $data);
    return $this->db->insert_id();
  }
  
  /*
   * Get Names of all users from ci_user
   **/
   function get_all_users_names() {
    $this->db->select('ci.display_name');
    $this->db->from('ci_users ci');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  /*
   * Get Names of all locations from ci_location
   **/
   function get_all_locations() {
    $this->db->select('cl.id, cl.name');
    $this->db->from('ci_master_location cl');
    $query = $this->db->get();
    return $query->result_array();
  }
  /*
   * Get Names of all terrtory from get_all_territory
   **/
  function get_all_territory() {
    $this->db->select('ct.id, ct.name');
    $this->db->from('ci_master_territory ct');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  function get_all_displaynames() {
    $this->db->select('ci.id, ci.display_name');
    $this->db->from('ci_users ci');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  function update_user_details($table_name, $data, $field_name) {
    $this->db->update_batch($table_name, $data, $field_name);
  }
  
  /*
   *  Get All duplicate reply_usernames
   **/ 
  function get_duplicate_reply_usernames() {
    
    $sql = 'SELECT ci_users.id, ci_users.reply_username, ci_users.first_name, ci_users.middle_name, ci_users.last_name,ci_users.email, ci_users.last_login_time FROM ci_users
    INNER JOIN (SELECT reply_username FROM ci_users
    GROUP BY reply_username HAVING count(*) > 1) dup ON ci_users.reply_username = dup.reply_username WHERE ci_users.last_login_time = 0';
    
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      $data = $result->result_array();
    }
    $result->free_result();
    return $data;
  }
  
  /*
   *  Update reply_username based on id
   **/
  function update_reply_username($data,$id) {
    
    $this->db->where('id', $id);
    $this->db->update('ci_users', $data);
    
  }
  /*
   *  Check Email id exist
   **/
  function check_email_exist_id($email,$id) {
    $this->db->select('cu.id');
    $this->db->from('ci_users cu');
    $this->db->where('cu.email',$email);
    $this->db->where('cu.id !=',$id);
    $query = $this->db->get();
    return $query->row();
  }  
  
   
}