<?php

class New_user_update extends MX_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('manage/manage_model');
    $this->load->model('new_user_update_model');
    $this->load->model('user_update/user_update_model');
    $this->config->load('intranet');
    $this->load->model('backend/moderate_model');
    $this->load->model('User');
    $this->load->model('Role');
  }
  
  function index() {
    $all_users = $this->new_user_update_model->get_all_users();
    foreach ($all_users as $user) {
      $user_array[] = $user['hrms_id'];
    }
    
    $data = array();
    set_time_limit(0);
    $valid_emails = 0;
    $dif_emails = 0;
    $unique_emails = array();
    $duplicate_emails = 0;
    $indb = 0;
    $indb2 = 0;
    $notdb = 0;
    $emailpoints = array();
    $emails_for_import_warn = array();
    $emails_for_import = array();

    $emails_that_should_not_be_in_db = array();

    $current_date = date("Ymd");

    // GET FROM FTP

    //$file_path = "/Applications/MAMP/htdocs/hrms/intranet_users_".$current_date.".csv";
    //$file_path = "/var/www/html/importer/in/intranet_users_".$current_date.".csv";
    $file_path = "/var/www/html/v2/intranet_users_csv/diff_".$current_date.".csv";
    //$file_path = "importer/in/diff_20140515.csv";//.$current_date.".csv";

    echo "Using intranet_users_$current_date.csv<br>";

    //$csv_file = "/Applications/MAMP/htdocs/hrms/ereview-".$current_date.".csv";
    /* $csv_file = "/var/www/html/importer/review/review-".$current_date.".csv";
    //$review_file = "/Applications/MAMP/htdocs/hrms/for_review.csv";
    $review_file = "/var/www/html/importer/review/for_review.csv"; */
    $csv_data = 'BUSINESS_GROUP_ID,TERRITOTY_NAME,PERSON_TYPE_ID,EMPLOYEE_NUMBER,TITLE,FIRST_NAME,MIDDLE_NAMES,LAST_NAME,ORIGINAL_DATE_OF_HIRE,GDOJ,DATE_OF_BIRTH,FULL_NAME,POSITION_NAME,DEPARTMENT,CONCEPT,ORGANISATION_NAME,ORGANIZATION_ID,LOCATION,EMAIL_ADDRESS,SUPERVISOR,SUPERVISOR_ID,BAND,Mobile Phone,Board Number,EXTN,PREVIOUS_LAST_NAME,ACTUAL_WRK_PLACE';
    $csv_data .= "\n";

    $file = fopen($file_path, "r") or exit("Unable to open file!");
    
    $row_cnt = 1;
    $row_validation = array();

    $processed_user_data = array();
    if(($handle = fopen($file_path, "r")) !== false){
      $header = fgetcsv($handle);
      $error_array = array();
       if (trim($header[0]) != 'BUSINESS_GROUP_ID') {
        $error_array[] = 'Business group ID missing(BUSINESS_GROUP_ID)';
      }
      if (trim($header[1]) != 'TERRITOTY_NAME') {
        $error_array[] = 'Territory Name missing(TERRITOTY_NAME)';
      }
      if (trim($header[2]) != 'PERSON_TYPE_ID') {
        $error_array[] = 'Person ID missing(PERSON_TYPE_ID)';
      }
      if (trim($header[3]) != 'EMPLOYEE_NUMBER') {
        $error_array[] = 'Employee number missing(EMPLOYEE_NUMBER)';
      }
      if (trim($header[4]) != 'TITLE') {
        $error_array[] = 'Title missing(TITLE)';
      }
      if (trim($header[5]) != 'FIRST_NAME') {
        $error_array[] = 'First name missing(FIRST_NAME)';
      }
      if (trim($header[6]) != 'MIDDLE_NAMES') {
        $error_array[] = 'Middle name missing(MIDDLE_NAMES)';
      }
      if (trim($header[7]) != 'LAST_NAME') {
        $error_array[] = 'Last name missing(LAST_NAME)';
      }
      if (trim($header[8]) != 'ORIGINAL_DATE_OF_HIRE') {
        $error_array[] = 'Hire date missing(ORIGINAL_DATE_OF_HIRE)';
      }
      if (trim($header[9]) != 'GDOJ') {
        $error_array[] = 'GDOJ missing(GDOJ)';
      }
      if (trim($header[10]) != 'DATE_OF_BIRTH') {
        $error_array[] = 'Date of birth missing(DATE_OF_BIRTH)';
      }
      if (trim($header[11]) != 'FULL_NAME') {
        $error_array[] = 'Full name missing(FULL_NAME)';
      }
      if (trim($header[12]) != 'POSITION_NAME') {
        $error_array[] = 'Position missing(POSITION_NAME)';
      }
      if (trim($header[13]) != 'DEPARTMENT') {
        $error_array[] = 'Department missing(DEPARTMENT)';
      }
      if (trim($header[14]) != 'CONCEPT') {
        $error_array[] = 'Concept missing(CONCEPT)';
      }
      if (trim($header[15]) != 'ORGANISATION_NAME') {
        $error_array[] = 'Organisation name missing(ORGANISATION_NAME)';
      }
      if (trim($header[16]) != 'ORGANIZATION_ID') {
        $error_array[] = 'Organisation id missing(ORGANIZATION_ID)';
      }
      if (trim($header[17]) != 'LOCATION') {
        $error_array[] = 'Location missing(LOCATION)';
      }
      if (trim($header[18]) != 'EMAIL_ADDRESS') {
        $error_array[] = 'Email missing(EMAIL_ADDRESS)';
      }
      if (trim($header[19]) != 'SUPERVISOR') {
        $error_array[] = 'Supervisor missing(SUPERVISOR)';
      }
      if (trim($header[20]) != 'SUPERVISOR_ID') {
        $error_array[] = 'Supervisor id missing(SUPERVISOR_ID)';
      }
      if (trim($header[21]) != 'BAND') {
        $error_array[] = 'Band missing(BAND)';
      }
      if (trim($header[22]) != 'Mobile Phone') {
        $error_array[] = 'Mobile missing(Mobile Phone)';
      }
      if (trim($header[23]) != 'Board Number') {
        $error_array[] = 'Board number missing(Board Number)';
      }
      if (trim($header[24]) != 'EXTN') {
        $error_array[] = 'Extension missing(EXTN)';
      }
      if (trim($header[25]) != 'PREVIOUS_LAST_NAME') {
        $error_array[] = 'Previous last name is missing(PREVIOUS_LAST_NAME)';
      }
      if (trim($header[26]) != 'ACTUAL_WRK_PLACE') {
        $error_array[] = 'Actual work place is missing(ACTUAL_WRK_PLACE)';
      } 

      /* if (count($error_array)) {
        $check_result['proceed'] = 0;
        $check_result['error'] = $error_array;
        $check_result['data'] = array();
        echo"<pre>";print_R($check_result);
        return $check_result;
      } */
/*       if (count($header) != 27) {
        $row_validation[] = 'Row count missmatch, didnt find 27 columns, Total columns: ' . count($header) . ', Error at row no.: ';
        echo"<pre>";print_R($row_validation);
        return $row_validation;
      } */
      $already_present = 0;
      while(($data = fgetcsv($handle)) !== false)
      {
        //Email address validation
        if (filter_var($data[18], FILTER_VALIDATE_EMAIL)) {
          $valid_emails++;
          $email = strtolower($data[18]);
          
          // Check the unique_emails array for a match - if a match is found, a duplicate flag is raised. Otherwise, save the email in the unique_emails array
          if(in_array($email, $unique_emails)) {
              $duplicate_emails++; 
          }else {
            $unique_emails[] = $email;
          }

          $emails = explode("@", $email); // extract the email username - the part before @
          $email1 = $emails[0];
          $hrmsid = $data[3];
          
          
          
          $emailpoints = array();
          $fname = strtolower(trim($data[5]));
          $mname = strtolower(trim($data[6]));
          $lname = strtolower(trim($data[7]));
          $emailpoints[$hrmsid][$email] = 0;
          $points = 0;
          
          $email_name = str_replace('.','',$email1);
          $email_name = str_replace('_','',$email_name);
          $email_name = str_replace('-','',$email_name);
          
          
          
          /*if(!stripos($email, "@cplmg.com") && !stripos($email, "@cpksa.com") && !stripos($email, "@fitnessfirst-me.com")
              && !stripos($email, "@citymaxhotels.com") && !stripos($email, "@arg.com.bh") && !stripos($email, "@landmarkgroup.com")) {
              $user['name'] = $fname .'&nbsp; '. $mname .'&nbsp; '. $lname;
              $user['points'] = $points;
              $user['email'] = $email;
              $user['hrmsid'] = $hrmsid;
               
              $black_list_array[] = $user;
              continue;
          }*/
          
          if (in_array($hrmsid, $user_array)) {
            $already_present++;
            $user['name'] = $fname .'&nbsp; '. $mname .'&nbsp; '. $lname;
            $user['points'] = $points;
            $user['email'] = $email;
            $user['hrmsid'] = $hrmsid;
             
            $existing_users[] = $user;
            continue;
          }
          
          /* $percent  = similar_text($email_name, $fname);
          $percent1 = similar_text($email_name, $mname);
          $percent2 = similar_text($email_name, $lname); */
          
          $f_name_arr = explode(' ',$fname);
          $m_name_arr = explode(' ',$mname);
          $l_name_arr = explode(' ',$lname);
          
          $f_count = 0;
          $m_count = 0;
          $l_count = 0;
          $percent = 0;
          $percent1 = 0;
          $percent2 = 0;
          $points = 0;

          $is_match = false;
          $email_count = strlen($email_name);
          
          foreach($f_name_arr as $f_name) {
            if (empty($f_name)) {
              continue;
            }
            $f_count = strlen($f_name);
            $percent = $this->LevenshteinDistance($email_name, $f_name);
            $diff = $email_count - $percent;

            if (($diff == $f_count  || ($diff > ($f_count/2))) && $f_count > 2) {

              $is_match = true;
              $points = 100;
            }
          }
          
          foreach($m_name_arr as $m_name) {
            if (empty($m_name)) {
              continue;
            }
            $m_count = strlen($m_name);
            $percent1 = $this->LevenshteinDistance($email_name, $m_name);
            $diff = $email_count - $percent1;

            if (($diff == $m_count || ($diff > ($m_count/2))) && $m_count > 2) {
              $is_match = true;
              $points += 100;
            }
          }
          
          foreach($l_name_arr as $l_name) {
            if (empty($l_name)) {
              continue;
            }
            $l_count = strlen($l_name);
            $percent2 = $this->LevenshteinDistance($email_name, $l_name);
            $diff = $email_count - $percent2;

            if (($diff == $l_count || ($diff > ($l_count/2))) && $l_count > 2) {
              $is_match = true;
              $points += 100;
            }
          }

          $emailpoints[$hrmsid][$email] = $points;
          
          $white_list_email = $this->new_user_update_model->get_whitelist_email($hrmsid);
          $black_list_email = $this->new_user_update_model->get_blacklist_email($hrmsid);
           $user = array();
         $valid_domains = array();
         $valid_domains = $this->config->item('valid_email_domain');
         
          if ($is_match && empty($white_list_email)) {
              if(!empty($emails[1]) && in_array($emails[1],$valid_domains)) {
                    $user['name'] = $fname .'&nbsp; '. $mname .'&nbsp; '. $lname;
                    $user['points'] = $points;
                    $user['email'] = $email;
                    $user['hrmsid'] = $hrmsid;
                    
                    $white_list_array[] = $user;
                }
          }elseif (empty($black_list_email)){
            if(!empty($emails[1]) && in_array($emails[1],$valid_domains)) {
                $user['name'] = $fname .'&nbsp; '. $mname .'&nbsp; '. $lname;
                $user['points'] = $points;
                $user['email'] = $email;
                $user['hrmsid'] = $hrmsid;
                 
                $black_list_array[] = $user;
            }            
          }
          unset($data);
        }
        
      }   
      fclose($handle);
    } else {
      exit("Unable to open file!");
    }
    fclose($file);
    $data['whitelist'] = $white_list_array;
    $data['blacklist'] = $black_list_array;
    $data['exisiting'] = $existing_users;
    //echo"<pre>";print_r($data);exit; 
    $this->load->view('new_user_update_view', $data);
  }
  
  function LevenshteinDistance($s1, $s2) { 
    $sLeft = (strlen($s1) > strlen($s2)) ? $s1 : $s2; 
    $sRight = (strlen($s1) > strlen($s2)) ? $s2 : $s1; 
    $nLeftLength = strlen($sLeft); 
    $nRightLength = strlen($sRight); 
    if ($nLeftLength == 0) 
      return $nRightLength; 
    else if ($nRightLength == 0) 
      return $nLeftLength; 
    else if ($sLeft === $sRight) 
      return 0; 
    else if (($nLeftLength < $nRightLength) && (strpos($sRight, $sLeft) !== FALSE)) 
      return $nRightLength - $nLeftLength; 
    else if (($nRightLength < $nLeftLength) && (strpos($sLeft, $sRight) !== FALSE)) 
      return $nLeftLength - $nRightLength; 
    else { 
      $nsDistance = range(1, $nRightLength + 1); 
      for ($nLeftPos = 1; $nLeftPos <= $nLeftLength; ++$nLeftPos) 
      { 
        $cLeft = $sLeft[$nLeftPos - 1]; 
        $nDiagonal = $nLeftPos - 1; 
        $nsDistance[0] = $nLeftPos; 
        for ($nRightPos = 1; $nRightPos <= $nRightLength; ++$nRightPos) 
        { 
          $cRight = $sRight[$nRightPos - 1]; 
          $nCost = ($cRight == $cLeft) ? 0 : 1; 
          $nNewDiagonal = $nsDistance[$nRightPos]; 
          $nsDistance[$nRightPos] = 
            min($nsDistance[$nRightPos] + 1, 
                $nsDistance[$nRightPos - 1] + 1, 
                $nDiagonal + $nCost); 
          $nDiagonal = $nNewDiagonal; 
        } 
      } 
      return $nsDistance[$nRightLength]; 
    } 
  }
  
  function update_user_bwlist() {
     
    if( (!empty($_POST['action'])) && ($_POST['action'] == "Update") ) {
      // Blacklist
      if(!empty($_POST['blacklist'])) {
        $blacklist_data = array();
        $blacklist_parts = array();
        $blacklisted_hrms_ids = array();
        
        foreach($_POST['blacklist'] as $blacklist_item) { 
          $blacklist_parts = explode("|", $blacklist_item);
          if(!empty($blacklist_parts)) {
            $blacklist_data[] = array('email'=>urldecode($blacklist_parts[0]),'hrmsid'=>0);
            $blacklisted_hrms_ids[] = $blacklist_parts[1];
          }
        }
       
        
        // Insert blacklisted data into import_blacklist table
        if(!empty($blacklist_data)) {
         $blacklisted_insert_query = $this->new_user_update_model->bulk_user_insert('import_blacklist',$blacklist_data);
         $this->user_update_model->deactivate_ex_emp($blacklisted_hrms_ids);
        }
      }
      //echo "<hr>";
      
      // Whitelist
      if(!empty($_POST['whitelist'])) {
        $whitelist_parts = array();
        $whitelist_data = array();
        foreach($_POST['whitelist'] as $whitelist_item) {
          $whitelist_parts = explode("|", $whitelist_item);
          if(!empty($whitelist_parts)) {
            $whitelist_data[] = array('email'=>urldecode($whitelist_parts[0]),'hrmsid'=>$whitelist_parts[1]);
          }
        }
        
        // Insert whitelisted users into import_whitelist table
        if(!empty($whitelist_data)) {
          $whitelisted_insert_query = $this->new_user_update_model->bulk_user_insert('import_whitelist',$whitelist_data); 
        }
      }
      //echo "<hr>";

      // Ignore
      if(!empty($_POST['ignore'])) {
        
        $ignorelist_data = array();
        foreach($_POST['ignore'] as $ignorelist_item) {
            $ignorelist_data[] = array('email'=>urldecode($ignorelist_item),'hrmsid'=>0);
        }
        
        //Insert whitelisted users into import_whitelist table
        if(!empty($ignorelist_data)) {
          $ignorelist_insert_query = $this->new_user_update_model->bulk_user_insert('import_blacklist',$ignorelist_data); 
        }
      }
      $data['user_inserted'] = true;
    }else{
      $data['user_inserted'] = false;
    }
    
    $this->load->view('new_user_update_status', $data);
    
  }
  /**
   *  @function Create a new user from admin section - IN-882
   * */
   function create_new_user() {
    
    $user_id = $this->session->userdata("userid");
    $profile = $this->User->getUserProfile($user_id);
    $this->load->model('UserPermission');
    if($profile['role_id'] != 1) {
      redirect(site_url());
    }
    if (!$user_id) {
      
      redirect(site_url());
    }
    
    $error = '';
    $reply_username = '';
    $errror_list = array();
    $raw_email = array();
    $sucess_list = array();
    $form_data = $this->input->post(NULL,TRUE);
    
    if(!empty($_POST)) {
      $email = $this->input->post('email', TRUE);
      $email = strtolower($email);
      if(!empty($email)) {
        
        // Email id validation and cheking email already exist
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
          
          $query_result = $this->new_user_update_model->get_userdetails_by_email($email);
        }else{  
          $error++;
          $errror_list[] = 'Invalid Email Address';
        }
        
        // Checking the reply_username exist
        if(empty($query_result)) {
          
          $raw_email = explode('@',$email);
          if(!empty($raw_email[0])) {
            
            $reply_username_exist = $this->new_user_update_model->check_replyusername_exist($raw_email[0]);
            if(empty($reply_username_exist)) {
              $reply_username = $raw_email[0];
            }else{
              $reply_username = $this->genrate_replyusername($raw_email[0]);
            }
          }
        }else{
            $errror_list[] = "Account alreay exist with $email ";
          }
      }else{
        $errror_list[] = 'Email field is required.';
      }
      
      $title = $this->input->post('title');
      $lmg_employee = $this->input->post('lmg_emp');
      $fname = $this->input->post('first_name');
      $mname = $this->input->post('middle_name');
      $lname = $this->input->post('last_name');
      $display_name = $this->input->post('display_name');
      $concept = $this->input->post('concept');
      $department = $this->input->post('department');
      $desingnation = $this->input->post('designation');
      $phone = $this->input->post('phone');
      $mobile = $this->input->post('mobile');
      $supervisor = $this->input->post('supervisor_name');
      $location = $this->input->post('location');
      $territory = $this->input->post('territory');
      if($lmg_employee == 'yes') {
          $hrms_id = $this->input->post('hrms_id');
          if(empty($hrms_id)) {
            $errror_list[] = 'Enter HRMS id';
          }
      }else{
          $hrms_id = '';
      }
      if(!empty($supervisor)) {
       $supervisour_id =  $this->new_user_update_model->get_hrmsid_by_display_name($supervisor);
       if(empty($supervisour_id)) {
        $errror_list[] = $supervisor.' is not an landmark employee. Please select the appropriate person Name';
       }
      }
      
      // Validating the form
      if(empty($supervisor)) {
        $errror_list[] = 'Select the supervisour name';
      }
      if(empty($fname)) { 
        $errror_list[] = 'Enter the Employee First name';
      }
      if(empty($mname)) { 
        $errror_list[] = 'Enter the Middle name';
      }
      if(empty($lname)) { 
        $errror_list[] = 'Enter the last name';
      }
      if(empty($concept)) { 
        $errror_list[] = 'Please select Concept';
      }
      if(empty($department)) { 
        $errror_list[] = 'Please select Department';
      }
       if(empty($desingnation)) { 
        $errror_list[] = 'Please select Desingnation';
      }
       if(empty($phone)) { 
        $errror_list[] = 'Enter Phone Number';
      }
       if(empty($mobile)) { 
        $errror_list[] = 'Enter Mobile Number';
      }
      if(empty($lmg_employee)) { 
        $errror_list[] = 'Select Employee type';
      }
      if(empty($errror_list)) {
        // Insert the userdetails in database
        $random_no = rand(1,35);
        $profile_pic = "default/$random_no.png";
        
        $user_details = array(
          'email' => $email,
          'reply_username' => $reply_username,
          'hrms_id' => $hrms_id,
          'title' => $title,
          'first_name' => $fname,
          'middle_name' => $mname,
          'last_name' => $lname,
          'display_name' => $display_name,
          'concept_id' => $concept,
          'department_id'=> $department,
          'designation_id'=> $desingnation,
          'phone'=> $phone,
          'mobile'=> $mobile,
          'profile_pic'=> $profile_pic,
          'supervisor_id'=>$supervisour_id->hrms_id,
          'location_id'=>$location,
          'territory_id'=>$territory,
          'user_type' => 1,
          'role_id' => 4,
          'has_hrms'=>$lmg_employee,
          'created_by'=>'admin',
          'welcome_mail'=>1,
          'status'=>'active'
        );
        
         $data_inserted =  $this->new_user_update_model->create_user($user_details); 
         if(!empty($data_inserted)) {
          unset($form_data);
          $form_data = array();
          $sucess_list[] = 'User has been created Sucessfully';
          // Email
          $this->load->helper('smtp_config');
          $smtp_config = smtp_config();
          
          $user_id = $data_inserted;
          $name = $fname;
          $email = $email;
          
          //random password
          $length = 8;
          $level = 3;
          list($usec, $sec) = explode(' ', microtime());
          srand((float) $sec + ((float) $usec * 100000));
          
          $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
          $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
          $validchars[3] = "0123456789_!@#$%&*()abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()";
          
          $password = "";
          $counter = 0;
          
          while ($counter < $length) {
          $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level]) - 1), 1);
          
          // All character must be different
          if (!strstr($password, $actChar)) {
          $password .= $actChar;
          $counter++;
          }
          }
          
          $update_pass = array(
          'password' => md5($password)
          );
          $result = $this->moderate_model->update_user_data($update_pass, $user_id);
          $this->load->library('email');
          
          $sender_email = "intranet@landmarkgroup.com";
          $sender_name = "Landmark Intranet";
          //$email = "intranet.lmg@gmail.com";
          //$to_email = $email;
          //$to_email = "raghu111@gmail.com";
          //$to_email = "thomson.george@cplmg.com";
          $to_email = $email;
          
          
          $subject = "Login to the Landmark Intranet";
          $message = '<table width="520" border="0" style="font-family:Arial;">
          <tr>
          <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
          <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">Welcome to the new Landmark Intranet</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;"><strong>Dear ' . $name . ',</strong> </p>
          <p style="color:#3e3e3e;font-size:14px; margin:5px 0 0 0; line-height:20px;">The all new Landmark Intranet allows you to collaborate with colleagues across various concepts and keep abreast with the latest in the company.</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Your account on the Intranet has been created. You can login using the following details.</p>
          <table  style="color:#3e3e3e;font-size:14px;margin-top:10px;margin-left:20px;">
          <tr>
          <td>Visit:</td>
          <td><a style="color:#1e5886;font-size:16px;" href="http://intranet.landmarkgroup.com/">http://intranet.landmarkgroup.com/</a></td>
          </tr>
          <tr>
          <td>Username:</td>
          <td>' . $email . '</td>
          </tr>
          <tr>
          <td>Password:</td>
          <td>' . $password . '</td>
          </tr>
          </table>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">As soon as you login please change your password and complete your profile.</p>
          
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Thanks,<br />
          Landmark Intranet</p>
          </td>
          </tr>
          
          
          </table>';
          
          $this->email->initialize($smtp_config);
          $this->email->from($sender_email, $sender_name);
          $this->email->set_newline("\r\n");
          $this->email->subject($subject);
          $this->email->message($message);
          $this->email->to($to_email);
          if ($this->email->send()) {
          $update_welcome_status = array(
          'welcome_mail' => 1
          );
          $this->moderate_model->update_user_data($update_welcome_status, $user_id);
          $sucess_list[] = 'Email has been sended to user with login details';
          } else {
            $errror_list[] = 'Unable to send E-mail. Please Contact to Administrator';
          }
         }else{
          $errror_list[] = 'User creation failed. Please Contact to Administrator';
         }
      }
    }
    
  $all_users = $this->new_user_update_model->get_all_users_names();
   $supervisour_names = array();
   foreach($all_users as $users_name) {
    $supervisour_names_array[] = $users_name['display_name'];
   }
   $data['supervisor_names'] = implode(",",$supervisour_names_array);
   $data['location_list'] = $this->new_user_update_model->get_all_locations();
   $data['territory_list'] = $this->new_user_update_model->get_all_territory();
    
    $data['concept_list'] = $this->new_user_update_model->get_concept_list();
    $data['designation_list'] = $this->new_user_update_model->get_designation_list();
    $data['department_list'] = $this->new_user_update_model->get_department_list();
    $data['errors'] = $errror_list;
    $data['sucess'] = $sucess_list;
    $data['form_data'] = $form_data;
    $data['username'] = $this->session->userdata('fullname');
    
    $this->load->view("user_registration_template",$data);
   }
   
   function genrate_replyusername($uname) {
    $count = 0;
    $i = 0;
    $new_name = '';
    while(true){
      $new_name = $uname.$i;
      $reply_username_exist = $this->new_user_update_model->check_replyusername_exist($new_name);
      if(empty($reply_username_exist)) {
        break;
      }
      $i++;
    }
    return $new_name;
   }
   
   function format_displayname() {
    $all_users = $this->new_user_update_model->get_all_displaynames();
    $updated_data = array();
    $i=0;
    foreach($all_users as $user) {
      
      $display_name = ucwords(strtolower($user['display_name']));
      $updated_data[$i]['id'] = $user['id'];
      $updated_data[$i]['display_name'] = $display_name;
      $i++;
    }
    $this->new_user_update_model->update_user_details('ci_users',$updated_data,'id');
    echo "User Details Updated sucessfully";
   }
   
   function remove_duplicate_username() {
    
    set_time_limit(0);
    $dup_users = $this->new_user_update_model->get_duplicate_reply_usernames();
    
    $i =0;
    $f_count = 0;
    $m_count = 0;
    $l_count = 0;
    $percent = 0;
    $percent1 = 0;
    $percent2 = 0;
    $is_match = false;
    $points = 0;
    $k = 0;
    
    foreach($dup_users as $user_details) {
      $is_match = false;
      $reply_username = "";
      $reply_username_exist = "";
      $username = "";
      $email_id = "";
              
      $points = 0;
      $fname = strtolower(trim($user_details['first_name']));
      $mname = strtolower(trim($user_details['middle_name']));
      $lname = strtolower(trim($user_details['last_name']));
      
      //Check for the valid reply_username if not then update with firstname and last name
      $reply_uname = $user_details['reply_username'];
      
      $email_name = str_replace('.','',$reply_uname);
      $email_name = str_replace('_','',$email_name);
      $email_name = str_replace('-','',$email_name);
      
      $f_name_arr = explode(' ',$fname);
      $m_name_arr = explode(' ',$mname);
      $l_name_arr = explode(' ',$lname);
      
      $email_count = strlen($email_name);
      
      foreach($f_name_arr as $f_name) {
        if (empty($f_name)) {
          continue;
        }
        $f_count = strlen($f_name);
        $percent = $this->LevenshteinDistance($email_name, $f_name);
        $diff = $email_count - $percent;
        
        if (($diff == $f_count  || ($diff > ($f_count/2))) && $f_count > 2) {
        
          $is_match = true;
          $points = 100;
        }
      }
      
      foreach($m_name_arr as $m_name) {
        if (empty($m_name)) {
          continue;
        }
        $m_count = strlen($m_name);
        $percent1 = $this->LevenshteinDistance($email_name, $m_name);
        $diff = $email_count - $percent1;
        
        if (($diff == $m_count || ($diff > ($m_count/2))) && $m_count > 2) {
          $is_match = true;
          $points += 100;
        }
      }
      
      foreach($l_name_arr as $l_name) {
        if (empty($l_name)) {
          continue;
        }
        $l_count = strlen($l_name);
        $percent2 = $this->LevenshteinDistance($email_name, $l_name);
        $diff = $email_count - $percent2;
        
        if (($diff == $l_count || ($diff > ($l_count/2))) && $l_count > 2) {
          $is_match = true;
          $points += 100;
        }
      }
      $i++;
      if((!$is_match) && ($points <= 0)) {
        
        $reply_username = "";          
        if(!empty($fname)) {
          $reply_username.=str_replace(" ","",$fname);
        }
        if(!empty($fname) && !empty($lname) ) {
          $reply_username.='.';
        }
        if(!empty($lname)) {
          $reply_username.=str_replace(" ","",$lname);
        }
          
        $reply_username_exist = $this->new_user_update_model->check_replyusername_exist($reply_username,$user_details['id']);
        
        if(empty($reply_username_exist)) {
          $username = $reply_username;
        }else{
          $username = $this->genrate_replyusername($reply_username);
        }
        
        $user_data = array();
        $user_data = array(
          'reply_username' => $username,
          'email' => ""
        );
        
        $this->new_user_update_model->update_reply_username($user_data,$user_details['id']);
      }else{
        // Remove the duplicate reply_username
        $email_id = $user_details['email'];
        
        if(!empty($reply_uname)) {
          $reply_username_exist = $this->new_user_update_model->check_replyusername_exist($reply_uname,$user_details['id']);
          if(empty($reply_username_exist)) {
            $username = $reply_uname;
          }else{
            $username = $this->genrate_replyusername($reply_uname);
          } 
        }
        
        if(!empty($email_id)) {
          $email_exist = $this->new_user_update_model->check_email_exist_id($email_id,$user_details['id']);
          if(empty($email_exist)) {
            $new_email = $email_id;
          }else{
            $new_email = "";
          }
        }
        
        $user_data = array();
        $user_data = array(
          'reply_username' => strtolower($username),
          'email' => strtolower($new_email)
        );
        
        $this->new_user_update_model->update_reply_username($user_data,$user_details['id']);
        //$user_data['type'] = 'else';
      }
     //echo "<pre>";print_r($user_data);echo "</pre>"; 
    }
    exit;
  }
}
