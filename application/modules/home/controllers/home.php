<?php

class Home extends MX_Controller {

  private $news_feeds_time = 24; //Specify Time in hours
  private $news_feeds_time_status_update = 24; //Only for status Updates Specify Time in hours
  private $mt_category_news = "news"; //This for MT database "mt_category->category_basename" For News category
  private $mt_category_announcement = "announcement"; //This for MT database "mt_category->category_basename" For Announcement category
  private $mt_category_offers = "offers"; //This for MT database "mt_category->category_basename" For Offers category
//Constructor function
  function Home() {
    parent::__construct();
    //date_default_timezone_set("Asia/Calcutta"); //default timezone setting
    //date_default_timezone_set("Asia/Baghdad"); //default timezone setting
    $this->load->model('home_model');
    $this->load->model('about/about_model');
    $this->load->model('includes/general_model'); //General Module
    $this->load->helper('time_ago');
    $this->load->model('includes/general_model'); //General Module
    $this->load->helper('text_to_link');
    $this->load->helper('auth');
    auth();
  }

  /* Private function for Favourite Links */

  function _favourite_links_list() {
    $show_limit = 20;
    $user_id = $this->session->userdata('userid');
    $db_favourite_links = $this->general_model->gn_favourite_links($user_id, $show_limit); //General Model function for Interest Groups
    if (!count($db_favourite_links)) {
      $db_favourite_links = array();
    }
    return $db_favourite_links;
  }

  /* Private function for My Reminders */

  function _my_reminders() {
    $user_id = $this->session->userdata('userid');
    $time = time();
    $this->load->model('market_place/market_place_model');
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($user_id);
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($user_id);
    $db_event_reminders = $this->general_model->gn_my_event_reminders($user_id, $time);
    $my_reminders['count_item_sale'] = count($db_item_sale);
    $my_reminders['count_item_requested'] = count($db_item_requested);
    $my_reminders['count_event_reminders'] = count($db_event_reminders);
    return $my_reminders;
  }


//Default page for Home
  function index() {
    //Loged in User
    /*echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
    die();*/


    $page_size = 0;
    $offset = (int)$this->session->userdata('cfgpagelimit');
    
    // SSO Bit
    include APPPATH.'modules/sso/index.php';
    

    $file_output = '';
    //exit;
    $all_feeds_data = $this->_get_all_feeds($page_size,$offset);
    $this->load->model('profile/m_profile');
    $user_email = $this->session->userdata("useremail");
    $data = array('active_tab_all' => 'class="active"',
        'active_tab_status_update' => '',
        'active_tab_news' => '',
        'active_tab_announcement' => '',
        'active_tab_offers' => '',
        'active_tab_files' => '',
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'load_more_request_type' => 'load_all'
    );
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();
    $data['print_output'] =  $all_feeds_data['all_feeds'];
    //Load more Option
    $data['show_load_more'] = true;

    $this->load->view("home_template", $data);
  }
  /* Status Update tab */

  // SSO bit
  function sso() {
     //$sso_dest = $this->uri->segment(3);
    $this->load->view("sso/process");
  }

  function status_updates() {
    $file_output = ""; //initialise

    $page_size = 0;
    $offset = $this->session->userdata('cfgpagelimit');

    //Fetching Status Update Data (CI)
    $file_output = $this->_get_status_updates($page_size, $offset); //status Update function
    $file_output_count = $file_output['data_count'];
    $file_output = $file_output['data'];

    $this->load->model('profile/m_profile');
    $user_email = $this->session->userdata("useremail");
    $data = array('active_tab_all' => '',
        'active_tab_status_update' => 'class="active"',
        'active_tab_news' => '',
        'active_tab_announcement' => '',
        'active_tab_offers' => '',
        'active_tab_files' => '',
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'load_more_request_type' => 'load_status_update'
    );
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();

    //$data['print_output'] = $file_output;
    $data['print_output'] = $file_output;
    $db_msg_count = $file_output_count;
    $page_limit = $offset;

    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }


    $this->load->view("home_template", $data);
  }

  /* News tab */

  function news() {
    $file_output = "";
    $page_size = 0;
    $offset = $this->session->userdata('cfgpagelimit');
    $file_output = $this->_get_mt_news($page_size, $offset);

    $file_output_count = $file_output['data_count'];
    $file_output = $file_output['data'];

    $this->load->model('profile/m_profile');
    $user_email = $this->session->userdata("useremail");

    $data = array('active_tab_all' => '',
        'active_tab_status_update' => '',
        'active_tab_news' => 'class="active"',
        'active_tab_announcement' => '',
        'active_tab_offers' => '',
        'active_tab_files' => '',
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'load_more_request_type' => 'load_mt_news'
    );
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();

    $data['print_output'] = $file_output;
    $db_msg_count = $file_output_count;
    $page_limit = $offset;

    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }

    $this->load->view("home_template", $data);
  }

  /* Announcement tab */

  function announcements() {
    $file_output = "";
    $page_size = 0;
    $offset = $this->session->userdata('cfgpagelimit');
    $file_output = $this->_get_mt_announcements($page_size, $offset);

    $file_output_count = $file_output['data_count'];
    $file_output = $file_output['data'];

    $this->load->model('profile/m_profile');
    $user_email = $this->session->userdata("useremail");

    $data = array('active_tab_all' => '',
        'active_tab_status_update' => '',
        'active_tab_news' => '',
        'active_tab_announcement' => 'class="active"',
        'active_tab_offers' => '',
        'active_tab_files' => '',
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'load_more_request_type' => 'load_mt_announcements'
    );
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();

    $data['print_output'] = $file_output;
    $db_msg_count = $file_output_count;
    $page_limit = $offset;

    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }

    $this->load->view("home_template", $data);
  }

  /* Offers tab */

  function offers() {
    $file_output = "";
    $page_size = 0;
    $offset = $this->session->userdata('cfgpagelimit');
    $file_output = $this->_get_mt_offers($page_size, $offset);

    $this->load->model('profile/m_profile');
    $user_email = $this->session->userdata("useremail");

    $file_output_count = $file_output['data_count'];
    $file_output = $file_output['data'];

    $data = array('active_tab_all' => '',
        'active_tab_status_update' => '',
        'active_tab_news' => '',
        'active_tab_announcement' => '',
        'active_tab_offers' => 'class="active"',
        'active_tab_files' => '',
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'load_more_request_type' => 'load_mt_offers'
    );
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();

    $data['print_output'] = $file_output;
    $db_msg_count = $file_output_count;
    $page_limit = $offset;

    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }

    $this->load->view("home_template", $data);
  }

  /* Files tab */

  function files() {
    //News feeds Time
    $news_feeds_time_limit = mktime((date("H") - $this->news_feeds_time), date("i"), date("s"), date("n"), date("j"), date("Y")); //Unix Timestamp
    //Loged in User
    //$user_id = $this->session->userdata('userid');
    $file_output = ""; //Initialise
    $page_size = 0;
    $offset = $this->session->userdata('cfgpagelimit');
    //Fetching Files Data (CI)
    $file_output = $this->_get_files($page_size, $offset);

    $file_output_count = $file_output['data_count'];
    $file_output = $file_output['data'];

    $this->load->model('profile/m_profile');
    $user_email = $this->session->userdata("useremail");

    $data = array('active_tab_all' => '',
        'active_tab_status_update' => '',
        'active_tab_news' => '',
        'active_tab_announcement' => '',
        'active_tab_offers' => '',
        'active_tab_files' => 'class="active"',
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'load_more_request_type' => 'load_files'
    );
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();
    $data['print_output'] = $file_output;

    $db_msg_count = $file_output_count;
    $page_limit = $offset;
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }
    $this->load->view("home_template", $data);
  }

  /* General Pagination */

//Load More pagination
  function pagination() {
    $request_type = $this->input->post("request_type"); //Which Tab to load

    $offset = $this->session->userdata('cfgpagelimit');
    $pagelimit = $this->input->post("pagesize");

    $output = "";

    switch ($request_type) {
      case "load_status_update": $output = $this->_get_status_updates($pagelimit, $offset);
        $output_data = $output['data'];
        $output_count = $output['data_count'];
        break;
      case "load_mt_news": $output = $this->_get_mt_news($pagelimit, $offset);
        $output_data = $output['data'];
        $output_count = $output['data_count'];
        break;
      case "load_mt_announcements": $output = $this->_get_mt_announcements($pagelimit, $offset);
        $output_data = $output['data'];
        $output_count = $output['data_count'];
        break;
      case "load_mt_offers": $output = $this->_get_mt_offers($pagelimit, $offset);
        $output_data = $output['data'];
        $output_count = $output['data_count'];
        break;
      case "load_files": $output = $this->_get_files($pagelimit, $offset);
        $output_data = $output['data'];
        $output_count = $output['data_count'];
        break;
      case "load_all": $output = $this->_get_all_feeds($pagelimit, $offset);
        $output_data = $output['all_feeds'];
        $output_count = $output['all_feeds_count'];
        break;
      default: break;
    }

    $db_msg_count = $output_count;
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $load_more = true;
    } else {
      $load_more = false;
    }

    $json_arr = array();
    $json_arr['load_more'] = $load_more;
    $json_arr['data'] = $output_data;
    $data['json'] = json_encode($json_arr);

    $this->load->view("print_json", $data);
    //echo $file_output;
  }

  /* General Tab Loading functino below */

  //Status update
  function _get_status_updates($pagelimit, $offset) {
    $user_id = $this->session->userdata('userid');
    $general_time_check = array(); //Stores all the timestamp from different sections
    $file_output = array(); //initialise
    $status_msg = "";
    $page_size = (int) $pagelimit;
    $offset = (int) $offset;

    //Fetching Status Update Data (CI)
    $ci_status_update_data = $this->home_model->get_last_status_update($page_size, $offset, $user_id);
    //Customizing the Database field
    $status_update_data = array();
    for ($i = 0; $i < count($ci_status_update_data); $i++) {
      $message_time = $ci_status_update_data[$i]['message_time'];
      $ci_status_update_data[$i]['message_time'] = time_ago(strtotime($message_time));
      $email = $ci_status_update_data[$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $ci_status_update_data[$i]['reply_user'] = $reply_user;

      $status_update_data[] = $ci_status_update_data[$i]; //main Files Data Store
    }
    if (count($status_update_data)) {
      //echo $status_update_data[$timestamp]['first_name']."<br />";
      foreach ($status_update_data as $updates) {
        //$message = (strlen($updates['message']) > 140)?substr($updates['message'],0,140)."..":$updates['message'];
        $message = $updates['message'];

        if ($this->session->userdata('userid') == $updates['user_id']) {
          $follow_unfollow_link = '<li><a class="msg-delete" id="' . $updates['message_id'] . '" href="#">Delete</a></li>';
        } else {
          $msg_class = ($updates['following_user_id']) ? "msg-unfollow" : "msg-follow";
          $msg_status = ($updates['following_user_id']) ? "Unfollow" : "Follow";

          $follow_unfollow_link = '<li><a rel="pop_win" class="msg-report" href="' . site_url("profile/report_status_update/" . $updates['message_id']) . '" class="msg-report" href="#">Report</a></li>
            <li><a class="' . $msg_class . '" rel="' . $updates['user_id'] . '" href="#">' . $msg_status . '</a></li>';
        }


        $status_msg .= '<li class="type2 hentry">
						<div class="holder">
							<div class="frame">
								<span class="visual">
									<img src="' . base_url() . 'images/user-images/105x101/' . $updates['profile_pic'] . '" alt="' . $updates['first_name'] . '" width="53" height="53" />
								</span>
								<div class="text">
									<strong class="entry-title"><a href="' . site_url("profile/view_profile/" . $updates['user_id']) . '">' . ucfirst(strtolower($updates['first_name'])) . " " . ucfirst(strtolower($updates['last_name'])) . '</a><em>' . $updates['designation'] . ', ' . $updates['concept'] . '</em></strong>
									<p>' . text_to_link($message) . '</p>
									<span>' . $updates['message_time'] . '</span>
									<ul class="actions">
										<li><a class="msg-reply" id="' . $updates['reply_user'] . '" href="#">Reply</a></li>
										<li><a href="mailto:' . $updates['email'] . '">Email</a></li>
										' . $follow_unfollow_link . '
									</ul>
								</div>
							</div>
						</div>
					</li>';
      }
    } else {
      $status_msg = "";
    }
    $file_output['data'] = $status_msg;
    $file_output['data_count'] = count($status_update_data);
    return $file_output;
  }

  //Get News
  function _get_mt_news($page_size, $offset) {
    $file_output = array();
    $page_size = (int) $page_size;
    $offset = (int) $offset;
    $news_data = array();
    $news_output = "";
    //Fetching News Data(MT)
    $mt_news_data = $this->home_model->get_last_data_from_mt($this->mt_category_news, $page_size, $offset);
    //echo "<xmp>".print_r($mt_news_data,1)."</xmp>";exit;
    //Customizing the Database field
    for ($i = 0; $i < count($mt_news_data); $i++) {
      $entry_time = $mt_news_data[$i]['entry_created_on'];
      $entry_id = $mt_news_data[$i]['entry_id'];
      $entry_text_more = $mt_news_data[$i]['entry_text_more'];
      $entry_title = $mt_news_data[$i]['entry_title'];

      //Custom Field
      $entry_time = strtotime($entry_time);

      $news_content['entry_id'] = $entry_id;
      $news_content['entry_time'] = time_ago($entry_time);
      $news_content['entry_text_more'] = $entry_text_more;
      $news_content['entry_title'] = $entry_title;

      $album_id = (int) $mt_news_data[$i]['entry_excerpt'];
      $news_thumb = "";
      if ($album_id) {
        $db_new_album = $this->about_model->alb_get_all_album_pics($album_id, 3, 0);
        $news_thumb = '<ul class="photos">';
        foreach ($db_new_album as $album_pic) {
          $thumbnail = $album_pic['image_path'] . $album_pic['sub_dir'] . $album_pic['filepath'] . $album_pic['thumb'] . $album_pic['filename'];
          $album_link = "about/view_photos/" . $album_id;
          //$news_thumb .='<li><a href="'.site_url($album_link).'"><img alt="News thumb" src="'.$thumbnail.'" width="130" height="85" /></a></li>';
          $news_thumb .='<li><a href="' . site_url($album_link) . '"><img alt="News thumb" src="' . $thumbnail . '" /></a></li>';
        }
        $news_thumb .= '</ul>';
      }
      $news_content['thumbnails'] = $news_thumb;


      $news_data[] = $news_content; //main News Data Store
    }
    if (count($news_data)) {
      //echo $status_update_data[$timestamp]['first_name']."<br />";
      foreach ($news_data as $data) {
        //echo $news_data[$timestamp]['entry_title']."<br />";
        $news_output .= '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-4.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("news/news_details/" . $data['entry_id']) . '">' . $data['entry_title'] . '</a></strong>
										<p>' . $data['entry_text_more'] . '</p>
										' . $data['thumbnails'] . '
										<span class="author vcard">In <a href="' . site_url("news/") . '">News</a>, ' . $data['entry_time'] . '</span>
									</div>
								</div>
							</div>
						</li>';
      }
    }
    $file_output['data'] = $news_output;
    $file_output['data_count'] = count($mt_news_data);
    return $file_output;
  }
  //Get Announcements
  function _get_mt_announcements($page_size, $offset) {
    $announcement_data = array();
    $file_output = array();
    $announcements = "";
    $page_size = (int) $page_size;
    $offset = (int) $offset;
    //Fetching Announcement Data(MT)
    $mt_announcement_data = $this->home_model->get_last_data_from_mt($this->mt_category_announcement, $page_size, $offset);
    //Customizing the Database field
    for ($i = 0; $i < count($mt_announcement_data); $i++) {
      $entry_time = $mt_announcement_data[$i]['entry_created_on'];
      $entry_id = $mt_announcement_data[$i]['entry_id'];
      $entry_text_more = $mt_announcement_data[$i]['entry_text_more'];
      $entry_title = $mt_announcement_data[$i]['entry_title'];

      //Custom Field
      $entry_time = strtotime($entry_time);

      $announcement_content['entry_id'] = $entry_id;
      $announcement_content['entry_time'] = time_ago($entry_time);
      $announcement_content['entry_text_more'] = $entry_text_more;
      $announcement_content['entry_title'] = $entry_title;

      $announcement_data[] = $announcement_content; //main Announcement Data Store
    }

    if (count($announcement_data)) {
      //echo $status_update_data[$timestamp]['first_name']."<br />";
      foreach ($announcement_data as $data) {
        //echo $news_data[$timestamp]['entry_title']."<br />";
        $announcements .= '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-5.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("announcement/announcement_details/" . $data['entry_id']) . '">' . $data['entry_title'] . '</a></strong>
										<p>' . $data['entry_text_more'] . '</p>
										<span class="author vcard">In <a href="' . site_url("announcement/") . '">Announcements</a>, ' . $data['entry_time'] . '</span>
									</div>
								</div>
							</div>
						</li>';
      }
    }

    $file_output['data'] = $announcements;
    $file_output['data_count'] = count($mt_announcement_data);

    return $file_output;
  }
  //Get Offers
  function _get_mt_offers($page_size, $offset) {
    $offers_data = array();
    $file_output = array();
    $offers = "";
    $page_size = (int) $page_size;
    $offset = (int) $offset;
    //Fetching Offers Data(MT)
    $mt_offers_data = $this->home_model->get_last_data_from_mt($this->mt_category_offers, $page_size, $offset);
    //Customizing the Database field
    for ($i = 0; $i < count($mt_offers_data); $i++) {
      $entry_time = $mt_offers_data[$i]['entry_created_on'];
      $entry_id = $mt_offers_data[$i]['entry_id'];
      $entry_text_more = $mt_offers_data[$i]['entry_text_more'];
      $entry_title = $mt_offers_data[$i]['entry_title'];

      //Custom Field
      $entry_time = strtotime($entry_time);

      $offers_content['entry_id'] = $entry_id;
      $offers_content['entry_time'] = time_ago($entry_time);
      $offers_content['entry_title'] = $entry_title;
      $offers_content['entry_text_more'] = $entry_text_more;

      $offers_data[] = $offers_content; //main Offers Data Store
    }
    if (count($offers_data)) {
      //echo $status_update_data[$timestamp]['first_name']."<br />";
      foreach ($offers_data as $data) {
        $offers .= '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-7.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("offers/offers_details/" . $data['entry_id']) . '">' . $data['entry_title'] . '</a></strong>
										<p>' . $data['entry_text_more'] . '</p>
										<span class="author vcard">In <a href="' . site_url("offers/") . '">Offers</a>, ' . $data['entry_time'] . '</span>
									</div>
								</div>
							</div>
						</li>';
      }
    }

    $file_output['data'] = $offers;
    $file_output['data_count'] = count($mt_offers_data);

    return $file_output;
  }
  //Get Files
  function _get_files($page_size, $offset) {
    $file_output = "";
    $files_data = array();
    $files = "";
    $page_size = (int) $page_size;
    $offset = (int) $offset;
    //Fetching Files Data (CI)
    $ci_files_data = $this->home_model->get_last_all_files($page_size, $offset);
    //Customizing the Database field
    for ($i = 0; $i < count($ci_files_data); $i++) {
      $uploaded_on = $ci_files_data[$i]['uploaded_on'];
      $ci_files_data[$i]['uploaded_on'] = time_ago($uploaded_on);
      $files_data[] = $ci_files_data[$i]; //main Files Data Store
    }
    if (count($files_data)) {
      //echo $status_update_data[$timestamp]['first_name']."<br />";
      foreach ($files_data as $data) {
        if ($data['file_size']) {
          $link_status = '';
        } else {
          $link_status = ' target="_blank" ';
        }
        $file_desc = (strlen($data['file_desc']) > 140) ? substr($data['file_desc'], 0, 140) . ".." : $data['file_desc'];
        $files .= '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-3.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a ' . $link_status . ' href="' . site_url("files/files_download/" . $data['unique_id']) . '">' . $data['title'] . '</a></strong>
										<p>' . $file_desc . '</p>
										<span class="author vcard">In <a href="' . site_url("files") . '">Files</a>, ' . $data['uploaded_on'] . '</span>
									</div>
								</div>
							</div>
						</li>';
      }
    }

    $file_output['data'] = $files;
    $file_output['data_count'] = count($ci_files_data);

    return $file_output;
  }
//Code of conduct page
  function code_of_conduct() {
    $this->load->model("profile/m_profile");
    $user_email = $this->session->userdata("useremail");
    $data['gn_favourite_links_list'] = $this->_favourite_links_list();
    $data['gn_my_reminders'] = $this->_my_reminders();
    $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email);

    $this->load->view("code_of_conduct_template", $data);
  }
//Release notes page
  function release_notes() {
    $this->load->view("release_notes_template");
  }
//Feedback page
  function feedback() {
    $this->load->view("feedback_template");
  }
//Feed back page submit action
  function submit_feedback() {
    $this->load->library('email');

    $querytype = $this->input->post("query");
    $name = $this->input->post("name");
    $desc = $this->input->post("desc");
    $reachtype = $this->input->post("reachtype");
    $contact = $this->input->post("contact");

    $feedback_details = array(
        'id' => NULL,
        'userfeedback_name' => $name,
        'userfeedback_categoryid' => $querytype,
        'userfeedback_description' => $desc,
        'userfeedback_contacttypeid' => $reachtype,
        'userfeedback_contact' => $contact
    );
    $feedback_id = $this->home_model->submit_feedback_form($feedback_details);
    if ($feedback_id) {
      if ($reachtype == 1) {
        $reach = 'Email';
      }
      if ($reachtype == 2) {
        $reach = 'Phone';
      }
      if ($reachtype == 3) {
        $reach = 'Mail';
      }

      if ($querytype == 1) {
        $query = 'Question on Landmark Intranet';
      }
      if ($querytype == 2) {
        $query = 'Comment on Landmark Intranet';
      }
      if ($querytype == 3) {
        $query = 'Complaint against a Landmark Intranet';
      }
      if ($querytype == 4) {
        $query = 'Compliment on a Landmark Intranet';
      }
      if ($querytype == 5) {
        $query = 'Comment on Landmark Intranet';
      }

      $msg = "<p>Hi,<br>A feedback has been posted at <a href='http://www.intranet.landmarkgroup.com'>www.intranet.landmarkgroup.com</a> which you might be interested in.</p><table cellpadding='0' border='0'><tr><td width='30%' style='border-bottom:1px solid #cccccc;padding:3px 0px;'>Feedback from</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $name . "</td></tr><tr><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>You can contact the sender at</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $contact . "</td></tr><tr><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>Feedback category</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $query . "</td></tr><tr><td style='border-bottom:1px solid #cccccc;padding:3px 0px;' valign='top'>Feedback</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $desc . "</td></tr></table><br><p>Admin,<br><a href='http://www.intranet.landmarkgroup.com'>www.intranet.landmarkgroup.com</a></p><p>NOTE: This is a system generated email, please do not try to reply to the email.</p>";
      $subject = "User Feedback from Landmark Intranet";
      $sender_email = "no-reply@landmarkgroup.com";
      $sender_name = "Intranet Feedback";
      $this->load->helper('smtp_config');
      /*
      $config = array();
      $config['protocol'] = 'smtp';
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
      $config['mailtype'] = "html";
      */
      $config = smtp_config();
      $this->email->initialize($config);

      $this->email->from($sender_email, $sender_name);
      $this->email->to("savitar.jagtiani@cplmg.com,shubham.puranik@cplmg.com,swati.kothari@cplmg.com");
      $this->email->subject($subject);
      $this->email->message($msg);
      $this->email->send();
      echo "<strong>Thanks for your thoughts.</strong>";
    } else {
      echo "<strong>Submission Failed. Please try again.</strong>";
    }
  }

  //hide announcement bar action
  function hide_announcement_bar(){
    $ann_id = $this->input->post('ann_id');
    $user_id = $this->session->userdata('userid');
    $insert_arr = array('id'=>null,'user_id'=>$user_id,'announcement_bar_id'=>$ann_id);
    $this->home_model->hide_announcement_bar($insert_arr);
    
    $json_arr = array();
    $json_arr['status'] = 1;
    echo json_encode($json_arr);
   
  }

  //When user clicks on Quick search this function will store the clicked user details
  function recode_quick_search(){
    $log_data = array(
       'id' => null ,
       'user_id' =>  $this->session->userdata('userid'),
       'log_datetime' => date('Y-m-d H:i:s')
    );

    return $this->db->insert('ci_quick_search_log', $log_data);

  }
  //This is a private function which gives a collective output for all the feed data
  function _get_all_feeds($page_size,$offset){
    $file_output  = '';
    $user_id = $this->session->userdata('userid');
    $page_size = (int)$page_size;
    $offset = (int)$offset;
    $home_feed = $this->home_model->home_page_all_feed($user_id,$page_size,$offset);
    //echo "<xmp>" . print_r($home_feed, 1) . "</xmp>";
    //exit;
    for ($i = 0; $i < count($home_feed); $i++) {
      /* Album */
      if ($home_feed[$i]['category'] == 'album') {
        $album_id = (int) $home_feed[$i]['id'];
        $alb_thumb = "";
        if ($album_id) {
          $db_new_album = $this->about_model->alb_get_all_album_pics($album_id, 3, 0);
          $alb_thumb = '<ul class="photos">';
          foreach ($db_new_album as $album_pic) {
            $thumbnail = $album_pic['image_path'] . $album_pic['sub_dir'] . $album_pic['filepath'] . $album_pic['thumb'] . $album_pic['filename'];
            $album_link = "about/view_photos/" . $album_id;
            //$news_thumb .='<li><a href="'.site_url($album_link).'"><img alt="News thumb" src="'.$thumbnail.'" width="130" height="85" /></a></li>';
            $alb_thumb .='<li><a href="' . site_url($album_link) . '"><img alt="Album thumb" src="' . $thumbnail . '" /></a></li>';
          }
          $alb_thumb .= '</ul>';
        }

        $tmp_alb = '<li class="hentry">
              <div class="holder">
                <div class="frame">
                  <span class="visual">
                    <img src="' . base_url() . 'images/ico-4.gif" alt="image description" width="15" height="16" />
                  </span>
                  <div class="text">
                    <strong class="entry-title"><a href="' . site_url("about/view_photos/" . $home_feed[$i]['id']) . '">' . $home_feed[$i]['title'] . '</a></strong>
                    <p>' . $home_feed[$i]['description'] . '</p>
                    ' . $alb_thumb . '
                    <span class="author vcard">In <a href="' . site_url("about/photos") . '">Albums</a>, ' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
                  </div>
                </div>
              </div>
            </li>';

          $file_output .= $tmp_alb;

      }

      /* Event */
      if ($home_feed[$i]['category'] == 'events') {
        //echo $news_data[$timestamp]['entry_title']."<br />";
        $tmp_events = '<li class="hentry">
              <div class="holder">
                <div class="frame">
                  <span class="visual">
                    <img src="' . base_url() . 'images/ico-8.gif" alt="image description" width="15" height="16" />
                  </span>
                  <div class="text">
                    <strong class="entry-title"><a href="' . site_url("events/event_detail/" . $home_feed[$i]['id']) . '">' . $home_feed[$i]['title'] . '</a></strong>
                    <p>' . $home_feed[$i]['description'] . '</p>
                    ' . $events_data[$timestamp]['thumbnails'] . '
                    <span class="author vcard">In <a href="' . site_url("events/") . '">Events</a>, ' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
                  </div>
                </div>
              </div>
            </li>';

          $file_output .= $tmp_events;
      }


      /* News */
      if ($home_feed[$i]['category'] == 'news') {
        //echo $news_data[$timestamp]['entry_title']."<br />";
        $album_id = (int) $home_feed[$i]['album_id'];
        $alb_thumb = "";
        if ($album_id) {
          $db_new_album = $this->about_model->alb_get_all_album_pics($album_id, 3, 0);
          $alb_thumb = '<ul class="photos">';
          foreach ($db_new_album as $album_pic) {
            $thumbnail = $album_pic['image_path'] . $album_pic['sub_dir'] . $album_pic['filepath'] . $album_pic['thumb'] . $album_pic['filename'];
            $album_link = "about/view_photos/" . $album_id;
            //$news_thumb .='<li><a href="'.site_url($album_link).'"><img alt="News thumb" src="'.$thumbnail.'" width="130" height="85" /></a></li>';
            $alb_thumb .='<li><a href="' . site_url($album_link) . '"><img alt="News thumb" src="' . $thumbnail . '" /></a></li>';
          }
          $alb_thumb .= '</ul>';
        }
        $tmp_news = '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-4.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("news/news_details/" . $home_feed[$i]['id']) . '">' . $home_feed[$i]['title'] . '</a></strong>
										<p>' . $home_feed[$i]['description'] . '</p>
										' . $alb_thumb . '
										<span class="author vcard">In <a href="' . site_url("news/") . '">News</a>, ' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
									</div>
								</div>
							</div>
						</li>';

          $file_output .= $tmp_news;
      }
      /* Announcement  */
      if ($home_feed[$i]['category'] == 'announcement') {
        $tmp_announcement = '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-5.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("announcement/announcement_details/" . $home_feed[$i]['id']) . '">' . $home_feed[$i]['title'] . '</a></strong>
										<p>' . $home_feed[$i]['description'] . '</p>
										<span class="author vcard">In <a href="' . site_url("announcement/") . '">Announcements</a>, ' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
									</div>
								</div>
							</div>
						</li>';


          $file_output .= $tmp_announcement;

      }
      /* Offers  */
      if ($home_feed[$i]['category'] == 'offers') {
        $tmp_offer = '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-7.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("offers/offers_details/" . $home_feed[$i]['id']) . '">' . $home_feed[$i]['title'] . '</a></strong>
										<p>' . $home_feed[$i]['description'] . '</p>
										<span class="author vcard">In <a href="' . site_url("offers/") . '">Offers</a>, ' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
									</div>
								</div>
							</div>
						</li>';

          $file_output .= $tmp_offer;
      }
      //echo "<xmp>".print_r($files_data,1)."</xmp>";exit;
      /* Files */
      if ($home_feed[$i]['category'] == 'files') {

        if ($home_feed[$i]['file_size']) {
          $link_status = '';
        } else {
          $link_status = ' target="_blank" ';
        }


        $file_desc = (strlen($home_feed[$i]['description']) > 140) ? substr($home_feed[$i]['description'], 0, 140) . ".." : $home_feed[$i]['description'];
        $file_output .= '<li class="hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/ico-3.gif" alt="image description" width="15" height="16" />
									</span>
									<div class="text">
										<strong class="entry-title"><a ' . $link_status . ' href="' . site_url("files/files_download/" . $home_feed[$i]['file_unique_id']) . '">' . $home_feed[$i]['title'] . '</a></strong>
										<p>' . $file_desc . '</p>
										<span class="author vcard">In <a href="' . site_url("files") . '">Files</a> , ' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
									</div>
								</div>
							</div>
						</li>';
      }

      /* Status updates */
      if ($home_feed[$i]['category'] == 'status updates') {
        //echo $status_update_data[$timestamp]['first_name']."<br />";
        //$message = (strlen($status_update_data[$timestamp]['message']) > 140)?substr($status_update_data[$timestamp]['message'],0,140)."..":$status_update_data[$timestamp]['message'];
        $message = $home_feed[$i]['message'];

        if ($this->session->userdata('userid') == $home_feed[$i]['user_id']) {
          $follow_unfollow_link = '<li><a class="msg-delete" id="' . $home_feed[$i]['message_id'] . '" href="#">Delete</a></li>';
        } else {
          $msg_class = ($home_feed[$i]['following_user_id']) ? "msg-unfollow" : "msg-follow";
          $msg_status = ($home_feed[$i]['following_user_id']) ? "Unfollow" : "Follow";

          $follow_unfollow_link = '<li><a rel="pop_win" class="msg-report" href="' . site_url("profile/report_status_update/" . $home_feed[$i]['message_id']) . '">Report</a></li>
            <li><a class="' . $msg_class . '" rel="' . $home_feed[$i]['user_id'] . '" href="#">' . $msg_status . '</a></li>';
        }

        list($reply_user, $domain) = @explode("@", $home_feed[$i]['email']); //Reply user name is the first part of the email address
        $file_output .= '<li class="type2 hentry">
							<div class="holder">
								<div class="frame">
									<span class="visual">
										<img src="' . base_url() . 'images/user-images/105x101/' . $home_feed[$i]['profile_pic'] . '" alt="' . $home_feed[$i]['first_name'] . '" width="53" height="53" />
									</span>
									<div class="text">
										<strong class="entry-title"><a href="' . site_url("profile/view_profile/" . $home_feed[$i]['user_id']) . '">' . ucfirst(strtolower($home_feed[$i]['first_name'])) . " " . ucfirst(strtolower($home_feed[$i]['last_name'])) . '</a><em>' . $home_feed[$i]['designation'] . ', ' . $home_feed[$i]['concept'] . '</em></strong>
										<p>' . text_to_link($message) . '</p>
										<span>' . time_ago(strtotime($home_feed[$i]['publish_date'])) . '</span>
										<ul class="actions">
											<li><a class="msg-reply" id="' . $reply_user . '" href="#">Reply</a></li>
                      <li><a href="mailto:' . $home_feed[$i]['email'] . '">Email</a></li>
                      ' . $follow_unfollow_link . '
										</ul>
									</div>
								</div>
							</div>
						</li>';
      }
    }

    $all_feeds['all_feeds'] = $file_output;
    $all_feeds['all_feeds_count'] = count($home_feed);
    return $all_feeds;

  }
}

/* End of file home.php */
/* Location: ./system/application/controllers/home.php */