<?php
$active_tab['photos_active'] = '';
$active_tab['home_active'] = 'class="active"';
$active_tab['people_active'] = '';
$active_tab['market_place_active'] = '';
$active_tab['files_active'] = '';
$active_tab['events_active'] = '';
$active_tab['about_lmg_active'] = '';
$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Intranet</title>
<?php $this->load->view("include_files/common_files"); ?>
    <style type="text/css">
      .simple-page #main{padding-top:20px;}
      .mainicon { padding: 10px;}
      h3 {font-family:arial; font-size: 13px; margin-bottom: 10px;}
      p {font-family:arial; font-size: 12px; line-height:20px; margin-top: 7px;margin-left: 10px;}
      .iconcell { vertical-align: top; padding: 5px;}
      .release_notes li{margin-top:5px;}
    </style>


  </head>
  <body>
<?php $this->load->view("includes/admin_nav"); ?>
    <!-- wrapper -->
    <div id="wrapper" class="simple-page">
      <!-- logo -->
      <h1 class="logo vcard"><a href="<?= base_url() ?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
      <!-- main -->
      <div id="main" class="release_notes">
        <div id="rev-content">
          <div id="revisions">
            <p>These release notes highlight what’s new and what’s been fixed on Landmark Intranet.</p>

            <h3 class="even acc_header">Version 1.4 - Sep 2011</h3>
            <div style="display: none;" class="revision even">

              <h2>New Feature</h2>
              <ul>
                <li>An instant messaging functionality has been added to the intranet that will enable the intranet users to interact with each other in real time.</li>
              </ul>
              <h2>Highlights of IM</h2>
              <ul>
                <li>Intuitive chat design: Intranet users can start using Instant Messenger the instant they login to the intranet. The Who's Online list will automatically display all online users and a single click will enable them to start chatting with others in real-time.</li>
                <li>Unlimited chat conversations: A user can chat with one, five or even twenty users simultaneously.</li>
                <li>Instant notifications: User online/offline: IM will notify the intranet users when the other users are online and available to chat.</li>
                <li>Notifications for new messages: IM will display a pop-up indicating the number of new messages, flicker the website title as well as provide a sound notification whenever a new message arrives. The sound notification can be disabled from the options menu.</li>
              </ul>
            </div>

            <h3 class="even acc_header">Version 1.3 - August 2011</h3>
            <div style="display: none;" class="revision even">

              <h2>Improvements</h2>
              <ul>
                <li>The <a href="<?=site_url("people")?>">Company directory</a> has been redesigned – with a more streamlined and user friendly design, new improved features like star your favourite contacts, better search functionality and a comprehensive search filter.</li>
                <li>The auto-suggest functionality for people search has been added in the global search bar.</li>
                <li>A "Delete" button has been added under <a href="<?=site_url("files/your_files/")?>">Your Files</a> section.</li>
                <li>"See more" link has been added for My Interest groups under <a href="<?=site_url("profile/myprofile")?>">profile page</a> to view more interests.</li>
                <li><a href="<?=site_url("offers")?>">Set your offer preferences</a> feature has been modified, allowing users to choose concepts from which they would like to receive offers.</li>
              </ul>
              <h2>Bug Fixes</h2>
              <ul>
                <li>A glitch restricting the IE8 users to post in the marketplace has been fixed.</li>
                <li>A bug restricting the IE8 user to upload files with bigger file size has been fixed.</li>
                <li>Photo album is now set to choose the first uploaded picture as the cover thumbnail if no cover image is selected.</li>
                <li>Other minor bug fixes.</li>
              </ul>
            </div>

            <h3 class="even acc_header">Version 1.2.1 - March 2011</h3>
            <div style="display: none;" class="revision even">            

              <h2>Improvements</h2>
              <ul>
                <li>Redesigned the Intranet login page to showcase key features upfront.</li>
                <li>Changed Daily Feed to <a href="<?= site_url('home') ?>" >News feed</a> to display 20 recent Intranet activities.</li>
                <li>Redesigned the <a href="<?= site_url('files/show_files') ?>" >Files section</a> home page with file thumbnails for added clarity.</li>
                <li>Surfaced employee mobile numbers to display upfront on search under <a href="<?= site_url('people') ?>" >Company directory</a>.</li>
              </ul>
            </div>




            <h3 class="even acc_header">Version 1.2 - January 2011</h3>
            <div style="display: none;" class="revision even">            

              <h2>Improvements</h2>
              <ul>
                <li>Visual indicator for new replies in <a href="<?= site_url("status_update") ?>">My Replies</a>.</li>
                <li><a href="<?= site_url("interest_groups") ?>">Interest Groups</a> names are now limited to 15 characters.</li>
                <li><a href="<?= site_url("files/show_files") ?>">Important files</a> can be downloaded directly by clicking on the file name.</li>
                <li>"Remember me" option added to the login page; to remember username and password for 24 hours.</li>
              </ul>

              <h2>Other Updates</h2>
              <ul>
                <li>Created a new Intranet bookmark icon for iPhone and iPad users.</li>
                <li>Launched the new admin tool for marketing teams to post Offers, News, Announcements, Events and Photos on the Intranet.</li>
              </ul>
            </div>



            <h3 class="even acc_header">Version 1.1 - November 2010</h3>
            <div style="display: none;" class="revision even">

              <h2>Improvements</h2>
              <ul>
                <li>A 'Report Data Error' link has been added in the Company Directory that allows users to submit data correction requests.</li>
                <li>A new 'My Replies' link has been added in People > Status Updates section, which allows you to track your own replies.</li>
                <li>A 'Feedback' link has been added in the footer, which allows users to easily provide their feedback as they navigate the Intranet.</li>
                <li>A 'Code of Conduct' link can be found in the footer, which describes the standard of conduct for all in the Intranet community.</li>
              </ul>

              <h2>Other Updates</h2>
              <ul>
                <li>Status updates are now limited to 160 characters.</li>
              </ul>

              <h2>Bug Fixes</h2>
              <ul>
                <li>Misaligned text field and text wrapping glitch in the Drop Box section has been fixed.</li>
                <li>A bug causing slow retrieval of events from the Events Calendar has been fixed.</li>
              </ul>

            </div>

          </div>

        </div>

      </div>
      <!-- header -->
      <div id="header">
        <div class="header-holder">
          <!-- navigation -->
<?php $this->load->view("includes/navigation", $active_tab); ?>
          <div class="head-bar">
            <!-- heading -->
            <div class="heading">
              <h2>Release Notes</h2>
            </div>
            <!-- header search form -->
<?php $this->load->view("includes/search_form"); ?>
          </div>
        </div>
      </div>
      <!-- footer -->
<?php $this->load->view("includes/footer"); ?>
    </div>
  </body>
</html>