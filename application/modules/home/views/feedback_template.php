<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = 'class="active"';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
  <link media="all" rel="stylesheet" type="text/css" href="<?=base_url()?>css/form.css" />
  <script type="text/javascript" src="<?=base_url()?>js/form.js"></script>
  <script type="text/javascript" src="<?=base_url()?>js/custom_js/feedback.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper" class="simple-page">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
      
      <form class="contact" action="javascript:void(0);">
        <label for="query">Categorize your feedback</label>
          <select name="query" class="width470">
          <option value="1" selected="selected">Question</option>
          <option value="2">Suggestions</option>
          <option value="3">Complaint</option>
          <option value="4">Praise</option>
        </select>
        <div class="cboth pt15"><label for="query">Describe your query or feedback</label>
          <div class="customtextarea">
            <div class="txtareatop"></div>
            <div class="txtareamid">  
              <textarea name="describe"></textarea>
            </div>
            <div class="txtareabot"></div>
          </div>
        </div>
        <div class="error" id="desc_error"></div>
          <div class="cboth ">
              <div class="custominput">
                <input type="hidden" name="name" class="width450" value="<?= $this->session->userdata("fullname") ?>" />
              </div>
        </div>
        <div class="error" id="name_error"></div>
        <div class="cboth ">
              <div>
                  <!--select name="reachtype" class="width100 fleft" >
              <option value="1" selected="selected">By Email</option>
                  </select-->
                  <input type="hidden" name="reachtype" class="width360" value="1" />
                  <input type="hidden" name="contact" class="width360" value="<?= $this->session->userdata("useremail") ?>" />
                  <div class="custominput cmargin fleft">
              
            </div>
              </div>
        </div>
          <div class="error" id="contact_error"></div>
        <div class="cboth pt15">
          <input type="image" src="<?=base_url()?>images/feedback/submit-btn.gif" id="feedbacksubmit" style="width:74px;height:27px;float:right; padding-right:450px;"/>
          <div id="load" style="display:none;"><img src="<?=base_url()?>images/feedback/ajax_loading.gif"/></div>
          </div>
      </form>

		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- heading -->
					<div class="heading">
						<h2>Feedback</h2>
					</div>
					<!-- header search form -->
          <?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>