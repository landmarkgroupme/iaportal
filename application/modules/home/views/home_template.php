<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = 'class="active"';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/home/home.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
	 <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
			
			<?php
			 $message = $this->home_model->get_announcement_bar_text($this->session->userdata('userid'));
			 if(!empty($message)):
			?>
				<!-- message box -->
				<div class="message">
				  <div id="announcement-bar-msg" ><a id="<?=$message['id'];?>" class="close-announcement" title="Hide Announcement" href="#"><img src="<?=base_url()?>images/close-announcementbar.png" alt="close" /></a></div>
					<?=$message['announcement_bar_text'];?>
				</div>
				<!-- status box -->
			<?php endif;?>
				<form action="<?=site_url("status_update/submit_status_message")?>" method="post" class="share-form-textarea">
          <fieldset>
            <textarea class="textarea" name="txtarShare" id="txtarShare" rows="4" cols="4">What are you working on ?</textarea>
            <input type="image" id="btn-ar-share" src="<?=base_url();?>images/btn-share-txtar.jpg" alt="Share!"/><div id="status-msg-count">160</div>
          </fieldset>
        </form><br />
				<div id="status-msg-status" class="msg-ok"></div>
				<!-- posts box -->
				<div class="posts-box">
					<div class="options">
						<!-- filters list -->
						<div class="filters">
							<strong>Show:</strong>
							<ul>
								<li <?=$active_tab_all?>><a href="<?=site_url("home/")?>"><span><em>All</em></span></a></li>
								<li <?=$active_tab_status_update?>><a href="<?=site_url("home/status_updates")?>"><span><em>Status Updates</em></span></a></li>
								<li <?=$active_tab_news?>><a href="<?=site_url("home/news")?>"><span><em>News</em></span></a></li>
								<li <?=$active_tab_announcement?>><a href="<?=site_url("home/announcements")?>"><span><em>Announcements</em></span></a></li>
								<li <?=$active_tab_offers?>><a href="<?=site_url("home/offers")?>"><span><em>Offers</em></span></a></li>
								<li <?=$active_tab_files?>><a href="<?=site_url("home/files")?>"><span><em>Files</em></span></a></li>
							</ul>
						</div>
						<!-- view list -->
						<!-- REMOVED after discussion with Savitar on 10 Sept 2010
						<ul class="view">
							<li><a href="#" id="view-details" class="detail">Detail</a></li>
							<li><a href="#" id="view-list" class="list">List</a></li>
						</ul>						
						-->
					</div>
					<!-- posts list -->
					<ul id="show-list" class="posts-list hfeed">
						<?=$print_output?>
					</ul>
				</div>
				<?php if($show_load_more):?>
				  <a rel="<?=$load_more_request_type?>" href="<?=site_url("home/pagination")?>" id="<?=$this->session->userdata('cfgpagelimit')?>" class="more-stories">View More</a>
				<?php endif; ?>  
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- sidebar box, Useful link box -->
				<?php $this->load->view("includes/useful_link");?>
				
				<!-- sidebar box, Latest update box -->
				<?php $this->load->view("includes/latest_update");?>
        
        
				<!-- sidebar box, Photo Album box -->
        <?php $this->load->view("includes/latest_photo_album");?>
				
				
				<?php //$this->load->view("includes/useful_link");?>
				<!-- sidebar box, My Reminders -->
				<?php //$this->load->view("includes/latest_update");?>
				<!-- sidebar box, My Favourites -->
				 <?php //$this->load->view("includes/latest_update");?>
				
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- heading -->
					<div class="heading">
						<h2>News Feed</h2>
						<!--a href="#" tabindex="15">Edit Feed Settings</a-->
					</div>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
	
</body>
</html>