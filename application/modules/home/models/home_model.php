<?php
Class Home_model extends CI_Model {
	function Home_model() {
		parent::__construct();
	}
  function get_data_from_alb($time_limit){
    $sql = "SELECT 
              a.aid,
              a.title,
              a.description,
              b.ctime
            FROM 
              alb_albums a,
              alb_pictures b
            WHERE 
              visibility = 0 AND
              a.aid = b.aid AND
              b.ctime > ?
            GROUP BY b.aid ";
    $result= $this->db->query($sql,array($time_limit));
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  //Fetching Events 
  function events_get_all(){
    $sql = "SELECT
      a.id,
      a.title,
      a.description,
      a.added_on,
      (SELECT album_id FROM ci_event_album WHERE event_id = a.id) as album_id
    FROM 
      `ci_event` as a
    WHERE 
      a.event_status = 1
    ORDER BY a.added_on DESC LIMIT 20";
      
    $result=$this->db->query($sql);
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  } 
//Fetching Data From  MT tables for categories(News, announcement, Offers) with Time
	function get_data_from_mt($mt_category,$time =null){
	/*	 $sql = 'SELECT 
							a.entry_id,
							a.entry_created_on,
							a.entry_text_more,
							a.entry_title,
							a.entry_excerpt
						FROM 
							mt_entry a,
							mt_category b,
							mt_placement c
						WHERE
							a.entry_status = 2 AND
							a.entry_created_on > "'.$time.'" AND
							b.category_basename = ? AND
							b.category_class = "category" AND
							b.category_id = c.placement_category_id AND
							c.placement_entry_id = a.entry_id   
						ORDER BY a.entry_created_on DESC ' ;
   */
   $sql = 'SELECT 
              a.entry_id,
              a.entry_created_on,
              a.entry_text_more,
              a.entry_title,
              a.entry_excerpt
            FROM 
              mt_entry a,
              mt_category b,
              mt_placement c
            WHERE
              a.entry_status = 2 AND
              b.category_basename = ? AND
              b.category_class = "category" AND
              b.category_id = c.placement_category_id AND
              c.placement_entry_id = a.entry_id   
            ORDER BY a.entry_created_on DESC limit 20' ;
		$result= $this->db->query($sql,array($mt_category));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
//Fetching Files with Time
	function files_get_all_files($time){
		
	/*	$sql = "SELECT 
			a.name as title,
			a.description as file_desc,
			a.id as file_id,
			a.uploaded_on,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM 
			`ci_files` a,
			ci_users c
		WHERE 
			a.files_status = 1 AND
			a.uploaded_on > {$time} AND
			a.user_id = c.id
			ORDER BY a.uploaded_on DESC";
	*/
	 $sql = "SELECT 
      a.name as title,
      a.description as file_desc,
      a.id as file_id,
      a.uploaded_on,
      a.unique_id,
      a.file_size,
      c.first_name,
      c.last_name,
      c.id as user_id
    FROM 
      `ci_files` a,
      ci_users c
    WHERE 
      a.files_status = 1 AND
      a.user_id = c.id
      ORDER BY a.uploaded_on DESC LIMIT 20";
      
		$result=$this->db->query($sql);
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}	
//Fetching Status Updates with Time
	function get_status_update($time,$userId){
/*
		$sql = 'SELECT 
						(SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.user_id ) as following_user_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							c.first_name,
							c.last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM 
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							c.id = b.user_id AND
							b.status = 1 AND
							b.message_time > "'.$time.'" AND
							d.id = designation_id AND
							e.id = concept_id
						ORDER BY b.message_time DESC LIMIT 30';	
*/
      $sql = 'SELECT 
            (SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.object_id ) as following_user_id,
              b.id as message_id,
              b.message,
              b.object_id as user_id,
              b.create_dttm as message_time,
              c.first_name,
              c.last_name,
              c.profile_pic,
              c.email,
              d.name as designation,
              e.name as concept
            FROM 
              ci_status_updates b,
              ci_users c,
              ci_master_designation d,
              ci_master_concept e
            WHERE
              c.id = b.object_id AND b.object_type = "User" AND
              b.status = 1 AND
              d.id = designation_id AND
              e.id = concept_id
            ORDER BY b.create_dttm DESC LIMIT 30';
		$result= $this->db->query($sql,array($userId));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}	
/* Individual Tab code below */	
	
//Fetching Status Updates with last records
	function get_last_status_update($page_size,$offset,$userId){
		$sql = 'SELECT 
						(SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.object_id ) as following_user_id,
							b.id as message_id,
						 	b.message,
							b.object_id as user_id,
							b.create_dttm as message_time,
							c.first_name,
							c.last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM 
							ci_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							c.id = b.object_id AND b.object_type = "User" AND
							b.status = 1 AND
							d.id = designation_id AND
							e.id = concept_id
						ORDER BY b.create_dttm DESC LIMIT ?,?';	
		$result= $this->db->query($sql,array($userId,$page_size,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}	
//Fetching Files with last records
	function get_last_all_files($page_size,$offset){
		
		$sql = "SELECT 
			a.name as title,
			a.description as file_desc,
			a.id as file_id,
			a.uploaded_on,
			a.unique_id,
			a.file_size,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM 
			`ci_files` a,
			ci_users c
		WHERE 
			a.files_status = 1 AND
			a.user_id = c.id
			ORDER BY a.uploaded_on DESC LIMIT ?,?";
		
		$result=$this->db->query($sql,array($page_size,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
	
//Fetching Data From  MT tables for categories(News, announcement, Offers) with last records
	function get_last_data_from_mt($mt_category,$page_size,$offset){
		 $sql = 'SELECT 
							a.entry_id,
							a.entry_created_on,
							a.entry_text_more,
							a.entry_title,
							a.entry_excerpt
						FROM 
							mt_entry a,
							mt_category b,
							mt_placement c
						WHERE
							a.entry_status = 2 AND
							b.category_basename = ? AND
							b.category_class = "category" AND
							b.category_id = c.placement_category_id AND
							c.placement_entry_id = a.entry_id   
						ORDER BY a.entry_created_on DESC LIMIT ?,?' ;
		$result= $this->db->query($sql,array($mt_category,$page_size,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
  //Submit feedback form
  function submit_feedback_form($data){
    $str = $this->db->insert('ci_feedback', $data);
    return $this->db->insert_id();
  }
  function get_announcement_bar_text($user_id){
    $sql =" SELECT * 
            FROM 
              ci_announcement_bar a
            WHERE 
              NOT EXISTS (SELECT id FROM ci_users_announcement_bar_disabled
                    WHERE announcement_bar_id = a.id AND user_id = '{$user_id}') AND
              a.status = 1 AND
              a.expiry_date > '".date('Y-m-d')."'
            ORDER BY a.id DESC LIMIT 1";
    $result= $this->db->query($sql);
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function hide_announcement_bar($data){
    $str = $this->db->insert('ci_users_announcement_bar_disabled', $data);
    return $this->db->insert_id();
  }

  function home_page_all_feed($user_id,$page_size,$offset){
    $sql = "(SELECT
              a.entry_id as id,
              a.entry_created_on as publish_date,
              a.entry_text_more as description,
              a.entry_title as title,
              a.entry_excerpt as album_id,
              b.category_basename as category,
              NULL as file_unique_id,
              NULL as file_size,
              NULL as following_user_id,
              NULL as message_id,
              NULL as message,
              NULL as user_id,
              NULL as first_name,
              NULL as last_name,
              NULL as profile_pic,
              NULL as email,
              NULL as designation,
              NULL as concept
            FROM
              mt_entry a,
              mt_category b,
              mt_placement c
            WHERE
              a.entry_status = 2 AND
              b.category_basename IN('news','announcement','offers') AND
              b.category_class = 'category' AND
              b.category_id = c.placement_category_id AND
              c.placement_entry_id = a.entry_id
            )UNION ALL(
              SELECT
                a.aid as id,
                FROM_UNIXTIME(b.ctime) as publish_date,
                a.description  as description,
                a.title as title,
                NULL as album_id,
                'album' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as concept
              FROM
                alb_albums a,
                alb_pictures b
              WHERE
                visibility = 0 AND
                a.aid = b.aid
              GROUP BY b.aid
            )UNION ALL(
              SELECT
                a.id,
                FROM_UNIXTIME(a.added_on) as publish_date,
                a.description,
                a.title,
                NULL as album_id,
                'events' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as concept
              FROM
                `ci_event` as a
              WHERE
                a.event_status = 1
            )UNION ALL(
              SELECT
                a.id as id,
                FROM_UNIXTIME(a.uploaded_on) as publish_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as concept
              FROM
                `ci_files` a,
                ci_users c
              WHERE
                a.files_status = 1 AND
                a.user_id = c.id
            )UNION ALL(
              SELECT
                NULL as id,
                b.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                (SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.object_id ) as following_user_id,
                b.id as message_id,
                b.message,
                b.object_id as user_id,
                c.first_name,
                c.last_name,
                c.profile_pic,
                c.email,
                d.name as designation,
                e.name as concept
              FROM
                ci_status_updates b,
                ci_users c,
                ci_master_designation d,
                ci_master_concept e
              WHERE
                c.id = b.object_id AND
                b.status = 1 AND b.object_type = 'User' AND
                d.id = designation_id AND
                e.id = concept_id
            )
            order by publish_date DESC LIMIT ?,?";
    $result= $this->db->query($sql,array($user_id,$page_size,$offset));

   // echo $this->db->last_query();

    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
}