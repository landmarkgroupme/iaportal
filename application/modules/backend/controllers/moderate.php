<?php

class Moderate extends MX_Controller {

  function Moderate() {
    parent::__construct();
    $this->load->model('moderate_model');
    $this->load->helper('auth');
    auth();
    $permissions = $this->session->userdata('permissions');
    if(!$permissions['moderate']){
      redirect('home');
    }
  }

  function index() {
    redirect("backend/moderate/show_all");
  }

  function show_all() {
    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $total_files = $this->moderate_model->getAllReportedCountStatus();
    $total_files = $total_files + $this->moderate_model->getAllReportedCountFiles();
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'backend/moderate/show_all/';
    $config['total_rows'] = $total_files;
    $config['uri_segment'] = '4';
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->moderate_model->getAllReported($config['per_page'], $this->uri->segment(4, 0));
    for ($i = 0; $i < count($db_all_files); $i++) {
      $db_all_files[$i]['report_time'] = date("D, d M Y", $db_all_files[$i]['report_time']);
    }
    $data['reported_all'] = $db_all_files;
    $this->load->view('show_moderate', $data);
  }

  function status_updates() {

    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $total_files = $this->moderate_model->getAllReportedCount_status_updates();
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'backend/moderate/status_updates/';
    $config['total_rows'] = $total_files;
    $config['uri_segment'] = '4';
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_reported_updates = $this->moderate_model->getAllReportedStatusUpdates($config['per_page'], $this->uri->segment(4, 0));

    for ($i = 0; $i < count($db_reported_updates); $i++) {
      $db_reported_updates[$i]['report_time'] = date("D, d M Y", $db_reported_updates[$i]['report_time']);
    }
    $data['reported_updates'] = $db_reported_updates;
    $this->load->view('show_moderate_status_updates', $data);
  }

  function market_place() {

    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $total_files = $this->moderate_model->getAllReportedCountMarketPlace();
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'backend/moderate/market-place/';
    $config['total_rows'] = $total_files;
    $config['uri_segment'] = '4';
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "test";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_reported_updates = $this->moderate_model->getAllReportedMarketPlace($config['per_page'], $this->uri->segment(4, 0));

    for ($i = 0; $i < count($db_reported_updates); $i++) {
      $db_reported_updates[$i]['report_time'] = date("D, d M Y", $db_reported_updates[$i]['report_time']);
    }
    $data['reported_updates'] = $db_reported_updates;
    $this->load->view('show_moderate_market_place', $data);
  }

  function files() {

    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $total_files = $this->moderate_model->getAllReportedCount_files();
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'backend/moderate/files/';
    $config['total_rows'] = $total_files;
    $config['uri_segment'] = '4';
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "test";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_reported_updates = $this->moderate_model->getAllReportedFiles($config['per_page'], $this->uri->segment(4, 0));

    for ($i = 0; $i < count($db_reported_updates); $i++) {
      $db_reported_updates[$i]['report_time'] = date("D, d M Y", $db_reported_updates[$i]['report_time']);
    }
    $data['reported_updates'] = $db_reported_updates;
    $this->load->view('show_moderate_files', $data);
  }

  function delete_status_update() {
    $del_item_id = $this->input->post("del_id");
    $status = $this->moderate_model->delete_status_update($del_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }
  function disble_announcement_bar() {
    $del_item_id = $this->input->post("del_id");
    $status = $this->moderate_model->disble_announcement_bar($del_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }
  function restore_status_update() {
    $res_item_id = $this->input->post("res_id");
    $status = $this->moderate_model->restore_status_update($res_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }

  function delete_market() {
    $del_item_id = $this->input->post("del_id");
    $status = $this->moderate_model->delete_market($del_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }

  function restore_market() {
    $res_item_id = $this->input->post("res_id");
    $status = $this->moderate_model->restore_market($res_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }

  function delete_file() {
    $del_item_id = $this->input->post("del_id");
    $status = $this->moderate_model->delete_file($del_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }

  function restore_file() {
    $res_item_id = $this->input->post("res_id");
    $status = $this->moderate_model->restore_file($res_item_id);

    $data['json'] = '{status: "' . $status . '"}';
    $this->load->view("moderate_print_json", $data);
  }

  function show_status_update($id) {
    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $status_update = $this->moderate_model->get_more_status_update($id);
    foreach ($status_update as $row) {
      $data['posterdetails'] = $this->moderate_model->get_user_details($row['posteduser']);
      $data['reporterdetails'] = $this->moderate_model->get_user_details($row['reporteduser']);
    }
    $data['status_update'] = $status_update;
    $this->load->view('show_status_update', $data);
  }

  function show_market_place($id) {
    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $market_place = $this->moderate_model->get_more_market_place($id);
    foreach ($market_place as $row) {
      $data['posterdetails'] = $this->moderate_model->get_user_details($row['posteduser']);
      $data['reporterdetails'] = $this->moderate_model->get_user_details($row['reporteduser']);
      $market_item_images = $this->moderate_model->get_market_place_images($row['market_id']);

      if (count($market_item_images)) {
        foreach ($market_item_images as $item) {
          $imageurl = base_url() . "images/marketplace/" . $item;
          if (@fopen($imageurl, "r")) {
            $data['item_images'][] = $imageurl;
          }
        }
      } else {
        $imageurl = base_url() . "images/marketplace/no-image.jpg";
        $data['item_images'][] = $imageurl;
      }


      switch ($row['item_type']) {
        case 1: $market_place[0]['price'] = $row['currency'] . " " . $row['item_max_price'];
          break; //Sale
        case 2: $market_place[0]['price'] = $row['currency'] . " " . $row['item_min_price'] . " ~ " . $row['item_max_price'];
          break; //
        case 3: $market_place[0]['price'] = "Free";
          break;
      }
    }
    //echo "<xmp>".print_r($market_place,1)."</xmp>; exit";
    $data['status_update'] = $market_place;
    $this->load->view('show_market_place', $data);
  }

  function show_file($id) {

    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $file = $this->moderate_model->get_more_file($id);

    foreach ($file as $row) {
      $db_file_tags = $this->moderate_model->files_get_tags_for_file($row['file_id']);
      $tag_arr = array();
      if (is_array($db_file_tags)) {
        foreach ($db_file_tags as $tags) {
          $tag_arr[] = $tags['name'];
        }
        $file_tag = @implode(", ", $tag_arr);
      }
      $data['file_tag'] = $file_tag;
      $data['posterdetails'] = $this->moderate_model->get_user_details($row['posteduser']);
      $data['reporterdetails'] = $this->moderate_model->get_user_details($row['reporteduser']);

      //Download Link
      if ($row['file_size']) {
        $data['download_path'] = base_url() . 'backend/moderate/files_download/' . $row['unique_id'];
      } else {
        $data['download_path'] = base_url() . 'backend/moderate/files_download/' . $row['unique_id'] . '" target="_blank"';
      }
    }
    $data['file'] = $file;
    $this->load->view('show_file', $data);
  }

  function archives() {

    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $total_files = $this->moderate_model->getAllReportedCountStatus_Archives();
    $total_files = $total_files + $this->moderate_model->getAllReportedCountFiles_Archives();
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'backend/moderate/archives/';
    $config['total_rows'] = $total_files;
    $config['uri_segment'] = '4';
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->moderate_model->getAllReported_Archives($config['per_page'], $this->uri->segment(4, 0));
    for ($i = 0; $i < count($db_all_files); $i++) {
      $db_all_files[$i]['report_time'] = date("D, d M Y", $db_all_files[$i]['report_time']);
    }
    $data['reported_all'] = $db_all_files;
    $this->load->view('show_archives', $data);
  }
  function add_announcement(){
    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);
    $this->load->view('add_announcement', $data);
  }
  function submit_announcement(){
    $message = $this->input->post('txt_body');
    $expiry_date = $this->input->post('txt_date');
    $user_id = $this->session->userdata('userid');
    @list($day, $month, $year) = @explode("/", $expiry_date);
    $data_arr = array(
        'id' => NULL,
        'announcement_bar_text' => $message,
        'posted_by_user_id' => $user_id,
        'created_at' => date('Y-m-d H:i:s'),
        'expiry_date' => $year."-".$month."-".$day,
        'status' => 1
    );
    $insert_id = $this->moderate_model->insert_announcement_bar($data_arr);
    if($insert_id){
      $this->session->set_flashdata('status_ok', "Announcement has been posted");
    }else{
      $this->session->set_flashdata('status_ok', "Error posting Announcement!");
    }
    redirect("backend/moderate/announcement_bar");
  }
  function announcement_bar(){
    $user_id = $this->session->userdata('userid');
    $data['user_name'] = $this->moderate_model->getUserFullName($user_id);

    $total_count = $this->moderate_model->getAllAnnouncementBarCount();
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'backend/moderate/announcement_bar/';
    $config['total_rows'] = $total_count;
    $config['uri_segment'] = '4';
    $config['per_page'] = 10;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_announcement_bar = $this->moderate_model->getAllAnnouncementBar($config['per_page'], $this->uri->segment(4, 0));

    for ($i = 0; $i < count($db_announcement_bar); $i++) {
      $db_announcement_bar[$i]['created_at'] = date("D, d M Y", strtotime($db_announcement_bar[$i]['created_at']));
    }
    $data['announcement_bar'] = $db_announcement_bar;
    $this->load->view('announcement_bar', $data);
  }
  function files_download() {
    $this->load->helper('download');
    $unique_id = $this->uri->segment(4, 0);
    $download_path = $this->moderate_model->files_download_path($unique_id);
    $file_id = $download_path['id'];
    $file_path = $download_path['path'];

    $ext = substr(strrchr($file_path, 'http'), 1);
    if (!(substr(strrchr($file_path, 'http'), 1)) || substr(strrchr($file_path, 'www'), 1)) {
      $file_name = basename($file_path);
      $get_file_content = file_get_contents($file_path);
      force_download($file_name, $get_file_content);
    } else {
      redirect($file_path);
    }
  }

  function notifications() {
    $this->load->model("people/m_people");
    $data['concepts'] = $this->m_people->concepts(8);
    $data['countries'] = $this->m_people->territories(8);
    $bands = $this->moderate_model->get_bands(6);
    //sort($bands);
    //echo "<xmp>".print_r($bands,1)."</xmp>"; exit;
    $data['bands'] = $bands;
    $mt_cat = "offers";
    $mt_cat_details = $this->moderate_model->get_category_title_from_mt($mt_cat);

    $offer_options = "";
    if (count($mt_cat_details)) {
      foreach ($mt_cat_details as $cat) {
        $offer_options.= "<option value='" . $cat['entry_id'] . "'>" . $cat['entry_title'] . "</option>";
      }
    } else {
      $offer_options .= "<option value=''>No Offers</option>";
    }
    $data['offer_options'] = $offer_options;
    $data['welcome_user_count'] = $this->moderate_model->get_welcome_user_count();

    $this->load->view("notification", $data);
  }

  function submit_send_notifications() {
    $notification_title = $this->input->post("cmb_category_title");

    $country = $this->input->post("country");
    $concepts = $this->input->post("concept");
    $bands = $this->input->post("band");
    //echo "<xmp>".print_r($bands,1)."</xmp>";exit;
    if ((!is_array($country)) AND (!is_array($concepts))) {
      $this->session->set_flashdata('chk_status', 'Concepts or Location is required.');
      redirect("backend/moderate/notifications");
    }
    $sql_where_arr = array();
    if (is_array($country)) {
      $country_id = @implode(",", $country);
      $sql_where_arr[] = "territory_id IN ($country_id)";
    }

    if (is_array($concepts)) {
      $concept_id = @implode(",", $concepts);
      $sql_where_arr[] = "concept_id IN ($concept_id)";
    }

    if (is_array($bands)) {
      $band_id = @implode(",", $bands);
      $sql_where_arr[] = "band_id IN ($band_id)";
    }

    if (count($sql_where_arr)) {
      $sql_where = @implode(" AND ", $sql_where_arr);
    }

    $user_list = $this->moderate_model->get_notification_user_list($sql_where);
    /* Testing Unit
      $user_list[0]['email'] = "raghu111@gmail.com";
      $user_list[0]['title'] = "Mr";
      $user_list[0]['first_name'] = "Raghunath";
      $user_list[0]['middle_name']="M";
      $user_list[0]['last_name'] = "";
     */



    //echo "<xmp>".print_r($user_list,1)."</xmp>";exit;
    $mt_entry_details = $this->moderate_model->get_mt_entry_details($notification_title);
    if (count($mt_entry_details)) {

      $mt_id = $mt_entry_details[0]['entry_id'];
      $mt_title = $mt_entry_details[0]['entry_title'];
      $mt_desc = $mt_entry_details[0]['entry_text'];
      $mt_cat = strtoupper($mt_entry_details[0]['category_label']);
      $message = '<table width="520" border="0" style="font-family:Arial;">
        <tr>
          <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
            <p style="color:#8c8d8d; font-family:Arial;margin:30px 0 0 0;padding:0px;font-size:13px;">' . $mt_cat . ' ALERT</p>
            <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">' . $mt_title . '</p>
            <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0; line-height:25px;">' . $mt_desc . '</p>
            <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">For more details visit</p>
            <p style="font-size:16px; margin:0px;"><a style="color:#1e5886;" href="' . site_url(strtolower($mt_cat) . '/' . strtolower($mt_cat) . '_details/' . $mt_id) . '">' . site_url(strtolower($mt_cat) . '/' . strtolower($mt_cat) . '_details/' . $mt_id) . '</a></p>
          </td>
        </tr>
      </table>';
    } else {
      $this->session->set_flashdata('chk_status', 'MT Entries not found');
      redirect("backend/moderate/notifications");
    }
    //echo count($user_list);
    //echo "<xmp>".print_r($user_list,1)."</xmp>";
    //exit;
    echo "Safety On....";
    exit;
    //EMail code starts here
    $subject = "Landmark Intranet, {$mt_cat} Alert";
    $sender_email = "intranet@landmarkgroup.com";
    $sender_name = "Landmark Intranet";

    $this->load->library('email');
    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    /*
      $config['smtp_host'] = 'mail.dygnos-eservices.com';
      $config['smtp_user'] = 'email@dygnos-eservices.com';
      $config['smtp_pass'] = 'emai4l2';
      $config['smtp_port'] = '26';
     */
    
    //echo "<xmp>".print_r($user_list,1)."</xmp>";
    //exit;

    if (count($user_list)) {
      foreach ($user_list as $user_details) {
        $email = $user_details['email'];

        $this->email->initialize($smtp_config);
        $this->email->from($sender_email, $sender_name);
        $this->email->set_newline("\r\n");
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($email);
        if ($this->email->send()) {
          $data = array(
              'id' => NULL,
              'user_email' => $email,
              'email_type' => $mt_cat,
              'email_subject' => $mt_title,
              'date_time' => date("Y-m-d H:i:s"),
          );
          $this->moderate_model->notification_log($data);
        };
      }
      $this->session->set_flashdata('notify_status', count($user_list) . ' Users Notified');
      redirect("backend/moderate/notifications");
    } else {
      $this->session->set_flashdata('chk_status', 'No Users not found');
      redirect("backend/moderate/notifications");
    }
  }

  function send_welcome_kit() {

    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    /*
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
     */
    /*
    $config['smtp_host'] = '83.111.79.200';
    $config['mailtype'] = "html";
    */


    $user_list = $this->moderate_model->get_welcome_users();
    if (!count($user_list)) {
      redirect("backend/moderate");
    }
    foreach ($user_list as $user_details) {
      $user_id = $user_details['id'];
      $name = $user_details['first_name'];
      $email = $user_details['email'];

      //random password
      $length = 8;
      $level = 3;
      list($usec, $sec) = explode(' ', microtime());
      srand((float) $sec + ((float) $usec * 100000));

      $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
      $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      $validchars[3] = "0123456789_!@#$%&*()abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()";

      $password = "";
      $counter = 0;

      while ($counter < $length) {
        $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level]) - 1), 1);

        // All character must be different
        if (!strstr($password, $actChar)) {
          $password .= $actChar;
          $counter++;
        }
      }

      $update_pass = array(
          'password' => md5($password)
      );
      $result = $this->moderate_model->update_user_data($update_pass, $user_id);
      $this->load->library('email');

      $sender_email = "intranet@landmarkgroup.com";
      $sender_name = "Landmark Intranet";
      //$email = "intranet.lmg@gmail.com";
      //$to_email = $email;
      //$to_email = "raghu111@gmail.com";
      //$to_email = "thomson.george@cplmg.com";
      $to_email = $email;


      $subject = "Login to the Landmark Intranet";
      $message = '<table width="520" border="0" style="font-family:Arial;">
       <tr>
        <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
          <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">Welcome to the new Landmark Intranet</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;"><strong>Dear ' . $name . ',</strong> </p>
          <p style="color:#3e3e3e;font-size:14px; margin:5px 0 0 0; line-height:20px;">The all new Landmark Intranet allows you to collaborate with colleagues across various concepts and keep abreast with the latest in the company.</p>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Your account on the Intranet has been created. You can login using the following details.</p>
          <table  style="color:#3e3e3e;font-size:14px;margin-top:10px;margin-left:20px;">
            <tr>
              <td>Visit:</td>
              <td><a style="color:#1e5886;font-size:16px;" href="http://intranet.landmarkgroup.com/">http://intranet.landmarkgroup.com/</a></td>
            </tr>
            <tr>
              <td>Username:</td>
              <td>' . $email . '</td>
            </tr>
            <tr>
              <td>Password:</td>
              <td>' . $password . '</td>
            </tr>
          </table>
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">As soon as you login please change your password and complete your profile.</p>
          
          <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Thanks,<br />
          Landmark Intranet</p>
        </td>
      </tr>
      
      
    </table>';

      $this->email->initialize($smtp_config);
      $this->email->from($sender_email, $sender_name);
      $this->email->set_newline("\r\n");
      $this->email->subject($subject);
      $this->email->message($message);
      $this->email->to($to_email);
      if ($this->email->send()) {
        $update_welcome_status = array(
            'welcome_mail' => 1
        );
        $this->moderate_model->update_user_data($update_welcome_status, $user_id);
        echo "Done: $email<br/>";
      } else {
        echo "<div style='color:#ff0000;'>Fail: $email</div>";
      }
    }
    echo "<h1>All Mail send</h1>";
  }

  function get_mt_category_title() {
    $mt_cat = $this->input->post("mt_cat");
    $mt_cat_details = $this->moderate_model->get_category_title_from_mt($mt_cat);
    $options = "";
    if (count($mt_cat_details)) {
      foreach ($mt_cat_details as $cat) {
        $options .= "<option value='" . $cat['entry_id'] . "'>" . $cat['entry_title'] . "</option>";
      }
    } else {
      $options .= "<option value=''>Select Notification</option>";
    }

    $data['json'] = '{options: "' . $options . '"}';
    $this->load->view("moderate_print_json", $data);
  }

  function recovery() {
    $message = "Team representing Landmark Group at Dubai Super Sixes, announced!";
    $notification_title = 18;

    $get_email_list = $this->moderate_model->recover_send_user_list($message);

    $sql_where_arr = array();

    $country_id = 1;
    $sql_where_arr[] = "territory_id IN ($country_id)";

    /*
      $concept_id = @implode(",",$concepts);
      $sql_where_arr[] = "concept_id IN ($concept_id)";
     */
    foreach ($get_email_list as $email) {
      $sql_where_arr[] = "email NOT IN ('$email')";
    }

    $sql_where = @implode(" AND ", $sql_where_arr);
    $get_pending_email_list = $this->moderate_model->recover_pending_user_list($sql_where);
    //echo count($get_pending_email_list);
    //echo "<xmp>".print_r($get_pending_email_list,1)."</xmp>";


    $mt_entry_details = $this->moderate_model->get_mt_entry_details($notification_title);
    if (count($mt_entry_details)) {

      $mt_id = $mt_entry_details[0]['entry_id'];
      $mt_title = $mt_entry_details[0]['entry_title'];
      $mt_desc = $mt_entry_details[0]['entry_text_more'];
      $mt_cat = strtoupper($mt_entry_details[0]['category_label']);
      $message = '<table width="520" border="0" style="font-family:Arial;">
        <tr>
          <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
            <p style="color:#8c8d8d; font-family:Arial;margin:30px 0 0 0;padding:0px;font-size:13px;">' . $mt_cat . ' ALERT</p>
            <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">' . $mt_title . '</p>
            <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0; line-height:25px;">' . $mt_desc . '</p>
            <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">For more details visit</p>
            <p style="font-size:16px; margin:0px;"><a style="color:#1e5886;" href="' . site_url(strtolower($mt_cat) . '/' . strtolower($mt_cat) . '_details/' . $mt_id) . '">' . site_url(strtolower($mt_cat) . '/' . strtolower($mt_cat) . '_details/' . $mt_id) . '</a></p>
          </td>
        </tr>
      </table>';
    } else {
      $this->session->set_flashdata('chk_status', 'MT Entries not found');
      redirect("backend/moderate/notifications");
    }
    //echo count($user_list);
    //echo "<xmp>".print_r($user_list,1)."</xmp>";
    //exit;
    echo "Safety On";
    exit;
    //EMail code starts here
    $subject = "Landmark Intranet, {$mt_cat} Alert";
    $sender_email = "intranet@landmarkgroup.com";
    $sender_name = "Landmark Intranet";

    $this->load->library('email');

    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();

    /*
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
     */
     /*
    $config['smtp_host'] = '83.111.79.200';
    $config['mailtype'] = "html";
    */
    if (count($get_pending_email_list)) {
      foreach ($get_pending_email_list as $user_details) {
        $email = $user_details['email'];

        $this->email->initialize($smtp_config);
        $this->email->from($sender_email, $sender_name);
        $this->email->set_newline("\r\n");
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($email);
        // echo $email."<br />";

        if ($this->email->send()) {
          $data = array(
              'id' => NULL,
              'user_email' => $email,
              'email_type' => $mt_cat,
              'email_subject' => $mt_title,
              'date_time' => date("Y-m-d H:i:s"),
          );
          $this->moderate_model->notification_log($data);
        };
      }
      $this->session->set_flashdata('notify_status', count($user_list) . ' Users Notified');
      redirect("backend/moderate/notifications");
    } else {
      $this->session->set_flashdata('chk_status', 'No Users not found');
      redirect("backend/moderate/notifications");
    }
  }

}