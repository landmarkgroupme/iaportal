<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation//style.css" />
    <script type="text/javascript" src="<?=base_url();?>js/moderation/jquery.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/moderation/main.js"></script>
  </head>
  
  <body>
    <div class="wrapper">
      <div class="top-header">
        <div class="left-header">
          <a class="logo" href="<?=site_url();?>"></a>
        </div>
        <div class="right-header">
          <div class="logininfo">Logged in as: <strong><?=$user_name?></strong></div>
          <a href="<?=base_url();?>login/logout"><div class="logout"></div></a>
            </div>  
       </div>
      
      <div class="menu">
        <ul class="main">
          <li class="active"><a href="<?=site_url("backend/moderate/show_all");?>">Moderate</a></li>
          <li><a href="<?=site_url("manage/news");?>">Manage</a></li>
          <!--<li><a href="">Manage</li>-->
        </ul>
        
        <ul class="submenu">
          <li><a href="<?=base_url();?>backend/moderate/show_all/">All</a></li>
          <li><a href="<?=base_url();?>backend/moderate/status_updates/">Status Updates</a></li>
          <li><a href="<?=base_url();?>backend/moderate/files/">Files</a></li>
          <li><a href="<?=base_url();?>backend/moderate/market_place/">Market Place</a></li>
          <li><a href="<?=base_url();?>backend/moderate/archives/">Archives</a></li>
          <li class="active" style="float:right;"><a href="<?=site_url('backend/moderate/announcement_bar');?>">Announcement Bar</a></li>
        </ul>
      </div>
      
      <div class="content">
        <div class='top-heading'>
          <div style="float:right;margin-top:12px;"><a href="<?php echo site_url('backend/moderate/add_announcement')?>"><img src="<?=base_url()?>images/add-message.png" alt="add message" /></a></div>
          <h1>Moderate <span>> Announcement Bar</span></h1>
          <!--<div class="highest">
            <ul>
              <li class="offender"><a href="">Highest Offenders</a></li>
              <li class="reporter"><a href="">Highest Reporters</a></li>
            </ul>
          </div>-->
        </div>
        
        <div class="main-table">
          <?php if ($this->session->flashdata('status_ok')): ?>
          <div class="msg-ok">
            <?= $this->session->flashdata('status_ok'); ?>
          </div>
          <?php endif; ?>
          <?php if ($this->session->flashdata('status_error')): ?>
              <div class="msg-ok">
            <?= $this->session->flashdata('status_error'); ?>
            </div>
          <?php endif; ?>
          <table width="100%" border="0" cellspacing="0">
            <tr>
              <th width="100">Created on</th>
              <th width="150">Posted by</th>
              <th width="100">Expiry date</th>
              <th align="left" width="500">Announcement</th>
              <th>Status</th>
              <th align="center">Actions</th>
            </tr>
            <? if(count($announcement_bar) > 0) {
                $i = 0;
                foreach($announcement_bar as $row){
                  if($i%2 == 0){
                  ?>
                  <tr>
                    <td><?=$row['created_at']?></td>
                    <td><?=$row['postedby']?></td>
                    <td><?=$row['expiry_date']?></td>
                    <td><?=$row['announcement_bar_text']?></td>
                    <td id="status_<?=$row['id'];?>"><?=($row['status'] == 1)? '<div class="post_status published">Live</div>':'<div class="post_status disabled">Disabled</div>' ?></td>
                    <td id="status_action_<?=$row['id'];?>"><?=($row['status'] == 1)?'<span class="remove"><a href="#" class="disble-announcement-bar" id="'.$row['id'].'">Disable</a></span>':'' ?></td>
                  </tr>
                  <?  
                  }
                  else
                  {
                  ?>
                  <tr class="alternate">
                    <td><?=$row['created_at']?></td>
                    <td><?=$row['postedby']?></td>
                    <td><?=$row['expiry_date']?></td>
                    <td><?=$row['announcement_bar_text']?></td>
                    <td id="status_<?=$row['id'];?>"><?=($row['status'] == 1)? '<div class="post_status published">Live</div>':'<div class="post_status disabled">Disabled</div>' ?></td>
                    <td id="status_action_<?=$row['id'];?>"><?=($row['status'] == 1)?'<span class="remove"><a href="#" class="disble-announcement-bar" id="'.$row['id'].'">Disable</a></span>':'' ?></td>
                  </tr>
                  <?  
                  }
                  $i++;
                }
            } 
            ?>
            <tr>
              <th colspan="6" align="right">
                <ul class="pagination"><?=$navlinks?></ul>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <!-- Global Footer -->
		<?php $this->load->view('global_footer.php'); ?>
  </body>
</html>
