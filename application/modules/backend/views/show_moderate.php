<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Landmark Intranet</title>
		<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation//style.css" />
		<script type="text/javascript" src="<?=base_url();?>js/moderation/jquery.js"></script>
		<script type="text/javascript" src="<?=base_url();?>js/moderation/main.js"></script>
	</head>
	
	<body>
		<div class="wrapper">
			<div class="top-header">
				<div class="left-header">
					<a class="logo" href="<?=site_url();?>"></a>
				</div>
				<div class="right-header">
					<div class="logininfo">Logged in as: <strong><?=$user_name?></strong></div>
					<a href="<?=base_url();?>login/logout"><div class="logout"></div></a>
        		</div>	
		   </div>
			
			<div class="menu">
				<ul class="main">
					<li class="active"><a href="<?=site_url("backend/moderate/show_all");?>">Moderate</a></li>
					<li><a href="<?=site_url("manage/news");?>">Manage</a></li>
					 <li><a href="<?=base_url();?>manage/admin">Admin</a></li>
					<!--<li><a href="">Manage</li>-->
				</ul>
				
				<ul class="submenu">
					<li class="active"><a href="<?=base_url();?>backend/moderate/show_all/">All</a></li>
					<li><a href="<?=base_url();?>backend/moderate/status_updates/">Status Updates</a></li>
					<li><a href="<?=base_url();?>backend/moderate/files/">Files</a></li>
					<li><a href="<?=base_url();?>backend/moderate/market_place/">Market Place</a></li>
					<li><a href="<?=base_url();?>backend/moderate/archives/">Archives</a></li>
					<li style="float:right;"><a href="<?=site_url('backend/moderate/announcement_bar');?>">Announcement Bar</a></li>
				</ul>
			</div>
			
			<div class="content">
				<div class='top-heading'>
					<h1>Moderate</h1>
					<!--<div class="highest">
						<ul>
							<li class="offender"><a href="">Highest Offenders</a></li>
							<li class="reporter"><a href="">Highest Reporters</a></li>
						</ul>
					</div>-->
				</div>
				
				<div class="main-table">
					<table width="100%" border="0" cellspacing="0">
						<tr>
							<th width="95">Date</th>
							<th width="120">Posted by</th>
							<th width="120">Reported by</th>
							<th width="90">Section</th>
							<th width="225">Title</th>
							<th>Comment</th>
							<th align="center">Actions</th>
						</tr>
						<? if(count($reported_all) > 0) {
								$i = 0;
								foreach($reported_all as $row){
									if($row['type'] == 1)
									{
										$type = 'Status Update';
										$url = base_url().'backend/moderate/show_status_update/';
										$class = 'status-update';
									}	
									
									if($row['type'] == 2)
									{
										$type = 'File';
										$url = base_url().'backend/moderate/show_file/';
										$class = 'file';
									}		
									
									if($i%2 == 0){
									?>
									<tr>
										<td><?=$row['report_time']?></td>
										<td><?=$row['postedby']?></td>
										<td><?=$row['reportedby']?></td>
										<td><?=$type?></td>
										<td><a href="<?=$url.$row['id']?>"><?=substr($row['message'],0,50)."...";?></a></td>
										<td align="center">
											<a href="#" class="showcomment" id="<?=$class.$row['id']?>"></a>
											<div class="hovercomment" id="comment-<?=$class.$row['id']?>">
												<span>&#9654;</span>
												<h4>Comment from <?=$row['reportedby']?></h4>
												<p><i><?=$row['report_time']?></i>,<br><?=$row['comment']?></p>
											</div>
										</td>
										<td>
											<span class="restore"><a href="" class="restore-<?=$class?>" id="<?=$row['id']?>">Restore</a></span>
											<span class="remove"><a href="" class="delete-<?=$class?>" id="<?=$row['id']?>">Remove</a></span>
										</td>
									</tr>
									<?	
									}
									else
									{
									?>
									<tr class="alternate">
										<td><?=$row['report_time']?></td>
										<td><?=$row['postedby']?></td>
										<td><?=$row['reportedby']?></a></td>
										<td><?=$type?></td>
										<td><a href="<?=$url.$row['id']?>"><?=substr($row['message'],0,50)."...";?></td>
										<td align="center">
											<a href="#" class="showcomment" id="<?=$class.$row['id']?>"></a>
											<div class="hovercomment" id="comment-<?=$class.$row['id']?>">
												<span>&#9654;</span>
												<h4>Comment from <?=$row['reportedby']?></h4>
												<p><i><?=$row['report_time']?></i>,<br><?=$row['comment']?></p>
											</div>
										</td>
										<td>
											<span class="restore"><a href="#" class="restore-<?=$class?>" id="<?=$row['id']?>">Restore</a></span>
											<span class="remove"><a href="#" class="delete-<?=$class?>" id="<?=$row['id']?>">Remove</a></span>
										</td>
									</tr>
									<?	
									}
									$i++;
								}
						}	
						?>
						<tr>
							<th colspan="7" align="right">
								<ul class="pagination"><?=$navlinks?></ul>
							</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- Global Footer -->
		<?php $this->load->view('global_footer.php'); ?>
	</body>
</html>
