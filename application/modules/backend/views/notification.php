<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <title>Notifications</title>
  <link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
  <link href="<?=base_url()?>css/moderation/notification.css" type="text/css" rel="stylesheet" media="all" />
  <script type="text/javascript" src="<?=base_url();?>js/moderation/jquery.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/moderation/main.js"></script>
</head>

<body>
	<!-- #Wrapper-->
	<div id="wrapper">
  	 <div id="header">
  	   <a href="#"><img src="<?=base_url()?>images/logo.png" alt="logo" /></a> <div class="logo-text">Notifications</div>
  	 </div>
  	 
  	 <div id="contents">
    	 <div class="form-set">  
    	   <h1>Announcements &amp; News</h1>
    	   <form action="<?=site_url("backend/moderate/submit_send_notifications")?>" method="post" class="advanced-search">
      	   <div  class="label">NOTIFICATIONS FOR</div>
      	   <select name="cmb_category" id="cmb_category">
      	     <option value="">Select Category</option>
      	     <option value="announcement">Annoucements</option>
      	     <option value="news">News</option>
      	   </select> 
           <span class="error-msg" id="error_cmb_category"></span>
           
      	   <div  class="label">SELECT CONTENT ITEM</div>
      	   <select name="cmb_category_title" id="cmb_category_title">
      	     <option value="">Select Notification</option>
      	   </select>
      	    <span class="error-msg" id="error_cmb_category_title"></span>
      	   <div  class="label">NOTIFICATIONS TO</div>
      	   <?php if($this->session->flashdata('chk_status')):?>
            <div class="msg-error">
              <?=$this->session->flashdata('chk_status');?>
            </div>
            <?php endif;?>
      	   <?php if($this->session->flashdata('notify_status')):?>
            <div class="msg-ok">
              <?=$this->session->flashdata('notify_status');?>
            </div>
            <?php endif;?>
      	   <div class="chk-group">
              <fieldset>
                <div class="checkboxes">
                  <div class="columns">
                    <div class="holder1">
                      <h3>Concepts (<a class="concept_check_all" href="#">all</a> | <a class="concept_check_none" href="#">none</a>)</h3>
                      <div class="container">
                        <?php foreach ($concepts as $conceptcol) : ?>
                          <div class="column">
                          <?php foreach ($conceptcol as $concept) : ?>
                            <div class="row">
                              <input type="checkbox" class="checkbox_concepts" id="concept<?php echo $concept['id']; ?>" name="concept[]" value="<?php echo $concept['id']; ?>" />
                              <label for="concept<?php echo $concept['id']; ?>"><?php echo ucfirst(strtolower($concept['name'])); ?></label>
                            </div>
                          <?php endforeach; ?>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    </div>
                    <div class="holder2">
                      <h3>Locations (<a class="location_check_all" href="#">all</a> | <a class="location_check_none" href="#">none</a>)</h3>
                      <div class="container">
                        <?php foreach ($countries as $countrycol) : ?>
                          <div class="column">
                          <?php foreach ($countrycol as $country) : ?>
                            <div class="row">
                              <input class="checkbox_locations" type="checkbox" id="country<?php echo $country['id']; ?>" name="country[]" value="<?php echo $country['id']; ?>" />
                              <label for="country<?php echo $country['id']; ?>"><?php echo ucfirst(strtolower($country['name'])); ?></label>
                            </div>
                          <?php endforeach; ?>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    </div>
                  </div>
                </div>
             </fieldset>
           </div>
           
           
           
           <div  class="label">BAND</div>
           
           
           <div class="chk-group">
              <fieldset>
                <div class="checkboxes">
                  <div class="columns">
                    <div class="holder1">
                      <h3>Bands (<a class="band_check_all" href="#">all</a> | <a class="band_check_none" href="#">none</a>)</h3>
                      <div class="container">
                        <?php foreach ($bands as $bandcol) : ?>
                          <div class="column">
                          <?php foreach ($bandcol as $band) : ?>
                            <div class="row">
                              <input type="checkbox" class="checkbox_bands" id="band<?php echo $band['id']; ?>" name="band[]" value="<?php echo $band['id']; ?>" />
                              <label for="band<?php echo $band['id']; ?>"><?php echo ucfirst(strtolower($band['name'])); ?></label>
                            </div>
                          <?php endforeach; ?>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    </div>
                    
                  </div>
                </div>
             </fieldset>
           </div>
           
           
           
           
           
           <div class="submit-button">
             <div class="button-holder"><input id="send_notifications" type="image" src="<?=base_url()?>images/btn-send.jpg"></div>
           </div>
        </form>
  	   </div>
  	   <div class="form-set">
    	   <h1>Offers</h1>
  	     <form>
      	   <div  class="label">SELECT OFFER</div>
      	   <select>
      	     <?=$offer_options?>
      	   </select>
      	   <div  class="label">NOTIFICATIONS TO</div>
      	   <p class="info">Email notifications will be sent to all people who have subscribed to your concept on the Intranet.</p>
      	   <div class="submit-button">
             <div class="button-holder"><input type="image" src="<?=base_url()?>images/btn-send.jpg"></div>
           </div>
    	   </form>
    	 </div>  
  	   <div class="form-set">
    	   <h1>Welcome Mail</h1>
    	   <form action="<?=site_url("backend/moderate/send_welcome_kit")?>" method="post">
      	   <div  class="label">New users on Intranet</div>
    	     <p class="welcome-info">There are <span><?=$welcome_user_count?></span> new users in the Intranet now <input type="image" class="welcome-img" src="<?=base_url()?>images/btn-send-welcome.jpg" /></p>
    	   </form>
  	   </div>
  	 </div>
  </div>
  <!-- /Wrapper-->
<!-- Global Footer -->
		<?php $this->load->view('global_footer.php'); ?>   
</body>
</html>
