<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Landmark Intranet</title>
		<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation//style.css" />
		<script type="text/javascript" src="<?=base_url();?>js/moderation/jquery.js"></script>
		<script type="text/javascript" src="<?=base_url();?>js/moderation/main.js"></script>
	</head>
	
	<body>
		<div class="wrapper">
			<div class="top-header">
				<div class="left-header">
					<a class="logo" href="<?=site_url();?>"></a>
				</div>
				<div class="right-header">
					<div class="logininfo">Logged in as: <strong><?=$user_name?></strong></div>
					<a href="<?=base_url();?>login/logout"><div class="logout"></div></a>
        		</div>	
		   </div>
			
			<div class="menu">
				<ul class="main">
          <li class="active"><a href="<?=site_url("backend/moderate/show_all");?>">Moderate</a></li>
          <li><a href="<?=site_url("manage/news");?>">Manage</a></li>
          <!--<li><a href="">Manage</li>-->
        </ul>
				
				<ul class="submenu">
					<li><a href="<?=base_url();?>backend/moderate/">All</a></li>
					<li class="active"><a href="<?=base_url();?>backend/moderate/status_updates/">Status Updates</a></li>
					<li><a href="<?=base_url();?>backend/moderate/files/">Files</a></li>
					<li><a href="<?=base_url();?>backend/moderate/market_place/">Market Place</a></li>
					<li><a href="<?=base_url();?>backend/moderate/archives/">Archives</a></li>
					<li style="float:right;"><a href="<?=site_url('backend/moderate/announcement_bar');?>">Announcement Bar</a></li>
				</ul>
			</div>
			
			<div class="content">
				<div class='top-heading'>
					<h1>Moderate <span>> Details</span></h1>
					<!--<div class="highest">
						<ul>
							<li class="offender"><a href="">Highest Offenders</a></li>
							<li class="reporter"><a href="">Highest Reporters</a></li>
						</ul>
					</div>-->
				</div>
				
				<div class="moderate-section">
					<h3>Reported Status Update</h3>
					<table border="0" cellspacing="0">
					<?  if(count($status_update) > 0)
						{
							foreach($status_update as $row)
							{
					?>
							<tr>
							<td valgin="top"><strong>Message:</strong></td>
							<td><?=$row['message']?></td>
						</tr>
						<tr>
							<td width="90" valgin="top"><strong>Posted on:</strong></td>
							<td><?=date("D, d M Y",$row['message_time']);?></td>
						</tr>
						
						<tr>
							<td colspan="2">
								<div class="contact-vcard">
									<? foreach($posterdetails as $poster)
									{
									?>
									<h3>Posted by</h3>
									<div class="inside">
										<div class="contact-vcard-img">
											<img height="84" width="84" alt="user vcard img" src="<?=base_url()?>images/user-images/105x101/<?=$poster['profile_pic']?>">
										</div>
										<div class="contact-vcard-details">
											<div class="contact-vcard-name"><?=$poster['name']?></div>
											<div class="contact-vcard-desg"><?=$poster['designation']?>, <?=$poster['concept']?></div>
											<?php if($poster['extension'] != ""):?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Extension:</div>
												<div class="contact-vcard-res"><?=$poster['extension']?></div>
											</div>
											<?php endif;?>	
											
											<?php if($poster['extension'] != ""):?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Landline: </div>
												<div class="contact-vcard-res"><?=$poster['phone']?></div>
											</div>
											<?php endif;?>
											
											<?php if($poster['mobile'] != ""):?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Mobile: </div>
												<div class="contact-vcard-res"><?=$poster['mobile']?></div>
											</div>
											<?php endif;?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Email: </div>
												<div class="contact-vcard-res"><a href="mailto:<?=$poster['email']?>"><?=$poster['email']?></a></div>
											</div>
										</div>
									</div>
								<?	}	?>
								</div>
								
								<div class="contact-vcard">
									<? foreach($reporterdetails as $reporter)
									{
									?>
									<h3>Reported by</h3>
									<div class="inside">
										<div class="contact-vcard-img">
											<img height="84" width="84" alt="user vcard img" src="<?=base_url()?>images/user-images/105x101/<?=$reporter['profile_pic']?>">
										</div>
										<div class="contact-vcard-details">
											<div class="contact-vcard-name"><?=$reporter['name']?></div>
											<div class="contact-vcard-desg"><?=$reporter['designation']?>, <?=$reporter['concept']?></div>
											<?php if($reporter['extension'] != ""):?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Extension:</div>
												<div class="contact-vcard-res"><?=$reporter['extension']?></div>
											</div>
											<?php endif;?>	
											
											<?php if($reporter['extension'] != ""):?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Landline: </div>
												<div class="contact-vcard-res"><?=$reporter['phone']?></div>
											</div>
											<?php endif;?>
											
											<?php if($reporter['mobile'] != ""):?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Mobile: </div>
												<div class="contact-vcard-res"><?=$reporter['mobile']?></div>
											</div>
											<?php endif;?>
											<div class="contact-vcard-info">
												<div class="contact-vcard-attr">Email: </div>
												<div class="contact-vcard-res"><a href="mailto:<?=$reporter['email']?>"><?=$reporter['email']?></a></div>
											</div>
										</div>
									</div>
									<?	}	?>
								</div>	
							</td>
						</tr>
						
						<tr>
							<td colspan="2">
								<h3>Report Comment</h3>
								<div class="comment">
									<?=$row['comment']?>
								</div>
							</td>
						</tr>
					<?	
							}
						}	
					?>	
					</table>
					<div class="action">
						<?
						if($row['status_id'] == 1){
						?>
						<a href="#" class="restore-status-update" id="<?=$row['id']?>"><img src="<?=base_url();?>images/moderation/restore-btn.gif"/></a>
						<a href="#" class="delete-status-update" id="<?=$row['id']?>"><img src="<?=base_url();?>images/moderation/delete.gif"/></a>
						<? } ?>
						<?
						if($row['status_id'] == 2){
						?>
						<a href="#"><img src="<?=base_url();?>images/moderation/restored-btn.gif"/></a>
						<? } ?>
						<?
						if($row['status_id'] == 3){
						?>
						<a href="#"><img src="<?=base_url();?>images/moderation/deleted.gif"/></a>
						<? } ?>
					</div>
				</div>
			</div>
			</div>
		</div>
		<!-- Global Footer -->
		<?php $this->load->view('global_footer.php'); ?>
	</body>
</html>
