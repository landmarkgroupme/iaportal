<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation//style.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?=base_url();?>js/moderation/jquery.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/moderation/main.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/nicEdit_admin.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#txt_date').calendricalDate();
    })
      bkLib.onDomLoaded(function() {
       //nicEditors({iconsPath : }).allTextAreas()
        new nicEditor({
          iconsPath : '<?=base_url();?>images/nicEditorIcons.gif',
          buttonList : ['link','unlink'],
          uploadURI : '<?=base_url()?>lib/nicUpload.php'
        }).panelInstance('txt_body'); 
      });
    </script>
  </head>
  
  <body>
    <div class="wrapper">
      <div class="top-header">
        <div class="left-header">
          <a class="logo" href="<?=site_url();?>"></a>
        </div>
        <div class="right-header">
          <div class="logininfo">Logged in as: <strong><?=$user_name?></strong></div>
          <a href="<?=base_url();?>login/logout"><div class="logout"></div></a>
            </div>  
       </div>
      
      <div class="menu">
        <ul class="main">
          <li class="active"><a href="<?=site_url("backend/moderate/show_all");?>">Moderate</a></li>
          <li><a href="<?=site_url("manage/news");?>">Manage</a></li>
          <!--<li><a href="">Manage</li>-->
        </ul>
        
        <ul class="submenu">
          <li><a href="<?=base_url();?>backend/moderate/show_all/">All</a></li>
          <li><a href="<?=base_url();?>backend/moderate/status_updates/">Status Updates</a></li>
          <li><a href="<?=base_url();?>backend/moderate/files/">Files</a></li>
          <li><a href="<?=base_url();?>backend/moderate/market_place/">Market Place</a></li>
          <li><a href="<?=base_url();?>backend/moderate/archives/">Archives</a></li>
          <li class="active" style="float:right;"><a href="<?=site_url('backend/moderate/announcement_bar');?>">Announcement Bar</a></li>
        </ul>
      </div>
      
      <div class="content">
        <div class='top-heading'>
          <h1>Moderate <span>> Announcement Bar</span></h1>
          <!--<div class="highest">
            <ul>
              <li class="offender"><a href="">Highest Offenders</a></li>
              <li class="reporter"><a href="">Highest Reporters</a></li>
            </ul>
          </div>-->
        </div>
        
        <div class="main-table">
          <form action="<?php echo site_url('backend/moderate/submit_announcement')?>" method="post" id="frm_create_news">
            <div class="main-table form">
              <div><strong>Message</strong><span id="error-body" class="error-msg"></span></div>
              <textarea style="width:900px;" rows="12" name="txt_body" id="txt_body" class="form-text"></textarea>    
              
                <div style="margin-top:10px;"><strong>Valid till</strong><span id="error_date" class="error-msg"></span></div>
                <div class="form-area">
                  <input type="text" class="date_field" name="txt_date" value="Pick a date" readonly="readonly" id="txt_date" />
                </div>
                  
              <div class="clear"></div>
            </div>     
                  
            <div class="button-holder">
                <input id="submit_announcement" class="ItemButtonSave submit_entry" name="save" type="submit" value="Publish Now" />
                <a href="<?=site_url("backend/moderate/announcement_bar")?>" class="clearLink" style="margin-left: 12px; margin-top:6px; color:#cf0b0b; font-weight:bold; text-decoration:underline">Cancel</a>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Global Footer -->
		<?php $this->load->view('global_footer.php'); ?>
  </body>
</html>
