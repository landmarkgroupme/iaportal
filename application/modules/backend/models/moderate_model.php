<?php

Class Moderate_model extends CI_Model {

  function Moderate_model() {
    parent::__construct();
  }

  function getAllReportedCountMarketPlace() {
    $this->db->from('ci_report_market');
    $this->db->where('status_id = 1');
    return $this->db->count_all_results();
  }

  function getAllReportedCountFiles() {
    $this->db->from('ci_report_files');
    $this->db->where('status_id = 1');
    return $this->db->count_all_results();
  }

  function getAllReportedCountStatus() {
    $this->db->from('ci_report_status_updates');
    $this->db->where('status_id = 1');
    return $this->db->count_all_results();
  }

  function getAllReported($limit, $offset) {
    $sql = "SELECT * FROM ((SELECT
			a.id as id, 
			b.message as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.object_id AND b.object_type = 'User' LIMIT 1) as postedby,
			a.report_time,
			a.comment,
			(SELECT 1 from ci_report_status_updates LIMIT 1) as type
		FROM 
			ci_report_status_updates a,
			ci_status_updates b
		WHERE 
			a.status_id = 1 AND
			a.users_status_update_id = b.id)
		UNION ALL
			(SELECT
			a.id as id,  
			b.name as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.user_id LIMIT 1) as postedby,
			a.report_time,
			a.comment,
			(SELECT 2 from ci_report_files LIMIT 1) as type
		FROM 
			ci_report_files a,
			ci_files b
		WHERE 
			a.status_id = 1 AND
			a.file_id = b.id)) as temp
		ORDER BY report_time desc
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function getAllReportedCount_status_updates() {
    $this->db->from('ci_report_status_updates');
    $this->db->where('status_id = 1');
    return $this->db->count_all_results();
  }
  
  function getAllReportedStatusUpdates($limit, $offset) {
    $sql = "SELECT
			a.id as id, 
			b.message as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.user_id LIMIT 1) as postedby,
			a.report_time,
			a.comment
		FROM 
			ci_report_status_updates a,
			ci_users_status_updates b
		WHERE 
			a.status_id = 1 AND
			a.users_status_update_id = b.id
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function insert_announcement_bar($data_arr){
    $this->db->update('ci_announcement_bar', array('status' => 2));//reset all announcement to disable mode
    
    $this->db->insert('ci_announcement_bar', $data_arr);
    return $this->db->insert_id();
  }
  function getAllAnnouncementBarCount() {
    $this->db->from('ci_announcement_bar');
    return $this->db->count_all_results();
  }
  function getAllAnnouncementBar($limit, $offset) {
    $sql = "SELECT
      a.id, 
      a.announcement_bar_text,
      (SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.posted_by_user_id LIMIT 1) as postedby,
      a.created_at,
      a.expiry_date,
      a.status
    FROM 
      ci_announcement_bar a
    ORDER BY a.id DESC
    limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function getAllReportedCount_files() {
    $this->db->from('ci_report_files');
    $this->db->where('status_id = 1');
    return $this->db->count_all_results();
  }

  function getAllReportedMarketPlace($limit, $offset) {
    $sql = "SELECT
			a.id,  
			b.title as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.user_id LIMIT 1) as postedby,
			a.report_time,
			a.comment
		FROM 
			ci_report_market a,
			ci_market_items b
		WHERE 
			a.status_id = 1 AND
			a.market_id = b.id
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function getAllReportedFiles($limit, $offset) {
    $sql = "SELECT
			a.id,  
			b.name as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.user_id LIMIT 1) as postedby,
			a.report_time,
			a.comment
		FROM 
			ci_report_files a,
			ci_files b
		WHERE 
			a.status_id = 1 AND
			a.file_id = b.id
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function disble_announcement_bar($del_id) {
    $sql = "UPDATE
					ci_announcement_bar SET
					status = 2 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($del_id));
    return $this->db->affected_rows();
  }
  function delete_status_update($del_id) {

    $sql = "UPDATE
					ci_report_status_updates SET
					status_id = 3 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($del_id));
    return $this->db->affected_rows();
  }

  function restore_status_update($res_id) {

    $sql = "UPDATE
					ci_report_status_updates SET
					status_id = 2 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($res_id));


    $getid = "SELECT
						users_status_update_id
				  FROM
				  		ci_report_status_updates
				  WHERE
				  		`id` = ?";
    $status_result = $this->db->query($getid, array($res_id));
    if ($status_result->num_rows() > 0) {
      $row = $status_result->row_array();
      $status_update_id = $row['users_status_update_id'];
    }

    $sql = "UPDATE
					ci_users_status_updates SET
					status = 1 
				WHERE 
					`id` = ? 
				LIMIT 1";
    return $this->db->query($sql, array($status_update_id));
  }

  function delete_market($del_id) {

    $sql = "UPDATE
					ci_report_market SET
					status_id = 3 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($del_id));
    return $this->db->affected_rows();
  }

  function restore_market($res_id) {

    $sql = "UPDATE
					ci_report_market SET
					status_id = 2 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($res_id));


    $getid = "SELECT
						market_id
				  FROM
				  		ci_report_market
				  WHERE
				  		`id` = ?";
    $file_result = $this->db->query($getid, array($res_id));
    if ($file_result->num_rows() > 0) {
      $row = $file_result->row_array();
      $market_id = $row['market_id'];
    }

    $sql = "UPDATE
					ci_market_items SET
					items_status = 1 
				WHERE 
					`id` = ? 
				LIMIT 1";
    return $this->db->query($sql, array($market_id));
  }

  function delete_file($del_id) {

    $sql = "UPDATE
					ci_report_files SET
					status_id = 3 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($del_id));
    return $this->db->affected_rows();
  }

  function restore_file($res_id) {

    $sql = "UPDATE
					ci_report_files SET
					status_id = 2 
				WHERE 
					`id` = ? 
				LIMIT 1";
    $result = $this->db->query($sql, array($res_id));


    $getid = "SELECT
						file_id
				  FROM
				  		ci_report_files
				  WHERE
				  		`id` = ?";
    $file_result = $this->db->query($getid, array($res_id));
    if ($file_result->num_rows() > 0) {
      $row = $file_result->row_array();
      $file_id = $row['file_id'];
    }

    $sql = "UPDATE
					ci_files SET
					files_status = 1 
				WHERE 
					`id` = ? 
				LIMIT 1";
    return $this->db->query($sql, array($file_id));
  }

  function get_more_market_place($id) {
    $sql = "SELECT
			a.id as id, 
			b.title,
			a.user_id as reporteduser,
			(SELECT id FROM ci_users WHERE id = b.user_id LIMIT 1) as posteduser,
			(SELECT code FROM ci_master_currency WHERE id = b.currency_id LIMIT 1) as currency,
			b.added_on,
			b.description,
			b.item_condition,
			b.item_type,
			b.item_min_price,
			b.item_max_price,
			b.id as market_id,
			a.comment,
			a.status_id
		FROM 
			ci_report_market a,
			ci_market_items b
		WHERE 
			a.market_id = b.id AND
			a.id = ?";

    $result = $this->db->query($sql, array($id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Get Item Images
  function get_market_place_images($market_id) {
    $sql = "SELECT
      original_image
    FROM 
      ci_map_items_images
    WHERE 
      market_items_id = ?";

    $result = $this->db->query($sql, array($market_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row['original_image'];
      }
    }
    $result->free_result();
    return $data;
  }

  function get_more_status_update($id) {
    $sql = "SELECT
			a.id as id, 
			b.message as message,
			a.user_id as reporteduser,
			(SELECT id FROM ci_users WHERE id = b.user_id LIMIT 1) as posteduser,
			b.message_time,
			a.comment,
			a.status_id
		FROM 
			ci_report_status_updates a,
			ci_users_status_updates b
		WHERE 
			a.users_status_update_id = b.id AND
			a.id = ?";

    $result = $this->db->query($sql, array($id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function get_more_file($id) {
    $sql = "SELECT
			a.id as id,
			a.file_id,
			b.unique_id, 
			b.name as title,
			b.description,
			a.user_id as reporteduser,
			(SELECT id FROM ci_users WHERE id = b.user_id LIMIT 1) as posteduser,
			b.uploaded_on,
			a.comment,
			a.status_id,
			b.file_size,
			c.name as filetype
		FROM 
			ci_report_files a,
			ci_files b,
			ci_master_file_category c
		WHERE 
			a.file_id = b.id AND
			c.id = b.file_category_id AND
			a.id = ?";

    $result = $this->db->query($sql, array($id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function get_user_details($id) {
    $sql = "SELECT
					a.id as id, 
					CONCAT(first_name,' ',last_name) as name,
					b.name as designation,
					c.name as concept,
					c.id as concept_id,
					a.extension,
					a.phone,
					a.mobile,
					a.email,
					a.profile_pic
				FROM 
					ci_users a,
					ci_master_designation b,
					ci_master_concept c
				WHERE 
					b.id = a.designation_id AND
					c.id = a.concept_id AND
					a.id = ?";

    $result = $this->db->query($sql, array($id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Get tags for file */

  function files_get_tags_for_file($file_id) {
    $sql = "SELECT a.name, a.id FROM ci_master_file_tag a,ci_map_files_tag b WHERE a.id=b.file_tag_id AND b.files_id = ? ";
    $result = $this->db->query($sql, $file_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function getAllReportedCountFiles_Archives() {
    $this->db->from('ci_report_files');
    $this->db->where('status_id = 2 OR status_id = 3');
    return $this->db->count_all_results();
  }

  function getAllReportedCountStatus_Archives() {
    $this->db->from('ci_report_status_updates');
    $this->db->where('status_id = 2 OR status_id = 3');
    return $this->db->count_all_results();
  }

  function getAllReported_Archives($limit, $offset) {
    $sql = "SELECT * FROM ((SELECT
			a.id as id, 
			b.message as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.user_id LIMIT 1) as postedby,
			a.report_time,
			a.comment,
			a.status_id as status,
			(SELECT 1 from ci_report_status_updates LIMIT 1) as type
		FROM 
			ci_report_status_updates a,
			ci_users_status_updates b
		WHERE 
			a.status_id != 1 AND
			a.users_status_update_id = b.id)
		UNION ALL
			(SELECT
			a.id as id,  
			b.name as message,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = a.user_id LIMIT 1) as reportedby,
			(SELECT CONCAT(first_name,' ',last_name) FROM ci_users WHERE id = b.user_id LIMIT 1) as postedby,
			a.report_time,
			a.comment,
			a.status_id as status,
			(SELECT 2 from ci_report_files LIMIT 1) as type
		FROM 
			ci_report_files a,
			ci_files b
		WHERE 
			a.status_id != 1 AND
			a.file_id = b.id)) as temp
		ORDER BY report_time desc
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function getUserFullName($id) {
    $sql = "SELECT CONCAT(first_name,' ',last_name) as fullname FROM ci_users where id = ? ";
    $result = $this->db->query($sql, $id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        return $row['fullname'];
      }
    }
  }

  function files_download_path($unique_id) {
    $this->db->select('id,path');
    $result = $this->db->get_where('ci_files', array('unique_id' => $unique_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Welcome Mail user check
  function get_welcome_user_count() {
    $sql = 'SELECT count(id) as user_count
            FROM 
              ci_users
            WHERE
              welcome_mail = 0';
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row['user_count'];
      }
    }
    $result->free_result();
    return $data;
  }

  //Mt Entry details
  function get_mt_entry_details($mt_entry_id) {

    $sql = 'SELECT
              a.entry_id,
              a.entry_title,
              a.entry_text_more,
              a.entry_text,
              b.category_label
            FROM 
              mt_entry a,
              mt_category b,
              mt_placement c
            WHERE
              a.entry_status = 2 AND
              a.entry_id = ? AND
              b.category_class = "category" AND
              b.category_id = c.placement_category_id AND
              c.placement_entry_id = a.entry_id   
            ORDER BY a.entry_id DESC';
    $result = $this->db->query($sql, array($mt_entry_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//MT Query	
  function get_category_title_from_mt($mt_category) {

    $sql = 'SELECT
              a.entry_id,
              a.entry_title 
            FROM 
              mt_entry a,
              mt_category b,
              mt_placement c
            WHERE
              a.entry_status = 2 AND
              b.category_basename = ? AND
              b.category_class = "category" AND
              b.category_id = c.placement_category_id AND
              c.placement_entry_id = a.entry_id   
            ORDER BY a.entry_id DESC';
    $result = $this->db->query($sql, array($mt_category));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function get_notification_user_list($sql_where) {
    $sql = "SELECT email,title,first_name,middle_name,last_name FROM ci_users WHERE {$sql_where}";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function get_welcome_users() {
    $this->db->select('id,first_name,email');
    $result = $this->db->get_where('ci_users', array('welcome_mail' => 0));

    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  
  
  function get_welcome_users_bulk_import() {
    $this->db->select('id,first_name,email');
    $result = $this->db->get_where('ci_users', array('welcome_mail' => 0,'created_by' => 'bulk_import' ));

    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function update_user_data($data, $user_id) {
    $this->db->where('id', $user_id);
    $this->db->update('ci_users', $data);
    return $this->db->affected_rows();
  }

  /* Create Files Tags Mapping */

  function notification_log($data) {
    $str = $this->db->insert('ci_notify_email_log', $data);
    return $this->db->insert_id();
  }

  function get_bands($rows) {
    $sql = "SELECT id,name FROM ci_master_band ORDER BY length(name), name";

    $qry = $this->db->query($sql);
    if ($qry->num_rows() > 0) {
      $count = 0;
      foreach ($qry->result_array() as $row) {
        ++$count;
        $data[] = $row;
        if (($count % $rows) == 0) {
          $rowdata[] = $data;
          $data = array();
        }
      }
      if ($data) {
        $rowdata[] = $data;
      }
    }
    $qry->free_result();
    return $rowdata;
  }

  //Recovery Email options
  function recover_send_user_list($message) {
    $sql = "SELECT user_email FROM ci_notify_email_log WHERE email_subject = '{$message}'";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row['user_email'];
      }
    }
    $result->free_result();
    return $data;
  }

  //Recovery Email options
  function recover_pending_user_list($sql_where) {
    $sql = "SELECT email,title,first_name,middle_name,last_name FROM ci_users WHERE {$sql_where}";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

}
