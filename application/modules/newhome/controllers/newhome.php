<?php

class NewHome extends General
{

//Constructor function
  function __construct()
  {
    parent::__construct();
    //date_default_timezone_set("Asia/Calcutta"); //default timezone setting
    //date_default_timezone_set("Asia/Baghdad"); //default timezone setting
    //$this->load->model('home_model');
    $this->load->model('Statuses');
    $this->load->model('Photo');
    $this->load->model('Album');
    $this->load->model('User');
    $this->load->model('Updates');
    $this->load->model('Prompt');
    $this->load->model('Announcement');
    $this->load->model('Group');
    $this->load->model('Concept');
    $this->load->model('GroupUser');
    $this->load->helper('time_ago_helper');
    $this->load->model('NewsEntries');
    $this->load->model('Item');
    $this->load->model('Widget');


    $this->load->model('GroupConceptPermission');
    $this->load->model('ObjectMember'); //generic table holding members for both concepts and groups
    $this->load->model('manage/manage_model');
    $this->load->model('includes/general_model'); //General Module
    $this->load->model('backend/moderate_model');
    $this->load->model('SurveyQuestionCategory');
    $this->load->model('SurveyQuestion');
    $this->load->model('Survey');
    $this->load->model('SurveyUser');
    $this->load->model('SurveyAnswer');
    $this->load->model('Outlets');
    $this->load->model('ConceptManager');
    $this->load->model('Territory');
    $this->load->model('ManagementActionPlan');
    $this->config->load('audit');
    
  }

  public function _remap($method)
  {

    if(in_array($method, array('news', 'announcement', 'offers'))) {
      $this->index($method);
    } else {
      $this->$method();
    }
  }


  /* Private function for Favourite Links */

  function index($method = null)
  {

   $data = array();
    $data['audit_constant'] = $this->config->item('audit');
    $concepts = $this->Survey->getSurveyConcept();
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
      }
    }
    $outlets = $this->Outlets->getOutletbyFilter(array());
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
      }
    }

    if ($this->session->userdata('submit_form') == true)  { 
        $this->session->set_userdata('submit_form' , false );
        $this->load->view('redirect_form', $data);
    }
    else {
       $this->load->view('user_view', $data);

    }

    
  }

  /* Private function for My Reminders */

  function _calculateProfileCompleteness($profile, $total_groups)
  {
    $progress = 5;
    if(trim($profile['skills'])) {
      $progress += 25;
    }
    if(trim($profile['about'])) {
      $progress += 25;
    }

    if(!preg_match('/^default/', $profile['profile_pic'])) {
      $progress += 20;
    }

    if(isset($total_groups) && $total_groups > 0) {
      $progress += 25;
    }
    return $progress;
  }

  function _favourite_links_list()
  {
    $show_limit = 20;
    $user_id = $this->session->userdata('userid');
    $db_favourite_links = $this->general_model->gn_favourite_links($user_id, $show_limit); //General Model function for Interest Groups
    if(!count($db_favourite_links)) {
      $db_favourite_links = array();
    }
    return $db_favourite_links;
  }

  //Default page for Home

  function _my_reminders()
  {
    $user_id = $this->session->userdata('userid');
    $time = time();
    $this->load->model('market_place/market_place_model');
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($user_id);
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($user_id);
    $db_event_reminders = $this->general_model->gn_my_event_reminders($user_id, $time);
    $my_reminders['count_item_sale'] = count($db_item_sale);
    $my_reminders['count_item_requested'] = count($db_item_requested);
    $my_reminders['count_event_reminders'] = count($db_event_reminders);
    return $my_reminders;
  }

  function updates()
  {
    $my_user = $this->session->userdata("logged_user");

    $type = $this->input->post('type');
    $limit = $this->input->post('limit');
    $feedtype = $this->input->post('feedtype');
    //$feedtype    = isset($feedtype) && $feedtype != 0 ? $feedtype : '';
    $feedtype_id = $this->input->post('feedtype_id');
    $filter_user = $this->input->post('filter_user');
    $tag = $this->input->post('tag');
    /*print_r($feedtype);
    die();*/
    $current_time = time();
    $limit = $limit == 0 ? 10 : (int)$limit;
    $offset = $this->input->post('start');
    $offset = (int)$offset;

    if($feedtype == 'concept') {
      $feed = $this->Updates->getConceptFeed($feedtype_id, $limit, $offset, $type, $my_user['userid']);
    } elseif($feedtype == 'profile') {
      $feed = $this->Updates->getProfileFeed($feedtype_id, $limit, $offset, $type, $my_user['userid']);
    } elseif($feedtype == 'files') {

      $feed = $this->Updates->getGroupFeed($feedtype_id, $limit, $offset, $type, $my_user['userid']);
    } elseif($feedtype == 'group') {
      if(isset($tag) && !empty($tag)) {
        $feed = $this->Updates->getGroupTagFile($feedtype_id, $limit, $offset, $type, $tag, $my_user['userid']);
      } else {
        $feed = $this->Updates->getGroupFeed($feedtype_id, $limit, $offset, $type, $my_user['userid']);
      }
    } else {
      $time = $this->input->post('current_time');
      if(isset($time) && $time != 0) {
        $feed = $this->Updates->getHomePageFeed($my_user['userid'], $limit, $offset, $type, null, $time);
      } else {

        $feed = $this->Updates->getHomePageFeed($my_user['userid'], $limit, $offset, $type);
      }
    }

   // echo "<pre>".print_r($feed,"\n")."</pre>";
   // exit;

    if($type == "photos") {

      $items = array();
      $item1 = array();
      foreach($feed as $item) {
        $thumbnail = $item['image_path'] . $item['sub_dir'] . $item['filepath'] . $item['thumb'] . $item['filename'];
        $thumbnail = str_replace(" ", "%20", $thumbnail);
        if($thumbnail == "") {
          $thumbnail = base_url() . "images/album.jpg";
        }

        $item1['thumbnail'] = $thumbnail;
        $item1['category'] = "photos";
        $item1['link'] = '';
        $item1['title'] = $item['title'];
        $item1['ctime'] = date("d M, Y", $item['ctime']);
        $item1['total_pic'] = '<span class="pic-count">| ' . $item['total_pic'] . ' Photos</span>';
        $item1['time_ago'] = time_ago(strtotime($item['publish_date']));
        $items[] = $item1;
      }
    } else if($type == "profilePhotos") {

      $items = array();
      $item1 = array();
      foreach($feed as $item) {
        $thumbnail = $item['image_path'] . $item['sub_dir'] . $item['filepath'] . $item['thumb'] . $item['filename'];
        $thumbnail = str_replace(" ", "%20", $thumbnail);
        if($thumbnail == "") {
          $thumbnail = base_url() . "images/album.jpg";
        }

        $image = $item['image_path'] . $item['sub_dir'] . $item['filepath'] . $item['filename'];
        $image = str_replace(" ", "%20", $image);
        if($image == "") {
          $image = base_url() . "images/album.jpg";
        }

        $item1['image'] = $image;
        $item1['thumbnail'] = $thumbnail;
        $item1['category'] = "profilePhotos";
        $item1['link'] = '';
        $item1['title'] = $item['description'];
        $item1['ctime'] = date("d M, Y", $item['ctime']);
        //$item1['total_pic'] = '<span class="pic-count">| ' . $item['total_pic'] . ' Photos</span>';
        $item1['time_ago'] = time_ago(strtotime($item['publish_date']));
        $items[] = $item1;
      }
    } else if($type == "polls") {
      $items = array();
      $item1 = array();
      foreach($feed as $item) {
        $options = array();
        $total_votes = 0;
        foreach($this->poll_lib->get_poll_options($item['id']) as $option) {
          $option_votes = $this->poll_lib->get_options_votes($option['option_id']);
          $options[] = array('option_id' => $option['option_id'], 'title' => $option['title'], 'votes' => $option_votes);
          // add up total number of votes for this poll
          $total_votes += $option_votes;
        }

        // calculate percentages
        foreach($options as $key => $value) {
          if($options[$key]['votes'] == 0) {
            $options[$key]['percentage'] = 0;
          } else {
            $options[$key]['percentage'] = ($options[$key]['votes'] / $total_votes) * 100;
          }
        }

        /*	$user = $this->User->get(intval($item['created_by']));
          if(isset($user[0]))
          {

            $item1['thumbnail'] = $user[0]->profile_pic;
            $item1['link'] = $user[0]->reply_username;
            $item1['display_name'] = $user[0]->display_name;

          } */
        $item['voted'] = $this->poll_lib->has_previously_voted($item['id'], $my_user['userid']);
        $item['options'] = $options;

        $item1['category'] = "polls";
        $item1['title'] = $item['title'];
        $item1['id'] = $item['id'];
        //$item1['total_votes'] = $item['total_votes'];
        $item['time_ago'] = time_ago(strtotime($item['publish_date']));

        //$item1['options'] = $item['options'];
        $items[] = $item;

      }
    } else {
      $items = array();
      foreach($feed as $key => $item) {

        if($item['category'] == 'status updates') {
          $content_type = 'Status';
          $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $my_user['userid']);
          $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
          $item['total_comments'] = $totalComment;
          foreach($item['comments'] as $key2 => $comment) {
            $item['comments'][$key2]['likes'] = $this->Statuses->getLike($comment['id'], $my_user['userid']);
            $item['comments'][$key2]['message'] = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message']);
            $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));
            $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
            $replace = '<a href="' . site_url() . '$1">$2</a>';
            $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
          }
          $item['likes'] = $this->Statuses->getLike($item['id'], $my_user['userid']);
          /*$replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
          $replacedText = item.message.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');*/
          $replacedText = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $item['message']);
          if($replacedText == $item['message']) {
            $item['message'] = $this->MakeUrls($replacedText);
          } else {
            $item['message'] = $replacedText;
          }
          //$item['message'] = "fjhdsjfhsd @#kunal.pati:Kunal :sgfh ";
          // print $item['message'];
          $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
          $replace = '<a href="' . site_url() . '$1">$2</a>';
          $item['message'] = preg_replace($pattern, $replace, $item['message']);
          $item['strip_message'] = strip_tags($item['message']);
          $item['message'] = stripslashes(htmlentities($item['message']));
          //print_r( $val ); exit;

        } else if($item['category'] == 'photo') {
          $content_type = 'Photo';
          $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $my_user['userid']);
          $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
          $item['total_comments'] = $totalComment;
          foreach($item['comments'] as $key2 => $comment) {
            $item['comments'][$key2]['likes'] = $this->Statuses->getLike($comment['id'], $my_user['userid']);
            $item['comments'][$key2]['message'] = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message']);
            $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));
            $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
            $replace = '<a href="' . site_url() . '$1">$2</a>';
            $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
          }
          $item['likes'] = $this->Photo->getLike($item['id'], $my_user['userid']);
          //----------------
          $replacedText = '';
          $replacedText = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $item['title']);
          if($replacedText == $item['title']) {
            $item['title'] = $this->MakeUrls($replacedText);
          } else {
            $item['title'] = $replacedText;
          }
          //-----------------
          $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
          $replace = '<a href="' . site_url() . '$1">$2</a>';
          $item['title'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['title'])));
        } else if($item['category'] == 'album') {
          $content_type = 'Albums';
          $this->load->model('AlbumPicture');
          $item['photos'] = $this->AlbumPicture->getAlbumPictures($item['id'], 3, 0);
          $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $my_user['userid']);
          $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
          $item['total_comments'] = $totalComment;
          foreach($item['comments'] as $key2 => $comment) {
            $item['comments'][$key2]['likes'] = $this->Statuses->getLike($comment['id'], $my_user['userid']);
            $item['comments'][$key2]['message'] = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message']);
            $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));
            $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
            $replace = '<a href="' . site_url() . '$1">$2</a>';
            $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
          }
          $item['likes'] = $this->Album->getLike($item['id'], $my_user['userid']);
        } else if(in_array($item['category'], array('news', 'offers', 'announcement'))) {
          $content_type = ucfirst($item['category']);
          $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $my_user['userid']);
          foreach($item['comments'] as $key2 => $comment) {
            $item['comments'][$key2]['likes'] = $this->Statuses->getLike($comment['id'], $my_user['userid']);
            $item['comments'][$key2]['message'] = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message']);
            $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));

            $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
            $replace = '<a href="' . site_url() . '$1">$2</a>';
            $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
          }
          $item['likes'] = $this->NewsEntries->getLike($item['id'], $my_user['userid'], $content_type);
          $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
          $item['total_comments'] = $totalComment;
          if($item['category'] == 'offers') {
            $current_date = date('Y-m-d');
            $expire_date = $item['offers_end_date'];
            $today = strtotime($current_date); 
            $expiration_date = strtotime($expire_date); 
            if ($expiration_date < $today) { 
             $item['offer_expired'] = "yes"; 
            } 
            else { 
               $item['offer_expired'] = "no"; 
            }

          }
          
          //$news_time = strtotime($item['publish_date']);
          //$item['created_date'] = date('l,d M Y', $news_time);
          //$item['created_time'] = date('H:i a', $news_time);
        } else if($item['category'] == 'marketplace') {

          $content_type = 'Marketplace';
          //  $item['photos'] = $this->AlbumPicture->getAlbumPictures($item['id'], 3, 0);
          $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $my_user['userid']);
          $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
          $item['total_comments'] = $totalComment;
          foreach($item['comments'] as $key2 => $comment) {
            $item['comments'][$key2]['likes'] = $this->Statuses->getLike($comment['id'], $my_user['userid']);
            $item['comments'][$key2]['message'] = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message']);
            $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));

            $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
            $replace = '<a href="' . site_url() . '$1">$2</a>';
            $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
          }

          $item['likes'] = $this->Item->getLike($item['id'], $my_user['userid']);
          $item_details = $this->Item->getItem($item['id']);
          $item['item_type'] = $item_details[0]['item_type'];

        }else if($item['category'] == 'widgets'){

          $content_type = 'Widget';

          $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $my_user['userid']);
          $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
          $item['total_comments'] = $totalComment;

          foreach($item['comments'] as $key2 => $comment) {
            $item['comments'][$key2]['likes'] = $this->Statuses->getLike($comment['id'], $my_user['userid']);
            $item['comments'][$key2]['message'] = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message']);
            $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));

            $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
            $replace = '<a href="' . site_url() . '$1">$2</a>';
            $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
          }

          $item['likes'] = $this->Widget->getWidgetLike($item['id'], $my_user['userid']);
          $item_details = $this->Widget->getWidget($item['id']);

          $item['username'] = 'concepts/'.$item['slug'];
          $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['weight'] = $item_details[0]['weight'];
          $item['category_title'] = $item_details[0]['category_title'];
        }
        if($item['category'] == 'polls') {

          // get the votes for each option
          $options = array();
          $total_votes = 0;

          foreach($this->poll_lib->get_poll_options($item['id']) as $option) {
            $option_votes = $this->poll_lib->get_options_votes($option['option_id']);
            $options[] = array('option_id' => $option['option_id'], 'title' => $option['title'], 'votes' => $option_votes);
            // add up total number of votes for this poll
            $total_votes += $option_votes;
          }

          // calculate percentages
          foreach($options as $key => $value) {
            if($options[$key]['votes'] == 0) {
              $options[$key]['percentage'] = 0;
            } else {
              $options[$key]['percentage'] = ($options[$key]['votes'] / $total_votes) * 100;
            }
          }

          $item['options'] = $options;
          //$item['options'] = $this->poll_lib->get_poll_options($item['id']);
          $item['voted'] = $this->poll_lib->has_previously_voted($item['id'], $my_user['userid']);
        }

        $item['time_ago'] = time_ago(strtotime($item['publish_date']));

        $items[] = $item;
      }
    }
    $data['json'] = json_encode(array('feed' => $items, 'current_time' => $current_time));

    $this->load->view('print_json', $data);
  }

  function MakeUrls($str)
  {
    $find = array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si', '`((?<!//)(www\.\S+[[:alnum:]]/?))`si');
    $replace = array('a href="$1" target="_blank">$1</a>', '<a href="http://$1"    target="_blank">$1</a>');
    return preg_replace($find, $replace, $str);
  }
  /* Status Update tab */

  // SSO bit

  function ajax_boarding_groups()
  {
    $my_user = $this->session->userdata("logged_user");
    $data = $this->Group->getGroupsForJoin($my_user['userid']);
    if(isset($data[1])) {
      $groups = $data[1];
      $groups['status'] = true;
    } else {
      $groups = array();
      $groups['status'] = false;
    }
    echo json_encode($groups);
  }

  function ajax_boarding_concepts()
  {
    $my_user = $this->session->userdata("logged_user");
    $data = $this->Concept->getConceptsForJoin($my_user['userid']);
    if(isset($data[1])) {
      $concepts = $data[1];
      $concepts['status'] = true;
    } else {
      $concepts = array();
      $concepts['status'] = false;
    }
    echo json_encode($concepts);
  }


//Code of conduct page

  function sso()
  {
    //$sso_dest = $this->uri->segment(3);
    $this->load->view("sso/process");
  }

//Release notes page

  function code_of_conduct()
  {
    /* $this->load->model("profile/m_profile");
     $user_email = $this->session->userdata("useremail");
     $data['gn_favourite_links_list'] = $this->_favourite_links_list();
     $data['gn_my_reminders'] = $this->_my_reminders();
     $data['myprofile'] = $this->m_profile->get_profile_by_email($user_email); */
    $this->load->view("code_of_conduct_template");
  }

  function release_notes()
  {
    $this->load->view("release_notes_template");
  }

  function help()
  {
    $this->load->view("help");
  }

//Feedback page
  /*function feedback() {
    $this->load->view("feedback_template");
  }*/
//Feed back page submit action

  function faq()
  {
    $this->load->view("faq");
  }

  function submit_feedback()
  {
    $this->load->model('Feedback');
    $my_user = $this->session->userdata("logged_user");
    $this->load->library('email');
    $querytype = $this->input->post("query");
    $name = $my_user['fullname'];
    $desc = $this->input->post("describe");
    $reachtype = 1;
    $contact = $my_user['useremail'];

    $feedback_details = array(
      'id' => NULL,
      'userfeedback_name' => $name,
      'userfeedback_categoryid' => $querytype,
      'userfeedback_description' => $desc,
      'userfeedback_contacttypeid' => $reachtype,
      'userfeedback_contact' => $contact
    );
    $feedback_id = $this->Feedback->add($feedback_details);
    if($feedback_id) {
      if($reachtype == 1) {
        $reach = 'Email';
      }
      if($reachtype == 2) {
        $reach = 'Phone';
      }
      if($reachtype == 3) {
        $reach = 'Mail';
      }

      if($querytype == 1) {
        $query = 'Question on Landmark Intranet';
      }
      if($querytype == 2) {
        $query = 'Comment on Landmark Intranet';
      }
      if($querytype == 3) {
        $query = 'Complaint against a Landmark Intranet';
      }
      if($querytype == 4) {
        $query = 'Compliment on a Landmark Intranet';
      }
      if($querytype == 5) {
        $query = 'Comment on Landmark Intranet';
      }

      $msg = "<p>Hi,<br>A feedback has been posted at <a href='http://www.intranet.landmarkgroup.com'>www.intranet.landmarkgroup.com</a> which you might be interested in.</p><table cellpadding='0' border='0'><tr><td width='30%' style='border-bottom:1px solid #cccccc;padding:3px 0px;'>Feedback from</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $name . "</td></tr><tr><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>You can contact the sender at</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $contact . "</td></tr><tr><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>Feedback category</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $query . "</td></tr><tr><td style='border-bottom:1px solid #cccccc;padding:3px 0px;' valign='top'>Feedback</td><td style='border-bottom:1px solid #cccccc;padding:3px 0px;'>" . $desc . "</td></tr></table><br><p>Admin,<br><a href='http://www.intranet.landmarkgroup.com'>www.intranet.landmarkgroup.com</a></p><p>NOTE: This is a system generated email, please do not try to reply to the email.</p>";
      $subject = "User Feedback from Landmark Intranet";
      $sender_email = "no-reply@landmarkgroup.com";
      $sender_name = "Intranet Feedback";
      $this->load->helper('smtp_config');
      /*
      $config = array();
      $config['protocol'] = 'smtp';
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
      $config['mailtype'] = "html";
      */


      // Need to worked on Email sending  and uncomment below section

      /*
      $this->config->load('intranet');
      $emails = $this->config->item('emails');
        $config = smtp_config();
        $this->email->initialize($config);
        $this->email->from($sender_email, $sender_name);
        //$this->email->to("savitar.jagtiani@cplmg.com,shubham.puranik@cplmg.com,swati.kothari@cplmg.com");
      $this->email->to($emails['feedback'][0]);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->send();
      */
      $fails['status'] = 'yes';
      $fails['success'] = 'yes';
    } else {
      $fails['status'] = 'no';
      $fails['success'] = 'yes';
    }
    $temp['json'] = json_encode($fails);
    echo $temp['json'];
  }

  function get_current_time()
  {
    $today = time(); // 2001-03-10 17:16:18 (the MySQL DATETIME format)
    echo $today;
  }


}

/* End of file home.php */
/* Location: ./system/application/controllers/home.php */