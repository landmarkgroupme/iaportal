<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Internal Audit Portal - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>
<style type="text/css">
.survey .concept-box {
    width: 300px;
    height: 100px;
    margin-right: 25px;
}
</style>

</head>
<body class="full-width">
<form method="post" action="http://devnet.landmarkgroup.com/" id="fom" style="display:none">
          <input type="hidden" name="client_id" value="test_client">
          <input type="hidden" name="client_secret" value="test_client_secret_id">
          <input type="hidden" name="grant_type" value="password">
          <input type="submit" name="oauth_submit" >
  </form>

	<?php //$this->load->view('global_header.php'); ?>


<?php //$this->load->view('global_footer.php'); ?>

<?php $this->load->view('partials/js_footer'); ?>


</body>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
    <script type="text/javascript">       
        $(document).ready(function() {
          $("#fom").submit();
        });       
      </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".inline").colorbox({inline:true, width:"50%"});
        });
    </script>

</html>