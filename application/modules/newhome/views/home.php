<!DOCTYPE html>
<html>
<head>
<meta name="robots" content="index, nofollow">
<title>Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('include_files/common_includes_new'); ?>
</head>
<body class="<?php echo isset($default_content) ? 'profile' : 'profile' ; ?>">



<?php $this->load->view('global_header.php'); ?>
<?php
if(isset($default_content)) {
?>
<div class="section wrapper clearfix">
	<h2><?php echo $default_content == 'announcement' ? 'Announcements' : ucfirst($default_content); ?></h2>
	<ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li><?php echo $default_content == 'announcement' ? 'Announcements' : ucfirst($default_content); ?></li>
    </ul>
</div>
<?php }
	else 
	{ 
		if ($this->session->userdata('just_login') && 0) //disabling the popup for time being. will enable once anything comes up
		{
			$this->session->unset_userdata('just_login');
			?>
			<div id="alert" style="display:none;"><a href="<?php echo site_url(); ?>concepts/home-centre"><img src="media/images/home-centre-banner.jpg" alt="" style="display:block;"/></a></div>
			<?php
		}
	} 
?>


<div class="section wrapper clearfix">
	<?php 
	if(!isset($default_content))
		{
	$this->load->view('popups/boarding'); 
}
	?>
	<div class="left-contents">


		<?php
		$content_updates = array('type' => '');
		if(!isset($default_content))
		{
			//do not load the widget on news/offers/announcement pages
			$this->load->view('partials/post_status');
		} else
		{
			$content_updates['type'] = $default_content;
		}
		
    ?>

        <?php $this->load->view('partials/content_updates', $content_updates); //type is use to load a tab by default ?>

    </div>

	<div class="right-contents media-widget">

		<?php //$this->load->view('widgets/compitition_winner_widget'); ?>
		<?php $this->load->view('widgets/birthday_widget'); ?>
		<?php //$this->load->view('links_widget'); ?>
		<?php //$this->load->view('poll_widget'); ?>
		<?php 
		if(isset($default_content)){      
			if($default_content == 'news'){ 
				$this->load->view('latest_offers_widget');
				$this->load->view('announcement_widget');
			} 
			else if($default_content == 'announcement'){ 
				$this->load->view('latest_news_widget'); 
				$this->load->view('latest_offers_widget'); 
			}
			else if($default_content == 'offers') {
				$this->load->view('latest_news_widget');
				$this->load->view('announcement_widget');
			}
		}
		else{
			$this->load->view('links_widget');
			$this->load->view('poll_widget');  
		}
		?>

    </div> <!-- right-contents -->
</div> <!-- section -->


<?php $this->load->view('global_footer.php'); ?>

<div id="bd-view-all-box" class="bd-all-box fb-container-box d-n">

	<div class="pop-bd clearfix">
		<img src="media/images/iconXYZ_sm.png" width="25" height="25" alt="Jesse Lander" class="round-c"/> <span class="normal-text">Nicholas Hughe</span>
		<div class="rp-social-comment-box"><input id="bd-post1" name="bd-post1" type="text"  class="rp-comment" placeholder="Wish him a Birthday" /></div>
	</div>
	<hr class="dotted"/>
<?for($i=0;$i<6;$i++){?>
	<div class="pop-bd clearfix">
		<img src="media/images/iconXYZ_sm.png" width="25" height="25" alt="Jesse Lander" class="round-c"/> <span class="normal-text">Jesse Lander</span>
		<div class="rp-social-comment-box"><input id="bd-post2" name="bd-post2" type="text" class="rp-comment" placeholder="Wish him a Birthday" /></div>
	</div>
	<hr class="dotted"/>
<?}?>
	<div class="pop-bd clearfix">
		<img src="media/images/iconXYZ_sm.png" width="25" height="25" alt="Jesse Lander" class="round-c"/> <span class="normal-text">Jesse Lander</span>
		<div class="rp-social-comment-box"><input id="bd-post2" name="bd-post2" type="text" class="rp-comment" placeholder="Wish him a Birthday" /></div>
	</div>

</div>

<?php $this->load->view('partials/js_footer'); ?>

<?php $this->load->view('templates/js/content_updates'); ?>

<?php $this->load->view('templates/js/new_status'); ?>
<?php $this->load->view('templates/js/new_status_poll'); ?>

<?php $this->load->view('templates/js/new_upload_image'); ?>

<?php $this->load->view('partials/interaction_js'); ?>
<script src="media/js/jquery.carouFredSel-6.2.0.js" type="text/javascript"></script>
<!-- <script src='media/js/jquery-textntags.js' type='text/javascript'></script> -->
<!-- <script src="js/custom_js/general_js.js" type="text/javascript"></script> -->
<script type="text/javascript">
var limit = 10;
var start = 0;
var nearToBottom = 250;
var idleTime = 0;
$(document).ready(function(){

  $('.pass_change').fancybox({
    maxHeight: 500,
    afterClose  : function() {
      $("#frmChangePassword_w input[type=password]").val("");
    }
  });

var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
 $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });



jQuery.fn.exists = function(){return this.length>0;}
  if ($('#onboarding-main').exists()) {
	$.fancybox({
			href		: '#onboarding-main',
			minWidth	: 640,
			maxWidth	: 640,
			maxHeight	: 640,
			openEffect	: 'fade',
			closeEffect	: 'fade',
      afterClose : function () {
        $(".pass_change").fancybox().trigger('click');
      }


		});
			var data1 = {'user_id' : myprofile.id, 'feature' : 'Boarding'};
			var url = siteurl+"status_update/add_prompt";
			$.ajax({
				url: url,
				data: data1,
				async: false,
				dataType: "json",
				type: "POST",
				success: function(data){

				}
			});

		<?php 
		if(!isset($default_content) && $boarding == 1)
		{
		?>

		progress(<?php print $progress;?>, $('#progressBar'));
		<?php } ?>
  }
  if ($('.people').exists()) {
		
	$('.people').carouFredSel({
			circular: true,
			infinite: true,
			auto: true,
			prev: '.left',
			next: '.right',
			mousewheel: true,
			scroll: 1,
			items : 4,
			swipe: {
				onMouse: true,
				onTouch: true
			},
			width:580,
			height:225
		});
  }
  
  $('.btn-board-concept-follow').click(function(){
		var object_id = $(this).attr('object_id');
		var object_type = $(this).attr('object_type');


	  	var button = $(this);

    	var service_url = siteurl+"objects/subscription";
	     $.ajax({
	      url: service_url,
	      data: {'object_id':object_id, 'object_type': object_type, 'action': $(this).val()},
	      async: false,
	      dataType: "json",
	      type: "POST",
	      success: function(msg1){
	        if(msg1.status == 'success'){
	         	
				
				var service_url1 = siteurl+"newhome/ajax_boarding_concepts";
						 $.ajax({
						  url: service_url1,
						  data: {'object_id':object_id, 'object_type': object_type},
						  async: false,
						  dataType: "json",
						  type: "POST",
						  success: function(msg){
							// console.log(msg);
							// console.log(msg.id);
							if(msg.status == true)
							{
							$( button ).closest( 'li' ).css('visibility', 'hidden');
							$( button ).attr('object_id',msg.id);
							$( button ).closest( 'li' ).find('.follow-block span').html(msg.name);
							$( button ).closest( 'li' ).find('.follow-block img').attr("src", "images/concepts/thumbs/"+msg.thumbnail);
							$( button ).closest( 'li' ).find('.follow-block img').attr("alt", msg.name);
							$( button ).closest( 'li' ).find('span').html(msg.total_members+" members");
							//console.log($( button ).closest( 'li' ).find('.follow-block span').html(msg.name));
							$( button ).closest( 'li' ).css('visibility', 'visible');
							}
							else
							{
								$( button ).css('visibility', 'hidden');
							}
						  }
						});

	        }
	      }
	    });

	});
	
	  $('.btn-board-group-follow').click(function(){
		var object_id = $(this).attr('object_id');
		var object_type = $(this).attr('object_type');


	  	var button = $(this);

    	var service_url = siteurl+"objects/subscription";
	     $.ajax({
	      url: service_url,
	      data: {'object_id':object_id, 'object_type': object_type, 'action': $(this).val()},
	      async: false,
	      dataType: "json",
	      type: "POST",
	      success: function(msg1){
	        if(msg1.status == 'success'){
	         	
				
				var service_url1 = siteurl+"newhome/ajax_boarding_groups";
						 $.ajax({
						  url: service_url1,
						  data: {'object_id':object_id, 'object_type': object_type},
						  async: false,
						  dataType: "json",
						  type: "POST",
						  success: function(msg){
							//console.log(msg);
							//console.log(msg.id);
							if(msg.status == true)
							{
								$( button ).closest( 'li' ).css('visibility', 'hidden');
								$( button ).attr('object_id',msg.id);
								$( button ).closest( 'li' ).find('.follow-block span').html(msg.name);
								$( button ).closest( 'li' ).find('.follow-block img').attr("src", "images/groups/thumbs/"+msg.thumbnail);
								$( button ).closest( 'li' ).find('.follow-block img').attr("alt", msg.name);
								$( button ).closest( 'li' ).find('span').html(msg.total_members+" members");
								//console.log($( button ).closest( 'li' ).find('.follow-block span').html(msg.name));
								$( button ).closest( 'li' ).css('visibility', 'visible');
							}
							else
							{
								$( button ).css('visibility', 'hidden');
							}
						  }
						});

	        }
	      }
	    });

	});
	
  $('#fileUploadPicture').change(function(){
	
			//var file_size = this.files[0].size/1000;
				
				var ext = $(this).val().split('.').pop().toLowerCase();
				var allow = new Array('gif','png','jpg','jpeg');
				if($.inArray(ext, allow) == -1) {
				 	alert("Invalid File");
					return false;
				}
        var options = { 
        url     : $('#upload_picture').attr('action'),
        type    : $("#upload_picture").attr('method'),
        dataType: 'json',
        beforeSubmit: function() { 
          $.fancybox.showLoading();
        },
        success: function(data) {
		$.fancybox.hideLoading();
          if(data.status == 'true'){
		  //window.location.reload();
		  $(".avatar-big").removeAttr("src").attr("src", data.new_image+'?'+(Math.random()*10000000000000000));
		  $(".accounts a").children('img').removeAttr("src").attr("src", data.new_image+'?'+(Math.random()*10000000000000000));
		  var res = data.new_image.split("/");
		  myprofile.profile_pic =  res.pop()+'?'+(Math.random()*10000000000000000);

          }else{
            alert('Something went wrong');
          }
        }  
    };
	 $('#upload_picture').ajaxSubmit(options);
	});
	
    $(".fancybox").fancybox({
		maxHeight	: 500
		
	}); 
	$('.loader-more').show();
	
	var default_type = $('#recent-posts ul.filter li.active a').attr('type');
	loadUpdates(default_type, $('#recent-posts ul.filter li.active'));
	
    setInterval(function(){
			if(idleTime < 2) {
			if(loadProgress == false){
				//$('#recent-posts').html('');
				//$("#recent-posts").children().filter(":not(.filter, .loader-more)").remove();
				var selected = $('#recent-posts ul.filter li[class=active] a');
				loadAutoUpdates($(selected).attr('type'), $('#recent-posts ul.filter li[class=active]'));
			}
			}
	}, 59000);

	$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			if(loadProgress == false){
			var selected = $('#recent-posts ul.filter li[class=active] a');
			start += 10;
		 	loadUpdates($(selected).attr('type'), $('#recent-posts ul.filter li[class=active]'));
			}
		}
	});
	


	$('#bd-view-all').fancybox({
		maxHeight	: 500
	})
	$('#poll-view-all').fancybox({
		maxHeight	: 500,
		minWidth	: 400
	})
	
	if(typeof poll_results !== 'undefined')
	{
		$.each(poll_results, function(index, item){
			_html	= '<li><label>'+item['title']+'</label><div id="progressBarWid'+index+'" class="progressBar"><div style="width:'+item['percentage']+'%">'+Math.round(Number(item['percentage']))+'%&nbsp;</div></div></li>';
			$('.widpoll').append(_html);
			//progress(item['percentage'], $('#progressBarWid'+index));
		});
	}



});

function loadAutoUpdates(type, parent)
{
	$('#recent-posts div.loader-more').show();
	var service_url = siteurl+ pageurl + "/updates";
	var request_data = {};
	
	//filter_user is used to filter status on user profile page.
	if(type !== '')
		request_data = {'type': type, 'limit': limit, 'start' : 0, 'filter_user' : filter_user,'tag':tag_id,'current_time':current_time};
	else
		request_data = {'limit': limit, 'start' : 0, 'filter_user' : filter_user,'tag':tag_id,'current_time':current_time};

	//filter by concept
	if (typeof feedtype != 'undefined')
	{
		request_data.feedtype = feedtype;
		request_data.feedtype_id = feedtype_id;
	}


	/*console.log(request_data);
	return;*/

	$.ajax({
		url: service_url,
		data: request_data,
		async: false,
		dataType: "json",
		type: "POST",
		cache: false,
		success: function(response){
		current_time = response.current_time;
		if(response.feed.length == 0 &&  start == 0)
		{        
		}
			//console.log(response);
			_.templateSettings.variable = "rc";

			// Grab the HTML out of our template tag and pre-compile it.
			var template = _.template(
				$( "script.template" ).html()
			);
			$('#recent-posts ul.filter li').removeClass('active');
			$(parent).addClass('active');
			//$('#recent-posts ul.filter').append(template(response));
			//$('#recent-posts .filter').after(template(response));
			var dl_class =$(".feature:last");
				//console.log(dl_class);
				if(dl_class.length>0)
				{
				 $(dl_class).after(template(response));		
				}else
					{
				  $('#recent-posts div.loader-more').after(template(response));			
				
				} 

			},
		beforeSend: function(){
			loadProgress = true;
			},
		complete: function(response){
			loadProgress = false;
			$('#recent-posts div.loader-more').hide();
			}
		   

	});
	$('div.readmore').expander({
                slicePoint: 200,
                expandText: 'See More',
                userCollapseText: ''
    });

}
function timerIncrement() {
    idleTime = idleTime + 1;
}

function progress(percent, $element) {
console.log(percent);
console.log($element);
console.log('kunal');
	var progressBarWidth = percent * $element.width() / 100;
	$element.find('div').animate({ width: progressBarWidth }, 500).html(Math.round(Number(percent)) + "%&nbsp;");
}

$(function() {
    $("img:below-the-fold").lazyload({
        event : "sporty"
    });
  $(document).ready(function () {
    $('#recent-posts').on('click', '.marketDeleteBt', function (e) {
      e.preventDefault();
      item_id = $(this).attr('data-id');
      var data = {"item_id":item_id};
      var service_url = siteurl+"market/inactiveMarketItem";
      $.ajax({
        url: service_url,
        data: data,
        async: false,
        cache: false,
        dataType: "json",
        type: "GET",
        success: function(data){
          $('#'+item_id).slideUp('fast');
        }
      });
    });
  });
});
$(window).bind("load", function() {
    var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
});


</script>

<script>
	$(window).load(function(){
		var tourSubmitFunc = function(e,v,m,f){
			if(v === -1){
				$.prompt.prevState();
				return false;
			}
			else if(v === 1){
				$.prompt.nextState();
				return false;
			}
			else if(v === 0){
				$.prompt.goToState(0);
				return false;
			}
},
tourStates = [
	{
		title: 'Welcome to the intranet',
		html: 'Your One-stop shop for everything in Landmark. Start by saying Hi to all your colleagues!',
		buttons: { Next: 1 },
		focus: 0,
		position: { container: '.hp-data',x: 0, y: 40, width: 300, arrow: 'tl' },
		submit: tourSubmitFunc
	},
	{
		title: 'Your News Feed',
		html: 'This is where all the actions happens!',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '.rp-avtar', x: 100, y: 70, width: 300, arrow: 'tc' },
		submit: tourSubmitFunc
	},
	{
		title: 'Filter',
		html: 'Filter your updates here',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '.filter', x: 100, y: 40, width: 300, arrow: 'tc' },
		submit: tourSubmitFunc
	},
	{
		title: "Notification bell",
		html: 'Tells you when you have something to read',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '.right-contents', x: -90, y: -10, width: 300, arrow: 'tc' },
		submit: tourSubmitFunc
	},
	{
		title: 'Like/Comment',
		html: 'You can like or a Comment on a status',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '.rp-social', x: 0, y: 30, width: 300, arrow: 'tl' },
		submit: tourSubmitFunc
	},
	{
		title: 'Birthday',
		html: 'Wish your colleagues a very Happy Birthday',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '.right-contents', x: -330, y: -10, width: 300, arrow: 'rt' },
		submit: tourSubmitFunc
	},
	{
		title: 'Polls',
		html: 'Participate in polls created by concepts',
		buttons: { Prev: -1, First: 0, Done: 2 },
		focus: 0,
		position: { container: '.poll-of-month',  x: -330, y: -10, width: 300, arrow: 'rt'},
		submit: tourSubmitFunc
	}
];
<?php 
		if(!isset($default_content))
		{
		?>
if(<?php echo $user_prompt;?>)
{
    $.prompt(tourStates);
	var data1 = {'user_id' : myprofile.id, 'feature' : 'Home'};
	var url = siteurl+"status_update/add_prompt";
	$.ajax({
		url: url,
		data: data1,
		async: false,
		dataType: "json",
		type: "POST",
		success: function(data){

		}
	});
}
if(<?php echo $corporate_prompt;?>)
{
	var data1 = {'user_id' : myprofile.id, 'feature' : 'Corporate'};
	var url = siteurl+"status_update/add_prompt";
	$.ajax({
		url: url,
		data: data1,
		async: false,
		dataType: "json",
		type: "POST",
		success: function(data){

		}
	});
}

<?php } ?>



});
</script>



</body>
</html>