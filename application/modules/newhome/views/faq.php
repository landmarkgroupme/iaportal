<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
	<meta name="robots" content="index, nofollow">
	<title>FAQs - Landmark Group</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<?php $this->load->view('include_files/common_includes_new'); ?>
</head>
<body class="full-width">
<?php $this->load->view('global_header.php'); ?>
	<div class="section wrapper clearfix">
		<h2>
			Frequently Asked Questions
		</h2>
		<ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>FAQs</li>
		</ul>
	</div>
	<div class="section wrapper clearfix">
		<div class="left-contents">
			<div class="container">
				<?
				//include_once "ssi/comp_jobs_filter.php";
				?>
				<div class="concept-grid clearfix" id="media-grid">
				    <ul class="help-info">
					    <li class="first-child">
						  <h3 id="follow-coworker">How do I follow a coworker?</h3>
						  <p>Simple. Go to the People page, or the your fellow co-worker's profile page and click on the Follow button. </p>
						</li>
						<li id="follow">
						  <h3>Well, what do you mean by 'follow'?</h3>
						  <p>When you follow someone, you get direct access to any status updates, interactions they do on their profile. All updates directly come to your feed ensuring you don't miss any of the exciting things they've been doing!</p>
						</li>
						<li id="unfollow-someone">
						  <h3>Can I unfollow someone?</h3>
						  <p>Yes, you can. Although your friend might be sad to lose you. Just head to the People page, or the person's profile page and click away at the Following button.</p>
						</li>
						<li id="find-people">
						  <h3>How do I find People within Landmark Group?</h3>
						  <p>In our People section (http://intranet.landmarkgroup.com/people). You use the filters, which gives you the ability to filter people, on the basis of concept, departments and territories. </p>
						</li>
						<li id="change-password">
						  <h3>How do i change my Password?</h3>
						  <p>We'll tell you! Couple of ways:</p>
						  <p>1. Click the tiny arrow on right upper side of the screen, just beside your name. You will see a drop down which says "Change Password". Click that, enter your current password ,and choose a new password of your choice. BOOM! You're done.</p>
						  <p>2. Click on your Profile name in top bar. Click the Edit Profile button on the page. Click on Change Password tab,  choose a new password and BOOM! You're done.</p>
						</li>
						<li id="create-group">
						  <h3>Can I create my own Groups on Intranet?</h3>
						  <p>Yes! You can create as many Groups on Intranet as you want.</p>
						</li>
						<li id="keep-group-private">
						  <h3>Can I keep my Group Private?</h3>
						  <p>Yes! You can create Private Groups. They don't appear in the listing so it's an exclusive invite-only group. Only those invited by the Group creator and moderator can join. Sshh.</p>
						</li>
						<li id="follow-concept">
						  <h3>Can I follow a Concept whom i do not belong to?</h3>
						  <p>Yes, absolutely. Concepts are meant to be followed, and loved. The concepts you follow will fill your wall with their News, Offers and Announcements.</p>
						</li>
						<li id="fix-profile">
						  <h3>My Profile information is incorrect on Intranet. How should i get it fixed?</h3>
						  <p>Just shout out to us ...or type to us at intranet@landmarkgroup.com with your Employee ID and tell us what's wrong.. We'll  look into this like our life depends on it. No lie.</p>
						</li>
					</ul>
				</div>
				<!-- concept-grid -->
			</div>
			<!-- container -->
			
		</div>
		<div class="right-contents media-widget">
		</div>
		<!-- right-contents -->
	</div>
	<!-- section -->
	
<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>
<script type="text/javascript">
    $(function() {
		$("img:below-the-fold").lazyload({
			event : "sporty"
		});
	});
	
	$(window).bind("load", function() {
		var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
	}); 
</script>
</body>
</html>
