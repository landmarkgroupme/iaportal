<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
	<meta name="robots" content="index, nofollow">
	<title>Help - Landmark Group</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">
<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
		<h2>
			Instant answers to common questions
		</h2>
		<ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Help</li>
    </ul>
	</div>
	<div class="section wrapper clearfix">
		<div class="left-contents">
			<div class="container">
				<?
				//include_once "ssi/comp_jobs_filter.php";
				?>
				<div class="concept-grid clearfix" id="media-grid">
				    <ul class="left-help-block">
					    <li><a href="<?php echo site_url(); ?>faq#follow-coworker">How do I follow a coworker?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#follow">Well, what do you mean by 'follow'?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#unfollow-someone">Can I unfollow someone?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#find-people">How do I find People within Landmark Group?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#change-password">How do i change my Password?</a></li>
					</ul>
				    <ul class="right-help-block">
					    <li><a href="<?php echo site_url(); ?>faq#create-group">Can I create my own Groups on Intranet?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#keep-group-private">Can I keep my Group Private?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#follow-concept">Can I follow a Concept whom i do not belong to?</a></li>
					    <li><a href="<?php echo site_url(); ?>faq#fix-profile">My Profile information is incorrect on Intranet. How should i get it fixed?</a></li>
					</ul>
					<ul class="faqs-block">
					    <li>Read <a href="<?php echo site_url(); ?>faq">all 9 FAQs.</a></li>
					</ul>
				</div>
				<!-- concept-grid -->
			</div>
			<!-- container -->
			<!-- <div class="intro-video clearfix" style="margin-bottom:40px;">
			    <video id="video1" width="640" height="360" controls>
                  <source src="images/1.mp4" type="video/mp4">
                  <source src="images/1.ogv" type="video/ogg">
                  Your browser does not support HTML5 video.
                </video>
			</div> -->
			
		</div>
		<div class="right-contents media-widget">
		</div>
		<!-- right-contents -->
	</div>
	<!-- section -->
	
<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?> 
<script type="text/javascript">
    $(function() {
		$("img:below-the-fold").lazyload({
			event : "sporty"
		});
	});
	
	$(window).bind("load", function() {
		var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
	}); 
</script>
</body>
</html>
