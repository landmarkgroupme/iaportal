<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Code of Conduct - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">
<?php $this->load->view('global_header.php'); ?>
<div class="section wrapper clearfix">
	<h2>Code of Conduct</h2>
	 <ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Code of Conduct</li>
    </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="job_details clearfix">
	 <div class="job_description">
	 <div class="jd-title"><b>Code of Conduct</b></div>
    	 <table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"  >
                  <tr>
                    <td align="left"  valign="top" >
                      <table cellspacing="0" cellpadding="0" border="0" >
                        <tr>
                          <td align="left" valign="top" style="" >                            
                              <p>For our new, socially networked Intranet, we need to adhere to a strict code of conduct to ensure that the Intranet is a purely professional social network(like LinkedIn, not Facebook) and that our communication remains professional at all times.</p>
                              
                              <p>Please review the following guidelines on how we can achieve this:</p>
                              <p><strong>IMPORTANT:</strong></p>
                              <p><strong>Time management:</strong>  While the new socially-networked Intranet is an innovative communication tool for employees, please be aware of the amount of time you spend on the Intranet daily. In no way should your daily responsibilities be affected by it. It's not in your best interest to get questioned about this by your supervisor. </p>
                              <p>
                              <strong>No anonymity:</strong>  Do remember that there is absolutely no anonymity in the new Intranet. Your posts can be viewed by a community of over 2000 employees, so always think before submitting and remember that your reputation can be affected by the quality and frequency of your posts.</p>
                              </p>
                              <p>
                              <strong>Language:</strong>  When posting anything, please remember to post in English. The Landmark Group is a multi-national community with employees from several parts of the world. However, English is the universal communication language that binds us all. Your posts should always be comprehensible by all 2000+ community members.</p>
                              </p>
                              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                  <td align="left" valign="top" class="iconrow">
                                    <img src="<?=base_url()?>images/code_of_conduct/profile_pic.jpg" class="mainicon" />                                 </td>
                                  <td align="left" valign="top" style="">
                                  
                                    <h3>Profile Photos</h3>
                                    <p>Profile photos are a great way to personalize the new Intranet experience. It visually represents you even to colleagues in the employee community who you may not have met as yet.</p>
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                      <tr>
                                        <td align="left" valign="top" >
                                          <table>
                                            <tr>
                                              <td class="iconcell">
                                                <img src="<?=base_url()?>images/code_of_conduct/right.jpg"/>                                              
                                              </td>
                                              <td  >
                                                <p><strong>What works:</strong> Real and friendly photos of you. </p>                                        
                                              </td>
                                            </tr>
                                            <tr valign="top">
                                              <td class="iconcell">
                                                <img src="<?=base_url()?>images/code_of_conduct/wrong.jpg"/>                                              
                                              </td>
                                              <td >
                                                <p><strong>What doesn't:</strong> Animated characters, symbols, logos and other images that don't visually communicate the real you.    </p>                                         
                                              </td>
                                            </tr>
                                          </table>                                        
                                        </td>
                                        <td align="left" valign="top" id="prfl-pic-frmt">
                                          <img src="<?=base_url()?>images/code_of_conduct/right_wrong_pic.jpg">                                       
                                        </td>
                                      </tr>
                                    </table>                                        
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" style="padding-top:20px;" class="iconrow">
                                    <img src="<?=base_url()?>images/code_of_conduct/profile.jpg" class="mainicon"/>                                 
                                  </td>
                                  <td align="left" valign="top" style="padding-left:5px;padding-top:15px;">
                                    <h3>Your Profile</h3>
                                    <p>Colleagues and the Landmark Group community can learn a little more about who you are, your skills, experience and interests through your profile. Do take the time to add this information.</p>
                                    <table>
                                      <tr>
                                        <td class="iconcell">
                                          <img src="<?=base_url()?>images/code_of_conduct/right.jpg"/>                                        
                                        </td>
                                        <td  >
                                          <p><strong>What works:</strong> Honest descriptions of your skills and experience and bits about you.</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell">
                                          <img src="<?=base_url()?>images/code_of_conduct/wrong.jpg"/>                                        
                                        </td>
                                        <td  >
                                          <p><strong>What doesn't:</strong> Anything that could be deemed objectionable, disrespectful or offensive by another member of the employee community. </p>
                                        </td>
                                      </tr>
                                    </table>                                  
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" style="padding-top:7px;" class="iconrow">
                                    <img src="<?=base_url()?>images/code_of_conduct/group.jpg" class="mainicon"/>                                 
                                  </td>
                                  <td align="left" valign="top" style="padding-left:5px;padding-top:15px;">
                                    <h3>Interest Groups</h4>
                                    <p>Interest groups allow you to connect with colleagues that have the same interests that you do. When you enter an interest group that already exists, choose that group from the auto-complete to become another member of that group. If no group exists, your entry will create a new interest group that others may join if they share that same interest.</p>
                                    <table>
                                      <tr>
                                        <td class="iconcell">
                                          <img src="<?=base_url()?>images/code_of_conduct/right.jpg"/>                                        
                                        </td>
                                        <td>
                                          <p><strong>What works:</strong> Do limit interest group names to one or two words each. Eg: Football instead of Foot Ball, Rafting instead of White Water Rafting. This helps make groups easier to find by other users with same interests.</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/wrong.jpg"/>                                        
                                        </td>
                                        <td >
                                          <p><strong>What doesn't:</strong> Anything that could be deemed objectionable, disrespectful or offensive by another member of the employee community. </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell">
                                          <img src="<?=base_url()?>images/code_of_conduct/moderation.jpg"/>                                       
                                        </td>
                                        <td >
                                          <p><strong>Moderation:</strong> If you believe an interest group is an issue, you can report this in by writing to the web team at intranet@landmarkgroup.com, adding a comment on why you thought the interest group was a problem, and submitting your request. A moderator from the web team will review this and decide whether to remove or keep the interest group.</p>
                                        </td>
                                      </tr>
                                    </table>                                  
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" style="padding-top:10px;" class="iconrow">
                                    <img src="<?=base_url()?>images/code_of_conduct/status.jpg" class="mainicon"/>                                  
                                  </td>
                                  <td align="left" valign="top" style="padding-left:5px;padding-top:15px;">
                                    <h3>Status Updates</h3>
                                    <p>Status updates are almost second nature to anyone already familiar to a social network like LinkedIn, Twitter or Facebook. If you haven't used status updates before, they are a great way to communicate thoughts, knowledge via link sharing, or even ask questions to the employee community.</p> 
                                    <table>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/right.jpg"/>                                        
                                        </td>
                                        <td >
                                          <p><strong>What works:</strong> Updates that are work-related and useful.                                      
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/wrong.jpg"/>                                        
                                        </td>
                                        <td  >
                                          <p><strong>What doesn't:</strong> Updates that are personal, mundane, not related to work, or may be insulting, offensive, objectionable or disrespectful.  Eg. "Hi...", "Have a nice weekend", non-work related 'thoughts of the day' and personal replies to other users' updates etc. </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/moderation.jpg"/>                                       
                                        </td>
                                        <td >
                                          <p><strong>Moderation:</strong> If you believe someone's status update is an issue, you can report this in by clicking on the "Report" link, adding a comment on why you thought the status was a problem, and submitting your request. Their status is removed and a moderator from the web team will review this and decide whether to permanently remove or keep the status.</p>
                                        </td>
                                      </tr>
                                    </table>                                  
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" style="padding-top:12px;" class="iconrow">
                                    <img src="<?=base_url()?>images/code_of_conduct/files.jpg" class="mainicon"/>                                 
                                  </td>
                                  <td align="left" valign="top" style="padding-left:5px;padding-top:15px;">
                                    <h3>Files & Links</h3>
                                    <p>The new Files & Links section allows you to privately share large files (upto 100mb for 7 days) with another or several colleagues via the Dropbox feature. It also allows you to share files and links with the entire employee community through the "Upload file" feature.</p>
                                    If you do decide to share a file or link with the entire online community...</p>
                                    <table>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/right.jpg"/>                                        
                                        </td>
                                        <td >
                                          <p><strong>What works:</strong> Ensure that your file or link is professional, useful and relates to your role and responsibilities.</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td  class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/wrong.jpg"/>                                        
                                        </td>
                                        <td  >
                                          <p><strong>What doesn't:</strong> Personal or confidential files and links. Anything that may be deemed illegal, disrespectful, objectionable or offensive by another member of the employee community.</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/moderation.jpg"/>                                       
                                        </td>
                                        <td >
                                          <p><strong>Moderation:</strong> If you believe a file or a link is an issue, you can report this in by clicking on the "Report" link, adding a comment on why you thought the file/link was a problem, and submitting your request. The file/link is removed and a moderator from the web team will review this and decide whether to permanently remove or keep the file/link.</p>
                                        </td>
                                      </tr>
                                    </table>                                  
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" style="padding-top:10px;" class="iconrow">
                                    <img src="<?=base_url()?>images/code_of_conduct/market.jpg" class="mainicon"/>                                  
                                  </td>
                                  <td align="left" valign="top" style="padding-left:5px;padding-top:15px;">
                                    <h3>Marketplace</h3>
                                    <p>Think of the Marketplace as the Landmark Group's very own online bulletin board or classifieds section. Got a book, movie, or a car for sale? Add it right here. Need a roommate or something specific like a TV, sofa, or music system? Request it from the Marketplace as well. It even allows you to give stuff away for free.</p>
                                    <table>
                                      <tr>
                                        <td class="iconcell" >
                                          <img src="<?=base_url()?>images/code_of_conduct/right.jpg"/>                                        
                                        </td>
                                        <td>
                                          <p><strong>What works:</strong> Selling, requesting or gifting legitimate, legal items</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td  class="iconcell">
                                          <img src="<?=base_url()?>images/code_of_conduct/wrong.jpg"/>                                        
                                        </td>
                                        <td >
                                          <p><strong>What doesn't:</strong> Trying to make a personal business out of this feature; selling or requesting anything that may be deemed as illegal, disrespectful, objectionable or offensive.</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="iconcell">
                                          <img src="<?=base_url()?>images/code_of_conduct/moderation.jpg"/>                                       
                                        </td>
                                        <td >
                                          <p><strong>Moderation:</strong> If you believe a Marketplace item is an issue, you can report this in by clicking on the "Report" link, adding a comment on why you thought the item was a problem, and submitting your request. The item is removed and a moderator from the web team will review this and decide whether to permanently remove or keep the item.</p>
                                        </td>
                                      </tr>
                                      <tr >
                                        <td colspan="2" style="padding-top:15px;padding-bottom:50px;">
                                          <p>If you notice that your colleagues aren't following the above, please advise them to do so. It's our collective responsibility to ensure that communication in our new Intranet is kept professional at all times.</p> 
                                        
                                        </td>
                                      </tr>
                                    </table>                                  
                                  </td>
                                </tr>
                              </table>                          
                            </td>
                        </tr>
                      </table>                    </td>
                  </tr>
                </table>  
</div>
    </div>



</div> <!-- container -->

</div> <!-- left-contents -->

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
    $(function() {
		$("img:below-the-fold").lazyload({
			event : "sporty"
		});
	});
	
	$(window).bind("load", function() {
		var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
	}); 
</script>

</body>
</html>
