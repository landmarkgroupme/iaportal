<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Intranet 2.0 Release Notes - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

</head>
<body class="full-width">

<?php $this->load->view('global_header.php'); ?>

<div class="section wrapper clearfix">
	<h2>Intranet 2.0 Release Notes</h2>
	 <ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Release Notes</li>
    </ul>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="job_details clearfix">
	 <div class="job_description">
    	 <div class="jd-title"><b>Feature Changes:</b></div>

			<div class="jd-title"><b>Features Added</b></div>
			<ul>
				<li>+ Ability to comment on status updates</li>
				<li>+ Ability to like someone's status</li>
				<li>+ Ability to choose a username and unique vanity urls (e.g http://intranet.landmarkgroup.com/aqeel)</li>
				<li>+ Ability to login with your username instead of full email (e.g shahid)</li>
				<li>+ Ability to change display name (killed the middle name)</li>
				<li>+ Watch out for birthdays in your department and concept</li>
				<li>+ Create Polls and have opinions</li>
				<li>+ Users can upload photos on theirs walls, (other than profile photos)</li>
				<li>+ A Fresh, Fast, Robust UI for all the sections</li>
				<li>+ Watch out for jobs with Landmark Group, Apply or Recommend a Friend</li>
				<li>+ Ability to Follow concepts</li>
				<li>+ Concepts have their own walls with their specific content. These concepts are being moderated by Concept Manager who are responsible for posting content and keeping their pages active</li>
				<li>+ Ability for Concept managers to post content to their concept without going to a backend panel. they can manage/moderate their content while staying on their page</li>
				<li>+ Introduced Public and Private Groups. Groups have their own wallboard, Polls, Files. Private Groups can even have private files which are shared with all members within group and nobody else OR they can share the files exclusively to few people within the Group</li>
				<li>+ Users can join Public Groups and Private Groups are open only on invitation basis sent by Group Moderators</li>
				<li>+ Users can create their own Groups</li>
				<li>+ Added Social Notifications. When someone likes or comment on your status, You receive a notification and can respond from notification area or the activities page</li>
				<li>+ Profiles have completeness progress so people can get to know and will complete their profiles to make them 100%</li>
			</ul>

			<div class="jd-title"><b>Features Removed</b></div>
			<ul>
				<li>
				- Remove Events from Intranet
				</li>
			</ul>

			<div class="jd-title"><b>Improvements:</b></div>
			<ul>
				<li>
				﻿+ ﻿Removed unnecessary "index.php" from urls throughout website.
				</li>
			</ul>
</div>
    </div>



</div> <!-- container -->

</div> <!-- left-contents -->

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>
<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
    $(function() {
		$("img:below-the-fold").lazyload({
			event : "sporty"
		});
	});
	
	$(window).bind("load", function() {
		var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
	}); 
</script>

</body>
</html>
