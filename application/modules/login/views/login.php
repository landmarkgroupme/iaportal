<!DOCTYPE html>
<html>
<head>
<?php 
$ci = get_instance(); // CI_Loader instance
$ci->load->config('intranet');
?>
<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Login - Landmark Group</title>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/a7307b6d-4641-44d1-b16a-c22888f05455.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/default.css?v=<?php echo $ci->config->item('css_ver'); ?>"" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/common.css?v=<?php echo $ci->config->item('css_ver'); ?>"" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/login.css?v=<?php echo $ci->config->item('css_ver'); ?>"" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>media/css/media.css?v=<?php echo $ci->config->item('css_ver'); ?>"" />
</head>
<body class="login login-bg">
<img src="<?php echo site_url(); ?>media/images/loader1.gif" style="display: none" />
<div class="wrapper">


	<div class="logo-intro">
		<img src="<?php echo site_url(); ?>media/images/logo_intro.png" width="658" height="102" border="0" alt="INTRANET @ LANDMARK - IT'S ALL ABOUT YOU."/><br>
		Our Intranet makes it easy for Landmarkers to come together and know more about each other
	</div>

	<div class="login_box">
		<div class="login-c">
			<ul id="mLoginPass" class="msg msg1 d-n"></ul>

			<form id="frmLogin" method="post" autocomplete="off" class="d-n">
				<fieldset>
					<div class="textC textCPos"><input type="text" id="txtUsername" name="email" placeholder="Your Email or Username" /></div>
					<div class="textC textCPos"><input type="password" id="txtPassword" name="password" placeholder="Password"/></div>
					<div class="textCPos remMe"><label for="chkRemember"><input type="checkbox" id="chkRemember" name="chkRemember" />Remember me</label></div>
					<div class="textCPos textC-c"><input type="submit" name="login" id="login" value="Login" class="tw_like_button"/></div>
					<div class="textCPos textC-c"><a id="aForgot" class="blue-link-small" href="#">Forgot Password</a></div>
					<input type="hidden" name="validate_01" class="validate" value="true"/>
				</fieldset>
			</form>
			<form id="frmForgot" method="post" autocomplete="off" class="d-n">
				<fieldset>
				<ul class="msg d-n"></ul>
					<div class="textC textCPos"><input type="text" id="txtResetEmail" name="txtResetEmail" placeholder="Your Email" /></div>
					<div class="textCPos textC-c"><input type="button" id="forget" name="forgot" value="Request Reset" class="tw_like_button"/></div>
					<div class="textCPos textC-c"><a id="aLogin" class="blue-link-small" href="#">Back to Login</a></div>
				</fieldset>
			</form>
			<form id="frmReset" method="post" autocomplete="off" class="d-n">
				<input name="reset_password_token" type="hidden" value="<?php echo isset($reset_password_token) ? $reset_password_token : '';?>" />
				<fieldset>
					<ul class="msg d-n"></ul>
					<div class="textC textCPos"><input type="password" id="txtPasswordNew" name="password" placeholder="New Password"/></div>
					<div class="textC textCPos"><input type="password" id="txtConfirm" name="confirm" placeholder="Confirm Password"/></div>
					<div class="textCPos textC-c"><input type="button" name="change" id="change" value="Change Password" class="tw_like_button"/></div>
					<div class="textCPos textC-c"><a id="aLogin" class="blue-link-small aForgetLogin" href="#">Login</a></div>
					<input type="hidden" name="validate_01" class="validate" value="true"/>
				</fieldset>
			</form>
		</div>
	</div>

	<!--<ul id="collage">
		<li></li>
	</ul> -->

</div>


<style>

</style>


<!-- <script language="javascript" type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>media/js/jquery-1.8.3/jquery-1.8.3.js"></script>
<script src="<?php echo site_url(); ?>media/js/jquery.form.js"></script>
<script src="<?php echo site_url(); ?>media/js/jquery.validate.js"></script>

<script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>media/js/login.js"></script>

<?php $this->load->view('partials/js_footer'); ?>

<script type="text/javascript">
$(function() {
    $("img:below-the-fold").lazyload({
        event : "sporty"
    });
});
$(window).bind("load", function() {
    var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
});

var imgArray	= [];
var imgArrayWeb	= [];
/*for(var i = 1; i <= 58; i++)
{
	imgArray.push(i+'.jpg');
}*/

$(document).ready(function(){
  if (/MSIE 9|MSIE 8|MSIE 7|MSIE 6/g.test(navigator.userAgent)) {
 $('#frmLogin input[type=password]').live('blur',function() {
		if($(this).val() == "Password"){
		$(this).val('');	
		}
	});
	$('#frmLogin input[type=password]').live('focus',function() {
		if($(this).val() == "Password"){
		$(this).val('');	
		}
	});
}
	<?php
	if(isset($user_profile))
	{ //this is a reset password section

		$date = new DateTime('NOW');
		$reset_date = new DateTime($user_profile['reset_password_expiry']);
		if($date < $reset_date)
		{	//checking if the reset link is valid or expired

			?>
			$('#frmReset').removeClass('d-n');
			<?php
		} else {
			//reset link is expired
			?>
			/*$('.msg').addClass('error');
	  		$('.msg').text('Your Reset Password link has expired, please try to reset again');
	  		$('.msg').removeClass('d-n');
	  		$('.msg').show();*/
			setError('#mLoginPass', 'Your Reset Password link has expired, please try to reset again');
			$('#frmForgot').removeClass('d-n');
			$('.wrapper .logo-intro').setAlignCenter();
			<?php
		}
		?>

		<?php
	} else {
		if(isset($reset_password_token) && $reset_password_token != '')
		{
			?>
			/*$('.msg').addClass('error');
	  		$('.msg').text('Your Reset Password link is invalid');
	  		$('.msg').removeClass('d-n');
	  		$('.msg').show();*/
			setError('#mLoginPass', 'Your Reset Password link is invalid');
			<?php
		}
		?>
		$('#frmLogin').removeClass('d-n');
		$('.wrapper .logo-intro').setAlignCenter();
		<?php
	}
	?>

	$('#txtPassword, #txtUsername').live('keypress',function (e) {
	  if (e.which == 13) {
	    checkLogin();
	    e.preventDefault();
	  }
	});
	/*$('div.login_box').hide();
	$('div.login_box')fadeIn()*/
	$('div.login_box').css({
    	opacity: 1,
    	transition : 'opacity 1s ease-in-out'
	});

	$.fn.showLoader = function(_status, _redirectUrl){
		if(_status == true){
			$(this).addClass('d-n');
			$(this).parent().css('text-align', 'center');
			$(this).parent().append('<img src="<?php echo site_url(); ?>media/images/loader1.gif" />');
			setTimeout(function() {
				if(_redirectUrl != undefined)
				window.location.href = _redirectUrl;
			}, 2000);
		}else{
			$(this).removeClass('d-n');
			$(this).parent().css('text-align', 'left');
			$(this).parent().find('img').remove();
		}
	}

	$('#login').click(function(e){
		checkLogin($(this).closest('form'));
		e.preventDefault();
	});


	/*$('#frmLogin').on('submit', function(e){
		alert('here');
		return;

		if(($('#txtUsername').val().length < 6) || ($('#txtPassword').val().length < 6)){
			setError('#mLoginPass', 'Username or password you entered is incorrect.');
			return false;
		}
		checkLogin($(this));
		e.preventDefault();
	});*/

/*	$('#login').click(function(e){
		checkLogin();
		e.preventDefault();
	});

	$('#forget').click(function(e){
		resetPassword();
	});
*/

	$('#forget').click(function(e){
		if(($('#txtResetEmail').val().length < 6)){
			setError('#mLoginPass', 'Username entered is incorrect.');
			return false;
		}
		resetPassword($(this));
		e.preventDefault();
	});


	$('#change').click(function(e){
		changePassword();
	});

	

});

	function checkLogin(_fId) {

		$.ajax({
		  type: "POST",
		  url: '<?php echo site_url(); ?>login/validate_user',
		  data: $("#frmLogin").serialize(),
		  beforeSend: function(){
			$(_fId).showLoader(true);
		  },
		  success: function(response){
		  	if(response.status == 'success')
		  	{
		  		//$('.msg').addClass('success');
		  		//$('.msg').text('');
		  		//$('.msg').removeClass('d-n');
		  		window.location = response.redirect_uri;
		  	}
		  	else {
          /*$('.msg').addClass('error');
          $('.msg').text('Please check your email/username and password');
          $('.msg').show();*/
          setError('#mLoginPass', 'Please check your email/username and password.');
          //$('#txtUsername').val('');
          $('#txtPassword').val('');
          $(_fId).showLoader(false);
		  	}
		  },
		  error: function(){
			  $(_fId).showLoader(false);
		  },
		  dataType: 'json'
		});
	}

	function resetPassword(_fId) {
		$.ajax({
		  type: "POST",
		  url: '<?php echo site_url(); ?>login/reset_password',
		  data: $("#frmForgot").serialize(),
		  beforeSend: function(){
			$(_fId).showLoader(true);
		  },
		  success: function(response){
		  	if(response.status == 'success')
		  	{
				setSuccess('#frmForgot .msg', 'Check email for instructions to reset password.');
				$(_fId).showLoader(false);
		  	}
		  	else {
				setError('#frmForgot .msg', 'Please make sure if you entered correct email, We were unable to recognise your email.');
				$(_fId).showLoader(false);
		  	}
		  },
		  error: function(){
			  $(_fId).showLoader(false);
		  },
		  dataType: 'json'
		});
	}

	function changePassword() {
		$.ajax({
		  type: "POST",
		  url: '<?php echo site_url(); ?>login/change_password',
		  data: $("#frmReset").serialize(),
		  success: function(response){
		  	if(response.status == 'success')
		  	{
				setSuccess('#frmReset .msg', 'Your password has been changed. You can now login with your new password.');
		  	}
		  	else {
				setError('#frmReset .msg', 'Please make sure password and confirm password match and you have clicked the reset link within 24 hrs.');
		  	}
		  },
		  dataType: 'json'
		});
	}




</script>

</body>
</html>
