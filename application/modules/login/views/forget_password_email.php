<table width="520" border="0" style="font-family:Arial;">
          <tr>
            <td  style="text-align:right;padding-right:20px;"><p style="color:#828282;font-size:12px;"><?php echo date("l, d F Y"); ?></p></td>
          </tr>
          <tr>
            <td style="padding-left:20px;padding-bottom:15px;"><img src="<?php echo base_url() . 'images/logo.jpg'; ?>" /></td>
          </tr>
          <tr>
            <td style="background:#f1f1f1;padding-left:20px;padding-right:20px;padding-bottom:30px;">
              <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">Intranet Reset Password Help.</p>
              <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;"><strong>Hi <?php echo $first_name.' '.$last_name; ?>,</strong> </p>
              <p style="color:#3e3e3e;font-size:14px; margin:5px 0 0 0; line-height:20px;">We have received a request to reset your password. If you have made the reset request, click on the link below. If you haven’t, just ignore this mail.</p>
              <table  style="color:#3e3e3e;font-size:14px;margin-top:10px;margin-left:20px;">
                <tr>
                  <td>Link:</td>
                  <td><a href="<?php echo site_url(); ?>login/reset/<?php echo $reset_password_token; ?>"><?php echo site_url(); ?>login/reset/<?php echo $reset_password_token; ?></a></td>
                </tr>
              </table>
              <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">If you are unable to click on the link, copy it and paste in the URL.</p>
              <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Thanks,<br />
              Landmark Intranet Team</p>
            </td>
          </tr>
        </table>