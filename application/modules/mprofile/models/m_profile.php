<?php
class M_profile extends CI_Model {

	function M_profile() {
		parent::__construct();
	}
	
	function get_profile($id,$user_id) {
		
		$sql = "select
					user.id,
					CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                                        CONCAT(UCASE(SUBSTRING(`middle_name`, 1,1)),LOWER(SUBSTRING(`middle_name`, 2))) as middle_name,
					user.profile_pic,
					designation.name designation,
					department.name department,
					concept.name concept,
					location.name location,
					territory.name territory,
					user.extension extension,
					user.phone phone,
					user.mobile mobile,
					user.email email,
					user.about about,
					user.skills skills,
					user.hrms_id hrms_id,
					user.title title,
          user.reply_username,
					(SELECT message FROM ci_status_updates WHERE object_id = $user_id AND status = 1 ORDER BY id DESC limit 1) as your_status,
					(SELECT following_user_id FROM ci_users_following_users WHERE user_id = $user_id AND follow_status = 1 AND following_user_id = user.id) as following_user_id,
					(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = user.id  AND contact_status = 1 AND user_id = $user_id)as contact_user
				from
					ci_users user,
					ci_master_designation designation,
					ci_master_concept concept,
					ci_master_department department,
					ci_master_location location,
					ci_master_territory territory
				where
					user.id = $user_id and
					user.designation_id = designation.id and
					user.concept_id = concept.id and
					user.department_id = department.id and
					user.location_id = location.id and
					user.territory_id = territory.id";
		
		$qry = $this->db->query($sql);
		
		$data = array();
		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row) {
				$data = $row;
			}
		}
		$qry->free_result();
		return $data;
	}
	
	function get_profile_by_email($email) {
		
		$sql = "select
				user.id,
				CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                                        CONCAT(UCASE(SUBSTRING(`middle_name`, 1,1)),LOWER(SUBSTRING(`middle_name`, 2))) as middle_name,
				user.profile_pic,
				designation.name designation,
				department.name department,
				concept.name concept,
				location.name location,
				territory.name territory,
				user.extension extension,
				user.phone phone,
				user.mobile mobile,
				user.email email,
				user.about about,
				user.skills skills,
				(SELECT message FROM ci_users_status_updates WHERE user_id = user.id AND status = 1 ORDER BY id DESC limit 1) as your_status
				from
				ci_users user,
				ci_master_designation designation,
				ci_master_concept concept,
				ci_master_department department,
				ci_master_location location,
				ci_master_territory territory
			where
				user.email = '$email' and
				user.designation_id = designation.id and
				user.concept_id = concept.id and
				user.department_id = department.id and
				user.location_id = location.id and
				user.territory_id = territory.id";
		
		$qry = $this->db->query($sql);
		
		$data = array();
		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row) {
				$data[] = $row;
			}
		}
		$qry->free_result();
		return $data[0];
	}
	
	//Get all Designations for Edit Profile
	function get_designations() {
		$this->db->order_by("name", "asc"); 
		return $this->db->get('ci_master_designation')->result();
	}
	
	//Get all Concepts for Edit Profile
	function get_concepts() {
		$this->db->order_by("name", "asc"); 
		return $this->db->get('ci_master_concept')->result();
	}

	//Status Update
	
	function su_show_my_updates($pagelimit,$userId,$profileId){
		$limit = (int)$pagelimit;
		$offset = (int)$this->session->userdata('cfgpagelimit');
		$sql = 'SELECT 
						(SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.user_id ) as following_user_id,
							b.id as message_id,
						 	b.message,
							b.user_id,
							b.message_time,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							c.profile_pic,
							c.email,
							d.name as designation,
							e.name as concept
						FROM 
							ci_users_status_updates b,
							ci_users c,
							ci_master_designation d,
							ci_master_concept e
						WHERE
							c.id = b.user_id AND
							b.status = 1 AND
							d.id = designation_id AND
							e.id = concept_id AND
							c.id = ?
						ORDER BY b.id DESC LIMIT ?, ?';	
		$result= $this->db->query($sql,array($userId,$profileId,$limit,$offset));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
//Files
/* Files count All active files */
	function files_get_files_count($profile_id){
		$this->db->select('count(id)as files_count');
		$result=$this->db->get_where('ci_files',array('files_status' => 1,'user_id' => $profile_id));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data  = $row;
			}
		}
		$result->free_result();
		return $data;
	}
	/* Get tags for file */
	function files_get_tags_for_file($file_id){
		$sql = "SELECT a.name, a.id FROM ci_master_file_tag a,ci_map_files_tag b WHERE a.id=b.file_tag_id AND b.files_id = ? ";
		$result=$this->db->query($sql,$file_id);
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}	
/* List All Files system */	
	function files_get_all_files($profile_id,$limit,$offset){
		$sql = "SELECT 
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.download_count,
			a.path,
			a.unique_id,
			a.uploaded_on,
			b.name as category,
			b.id as cat_id,
			CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
			c.id as user_id
		FROM 
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE 
			a.files_status = 1 AND
			a.user_id = {$profile_id} AND
			a.file_category_id = b.id AND
			a.user_id = c.id
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";
		
		$result=$this->db->query($sql);
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
//Interest Groups
	function get_users_interest_groups($user_id){
		$sql = "SELECT 
							a.name, 
							a.id 
						FROM 
							ci_interest_groups a,
							ci_map_intrest_users b
						WHERE
							b.user_id = ? AND
							b.intrest_groups_id = a.id";
		$result=$this->db->query($sql,array($user_id));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}	
/* Autocomplete Interest Groups */
	function get_all_interest_groups($src_term){
		$this->db->like('name',$src_term , 'after'); 
		$result=$this->db->get('ci_intrest_groups');
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data[] = $row;
			}
		}
		$result->free_result();
		return $data;
	}
	function update_user_info($user_details,$user_id){
		$this->db->where('id', $user_id);
		$this->db->update('ci_users', $user_details);
		return $this->db->affected_rows();
	}
	
//Interest Groups Remove for one User
	function remove_user_interest_groups($user_id){
		$sql = "DELETE FROM `ci_map_intrest_users` WHERE `user_id` = ?";
		$result=$this->db->query($sql,array($user_id));
		return $this->db->affected_rows();
	}
//Interest Groups check by name
	function get_interest_groups_id($group_name){
		$sql = "SELECT 
							id 
						FROM 
							ci_intrest_groups 
						WHERE
							name = ? ";
		$result=$this->db->query($sql,array($group_name));
		$data = array();
		if($result->num_rows() > 0){
			foreach($result->result_array() as $row){
				$data = $row['id'];
			}
		}
		$result->free_result();
		return $data;
	}
	//Insert Interest Groups
	function insert_interest_groups($data){
		$str = $this->db->insert('ci_intrest_groups', $data);
		return $this->db->insert_id();
	}
	//Update user and interest groups
	function insert_user_interest_groups($data){
		$this->db->insert('ci_map_intrest_users', $data);
		return $this->db->affected_rows();
	}
	//Update Password
	function update_user_password($user_details,$user_id){
		$this->db->where('id', $user_id);
		$this->db->update('ci_users', $user_details);
		return $this->db->affected_rows();
	}
	//Update Fav. link
	function update_fav_link($link_details,$user_id,$link_id){
		$this->db->update('ci_fav_links', $link_details, array('id' => $link_id,"user_id"=>$user_id));
		return $this->db->affected_rows();
	}
	//Update Profile Pic NAme
	function update_profile_pic($user_details,$user_id){
		$this->db->update('ci_users', $user_details, array("id"=>$user_id));
		return $this->db->affected_rows();
	}
	//Add fav. link
	function insert_fav_link($data){
	  $this->db->insert('ci_fav_links', $data);
    return $this->db->affected_rows();
	}
	//Add file report to db
	function insert_file_report($report_file){
	  $this->db->insert('ci_report_files', $report_file);
    return $this->db->affected_rows();
	}
	//Add Market report to db
	function insert_market_report($report_item){
	  $this->db->insert('ci_report_market', $report_item);
    return $this->db->affected_rows();
	}
	//Update File status for File Report
	function update_file_status($update_file_status,$file_id){
		$this->db->update('ci_files', $update_file_status, array('id' => $file_id));
		return $this->db->affected_rows();
	}
	//Update Market item status for Market Report
	function update_item_status($update_item_status,$item_id){
		$this->db->update('ci_market_items', $update_item_status, array('id' => $item_id));
		return $this->db->affected_rows();
	}
	//Update Message status for status update
	function update_message_status($update_message_status,$message_id){
		$this->db->update('ci_users_status_updates', $update_message_status, array('id' => $message_id));
		return $this->db->affected_rows();
	}
	//Add message report to db
	function insert_message_report($report_status_update){
	  $this->db->insert('ci_report_status_updates', $report_status_update);
    return $this->db->affected_rows();
	}
}