<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/profile/my_profile.js"></script>
	
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
	  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url()."/people/"?>">People</a></li>
					<li>My Profile</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?php echo ucfirst(strtolower($profile['first_name'])) . ' ' . ucfirst(strtolower($profile['last_name'])); ?></h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li class="active"><a href="<?=site_url("profile/myprofile/")?>"><span><em>Info</em></span></a></li>
							<li><a href="<?=site_url("profile/status_updates/".$profile['id'])?>"><span><em>Status Updates</em></span></a></li>
							<li><a href="<?=site_url("profile/files/".$profile['id'])?>"><span><em>Files</em></span></a></li>
							<li><a href="<?=site_url("profile/items/".$profile['id'])?>"><span><em>Items</em></span></a></li>
						</ul>
					</div>
				</div>
				
				<form action="<?=base_url()?>index.php/status_update/submit_status_message" method="post" class="share-form-textarea">
          <fieldset>
            <textarea class="textarea" name="txtarShare" id="txtarShare" rows="4" cols="4">What are you working on ?</textarea>
            <div id="status-msg-count">160</div><input type="image" id="btn-ar-share" src="<?=base_url();?>images/btn-share-txtar.jpg" alt="Share!"/>
          </fieldset>
        </form>
        <div id="status-msg-status" class="msg-ok"></div>
        <br />
				
				<!-- status message -->
				<!-- <div class="status-message my-profile-status-message"><strong>Your Status :</strong> <?=(strlen($profile['your_status']) > 70)?substr(strip_tags($profile['your_status']),0,70)."..":strip_tags($profile['your_status'])?></div>-->
        <?php if ($this->session->userdata('first_login_time')): ?>
				<div class="msg-box">
          <div class="msg-box-title">Welcome to the new Intranet!</div>
          <p>We see that you have logged in for the first time. Do <a rel="pop_win" href="<?=site_url("profile/change_photo")?>">upload a nice picture</a> of yourself and add some of 
your details. A richer profile will help people find you and connect for relevant activities. Don’t forget to <a href="<?=site_url("profile/change_password")?>" rel="pop_win" class="password">change your password</a>.</p>
        </div>
        <?php endif;?>
				<!-- profile details -->
				<div class="container">
					<div class="aside">
						<div class="thumb">
							<img class="profile_pic" src="<?=base_url()?>images/user-images/105x101/<?=$profile['profile_pic']?>" alt="image description" width="105" height="101" />
						</div>
						<ul>
							<li><a rel="pop_win" href="<?=site_url("profile/change_photo")?>" class="photo">Change Photo</a></li>
							<li><a href="<?=site_url("profile/edit_profile")?>" class="profile">Edit Profile</a></li>
							<li><a href="<?=site_url("profile/change_password")?>" rel="pop_win" class="password">Change Password</a></li>
							<li><a href="<?=site_url("profile/edit_profile#skills")?>" class="specialities">Add Specialities</a></li>
							<li><a href="<?=site_url("profile/fav_links")?>" class="link" rel="pop_win" >Add a link</a></li>
						</ul>
					</div>
					<div class="description vcard">
						<div class="contact-details">
							<div class="container">
								<a href="<?=site_url("profile/download_vcard/".$profile['id'])?>" class="vcard-load">Download vcard</a>
								<h3 class="fn"><?php echo ucfirst(strtolower($profile['first_name'])) . ' ' . ucfirst(strtolower($profile['last_name'])); ?></h3>
							</div>
							<span><strong><?php echo $profile['designation']; ?></strong>, <?php echo $profile['concept']; ?></span>
							<dl class="tel">
								<dt class="type">Ext:</dt>
								<dd><?php echo ($profile['extension'])?$profile['extension']:"&nbsp;"; ?></dd>
								<dt class="type">Phone:</dt>
								<dd><?php echo ($profile['phone'])? $profile['phone']:"&nbsp;"; ?></dd>
								<dt class="type">Mobile:</dt>
								<dd><?php echo ($profile['mobile'])?$profile['mobile']:"&nbsp;"; ?></dd>
								<dt>Email:</dt>
								<dd><a href="mailto:<?php echo $profile['email']; ?>" class="email"><?php echo $profile['email']; ?></a></dd>
							</dl>
						</div>
						<a href="mailto:datacorrection@cplmg.com" class="data-error">Report data error</a>
						<ul class="about-list">
							<li>
								<h3>Skills &amp; Experience</h3>
								<div>
									<p><?php echo $profile['skills']?></p>
								</div>
							</li>
							<li>
								<h3>Interests</h3>
								<div>
									<p> <?=$user_interest_groups?></p>
								</div>
							</li>
							<li>
								<h3>About Me</h3>
								<div>
									<p><?php echo $profile['about']?></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- sidebar box, user box -->
				<?php $this->load->view("includes/useful_link");?>
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/latest_update");?>
				<!-- sidebar box, My Interest Groups -->
				<?php $this->load->view("includes/my_interest_groups");?>
				<!-- sidebar box, Newcomers -->
				<?php $this->load->view("includes/newcomers");?>
				 
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=site_url()."/people/"?>" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="<?=site_url()."/status_update/"?>" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=site_url()."/interest_groups/"?>" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>