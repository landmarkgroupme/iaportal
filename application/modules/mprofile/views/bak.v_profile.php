<?php

$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/profile/view_profile.js"></script>
</head>
<body>
	<?php $this->load->view("includes/admin_nav");?>
	<!-- wrapper -->
	<div id="wrapper">
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url()."/people/"?>">People</a></li>
					<li>Profile</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?php echo $profile['first_name'] . ' ' . $profile['last_name']; ?></h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li class="active"><a href="<?=site_url("profile/view_profile/".$profile['id'])?>"><span><em>Info</em></span></a></li>
							<li><a href="<?=site_url("profile/status_updates/".$profile['id'])?>"><span><em>Status Updates</em></span></a></li>
							<li><a href="<?=site_url("profile/files/".$profile['id'])?>"><span><em>Files</em></span></a></li>
							<li><a href="<?=site_url("profile/items/".$profile['id'])?>"><span><em>Items</em></span></a></li>
						</ul>
					</div>
				</div>
				<!-- status message -->
				<div class="status-message"><strong>Status :</strong> <?=(strlen($profile['your_status']) > 70)?substr(strip_tags($profile['your_status']),0,70)."..":strip_tags($profile['your_status'])?></div>
				<!-- profile details -->
				<div class="container">
					<div class="aside">
						<div class="thumb">
							<img src="<?=base_url()?>images/user-images/105x101/<?=$profile['profile_pic']?>" alt="image description" width="105" height="101" />
						</div>
						<ul>
							<li><a href="mailto:<?php echo $profile['email']; ?>" class="email">Email <?php echo $profile['first_name']; ?></a></li>
							<li><a id="<?=$profile['id']?>" href="<?=base_url()?>index.php/interest_groups/" class="<?=($profile['contact_user'])? "remove-contact":"add-contact"?>"><?=($profile['contact_user'])? "Remove from my contacts":"Add to my contacts"?></a></li>
							
							<li><a href="<?=base_url()?>index.php/status_update/" class="<?=($profile['following_user_id'])? "unfollow":"follow"?>" id="<?=$profile['id']?>" ><?=($profile['following_user_id'])? "Unfollow Updates":"Follow Updates"?></a></li>
						</ul>
					</div>
					<div class="description vcard">
						<div class="contact-details">
							<div class="container">
								<a href="<?=site_url("profile/download_vcard/".$profile['id'])?>" class="vcard-load">Download vcard</a>
								<h3 class="fn"><?php echo $profile['first_name'] . ' ' . $profile['last_name']; ?></h3>
							</div>
							<span><strong><?php echo $profile['designation']; ?></strong>, <?php echo $profile['concept']; ?></span>
							<dl class="tel">
								<dt class="type">Ext:</dt>
								<dd><?php echo ($profile['extension'])? $profile['extension']: "&nbsp;"; ?></dd>
								<dt class="type">Phone:</dt>
								<dd><?php echo ($profile['phone'])? $profile['phone'] : "&nbsp;"; ?></dd>
								<dt class="type">Mobile:</dt>
								<dd><?php echo ($profile['mobile'])?$profile['mobile']: "&nbsp;"; ?></dd>
								<dt>Email:</dt>
								<dd><a href="mailto:<?php echo $profile['email']; ?>" class="email"><?php echo $profile['email']; ?></a></dd>
							</dl>
						</div>
						<a href="mailto:datacorrection@cplmg.com" class="data-error">Report data error</a>
						<ul class="about-list">
							<li>
								<h3>Skills &amp; Experience</h3>
								<div>
									<p><?php echo $profile['skills']?></p>
								</div>
							</li>
							<li>
								<h3>Interests</h3>
								<div>
									<p><?=$user_interest_groups?></p>
								</div>
							</li>
							<li>
								<h3>About Me</h3>
								<div>
									<p><?php echo $profile['about']?></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/user_box");?>
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/my_reminders");?>
				<!-- sidebar box, My Interest Groups -->
				<?php $this->load->view("includes/my_interest_groups");?>
				<!-- sidebar box, Newcomers -->
				<?php $this->load->view("includes/newcomers");?>
				 
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=site_url()."/people/"?>" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="<?=site_url()."/status_update/"?>" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=site_url()."/interest_groups/"?>" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>