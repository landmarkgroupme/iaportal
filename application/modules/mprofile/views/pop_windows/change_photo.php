<div class="pop-win">
	<div class="pop-win-header">
		Change Your Photo
		<span><a href="#"><img class="pop-win-close" src="<?=base_url()?>/images/ico-50.jpg" alt="close-win" /></a></span>
	</div>
	<p class="error-msg"></p>
	 <div class="change-pic">
		 <div class="photo-box">
		 	<div class="pic-title">Current Photo <span>Upload new photo ( .jpg, gif, .png )</span></div>
			<div class="pic-control">
			  <form action="#" method="post" enctype="multipart/form-data" id="frm_change_pic">
  			  <input type="file" name="file_new_pic" id="file_new_pic" /><br /><br /><br />
  			  <input id="btn_change_pic" type="image" src="<?=base_url()?>images/btn-upload.jpg" />
			  </form>
			</div>
			<div class="image-holder"><img class="profile_pic" height="86" width="89" alt="image description" src="<?=base_url()?>images/user-images/105x101/<?=$profile_pic?>"></div>
		</div>
	</div>	
</div>