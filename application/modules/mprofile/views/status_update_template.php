<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/profile/profile_status_update.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
	  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url()."/people/"?>">People</a></li>
					<li><a href="<?=site_url("profile/view_profile/".$profile_id)?>">Profile</a></li>
					<li>Status Update</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?=$profile_name?></h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li><a href="<?=site_url("profile/view_profile/".$profile_id)?>"><span><em>Info</em></span></a></li>
							<li class="active"><a href="<?=site_url("profile/status_updates/".$profile_id)?>"><span><em>Status Updates</em></span></a></li>
							<li><a href="<?=site_url("profile/files/".$profile_id)?>"><span><em>Files</em></span></a></li>
							<li><a href="<?=site_url("profile/items/".$profile_id)?>"><span><em>Items</em></span></a></li>
						</ul>
					</div>
				</div>
				<!-- profile details -->
				<div class="container">
					<div class="posts-box">
						<ul id="show-list" class="posts-list hfeed">
					 <?php foreach($messagelist as $msg):?>
							<li class="type2 hentry">
									<div class="frame">
										<span class="visual">
											<img src="<?=base_url();?>images/user-images/105x101/<?=$msg['profile_pic']?>" alt="<?=$msg['first_name']." ".$msg['last_name']?>" width="53" height="53" />
										</span>
										<div class="text">
											<strong class="entry-title"><a href="<?=site_url('profile/view_profile/' . $msg['user_id'])?>"><?=ucfirst(strtolower($msg['first_name']))." ".ucfirst(strtolower($msg['last_name']))?></a><em><?=$msg['designation'].", ".$msg['concept']?></em></strong>
											<p><?=$msg['message']?></p>
											<span><?=$msg['message_time']?></span>
											<ul class="actions">
												<li><a href="mailto:<?=$msg['email']?>">Email</a></li>
												<?php if($this->session->userdata('userid') == $msg['user_id']):?>
												<li><a class="msg-delete" id="<?=$msg['message_id']?>" href="#">Delete</a></li>
												<?php elseif($this->session->userdata('userid') != $msg['user_id']): ?>
												<li><a rel="pop_win" class="msg-report" href="<?=site_url("profile/report_status_update/".$msg['message_id'])?>">Report</a></li>
												<li><a class="<?=($msg['following_user_id'])? "msg-unfollow":"msg-follow"?>" rel="<?=$msg['user_id']?>" href="<?=site_url("/status_update")?>"><?=($msg['following_user_id'])? "Unfollow":"Follow"?></a></li>
												<?php endif;?>
											</ul>
										</div>
									</div>
							</li>
						<?php endforeach;?>
					 </ul>
					 <?php if($show_load_more):?>
					 <a href="<?=site_url("profile/pagination")?>" rel="<?="user_".$profile_id?>" id="<?=$this->session->userdata('cfgpagelimit')?>" class="more-stories">View More</a>
					<?php endif;?>
					</div>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- sidebar box, user box -->
				<?php $this->load->view("includes/useful_link");?>
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/latest_update");?>
				<!-- sidebar box, My Interest Groups -->
				<?php $this->load->view("includes/my_interest_groups");?>
				<!-- sidebar box, Newcomers -->
				<?php $this->load->view("includes/newcomers");?>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=site_url()."/people/"?>" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="<?=site_url()."/status_update/"?>" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=site_url()."/interest_groups/"?>" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>