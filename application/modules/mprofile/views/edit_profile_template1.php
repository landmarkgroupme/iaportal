<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.core.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.widget.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.position.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.autocomplete.min.js"></script>
	
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/profile/edit_profile.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper" class="edit-profile">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url("people")?>">People</a></li>
					<li><a href="<?=site_url("people")?>">Company Directory</a></li>
					<li><a href="<?=site_url("profile/myprofile")?>"><?=$name?></a></li>
					<li>Edit Profile</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>Edit Profile</h2>
				</div>
				<!-- Edit profile details -->
        <?php if($this->session->flashdata('profile_status')):?>
				<div class="msg-ok">
	        <?=$this->session->flashdata('profile_status');?>
        </div>
        <?php endif;?>
				<div class="container">
					<form method="post" action="" id="frm-edit-profile">
						<div class="form-fields">
							<!-- Locked contact details -->
							<div class="contact-detail-box">
								<div class="info-strip"><img src="<?=base_url()?>images/ico-49.jpg" alt="locked" width="16" height="15"/> <span>If you want to change this information, please contact your HR team.</span></div>
								<div class="contact-details">
									<div class="contact-name"><?=$name?></div>
									<div class="contact-desg"><strong><?=$designation?></strong>, <?=$concept?></div>
									<div class="contact-info">
										<div class="contact-attr">Ext: </div>
										<div><input type="text" name="extension" value="<?=$extension?>" /></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">First Name:</div>
										<div><input type="text" name="first_name" value="<?=$first_name?>" /></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">Middle Name:</div>
										<div><input type="text" name="middle_name" value="<?=$middle_name?>" /></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">Last Name:</div>
										<div><input type="text" name="last_name" value="<?=$last_name?>" /></div>
									</div>
									
									<div class="contact-info">
										<div class="contact-attr">Title:</div>
										<div>
											<select name="concept">
												<option value="MR." <?php echo $title == 'MR.' ? 'selected="selected"' : ''; ?>>MR.</option>
												<option value="MRS." <?php echo $title == 'MRS.' ? 'selected="selected"' : ''; ?>>MRS.</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">Phone:</div>
										<div><input type="text" name="phone" value="<?=$phone?>" /></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">Mobile:</div>
										<div><input type="text" name="mobile" value="<?=$mobile?>" /></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">Designation:</div>
										<div><select name="designation">
										<?php foreach($designations as $des) {
											?><option value="<?php echo $des->id; ?>" <?php echo $des->name == $designation ? 'selected="selected"' : ''; ?>><?php echo $des->name; ?></option><?php
										}?>
										</select></div>
									</div>
									<div class="contact-info">
										<div class="contact-attr">Concept:</div>
										<div><select name="concept">
										<?php foreach($concepts as $conc) {
											?><option value="<?php echo $conc->id; ?>" <?php echo $conc->name == $concept ? 'selected="selected"' : ''; ?>><?php echo $conc->name; ?></option><?php
										}?>
										</select></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">Email:</div>
										<div><input type="text" name="email" value="<?=$email?>" /></div>
									</div>
									<div class="clear"></div>
									<div class="contact-info">
										<div class="contact-attr">HRMS ID:</div>
										<div><input type="text" name="hrms_id" value="<?=$hrms_id?>" /></div>
									</div>
								</div>
							</div>
							<!-- Skills and Experience -->
							<div class="field-box">
								<div class="field-label"><a name="skills"></a>Skill &amp; Experiences</div>
								<textarea name="txtar-skill-exp" id="txtar-skill-exp" cols="52" rows="6"><?=$user_skills?></textarea>
							</div>
							<!-- Interests -->
							<div id="interest-groups" class="field-box">
								<div class="field-label">Interests</div>
								<div class="error" id="error-interest-groups" style="margin-top:5px; margin-bottom:5px;"></div>
								<textarea name="txtar-interest" id="txtar-interest" cols="52" rows="6"><?=$user_interest_groups?></textarea>
								<div>Restrict your interest group to 15 characters each (including spaces)</div>
							</div>
							<!-- About Me -->
							<div class="field-box">
								<div class="field-label">About Me</div>
								<textarea name="txtar-aboutme" id="txtar-aboutme" cols="52" rows="6"><?=$user_about?></textarea>
							</div>
						</div>
						<div class="submit-div"><input type="image" src="<?=base_url()?>images/btn-save.jpg" name="save changes" /> <a href="#">Cancel</a></div>
					</form>
				</div>
			</div>
			
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=site_url("people")?>" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="<?=site_url("status_update")?>" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=site_url("interest_groups")?>" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>