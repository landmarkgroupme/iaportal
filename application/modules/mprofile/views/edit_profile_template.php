<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/mprofile/all.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?=base_url();?>js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
    <style type="text/css">
    .thumb-span {background:#860e9c;padding:2px 4px;text-transform:uppercase;color:#fff;-moz-border-radius:4px; border-radius: 4px;font-weight:bold;}
    </style>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'employees';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Edit Employee</h1>
        </div>
        <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
            <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
            <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
        <div class="clear"></div>


        <div>
          <form action="<?php echo site_url('people/search')?>" class="search-form" name="searchform" id="quick_search_frm" method="post">
            <fieldset>
              <input accesskey="4" name="term" autocomplete="off" tabindex="23" type="text" class="text" id="txt_general_search" value="Search by name.."/>
              <input name="search" type="hidden" value="search"/>
              <input name="search_from" type="hidden" value="" id="search_from"/>
            </fieldset>
            <div id ="search_element">
              <div id='search_listing'>
                <div id="src-loader" ><img src="<?php echo base_url()?>images/loader.gif" /></div>
                <ul id='src_user_list'>
                  
                </ul>
                <p class='src_more_res'><a id="submit_src" href="#"></a></p>
              </div>
              <div id='search_listing_f'></div>
            </div>
            <img src="<?php echo base_url()?>images/gnr_src_bg-f.jpg" style="display: none;" alt="bg-preload" />
            <img src="<?php echo base_url()?>images/user_list_bg.png" style="display: none;" alt="bg-preload" />
            <img src="<?php echo base_url()?>images/user_list_f_bg.png" style="display: none;" alt="bg-preload" />
          </form>
        </div>
        
        <div class="clear"></div>
        <form method="post" action="" id="frm-edit-profile">
            <div class="form-fields">
              <!-- Locked contact details -->
              <div class="contact-detail-box">
                <div class="contact-details">
                  <div class="clear"></div>
                  
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Ext: </div>
                    <div><input type="text" name="extension" value="<?=$extension?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">First Name:</div>
                    <div><input type="text" name="first_name" value="<?=$first_name?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Middle Name:</div>
                    <div><input type="text" name="middle_name" value="<?=$middle_name?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Last Name:</div>
                    <div><input type="text" name="last_name" value="<?=$last_name?>" /></div>
                  </div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Title:</div>
                    <div>
                      <select name="concept">
                        <option value="MR." <?php echo $title == 'MR.' ? 'selected="selected"' : ''; ?>>MR.</option>
                        <option value="MRS." <?php echo $title == 'MRS.' ? 'selected="selected"' : ''; ?>>MRS.</option>
                        <option value="MS." <?php echo $title == 'MS.' ? 'selected="selected"' : ''; ?>>MS.</option>
                        <option value="MISS." <?php echo $title == 'MISS.' ? 'selected="selected"' : ''; ?>>MISS.</option>
                        <option value="DR." <?php echo $title == 'DR.' ? 'selected="selected"' : ''; ?>>DR.</option>
                        MRS
                      </select>
                    </div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Phone:</div>
                    <div><input type="text" name="phone" value="<?=$phone?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Mobile:</div>
                    <div><input type="text" name="mobile" value="<?=$mobile?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Designation:</div>
                    <div><select name="designation">
                    <?php foreach($designations as $des) {
                      ?><option value="<?php echo $des->id; ?>" <?php echo $des->name == $designation ? 'selected="selected"' : ''; ?>><?php echo $des->name; ?></option><?php
                    }?>
                    </select></div>
                  </div>
                  <div class="contact-info">
                    <div class="contact-attr">Concept:</div>
                    <div><select name="concept">
                    <?php foreach($concepts as $conc) {
                      ?><option value="<?php echo $conc->id; ?>" <?php echo $conc->name == $concept ? 'selected="selected"' : ''; ?>><?php echo $conc->name; ?></option><?php
                    }?>
                    </select></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Email:</div>
                    <div><input type="text" name="email" value="<?=$email?>" readonly/></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">HRMS ID:</div>
                    <div><input type="text" name="hrms_id" value="<?=$hrms_id?>" /></div>
                  </div>
                </div>
              </div>
              <!-- Skills and Experience -->
              <div class="field-box">
                <div class="field-label"><a name="skills"></a>Skill &amp; Experiences</div>
                <textarea name="txtar-skill-exp" id="txtar-skill-exp" cols="52" rows="6"><?=$user_skills?></textarea>
              </div>
              <!-- Interests -->
              <div id="interest-groups" class="field-box">
                <div class="field-label">Interests</div>
                <div class="error" id="error-interest-groups" style="margin-top:5px; margin-bottom:5px;"></div>
                <textarea name="txtar-interest" id="txtar-interest" cols="52" rows="6"><?=$user_interest_groups?></textarea>
                <div>Restrict your interest group to 15 characters each (including spaces)</div>
              </div>
              <!-- About Me -->
              <div class="field-box">
                <div class="field-label">About Me</div>
                <textarea name="txtar-aboutme" id="txtar-aboutme" cols="52" rows="6"><?=$user_about?></textarea>
              </div>
            </div>
            <div class="submit-div"><input type="image" src="<?=base_url()?>images/btn-save.jpg" name="save changes" /> <a href="#">Cancel</a></div>
          </form>
      
          <div style="display:none">
              <div id="previewBox" style="padding:10px; background:#fff;">
                  <p><strong>This content comes from a hidden element on this page.</strong></p>
                  <p>The inline option preserves bound JavaScript events and changes, and it puts the content back where it came from when it is closed.<br /></p>
              </div>
          </div>
    </div>
</div>
<script type="text/javascript">
//General Search Field
var base_url = "/";
$(document).ready(function(){
  /* General Files */
  //loading base Url of the site
  //base_url = $("#base_url").val()+"index.php/";
});
function general_search_form(){
 
  //search when clikc on link
  $('#submit_src').live('click',function(){
    $('#quick_search_frm').submit();
  })

  //Hover background change
  $('#src_user_list li div').live('mouseover',function(){
    $('#src_user_list li div').removeClass('hover');
    $(this).addClass('hover')
    return false;
  })
  //Hide search list on main mouse out
  $('#src_user_list').blur(function(){
    $('#search_element').hide();
  })

  //User click
  $('#src_user_list li').live('click',function(){
    $('#search_element').show();
    var user_profile = this.id
    window.location = user_profile;
    return false;
  })
  //Type event
  var tmp_cnt = 0;// arrow key navigation count
  $('#txt_general_search').keyup(function(e){

    if(e.keyCode == 27){
      $('#search_element').hide();
      return false;
    }
   
    var src_term =  $('#txt_general_search').val();
    var src_from =  '';
    if($('#search_from')) 
    {
      src_from = $('#search_from').val(); 
    }
    if(src_term.length < 3){
      return false;
    }
   

    var size_li = $("#src_user_list li").size();

    if(e.keyCode == 38){
      if(tmp_cnt <=0){
        tmp_cnt =  size_li;
      }else{
        tmp_cnt = tmp_cnt-1
      }
      //$('#src_user_list li div').css('background-color','inherit')
      //$('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
      $('#src_user_list li div').removeClass('hover')
      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
      return false;
    }else if(e.keyCode == 40){
      if(size_li == tmp_cnt){
        tmp_cnt = 0;
      }else{
        tmp_cnt = tmp_cnt+1
      }
      //$('#src_user_list li div').css('background-color','inherit')
      // $('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
      $('#src_user_list li div').removeClass('hover')
      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
      return false;
    }



    $('#search_element').show();
    $('#src-loader').show();

    $.ajax({
      url: base_url+"people/quick_search_sphinx",
      data: {
        "src_term":src_term,
        'src_from': src_from
      },
      async: true,
      dataType: "json",
      type: "POST",
      success: function(data){
        $('#src-loader').hide();
        if(data['show']){
          //alert(e.keyCode);
          $('#src_user_list').html(data['result']);
          $('#submit_src').html(data['result_count']);
          $('#search_element').show();
        //alert($('#src_user_list li:first').html());
        }else{
          $('#src_user_list').html(data['result']);
          $('#submit_src').html('');
          $('#search_element').show();
        }
      }
    });

    return false;
  })


  //Focus
  $("#txt_general_search").focus(function(){
    var src_term =  $("#txt_general_search").val();
    if( $.trim(src_term) == 'Search by name..'){
      $("#txt_general_search").val('');
    }
    $(this).css('color','#000000');
  })
  //Blur
  $("#txt_general_search").blur(function(){
    var src_term =  $("#txt_general_search").val();
    if( $.trim(src_term) == ''){
      $("#txt_general_search").val('Search by name..');
    }
    $(this).css('color','#747474');
  })
}
general_search_form();
</script>
</body>
</html>
