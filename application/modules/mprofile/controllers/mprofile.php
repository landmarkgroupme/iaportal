<?php

Class Mprofile extends MX_Controller {
//Constructor 
  function Mprofile() {
    parent::__construct();
    $this->load->model('m_profile');
    $this->load->model('includes/general_model'); //General Module
    $this->load->model('manage/manage_model');
    $this->load->model('backend/moderate_model');
    /*
      if (! $this->session->userdata('is_logged_in')) {
      $this->session->set_userdata('requested_uri', $this->uri->uri_string());
      redirect('auth');
      }
     */
    $this->load->helper('auth');
    auth();
  }

//image resizing and croping


  /* Private function for Newcommer */
  function _newcommer_list() {
    $newcommer_show_limit = 3;
    $user_id = $this->session->userdata('userid');
    $db_newcommer = $this->general_model->gn_newcomers($user_id, $newcommer_show_limit); //General Model function for New Commers
    for ($i = 0; $i < count($db_newcommer); $i++) {
      $contact_user = $db_newcommer[$i]['contact_user'];
      $profile_pic = base_url() . "images/user-images/105x101/" . $db_newcommer[$i]['profile_pic'];
      if (!$db_newcommer[$i]['profile_pic']) {
        $db_newcommer[$i]['profile_pic'] = "default.jpg";
      } else if (!@fopen($profile_pic, "r")) {
        $db_newcommer[$i]['profile_pic'] = "default.jpg";
      }
      $id = $db_newcommer[$i]['id'];
      if ($contact_user) {
        $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="remove-contact">Remove from my contacts</a>';
      } else {
        $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="add-contact">Add to My Contacts</a>';
      }
    }
    return $db_newcommer;
  }

  /* Private function for Interest Groups */

  function _interest_group_list() {
    $show_limit = 20;
    $user_id = $this->session->userdata('userid');
    $db_interest_groups = $this->general_model->gn_intrest_groups($user_id, $show_limit); //General Model function for Interest Groups
    if (!count($db_interest_groups)) {
      $db_interest_groups = array();
    }
    return $db_interest_groups;
  }

  /* Private function for My Reminders */

  function _my_reminders() {
    $user_id = $this->session->userdata('userid');
    $time = time();
    $this->load->model('market_place/market_place_model');
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($user_id);
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($user_id);
    $db_event_reminders = $this->general_model->gn_my_event_reminders($user_id, $time);
    $my_reminders['count_item_sale'] = count($db_item_sale);
    $my_reminders['count_item_requested'] = count($db_item_requested);
    $my_reminders['count_event_reminders'] = count($db_event_reminders);
    return $my_reminders;
  }
//Default page for profile, redirect to the the logged in users profile
  function index() {
    redirect("profile/myprofile");
  }

//profile Progress Bar
  function profile_progress_bar() {
    $progress = 5;

    $user_id = $this->session->userdata('userid');
    //Profile Details
    $user_data = $this->m_profile->get_profile($user_id, $user_id);
    //Intrest Group 
    $db_interest_groups = $this->m_profile->get_users_interest_groups($user_id);

    $user_skills = $user_data['skills'];
    $user_about = $user_data['about'];
    $profile_pic = $user_data['profile_pic'];

    if (trim($user_skills)) {
      $progress += 25;
    }
    if (trim($user_about)) {
      $progress += 25;
    }

    if ($profile_pic != "default.jpg" && $profile_pic != "default-f.jpg") {
      $progress += 20;
    }

    if (count($db_interest_groups)) {
      $progress += 25;
    }

    $result = '{"progress": "' . $progress . '"}';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }
//Profile View page this page has to be called with the user id else it will redirect to myprofile
  function view_profile() {
    $user_email = $this->session->userdata('useremail');
    $user_id = $this->session->userdata('userid');

    $id = (int) $this->uri->segment(3);
    if (!$id) {
      redirect('profile');
    }
    $profile_details = $this->m_profile->get_profile($id, $user_id);
    if (!count($profile_details)) {
      redirect('profile');
    }

    $db_interest_groups = $this->m_profile->get_users_interest_groups($id);
    if (count($db_interest_groups)) {
      for ($i = 0; $i < count($db_interest_groups); $i++) {
        $user_groups[] = '<a href="' . site_url("interest_groups/group_page/" . $db_interest_groups[$i]['id']) . '">' . $db_interest_groups[$i]['name'] . '</a>';
      }
    } else {
      $user_groups = array();
    }
    $data = array(
        'profile' => $profile_details,
        'user_interest_groups' => @implode(", ", $user_groups),
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );

    $this->load->view('v_profile', $data);
  }
//This is the logged in users profile page
  function myprofile() {
    $id = $this->session->userdata('userid');
    $user_email = $this->session->userdata('useremail');
    if (!$id) {
      redirect('people');
    }

    $db_interest_groups = $this->m_profile->get_users_interest_groups($id);
    if (count($db_interest_groups)) {
      for ($i = 0; $i < count($db_interest_groups); $i++) {
        $user_groups[] = '<a href="' . site_url("interest_groups/group_page/" . $db_interest_groups[$i]['id']) . '">' . $db_interest_groups[$i]['name'] . '</a>';
      }
    } else {
      $user_groups = array();
    }

    $data = array(
        'profile' => $this->m_profile->get_profile($id, $id),
        'user_interest_groups' => @implode(", ", $user_groups),
        'myprofile' => $this->m_profile->get_profile_by_email($user_email),
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );
    $this->load->view('v_myprofile', $data);
  }
//Editing profile details
  function edit_profile() {
    $id = (int) $this->uri->segment(3);
    
    if ($id) {
      //$this->load->view("edit_profile_template", $data);
      return;
    }
    
    if(isset($_POST['txtar-skill-exp'])) {
      /*echo 'asdfas';
      die();*/
      $user_details = array();

      $user_details['skills'] = $this->input->post('txtar-skill-exp');
      $user_details['about'] = $this->input->post('txtar-aboutme');
      $user_details['phone'] = $this->input->post('phone');
      $user_details['mobile'] = $this->input->post('mobile');
      $user_details['extension'] = $this->input->post('extension');
      $user_details['first_name'] = $this->input->post('first_name');
      $user_details['last_name'] = $this->input->post('last_name');
      $user_details['middle_name'] = $this->input->post('middle_name');
      $user_details['hrms_id'] = $this->input->post('hrms_id');
      $user_details['email'] = $this->input->post('email');
      $user_details['title'] = $this->input->post('title');
      $user_details['designation_id'] = $this->input->post('designation');
      $user_details['concept_id'] = $this->input->post('concept');


      $interest_groups = $this->input->post('txtar-interest');

      //Interest Groups Check
      $interest_groups = @explode(",", $interest_groups);

      
      //Update User
      //$this->m_profile->update_user_info($user_details, $id);
      $this->session->set_flashdata('profile_status', 'Your profile has been edited successfuly.');
    }


    $user_id = $this->session->userdata("userid");
    $user_data = $this->m_profile->get_profile($id, $user_id);
    
    $data['designations'] = $this->m_profile->get_designations();

    $data['concepts'] = $this->m_profile->get_concepts(); 
    
    if (!$user_id) {
      redirect(site_url());
    }
    
    $data['name'] = ucfirst(strtolower($user_data['first_name'])) . " " . ucfirst(strtolower($user_data['last_name']));
    $data['designation'] = $user_data['designation'];
    $data['concept'] = $user_data['concept'];
    $data['extension'] = $user_data['extension'];
    $data['phone'] = $user_data['phone'];
    $data['mobile'] = $user_data['mobile'];
    $data['email'] = $user_data['email'];
    $data['title'] = $user_data['title'];
    $data['first_name'] = $user_data['first_name'];
    $data['last_name'] = $user_data['last_name'];
    $data['middle_name'] = $user_data['middle_name'];
    $data['hrms_id'] = $user_data['hrms_id'];
    $data['user_name'] = $user_data['reply_username'];

    $data['user_skills'] = $user_data['skills'];
    $data['user_about'] = $user_data['about'];

    $db_interest_groups = $this->m_profile->get_users_interest_groups($user_id);
    //echo "<xmp>".print_r($db_interest_groups,1)."</xmp>";exit;
    if (count($db_interest_groups)) {
      for ($i = 0; $i < count($db_interest_groups); $i++)
        $user_groups[] = $db_interest_groups[$i]['name'];
    } else {
      $user_groups = array();
    }
    //$db_interest_groups =

    $data['user_interest_groups'] = @implode(", ", $user_groups);
    $this->load->view("edit_profile_template", $data);
  }
//Status update tab for logged in user
  function status_updates() {
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');
    $profile_id = (int) $this->uri->segment(3);
    $userId = $this->session->userdata("userid");
    $user_email = $this->session->userdata("useremail");
    $db_myprofile_details = $this->m_profile->get_profile_by_email($user_email);

    $profile_details = $this->m_profile->get_profile($profile_id, $userId);
    $profile_name = ucfirst(strtolower($profile_details['first_name'])) . " " . ucfirst(strtolower($profile_details['last_name']));

    $data = array(
        'profile_id' => $profile_id,
        'profile_name' => $profile_name,
        'myprofile' => $db_myprofile_details,
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );

    $pagelimit = 0;
    $data['messagelist'] = $this->m_profile->su_show_my_updates($pagelimit, $userId, $profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $message = $data['messagelist'][$i]['message'];
      $formated_time = time_ago($message_time); //calling time ago helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }
    $db_msg_count = count($data['messagelist']);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }
    $this->load->view('status_update_template', $data);
  }

//Load More pagination
  function pagination() {
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');

    $offset = $this->input->post("pagesize");
    $userId = $this->session->userdata("userid");
    $profileId = $this->input->post("profile_id");
    $data['messagelist'] = $this->m_profile->su_show_my_updates($offset, $userId, $profileId);
    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $formated_time = time_ago($message_time); //calling time ago helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }
    $this->load->view('show_status_message', $data);
  }

  ////Files tab for logged in user
  function files() {
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');
    $profile_id = (int) $this->uri->segment(3);
    $userId = $this->session->userdata("userid");
    $user_email = $this->session->userdata("useremail");
    $db_myprofile_details = $this->m_profile->get_profile_by_email($user_email);

    $profile_details = $this->m_profile->get_profile($profile_id, $userId);
    $profile_name = ucfirst(strtolower($profile_details['first_name'])) . " " . ucfirst(strtolower($profile_details['last_name']));

    $data = array(
        'profile_id' => $profile_id,
        'myprofile' => $db_myprofile_details,
        'profile_name' => $profile_name,
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );

    $total_files = $this->m_profile->files_get_files_count($profile_id);
    $total_files = $total_files['files_count'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/profile/files/' . $profile_id;
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['uri_segment'] = 4;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);

    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->m_profile->files_get_all_files($profile_id, $config['per_page'], $this->uri->segment(4, 0));

    //Data formating
    if (is_array($db_all_files)) {
      for ($i = 0; $i < (count($db_all_files)); $i++) {

        $uploaded_on_time = $db_all_files[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $db_all_files[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $db_all_files[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }

        //Tags
        $db_file_tags = $this->m_profile->files_get_tags_for_file($db_all_files[$i]['file_id']);
        $tag_arr = array();
        if (is_array($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            $tag_arr[] = '<a class="grey-shade-1" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
          }
          $file_tag = @implode(", ", $tag_arr);
        }

        $db_all_files[$i]['file_tags'] = $file_tag;
        $db_all_files[$i]['file_ext'] = $ext;
        $db_all_files[$i]['file_icon'] = $icon_path;
      }
    }
    $data['files_list'] = $db_all_files;
    $this->load->view('files_template', $data);
  }

  ////Your posted marketplace items tab for logged in user
  function items() {
    $this->load->helper('time_ago');
    $this->load->model("market_place/market_place_model");
    $profile_id = (int) $this->uri->segment(3);
    $user_email = $this->session->userdata("useremail");
    $userId = $this->session->userdata("userid");
    $db_myprofile_details = $this->m_profile->get_profile_by_email($user_email);

    $profile_details = $this->m_profile->get_profile($profile_id, $userId);
    $profile_name = ucfirst(strtolower($profile_details['first_name'])) . " " . ucfirst(strtolower($profile_details['last_name'])) ;

    $data = array(
        'profile_id' => $profile_id,
        'myprofile' => $db_myprofile_details,
        'profile_name' => $profile_name,
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );
    //Items For Sales
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($db_item_sale); $i++) {
      $added_on_time = $db_item_sale[$i]['added_on'];
      $formated_time = time_ago($added_on_time); //calling time ago helper
      $db_item_sale[$i]['added_on'] = $formated_time;
      $image_path = base_url() . "images/marketplace/" . $db_item_sale[$i]['item_image'];
      if ($db_item_sale[$i]['item_image']) {
        if (@fopen($image_path, 'r')) {
          $db_item_sale[$i]['item_image'] = $image_path;
        } else {
          $db_item_sale[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
        }
      } else {
        $db_item_sale[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
      }
    }
    $data['your_item_sale_limit'] = 1;
    $data['your_item_sale_offset'] = count($db_item_sale);
    $data['your_item_sale_total_count'] = count($db_item_sale);
    $data['your_items_sale'] = $db_item_sale;
    //Requested Items list
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($db_item_requested); $i++) {
      $added_on_time = $db_item_requested[$i]['added_on'];
      $formated_time = time_ago($added_on_time); //calling time ago helper
      $db_item_requested[$i]['added_on'] = $formated_time;
      $image_path = base_url() . "images/marketplace/" . $db_item_requested[$i]['item_image'];
      if ($db_item_requested[$i]['item_image']) {
        if (@fopen($image_path, 'r')) {
          $db_item_requested[$i]['item_image'] = $image_path;
        } else {
          $db_item_requested[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
        }
      } else {
        $db_item_requested[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
      }
    }
    $data['your_item_requested_limit'] = 1;
    $data['your_item_requested_offset'] = count($db_item_requested);
    $data['your_item_requested_total_count'] = count($db_item_requested);
    $data['your_items_requested'] = $db_item_requested;
    //Freebies List
    $db_item_freebies = $this->market_place_model->mp_get_all_your_items_freebies($profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($db_item_freebies); $i++) {
      $added_on_time = $db_item_freebies[$i]['added_on'];
      $formated_time = time_ago($added_on_time); //calling time ago helper
      $db_item_freebies[$i]['added_on'] = $formated_time;
      $image_path = base_url() . "images/marketplace/" . $db_item_freebies[$i]['item_image'];
      if ($db_item_freebies[$i]['item_image']) {
        if (@fopen($image_path, 'r')) {
          $db_item_freebies[$i]['item_image'] = $image_path;
        } else {
          $db_item_freebies[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
        }
      } else {
        $db_item_freebies[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
      }
    }
    $data['your_item_freebies_limit'] = 1;
    $data['your_item_freebies_offset'] = count($db_item_freebies);
    $data['your_item_freebies_total_count'] = count($db_item_freebies);
    $data['your_items_freebies'] = $db_item_freebies;
    $this->load->view("items_template", $data);
  }

  //Change Password
  function change_password() {
    $this->load->view("pop_windows/change_password");
  }

  //Change Photo
  function change_photo() {
    $user_email = $this->session->userdata("useremail");
    $db_data = $this->m_profile->get_profile_by_email($user_email);
    $data['profile_pic'] = $db_data['profile_pic'];
    $this->load->view("pop_windows/change_photo", $data);
  }

  //Add Fav. Links
  function fav_links() {
    $this->load->view("pop_windows/fav_links");
  }

  //Report File Screen
  function report_file() {
    $data['file_id'] = (int) $this->uri->segment(3);
    $this->load->view("pop_windows/report_file", $data);
  }

  //Report Markst Screen
  function report_market() {
    $data['item_id'] = (int) $this->uri->segment(3);
    $this->load->view("pop_windows/report_market", $data);
  }

  //Report Status update Screen
  function report_status_update() {
    $data['message_id'] = (int) $this->uri->segment(3);
    $this->load->view("pop_windows/report_status_update", $data);
  }

  //Remove Fav. Link
  function fav_remove_link() {
    $user_id = $this->session->userdata("userid");
    $link_id = $this->input->post("link_id");
    $link_details = array(
        'status' => 2,
    );
    //Update Links details
    $row = $this->m_profile->update_fav_link($link_details, $user_id, $link_id);
    if ($row) {
      $status = 1;
    } else {
      $status = 0;
    }
    $result = '{"status": "' . $status . '"}';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Download Vcard of user
  function download_vcard() {
    $profile_id = (int) $this->uri->segment(3);
    $user_id = $this->session->userdata("userid");
    $user_data = $this->m_profile->get_profile($profile_id, $user_id);
    //echo "<xmp>".print_r($user_data,1)."</xmp>";exit;
    $v_card = "BEGIN:VCARD
							VERSION:3.0
							N:" . ucfirst(strtolower($user_data['last_name'])) . ";" . ucfirst(strtolower($user_data['middle_name'])) . ";" . ucfirst(strtolower($user_data['first_name'])) . "
							FN:" . ucfirst(strtolower($user_data['first_name'])) . " " . ucfirst(strtolower($user_data['last_name'])) . "
							ORG:" . $user_data['concept'] . "
							ROLE:" . $user_data['department'] . "
							TITLE:" . $user_data['designation'] . "
							TEL;TYPE=WORK,PHONE:" . $user_data['phone'] . "
							TEL;TYPE=WORK,cell:" . $user_data['mobile'] . "
							ADR;TYPE=WORK:;;;;" . $user_data['location'] . ";;" . $user_data['territory'] . "
							LABEL;TYPE=WORK:100 Waters Edge\nBaytown, LA 30314\nUnited States of America
							EMAIL;TYPE=PREF,INTERNET:" . $user_data['email'] . "
							REV:20080424T195243Z
							END:VCARD";
    $data['vcard_name'] = ucfirst(strtolower($user_data['first_name']));
    $data['vcard'] = $v_card;
    $this->load->view('v_vcard_print', $data);
  }

  //Interest Group Autocomplete
  function autocomplete_interest_groups() {
    $src_term = $this->input->post('term');
    $interest_groups = $this->m_profile->get_all_interest_groups($src_term);
    $json_result = array();
    foreach ($interest_groups as $val) {
      $json_result[] = '{"id": "' . $val['id'] . '", "label": "' . $val['name'] . '"}';
    }
    if (count($json_result)) {
      $result = '[ ' . @implode(",", $json_result) . ' ]';
    } else {
      $result = '[ {"id": "0", "label": "No Results"} ]';
    }
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

//Edit profile Submit	
  function submit_edit_profile() {
    $user_id = $this->session->userdata("userid");
    $user_skill = $this->input->post('txtar-skill-exp');
    $interest_groups = $this->input->post('txtar-interest');
    $about_me = $this->input->post('txtar-aboutme');
    $phone = $this->input->post('phone');
    $mobile = $this->input->post('mobile');
    $extension = $this->input-post('extension');
    //Interest Groups Check
    $interest_groups = @explode(",", $interest_groups);

    //creating update array for database
    $user_details = array(
        'skills' => $user_skill,
        'about' => $about_me
    );
    //Update User
    $this->m_profile->update_user_info($user_details, $user_id);
    $this->session->set_flashdata('profile_status', 'Your profile has been edited successfuly.');
    if (is_array($interest_groups)) {
      $this->m_profile->remove_user_interest_groups($user_id);

      foreach ($interest_groups as $groups) {
        $group = trim($groups);
        if (strlen($group) <= 15) {
          $group_id = $this->m_profile->get_interest_groups_id($group);
          if (!$group_id) {
            $group_details = array(
                'id' => NULL,
                'name' => $group,
                'created_by' => $user_id,
                'created_on' => time(),
                'group_status' => 1
            );
            $group_id = $this->m_profile->insert_interest_groups($group_details);
          }

          $user_group_details = array(
              'intrest_groups_id' => $group_id,
              'user_id' => $user_id,
              'joined_on' => time()
          );
          //Insert user Intrest Group
          $this->m_profile->insert_user_interest_groups($user_group_details);
        }
      }
    }
    $this->session->set_flashdata('profile_status', 'Your profile has been edited successfuly.');
    redirect(site_url("profile/edit_profile"));
  }

  //Submit change Password
  function submit_change_password() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $password = $this->input->post("txtpassword");
    $cnf_password = $this->input->post("txtcnfpassword");
    if (!trim($password)) {
      $status = 1;
      $status_msg = "New password is required.";
    } elseif ($password != $cnf_password) {
      $status = 1;
      $status_msg = "Passwords do not match";
    } else {
      $status = 2;
    }
    if ($status == 2) {
      //creating update array for database
      $user_details = array(
          'password' => md5($password)
      );
      $pass_status = $this->m_profile->update_user_password($user_details, $user_id);
      if ($pass_status) {
        $status_msg = "Password Changed";
      }
    }
    $result = '{"status": "' . $status_msg . '"}';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit Change Profile Pic
  function submit_change_profile_pic() {
    //replacing the users file to avoid duplications
    $user_id = $this->session->userdata('userid');
    $user_email = $this->session->userdata('useremail');
    $db_user_details = $this->m_profile->get_profile_by_email($user_email);
    $profile_pic = $db_user_details['profile_pic'];
    //If default image
    if (($profile_pic == "default.jpg") || ($profile_pic == "default-f.jpg")) {
      $profile_pic = $user_id . ".jpg";
      $user_details = array("profile_pic" => $profile_pic);
      $this->m_profile->update_profile_pic($user_details, $user_id);
    }
    $status = 0;


    $img_path = $_FILES['file_new_pic']['tmp_name'];
    $img_thumb = './images/user-images/105x101/' . $profile_pic;
    $this->load->helper('image_resize');

    //$this->crop($img_path,$img_thumb);

    $config['upload_path'] = './images/user-images/tmp_images/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['overwrite'] = false;
    $config['max_width'] = '0';
    $config['max_height'] = '0';

    $this->load->library('upload', $config);
    if ($this->upload->do_upload("file_new_pic")) {
      $file_details = $this->upload->data();

      $resizeObj = new resize($file_details['full_path']);
      $resizeObj->resizeImage(105, 105, 'crop');
      $resizeObj->saveImage($img_thumb, 100);

      if (@fopen($img_thumb, "r")) {
        $status = 1;
        @unlink($file_details['full_path']);
      } else {
        $status = 0;
      }
    } else {
      $status = 0;
    }
    if ($status) {
      $status_msg = "Profile photo changed";
    } else {
      $status_msg = "Please try again";
    }
    $new_pic = base_url() . "images/user-images/105x101/" . $profile_pic;
    $result = '{"status": "' . $status_msg . '","pic":"' . $new_pic . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }
//profile pic optimising
  public function crop($img_path, $img_thumb) {
    //$img_path = 'uploads\capsamples.jpg';
    //$img_thumb = 'uploads\capsamples_thumb.jpg';

    $config['image_library'] = 'gd2';
    $config['source_image'] = $img_path;
    $config['create_thumb'] = TRUE;
    $config['maintain_ratio'] = FALSE;

    $img = imagecreatefromjpeg($img_path);
    $_width = imagesx($img);
    $_height = imagesy($img);

    $img_type = '';
    $thumb_size = 105;

    if ($_width > $_height) {
      // wide image
      $config['width'] = intval(($_width / $_height) * $thumb_size);
      if ($config['width'] % 2 != 0) {
        $config['width']++;
      }
      $config['height'] = $thumb_size;
      $img_type = 'wide';
    } else if ($_width < $_height) {
      // landscape image
      $config['width'] = $thumb_size;
      $config['height'] = intval(($_height / $_width) * $thumb_size);
      if ($config['height'] % 2 != 0) {
        $config['height']++;
      }
      $img_type = 'landscape';
    } else {
      // square image
      $config['width'] = $thumb_size;
      $config['height'] = $thumb_size;
      $img_type = 'square';
    }

    $this->load->library('image_lib');
    $this->image_lib->initialize($config);
    $this->image_lib->resize();

    // reconfigure the image lib for cropping
    $conf_new = array(
        'image_library' => 'gd2',
        'source_image' => $img_thumb,
        'create_thumb' => FALSE,
        'maintain_ratio' => FALSE,
        'width' => $thumb_size,
        'height' => $thumb_size
    );

    if ($img_type == 'wide') {
      $conf_new['x_axis'] = ($config['width'] - $thumb_size) / 2;
      $conf_new['y_axis'] = 0;
    } else if ($img_type == 'landscape') {
      $conf_new['x_axis'] = 0;
      $conf_new['y_axis'] = ($config['height'] - $thumb_size) / 2;
    } else {
      $conf_new['x_axis'] = 0;
      $conf_new['y_axis'] = 0;
    }

    $this->image_lib->initialize($conf_new);

    $this->image_lib->crop();
  }

  //Submit Add Link
  function submit_add_fav_link() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txturl = $this->input->post("txturl");
    $txturltitle = $this->input->post("txturltitle");

    if (!$txturl) {
      $status = 1;
      $status_msg = "Url is required";
    } else if (!$txturltitle) {
      $status = 1;
      $status_msg = "Url Title is required";
    } else {
      $status = 2;
    }

    $add_link = array(
        'id' => NULL,
        'link_name' => $txturltitle,
        'link' => $txturl,
        'user_id' => $user_id,
        'status' => 1
    );
    $row = $this->m_profile->insert_fav_link($add_link);
    if ($row) {
      $status_msg = "Url has been added";
      $link = "<li><a target='_blank' href='" . $txturl . "'>" . $txturltitle . "</a></li>";
    } else {
      $status_msg = "Please try again";
      $link = "";
    }
    $result = '{"status": "' . $status_msg . '","link":"' . $link . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit report files
  function submit_report_files() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txtfile_id = $this->input->post("file_id");
    $comments = $this->input->post("comments");

    if (!$txtfile_id) {
      $status = 1;
      $status_msg = "File is required";
    } else if (!$comments) {
      $status = 1;
      $status_msg = "Comments Required";
    } else {
      $status = 2;
    }

    $update_file_status = array(
        "files_status" => 2
    );
    $row_file_status = $this->m_profile->update_file_status($update_file_status, $txtfile_id);

    if ($row_file_status) {
      $report_file = array(
          'id' => NULL,
          'file_id' => $txtfile_id,
          'comment' => $comments,
          'user_id' => $user_id,
          'status_id' => 1,
          'report_time' => time()
      );
      $row = $this->m_profile->insert_file_report($report_file);
      if ($row) {
        $status_msg = "This file has been reported to Admin";
      } else {
        $status_msg = "Please try again";
      }
    } else {
      $status_msg = "Please try again";
    }
    $result = '{"status": "' . $status_msg . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit report market item
  function submit_report_market() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txtfile_id = $this->input->post("item_id");
    $comments = $this->input->post("comments");

    if (!$txtfile_id) {
      $status = 1;
      $status_msg = "File is required";
    } else if (!$comments) {
      $status = 1;
      $status_msg = "Comments Required";
    } else {
      $status = 2;
    }

    $update_item_status = array(
        "items_status" => 2
    );
    $row_item_status = $this->m_profile->update_item_status($update_item_status, $txtfile_id);

    if ($row_item_status) {
      $report_item = array(
          'id' => NULL,
          'market_id' => $txtfile_id,
          'comment' => $comments,
          'user_id' => $user_id,
          'status_id' => 1,
          'report_time' => time()
      );
      $row = $this->m_profile->insert_market_report($report_item);
      if ($row) {
        $status_msg = "This item has been reported to Admin";
      } else {
        $status_msg = "Please try again";
      }
    } else {
      $status_msg = "Please try again";
    }
    $result = '{"status": "' . $status_msg . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit report files
  function submit_report_status_update() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txtmessage_id = $this->input->post("message_id");
    $comments = $this->input->post("comments");

    if (!$txtmessage_id) {
      $status = 1;
      $status_msg = "Message is required";
    } else if (!$comments) {
      $status = 1;
      $status_msg = "Comments Required";
    } else {
      $status = 2;
    }

    $update_message_status = array(
        "status" => 2
    );
    $row_message_status = $this->m_profile->update_message_status($update_message_status, $txtmessage_id);

    if ($row_message_status) {
      $report_status_update = array(
          'id' => NULL,
          'users_status_update_id' => $txtmessage_id,
          'comment' => $comments,
          'user_id' => $user_id,
          'status_id' => 1,
          'report_time' => time()
      );
      $row = $this->m_profile->insert_message_report($report_status_update);
      if ($row) {
        $status_msg = "This message has been reported to Admin";
      } else {
        $status_msg = "Please try again";
      }
    } else {
      $status_msg = "Please try again";
    }
    $result = '{"status": "' . $status_msg . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

}