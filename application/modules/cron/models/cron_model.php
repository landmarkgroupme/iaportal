<?php

Class Cron_model extends CI_Model {

  function Cron_model() {
    parent::__construct();
  }

  function email_queue_process() {
    $sql = 'SELECT
              a.id,
              a.email_campaign_id,
              a.to_email,
              a.to_name,
              b.email_title,
              b.email_subject,
              b.email_body,
              b.from_name,
              b.from_email
            FROM
              ci_email_queue a,
              ci_email_campaign b
            WHERE
              number_of_tries < 5 AND
              a.email_campaign_id = b.id and 
              ( b.event_type = "ANNOUNCEMENT")
            ORDER BY a.id ASC LIMIT 100';

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function clear_email_queue($id) {
    $sql = 'DELETE FROM `ci_email_queue` WHERE `id` = ?';
    $result = $this->db->query($sql, array($id));
    return $this->db->affected_rows();
  }

  function failed_email_log($id, $error_msg) {
    $sql = 'UPDATE ci_email_queue SET number_of_tries = number_of_tries+1,email_error = ? WHERE id = ?';
    $result = $this->db->query($sql, array($error_msg, $id));
    return $this->db->affected_rows();
  }

  function notification_log($data) {
    $str = $this->db->insert('ci_notify_email_log', $data);
    return $this->db->insert_id();
  }
  function get_pending_post(){
    $sql = 'SELECT
                  a.entry_id,
                  a.entry_created_on,
                  a.entry_text_more,
                  a.entry_title,
                  a.entry_status,
                  a.entry_excerpt,
                  b.category_basename,
                  d.first_name as name,
                  d.last_name as surname,
                  e.name as concept_name
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND
                  d.concept_id = e.id AND
                  a.entry_status = 3
                ORDER BY a.entry_id DESC';

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function  get_pending_events() {
    $sql = 'SELECT
                      a.id as event_id,
                      a.title as event_title,
                      a.description as event_description,
                      a.event_datetime as event_datetime,
                      a.added_on as added_on,
                      a.event_status,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      ci_event a,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      a.user_id = d.id AND
                      d.concept_id = e.id AND
                      a.event_status = 2 
                    ORDER BY event_datetime DESC ';
    $result = $this->db->query($sql);
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function dropbox_expire_sql($expiry_timestamp){
    $sql = 'UPDATE ci_files_dropbox SET dropbox_status = 2 WHERE uploaded_on < ?';
    $result = $this->db->query($sql, array($expiry_timestamp));
    return $this->db->affected_rows();
  }

  function event_notification_userlist($start_timespamp,$end_timespamp){
    $sql = 'SELECT
              a.id,
              a.title as event_title,
              a.event_datetime,
              b.email,
              b.title,
              b.first_name,
              b.last_name

            FROM
              ci_event a,
              ci_users b,
              ci_event_reminder c
            WHERE
              a.id = c.event_id AND
              c.user_id = b.id AND
              a.event_status = 1 AND
              c.status = 1 AND
              c.reminder_sent = 2 AND
              a.event_datetime >= ? AND
              a.event_datetime <= ?
            ORDER BY event_datetime DESC ';
    $result = $this->db->query($sql,array($start_timespamp,$end_timespamp));
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }
}

