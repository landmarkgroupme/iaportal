<?php
Class Users_model extends CI_Model {

	function __construct()
	{
	    parent::__construct();
	}
  
	function authenticate_user($email, $password) {
		$this->db->load();
		$this->db->select('first_name, last_name');
		$query = $this->db->get_where('site_user', array('email' => $email, 'password' => $password));
		if ($query->result() as $user)
		{
			$response = array('msg' => 'authenticated', 'first_name' => $user->first_name, 'last_name' => $user->last_name);
				return $response;
		}
		return array('msg' => 'invalid');
	}

}