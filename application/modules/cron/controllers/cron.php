<?php

class Cron extends MX_Controller {

  function Cron() {
    parent::__construct();
    $this->load->model('cron_model');
	$this->load->model('Job');
	$this->load->model('User');
	$this->load->helper('directory');
	$this->load->helper('prog_image_helper');
  }
  
  // Function added to convert existing marketplace images to make them progressive
 function marketplace_progressive_images()
 {
	$count = 0;
	$map = directory_map('./images/marketplace/');
	foreach($map as $image)
	{
		 $type = strtolower(substr(strrchr($image,"."),1));
		 $file = './images/marketplace/'.$image;
		 if(in_array($type, array('jpeg','jpg','gif','bmp','png')))
		 {
			progressive_image($file, $type);
			$count++;
		 }
		
	}
	
	echo $count.' Images are converted to progressive.';
 }
 
 function files_progressive_images()
 {
	$count = 0;
	$map = directory_map('./files_downloads/');
	foreach($map as $image)
	{
		 $type = strtolower(substr(strrchr($image,"."),1));
		 $file = './images/marketplace/'.$image;
		 if(in_array($type, array('jpeg','jpg','gif','bmp','png')))
		 {
			progressive_image($file, $type);
			$count++;
		 }
		
	}
	
	echo $count.' Images are converted to progressive.';
 }

 function importjobs()
  {
    $this->Job->importJobs();
  }
  
  function update_profile_pics()
  {
	   $count = 0;
	   $sortby = array(
		'id' => 'Asc'
		);
		$users = $this->User->getAll($limit = null, $sortby);
		foreach($users as $user)
		{
			if(!file_exists('./images/user-images/105x101/'.$user->profile_pic) || $user->profile_pic == '')
			{
				$pic = 'default/'.rand(1, 36).'.png';
				$data = array(
				'profile_pic' => $pic
				);
				$this->User->update($data, $user->id);
				$count++;
			}
			
			
		}
		if($count == 0)
		{
			print "<pre> All profile pictures exist on server.";
		}else{
		
		print "<pre> Default profile pictures added Successfully! For those user whose profile image not exist on server.<br/>";
		print $count." Records Updated";
		
		}
  }
  function minify_css_js()
  {
	$this->load->driver('minify');
	$cssfiles = array('media/css/default.css', 'media/css/common.css', 'media/css/home.css', 'media/css/jobs.css',   'media/css/jquery-impromptu.css', 'media/css/jquery.Jcrop.css', 'media/css/jquery.fancybox.css', 'media/css/token-input.css', 'media/css/token-input-facebook.css', 'media/css/jquery.qtip.min.css');
    $cssfile = $this->minify->combine_files($cssfiles);
    $csscontents = $this->minify->css->min($cssfile);
    $this->minify->save_file($csscontents, 'media/css/all.css');
	
	 // minify js
    $jsfiles = array('media/js/jquery.autosize.min.js', 'media/js/lodash.min.js', 'media/js/moment.min.js', 'media/js/jquery.lazyload.js', 'media/js/jquery-impromptu.js', 'media/js/jquery.fancybox.js', 'media/js/moment-timezone.js', 'media/js/main.js', 'media/js/jquery.form.js', 'media/js/jquery.tokeninput.js', 'media/js/jquery.ui.autocomplete.html.js','media/js/jquery-textntags.js');
    $jsfile = $this->minify->combine_files($jsfiles);
    $jscontents = $this->minify->js->min($jsfile);
    $this->minify->save_file($jscontents, 'media/js/all.js');
	echo "minifyed and combined js and css files. Done!";
  }
  function process_email_queue() {
    $this->load->library('email');
    $this->load->helper('smtp_config');


    $queue_list = $this->cron_model->email_queue_process();
    //echo '<xmp>'.print_r($queue_list,1).'</xmp>';
    //echo 'Safety on'; exit;

    $cnt = 1;

    if (count($queue_list)) {
      $smtp_config = smtp_config();


      foreach ($queue_list as $details) {
        $sender_email = $details['from_email'];
        $sender_name = $details['from_name'];
        $email = $details['to_email'];
        $subject = $details['email_subject'];
        $message = $details['email_body'];
        $email_campaign_id = $details['email_campaign_id'];
        $id = $details['id'];

        $this->email->initialize($smtp_config);
        $this->email->set_newline("\r\n");


        $this->email->from($sender_email, $sender_name);

        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($email);
        if ($this->email->send()) {
          $data = array(
              'id' => NULL,
              'to_email' => $email,
              'email_campaign_id' => $email_campaign_id,
              'sent_on' => date("Y-m-d H:i:s"),
          );
          $this->cron_model->clear_email_queue($id);
          $this->cron_model->notification_log($data);
          echo $cnt++.' mail sent<br />';
        } else {
          $error_msg = $this->email->print_debugger();
          $this->cron_model->failed_email_log($id, $error_msg);
        }
      }
    }
  }

  function send_pending_post_notification(){
    $pending_cat_post = array();
    $pending_post = $this->cron_model->get_pending_post();
    if(count($pending_post)){
      foreach($pending_post as $post){
        $cat = $post['category_basename'];
        $post_id = $post['entry_id'];
        $pending_cat_post[$cat][] = '<a style="color:#4f7f9a;" href="' . site_url('manage/edit_' . strtolower($cat) . '/' . $post_id) . '">'.$post['entry_title'].'</a>';
      }
    }
    $pending_events = $this->cron_model->get_pending_events();
    if(count($pending_events)){
      foreach($pending_events as $post){
        $post_id = $post['event_id'];
        $pending_cat_post['events'][] = '<a  style="color:#4f7f9a;" href="' . site_url('manage/edit_event/' . $post_id) . '">'.$post['event_title'].'</a>';
      }
    }
    if(!count($pending_cat_post)){
      return false;
    }
    $pending_items = null;
    foreach($pending_cat_post as $cat => $entry){
      $pending_items .= "<strong>". ucfirst($cat) ."</strong><br />";
      $post_cnt = 0;
      foreach($entry as $items){
        $pending_items .= "&nbsp;&nbsp;&nbsp;". ++$post_cnt . " " . $items . "<br />";
      }
      $pending_items .= '<br />';
    }
   
    $this->load->model('manage/manage_model');
    $review_permission_users = $this->manage_model->get_users_with_review_permission();
    if(count($review_permission_users)){
       foreach($review_permission_users as $review_user){
        $email_details['title'] = 'Reminder: Your approval is pending as on ' . date('dS M Y');
        $email_details['to_email'] = $review_user['email'];
        $email_details['body'] = '<table width="645" border="0" cellspacing="0" cellpadding="0">
             <tbody><tr>
             <td align="left" valign="top" bgcolor="#dddddd" style="padding:16px 22px 20px 23px"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
             <tbody><tr>
             <td align="left" valign="top">&nbsp;</td>
             <td align="left" valign="top" style="padding:17px 15px 20px 11px"><p style="margin:0px;margin-bottom:20px;font-family:Arial, Helvetica, sans-serif;font-size:18px;line-height:20px"><b>Reminder: Your approval is pending as on ' . date('dS M Y') . '</b><br>
             <b style="font-size:14px;color:#b4b4b4"><span class="il">Landmark</span> Intranet</b></p>
             <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px">Hi ' . ucfirst(strtolower($review_user['first_name'])). ',</p>
             <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px">New items were submitted on the intranet and your approval for them is pending. Please review them.</p>
             <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px"> Just click on the links below to review them.</p>
             <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px">'.$pending_items.'</p>
             </td>
             <td align="left" valign="top">&nbsp;</td>
             </tr>
             </tbody></table></td>
             </tr>
             </tbody>
           </table>';
       }
    }
    echo '<xmp>'. print_r($review_permission_users,1).'</xmp>';
  }
  function _send_email($email_details){

    $this->load->library('email');
    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    $this->email->initialize($smtp_config);
    $this->email->set_newline("\r\n");
    $sender_email = 'intranet@landmarkgroup.com';
    $sender_name = 'Landmark Intranet';

    $this->email->from($sender_email, $sender_name);
    $this->email->subject($email_details['title']);
    $this->email->message($email_details['body']);

    $this->email->to($email_details['to_email']);
    return $this->email->send();
  }

  function dropbox_expiry(){
    $expiry_timestamp = time() - 604800; //7 days
    echo $result = $this->cron_model->dropbox_expire_sql($expiry_timestamp);
  }
  function event_reminder(){
    $start_timespamp = mktime(0,0 ,1, date('m'), (date('d')+1), date('Y')); // One day beofre
    $end_timespamp = mktime(23,59 ,59, date('m'), (date('d')+1), date('Y')); //One day before

    $result = $this->cron_model->event_notification_userlist($start_timespamp,$end_timespamp);
    if(!count($result)){
      echo 'No reminders';
      return false;
    }

    foreach ($result as $value) {
      $email_details['title'] = 'Event Reminder: '.$value['event_title'];
      $email_details['to_email'] = $value['email'];
      $email_details['body'] = '<table width="645" border="0" cellspacing="0" cellpadding="0">
                 <tbody><tr>
                 <td align="left" valign="top" bgcolor="#dddddd" style="padding:16px 22px 20px 23px"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                 <tbody><tr>
                 <td align="left" valign="top">&nbsp;</td>
                 <td align="left" valign="top" style="padding:17px 15px 20px 11px"><p style="margin:0px;margin-bottom:20px;font-family:Arial, Helvetica, sans-serif;font-size:18px;line-height:20px"><b>Event Reminder: '.$value['event_title'].' on '.date('d M Y',$value['event_datetime']).'</b><br>
                   <b style="font-size:14px;color:#b4b4b4"><span class="il">Landmark</span> Intranet</b></p>
                   <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px">Hi ' . ucfirst(strtolower($value['first_name'])). ',</p>
                   <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px">There is a upcoming event which you are subscribed to.</p>
                   <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px"> Just click on the links below to view the event details.</p>
                   <p style="margin:0px;margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px"><a href="'.  site_url('events/event_detail/'.$value['id']).'">'.site_url('events/event_detail/'.$value['id']).'</a></p>
                 </td>
                 <td align="left" valign="top">&nbsp;</td>
                 </tr>
                 </tbody></table></td>
                 </tr>
                 </tbody>
               </table>';
      $this->_send_email($email_details);
    }
    
  }
}
