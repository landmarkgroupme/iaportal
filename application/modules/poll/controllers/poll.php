<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poll extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('poll_lib');
		$this->load->library('form_validation');

		$this->load->helper('html');
		$this->load->model('Concept');
		$this->load->model('User');
		$this->load->model('Group');


		$this->form_validation->set_error_delimiters('<dd class="error">', '</dd>');
		// $this->output->enable_profiler(TRUE);
	}

	// List latest polls
	// ----------------------------------------------------------------------
	public function index()
	{
		$this->session->userdata('fullname');

		$data['title'] = 'Polls';
		$data['base_styles'] = 'polls/res/css/base.css';

		$config['base_url'] = site_url('poll/page');
		$config['total_rows'] = $this->poll_lib->num_polls();
		$config['per_page'] = 10;
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data['results'] = $this->poll_lib->all_polls($config['per_page'], $this->uri->segment(3));
		$data['paging_links'] = $this->pagination->create_links();

		$this->load->view('poll/index', $data);
	}

	// Create new poll
	// ----------------------------------------------------------------------
	public function create()
	{
		$data['title'] = 'Create a new poll';
		$data['base_styles'] = 'polls/res/css/base.css';
		$data['min_options'] = $this->config->item('min_poll_options', 'poll');
		$data['max_options'] = $this->config->item('max_poll_options', 'poll');


		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('options[]', 'options', 'poll_options_required');
		$this->form_validation->set_error_delimiters('<li>', '</li>');

		//$this->load->view('poll/create', $data);

		if ($this->input->is_ajax_request())
		{
			/*print_r($_POST);
			die();*/

			if ($this->form_validation->run() == FALSE)
			{
				echo json_encode(array('fail' => TRUE, 'error_messages' => validation_errors()));
			}
			else
			{
				$created_by 		= $this->input->post('created_by');
				$created_by_type 	= $this->input->post('created_by_type');
				
				if(isset($_POST['target_id']) && isset($_POST['target_type']))
				{
					$target_id = $this->input->post('target_id');
					$target_type = $this->input->post('target_type');
				} else {
					$target_id = '';
					$target_type = '';
				}
				//$target_id 		= isset($this->input->post('target_id')) ? $this->input->post('target_id') : '';
				//$target_type 	= isset($this->input->post('target_type')) ? $this->input->post('target_type') : '';
				if(!isset($created_by) || $created_by == '')
				{
					$created_by 		= $this->session->userdata('userid');
					$created_by_type	= 'User';
				}
				
				$poll_id =  $this->poll_lib->create_poll($this->input->post('title'), $this->input->post('options'), $created_by, $created_by_type, $target_id, $target_type);
				if(!is_int($poll_id))
				{
					echo json_encode(array('status' => 'failed'));
				} else
				{
					 $latest_poll = $this->poll_lib->getPollCreatedBy();
					 if($latest_poll['created_by_type'] == 'Concept')
					{
						$item = $this->Concept->get(intval($latest_poll['created_by']));
						$item = $item[0];
						//print_r($item);exit;
						$latest_poll['profile_pic'] = 'images/concepts/thumbs/'.$item->thumbnail;
						$latest_poll['display_name'] = $item->name;
						$latest_poll['username'] = 'concepts/'.$item->slug;
					}
					else if($latest_poll['created_by_type'] == 'Group')
					{
						$item = $this->Group->get(intval($latest_poll['created_by']));
						$item = $item[0];
						$latest_poll['profile_pic'] = 'images/groups/thumbs/'.$item->thumbnail;
						$latest_poll['display_name'] = $item->name;
						$latest_poll['username'] = 'groups/'.$item->id;
					}
					else /* if($latest_poll['created_by_type'] == 'User') */
					{
						$item = $this->User->get(intval($latest_poll['created_by']));
						//print_r($item);exit;
						$item = $item[0];
						$latest_poll['profile_pic'] = 'images/user-images/105x101/'.$item->profile_pic;
						$latest_poll['display_name'] = $item->display_name;
						$latest_poll['username'] = $item->reply_username;
					}
					echo json_encode(array('status' => 'success', 'poll_id' => $poll_id, 'latest_poll'=> $latest_poll));
				}

			}
		}
		else
		{
			$this->load->view('poll/create', $data);
		}
	}

	// Add vote on option to poll
	// ----------------------------------------------------------------------
	public function vote($poll_id, $option_id)
	{
		$voter_id = $this->session->userdata('userid');
		if ( ! $this->poll_lib->vote($poll_id, $option_id, $voter_id))
		{
			//$data['base_styles'] = 'polls/res/css/base.css';
			$data['title'] = 'Sorry an error occured';
			$data['error_message'] = $this->poll_lib->get_errors();
			//$this->load->view('poll/error', $data);
			echo json_encode(array('status' => 'failed', 'error' => $data));
		}
		else
		{
			echo json_encode(array('status' => 'success', 'poll_id' => $poll_id, 'results' => $this->poll_lib->single_poll($poll_id) ));
		}
	}

	// View poll
	// ----------------------------------------------------------------------
	public function view($poll_id)
	{
		$data['title'] = 'Polls';
		$data['row'] = $this->poll_lib->single_poll($poll_id);
		$data['base_styles'] = 'polls/res/css/base.css';

		$this->load->view('poll/view', $data);
	}

	// Delete poll
	// ----------------------------------------------------------------------
	public function delete($poll_id)
	{
		if ($this->poll_lib->delete_poll($poll_id))
		{
			redirect('', 'refresh');
		}
	}

	// Open closed poll
	// ----------------------------------------------------------------------
	public function open($poll_id)
	{
		$this->poll_lib->open_poll($poll_id);
		redirect('', 'refresh');
	}

	// Close opened poll
	// ----------------------------------------------------------------------
	public function close($poll_id)
	{
		$this->poll_lib->close_poll($poll_id);
		redirect('', 'refresh');
	}

	// View datastructure
	// ----------------------------------------------------------------------
	public function data()
	{
		echo '<h2>Single poll (note: needs a valid id): </h2>';
		echo '<pre>';
		print_r($this->poll_lib->single_poll(4));
		echo '</pre>';

		echo '<h2>Latest poll: </h2>';
		echo '<pre>';
		print_r($this->poll_lib->single_poll()); // note no value passed in: so returns latest poll
		echo '</pre>';

		echo '<h2>Multiple polls: </h2>';
		echo '<pre>';
		print_r($this->poll_lib->all_polls(10, 0));
		echo '</pre>';
	}
}

/* End of file poll.php */
/* Location: ./application/controllers/poll.php */