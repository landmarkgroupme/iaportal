<?php
class Manage extends MX_Controller {

  function Manage() {
    parent::__construct();
    $this->load->model('manage_model');
    $this->load->model('backend/moderate_model');
    $this->load->model('ObjectMember');
    $this->load->model('Album');
    //date_default_timezone_set ('Asia/Dubai');
    $this->load->model('User');
    $this->load->model('Widget');
    $this->load->model('Role');
  }

  function _remap($method) {
    session_start();
    $permissions = $this->session->userdata('permissions');
    switch ($method) {
      case 'news':
      case 'create_news':
      case 'edit_news':
        if (!$permissions['news']) {
          $this->session->set_flashdata('status_error', 'Sorry, you do not have the permission to access News module');
          redirect(site_url("manage"));
        }
        break;
      case 'offers':
      case 'create_offers':
      case 'edit_offers':
        if (!$permissions['offers']) {
          $this->session->set_flashdata('status_error', 'Sorry, you do not have the permission to access Offers module');
          redirect(site_url("manage"));
        }
        break;
      case 'announcement':
      case 'create_announcement':
      case 'edit_announcement':
        if (!$permissions['announcements']) {
          $this->session->set_flashdata('status_error', 'Sorry, you do not have the permission to access Announcements module');
          redirect(site_url("manage"));
        }
        break;
      case 'photos':
    //  case 'create_album':
      case 'add_photos':
      case 'edit_album':
      //case 'uploadify':
      case 'submit_edit_album':
      case 'photos_bulk_action':
        if (!$permissions['photos']) {
          $this->session->set_flashdata('status_error', 'Sorry, you do not have the permission to access Photos module');
          redirect(site_url("manage"));
        }
        break;
      case 'permissions':
      case 'permissions_toggle':
      case 'make_user_as_admin':
      case 'autocomplete_users_name':
      case 'remove_admin':
        if (!$permissions['users']) {
          $this->session->set_flashdata('status_error', 'Sorry, you do not have the permission to access Permissions module');
          redirect(site_url("manage"));
        }
        break;
    }
    $this->load->helper('auth');
    auth();
    $this->$method();
  }

  function index() {
    $permissions = $this->session->userdata('permissions');
    //echo "<xmp>".print_r($permissions,1)."</xmp>";exit;
    if ($permissions["news"]) {
      redirect(site_url("manage/news"));
    } elseif ($permissions["announcements"]) {
      redirect(site_url("manage/announcement"));
    } elseif ($permissions["offers"]) {
      redirect(site_url("manage/offers"));
    } elseif ($permissions["photos"]) {
      redirect(site_url("manage/photos"));
    } elseif ($permissions["users"]) {
      redirect(site_url("manage/permissions"));
    } else {
      redirect(site_url());
    }


    //redirect("manage/news");
  }

  function _general_mt_data($mt_category, $query = '', $oConfig = '',$review='' , $filterExpiry = '') {
    $this->load->helper('time_ago');
    //MT CATEGORY
    $mt_category = $mt_category;
    $data = array();
    $data['mt_category'] = $mt_category;
    $data['page_title'] = ucfirst(strtolower($mt_category)); //Page Title
    if($query == 'show_review'){
      $query ='';
    }
    $data['user_name'] = $this->session->userdata('fullname');
    if($review == 'show_review'){
      $db_cat_count = $this->manage_model->get_review_count_mt_entries($mt_category, $query);
    }else{
      $db_cat_count = $this->manage_model->get_count_mt_entries($mt_category, $query, $filterExpiry);
    }
    $db_cat_count = $db_cat_count['total_entries'];
    
    
    $this->load->library('pagination');
    //check to overwrite config
    if (empty($oConfig)) {
      $config['base_url'] = site_url("manage/" . $mt_category);
      $config['uri_segment'] = 3;
    } else {
      $config['base_url'] = $oConfig['base_url'];
      $config['uri_segment'] = $oConfig['uri_segment'];
    }
    $config['total_rows'] = $db_cat_count;
    $config['per_page'] = $this->session->userdata('cfgpagelimit');
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;&#171;";
    $config['last_link'] = "&#187;&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $db_cat_count;

    $data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);
    $this->pagination->initialize($config);
    $data['navlinks'] = $this->pagination->create_links();
    
    
    if($review == 'show_review'){
      $db_data = $this->manage_model->get_all_review_mt_entries($mt_category, $config['per_page'], $this->uri->segment($config['uri_segment'], 0), $query);
    }else{
      $db_data = $this->manage_model->get_all_mt_entries($mt_category, $config['per_page'], $this->uri->segment($config['uri_segment'], 0), $query,$filterExpiry);
    }
    
    
    //Customizing the Database field
    for ($i = 0; $i < count($db_data); $i++) {
      $entry_status = $db_data[$i]['entry_status'];
      $entry_date = $db_data[$i]['entry_created_on'];
      $entry_date_timetamp = strtotime($entry_date);
      $formated_time = time_ago($entry_date_timetamp); //calling time ago helper
      $db_data[$i]['entry_created_on'] = $formated_time;
    }
    $data['list_data'] = $db_data;

    return $data;
  }

  function offers() {
    $mt_category = "offers";
    $filterExpiry = false;
    if(!empty($_GET['hide_expiry'])) {
      $filterExpiry = $_GET['hide_expiry'];
    }
    $this->session->set_flashdata('hide_expiry', $filterExpiry);
    $query = '';
    $oConfig = '';
    $review='';

    $data = $this->_general_mt_data($mt_category, $query, $oConfig, $review, $filterExpiry);
//echo "<pre>";print_r($data);echo "</pre>";exit;
    //Active Navigation menu
    $data['nav']['active_news'] = '';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '  class="active"';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
	$data['filterExpiry'] =  $filterExpiry;
    $this->load->view("listing_template", $data);
  }

  function news() {
    $mt_category = "news";

    $data = $this->_general_mt_data($mt_category);
    //Active Navigation menu
    $data['nav']['active_news'] = ' class="active"';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
    $this->load->view("listing_template", $data);
  }
  function email_queue_search() {
    $this->load->library('pagination');
    if($_POST['SearchText']){
      $search_term = $this->input->post('SearchText');
    }elseif($this->uri->segment(3, 0)){
      $search_term =$this->uri->segment(3, 0);
    }

    if(!$search_term){
      redirect('manage/email_queue');
    }
    $data['search_term'] = $search_term;
    $per_page = $this->session->userdata('cfgpagelimit');

    $total_count_email_manager = $this->manage_model->count_email_manager($search_term);

    $config['total_rows'] = $total_count_email_manager;
    $config['base_url'] = site_url('manage/email_queue_search/'.$search_term.'/');
    $config['per_page'] = $per_page;
    $config['num_links'] = 2;
    $config['uri_segment'] = 4;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;&#171;";
    $config['last_link'] = "&#187;&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);

    $this->pagination->initialize($config);
    $data['navlinks'] = $this->pagination->create_links();
    $email_queue = array();
    $email_queue_details = array();
    $email_queue = $this->manage_model->read_email_manager($search_term,$per_page,$this->uri->segment(4, 0) );

    if (count($email_queue)) {
      for ($i = 0; $i < count($email_queue); $i++) {
        $campaign_id = $email_queue[$i]['id'];
        $pending_queue = $this->manage_model->count_email_queue($campaign_id); // For total email count
        $pending_failed_queue = $this->manage_model->count_email_queue_failed($campaign_id); // For total failed email count
        $finished_queue = $this->manage_model->count_email_finished_queue($campaign_id);

        $final_pending_queue = (int) $pending_failed_queue - (int) $pending_queue;

        if (!$finished_queue) {
          $email_queue_details[$i]['status'] = 'Not Started';
          $email_queue_details[$i]['class'] = 'red';
        } elseif (!$final_pending_queue) {
          $email_queue_details[$i]['status'] = 'Completed';
          $email_queue_details[$i]['class'] = 'green';
        }elseif($finished_queue &&$final_pending_queue < 0){
          $email_queue_details[$i]['status'] = 'In progress';
          $email_queue_details[$i]['class'] = 'orange';
        }


        $email_queue_details[$i]['total_email_count'] = ($finished_queue + $pending_queue);
        $email_queue_details[$i]['total_email_failed_count'] = $pending_failed_queue;
        $email_queue_details[$i]['total_email_sent_count'] = ($finished_queue) ? $finished_queue : 0;
        $email_queue_details[$i]['campaign_id'] = $campaign_id;
        $email_queue_details[$i]['email_title'] = $email_queue[$i]['email_title'];
        $email_queue_details[$i]['email_type'] = $email_queue[$i]['email_type'];
        $email_queue_details[$i]['created_on'] = date('d M', strtotime($email_queue[$i]['created_on']));
        $email_queue_details[$i]['name'] = ucfirst(strtolower($email_queue[$i]['first_name'])) . ' ' . ucfirst(strtolower($email_queue[$i]['last_name']));
      }
    }

    //echo '<xmp>'.print_r($email_queue_details,1).'</xmp>';exit;
    $data['email_manager_list'] = $email_queue_details;
    //Active Navigation menu
    $data['nav']['active_news'] = ' class="active"';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
    $this->load->view("email_queue_template", $data);
  }
  function email_queue() {
    $this->load->library('pagination');
    $data['user_name'] = $this->session->userdata('fullname');
    $mt_category = "email_queue";
    $per_page = $this->session->userdata('cfgpagelimit');
    //$per_page = 4;
    $total_count_email_manager = $this->manage_model->count_email_manager('');

    $config['total_rows'] = $total_count_email_manager;
    $config['base_url'] = site_url('manage/email_queue/');
    $config['per_page'] = $per_page;
    $config['num_links'] = 2;
    $config['uri_segment'] = 3;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;&#171;";
    $config['last_link'] = "&#187;&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);

    $this->pagination->initialize($config);
    $data['navlinks'] = $this->pagination->create_links();
    $email_queue = array();
    $email_queue_details = array();
    $email_queue = $this->manage_model->read_email_manager('',$per_page,$this->uri->segment(3, 0) );

    if (count($email_queue)) {
      for ($i = 0; $i < count($email_queue); $i++) {
        $campaign_id = $email_queue[$i]['id'];
        $pending_queue = $this->manage_model->count_email_queue($campaign_id); // For total email count
        $pending_failed_queue = $this->manage_model->count_email_queue_failed($campaign_id); // For total failed email count
        $finished_queue = $this->manage_model->count_email_finished_queue($campaign_id);

        $final_pending_queue = (int) $pending_failed_queue - (int) $pending_queue;

        if (!$finished_queue && !$pending_failed_queue) {
          $email_queue_details[$i]['status'] = 'Not Started';
          $email_queue_details[$i]['class'] = 'red';
        } elseif (!$final_pending_queue) {
          $email_queue_details[$i]['status'] = 'Completed';
          $email_queue_details[$i]['class'] = 'green';
        }elseif($finished_queue &&$final_pending_queue < 0){
          $email_queue_details[$i]['status'] = 'In progress';
          $email_queue_details[$i]['class'] = 'orange';
        }


        $email_queue_details[$i]['total_email_count'] = ($finished_queue + $pending_queue);
        $email_queue_details[$i]['total_email_failed_count'] = $pending_failed_queue;
        $email_queue_details[$i]['total_email_sent_count'] = ($finished_queue) ? $finished_queue : 0;
        $email_queue_details[$i]['campaign_id'] = $campaign_id;
        $email_queue_details[$i]['email_title'] = $email_queue[$i]['email_title'];
        $email_queue_details[$i]['email_type'] = $email_queue[$i]['email_type'];
        $email_queue_details[$i]['created_on'] = date('d M', strtotime($email_queue[$i]['created_on']));
        $email_queue_details[$i]['name'] = ucfirst(strtolower($email_queue[$i]['first_name'])) . ' ' . ucfirst(strtolower($email_queue[$i]['last_name']));
      }
    }

    //echo '<xmp>'.print_r($email_queue_details,1).'</xmp>';exit;
    $data['email_manager_list'] = $email_queue_details;
    //Active Navigation menu 
    $data['nav']['active_news'] = ' class="active"';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
    $this->load->view("email_queue_template", $data);
  }
  function emailqueue_error(){
    $campaign_id =  $this->uri->segment(3, 0);
    $email_queue_list = $this->manage_model->email_queue_errors(1);


    header("Content-Description: File Transfer");
    header("Content-type: application/vnd.ms-excel");
    header("Content-disposition: filename=email_queue.xls");

   
    $order   = array("\r\n", "\n", "\r");
    $replace = ' ';
	$data['email_queue_list']= $email_queue_list;
	$csv="";
	$csv = $this->load->view('emailqueue_error_view', $data, true);
    echo $csv;
  }
  function create_offers() {
    $mt_category = "offers";
    $data['user_name'] = $this->session->userdata('fullname');

    $location = $this->manage_model->get_territory_names();

    $concepts = $this->manage_model->get_concept_names();
    
    $grades = $this->manage_model->get_grade_names();
    
    $data['location'] = $location;
    $data['concepts'] = $concepts;
    $data['grades'] = $grades;

    $this->load->view('offers_create_template', $data);
  }

  function create_news() {
    $mt_category = "news";
    $data['user_name'] = $this->session->userdata('fullname');
    $albums = $this->manage_model->get_album_list();
    /*$location = $this->manage_model->get_territory_names();
    $concepts = $this->manage_model->get_concept_names();
    $grades = $this->manage_model->get_grade_names();
    $data['location'] = $location;
    $data['concepts'] = $concepts;
    $data['grades'] = $grades;*/
    $data['albums'] = $albums;

    $this->load->view('news_create_template', $data);
  }

  function edit_news($entry_id = NULL) {
    $entry_id = $this->uri->segment(3, 0);
    if (!is_numeric($entry_id)) {
      redirect(site_url("manage/news"));
    }
    $mt_category = "news";
    $data['user_name'] = $this->session->userdata('fullname');

    $entry_details = $this->manage_model->get_entry_details($entry_id);
    if (!isset($entry_details['entry_id'])) {
      redirect(site_url("manage/news"));
    }
    $albums = $this->manage_model->get_album_list();
    $data['albums'] = $albums;

    $data['entry_details'] = $entry_details;

    $this->load->view('news_edit_template', $data);
  }

  function edit_offers($entry_id = NULL) {
    $entry_id = $this->uri->segment(3, 0);
    if (!is_numeric($entry_id)) {
      redirect(site_url("manage/offers"));
    }
    $mt_category = "offers";
    $data['user_name'] = $this->session->userdata('fullname');

    $entry_details = $this->manage_model->get_entry_details($entry_id);
    //Override variables
    $entry_details['offers_start_date'] = date('d/m/Y',strtotime($entry_details['offers_start_date']));
    $entry_details['offers_end_date'] = date('d/m/Y',strtotime($entry_details['offers_end_date']));
    $data['entry_details'] = $entry_details;
    //echo "<xmp>".print_r($entry_details,1)."</xmp>"; exit;
    $this->load->view('offers_edit_template', $data);
  }

  function announcement() {
    $mt_category = "announcement";
    $data = $this->_general_mt_data($mt_category);
    //Active Navigation menu
    $data['nav']['active_news'] = '';
    $data['nav']['active_annoucements'] = ' class="active"';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
    $this->load->view("listing_template", $data);
  }

  function create_announcement() {
    $mt_category = "announcement";
    $data['user_name'] = $this->session->userdata('fullname');

    $albums = $this->manage_model->get_album_list();    
    $location = $this->manage_model->get_territory_names();    
    $concepts = $this->manage_model->get_concept_names();    
    $grades = $this->manage_model->get_grade_names();
    
    $data['location'] = $location;
    $data['concepts'] = $concepts;
    $data['grades'] = $grades;
    $data['albums'] = $albums;

    $this->load->view('announcement_create_template', $data);
  }

  function edit_announcement($entry_id = NULL) {
    $entry_id = $this->uri->segment(3, 0);
    if (!is_numeric($entry_id)) {
      redirect(site_url("manage/announcement"));
    }
    $mt_category = "news";
    $data['user_name'] = $this->session->userdata('fullname');

    $entry_details = $this->manage_model->get_entry_details($entry_id);
    $albums = $this->manage_model->get_album_list();    
	$concept_ids_arr = array();
    $territory_ids_arr = array();
    $band_ids_arr = array();
    //Get saved notification
    if($entry_details['entry_status'] == 3 || $entry_details['entry_status'] == 1){
      $saved_notification = $this->manage_model->get_saved_notifications($entry_id);
      //echo "<xmp>" . print_r($saved_notification, 1) . "</xmp>";
      

      if(count ($saved_notification)){
        $concept_ids_arr = @explode(',',$saved_notification['ci_master_concept_ids']);
        $territory_ids_arr = @explode(',',$saved_notification['ci_master_territory_ids']);
        $band_ids_arr = @explode(',',$saved_notification['ci_master_band_ids']);
      }
    }
    $location = $this->manage_model->get_territory_names();
	$concepts =  $this->manage_model->get_concept_names();
    $grades = $this->manage_model->get_grade_names();
    $data['location'] = $location;
	$data['territory_ids_arr'] = $territory_ids_arr;
    $data['concepts'] = $concepts;
	$data['concept_ids_arr'] = $concept_ids_arr;
    $data['grades'] = $grades;
	$data['band_ids_arr'] = $band_ids_arr;
    $data['albums'] = $albums;
	$data['entry_details'] = $entry_details;

    $this->load->view('announcement_edit_template', $data);
  }

  //MT Entries Delete / Publish action
  function mt_delete_publish_action() {
    $search = $this->input->post('Search');
    $mt_category = $this->input->post('mt_category');
    $show_review = $this->input->post('show_review');
    $this->session->set_flashdata('show_review', $show_review);
    
    if (!empty($search)) {
      $query = $this->input->post('SearchText');
      if(strtolower($query) == 'search '.$mt_category){
        $query = '';   
      }
    }
    if (!empty($query) || !empty($show_review)) {
      if(empty($query)){
        $query = 'show_review';
      }
      $this->session->set_flashdata('SearchText', $query);
      redirect('manage/results/' . $mt_category . '/' . $query.'/'.$show_review);
    }
    
    if (!$_SERVER['HTTP_REFERER']) {
      redirect("manage/news");
    }

    $del_status = $this->input->post("Delete");
    $publish_status = $this->input->post("Publish");
	$selected_ids = $this->input->post("sel");
    if ( isset($selected_ids) && (isset($del_status) || isset($publish_status))) {
      $update_status = 2;
      $status_del = 0;
      if ($del_status == "Remove") {
        $status_del = 1;
        $update_status = 0;
      }
      //Delete the offer
      
      $update_status = $this->manage_model->update_mt_entries_status($update_status, $selected_ids);
      if ($update_status) {
        if ($status_del == 1) {
          $this->session->set_flashdata('status_del', 'Selected entries have been deleted successfully.');
        } else {
          $this->session->set_flashdata('status_ok', 'Selected entries have been updated successfuly.');
        }
      } 
    }
    redirect($_SERVER['HTTP_REFERER']);
  }

  //general function for creating a Entry (news, Announcements, Offers)
  function submit_mt_entry() {


    $this->load->model('backend/moderate_model');
    unset($_SESSION['status_ok']);
    $country = $this->input->post("terr");
    $concepts = $this->input->post("conc");
    $bands = $this->input->post("grad");

    $int_constants = $this->config->item('const');

    $concept_id =$this->input->post('concept_id');
    if($concept_id == '')
      $concept_id = $int_constants['default_concept'];

    $entry_category = $this->input->post('entry_category');
    $entry_title = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "",$this->input->post('txt_title'));
    $entry_textmore = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $this->input->post('txt_excerpt'));
    $entry_body = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "",$this->input->post('txt_body'));
    $publish_status = $this->input->post('txt_publish');
    $offer_start_date = ($this->input->post('txt_date_start'))? $this->input->post('txt_date_start'):null;
    $offer_end_date = ($this->input->post('txt_date_end'))? $this->input->post('txt_date_end'):null;
    $button = $this->input->post('save');
    $album_id = $this->input->post('txt_album');
    $album_id = ($album_id > 0) ? $album_id : NULL;
    

    
    if($offer_start_date){
      list($day,$month,$year) = @explode('/',$offer_start_date);
      $offer_start_date = "{$year}-{$month}-{$day}";
    }
    if($offer_end_date){
      list($day,$month,$year) = @explode('/',$offer_end_date);
      $offer_end_date = "{$year}-{$month}-{$day}";
    }



    $user_permission = $this->session->userdata('permissions');
    //Publish sstatus validation
    if($publish_status != "1"){
      $publish_status = ($user_permission['review'])? 2:3; //IF review permission is present then directly publish the page without going into review mode
    }
    if($publish_status != "1" && $entry_category == 'offers'){
      $publish_status = 2;
    }

    

    if ($entry_category) {
      $category_details = $this->manage_model->get_mt_category_details($entry_category);

      $entry_array = array(
          'entry_id' => NULL,
          'entry_author_id' => $this->session->userdata('userid'),
          'entry_authored_on' => date('Y-m-d H:i:s'),
          'entry_basename' => url_title($entry_title, 'underscore', TRUE),
          'entry_blog_id' => $category_details['category_blog_id'],
          'entry_created_by' => $this->session->userdata('userid'),
          'entry_created_on' => date('Y-m-d H:i:s'),
          'entry_excerpt' => $album_id,
          'entry_modified_on' => date('Y-m-d H:i:s'),
          'entry_status' => $publish_status,
          'entry_text' => $entry_body,
          'entry_text_more' => $entry_textmore,
          'offers_start_date' => $offer_start_date,
          'offers_end_date' => $offer_end_date,
          'entry_title' => $entry_title
      );
      

      // concept specific posting

      $entry_array['concept_id'] = $concept_id;

      $entry_id = $this->manage_model->submit_mt_entry($entry_array);

      if ($entry_id) {
        $placement_array = array(
            'placement_id' => NULL,
            'placement_blog_id' => $category_details['category_blog_id'],
            'placement_category_id' => $category_details['category_id'],
            'placement_entry_id' => $entry_id,
            'placement_is_primary' => 1
        );
        $this->manage_model->submit_mt_placement($placement_array);




        //Send emails if publish status is Review
        if($publish_status == 3){
          $user_details = $this->moderate_model->get_user_details($this->session->userdata('userid'));
          if(count($user_details)){
            $review_permission_users = $this->User->getContentModerators();
            foreach($review_permission_users as $review_user){
              $email_details['title'] = ucfirst($entry_category)." review: ".$entry_title;
              $email_details['to_email'] = $review_user['email'];
			 // $email_details['to_email'] = 'kunal.patil@intelliswift.co.in';
      			  $data['entry_category']= $entry_category;
      			  $data['review_user'] = $review_user['first_name'];
      			  $data['entry_id'] = $entry_id;
              $email_details['body'] = $this->load->view( 'emails/approval_waiting', $data, true );
              $this->_send_email($email_details);
            }
          }
        }



        if ($publish_status == 2) {
          
          $this->session->set_flashdata('status_ok', 'Entry has been added');
          $status = 1;
          
		 
        } elseif($entry_category == "announcement") {
          $this->session->set_flashdata('status_ok', 'Entry has been added');
          $status = 1;
        }elseif($entry_category == "offers"){
          //sending email to offer submitter
          $entry_owner = $this->manage_model->get_mt_entry_owner($entry_id);
          $submitter_name = $entry_owner['first_name'];
          $email_details['title'] = 'Your Offer announcement is now live';
          $email_details['to_email'] = $entry_owner['email'];
		  
		      $data['submitter_name']= $submitter_name;
          $email_details['body'] =  $this->load->view( 'emails/offer_announcement_live', $data, true );
          $this->_send_email($email_details);
         
          $status = 1;
          $this->session->set_flashdata('status_ok', 'Entry has been added');
        }
		elseif($entry_category == "news"){
          $status = 1;
          $this->session->set_flashdata('status_ok', 'Entry has been added');
        }
      } else {
        $status = 2;
        $this->session->set_flashdata('status_error', 'Error creating new Entry');
      }

	  $redirect_url = explode('/',$_SERVER['HTTP_REFERER']);
	  if($redirect_url[3] == 'concepts' && $redirect_url[4] == 'view')
	  {
	  $result = '{"status": "' . $status . '","redirect" : "' . $_SERVER['HTTP_REFERER'] . '"}';
	  }
	  else
	  {
      $result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
	  }
      $data['json'] = $result;
      $this->load->view("includes/print_json", $data);
    }
  }
  
  function get_mt_entry()
  {
	$entry_id = $this->input->post("entry_id");
    $entry_details = $this->manage_model->get_entry_details($entry_id);
	$data['json'] = json_encode($entry_details);
    $this->load->view('print_json', $data);
	
  }
	
  function edit_mt_entry() {
    $this->load->model('backend/moderate_model');
    unset($_SESSION['status_ok']);
    $country = $this->input->post("terr");
    $concepts = $this->input->post("conc");
    $bands = $this->input->post("grad");
    $int_constants = $this->config->item('const');
    $concept_id =$this->input->post('concept_id');
    if($concept_id == '')
      $concept_id = $int_constants['default_concept'];
	$entry_id = $this->input->post('entry_id');
    $entry_category = $this->input->post('entry_category');
    $entry_title = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "",$this->input->post('txt_title'));
    $entry_textmore = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $this->input->post('txt_excerpt'));
    $entry_body = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "",$this->input->post('txt_body'));
    $publish_status = $this->input->post('txt_publish');
    $offer_start_date = ($this->input->post('txt_date_start'))? $this->input->post('txt_date_start'):null;
    $offer_end_date = ($this->input->post('txt_date_end'))? $this->input->post('txt_date_end'):null;
    $button = $this->input->post('save');
    $album_id = $this->input->post('txt_album');
    $album_id = ($album_id > 0) ? $album_id : NULL;
    

    
    if($offer_start_date){
      list($day,$month,$year) = @explode('/',$offer_start_date);
      $offer_start_date = "{$year}-{$month}-{$day}";
    }
    if($offer_end_date){
      list($day,$month,$year) = @explode('/',$offer_end_date);
      $offer_end_date = "{$year}-{$month}-{$day}";
    }



    $user_permission = $this->session->userdata('permissions');
    //Publish sstatus validation
    if($publish_status != "1"){
      $publish_status = ($user_permission['review'])? 2:3; //IF review permission is present then directly publish the page without going into review mode
    }
    if($publish_status != "1" && $entry_category == 'offers'){
      $publish_status = 2;
    }

    

    if ($entry_category) {
      $category_details = $this->manage_model->get_mt_category_details($entry_category);

      $entry_array = array(
          'entry_author_id' => $this->session->userdata('userid'),
          'entry_authored_on' => date('Y-m-d H:i:s'),
          'entry_basename' => url_title($entry_title, 'underscore', TRUE),
          'entry_blog_id' => $category_details['category_blog_id'],
          'entry_created_by' => $this->session->userdata('userid'),
          'entry_excerpt' => $album_id,
          'entry_modified_on' => date('Y-m-d H:i:s'),
          'entry_status' => $publish_status,
          'entry_text' => $entry_body,
          'entry_text_more' => $entry_textmore,
          'offers_start_date' => $offer_start_date,
          'offers_end_date' => $offer_end_date,
          'entry_title' => $entry_title
      );
      

      // concept specific posting

      $entry_array['concept_id'] = $concept_id;

      $updated = $this->manage_model->update_mt_entry($entry_array, $entry_id);

      if ($updated) {
	  $placement_id = $this->manage_model->get_mt_placement($entry_id);
        $placement_array = array(
            'placement_blog_id' => $category_details['category_blog_id'],
            'placement_category_id' => $category_details['category_id'],
            'placement_entry_id' => $entry_id,
            'placement_is_primary' => 1
        );
        $this->manage_model->update_mt_placement($placement_array, $placement_id);




        //Send emails if publish status is Review
        if($publish_status == 3){
          $user_details = $this->moderate_model->get_user_details($this->session->userdata('userid'));
          if(count($user_details)){
            $review_permission_users = $this->User->getContentModerators();
            foreach($review_permission_users as $review_user){
              $email_details['title'] = ucfirst($entry_category)." review: ".$entry_title;
              $email_details['to_email'] = $review_user['email'];
			 // $email_details['to_email'] = 'kunal.patil@intelliswift.co.in';
      			  $data['entry_category']= $entry_category;
      			  $data['review_user'] = $review_user['first_name'];
      			  $data['entry_id'] = $entry_id;
              $email_details['body'] = $this->load->view( 'emails/approval_waiting', $data, true );
              $this->_send_email($email_details);
            }
          }
        }



        if ($publish_status == 2) {
          
          $this->session->set_flashdata('status_ok', 'Entry has been added');
          $status = 1;
          
		 
        } elseif($entry_category == "announcement") {
          $this->session->set_flashdata('status_ok', 'Entry has been added');
          $status = 1;
        }elseif($entry_category == "offers"){
          //sending email to offer submitter
          $entry_owner = $this->manage_model->get_mt_entry_owner($entry_id);
          $submitter_name = $entry_owner['first_name'];
          $email_details['title'] = 'Your Offer announcement is now live';
          $email_details['to_email'] = $entry_owner['email'];
		  
		      $data['submitter_name']= $submitter_name;
          $email_details['body'] =  $this->load->view( 'emails/offer_announcement_live', $data, true );
          $this->_send_email($email_details);
         
          $status = 1;
          $this->session->set_flashdata('status_ok', 'Entry has been added');
        }
		elseif($entry_category == "news"){
          $status = 1;
          $this->session->set_flashdata('status_ok', 'Entry has been added');
        }
      } else {
        $status = 2;
        $this->session->set_flashdata('status_error', 'Error creating new Entry');
      }

	  $redirect_url = explode('/',$_SERVER['HTTP_REFERER']);
	  if($redirect_url[3] == 'concepts' && $redirect_url[4] == 'view')
	  {
	  $result = '{"status": "' . $status . '","redirect" : "' . $_SERVER['HTTP_REFERER'] . '"}';
	  }
	  else
	  {
      $result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
	  }
      $data['json'] = $result;
      $this->load->view("includes/print_json", $data);
    }
  }

  //general function for creating a Entry (news, Announcements, Offers)
  function update_mt_entry() {
    $this->load->model('backend/moderate_model');
    $entry_category = $this->input->post('entry_category');
    $entry_id = $this->input->post('entry_id');
    $entry_title = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $this->input->post('txt_title'));
    $entry_textmore = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $this->input->post('txt_excerpt'));
    $entry_body = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $this->input->post('txt_body'));
    $publish_status = $this->input->post('txt_publish');
    $offer_start_date = ($this->input->post('txt_date_start'))? $this->input->post('txt_date_start'):null;
    $offer_end_date = ($this->input->post('txt_date_end'))? $this->input->post('txt_date_end'):null;
    //$featured_post = ($this->input->post('featured_post'))? $this->input->post('featured_post'):0;

    $button = $this->input->post('save');
    $album_id = $this->input->post('txt_album');
    $album_id = ($album_id > 0) ? $album_id : NULL;


    if($offer_start_date){
      list($day,$month,$year) = @explode('/',$offer_start_date);
      $offer_start_date = "{$year}-{$month}-{$day}";
    }
    if($offer_end_date){
      list($day,$month,$year) = @explode('/',$offer_end_date);
      $offer_end_date = "{$year}-{$month}-{$day}";
    }
    
    $user_permission = $this->session->userdata('permissions');
    //Publish status validation


    if($publish_status == "4"){
      //TODO -- Reject email notification here

      $publish_status = 4;
    }elseif($publish_status != "1"){
      $publish_status = ($user_permission['review'])? 2:3; //IF review permission is present then directly publish the page without going into review mode
    }
    
    if($publish_status != "1" && $entry_category == 'offers'){
      $publish_status = 2;
    }
    if ($entry_category) {
      $entry_array = array(
          'entry_basename' => url_title($entry_title, 'underscore', TRUE),
          'entry_excerpt' => $album_id,
          'entry_modified_on' => date('Y-m-d H:i:s'),
          'entry_modified_by' => $this->session->userdata('userid'),
          'entry_status' => $publish_status,
          'entry_text' => $entry_body,
          'entry_text_more' => $entry_textmore,
          'offers_start_date' => $offer_start_date,
          'offers_end_date' => $offer_end_date,
          'entry_title' => $entry_title,
          //'featured_post' => $featured_post
      );


      $update_status = $this->manage_model->update_mt_entry($entry_array, $entry_id);
      
      if ($update_status) {



        //email code here
        $user_details = $this->moderate_model->get_user_details($this->session->userdata('userid'));
        //Review
        if($publish_status == 3){
          if(count($user_details)){
            $review_permission_users = $this->manage_model->get_users_with_review_permission();
           
            $email_details['title'] = ucfirst($entry_category)." review: ".$entry_title;
            
            foreach($review_permission_users as $review_user){
              $email_details['to_email'] = $review_user['email'];
			  
			  /* Call Email template with passing data */
			  $data['entry_category']= $entry_category;
			  $data['review_user'] = $review_user['first_name'];
			  $data['entry_id'] = $entry_id;
              $email_details['body'] = $this->load->view( 'emails/approval_waiting', $data, true );
              $this->_send_email($email_details);
			  /* end */
            }
          }
        }elseif($publish_status == 2){
          //Published email

            

            $entry_owner = $this->manage_model->get_mt_entry_owner($entry_id);

            if($entry_category == 'news') {
              

              $email_details['title'] = 'Your '.ucfirst($entry_category)." announcement has been approved";
              $email_details['body'] = '';
			  
		  $data['entry_category']= $entry_category;
		  $data['entry_owner']= $entry_owner['first_name'];
		  $data['entry_id']= $entry_id;
		  $email_details['body'] =  $this->load->view( 'emails/news_announcement_live', $data, true );
          $email_details['to_email'] = $entry_owner['email'];
            } else {

              

              $email_details['title'] = ucfirst($entry_category)." published: ".$entry_title;
			  $data['entry_category']= $entry_category;
			  $data['entry_owner']= $entry_owner['first_name'];
		      $data['entry_id']= $entry_id;
              $email_details['body'] = $this->load->view( 'emails/new_post_live', $data, true );
              $email_details['to_email'] = $entry_owner['email'];
            }

            $this->_send_email($email_details);

        }elseif($publish_status == 4){
          //Rejected email
            $entry_owner = $this->manage_model->get_mt_entry_owner($entry_id);


            if($entry_category == 'news') {
              
              $email_details['title'] = 'Your '.ucfirst($entry_category)." announcement has been rejected";
			  $data['entry_category']= $entry_category;
			  $data['entry_owner']= $entry_owner['first_name'];
		      $data['entry_id']= $entry_id;
              $email_details['body'] = $this->load->view( 'emails/news_announcement_rejected', $data, true );
              $email_details['to_email'] = $entry_owner['email'];
            } else {
			 $data['entry_category']= $entry_category;
			  $data['entry_owner']= $entry_owner['first_name'];
		      $data['entry_id']= $entry_id;
              $email_details['title']= ucfirst($entry_category)." rejected: ".$entry_title;
              $email_details['body'] = $this->load->view( 'emails/new_post_rejected', $data, true );
              $email_details['to_email'] = $entry_owner['email'];
            }       
            $this->_send_email($email_details);
        }

        /* Notification code here */
        //Check for notification
        $saved_notification = $this->manage_model->get_saved_notifications($entry_id,1);

        $country = $this->input->post("terr");
        $concepts = $this->input->post("conc");
        $bands = $this->input->post("grad");

        $sql_where_arr = array();
        $country_ids = null;
        if (is_array($country)) {
          $country_ids = @implode(",", $country);
          $sql_where_arr[] = "territory_id IN ($country_ids)";
        }
        $concept_ids=null;
        if (is_array($concepts)) {
          $concept_ids = @implode(",", $concepts);
          $sql_where_arr[] = "concept_id IN ($concept_ids)";
        }

        $band_ids=null;
        if (is_array($bands)) {
          $band_ids = @implode(",", $bands);
          $sql_where_arr[] = "band_id IN ($band_ids)";
        }


        

        //if (count($sql_where_arr) && $publish_status == 2 && ( $entry_category == 'announcement' or $entry_id == 1035) ) {
        if (count($sql_where_arr) && $publish_status == 2 && ( $entry_category == 'announcement') ) {
          $sql_where = @implode(" AND ", $sql_where_arr);

          


          if ($entry_category == "offers") {
            $user_list = $this->manage_model->get_offers_users($sql_where);
          } else {
            $user_list = $this->moderate_model->get_notification_user_list($sql_where);
          }

          $mt_entry_details = $this->moderate_model->get_mt_entry_details($entry_id);
          if (count($mt_entry_details)) {

            $mt_id = $mt_entry_details[0]['entry_id'];
            $mt_title = $mt_entry_details[0]['entry_title'];
            $mt_desc = $mt_entry_details[0]['entry_text_more'];
            $mt_cat = strtoupper($mt_entry_details[0]['category_label']);
			$data['mt_id']=  $mt_id;
			$data['mt_cat']=  $mt_cat;
			$data['mt_title'] = $mt_title;
			$data['mt_desc'] = $mt_desc;
			$message = $this->load->view( 'emails/new_activity', $data, true );
          }
          //EMail code starts here
          $subject = "Landmark Intranet, {$mt_cat} Alert";
          $sender_email = "intranet@landmarkgroup.com";
          $sender_name = "Landmark Intranet";
          $initiator = $this->session->userdata('userid');

          $data = array(
              'id' => NULL,
              'email_title' => $mt_title,
              'email_subject' => $subject,
              'initiator' => $this->session->userdata('userid'),
              'email_body' => $message,
              'from_name' => $sender_name,
              'from_email' => $sender_email,
              'email_type' => $mt_cat,
              'created_on' => date("Y-m-d H:i:s")
          );

          $email_manager_id = $this->manage_model->create_email_manager($data);

          $email_queue = array();
          if (count($user_list)) {
            foreach ($user_list as $user_details) {


              $email = $user_details['email'];
              $name = $user_details['first_name'] . ' ' . $user_details['last_name'];

              $data = array(
                  'id' => NULL,
                  'email_campaign_id' => $email_manager_id,
                  'to_email' => $email,
                  'to_name' => $name,
                  'number_of_tries' => 0,
                  'email_error' => NULL,
                  'queued_on' => date("Y-m-d H:i:s")
              );

              $this->manage_model->create_email_queue($data);
            }
            if(isset($saved_notification['id'])){
              $this->manage_model->delete_saved_notifications($saved_notification['id']);
            }
          }
        }elseif($publish_status != 2 && $entry_category == "announcement"){
          /* Notifications saved here */
          $notification_save = array('page_id'=>$entry_id,'page_type'=>1,'ci_master_concept_ids'=>$concept_ids,'ci_master_territory_ids'=>$country_ids,'ci_master_band_ids'=>$band_ids);
          if(isset($saved_notification['id'])){
            $this->manage_model->update_saved_notifications($notification_save, $saved_notification['id']);
          }elseif($concept_ids || $country_ids || $band_ids){
            $this->manage_model->save_post_notification($notification_save,2);
          }
        }


        $this->session->set_flashdata('status_ok', 'Entry has been updated');
        $status = 1;
      } else {
        $this->session->set_flashdata('status_error', 'Unable to update Entry');
        $status = 2;
      }
	 
      $result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
      $data['json'] = $result;
      $this->load->view("includes/print_json", $data);
    }
  }

  function photos() {
    $this->load->helper('time_ago');
    $per_page = $this->session->userdata('cfgpagelimit');
    $data['user_name'] = $this->session->userdata('fullname');
    $data['allAlbums'] = array();



    //Get total offers and the list of all offers
    $data['total_photos'] = 0;
    $this->load->library('pagination');



    //Configuration for pagination libarary
    $action = $this->uri->segment(3, 0);
    $search_term = '';
    if ($action == 'results') {
      $search_term = urldecode($this->uri->segment(4, 0));
    }

    if (!empty($search_term)) {
      $config['base_url'] = site_url("manage/photos/results/" . $search_term);
      $config['uri_segment'] = 5;
    } else {
      $config['base_url'] = site_url("manage/photos/page/");
      $config['uri_segment'] = 4;
    }

    //Get total offers and the list of all offers
    $data['allAlbums'] = $this->manage_model->get_all_albums_for_index($per_page, $this->uri->segment($config['uri_segment'], 0), $search_term);
    $data['total_albums'] = $this->manage_model->get_albums_count($search_term);
	
    $config['total_rows'] = $data['total_albums'];
    $config['per_page'] = $per_page;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;&#171;";
    $config['last_link'] = "&#187;&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);

    $this->pagination->initialize($config);
    $data['navlinks'] = $this->pagination->create_links();
	$data['search_term'] = $search_term;
    $this->load->view('photos_template', $data);
  }

  function delete_photos() {

    $refer_url = $_SERVER['HTTP_REFERER'];
    if (!$refer_url) {
      redirect(site_url("manage"));
    }

    $sel_id = $this->input->post('sel');

    if (is_array($sel_id)) {
      foreach ($sel_id as $del_id) {
        $this->manage_model->delete_photos($del_id);
      }

      $this->session->set_flashdata('status_ok', 'Pictures have been delete Sucessfully');
      redirect($refer_url);
    } else {
      $this->session->set_flashdata('status_error', 'Please select the pictures you want to delete');
      redirect($refer_url);
    }
  }

  function create_album() {
    $data['user_name'] = $this->session->userdata('fullname');

    //Check for form submission
    if ($this->input->post('save')) {
//      echo $this->input->post('save'); exit;
      //echo $this->input->post('save'); exit;
      //Get the submitted values
      $album_name = $this->input->post('album_name');
      $album_description = $this->input->post('album_description');
      $album_visibility = ($this->input->post('save') == "Save as draft") ? 1 : 0;
      $album_category_id = $this->input->post('album_category_id');

      //echo "<xmp>".print_r($_SERVER,1)."</xmp>";
      //Create the album array to insert
      $album_slug = url_title($album_name, 'underscore', TRUE); //this will be the name of the folder we create
      $count_album_slug = $this->manage_model->get_album_slug_count($album_slug);
      if ($count_album_slug) {
        $album_slug = $album_slug . '_' . $count_album_slug;
      }
      $album = array(
          'title' => $album_name,
          'description' => $album_description,
          'visibility' => $album_visibility,
          'category' => $album_category_id,
          'owner' => $this->session->userdata('userid'),
          'keyword' => $album_slug
      );
      $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath'));
      $create_album_directory = str_replace(base_url(), '', $album_config['ecards_more_pic_target']) . $album_config['fullpath'];
     $create_album_directory .= $album_slug;

      $alb_dir_path = str_replace("/system/", '', BASEPATH);
      $create_album_directory = $alb_dir_path . '/phpalbum/albums/' . $album_slug;
      if (@mkdir($create_album_directory, 0777)) {
        $album_id = $this->manage_model->insert_album($album);
        if ($album_id) {
          $this->session->set_flashdata('status_ok', 'Album created successfully. You may now add photos to your album');
          redirect('manage/add_photos/' . $album_id);
        } else {
          $this->session->set_flashdata('status_error', 'Sorry. There was an error while creating your album.Please check your inputs and try again later.');
        }
      } else {
        $this->session->set_flashdata('status_error', 'Sorry. We cannot create your album at the moment. Please check your inputs and try again later.');
      }
      redirect('manage/create_album');
    }

    $categories = $this->manage_model->get_album_categories();
    $categories_list = '';
    if (count($categories)) {
      foreach ($categories as $category):
        $categories_list .= '<option value="' . $category['cid'] . '">' . $category['name'] . '</option>';
      endforeach;
    }
    $data['categories_list'] = $categories_list;
    $this->load->view('album_create_template', $data);
  }
  function submit_photo_album(){
    
    if ($this->input->post('save')) {
      //Get the submitted values
      $album_name = $this->input->post('album_name');
      $album_description = $this->input->post('album_description');
      $album_visibility = ($this->input->post('save') == 1) ? 1 : 0;
      $album_category_id = $this->input->post('album_category_id');
	  $count_album = $this->Album->getIdByTitle($album_name);
	  if(isset($count_album) && $count_album !='')
	  {
		   $result = '{"status": "2","message": "Sorry. There was a album with the same name. Please Change your album name and try again later."}';
	  }
	  else
	  {
			
	  
	  if($this->input->post('concept_id') !='')
	  {
		$album_concept_id = intval($this->input->post('concept_id'));
	  }
	  else
	  {
		$album_concept_id =  5;
	  }
	  

      //echo "<xmp>".print_r($_SERVER,1)."</xmp>";
      //Create the album array to insert
      $album_slug = url_title($album_name, 'underscore', TRUE); //this will be the name of the folder we create
      $count_album_slug = $this->manage_model->get_album_slug_count($album_slug);
	  $i = 0;
	  $album_slug_new = $album_slug;
      while($count_album_slug) {
		$count_album_slug = $count_album_slug + $i;
        $album_slug_new = $album_slug . '_' . $count_album_slug;
		$count_album_slug = $this->manage_model->get_album_slug_count($album_slug_new);
		$i++;
      }
	  $album_slug = $album_slug_new;
      $album = array(
          'title' => $album_name,
          'description' => $album_description,
          'visibility' => 1, //Always publish as daraft mode unless photos are uploaded
          'category' => $album_category_id,
          'owner' => $this->session->userdata('userid'),
          'keyword' => $album_slug,
	        'concept_id' => $album_concept_id
      );
      $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath'));
      $create_album_directory = str_replace(base_url(), '', $album_config['ecards_more_pic_target']) . $album_config['fullpath'];
      $create_album_directory .= $album_slug;

      $alb_dir_path = str_replace("/system/", '', BASEPATH);
      $create_album_directory = 'phpalbum/albums/' . $album_slug;
if (@mkdir($create_album_directory, 0777)) {
        $album_id = $this->manage_model->insert_album($album);
        if ($album_id) {
          $album_upload_path ="/phpalbum/albums/";
          // Moving the images from tmp to album folder
          $tmp_dir = $this->input->post('tmpfolder');
          $handle = opendir("phpalbum/albums/".$tmp_dir);
          while (($file = readdir($handle))!=false) { 
            if($file != "." && $file != "..") {
              @rename("phpalbum/albums/$tmp_dir/$file", "$create_album_directory/$file");
            } 
          } 
          closedir($handle);
          
          $result = '{"status": "1","album_folder": "' . $album_slug . '","album_id": "' . $album_id . '","album_upload_path":"'.$album_upload_path.'","publish_status":"'.$album_visibility.'"}';
         // $this->session->set_flashdata('status_ok', 'Album created successfully. You may now add photos to your album');
          //redirect('manage/add_photos/' . $album_id);
        
          $_SESSION['status_ok']= 'Album has been added';
        } else {
          $result = '{"status": "2","message": "Sorry. There was an error while creating your album.Please check your inputs and try again later."}';
        }
      } else {
        $result = '{"status": "2","message": "Sorry. There was an error while creating your album.Please check your inputs and try again later."}';
      }
	  }
    }
    $data['json'] = $result;
    $this->load->view("includes/print_json", $data);
  }
/*
 * @function Create the tmp folder for uploading the albums images
 **/
  function create_temp_folder() {
    $create_album_directory = 'phpalbum/albums/' . $this->input->post('tmpfolder');      
    if (@mkdir($create_album_directory, 0777)) {
      print "Directory Created";
      print "\n";
      if(@chown($create_album_directory, "root")) {
        print "Folder User Updated";
      }
    }
  }
  
  /*
   * @function Updated the tmp album id from alb_picture table
   **/
  
  function update_album_id_path() {
    
    $tmp_folder = $this->input->post('tmpfolder');
    $alb_id = $this->input->post('new_album_id');
    $alb_folder = $this->input->post('new_album_folder');
    $album_data = array(
        'aid' => $alb_id,
        'filepath' => $alb_folder.'/'
     );
    $result = $this->manage_model->update_tmp_album($album_data,$tmp_folder);
    
    // Update the Thumb image for album//Default Album check
    $alb_details = $this->manage_model->check_album_thumb($alb_id);
    if((empty($alb_details['thumb'])) || $alb_details['thumb'] == 0 ){
      $picture_ids = $this->manage_model->get_photos_by_aid($alb_id);
      if(!empty($picture_ids)) {
       $this->manage_model->update_album_thumb($picture_ids[0]['pid'],$alb_id);
       echo "Album Thumbnail Image Update Sucessfully";
      }
    }
    
    $data['json'] = $result;
    $this->load->view("includes/print_json", $data);
  }
  function add_photos($album_id = null) {
    $album_id = $this->uri->segment(3);
    $data['user_name'] = $this->session->userdata('fullname');
    $albums_list = $this->manage_model->get_all_albums();
    
    $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));
    $this->session->set_userdata($album_config);
    //**fix**
    //This link should be same as defined in PHP Album database
    //$_SERVER['HTTP_HOST'];
    //$album_upload_location = str_replace('http://intranet.landmarkgroupme.com','',$album_config['ecards_more_pic_target']).$album_config['fullpath'];
    //$album_upload_location = str_replace('http://raghunath', '', $album_config['ecards_more_pic_target']) . $album_config['fullpath'];
    $album_upload_location = "/phpalbum/albums/";
    //$album_upload_location = base_url()."phpalbum/album/";
	$data['album_id'] = $album_id;
    $data['album_upload_location'] = $album_upload_location;
    $data['albums_list'] = $albums_list;
    $this->load->view('add_photos_view', $data);
  }
  

  function edit_album($album_id = null) {
    $album_id = $this->uri->segment(3, 0);
    if ($album_id) {
      $per_page = $this->session->userdata('cfgpagelimit');

      //Pagination for pictures
      $data['pictures_count'] = $this->manage_model->get_pictures_count($album_id);
      $this->load->library('pagination');
      $this->load->helper('text');

      //Configuration for pagination libarary
      $config['base_url'] = site_url('manage/edit_album/' . $album_id . '/page/');
      $config['total_rows'] = $data['pictures_count'];
      $config['uri_segment'] = '5';
      $config['per_page'] = $per_page;
      $config['num_links'] = 2;
      $config['prev_link'] = "&laquo;";
      $config['next_link'] = "&raquo;";
      $config['first_link'] = "&#171;&#171;";
      $config['last_link'] = "&#187;&#187;";
      $config['first_tag_open'] = "<li>";
      $config['first_tag_close'] = "</li>";
      $config['last_tag_open'] = "<li>";
      $config['last_tag_close'] = "</li>";
      $config['num_tag_open'] = "<li>";
      $config['num_tag_close'] = "</li>";
      $config['cur_tag_open'] = "<li class='active'>";
      $config['cur_tag_close'] = "</li>";
      $config['next_tag_open'] = "<li>";
      $config['next_tag_clos'] = "</li>";
      $config['prev_tag_open'] = "<li>";
      $config['prev_tag_clos'] = "</li>";

      $data['total_count'] = $config['total_rows'];
      $data['limit'] = ($this->uri->segment(5, 0)) ? $this->uri->segment(5, 0) : 1;
      $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(5, 0) + $config['per_page']);

      $this->pagination->initialize($config);
      $data['navlinks'] = $this->pagination->create_links();

      //Get Album Details
      $album_details = $this->manage_model->get_album_details($album_id, $per_page, $this->uri->segment(5, 0));
      $data['album_details'] = $album_details;
      $data['user_name'] = $this->session->userdata('fullname');
      $data['album_config'] = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));

      if (!empty($album_details)) {
        //Get the album categories
        $categories_list = $this->manage_model->get_album_categories();
        $data['categories_list'] = $categories_list;

      } else {
        $this->session->set_flashdata('status_error', 'Sorry, No album found');
        redirect('manage/photos');
      }

      $this->load->view('edit_album_view', $data);
    } else {
      $this->session->set_flashdata('status_error', 'Sorry we cannot find the album for you');
      redirect("manage/photos");
    }
  }

  function uploadify() {
    $file = $this->input->post('filearray');
    $album_folder = $this->input->post('album_folder');
    $album_id = $this->input->post('album_id');
    $json = json_decode($file);
	$this->load->library('image_lib');
	
    //Get the configuration for thumbnail resizing
    $thumb_pfx = $this->session->userdata('thumb_pfx');
    if (empty($thum_pfx)) {
      $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));
      $this->session->set_userdata($album_config);
      $thumb_pfx = $album_config['thumb_pfx'];
      $thumb_height = $album_config['thumb_height'];
      $thumb_width = $album_config['thumb_width'];
    } else {
      $thumb_pfx = $this->session->userdata('thumb_pfx');
      $thumb_height = $this->session->userdata('thumb_height');
      $thumb_width = $this->session->userdata('thumb_width');
    }
    //Generate Thumbnail
    $this->load->helper('image_resize');
    $album_upload_location = str_replace(base_url(), '', $album_config['ecards_more_pic_target']) . $album_config['fullpath'];
    $alb_dir_path = str_replace("/system/", '', BASEPATH);
    $album_upload_location =  'phpalbum/albums/';
    $img_thumb = $album_upload_location . $album_folder . '/' . $thumb_pfx . $json->file_name;
	$img_ori = $album_upload_location . $album_folder . '/' .$json->file_name;
					$config1['image_library'] = 'gd2';
					$config1['source_image']	= $img_ori;
					$config1['maintain_ratio'] = TRUE;
					//$config1['master_dim'] = 'height';
					//$config1['create_thumb'] = TRUE;
					$config1['width']	 = 450;//$thumb_width;
					$config1['height']	= 350;//$thumb_height;
					$config1['new_image']	= $thumb_pfx.$json->file_name;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
					$this->image_lib->clear();
    $handlers = array(
      '.jpg'  => 'imagecreatefromjpeg',
      '.jpeg' => 'imagecreatefromjpeg',
      '.png'  => 'imagecreatefrompng',
      '.gif'  => 'imagecreatefromgif'
    );
    $imageCreators = array(
      '.jpg'  => 'imagejpeg',
      '.jpeg' => 'imagejpeg',
      '.png'  => 'imagepng',
      '.gif'  => 'imagegif'
    );
      $extension = strtolower($json->file_ext);
      if (($handler = $handlers[$extension]) && ($imageCreator = $imageCreators[$extension])){
//echo "<pre>";print_r($img_ori);echo "</pre>";
          $im = $handler(base_url().$img_ori);
      @imageinterlace($im, true);
      $imageCreator($im, $img_ori);  
    }
    /*$resizeObj = new resize($json->file_path);
    //$resizeObj->resizeImage($thumb_width, $thumb_height, 'crop');
		$resizeObj->resizeImage($thumb_width, $thumb_height, 'crop');
    $resizeObj->saveImage($img_thumb, 100);*/


		//Replace original files
    /*$resizeObj = new resize($json->file_path);
	$resizeObj->resizeImage(800, 600);
    $resizeObj->saveImage($img_ori, 100);*/
	
//	$va = move_uploaded_file($json->file_temp,$album_upload_location . $album_folder . '/' . $json->file_name);
//print $va;
	

    //Create the picture array
    $pic_file_size = $json->file_size * 1024;
    $picture = array(
        'pid' => NULL,
        'aid' => $album_id,
        'filepath' => $album_folder . '/',
        'filename' => $json->file_name,
        'filesize' => $pic_file_size,
        'owner_id' => $this->session->userdata('userid'),
        'ctime' => time(),
        'title' => $json->file_name
    );
    $picture_id = $this->manage_model->insert_picture($picture);
    if ($picture_id) {
      //Default Album check
      $alb_details = $this->manage_model->check_album_thumb($album_id);
      if(!empty($alb_details['thumb'])){
        $this->manage_model->update_album_thumb($picture_id,$album_id);
      }

      $picture['pid'] = $album_id;
      $data['picture'] = $picture;
      $data['result'] = 1;
      $data['message'] = 'Picture added successfully';
      $data['thumb_location'] = $album_config['ecards_more_pic_target'] . $album_config['fullpath'] . $album_folder . '/' . $thumb_pfx . $json->file_name;
    } else {
      $data['result'] = 0;
      $data['message'] = 'There was an error while adding the ' . $json->file_name . '. Try again later';
    }
    
    $data['json'] = $json;
    $this->load->view('uploadify', $data);
  }
  function publish_album(){
    $album_id = $this->input->post('album_id');
    $this->manage_model->update_photo_status(0,$album_id );//Publish album after uploading of file
    $data['result'] = 1;
    echo json_encode($data);
  }
  function submit_edit_album($album_id = null) {
    $album_id = $this->input->post("album_id");
    if ($album_id != null) {
      if (isset($_POST['save'])) {
        $album_title = $this->input->post('album_name');
        $album_description = $this->input->post('album_description');
        $album_category_id = $this->input->post('album_category_id');
        $album_thumbnail = $this->input->post('picture_id');
        $album_visibility = ($this->input->post('save') == "Save as draft") ? 1 : 0;
        $album_id = $this->input->post('album_id');

        //Create the album array to update
        $album = array(
            'title' => $album_title,
            'description' => $album_description,
            'visibility' => $album_visibility,
            'category' => $album_category_id,
            'thumb' => $album_thumbnail
        );
        if ($this->manage_model->update_album($album_id, $album)) {
          $this->session->set_flashdata('status_ok', 'Updated Sucessfully');
        } else {
          $this->session->set_flashdata('status_error', 'Sorry, there was an error while updating. Try again later');
        }
      }
      redirect('manage/edit_album/' . $album_id);
    } else {
      $this->session->set_flashdata('status_error', 'Sorry. There was an error. Please make sure you entered the right URL.');
      redirect('manage/photos');
    }
  }

  function photos_bulk_action() {
    if ( isset($_POST['sel']) && ( isset($_POST['Delete']) || isset($_POST['Publish']))) {
      $update_status = 0;
      $bulk_action = 'published';
      if (isset($_POST['Delete'])) {
        $update_status = 2;
        $bulk_action = 'deleted';
      }
      //Delete the offer
      $selected_ids = $_POST['sel'];
	  
      $up_status = $this->manage_model->update_photo_status($update_status, $selected_ids);
      if ($up_status) {
        if ($update_status == 2) {
          $this->session->set_flashdata('status_del', 'Selected albums have been deleted successfully.');
        } else {
          $this->session->set_flashdata('status_ok', 'Album(s) have been ' . $bulk_action . ' successfully.');
        }
      } else {
        $this->session->set_flashdata('status_error', 'Error performing ' . $bulk_action . ' operation.');
      }
    } elseif (isset($_POST['Search'])) {
      $query = $this->input->post('SearchText');
      if (!empty($query)) {
        redirect('manage/photos/results/' . $query);
      }
    }
    redirect("manage/photos");
  }

  function permissions() {
    $this->load->helper('time_ago');
  	$this->load->model('Group');
  	$this->load->model('Role');
  	$this->load->model('Concept');
    $this->load->model('User');
  	$this->load->model('GroupConceptPermission');
  	$data['group_list'] = $this->Group->getAll();
  	$data['concept_list'] = $this->Concept->getAll();
    $per_page = $this->session->userdata('cfgpagelimit');

    $data['user_name'] = $this->session->userdata('fullname');
    $data['allUsers'] = array();

    $this->load->library('pagination');

    $action = $this->uri->segment(3, 0);
    $search_term = '';
    if ($action == 'results') {
      $search_term = $this->uri->segment(4, 0);
    }

    if (!empty($search_term)) {
      $search_term = str_replace('-', ' ', $search_term);
      $config['base_url'] = site_url("manage/permissions/results/" . $search_term);
      $config['uri_segment'] = 5;
    } else {
      $config['base_url'] = site_url("manage/permissions/page/");
      $config['uri_segment'] = 4;
    }
    //setting this for text field
    $data['search_term'] = $search_term;

    $data['permission_details'] = $this->User->getUserswithRole($per_page, $this->uri->segment($config['uri_segment'], 0), $search_term);

    //$data['permission_details'] = $this->manage_model->get_admins_and_permissions($per_page, $this->uri->segment($config['uri_segment'], 0), $search_term);

    /*print_r($data['permission_details']);
    die();*/

	$type='Group';

	if(isset($data['permission_details']))
	{
	foreach($data['permission_details'] as $key => $users)
	{
	  $gp= array();
		$group_data= $this->GroupConceptPermission->getUserGroupPer(intval($users['id']), $type);
		//print $key;
		foreach($group_data as $group)
		{
		$gp[]=intval($group->object_id);
		}
		//$gp=array('426','660',);
		if(isset($gp[0]))
		{
		$data['permission_details'][$key]['group']= json_encode($this->GroupConceptPermission->getUserGroup($gp));
		}
		//getUserGroup
		//print "<pre>";
		//	print_r($gp);
		//	print "HI";
		
	}
	}
	//print "<pre>";
	//print_r($data['permission_details']);
	//exit;
	$data['roles'] = $this->Role->getAll();
  $data['total_users'] = $this->User->countUserswithRole($search_term);
	$sortby = array(
            'id' => 'Asc',
        );
	$limit = null;
	$data['gcpermission']= $this->GroupConceptPermission->getAll($limit,$sortby);

	
    //echo '<pre>'.print_r($data['permission_details'],1).'</pre>';
    //Configuration for pagination libarary
    $config['total_rows'] = $data['total_users'];
    $config['per_page'] = $per_page;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;&#171;";
    $config['last_link'] = "&#187;&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);

    $this->pagination->initialize($config);
    $data['navlinks'] = $this->pagination->create_links();

    $this->load->view('permissions_index_view', $data);
  }

  function permissions_toggle() {
    $column_name = $this->uri->segment(3, 0);
    $user_id = $this->uri->segment(4, 0);
    $success = false;

    //Check for insert or update
    $permission_id = $this->manage_model->check_insert_or_update_permissions($user_id);
    if ($permission_id) {
      //Update the record
      if ($this->manage_model->update_permission_record($permission_id, $column_name)) {
        $success = true;
      }
    } else {
      //Insert the record
      $permission = array('user_id' => $user_id, $column_name => 1);
      if ($this->manage_model->insert_permission_record($permission)) {
        $success = true;
      }
    }

    if ($success) {
      $data['result'] = 1;
      $data['message'] = 'Permission was updated successfully';
      $allowed = $this->manage_model->get_permission($user_id, $column_name);
      if ($allowed) {
		$data['base_url']=base_url();
        $data['html'] = $this->load->view( 'permissions_active', $data, true ); 
      } else {
		$data['base_url']=base_url();
        $data['html'] = $this->load->view( 'permissions_inactive', $data, true );
      }
    } else {
      $data['result'] = 0;
      $data['message'] = 'Sorry, There was an error while updating the permission.';
    }
    echo json_encode($data);
  }

  function role_toggle() {
  $result = false;
  $this->load->model('User');
  $id = $this->input->post('user');
  $role_id = $this->input->post('role');
  if(isset($id)&& isset($role_id))
  {
  $roles = array(
            'role_id' => $role_id,
        );
  $result = $this->User->update($roles,$id);
  }
 echo json_encode($result);
  }

    function role_permissions() {
    $this->load->helper('time_ago');
	$this->load->model('Role');
	
    $per_page = $this->session->userdata('cfgpagelimit');

    $data['user_name'] = $this->session->userdata('fullname');
    $limit = 20;
	$sortby['id'] = 'Asc';

	
$roles = $this->Role->getAll($limit, $sortby);
$data['roles'] = $roles;
	/*print "<pre>";
	
	print_r($roles);
	exit;
*/
    $action = $this->uri->segment(3, 0);
    $search_term = '';
    if ($action == 'results') {
      $search_term = $this->uri->segment(4, 0);
    }

    if (!empty($search_term)) {
      $config['base_url'] = site_url("manage/permissions/results/" . $search_term);
      $config['uri_segment'] = 5;
    } else {
      $config['base_url'] = site_url("manage/permissions/page/");
      $config['uri_segment'] = 4;
    }

    $data['permission_details'] = $this->manage_model->get_admins_and_permissions($per_page, $this->uri->segment($config['uri_segment'], 0), $search_term);


	
    //echo '<pre>'.print_r($data['permission_details'],1).'</pre>';
    //Configuration for pagination libarary
    $config['total_rows'] = 6;
    $config['per_page'] = $per_page;
    $config['num_links'] = 2;
    $config['prev_link'] = "&laquo;";
    $config['next_link'] = "&raquo;";
    $config['first_link'] = "&#171;&#171;";
    $config['last_link'] = "&#187;&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li class='active'>";
    $config['cur_tag_close'] = "</li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $this->load->view('role_permissions', $data);
  }
    function roll_permissions_toggle() {
	$this->load->model('Role');
    $column_name = $this->uri->segment(3, 0);
    $role_id = $this->uri->segment(4, 0);

    $success = false;

    //Check for insert or update

      //Update the record
      if ($this->Role->change_permission($role_id, $column_name)) {
        $success = true;
      }

    if ($success) {
      $data['result'] = 1;
      $data['message'] = 'Permission was updated successfully';
      $allowed = $this->Role->get_permission($role_id, $column_name);
      if ($allowed) {
		$data['base_url']=base_url();
        $data['html'] = $this->load->view( 'permissions_active', $data, true ); 
      } else {
		$data['base_url']=base_url();
        $data['html'] = $this->load->view( 'permissions_inactive', $data, true );
      }
    } else {
      $data['result'] = 0;
      $data['message'] = 'Sorry, There was an error while updating the permission.';
    }
    echo json_encode($data);
  }
  
  
  function make_user_as_admin() {
    $data = array();
    if (isset($_POST['make_user_as_admin'])) {
      $user_id = $this->input->post('user_id');
      if (!empty($user_id)) {
        if ($this->manage_model->make_admin($user_id)) {
          $this->session->set_flashdata('status_ok', 'User added as admin successfully');
        } else {
          $this->session->set_flashdata('status_error', 'Sorry, there was an error while adding the user as admin');
        }
      }
      redirect('manage/permissions');
    }
    $this->load->view('make_user_as_admin_view', $data);
  }

  function autocomplete_users_name() {
    $src_term = $this->input->post('term');
    $users = $this->manage_model->permissions_get_all_users_name($src_term);
    $json_result = array();

    foreach ($users as $user) {
      $user['id'] = $user['id'];
      $user['label'] = $user['full_name'];
      $json_result [] = $user;
    }
    if (count($json_result)) {
      $result = $json_result;
    } else {
      $result['id'] = 0;
      $result['label'] = 'No Results';
    }
    echo json_encode($result);
  }

  function searchuser() {

    if (isset($_POST['SearchText'])) {
      $search_term = $this->input->post('SearchText');
      $search_term = str_replace(' ', '-', $search_term);
      redirect('manage/permissions/results/' . $search_term);
    }
  }


  function remove_admin() {
    $ids = $_POST['sel'];
    if (count($ids)) {
      if ($this->manage_model->remove_admin($ids)) {
        $this->session->set_flashdata('status_del', 'Selected users have been removed from Admin category.');
      } else {
        $this->session->set_flashdata('status_error', 'Sorry, there was an error while removing the user(s)');
      }
    }
    redirect('manage/permissions');
  }
   function remove_role() {
$this->load->model('Role');
    $ids = $_POST['sel'];
    if (count($ids)) {
      if ($this->Role->remove_role($ids)) {
        $this->session->set_flashdata('status_del', 'Selected role have been removed.');
      } else {
        $this->session->set_flashdata('status_error', 'Sorry, there was an error while removing the role(s)');
      }
    }
    redirect('manage/role_permissions');
  }

  function results() {
    $module = $this->uri->segment(3, 0);
    $search_term = $this->uri->segment(4, 0);
    $review = $this->uri->segment(4, 0);
    $search_term = $search_term;
    $config['base_url'] = site_url("manage/results/" . $module . '/' . $search_term);
    $config['uri_segment'] = 6;
    $search_term = str_replace("%20"," ",$search_term);

    if (!empty($module) && !empty($search_term)) {
      $mt_category = $module;
	  if($module == 'group')
	  {
	  $data = $this->_general_group_data($mt_category, $search_term);
	 
	  }
	  elseif($module == 'concept')
	  {
	  $data = $this->_general_concept_data($mt_category, $search_term);
	  }
	  else
	  {
      $data = $this->_general_mt_data($mt_category, $search_term, $config,$review);
	  }
      //Active Navigation menu
      $data['nav']['active_news'] = '';
      $data['nav']['active_annoucements'] = '';
      $data['nav']['active_offers'] = '';
      $data['nav']['active_photos'] = '';
      $data['nav']['active_permissions'] = "";
	  $data['nav']['active_group'] = "";
	  $data['nav']['active_concept'] = "";
	  

      switch ($module) {
        case 'news':
          $data['nav']['active_news'] = '  class="active"';
          break;
        case 'announcement':
          $data['nav']['active_annoucements'] = '  class="active"';
          break;
          $data['nav']['active_offers'] = '  class="active"';
          break;
        case 'photos':
          $data['nav']['active_photos'] = '  class="active"';
          break;
        case 'permissions':
          $data['nav']['active_permissions'] = '  class="active"';
		case 'group':
			$data['nav']['active_group'] = '  class="active"';
		case 'concept':
			$data['nav']['active_concept'] = '  class="active"';
		break;
      }
 
	   if($module == 'group')
	  {
	  $data['delete_link']='delete_group';
      $this->load->view("list_template", $data);
	  }
	  elseif($module == 'concept')
	  {
	  $data['delete_link']='delete_concept';
      $this->load->view("list_template", $data);
	  }
	  else
	  {
	  $this->load->view("listing_template", $data);
	  }
    }
	
  }

  function preview_mt_entry() {
    $entry_category = $this->input->post('entry_category');
    $entry_title = $this->input->post('txt_title');
    $entry_textmore = $this->input->post('txt_excerpt');
	if($entry_category == 'photos')
	{
		 $entry_body = $this->input->post('alb_txt_body');
	}
	else
	{
		$entry_body = $this->input->post('txt_body');
	}
    $publish_status = $this->input->post('txt_publish');
    $button = $this->input->post('save');
    $album_id = $this->input->post('txt_album');
    $album_id = ($album_id > 0) ? $album_id : NULL;
    if ($entry_category) {
      $entry_array = array(
          'entry_id' => NULL,
          'entry_author_id' => $this->session->userdata('userid'),
          'entry_authored_on' => date('Y-m-d H:i:s'),
          'entry_basename' => url_title($entry_title, 'underscore', TRUE),
          'entry_created_by' => $this->session->userdata('userid'),
          'entry_created_on' => date('Y-m-d H:i:s'),
          'entry_excerpt' => $album_id,
          'entry_modified_on' => date('Y-m-d H:i:s'),
          'entry_status' => $publish_status,
          'entry_text' => $entry_body,
          'entry_text_more' => $entry_textmore,
          'entry_title' => $entry_title,
      );

      $data['entry'] = $entry_array;
      if ($album_id) {
        $this->load->model("about/about_model");
        $a_id = (int) $album_id;
        $album = $this->about_model->alb_get_album($a_id);
        if (count($album)) {
          $thumbnail = $album[0]['image_path'] . $album[0]['sub_dir'] . $album[0]['filepath'] . $album[0]['thumb'] . $album[0]['filename'];
          if (!@fopen($thumbnail, "r")) {
            $thumbnail = base_url() . "images/album.jpg";
          }
          $thumbnail = $thumbnail;
          $link = base_url() . "about/view_photos/" . $album[0]['aid'];
          $title = $album[0]['title'];
          $ctime = date("d M, Y", $album[0]['ctime']);
          $total_pic = $album[0]['total_pic'];
		  $data['album'] = $album;
	  $data['link'] = $link;
	  $data['thumbnail'] = $thumbnail;
	  $data['title'] = $title;
	  $data['ctime'] = $ctime;
	  $data['total_pic'] = $total_pic;

        } else {
          $album_details = "";
        }
      } else {
        $album_details = '';
      }
	  
      $this->load->view('preview_mt', $data);
    }
  }


  function process_email_queue(){
    $this->load->library('email');
    $this->load->helper('smtp_config');


    $queue_list = $this->manage_model->email_queue_process();
    //echo '<xmp>'.print_r($queue_list,1).'</xmp>';
    //echo 'Safety on'; exit;

    

    if(count($queue_list)){
      $smtp_config = smtp_config();
      
      
      foreach($queue_list as $details){
        $sender_email = $details['from_email'];
        $sender_name = $details['from_name'];
        $email = $details['to_email'];
        $subject = $details['email_subject'];
        $message = $details['email_body'];
        $email_campaign_id = $details['email_campaign_id'];
        $id = $details['id'];

        $this->email->initialize($smtp_config);
        $this->email->set_newline("\r\n");


        $this->email->from($sender_email, $sender_name);
        
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($email);
        if ($this->email->send()) {
          $data = array(
              'id' => NULL,
              'to_email' => $email,
              'email_campaign_id' => $email_campaign_id,
              'sent_on' => date("Y-m-d H:i:s"),
          );
          $this->manage_model->clear_email_queue($id);
          $this->manage_model->notification_log($data);
          echo 'mail sent<br />';
        }else{
          $error_msg = $this->email->print_debugger();
          $this->manage_model->failed_email_log($id,$error_msg);
        }
      }
    }
  }
  //New Create Module
  function create_post(){
    $this->load->model('includes/general_model'); //General Module
    $mt_category = "news";
	   $offer_concepts="";
    $data['user_name'] = $this->session->userdata('fullname');
    $user_permission = $this->session->userdata('permissions');
    $user_id = $this->session->userdata('userid');
    $user_details = $this->moderate_model->get_user_details($user_id);
    if(count($user_details)){
      $concept_id = $user_details[0]['concept_id'];
    }
    //If concept Corporate then show offer posting dropdown
    if($concept_id == 5){
      $offer_concepts = $this->general_model->gn_get_concepts();
     
    }else{
      $offer_concepts = '';
    }

    $albums = $this->manage_model->get_album_list();
    
//TODO-- remove code dupilication here
    $territory = $this->manage_model->get_territory_names();
    $concepts = $this->manage_model->get_concept_names();
    $grades = $this->manage_model->get_grade_names();
    $album_upload_location = str_replace($_SERVER['DOCUMENT_ROOT'], '', FCPATH)."phpalbum/albums/";
    //$album_upload_location = base_url()."phpalbum/album/";
    $data['album_upload_location'] = $album_upload_location;
     $data['concept_id'] = $concept_id;
	$data['user_details'] = $user_details[0];
    $data['offer_concepts'] = $offer_concepts;
    $data['location'] = $territory;
    $data['cmb_location'] = $territory;
    $data['concepts'] = $concepts;
    $data['cmb_concepts'] = $concepts;
    $data['cmb_album_category'] = $this->manage_model->get_album_categories();
    $data['grades'] = $grades;
    $data['albums'] = $albums;
    $data['user_permissions'] = $user_permission;
    $this->load->view('create_template', $data);
  }
  function _send_email($email_details){

    $this->load->library('email');
    $this->load->helper('smtp_config');
    $smtp_config = smtp_config();
    $this->email->initialize($smtp_config);
    $this->email->set_newline("\r\n");
    $sender_email = 'intranet@landmarkgroup.com';
    $sender_name = 'Landmark Intranet';

    $this->email->from($sender_email, $sender_name);
    $this->email->subject($email_details['title']);
    $this->email->message($email_details['body']);

    $this->email->to($email_details['to_email']);
	$this->email->bcc('kunal.patil@intelliswift.co.in, tanushri.kanchan@intelliswift.co.in'); 
    return $this->email->send();
  }
  
  function admin()
  {
	$data = array (
                  'email' => $this->config->item('ga_email'),
                  'password'  => $this->config->item('ga_password')   
               );
	$this->load->library('gapi',$data);
	$ga= $this->gapi->requestReportData($this->config->item('ga_profile_id'),array('browser','browserVersion'),array('pageviews','visits'));
	$data['pageviews']=$this->gapi->getPageviews();
	$data['visits']=$this->gapi->getVisits(); 
	$data['updated']=$this->gapi->getUpdated();
	$data['nav']['active_news'] = '';
	$data['nav']['active_annoucements'] = '';
	$data['nav']['active_offers'] = '';
	$data['nav']['active_photos'] = '';
	$data['nav']['active_permissions'] = "";
	$data['nav']['active_analytics'] =' class="active"';
	$data['page_title'] = ucfirst(strtolower("Analytics")); //Page Title
	$data['user_name'] = $this->session->userdata('fullname');
	$this->load->model('User');
	$data['intranet_users'] = $this->User->countAll();
	$data['active_users'] = $this->User->getActivUser();
	$data['online_users'] = $this->User->getOnlineUser();
	$this->load->model('Concept');
	$data['concept'] = $this->Concept->countAll();
	$this->load->model('Group');
	$data['group'] = $this->Group->countAll();
	$this->load->model('File');
	$data['file'] = $this->File->countAll();
	$this->load->model('Groupuser');
	$data['groupuser'] = $this->Groupuser->countAll();

	$this->load->view("analytics_template", $data);
  }
   
   function group() {
  
    $mt_category = "group";
	$this->load->model('Group');
	$data['list_data'] = $this->Group->getAll();
	$this->Group->countAll();
    $data = $this->_general_group_data($mt_category);
	$data['delete_link']='delete_group';
	$data['page_type']='groups';

    //Active Navigation menu
    $data['nav']['active_news'] = '';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
	$data['nav']['active_group'] =' class="active"';
	//print "<pre>";
	///print_r($data);
	//exit;
    $this->load->view("list_template", $data);
  }
  
  function concept() {
  
    $mt_category = "concept";
	$this->load->model('Concept');
	$data['list_data'] = $this->Concept->getAll();
	$this->Concept->countAll();
    $data = $this->_general_concept_data($mt_category);
	$data['delete_link']='delete_concept';
	$data['page_type']='concepts';
    //Active Navigation menu
    $data['nav']['active_news'] = '';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
	$data['nav']['active_group'] =' class="active"';
    $this->load->view("list_template", $data);
  }
  
function _general_group_data($mt_category, $query = '', $oConfig = '',$review='') {
  $this->load->helper('time_ago');
	$this->load->model('Group');
	$this->load->model('User');
	$this->load->model('GroupConceptPermission');
  $mt_category = $mt_category;
  $data = array();
  $data['mt_category'] = $mt_category;
  $data['page_title'] = ucfirst(strtolower($mt_category)); //Page Title
  if($query == 'show_review'){
    $query ='';
  }
  $data['user_name'] = $this->session->userdata('fullname');
  if($query != ''){
    //$db_cat_count = $this->manage_model->get_review_count_mt_entries($mt_category, $query);
  $db_cat_count = $this->Group->countAllSearch($query);
  }else{
    $db_cat_count = $this->Group->countAll();
  }
  $db_cat_count = $db_cat_count;
    
    
  $this->load->library('pagination');
  if (empty($oConfig)) {
    $config['base_url'] = site_url("manage/" . $mt_category);
    $config['uri_segment'] = 3;
  } else {
    $config['base_url'] = $oConfig['base_url'];
    $config['uri_segment'] = $oConfig['uri_segment'];
  }
  $config['total_rows'] = $db_cat_count;
  $config['per_page'] = $this->session->userdata('cfgpagelimit');
  $config['num_links'] = 2;
  $config['prev_link'] = "&laquo;";
  $config['next_link'] = "&raquo;";
  $config['first_link'] = "&#171;&#171;";
  $config['last_link'] = "&#187;&#187;";
  $config['first_tag_open'] = "<li>";
  $config['first_tag_close'] = "</li>";
  $config['last_tag_open'] = "<li>";
  $config['last_tag_close'] = "</li>";
  $config['num_tag_open'] = "<li>";
  $config['num_tag_close'] = "</li>";
  $config['cur_tag_open'] = "<li class='active'>";
  $config['cur_tag_close'] = "</li>";
  $config['next_tag_open'] = "<li>";
  $config['next_tag_clos'] = "</li>";
  $config['prev_tag_open'] = "<li>";
  $config['prev_tag_clos'] = "</li>";

  $data['total_count'] = $db_cat_count;

  $data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
  $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);
  $this->pagination->initialize($config);
  $data['navlinks'] = $this->pagination->create_links();
  
  
  if($review == 'show_review'){
    //$db_data = $this->manage_model->get_all_review_mt_entries($mt_category, $config['per_page'], $this->uri->segment($config['uri_segment'], 0), $query);
  }else{
    //$db_data = $this->manage_model->get_all_mt_entries($mt_category, $config['per_page'], $this->uri->segment($config['uri_segment'], 0), $query);
    $db_data = $this->Group->getAll($config['per_page'], $sortby = null ,$this->uri->segment($config['uri_segment'], 0),$query);
  }
  
  
  //Customizing the Database field
  /* foreach ($db_data as $key => $row) {
  $user_id=intval($row->created_by);
    $db_data[$key]->user_name = $this->User->getFullName($user_id);
  }*/
  $data['list_data'] = $db_data;
  foreach($data['list_data'] as $key => $group)
  {
  $gp= array();
  $group_data= $this->GroupConceptPermission->getGroupUserPer(intval($group->id));
  foreach($group_data as $group)
    {
    $gp[]=intval($group->user_id);
    }
    if(isset($gp[0]))
    {
    $group_user=$this->GroupConceptPermission->getGroupUser($gp);
    $arr = $group_user;
    $data['list_data'][$key]->manager = json_encode($arr);
    
    }
  }
  return $data;
}
	function _general_concept_data($mt_category, $query = '', $oConfig = '',$review='') 
	{
		$this->load->helper('time_ago');
		$this->load->model('Concept');
		$this->load->model('GroupConceptPermission');
		$mt_category = $mt_category;
		$data = array();
		$data['mt_category'] = $mt_category;
		$data['page_title'] = ucfirst(strtolower($mt_category)); //Page Title
		if($query == 'show_review'){
		  $query ='';
		}
		$data['user_name'] = $this->session->userdata('fullname');
		if($query != ''){
		  //$db_cat_count = $this->manage_model->get_review_count_mt_entries($mt_category, $query);
		  $db_cat_count = $this->Concept->countAllSearch($query);
		}else{
		  $db_cat_count = $this->Concept->countAll();
		}
		$db_cat_count = $db_cat_count;
		
		
		$this->load->library('pagination');
		if (empty($oConfig)) {
		  $config['base_url'] = site_url("manage/" . $mt_category);
		  $config['uri_segment'] = 3;
		} else {
		  $config['base_url'] = $oConfig['base_url'];
		  $config['uri_segment'] = $oConfig['uri_segment'];
		}
		$config['total_rows'] = $db_cat_count;
		$config['per_page'] = $this->session->userdata('cfgpagelimit');
		$config['num_links'] = 2;
		$config['prev_link'] = "&laquo;";
		$config['next_link'] = "&raquo;";
		$config['first_link'] = "&#171;&#171;";
		$config['last_link'] = "&#187;&#187;";
		$config['first_tag_open'] = "<li>";
		$config['first_tag_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = "</li>";
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'>";
		$config['cur_tag_close'] = "</li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tag_clos'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tag_clos'] = "</li>";

		$data['total_count'] = $db_cat_count;

		$data['limit'] = ($this->uri->segment($config['uri_segment'], 0)) ? $this->uri->segment($config['uri_segment'], 0) : 1;
		$data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment($config['uri_segment'], 0) + $config['per_page']);
		$this->pagination->initialize($config);
		$data['navlinks'] = $this->pagination->create_links();
		
		
		if($review == 'show_review'){
		  //$db_data = $this->manage_model->get_all_review_mt_entries($mt_category, $config['per_page'], $this->uri->segment($config['uri_segment'], 0), $query);
		}else{
		  //$db_data = $this->manage_model->get_all_mt_entries($mt_category, $config['per_page'], $this->uri->segment($config['uri_segment'], 0), $query);
		  $db_data = $this->Concept->getAll($config['per_page'], $sortby = null , $this->uri->segment($config['uri_segment'], 0), $query);
		}
		
		
		//Customizing the Database field
	   /*foreach ($db_data as $key => $row) {
	   $user_id=intval($row->created_by);
		  $db_data[$key]->user_name = $this->User->getFullName($user_id);
		}*/
		$data['list_data'] = $db_data;
		
		foreach($data['list_data'] as $key => $group)
		{
		$gp= array();
		$group_data= $this->GroupConceptPermission->getGroupUserPer(intval($group->id));
		foreach($group_data as $group)
			{
			$gp[]=intval($group->user_id);
			}
			if(isset($gp[0]))
			{
			$group_user=$this->GroupConceptPermission->getGroupUser($gp);
			$arr = $group_user;
			$data['list_data'][$key]->manager = json_encode($arr);
			
			}
		}
		

		return $data;
	}
  
  function post_group($group_id = NULL) {
	$group_id = $this->uri->segment(3, 0);
    $mt_category = "group";
	$entry_category = "group";
	$this->load->model('Group');
    $data['user_name'] = $this->session->userdata('fullname');
	if($this->input->post('txt_title')) 
	{
		$config['overwrite'] = FALSE;
		$config['encrypt_name'] = FALSE;
		$config['remove_spaces'] = TRUE;
		$config['upload_path'] = "images/groups/thumbs/"; // use an absolute path
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']   = '5000';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		if ( ! is_dir($config['upload_path']) ) {
		  mkdir($config['upload_path']);
		}
		$this->load->library('upload',$config);
		$image['file_name']='';
		if($_FILES['userfile']['name'] != "")
				{
					if (! $this->upload->do_upload('userfile'))
					{

						echo "UPLOAD ERROR ! ".$this->upload->display_errors();
						
					} 
					else 
					{
					
						$image=$this->upload->data();
					}
				}
		$entry_id = $this->input->post('group_id');
		$user_id = $this->session->userdata('userid');
		$entry_title = $this->input->post('txt_title');
		$entry_body = $this->input->post('txt_body');
		$entry_status = $this->input->post('txt_status');
		$entry_open =  $this->input->post('txt_open');
		
		if(isset($entry_id) && $entry_id != "") // Edit Action
		{
			
			if($image['file_name'] =='')
			{	
				$details =$this->Group->get(intval($entry_id));
				$image['file_name']=$details[0]->thumbnail;
			}
			$entry_array = array(
				  'name' => $entry_title,
				  'group_status' => $entry_status,
				  'description' => $entry_body,
				  'thumbnail' => $image['file_name'],
				  'is_open' => $entry_open,
				  'update_dttm' => date('Y-m-d H:i:s'),
			  );
			$this->Group->update($entry_array, $entry_id);
			$this->session->set_flashdata('status_ok', 'Group has been updated');
			redirect('manage/group');
			/*
			$status = 1;
			$result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
			$data['json'] = $result;
			$this->load->view("includes/print_json", $data);*/
			
		}
		else // Add Action
		{
			
      $user_id = $this->session->userdata('userid');
      $entry_title = $this->input->post('txt_title');
      $entry_body = $this->input->post('txt_body');
      $entry_status = $this->input->post('txt_status');
      $entry_open =  $this->input->post('txt_open');
				
			$entry_array = array(
				'name' => $entry_title,
				'created_by' => $user_id,
				'created_on' => time(),
				'group_status' => $entry_status,
				'description' => $entry_body,
				'thumbnail' => $image['file_name'],
				'total_members' => '',
				'is_open' => $entry_open,
				'create_dttm' => date('Y-m-d H:i:s'),
				'update_dttm' => date('Y-m-d H:i:s'),
			);
			$this->Group->add($entry_array);
			$this->session->set_flashdata('status_ok', 'Group has been added');
			redirect('manage/group');
			/*$status = 1;
			$result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
			$data['json'] = $result;
			$this->load->view("includes/print_json", $data);*/
			
		}
	}
	else
	{
		if (isset($group_id) && is_numeric($group_id) && $group_id > 0 ) {
			$details =$this->Group->get(intval($group_id));
			$details = (array) $details[0];
			$data['details']=$details;
			$this->load->view('post_group_template', $data);
		}
		else
		{
			$this->load->view('post_group_template', $data);
		}
	}
  }
  function load_group_ajax(){
    $this->load->model('Group');
    $group_id = $this->input->post('group_id');
    if(isset($group_id) && !empty($group_id))
    {
      $group_detail = $this->Group->get(intval($group_id));
      if(isset($group_detail) && !empty($group_detail))
      {
        $record['record'] = $group_detail[0];
        $record['status'] = true;
      }
      else
      {
        $record['status'] = false;
      }
    }
    else
    {
      $record['status'] = false;
    }
    $response['json'] = json_encode($record);
    echo $response['json'];
  }
  function post_group_ajax(){
    $this->load->model('Group');
	$this->load->model('GroupConceptPermission');
    $config['overwrite'] = FALSE;
		$config['encrypt_name'] = FALSE;
		$config['remove_spaces'] = TRUE;
		$config['upload_path'] = "images/groups/thumbs/"; // use an absolute path
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']   = '5000';
		
    $user_id = $this->session->userdata('userid');
    $entry_title = $this->input->post('txt_title');
    $entry_body = $this->input->post('txt_body');
    $entry_status = $this->input->post('txt_status');
    $entry_open = $this->input->post('txt_open');
    $group_id = $this->input->post('group_id');
    
    $success = array();
	$success['duplicate'] = false;
    if((isset($entry_title) && !empty($entry_title)))
    {
      if (!is_dir($config['upload_path'])) {
  		  mkdir($config['upload_path']);
  		}
  		$this->load->library('upload',$config);
  		$image['file_name']= '';
  		if((isset($_FILES['txt_thumbnail']['name']) && !empty($_FILES['txt_thumbnail']['name'])) && $_FILES['txt_thumbnail']['name'] != "")
  		{
  			if (!$this->upload->do_upload('txt_thumbnail'))
  			{
  
  				echo "UPLOAD ERROR ! ".$this->upload->display_errors();
  				
  			} 
  			else 
  			{
  			
  				$image = $this->upload->data();
  			}
  		}
      
      if(isset($group_id) && !empty($group_id))
      {
        if($image['file_name'] == '')
  			{	
  				$details =$this->Group->get(intval($group_id));
  				$image['file_name']=$details[0]->thumbnail;
  			}
  			$entry_array = array(
  				  'name' => $entry_title,
  				  'group_status' => $entry_status,
  				  'description' => $entry_body,
  				  'thumbnail' => $image['file_name'],
  				  'is_open' => $entry_open,
  				  'update_dttm' => date('Y-m-d H:i:s'),
  			  );
          $status = $this->Group->update($entry_array, $group_id);
          if($status)
          {
            $success['status'] = true;
            $success['reload'] = true;
          }
          else
          {
            $success['status'] = false;
          }
      }
      else
      {
		$group_exist = $this->Group->getIdByName($entry_title);
		if(isset($group_exist) && $group_exist > 0)
		{
			$success['status'] = false;
			$success['duplicate'] = true;
		
		}
		else{
		if($image['file_name'] == '')
  		{
			$image['file_name']= 'default/'.rand(1, 4).'.png';
		}
        $entry_array = array(
        	'name' => $entry_title,
        	'created_by' => $user_id,
        	'created_on' => time(),
        	'group_status' => $entry_status,
        	'description' => $entry_body,
        	'thumbnail' => $image['file_name'],
        	'total_members' => '',
        	'is_open' => $entry_open,
        	'create_dttm' => date('Y-m-d H:i:s'),
        	'update_dttm' => date('Y-m-d H:i:s'),
        );
        $last_id =  $this->Group->add($entry_array);
		
		//add user as group member and moderator
		$entry_array = array(
		'user_id' => $user_id,
		'object_type' => 'Group',
		'object_id' => $last_id,
		);
		$this->GroupConceptPermission->add($entry_array);
		
		$details['object_id'] = $user_id;
		$details['object_type'] = 'User';
		$details['parent_type'] = 'Group';
		$details['parent_id']  = $last_id;
		$data = $this->ObjectMember->getByFilters($details);
		if(isset($data) && is_array($data))
		{}
		else
		{
			$this->ObjectMember->followObject($details);
		}
		
		$success['group_id'] = $last_id;
		$success['reload'] = false;
        if(is_numeric($last_id))
        {
          $success['status'] = true;
        }
        else
        {
          $success['status'] = false;
        }
		}
      }
    }
    else
    {
      $success['status'] = false;
    }
    $response['json'] = json_encode($success);
    echo $response['json'];
  }
	function post_concept($concept_id = NULL) {
	$this->load->library('image_lib');
		$concept_id = $this->uri->segment(3, 0);
		$mt_category = "Consept";
		$entry_category = "concept";
		$this->load->model('Concept');
		$data['user_name'] = $this->session->userdata('fullname');
		if($this->input->post('txt_title')) 
		{
			$config['overwrite'] = FALSE;
			$config['encrypt_name'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['upload_path'] = "./images/concepts/thumbs/"; // use an absolute path
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']   = '5000';
			//$config['max_width']  = '1024';
			//$config['max_height']  = '768';
			if ( ! is_dir($config['upload_path']) ) die("THE UPLOAD DIRECTORY DOES NOT EXIST");
			$this->load->library('upload',$config);
			$image['file_name']='';
			$image_icon['file_name']='';
			if($_FILES['userfile']['name'] != "")
					{
						if (! $this->upload->do_upload('userfile'))
						{

							echo "UPLOAD ERROR ! ".$this->upload->display_errors();
							
						} 
						else 
						{
						
							$image=$this->upload->data();
							if($image['image_width'] > 220 )
							{
									$config1['image_library'] = 'gd2';
									$config1['source_image']	= $image['full_path'];
									$config1['maintain_ratio'] = false;
									$config1['width']	 = 220;
									$config1['height']	= 220;
									$this->image_lib->initialize($config1);
									$this->image_lib->resize();
									$this->image_lib->clear();
							}
						}
					}
					
			$entry_id = $this->input->post('concept_id');
			$user_id = $this->session->userdata('userid');
			$entry_title = $this->input->post('txt_title');
			$entry_body = $this->input->post('txt_body');
			$entry_status = $this->input->post('txt_status');
			$entry_open =  $this->input->post('txt_open');

			if(isset($entry_id) && $entry_id != "") // Edit Action
			{
				if($image['file_name'] =='')
				{	
					$details =$this->Concept->get(intval($entry_id));
					$image['file_name']=$details[0]->thumbnail;
				}
				if($image_icon['file_name'] =='')
				{	
					$details =$this->Concept->get(intval($entry_id));
					$image_icon['file_name']=$details[0]->thumbnail;
				}
				
				$entry_array = array(
					'name' => $entry_title,
					'status' => $entry_status,
					'description' => $entry_body,
					'thumbnail' => $image['file_name'],
					'icon' => '',
					'is_open' => $entry_open,
					'update_dttm' => date('Y-m-d H:i:s'),
				);
				$this->Concept->update($entry_array, $entry_id);
				$this->session->set_flashdata('status_ok', 'Concept has been updated');
				redirect('manage/concept');
				/*$status = 1;
				$result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
				$data['json'] = $result;
				$this->load->view("includes/print_json", $data);*/
				
			}
			else // Add Action
			{

					if($image['file_name'] == '')
					{
						$image['file_name'] = $this->input->post('default_image');
					
					}
				$user_id = $this->session->userdata('userid');
				$concept_slug = url_title($entry_title, '-', TRUE); 
				$count_concept_slug = $this->Concept->getConceptBySlug($concept_slug,$user_id);	
				$i = 0;
				$concept_slug_new = $concept_slug;
					while(!empty($count_concept_slug)) {
					$concept_slug_new = $concept_slug . '-' . $i;
					$count_concept_slug = $this->Concept->getConceptBySlug($concept_slug_new,$user_id);	
					$i++;
				}
				$concept_slug = $concept_slug_new;

				$entry_array = array(
					'name' => $entry_title,
					'description' => $entry_body,
					'thumbnail' => $image['file_name'],
					'icon' => '',
					'slug' => $concept_slug,
					'is_open' => $entry_open,
					'create_dttm' => date('Y-m-d H:i:s'),
					'update_dttm' => date('Y-m-d H:i:s'),
					'status' => $entry_status,
				);
				$this->Concept->add($entry_array);
				$this->session->set_flashdata('status_ok', 'Concept has been added');
				redirect('manage/concept');
				/*$status = 1;
				$result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
				$data['json'] = $result;
				$this->load->view("includes/print_json", $data);*/
				
			}
		}
		else
		{
			if (isset($concept_id) && is_numeric($concept_id) && $concept_id > 0 ) {
				$details =$this->Concept->get(intval($concept_id));
				$details = (array) $details[0];
				$data['details']=$details;
				$this->load->view('post_concept_template', $data);
			}
			else
			{
				$this->load->view('post_concept_template', $data);
			}
		}
	}
  
	
	function post_concept_ajax() {
		$this->load->library('image_lib');
		$this->load->model('Concept');
		$data['user_name'] = $this->session->userdata('fullname');
		if($this->input->post('txt_title')) 
		{
			$config['overwrite'] = FALSE;
			$config['encrypt_name'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['upload_path'] = "./images/concepts/thumbs/"; // use an absolute path
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']   = '5000';
			//$config['max_width']  = '1024';
			//$config['max_height']  = '768';
			if ( ! is_dir($config['upload_path']) ) die("THE UPLOAD DIRECTORY DOES NOT EXIST");
			$this->load->library('upload',$config);
			$image['file_name']='';
			$image_icon['file_name']='';
			if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "")
					{
						if (! $this->upload->do_upload('userfile'))
						{

							echo "UPLOAD ERROR ! ".$this->upload->display_errors();
							
						} 
						else 
						{
						
							$image = $this->upload->data();
							if($image['image_width'] > 220 )
							{
									$config1['image_library'] = 'gd2';
									$config1['source_image']	= $image['full_path'];
									$config1['maintain_ratio'] = false;
									$config1['width']	 = 220;
									$config1['height']	= 220;
									$this->image_lib->initialize($config1);
									$this->image_lib->resize();
									$this->image_lib->clear();
							}
						}
					}
					if(isset($_FILES['icon']['name'] ) && $_FILES['icon']['name'] != "")
					{
						
						if (! $this->upload->do_upload('icon'))
						{

							echo "UPLOAD ERROR ! ".$this->upload->display_errors();
							
						} 
						else 
						{
						
							$image_icon=$this->upload->data();
							
						}
					}
			$entry_id = $this->input->post('concept_id');
			$user_id = $this->session->userdata('userid');
			$entry_title = $this->input->post('txt_title');
			$entry_body = $this->input->post('txt_body');
			$entry_status = $this->input->post('txt_status');
			$entry_open =  $this->input->post('txt_open');
			$entry_email = $this->input->post('txt_email');
			$entry_phone =  $this->input->post('txt_phone');

			if(isset($entry_id) && $entry_id != "") // Edit Action
			{
				if($image['file_name'] =='')
				{	
					$details =$this->Concept->get(intval($entry_id));
					$image['file_name']=$details[0]->thumbnail;
				}
				if($image_icon['file_name'] =='')
				{	
					$details =$this->Concept->get(intval($entry_id));
					$image_icon['file_name']=$details[0]->thumbnail;
				}
				
				$entry_array = array(
					'name' => $entry_title,
					'email' => $entry_email,
					'phone' => $entry_phone,
					'description' => $entry_body,
					'thumbnail' => $image['file_name'],
					'icon' => '',
					'update_dttm' => date('Y-m-d H:i:s'),
				);
				$this->Concept->update($entry_array, $entry_id);
				/* $this->session->set_flashdata('status_ok', 'Concept has been updated');
				redirect('manage/concept'); */
				
				$success['status']= 'yes';
				
				/*$status = 1;
				$result = '{"status": "' . $status . '","redirect" : "' . $entry_category . '"}';
				$data['json'] = $result;
				$this->load->view("includes/print_json", $data);*/
				
			}
			else
			{
			$success['status']= 'no';
			}
			
		}
		else
		{
			$success['status']= 'no';
		}
		
		$temp2['json'] = json_encode($success);
	 echo $temp2['json'];

	}
	
  
	function delete_group() {
	if($this->input->post('Search'))
	{
		if($this->input->post('SearchText') && trim($this->input->post('SearchText')) !="")
		{

			$search = $this->input->post('Search');
			$mt_category = $this->input->post('mt_category');
			$show_review = $this->input->post('show_review');
			$this->session->set_flashdata('show_review', $show_review);

			if (!empty($search)) {
				$query = $this->input->post('SearchText');
				if(strtolower($query) == 'search '.$mt_category){
					$query = '';   
				}
			}
			if (!empty($query) || !empty($show_review)) {
				if(empty($query)){
					$query = 'show_review';
				}
				$this->session->set_flashdata('SearchText', $query);
				redirect('manage/results/' . $mt_category . '/' . $query.'/'.$show_review);
			}
		}
		else
		{
			redirect('manage/group');
		}
	}
	else
	{
		//Delete the Groups
		$selected_ids = $this->input->post("sel");
		$this->load->model('Group');
    
		foreach($selected_ids as $id)
		{
			$data['group_status'] = '2';
			$this->Group->update($data, $id);
		}
    if ($selected_ids) {
      $this->session->set_flashdata('status_ok', 'Selected groups have been deleted successfully.');
    } else {
      $this->session->set_flashdata('status_ok', '');
    } 
		redirect($_SERVER['HTTP_REFERER']);
		
	}
  }
  
  
   function delete_concept() {
	if( $this->input->post('Search'))
	{
		if($this->input->post('SearchText') && trim($this->input->post('SearchText')) !="")
		{
			$search = $this->input->post('Search');
			$mt_category = $this->input->post('mt_category');
			$show_review = $this->input->post('show_review');
			$this->session->set_flashdata('show_review', $show_review);

			if (!empty($search)) {
			$query = $this->input->post('SearchText');
			if(strtolower($query) == 'search '.$mt_category){
			$query = '';   
			}
			}
			if (!empty($query) || !empty($show_review)) {
			if(empty($query)){
			$query = 'show_review';
			}
			$this->session->set_flashdata('SearchText', $query);
			redirect('manage/results/' . $mt_category . '/' . $query.'/'.$show_review);
			}
		}
		else
		{
		redirect('manage/concept');
		}
	}
	else
	{

		//Delete the Concepts
		$selected_ids = $this->input->post("sel");
		$this->load->model('Concept');

		foreach($selected_ids as $id)
		{
		$data['status'] = 'inactive';
		$this->Concept->update($data, $id);
		}
		$this->session->set_flashdata('status_ok', 'Selected concepts have been deleted successfully.'); 
		redirect($_SERVER['HTTP_REFERER']);
	}
	
  }
  function concept_permissions()
  {
	
		$this->load->model('GroupConceptPermission');
		$filters = array(
		'user_id' =>$this->input->post('user_id'),
		'object_type' => 'Concept',
		);
		$this->GroupConceptPermission->remove_all_permissions($filters);
		
		foreach($this->input->post('concept') as $concept)
		{
			$entry_array = array(
			'user_id' => $this->input->post('user_id'),
			'object_type' => 'Concept',
			'object_id' => $concept,
			);
			$this->GroupConceptPermission->add($entry_array);
		}
		redirect($_SERVER['HTTP_REFERER']);
  
  }
  
	function group_permissions()
	{ 
		$this->load->model('GroupConceptPermission');
		if($this->input->post('blah') != "")
		{
		$filters = array(
		'user_id' =>$this->input->post('user_id'),
		'object_type' => 'Group',
		);
		$this->GroupConceptPermission->remove_all_permissions($filters);
		$groups = explode(",", $this->input->post('blah'));
		
		foreach($groups as $group)
		{
			$entry_array = array(
			'user_id' => $this->input->post('user_id'),
			'object_type' => 'Group',
			'object_id' => $group,
			);
			$this->GroupConceptPermission->add($entry_array);
		}
		
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
 
	function auto_concept()
	{
		$this->load->model('Concept');
		$this->load->model('GroupConceptPermission');
		if(isset($_GET["q"]))
		{
			$name = mysql_real_escape_string($_GET["q"]);
		}
		else
		{
			$name='';
		}
		$limit = 10;
		$obj = $this->GroupConceptPermission->getAllConcept($limit , $name);
		 $arr[] = $obj;
		# JSON-encode the response
		$json_response = json_encode($obj);
		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
		$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		# Return the response
		echo $json_response;
	}
	function auto_group()
	{
		$this->load->model('GroupConceptPermission');
		if(isset($_GET["q"]))
		{
			$name = mysql_real_escape_string($_GET["q"]);
		}
		else
		{
			$name='';
		}
		$limit = 10;
		$obj = $this->GroupConceptPermission->getAllGroup($limit , $name);
		$arr[] = $obj;
		# JSON-encode the response
		$json_response = json_encode($obj);
		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		# Return the response
		echo $json_response;
	}
  
	function auto_users()
	{
		$this->load->model('GroupConceptPermission');
		if(isset($_GET["q"]))
		{
			$name = mysql_real_escape_string($_GET["q"]);
		}
		else
		{
			$name='';
		}
		$limit = 10;
		$obj = $this->GroupConceptPermission->getAllUser($limit , $name);
		$arr[] = $obj;
		foreach($obj as $key => $user)
		{
			$obj[$key]->name =$user->first_name." ".$user->last_name;
			unset($obj[$key]->last_name);
			unset($obj[$key]->first_name);
		}

		# JSON-encode the response
		$json_response = json_encode($obj);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support

		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function auto_conept_managers()
	{
		$this->load->model('GroupConceptPermission');
		if(isset($_GET["q"]))
		{
			$name = mysql_real_escape_string($_GET["q"]);
		}
		else
		{
			$name='';
		}
		$limit = 10;
		$obj = $this->GroupConceptPermission->get_conept_managers($limit , $name);
		$arr[] = $obj;
		foreach($obj as $key => $user)
		{
			$obj[$key]->name =$user->first_name." ".$user->last_name;
			unset($obj[$key]->last_name);
			unset($obj[$key]->first_name);
		}

		# JSON-encode the response
		$json_response = json_encode($obj);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support

		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
  function auto_nongroup_users()
	{
		$this->load->model('GroupConceptPermission');
		if(isset($_GET["q"]))
		{
			$name = mysql_real_escape_string($_GET["q"]);
		}
		else
		{
			$name='';
		}
    $group = $this->input->get('group_id');    
		$limit = 10;
		$obj = $this->GroupConceptPermission->getAllUser($limit , $name);
    $groupMember = $this->ObjectMember->getMembers('Group', $group);
    $groupMem = array();
    foreach($groupMember as $member)
    {
      $groupMem[] = $member['user_id'];
    }
    $arr[] = $obj;
		foreach($obj as $key => $user)
		{
		  if(!in_array($user->id,$groupMem))
      {
  			$obj[$key]->name =$user->first_name." ".$user->last_name;
  			unset($obj[$key]->last_name);
  			unset($obj[$key]->first_name);
      }
		}

		# JSON-encode the response
		$json_response = json_encode($obj);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support

		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
  function concept_group_managers()
  { 

		$this->load->model('GroupConceptPermission');
		$filters = array(
		'object_id' =>$this->input->post('group_id'),
		'object_type' => ucwords(strtolower($this->input->post('object_type'))),
		);
		$this->GroupConceptPermission->remove_all_permissions($filters);
		if($this->input->post('blah') != "")
		{
		
		$managers = explode(",", $this->input->post('blah'));
		
		foreach($managers as $manager)
		{
			$entry_array = array(
			'user_id' => $manager,
			'object_type' => ucwords(strtolower($this->input->post('object_type'))),
			'object_id' => $this->input->post('group_id'),
			);
			$this->GroupConceptPermission->add($entry_array);
			
			$details['object_id'] = $manager;
			$details['object_type'] = 'User';
			$details['parent_type'] = ucwords(strtolower($this->input->post('object_type')));
			$details['parent_id']  = $this->input->post('group_id');
			$data = $this->ObjectMember->getByFilters($details);
			if(isset($data) && is_array($data))
			{}
			else
			{
				$this->ObjectMember->followObject($details);
			}
		}
		
		}
		redirect($_SERVER['HTTP_REFERER']);
  }
  function group_invite_member(){
    $response = array();
    $memberInvited = array();
    $this->load->model('Invite');
    $this->load->model('Activities');
    $moderator_id = $this->input->post('moderator_id');
    $members = $this->input->post('invite_members');
    $group_id = $this->input->post('group_id');
    
    if(isset($members) && !empty($members))
    {
      $membersArr = explode(",",$members);
      $success = true;
      $memberExist = array();
      foreach($membersArr as $member)
  		{
  		  $invitedMember = $this->Invite->fetchInvitationByUserGroup($member,$group_id);
        if($invitedMember !== false)
        {
          $entry_array = array(
      			'add_date' => date('Y-m-d',time()),
    			);
          if($this->Invite->update($entry_array,$invitedMember['id']) )
          {
            $notification_html = '<a href="'.site_url().'groups/'.$this->input->post('group_id').'">'.ucfirst($this->input->post('group_name')).'</a>';
            $notification = array(
              'object_id' => $member,
              'object_type' => 'User',
              'parent_id' => $moderator_id,
              'parent_type' => 'User',
              'activity_type' => 'Invitation',
              'dttm' => date('Y-m-d H:i:s',time()),
              'status' => 'unviewed',
              'notification' => $notification_html,
            );
            $this->Activities->groupUpdateNotification($notification);
          }
          else
          {
            $success = false;
          }
        }
        else
        {
         	$entry_array = array(
      			'invite_to' => $member,
            'invite_type' => 'User',
      			'object' => 'Group',
      			'object_id' => $this->input->post('group_id'),
            'add_date' => date('Y-m-d',time()),
    			);
    			$last_id = $this->Invite->add($entry_array);
          $notification_html = '<a href="'.site_url().'groups/'.$this->input->post('group_id').'">'.ucfirst($this->input->post('group_name')).'</a>';
          if(is_numeric($last_id))
          {
            $notification = array(
              'object_id' => $member,
              'object_type' => 'User',
              'parent_id' => $moderator_id,
              'parent_type' => 'User',
              'activity_type' => 'Invitation',
              'dttm' => date('Y-m-d H:i:s',time()),
              'status' => 'unviewed',
              'notification' => $notification_html,
            );
            $this->Activities->groupUpdateNotification($notification);
          }
          else
          {
            $success = false;
          }
        }
  		}
      if($success == true)
      {
        $response['status'] = true;
      }
      else
      {
        $response['status'] = false;
      }
    }
    else
    {
      $response['status'] = false;
    }
    $result['json'] = json_encode($response);
    $this->load->view("print_json", $result);
  }
  function group_member_autocomplete()
  {
    $this->load->model('ObjectMember');
    $text = $this->input->get('q');
    $group_id = $this->input->get('group_id');             
    if(isset($text))
		{
			$name = mysql_real_escape_string($_GET["q"]);
		}
		else
		{
			$name='';
		}
		$limit = 10;
    $obj = $this->ObjectMember->getMembersByFilter('Group',$group_id,$name);
    $arr[] = $obj;
		foreach($obj as $key => $user)
		{
			$obj[$key]->name =$user->first_name." ".$user->last_name;
			unset($obj[$key]->last_name);
			unset($obj[$key]->first_name);
		}

		# JSON-encode the response
		$json_response = json_encode($obj);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support

		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
  }
  
   public function widget_sort() {
   $widgets = $this->input->post('widgets');
   if(!empty($widgets))
   {
	foreach($widgets as $key => $value)
	{
		$data = array(
		'weight' => $key);
		$this->Widget->update($data, $value);
	}
	print "<pre>";
	print_r($this->input->post('widgets'));
	exit;
   }
   }

   function stats() {
    //built for concept stats
    $data['nav']['active_news'] = '';
    $data['nav']['active_annoucements'] = '';
    $data['nav']['active_offers'] = '';
    $data['nav']['active_photos'] = '';
    $data['nav']['active_permissions'] = "";
    $data['nav']['active_analytics'] =' class="active"';
    $data['user_name'] = $this->session->userdata('fullname');
    $data['page_title'] = ucfirst(strtolower("Analytics"));

    $this->load->model('Stats');

    $concepts_db = $this->Stats->getConceptStats();

    $concepts_users = $this->Stats->getConceptUserStats();

    /*echo '<pre>';
    print_r($concepts_db);
*/
    $data['start_date'] = '2014-02-23';

    $data['end_date'] = date('Y/m/d');

    $start = date('Y/m/d', strtotime($data['start_date']));

    $months = array();

    while($start <= $data['end_date'])
    {
      $months[] = date('F, Y', strtotime($start));
      //echo $start;
      $start = date('Y/m/d', strtotime('+1 month', strtotime($start)));
      /*echo $start.'<br>';*/

    }

    $concepts = array();

    foreach($concepts_db as $key => $concept)
    {
      $concepts[$concept['concept_name']]['stats'][$concept['month'].', '.$concept['year']] = $concept['posts'];
    }


    $data['concepts'] = $concepts;

    /*echo '<pre>';
    print_r($concepts);
    die();*/

    /*$data['start_month'] = date('Ym', strtotime($data['start_date']));
    
    $data['end_month'] = date('Ym', strtotime($data['end_date']));*/

    $data['months'] = $months;

    //print_r($data['concepts']);
    //die();

    $this->load->view('concept_analytics', $data);
   }

   
   
   function weekly_reports() {
    
    $user_id = $this->session->userdata("userid");
    $profile = $this->User->getUserProfile($user_id);
    $this->load->model('UserPermission');
    if($profile['role_id'] != 1) {
      redirect(site_url());
    }
    if (!$user_id) {
      
      redirect(site_url());
    }
    $form_data = $this->input->post(NULL,TRUE);
    
    $sdate = '';
    $edate = '';
    if(!empty($form_data)) {
      $startdate = $this->input->post('start-datepicker',TRUE);
      $enddate = $this->input->post('end-datepicker',TRUE);
      
      if(!empty($startdate)) {
        $sdate = date("Y-m-d", strtotime($startdate));
        $data['sdate'] = $sdate;
      }
      
      if(!empty($enddate)) {
        $edate = date("Y-m-d", strtotime($enddate));
        $data['edate'] = $edate;
      }
      if(empty($sdate) && (!empty($edate))) {
        $data['error'] = 'Please select the starting date';
      }
      if(!empty($sdate) && ( $sdate < $edate ) ) {
        $data['error'] = 'End Date should not be less than start date';
      }
      
    }else{
      // Show only 2014 Data as we are showing the count values weekwise
      $sdate = '2014-01-01';
      $edate = '2014-12-31'; 
    }
    
    //echo "<pre>";print_R($sdate);echo "\n";print_r($edate);echo "</pre>";
    $news_data = $this->manage_model->weekly_news_offer_announcement_count($sdate,$edate);
    $data['news_data'] = $news_data;
    
    $login_data = $this->manage_model->weekly_login_count($sdate,$edate);
    $data['login_data'] = $login_data;
    
    $status_data = $this->manage_model->weekly_status_count($sdate,$edate);
    $data['status_data'] = $status_data;
    
    $polls_data = $this->manage_model->weekly_polls_count($sdate,$edate);
    $data['polls_data'] = $polls_data;
    
    $files_uploaded = $this->manage_model->weekly_files_uploaded_count($sdate,$edate);
    $data['files_uploaded'] = $files_uploaded;    
    
    $comment_data = $this->manage_model->weekly_comments_count($sdate,$edate);
    $data['comment_data'] = $comment_data;
    
    $likes_data = $this->manage_model->weekly_likes_count($sdate,$edate);
    $data['likes_data'] = $likes_data;

    $uploaded_pictures_data = $this->manage_model->weekly_uploaded_pictures_count($sdate,$edate);
    $data['uploaded_pictures_data'] = $uploaded_pictures_data;
    
    $chat_msgs_data = $this->manage_model->weekly_chat_msgs_count($sdate,$edate);
    $data['chat_msgs_data'] = $chat_msgs_data;
    
    $market_place_item_data = $this->manage_model->weekly_market_place_item_count($sdate,$edate);
    $data['market_place_item_data'] = $market_place_item_data;
    
    $data['monthly_login_data'] = $this->manage_model->monthly_logins_count($sdate,$edate);
    
    $data['monthly_unique_login_data'] = $this->manage_model->monthly_unique_logins_count($sdate,$edate);
    $data['username'] = $this->session->userdata('fullname');
    
    
    $this->load->view('weekly_report',$data);
   }
   
   function save_csv_data() {
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="weeklyReport.csv"');
      if (isset($_POST['csv'])) {
      $csv = $_POST['csv'];
      echo $csv;
      }
   }

   function save_pdf_data() {
      header('Content-Type: application/pdf');
      header('Content-Disposition: attachment; filename="weeklyReport.pdf"');
      if (isset($_POST['csv'])) {
      $csv = $_POST['csv'];
      echo $csv;
      }
   }
   
   function fetch_concept_group_tags(){
    $data = array();
    $tags = array();
    $this->load->model('FileTag');
    $user_id = $this->session->userdata('userid');
    $concept_id = $this->input->post('concept_id');
    $group_id = $this->input->post('group_id');
    $tags = $this->FileTag->getConceptPerGroupTags($group_id,$concept_id,$user_id,false);
    $data['json'] = json_encode($tags);
    $this->load->view("print_json", $data);
   }
   
}

/*
$user_details = array(
				'skills' => $user_skill,
				'about' => $about_me,
				'display_name' =>$display_name
			);
			 $this->User->update($user_details, $user_id);
*/