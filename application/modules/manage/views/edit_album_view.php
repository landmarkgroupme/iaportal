<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/editabletext.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
    <style type="text/css">
    .thumb-span {background:#860e9c;padding:2px 4px;text-transform:uppercase;color:#fff;-moz-border-radius:4px; border-radius: 4px;font-weight:bold;}
    </style>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'photos';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Edit Album</h1>
        </div>
        <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
            <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
            <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
        <form action="<? echo site_url("manage/submit_edit_album/".$album_details['Album']['aid'])?>" method="post" enctype="multipart/form-data">
            <div  id="create_content" class="main-table form">
                <div class="form-title">Album Name <span id="error-title" class="error-msg"></span></div>
                <input type="text" name="album_name" id="txt_title" class="form-text" value="<?php echo $album_details['Album']['title'];?>" />

                <div class="form-title">Album Description</div>
                <textarea rows="6" cols="3" name="album_description" id="txt_excerpt" class="form-text"><?php echo $album_details['Album']['description'];?></textarea>

                <div class="form-title">Album Category</div>
                <div class="form-area">
                    <select name="album_category_id">
                        <?=$categories_list;?>
						<?php
						if (count($categories_list)):
						foreach ($categories_list as $category):
							if ($album_details['Album']['category'] == $category['cid']):
						?>
								<option selected="selected" value="<?=$category['cid']?>"><?=$category['name']?></option>
						<?php
							else:
						?>
								<option value="<?=$category['cid']?>"><?=$category['name']?></option>
						<?php
							endif;
						endforeach;
						endif;
						?>
                    </select>
                </div>

                <div class="form-title">Album Thumbnail</div>
                <div class="form-area">
                    <select name="picture_id">
					<option value="0">Select a thumbnail</option>
						<?php
						if (count($album_details['Pictures'])):
							foreach ($album_details['Pictures'] as $picture):
								if (!empty($picture['title'])):
									$option_text = $picture['title'];
								else:
									$option_text = $picture['filename'];
								endif;
								if ($picture['pid'] == $album_details['Album']['thumb']):
								?>
									<option selected="selected" value="<?=$picture['pid']?>"><?=$option_text?></option>
								<?php
								else:
								?>
									<option value="<?=$picture['pid']?>"><?=$option_text?></option>
								<?php
								endif;
							endforeach;
						endif;
						?>
                    </select>
                </div>

                <input name="album_id" type="hidden" value="<?php echo $album_details['Album']['aid'];?>" />
                <div class="clear"></div>
            </div>
            <div class="button-holder">
             <div style="float:right;">
                <input class="submit_entry ItemButtonDraft" name="save" type="submit" value="Save as draft" />
                <input id="submit_news" class="ItemButtonSave submit_entry" name="save" type="submit" value="Publish Now" />
             </div><div class="clear"></div>
            </div>
        </form>
        <div id="photos-in-album" style="margin-top:20px;">
            <h1>Photos (<?php echo $pictures_count;?>)</h1>
            <form method="POST" action="<?php echo site_url('manage/delete_photos/'.$album_details['Album']['aid']);?>">
                <table id="list-table" class="main-table" width="810" style="width:810px;" border="0" cellspacing="0">
                    <tr>
                        <th class="odd" colspan="6" style="margin-left:0px; padding-left:0px;">
                            <div class="ArrowButton"></div>
                            <div><input class="ItemButton" id="delete_item" name="Delete" type="submit" value="Delete" /></div>
                            <a href="<?php echo site_url('manage/add_photos/'.$album_details['Album']['aid']);?>" style="float:right;font-weight:bold;margin-top:5px"><img src="<?php echo base_url();?>images/ico-12.gif" style="float:left;margin-top:3px;margin-right:5px;" />Add Photos to this album</a>
                        </th>
                    </tr>
                    <tr>
                        <th width="5"><input  type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
                        <th width="130"><strong>Thumb</strong></th>
                        <th><strong>Photo Caption</strong></th>
                        <th><strong>Size</strong></th>
                        <th><strong>Type</strong></th>
                    </tr>
                    <?php
                    if(count($album_details['Pictures']) > 0) {
                            $i = 1;
                    foreach($album_details['Pictures'] as $row){
                        if($i % 2 == 0)	{ $tableStyle = "odd"; }else { $tableStyle = "even"; }
                    ?>
                    <tr>
                      <th class="<?=$tableStyle;?>" width="5"><input class="selectBox" name="sel[]" type="checkbox" value="<? echo $row['pid'];?>" /></th>
                      <th class="<?=$tableStyle;?>" width="130">
                            <img class="album_image"  src="<? echo $album_config['ecards_more_pic_target'].$album_config['fullpath'].$row['filepath'].$album_config['thumb_pfx'].$row['filename'];?>" alt="<?php echo $row['title'];?>" />
                            <br/><span>
                                <?php
                                    echo substr($row['filename'],0,20);
                                    if(strlen($row['filename'] > 20)){
                                        echo '...';
                                    }
                                ?>
                            </span>
                      </th>
                      <th class="<?=$tableStyle;?>">
                          <div id="<?php echo $row['pid'];?>" class="editableText"><?php if(!empty($row['title'])){echo $row['title'];}else {echo '&nbsp;';}?></div>
                          <?php if($album_details['Album']['thumb'] == $row['pid']){ echo '<span class="thumb-span">Thumb</span>';}?>
                      </th>
                      <th class="<?=$tableStyle;?>"><?php $filesize = (int)$row['filesize'];$filesize = round($filesize/1024,2); echo $filesize;?>KB</th>
                      <th class="<?=$tableStyle;?>"><?php $ext =  substr($row['filename'], strrpos($row['filename'], '.') + 1);echo strtoupper($ext);?></th>
                    </tr>
                    <? $i++; }} ?>
                     <tr>
                      <th colspan="6" align="right"><ul class="pagination"> <? echo $navlinks; ?> </ul></th>
                    </tr>
                  </table>
            </form>
        </div>
        <div style="display:none">
            <div id="previewBox" style="padding:10px; background:#fff;">
                <p><strong>This content comes from a hidden element on this page.</strong></p>
                <p>The inline option preserves bound JavaScript events and changes, and it puts the content back where it came from when it is closed.<br /></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('.editableText').editableText({
          // default value
          newlinesEnabled: false
});
$('.edit').click(function(){return false;});
$('.save').click(function(){return false;});
$('.cancel').click(function(){return false;});
$('.editableText').change(function(){
     var newValue = $(this).text();
     var photo_id = $(this).attr('id');
     $.ajax({
        type: "POST",
        url: "<?php echo site_url('manage/update_photo_caption');?>/"+photo_id,
        data: "photo_caption=" + newValue,
        dataType: 'json',
        success: function(msg){
          if(msg.result == 1){
              return true;
          }else{
              alert('There was an error while updating. Please try again');
              return false
          }
        }
     });
 });
</script>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
