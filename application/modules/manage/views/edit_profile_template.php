<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/editabletext.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
    <style type="text/css">
    .thumb-span {background:#860e9c;padding:2px 4px;text-transform:uppercase;color:#fff;-moz-border-radius:4px; border-radius: 4px;font-weight:bold;}
    </style>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'photos';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Edit Album</h1>
        </div>
        <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
            <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
            <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
        <form method="post" action="" id="frm-edit-profile">
            <div class="form-fields">
              <!-- Locked contact details -->
              <div class="contact-detail-box">
                <div class="info-strip"><img src="<?=base_url()?>images/ico-49.jpg" alt="locked" width="16" height="15"/> <span>If you want to change this information, please contact your HR team.</span></div>
                <div class="contact-details">
                  <div class="contact-name"><?=$name?></div>
                  <div class="contact-desg"><strong><?=$designation?></strong>, <?=$concept?></div>
                  <div class="contact-info">
                    <div class="contact-attr">Ext: </div>
                    <div><input type="text" name="extension" value="<?=$extension?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">First Name:</div>
                    <div><input type="text" name="first_name" value="<?=$first_name?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Middle Name:</div>
                    <div><input type="text" name="middle_name" value="<?=$middle_name?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Last Name:</div>
                    <div><input type="text" name="last_name" value="<?=$last_name?>" /></div>
                  </div>
                  
                  <div class="contact-info">
                    <div class="contact-attr">Title:</div>
                    <div>
                      <select name="concept">
                        <option value="MR." <?php echo $title == 'MR.' ? 'selected="selected"' : ''; ?>>MR.</option>
                        <option value="MRS." <?php echo $title == 'MRS.' ? 'selected="selected"' : ''; ?>>MRS.</option>
                      </select>
                    </div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Phone:</div>
                    <div><input type="text" name="phone" value="<?=$phone?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Mobile:</div>
                    <div><input type="text" name="mobile" value="<?=$mobile?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Designation:</div>
                    <div><select name="designation">
                    <?php foreach($designations as $des) {
                      ?><option value="<?php echo $des->id; ?>" <?php echo $des->name == $designation ? 'selected="selected"' : ''; ?>><?php echo $des->name; ?></option><?php
                    }?>
                    </select></div>
                  </div>
                  <div class="contact-info">
                    <div class="contact-attr">Concept:</div>
                    <div><select name="concept">
                    <?php foreach($concepts as $conc) {
                      ?><option value="<?php echo $conc->id; ?>" <?php echo $conc->name == $concept ? 'selected="selected"' : ''; ?>><?php echo $conc->name; ?></option><?php
                    }?>
                    </select></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">Email:</div>
                    <div><input type="text" name="email" value="<?=$email?>" /></div>
                  </div>
                  <div class="clear"></div>
                  <div class="contact-info">
                    <div class="contact-attr">HRMS ID:</div>
                    <div><input type="text" name="hrms_id" value="<?=$hrms_id?>" /></div>
                  </div>
                </div>
              </div>
              <!-- Skills and Experience -->
              <div class="field-box">
                <div class="field-label"><a name="skills"></a>Skill &amp; Experiences</div>
                <textarea name="txtar-skill-exp" id="txtar-skill-exp" cols="52" rows="6"><?=$user_skills?></textarea>
              </div>
              <!-- Interests -->
              <div id="interest-groups" class="field-box">
                <div class="field-label">Interests</div>
                <div class="error" id="error-interest-groups" style="margin-top:5px; margin-bottom:5px;"></div>
                <textarea name="txtar-interest" id="txtar-interest" cols="52" rows="6"><?=$user_interest_groups?></textarea>
                <div>Restrict your interest group to 15 characters each (including spaces)</div>
              </div>
              <!-- About Me -->
              <div class="field-box">
                <div class="field-label">About Me</div>
                <textarea name="txtar-aboutme" id="txtar-aboutme" cols="52" rows="6"><?=$user_about?></textarea>
              </div>
            </div>
            <div class="submit-div"><input type="image" src="<?=base_url()?>images/btn-save.jpg" name="save changes" /> <a href="#">Cancel</a></div>
          </form>
      
          <div style="display:none">
              <div id="previewBox" style="padding:10px; background:#fff;">
                  <p><strong>This content comes from a hidden element on this page.</strong></p>
                  <p>The inline option preserves bound JavaScript events and changes, and it puts the content back where it came from when it is closed.<br /></p>
              </div>
          </div>
          <div id="header">
      <div class="header-holder">
        <!-- navigation -->
        <?php $this->load->view("includes/navigation",$active_tab);?>
        <div class="head-bar">
          <!-- sub navigation -->
          <ul class="subnav">
            <li class="active"><a href="<?=site_url("people")?>" tabindex="15"><span>Company Directory</span></a></li>
            <li><a href="<?=site_url("status_update")?>" tabindex="16"><span>Status Updates</span></a></li>
            <li><a href="<?=site_url("interest_groups")?>" tabindex="17"><span>Interest Groups</span></a></li>
          </ul>
          <!-- header search form -->
          <?php $this->load->view("includes/search_form");?>
        </div>
      </div>
    </div>
    </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
