<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/calendrical.css" />
<script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
<script type="text/javascript">
$(function() {
  $('.circle').sparkline('html',  {type: 'pie',sliceColors: ['#2fbb17','#dad9d9']        });
});
</script>
</head>
<body>
<div class="wrapper">
    <?php
        $data['active_tab'] = 'email_queue';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
  <div class="content">
    <div class="top-heading">
      <h1>Notification E-mail Queue</h1>
      <p style="clear:both;font-size: 14px;line-height: 22px; width: 539px;">Notification emails can be scheduled for announcement, news and offers items when they are posted. The emails are sent out in batches of 200 and usually takes 2 hours. You can see the status of your notification queue below.</p>
    </div>
    <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
		<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_error');?>
        </div>
    <?php endif;?>   
    
    <div class="main-table">
    <form name="form" method="post" action="<?=site_url("manage/email_queue_search")?>">
      <table width="100%" border="0" cellspacing="0">
         <tr>
          <th class="odd" colspan="7" style="margin-left:0px; padding-left:0px;">
          <div class="searchArea"><input type="text" name="SearchText" id="SearchBox" value="<?php if(isset($search_term)){echo $search_term;}else{echo 'Search Notifications';}?>" /></div>
          <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />          
          <ul class="pagination"><? echo $navlinks; ?></ul></th>
        </tr>      
        <tr>
          <th width="100"><strong>Date</strong></th>
          <th width="400"><strong>Notification Email for</strong></th>
          <th width="300"><strong>Initiator</strong></th>
          <th width="200"><strong>Status</strong></th>
          <th width="150" style="text-align: center;" ><strong>Total Emails</strong></th>
          <th width="200" style="text-align: center;" ><strong>Sent</strong></th>
          <th width="120" style="text-align: center;">
            <strong>Failed</strong>
            <img class="show-info" id="arrow" alt="faq" src="<?php echo base_url()?>images/arrow-right.png" />
            <div class="show-info" id="faq-queue-detail">
            <p><strong>What does this mean ?</strong></p>
            <p>The system makes 5 attempts to send an email. If due to some reason, an email is not sent even after 5 attempts, it is marked as failed.
                <br />The administrators of the Intranet can later manuallly investigate the reasons fo the failure.</p>
            </div> <a id="faq-email" href="#">  <img alt="faq" src="<?php echo base_url()?>images/ico-66.png" /></a></th>
        </tr>
        <?php if(count($email_manager_list) > 0) {
        $i = 1;
        
        foreach($email_manager_list as $row){
        if($i % 2 == 0)	{ $tableStyle = "odd"; }else { $tableStyle = "even"; }
        ?>
        <tr>

          <td class="<?=$tableStyle;?>" width="50"><?php echo $row['created_on'];?></td>
          <td class="<?=$tableStyle;?>" width="5"><?php echo $row['email_title'];?></td>
          <td class="<?=$tableStyle;?>" width="440"><?php echo $row['name'];?></td>
          <td class="<?=$tableStyle?>" width="200"><span class="<?php echo $row['class'];?>"><?php echo $row['status'];?></span></td>
          <td class="<?=$tableStyle;?>" align="center" width="150"><?php echo $row['total_email_count'];?></td>
          <td class="<?=$tableStyle;?>" align="center" width="40"><?php echo $row['total_email_sent_count'];?> <span class="circle"><?php echo $row['total_email_sent_count'];?>,<?php echo ($row['total_email_count']-$row['total_email_sent_count']);?></span></td>
          <td class="<?=$tableStyle;?>"  align="center" width="40"><?php if($row['total_email_failed_count']){echo '<a href="'.site_url('manage/emailqueue_error/'.$row['campaign_id']).'"> '.$row['total_email_failed_count'].'</a>';}else{echo 'NA';} ?></td>
        </tr> 
        <? $i++; }}  else {
           echo '<tr>
          <td colspan="7" align="center"><strong>Email Queue is empty</strong></td>
        </tr>';
         } ?>
         <tr>
          <th colspan="7" align="right"><ul class="pagination"> <? echo $navlinks; ?> </ul></th>
        </tr>
      </table>
      </form>
    </div>
  </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
