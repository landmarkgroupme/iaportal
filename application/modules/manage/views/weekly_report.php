<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Intranet-Weekly-Stats</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/mprofile/all.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/user_update/misc.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css">
    <link rel="stylesheet" href="//cdn.datatables.net/tabletools/2.2.1/css/dataTables.tableTools.css">
    <link rel="stylesheet" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/jqueryui/dataTables.jqueryui.css">
        
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <!--<script type="text/javascript" src="=//base_url();?>js/custom_js/manage/manage.js"></script>-->
    <script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
    <script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script src="//cdn.datatables.net/tabletools/2.2.1/js/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/weekly_export.js"></script>
    
    <style type="text/css">
    .thumb-span {background:#860e9c;padding:2px 4px;text-transform:uppercase;color:#fff;-moz-border-radius:4px; border-radius: 4px;font-weight:bold;}
    </style>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'weekly_report';
        $data['user_name'] = $username;
        $this->load->view('includes/admin_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Intranet Weekly Status</h1>
        </div>
        <?php if(!empty($sucess)):?>
        <div class="msg-ok">
           <ul>
            <?php foreach($sucess as $suc) :?>
            <li>
                <?php print($suc); ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <?php if(!empty($errors)):?>
        <div class="msg-error">
            <ul>
            <?php foreach($errors as $error) :?>
            <li>
                <?php print($error); ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <div class="clear"></div>
        <div>
          <!--<form action="<?php //echo site_url('people/search')?>" class="search-form" name="searchform" id="quick_search_frm" method="post">
            <fieldset>
              <input accesskey="4" name="term" autocomplete="off" tabindex="23" type="text" class="text" id="txt_general_search" value="Search by name.."/>
              <input name="search" type="hidden" value="search"/>
              <input name="search_from" type="hidden" value="" id="search_from"/>
            </fieldset>
            <div id ="search_element">
              <div id='search_listing'>
                <div id="src-loader" ><img src="<?php //echo base_url()?>images/loader.gif" /></div>
                <ul id='src_user_list'>
                  
                </ul>
                <p class='src_more_res'><a id="submit_src" href="#"></a></p>
              </div>
              <div id='search_listing_f'></div>
            </div>
            <img src="<?php //echo base_url()?>images/gnr_src_bg-f.jpg" style="display: none;" alt="bg-preload" />
            <img src="<?php// echo base_url()?>images/user_list_bg.png" style="display: none;" alt="bg-preload" />
            <img src="<?php// echo base_url()?>images/user_list_f_bg.png" style="display: none;" alt="bg-preload" />
          </form>-->
        </div>
        
        <div class="clear"></div>
        <form method="post" action="<?php echo site_url('manage/weekly_reports'); ?>" id="weekly_report_form" name="weekly_report_form">
            <p>Start Date: </p><input type="text" id="start-datepicker" name="start-datepicker" value="<?php echo !empty($sdate) ? $sdate : ''; ?>"> 
            <p>End Date: </p><input type="text" id="end-datepicker" name="end-datepicker" value="<?php echo !empty($edate) ? $edate : ''; ?>">
            <input type="submit" value="Filter by Date" />
          </form>
          <!--<a id="export_all_data" class="myButton"  href="javascript:void(0)">Export Report in CSV </a>-->
          <!--<a id="export_all_data_pdf" class="myButton"  href="javascript:void(0)">Export Report in PDF </a>-->
          <table id="news_data" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">News offers announcements</th>    
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                
                <?php foreach($news_data as $news): ?>
                <tr>
                    <td><?php print $news['week']; ?></th>
                    <td><?php print $news['entry_count']; ?></th>
                </tr>
                <?php endforeach; ?>
          </table>
          
        <table id="login_data" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Weekly Logins Data</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($login_data as $login): ?>
                <tr>
                    <td><?php print $login['week']; ?></th>
                    <td><?php print $login['no_of_logins']; ?></th>
                </tr>
                <?php endforeach; ?>
          </table>
                    
        <table id="status_data" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Weekly Status Report</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($status_data as $status): ?>
                <tr>
                    <td><?php print $status['week']; ?></th>
                    <td><?php print $status['value']; ?></th>
                </tr>
                <?php endforeach; ?>
          </table>
        
        <table id="poll_data" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Polls</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($polls_data as $poll): ?>
                <tr>
                    <td><?php print $poll['week']; ?></th>
                    <td><?php print $poll['value']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        
        <table id="files_uploaded_data" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Files Uploaded</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($files_uploaded as $file): ?>
                <tr>
                    <td><?php print $file['week']; ?></th>
                    <td><?php print $file['no_of_files']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        <table id="comments_data" class="display" cellspacing="0" width="100%">
            <thead>
               <tr>
                    <th class="tmhead">Number of Comments
</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($comment_data as $comment): ?>
                <tr>
                    <td><?php print $comment['week']; ?></th>
                    <td><?php print $comment['value']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        
                <table id="likes_data" class="display" cellspacing="0" width="100%">
            <thead>
               <tr>
                    <th class="tmhead">Total likes</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($likes_data as $like): ?>
                <tr>
                    <td><?php print $like['week']; ?></th>
                    <td><?php print $like['value']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        <table id="pictures_data" class="display" cellspacing="0" width="100%">
            <thead>
<tr>
                    <th class="tmhead">Photos posted by users</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($uploaded_pictures_data as $uploaded_pic): ?>
                <tr>
                    <td><?php print $uploaded_pic['week']; ?></th>
                    <td><?php print $uploaded_pic['value']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>

        <table id="weekly_chat_msgs" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Chat messages</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($chat_msgs_data as $chat): ?>
                <tr>
                    <td><?php print $chat['week']; ?></th>
                    <td><?php print $chat['value']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        
        <?php if(!empty($market_place_item_data)) : ?>
        <table id="market_place_item_data" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Market Place Items</th>
                </tr>
                <tr>
                    <th class="tsbhead">Month</th>
                    <th class="tsbhead">No Of Logins</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($market_place_item_data as $market_place_item): ?>
                <tr>
                    <td><?php print $market_place_item['week']; ?></th>
                    <td><?php print $market_place_item['value']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        <?php endif;?>
        
         <?php if(!empty($monthly_login_data)) : ?>
        <table id="market_place_logindata" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Monthly Login Data</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($monthly_login_data as $monthly_login): ?>
                <tr>
                    <td><?php print $monthly_login['month']; ?></th>
                    <td><?php print $monthly_login['no_of_logins']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        <?php endif;?>
        
         <?php if(!empty($monthly_login_data)) : ?>
        <table id="market_place_uniquedata" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="tmhead">Monthly Unique Login</th>
                </tr>
                <tr>
                    <th class="tsbhead">Week</th>
                    <th class="tsbhead">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($monthly_unique_login_data as $monthly_unique_login): ?>
                <tr>
                    <td><?php print $monthly_unique_login['month']; ?></th>
                    <td><?php print $monthly_unique_login['no_of_logins']; ?></th>
                </tr>
                <?php endforeach; ?>
        </table>
        <?php endif;?>   
    </div>
</div>
<script type="text/javascript">
//General Search Field


$(document).ready(function(){
var base_url = "<?php print site_url(); ?>";    
    $( "#start-datepicker" ).datepicker();
    $( "#end-datepicker" ).datepicker();
    
    TableTools.DEFAULTS.aButtons = [ "csv", "xls", "pdf" ];
    TableTools.DEFAULTS.sSwfPath = base_url+"/js/TableTools-2.2.1/swf/copy_csv_xls_pdf.swf";

    
    //$('#news_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#login_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});    
    //
    //$('#status_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#poll_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#files_uploaded_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#comments_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});    
    //
    //$('#likes_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#pictures_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#weekly_chat_msgs').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#market_place_item_data').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //
    //$('#market_place_logindata').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    //    
    //$('#market_place_uniquedata').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": 'T<"clear">lfrtip',
    //});
    
        
    
    $("#tabs").tabs( {
        "activate": function(event, ui) {
            $( $.fn.dataTable.tables( true ) ).DataTable({
                "tableTools": {
                    "sSwfPath": base_url+"/js/TableTools-2.2.1/swf/copy_csv_xls_pdf.swf"
                }
                }).columns.adjust();
        }
    } );
    
    $('table.display').dataTable( {
        "scrollY": "200px",
        "scrollCollapse": true,
        "paging": false,
        "jQueryUI": true
    });
    
    //$('table.display').dataTable({
    //    
    //    "sDom": 'T<"clear">lfrtip',
    //    //"tableTools": {
    //    //    "sSwfPath": base_url+"/js/TableTools-2.2.1/swf/copy_csv_xls_pdf.swf"
    //    //}
    //});    
});
//function general_search_form(){
// 
//  //search when clikc on link
//  $('#submit_src').live('click',function(){
//    $('#quick_search_frm').submit();
//  })
//
//  //Hover background change
//  $('#src_user_list li div').live('mouseover',function(){
//    $('#src_user_list li div').removeClass('hover');
//    $(this).addClass('hover')
//    return false;
//  })
//  //Hide search list on main mouse out
//  $('#src_user_list').blur(function(){
//    $('#search_element').hide();
//  })
//
//  //User click
//  $('#src_user_list li').live('click',function(){
//    $('#search_element').show();
//    var user_profile = this.id
//    window.location = user_profile;
//    return false;
//  })
//  //Type event
//  var tmp_cnt = 0;// arrow key navigation count
//  $('#txt_general_search').keyup(function(e){
//
//    if(e.keyCode == 27){
//      $('#search_element').hide();
//      return false;
//    }
//   
//    var src_term =  $('#txt_general_search').val();
//    var src_from =  '';
//    if($('#search_from')) 
//    {
//      src_from = $('#search_from').val(); 
//    }
//    if(src_term.length < 3){
//      return false;
//    }
//   
//
//    var size_li = $("#src_user_list li").size();
//
//    if(e.keyCode == 38){
//      if(tmp_cnt <=0){
//        tmp_cnt =  size_li;
//      }else{
//        tmp_cnt = tmp_cnt-1
//      }
//      //$('#src_user_list li div').css('background-color','inherit')
//      //$('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
//      $('#src_user_list li div').removeClass('hover')
//      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
//      return false;
//    }else if(e.keyCode == 40){
//      if(size_li == tmp_cnt){
//        tmp_cnt = 0;
//      }else{
//        tmp_cnt = tmp_cnt+1
//      }
//      //$('#src_user_list li div').css('background-color','inherit')
//      // $('#src_user_list :nth-child('+tmp_cnt+') div').css('background-color','#3671a6')
//      $('#src_user_list li div').removeClass('hover')
//      $('#src_user_list :nth-child('+tmp_cnt+') div').addClass('hover')
//      return false;
//    }
//
//
//
//    $('#search_element').show();
//    $('#src-loader').show();
//
//    $.ajax({
//      url: base_url+"people/quick_search_sphinx",
//      data: {
//        "src_term":src_term,
//        'src_from': src_from
//      },
//      async: true,
//      dataType: "json",
//      type: "POST",
//      success: function(data){
//        $('#src-loader').hide();
//        if(data['show']){
//          //alert(e.keyCode);
//          $('#src_user_list').html(data['result']);
//          $('#submit_src').html(data['result_count']);
//          $('#search_element').show();
//        //alert($('#src_user_list li:first').html());
//        }else{
//          $('#src_user_list').html(data['result']);
//          $('#submit_src').html('');
//          $('#search_element').show();
//        }
//      }
//    });
//
//    return false;
//  })
//
//
//  //Focus
//  $("#txt_general_search").focus(function(){
//    var src_term =  $("#txt_general_search").val();
//    if( $.trim(src_term) == 'Search by name..'){
//      $("#txt_general_search").val('');
//    }
//    $(this).css('color','#000000');
//  })
//  //Blur
//  $("#txt_general_search").blur(function(){
//    var src_term =  $("#txt_general_search").val();
//    if( $.trim(src_term) == ''){
//      $("#txt_general_search").val('Search by name..');
//    }
//    $(this).css('color','#747474');
//  })
//}
//general_search_form();
</script>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  
</body>
</html>