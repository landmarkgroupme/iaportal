<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/css/manage/style.css" />
    <script type="text/javascript" src="<?= base_url(); ?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.limit.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.form.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/manage/manage_offers_create.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/facebox_manage.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?= base_url(); ?>js/facebox.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/nicEdit_admin.js"></script>
    <script type="text/javascript">
      bkLib.onDomLoaded(function() {
        //nicEditors({iconsPath : }).allTextAreas()
        new nicEditor({
          iconsPath : '<?= base_url(); ?>images/nicEditorIcons.gif',
          buttonList : ['image','upload','link','unlink'],
          uploadURI : '<?= base_url() ?>lib/nicUpload.php'
        }).panelInstance('txt_body');
      });
    </script>
  </head>
  <body>
    <div class="wrapper">
      <?php
      $data['active_tab'] = 'offers';
      $data['user_name'] = $user_name;
      $this->load->view('includes/manage_header', $data);
      ?>
      <div class="content">
        <div class="top-heading">
          <h1>Edit Offers</h1>
        </div>
        <?php if ($this->session->flashdata('status_ok')): ?>
          <div class="msg-ok">
          <?= $this->session->flashdata('status_ok'); ?>
        </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('status_error')): ?>
            <div class="msg-ok">
          <?= $this->session->flashdata('status_error'); ?>
          </div>
        <?php endif; ?>

            <form action="<? site_url("manage/edit_offers") ?>" method="post" id="frm_update_offers">
              <input type="hidden" name="entry_category" id="entry_category" value="offers" />
              <input type="hidden" name="entry_id" id="entry_id" value="<?= $entry_details['entry_id'] ?>" />
              <div id="create_content" class="main-table form">
                <div class="counter">Characters left: <span id="charsLeft"></span></div>
                <div class="form-title">Title <span id="error-title" class="error-msg"></span></div>
                <input type="text" name="txt_title" id="txt_title" class="form-text" value="<?= $entry_details['entry_title'] ?>" />
                <div class="counter">Characters left: <span id="charsLeft2"></span></div>
                <div class="form-title">Excerpt</div>
                <textarea rows="6" name="txt_excerpt" id="txt_excerpt" class="form-text"><?= $entry_details['entry_text_more'] ?></textarea>

                <div class="form-title">Body <span id="error-body" class="error-msg"></span></div>
                <div class="txt_body_holder"><textarea rows="12" name="txt_body" id="txt_body" class="form-text"><?= $entry_details['entry_text'] ?></textarea></div>
                <div style="padding-left:7px;">Maximum image width: 560 pixels and maximum file size: 70kb</div>
                <div class="form-title-note"><span class="red-text">NOTE:</span> The body will be <strong>Center Aligned</strong> in Offer's page</span></div>

                <div id="date-period-box" class="custom-form-element offer-page" >
                  <div class="form-title">OFFER PERIOD</div>
                  <div class="form-area">
                    <input type="text" class="date_field" name="txt_date_start" readonly="readonly" id="txt_date_start" value="<?php echo $entry_details['offers_start_date']; ?>" />
                    <input type="text" class="date_field" name="txt_date_end" readonly="readonly" id="txt_date_end" value="<?php echo $entry_details['offers_end_date']; ?>" style="margin-left:20px;" />
                    <span id="error_offer_date_time" class="error-msg"></span>
                  </div>
                </div>

                <input name="txt_publish" id="txt_publish" type="hidden" value="1" />

                <div class="clear"></div>
              </div>

              <div class="button-holder">
                <div style="float:right;">
                  <a href="#" class="previewLink" style="display: block; float:left;margin-right:10px; margin-top:6px; background:url(<?= base_url() ?>images/moderation/preview.gif) no-repeat; height:20px; padding-left:20px; color:#1f5987; font-weight:bold; text-decoration:underline">Preview</a>
                  <input class="submit_entry ItemButtonDraft" name="save_draft" type="submit" value="Save as draft" />
                  <input id="submit_offers" class="ItemButtonSave submit_entry" name="save" type="submit" value="Publish Now" />
                </div><div class="clear"></div>
              </div>
            </form>
          </div>    
          <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>">
            <script type="text/javascript">
              $('.previewLink').click(function(){
			   $('#txt_body').val($.trim($(".nicEdit-main").html()));//General
                var options = {
                  success:       showResponse,
                  url:'<?= site_url('manage/preview_mt_entry'); ?>'
            };
            $('#frm_update_offers').ajaxSubmit(options);
            return false;
          });

          // post-submit callback
          function showResponse(responseText, statusText, xhr, $form)  {
            jQuery.facebox(responseText);
          }
        </script>
	    <!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  
        </body>
  
        </html>
