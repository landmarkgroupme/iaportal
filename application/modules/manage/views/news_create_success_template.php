<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/calendrical.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/uploadify.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/colorbox.css" />
<script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/moderation/swfobject.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/moderation/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#uploadify").uploadify({
'uploader'       : '/landmark/js/moderation/uploadify.swf',
'script'         : '/landmark/js/moderation/uploadify.php',
'cancelImg'      : '/landmark/css/manage/cancel.png',
'folder'         : '/landmark/files_dropbox',
'queueID'        : 'fileQueue',
'fileDesc'       : '',
'fileExt'        : '*.jpg,*.jpeg,*.png,*.bmp,*.gif',
'sizeLimit'      : '71680',
'auto'           : false,
'auto'           : false,
'multi'          : false
});
$(".previewLink").colorbox({width:"900px", inline:true, href:"#previewBox"});
})
</script>
</head>
<body>
<div class="wrapper">
  <div class="top-header">
    <div class="left-header">
      <div class="logo"></div>
      <div class="separator"></div>
      <div class="pagetitle">Manage</div>
    </div>
    <div class="right-header">
      <div class="logininfo">Logged in as: <strong>
        <?=$user_name?>
        </strong></div>
      <a href="<?=base_url();?>index.php/login/logout">
      <div class="logout"></div>
      </a> </div>
  </div>
  <div class="menu">
    <div class="createNewMenu">
      <ul>
        <li class="first"><a href="<?=base_url();?>index.php/manage/createnews">� News</a></li>
        <li><a href="<?=base_url();?>index.php/manage/createannouncement">� Announcement</a></li>
        <li><a href="<?=base_url();?>index.php/manage/createoffers">� Offers</a></li>
        <li><a href="<?=base_url();?>index.php/manage/createphotos">� Photos</a></li>
        <li><a href="<?=base_url();?>index.php/manage/createevents">� Events</a></li>
        <li><a href="<?=base_url();?>index.php/manage/createpermissions">� Permissions</a></li>
      </ul>
    </div>
    <ul class="main">
      <li><a href="<?=base_url();?>index.php/backend/moderate/show_all/">Moderate</a></li>
      <li class="active"><a href="<?=base_url();?>index.php/manage/news">Manage</a></li>
      <li class="createNew"><a class="createNewButton" href="#" style="padding:4px 59px 2px 59px"></a></li>
    </ul>
    <ul class="submenu">
      <li class="active"><a href="<?=base_url();?>index.php/manage/news">News</a></li>
      <li><a href="<?=base_url();?>index.php/manage/announcement">Announcements</a></li>
      <li><a href="<?=base_url();?>index.php/manage/offers">Offers</a></li>
      <li><a href="<?=base_url();?>index.php/manage/photos">Photos</a></li>
      <li><a href="<?=base_url();?>index.php/manage/events">Events</a></li>
      <!--li><a href="<?=base_url();?>index.php/manage/permissions">Permissions</a></li-->
    </ul>
  </div>
<div class="content">
    <div class="top-heading">
      <h1>News Item Created Succesfully!</h1>
    </div>
  </div>
  <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>">
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  
</body>
</html>