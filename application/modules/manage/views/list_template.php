<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php

//print "<pre>";
//print_r($list_data);
//exit;
?>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
  <!--<script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script> -->
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input-facebook.css" />

 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.tokeninput.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".inline").colorbox({inline:true, width:"50%"});
			
		   $("input[type=button]").click(function () {
		  // $('#target').submit();
            //alert("Would submit: " + $(this).siblings("input[type=text]").val());
        });
				});
				</script>
  </head>
  <body>
    <div class="wrapper">
      <?php
        $data['active_tab'] = strtolower($page_title);
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>

      <!-- #Content-->
      <div class="content">
        <div class="top-heading">
          <h1><?=$page_title?></h1>
        </div>
        
        <?php if($this->session->flashdata('status_ok')):?>
            <div class="msg-ok">
              <?=$this->session->flashdata('status_ok');?>
            </div>
            <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
            <div class="msg-error">
              <?=$this->session->flashdata('status_error');?>
            </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_del')):?>
          <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
        <?php endif;?>
        <!-- #Listing-->
        <div class="main-table">
          <form id="search_post" name="form" method="post" action="<?=site_url("manage/".$delete_link)?>">
            <table id="list-table" width="100%" border="0" cellspacing="0">
              <thead>
              <tr>
                <th class="odd" colspan="6" style="margin-left:0px; padding-left:0px;">
                  <div class="ArrowButton"></div>
                  <div>
                  <input class="ItemButton" name="Delete" type="submit" value="Remove" />
                  </div>
                  <div class="searchArea">Search: <input type="text" name="SearchText" id="SearchBox" placeholder="<?php echo "Search ".$page_title;?>"  value="<?php echo ($this->session->flashdata('SearchText')? $this->session->flashdata('SearchText'):'');?>"/></div>
                  <input type="hidden" name="mt_category" value="<?php echo $mt_category;?>" />
                  <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />          
                </th>
              </tr>
              <tr>
                <th width="5"><input class="selectBox" type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
                <th width="400"><strong>Title</strong></th>
                <th width="155">Status</th>
                <!--<th width="200"><strong>Author (Concept)</strong></th> -->
                <th width="100"><strong>Created</strong></th>
                <th width="40"><strong>View</strong></th>
				 <th width="40"><strong>Managers</strong></th>
              </tr>
              </thead>
              <tbody>
			  <?php if (empty($list_data)){ ?>
			  <tr>
                <th class ="odd" colspan="6" align="right">NO data present</th>
              </tr>
			<?php } ?>
            <?php $i = 1; foreach($list_data as $row): if($i % 2 == 0)  { $tableStyle = "odd"; }else { $tableStyle = "even"; }?>
              <tr>
                <th class="<?=$tableStyle;?>" width="5"><input class="selectBox" name="sel[]" type="checkbox" value="<? echo $row->id;?>" /></th>
                <th class="<?=$tableStyle;?>" width="400"><a href="<?=site_url("manage/post_".strtolower($page_title)."/".$row->id)?>"><? echo $row->name;?></a></th>
				<th class="<?=$tableStyle;?>" width="155"><?php
				$status = isset($row->group_status)?$row->group_status:$row->status;
				switch ($status) {
				
				case 1:
				case 'active':
					echo '<div class="post_status published">Active</div>';
					break;
				
				case 2:
				case 'inactive';
					echo '<div class="post_status rejected">Inactive</div>';
					break;
				default: 
					echo '<img style="margin-left:3px" src="' . base_url() . 'images/moderation/ico-question.gif" alt="unknown" />';
					break;
				}
				?></th>
			<!--	<th class="<?//=$tableStyle;?>" width="200"><?// echo $row->user_name; ?></th> -->
				<th class="<?=$tableStyle;?>" width="100"><? echo $row->create_dttm; // date("Y/m/d", $tomorrow)?></th>
				 <th class="<?=$tableStyle;?>" width="40">
                  <?php 
				  $item_path = isset($row->slug)?$row->slug:$row->id;
				  ?>
                  <a target="_blank" href="<?=site_url(strtolower($page_type)."/".$item_path)?>"><img style="margin-left:9px" src="<?=base_url()?>images/moderation/view.gif" alt="View" /></a>
                  
                </th>
				 <th class="<?=$tableStyle;?>" width="40">
                  
                 <p><a class='inline' href="#<?=$mt_category?>_permissions_<?=$row->id?>">Permissions</a></p>
                  
                </th>
              </tr>
               
            <?php $i++; endforeach;?>
              </tbody>
              
              <tr>
                <th colspan="6" align="right"><ul class="pagination"> <? echo $navlinks; ?></ul></th>
              </tr>
            </table>
          </form>
		  <div style='display:none'>
	<?php
	foreach($list_data as $row){
	?>
	<div id='<?=$mt_category?>_permissions_<?=$row->id?>' style='padding:10px; background:#fff;'>
		 <form name="form" id= "target" method="post" action="<?=site_url("manage/concept_group_managers")?>"> 
		<input type="hidden" name="group_id" value="<?=$row->id?>">
		<input type="hidden" name="object_type" value="<?=$mt_category?>">
				 <h2><?=$row->name?> <?=$mt_category?> Managers</h2>
					<div>
					
						<input type="text" id="demo-input-<?=$row->id?>" name="blah" />
						<input type="submit" value="Submit" />
						<script type="text/javascript">
						$(document).ready(function() {
						$("#demo-input-<?=$row->id?>").tokenInput("<?=base_url();?>manage/auto_conept_managers",{
						theme: "facebook",
						<?php if(isset($row->manager))		
						{		?>
						prePopulate:/* [

						{id: 123, name: "Slurms MacKenzie"},
						{id: 555, name: "Bob Hoskins"},
						{id: 9000, name: "Kriss Akabusi"}
						]*/
						<?php print $row->manager; }?>

						});
						});
						
						 
						</script>
					</div>
				</form>
	</div>
	<?php
	}
	?>
	</div>
        </div>
       <!-- /Listing-->
      </div>
      <!-- /Content-->
    </div>
	<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>">
	<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
  </body>
  
</html>