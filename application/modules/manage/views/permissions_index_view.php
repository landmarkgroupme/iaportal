<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/style.css" />

<!--<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/facebox_manage.css" />-->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input-facebook.css" />

 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.tokeninput.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
<style type="text/css">
  .main-table th.permission-box{border-left:1px #dcdcdc solid;text-align: center;}
</style>

</head>
<body>
<div class="wrapper">
  <?php
		$this->load->helper('form');
        $data['active_tab'] = 'permissions';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
  <div class="content">
    <div class="top-heading">
      <h1>People &amp; Permissions</h1>
    </div>
    <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
		<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
          <?=$this->session->flashdata('status_error');?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_del')):?>
      <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
    <?php endif;?>
    <div>
		  <a class="ItemButton" href="<?=site_url('manage/role_permissions')?>" style="height:18px;padding-top:3px;">Role &amp; Permissions</a>
    </div>
    <div class="main-table">
    <input type="hidden" value="<?=base_url()?>" id="base_url" name="base_url" /> 
     <form name="form" method="post" action="<?=site_url("manage/searchuser")?>"> 
    
      <table id="list-table" width="100%" border="0" cellspacing="0">
         <tr>
          <th class="odd" colspan="15" style="margin-left:0px; padding-left:0px;">	  
		  <div class="searchArea">Search: <input type="text" name="SearchText" id="SearchBox" placeholder="<?php echo "Search Users";?>"  value="<?php echo ($search_term ? $search_term:'');?>"/></div>
		  
          <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />
          <ul class="pagination"><?php echo $navlinks; ?></ul></th>
        </tr>
        <tr>
          <th width="5"><input type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
          <th><strong>Name</strong></th>
          <th><strong>Concept</strong></th>
    		  <!-- <th><strong>groups</strong></th> -->
    		  <!-- <th><strong>Concepts</strong></th> -->
    		  <th><strong>Role</strong></th>
          <!--th width="140"><img src="<?=base_url()?>images/permission-info.png" alt="permission info" style="position:absolute;margin-left:-10px;margin-top:20px;" /></th-->
        </tr>
		<?php 
		if(isset($permission_details) && !empty($permission_details))
	{
		if(count($permission_details) > 0) {
            $i = 1;
            
            foreach($permission_details as $row){
			if($i % 2 == 0)	{ $tableStyle = "odd";$tableStyle_review = 'odd_review'; }else { $tableStyle = "even"; $tableStyle_review = 'even_review';}
			?>
        <tr>
          <th class="<?=$tableStyle;?>" ><input class="selectBox" <?php if($row['id'] == $this->session->userdata('userid')){ echo 'disabled="disabled"';} ?> name="sel[]" type="checkbox" value="<? echo $row['id'];?>" /></th>
          <th class="<?=$tableStyle;?>" ><strong><? echo !empty($row['display_name']) ? $row['display_name'] : $row['first_name'].'&nbsp'.$row['last_name'];?></strong></th>
          <th class="<?=$tableStyle;?>"><? echo ucfirst(strtolower($row['concept_name']));?></th>
          
          
          
      		<!-- <th class="<?=$tableStyle;?> permission-box">
      			<p><a class='inline' href="#group_permissions_<?=$row['id']?>">Group Permissions</a></p>
      		</th> -->
      		<!-- <th class="<?=$tableStyle;?> permission-box">
      			<p><a class='inline' href="#concept_permissions_<?=$row['id']?>">Concept Permissions</a></p>
      		</th> -->
      		<th class="<?=$tableStyle;?> permission-box">
      			<select id="role<?=$row['id']?>" name="roles" class="roles form-text-dropdown">
            <?php
            foreach($roles as $role)
            {
              if($row['role_id'] == $role->id)
              {
               $selected = "selected";
              }
              else
              {
              $selected = "";
              }
              ?>		
      			<option value="<?=$role->id?>" <?=$selected?>><?=$role->name?></option>
      			<?php
      			}
      			?>
      			</select>
      		</th>
        </tr>

        <?
		$i++; 
		
		}} 
	}
	else
	{
	?>
	<tr>
	 <th class ="odd" colspan="15" align="right">NO data present</th>
	</tr>
	<?php
	}
		
		
		?>
         <tr>
          <th colspan="15" align="right"><ul class="pagination"> <?php echo $navlinks; ?> </ul></th>
        </tr>
      </table>
     </form>
    </div>
  </div>
</div>

		
			
<?php
if(isset($permission_details['Users']))
{
 if(count($permission_details['Users']) > 0) {
            $i = 1;
            foreach($permission_details['Users'] as $row){
			?>
		<div style='display:none'>
				<div id='group_permissions_<?=$row['id']?>' style='padding:10px; background:#fff;'>

	 <form name="form" id= "target" method="post" action="<?=site_url("manage/group_permissions")?>"> 
	 <input type="hidden" name="user_id" value="<?=$row['id']?>">
				 <h2><? echo $row['name']." ". $row['surname'];?> Groups</h2>
					<div>
					
						<input type="text" id="demo-input-<?=$row['id']?>" name="blah" />
						<input type="submit" value="Submit" />
						<script type="text/javascript">
						$(document).ready(function() {
							$("#demo-input-<?=$row['id']?>").tokenInput("<?=base_url();?>manage/auto_group",{
							theme: "facebook",
<?php if(isset($row['group']))		
{		?>
			prePopulate:/* [
				
                    {id: 123, name: "Slurms MacKenzie"},
                    {id: 555, name: "Bob Hoskins"},
                    {id: 9000, name: "Kriss Akabusi"}
                ]*/
				<?php print $row['group']; }?>
				
            });
						});
						
						 
						</script>
					</div>
				</form>
			</div>
			<div id='concept_permissions_<?=$row['id']?>' style='padding:10px; background:#fff;'>
			<?php echo form_open('manage/concept_permissions/');?>
			<input type="hidden" name="user_id" value="<?=$row['id']?>">
					<h2><? echo $row['name']." ". $row['surname'];?> Consepts</h2>
						<?php
						foreach($concept_list as $concept){
						if(isset($gcpermission))
						{
							foreach($gcpermission as $per)
							{
								if(($per->object_id == $concept->id) && ($row['id'] == $per->user_id) && ($per->object_type == 'Concept'))
								{
									$checked="checked";
									break;
								}
								else
								{
									$checked="";
								}
							}
						}
						?>
							
							<input class="selectBox" name="concept[]" type="checkbox" value="<? echo $concept->id;?>" <?=$checked?>/>
							<strong><? echo $concept->name;?></strong></br>
						
						<?php
						}
						
						?>
					
					<!--<a class="ajax" href="group_permissions/<?//$row['id']?>">Allow</a></p>-->
					<input type="submit" name="submit" value="Submit">
					</form>
			</div>
		</div>
		<?php
		}
}
}
?>
<script type="text/javascript">

	$( ".roles" ).change(function() {
 
  var str_array = this.id.split('role');
   var user = str_array[1];
   var role = $('#'+this.id).val();
   $.ajax({
        type:"POST",
        url : "<?=base_url()?>manage/role_toggle",
        data :  {'user': user , 'role': role},
        success : function(response) {
            //data = response;
            //return response;
			 //alert(response);
        },
        error: function() {
            //alert('Error occured');
        }
    });
});

	
    $('.toggle-status-link').click(function(){
        var the_link = $(this);
        var href = the_link.attr('href');
        $.post(href,{},function(data){
            if(data.result == 1){
                the_link.html(data.html);
            }else{
                alert('Sorry, there was an error while updating permissions. Please try again later');
            }
        },'json');
        return false;
    });
    $('a[rel*=facebox]').facebox({closeImage:'<?=base_url()?>images/facebox/closelabel.gif',loadingImage:'<?=base_url()?>images/facebox/loading.gif'}) 
    
			$(document).ready(function(){
			$(".inline").colorbox({inline:true, width:"50%"});
			
		   $("input[type=button]").click(function () {
		  // $('#target').submit();
            //alert("Would submit: " + $(this).siblings("input[type=text]").val());
        });
				});
</script>
<!-- This contains the hidden content for inline calls -->
	<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  	
</body>
</html>