<?php
  $user_permission = $this->session->userdata('permissions');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?= base_url(); ?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.form.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/manage/manage_events_create.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/facebox_manage.css" />
    <script type="text/javascript" src="<?= base_url(); ?>js/facebox.js"></script>

  </head>
  <body>
    <div class="wrapper">
      <?php
      $data['active_tab'] = 'events';
      $data['user_name'] = $user_name;
      $this->load->view('includes/manage_header', $data);
      ?>
      <div class="content events-content">
        <div class='top-heading'>
          <h1>Events</h1>
        </div>
        <?php if ($this->session->flashdata('status_ok')): ?>
          <div class="msg-ok">
          <?= $this->session->flashdata('status_ok'); ?>
        </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('status_error')): ?>
            <div class="msg-ok">
          <?= $this->session->flashdata('status_error'); ?>
          </div>
        <?php endif; ?>
            <form method="post" action="<?= site_url("manage/update_event") ?>" id="frm-update-event" class="advanced-search">
              <div id="create_content" class="main-table form">
                <div class="form-title">Title <span id="error_txt_title" class="error-msg"></span></div>
                <input type="text" name="txt_title" id="txt_title" class="form-text" value="<?php echo $event_details['title']; ?>"/>
                <div class="form-title">Description <span id="error_txtar_desc" class="error-msg"></span></div>
                <div class="txt_body_holder"><textarea rows="6" cols="105" name="txtar_desc" id="txtar_desc" ><?php echo $event_details['description']; ?></textarea></div>
                <div class="form-title">Event Details</div>

                <div class="form-title">WHEN? <span id="error_date_time" class="error-msg"></span></div>
                <div class="form-area">
                  <input type="text" name="txt_date" id="txt_date" value="<?php echo date('d/m/Y', $event_details['event_datetime']); ?>" />
                  <input type="text" name="txt_time" readonly="readonly" id="txt_time" style="margin-left:20px;" value="<?php echo date('g:ia', $event_details['event_datetime']); ?>" />
                </div>

                <div class="form-title">TO?</div>
                  <div class="form-area">
                    <input type="text" class="date_field" name="txt_to_date" value="<?php echo ($event_details['event_end_datetime'])? date('d/m/Y', $event_details['event_end_datetime']):'Pick a date'; ?>" readonly="readonly" id="txt_to_date" />
                    <input type="text" name="txt_to_time" class="time_field" readonly="readonly"  value="<?php echo ($event_details['event_end_datetime'])? date('g:ia', $event_details['event_end_datetime']):'Pick a time'; ?>" id="txt_to_time" style="margin-left:20px;" />
                  </div>




                <div class="form-title">Location <span id="error_cmb_location" class="error-msg"></span></div>
                <select id="cmb_location" name="cmb_location" class="form-text-dropdown  form-area">
                  <option value="0">Select Location</option>
                 
				<?php

				if (count($location)): foreach ($location as $details):
					$id = $details['id'];
					$name = $details['name'];
					if ($event_details['event_location'] == $id):
					?>
						<option value="<?=$id?>" selected="selected"><?=$name?></option>
					<?php
					else: 
					?>
						<option value="<?=$id?>"> <?=$name?></option>
					<?php
					endif;
					endforeach;
				endif;
				?>
                </select>
            <div class="form-title">Concept</div>
            <select id="cmb_concept" name="cmb_concept" class="form-text-dropdown  form-area">
              <option value="0">Select Concept</option>
              <?php

				if (count($concepts)): foreach ($concepts as $details):
					$id = $details['id'];
					$name = $details['name'];
					if ($event_details['concept_id'] == $id):
					?>
						<option value="<?=$id?>" selected="selected"><?=$name?></option>
					<?php
					else: 
					?>
						<option value="<?=$id?>"> <?=$name?></option>
					<?php
					endif;
					endforeach;
				endif;
				?>
            </select>
            
            <input type="hidden" name="event_id" value="<?php echo $event_details['id']; ?>" />
           

                  <div class="clear"></div>
                </div>
                <div class="button-holder">
                  <div style="float:right;">
                    <input type="hidden" name="save" id="btn_save" value="" />
                    <a href="#" class="previewLink" style="display: block; float:left;margin-right:10px; margin-top:6px; background:url(<?= base_url() ?>images/moderation/preview.gif) no-repeat; height:20px; padding-left:20px; color:#1f5987; font-weight:bold; text-decoration:underline">Preview</a>
                    <?php if($event_details['event_status'] == 2 && $user_permission['review']):  ?>
                    <input id="reject_events" class="ItemButtonSave submit_entry" name="btn_save" type="submit" value="Reject" />
                    <?php endif; ?>
                    <input id="event-submit" class="ItemButtonSave submit_entry" name="btn_save" type="submit" value="<?php echo ($user_permission['review'])? 'Publish Now':'Submit for Review' ; ?>" />
                  </div><div class="clear"></div>
                </div>
              </form>
            </div>
          </div>
          <script type="text/javascript">
            $('.previewLink').click(function(){
              var options = {
                success:       showResponse,
                url:'<?= site_url('manage/preview_event'); ?>',
          dataType : 'json'
        };
        $('#frm-update-event').ajaxSubmit(options);
        return false;
      });

      // post-submit callback
      function showResponse(responseText, statusText, xhr, $form)  {
        var location = $('#cmb_location option:selected').text();
        var event_title = $('#txt_title').val();
        var event_description = $('#txtar_desc').val();
        var html =     '<div id="preview-box"><div class="heading-box"><h2 id="preview-news-title">'+event_title+'</h2></div><div class="container"><div class="details-attr"><span class="details-attr-title"><strong>Date: </strong></span><span class="details-attr-res" id="preview-news-date">'+responseText.event_date+'</span> | <span class="details-attr-title"><strong>Time: </strong></span><span class="details-attr-res" id="preview-news-time">'+responseText.event_time+'</span> | <span><strong>Location:</strong> '+location+'</span> </div><div  class="details-desc-box news"><div class="details-desc"><div class="details-desc-title"><h2>Description</h2></div><p id="preview-news-description">' + event_description + '</p></div>'+ responseText.album_details + '</div></div></div>';
        jQuery.facebox(html);
      }
    </script>
	  <!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
  </body>
</html>
