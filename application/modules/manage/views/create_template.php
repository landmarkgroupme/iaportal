<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/facebox_manage.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?= base_url(); ?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.limit.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.form.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/custom_js/manage/manage_post_content.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/facebox.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/nicEdit_admin.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" language="javascript" src="<?= base_url(); ?>js/swfobject.js"></script>
    <!-- Includes for uploadify -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/uploadify/uploadify.css" />
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
    <!-- Includes for uploadify ends -->
    <script type="text/javascript">
      bkLib.onDomLoaded(function() {
        //nicEditors({iconsPath : }).allTextAreas()
        new nicEditor({
            iconsPath : '<?= base_url(); ?>images/nicEditorIcons.gif',
            buttonList : ['image','upload','link','unlink'],
            uploadURI : '<?= base_url() ?>lib/nicUpload.php'
           
          }).
            panelInstance('txt_body');
         });
    </script>
    <script type="text/javascript" language="javascript">
      var ajaxUploadifyProcessing = false;
      var uploadifyTimer = null;
      function guessUploadify(){
        if(!ajaxUploadifyProcessing) {
          //alert('done all');
          clearTimeout(uploadifyTimer);
         window.location = 'photos';
        } else {
          uploadifyTimer = setTimeout(guessUploadify, 1000);
        }
          
        
      }

      $(document).ready(function(){
           var tmpFolderName =  $('#tmpfoldername').val();
          $.post('<?php echo site_url('manage/create_temp_folder');?>',{tmpfolder:tmpFolderName},function(data) {
          folderExist = true;
        });

        $("#upload").uploadify({
          'auto':true,
          'uploader': '<?php echo base_url(); ?>js/uploadify/uploadify.swf',
          'script': '<?php echo base_url(); ?>js/uploadify/uploadify.php',
          'cancelImg': '<?php echo base_url(); ?>js/uploadify/cancel.png',
          'folder': '../phpalbum/albums/'+tmpFolderName,
          'fileDesc': 'Image Files',
          'fileExt': '*.jpg;*.jpeg;*.gif;*.png',
          'multi': true,
		      'sizeLimit': '1024000',
          'removeCompleted' : false,
          'buttonText': 'Select photos',
          buttonImg: '<?php echo base_url(); ?>images/select-photo.png',
          onError : function (a, b, c, d) {
            if (d.status == 404)
              alert('Could not find upload script.');
            else if (d.type === "HTTP")
              alert('error '+d.type+": "+d.status);
            else if (d.type ==="File Size")
              alert(c.name+' '+d.type+' Limit: '+Math.round(d.info/1000)+'KB');
            else
              alert('error '+d.type+": "+d.text);
          },
          onComplete   : function (event, queueID, fileObj, response, data) {
            var entry_type = $("input[name='entry_category']:checked").val();
            if (entry_type == 'photos') {
              tmp_name = $('#tmpfoldername').val();

              //Creating the folder with tmp name
              var folderExist = false;
              if (!folderExist) {
                setTimeout(function() {
                  $.post('<?php echo site_url('manage/create_temp_folder');?>',{tmpfolder:tmp_name},function(data) {
                    folderExist = true;
                  });
                }, 5000);
              }
              var album_folder =  tmp_name;
              var album_id = tmp_name;
            }else{
              var album_folder =  $('#album_folder').val();
              var album_id = $('#album_id').val();
            }
            ajaxUploadifyProcessing = true;
            $.post('<?php echo site_url('manage/uploadify');?>',{filearray: response,album_folder: album_folder,album_id : album_id},function(info){
              //$('#uploaded-photos-title').show();
              //$("#target").append(info);  //Add response returned by controller
              //alert(info)
              ajaxUploadifyProcessing = false;
            });
          },

          onAllComplete: function(){
            album_id = $('#album_id').val();
            publish_status = $('#alb_publish_status').val();
            txt_title = $('#txt_title').val();
            news_create_flag =  $('#news_create_flag').val();
            var btn_text = $.trim($("#submit_content").val()); //general
            if (entry_type != 'photos') {
              if(publish_status == 0 ){
                $.ajax({
                  type: 'POST',
                  url: '<?php echo site_url('manage/publish_album');?>',
                  data: {album_id : album_id},
                  async: false,
                  dataType: 'json',
                  success: function(result){
                    //alert('ajax call')
                    //window.location ='photos';
                    location.reload();
                  }
                });
              }
            }
            if(news_create_flag == 'yes'){
              $('#category_news').attr('checked','checked');
              $('.custom-form-element').hide();
              $('#title-box').show();
              $('#excerpt-box').show();
              $('#body-box').show();
              $('#album-box').show();
              $('#email-notify-box').show();
              $('#save_content').show();
              $('.help-info').hide();
              $('#help-text').show();
              $('#news-help').show();
              $('#selected-content-type').html('News');
              $('#txt_album').append('<option value = "'+album_id+'" selected="selected">'+txt_title+'</option>');
              $('#news-ablum-created-status').show();

              $("#submit_content").css("background","url(<?php echo base_url()?>images/moderation/btn-bg-hover.gif) repeat-x");
              $("#submit_content").css("border",'1px solid #000000');
              $("#submit_content").css("width","auto");
              $(".submit_entry").removeAttr("disabled");
              $('#submit_content').val($('#submit_content').attr('title'));

              $("#save_content").css("background","url(<?php echo base_url()?>images/save_draft.jpg) repeat-x");
              $("#save_content").css("border",'1px solid #939393');
              $("#submit_content").css("width","auto");
              $('#save_content').val('Save as draft');
              return false;
            }else{
              guessUploadify();
            }
         }

        });

        $("#albums-list").change(function(){
          var base_location = '<?php echo $album_upload_location; ?>';
            var album_folder =  $("#albums-list option:selected").attr('id');
            var upload_folder = base_location + album_folder;
	    
            $('#upload').uploadifySettings('folder',upload_folder);
            });
            $('#clear_queue').click(function(){
			//$('#upload').uploadify('cancel','*');
            $('#upload').uploadifyClearQueue();
			$("#uploadQueue").html('');
            return false;
            })

            });
    </script>
    <style type="text/css">
        #target {
            overflow:hidden;
            padding:0px;
            width:555px;
        }
        .uploadify_result {
            margin:5px;
        }
    </style>
  </head>
  <body>
    <div class="wrapper">
      <?php
      $data['active_tab'] = '';
      $data['user_name'] = $user_name;
      $this->load->view('includes/manage_header', $data);
      ?>
      <div class="content">
        <div class="top-heading">
          <h1>Post new activity</h1>
        </div>

        <?php if ($this->session->flashdata('status_ok')): ?>
          <div class="msg-ok">
          <?= $this->session->flashdata('status_ok'); ?>
        </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('status_error')): ?>
            <div class="msg-ok">
          <?= $this->session->flashdata('status_error'); ?>
          </div>
        <?php endif; ?>
            <form action="<? site_url("manage/createnews") ?>" enctype="multipart/form-data" method="post" id="frm_post">
            <div id="help-text">
              <div id="news-help" class='help-info'>
                <p><strong>News</strong> consists of important initiatives by different teams, information about store openings and achievements by Landmark Group employees.</p>
              </div>
              <div id="announcement-help" class='help-info'>
                <p><strong>Announcements</strong> are very important messages about an upcoming holiday, a policy change or a message from top management.</p>
              </div>
              <div id="photo-help" class='help-info'>
                <p><strong>Photos</strong> document large or small events held by any team in the company. </p>
              </div>
              <div id="events-help" class='help-info'>
                <p><strong>Events</strong> apply to all employees of the company, and should be posted at least 1 week in advance.</p>
              </div>
              <div id="offers-help" class='help-info'>
                <p><strong>Offers</strong> include sales and promotions from various concepts of the Landmark Group.</p>
              </div>
            </div>
              <div id="create_content" class="main-table form" style="width:700px;">
                <div id="content-type-selector">
                  <p class='content-type'><strong>Select an activity type:</strong></p>
                  <ul id="content-type-selection">
              <?php if ($user_permissions['news']): ?>
                <li><input class="post_content_type" type="radio" name="entry_category" id="category_news" value="news" /> <label for="category_news">News</label></li>
              <?php endif; ?>
              <?php if ($user_permissions['announcements']): ?>
                  <li><input class="post_content_type" type="radio" name="entry_category" id="category_announcements" value="announcement" /> <label for="category_announcements">Announcements</label></li>
              <?php endif; ?>
              <?php if ($user_permissions['photos']): ?>
                    <li><input class="post_content_type"  type="radio" name="entry_category" id="category_photos" value="photos" /> <label for="category_photos">Photos album</label></li>
              <?php endif; ?>
              <?php if ($user_permissions['events']): ?>
                      <li><input  class="post_content_type" type="radio" name="entry_category" id="category_events" value="events" /> <label for="category_events">Events</label></li>
              <?php endif; ?>
              <?php if ($user_permissions['offers']): ?>
                        <li><input  class="post_content_type"  type="radio" name="entry_category" id="category_offers" value="offers" /> <label for="category_offers">Offers</label></li>
              <?php endif; ?>
                    </ul>
                      </div>
                      <div id="content_post_form" style="display:none;">
                      <p id="selected-content-type">News</p>
                        <div id ="title-box" class="custom-form-element">
                          <div class="counter">Characters left: <span id="charsLeft"></span></div>
                          <div class="form-title">Title <span id="error-title" class="error-msg"></span></div>
                          <input type="text" name="txt_title" id="txt_title" class="form-text" style="width:670px;" value="" />
                        </div>
                        <div id="excerpt-box" class="custom-form-element">
                          <div class="counter">Characters left: <span id="charsLeft2"></span></div>
                          <div class="form-title">Excerpt <span id="error-excerpt" class="error-msg"></span></div>
                          <textarea rows="6" name="txt_excerpt" id="txt_excerpt"  style="width:670px;" class="form-text"></textarea>
                        </div>
                        <div id="body-box" class="custom-form-element">
                          <div class="form-title">Body <span id="error-body" class="error-msg"></span></div>
                          <div class="txt_body_holder"><textarea rows="12" style="width:682px;" name="txt_body" id="txt_body" ></textarea></div>
                          <div style="padding-left:7px;color:#5e5e5e;margin-top:5px;">Maximum image width: 560 pixels and maximum file size: 70kb</div>
                          <div id="offer-info" class="custom-form-element" style="padding-left:7px;margin-top:10px;" class="offer-page">NOTE: The offer body will be center-aligned in the offer's page</div>
                        </div>

                        <div id="alb-body-box" class="custom-form-element">
                          <div class="form-title">Description <span id="error-alb-body" class="error-msg"></span></div>
                          <div class="txt_body_holder"><textarea rows="3" style="width:670px;" name="alb_txt_body" id="alb_txt_body" ></textarea></div>
                        </div>

                        <div id="event-body-box" class="custom-form-element">
                          <div class="form-title">BODY <span id="error-event-body" class="error-msg"></span></div>
                          <div class="txt_body_holder"><textarea rows="6" style="width:670px;" name="event_txt_body" id="event_txt_body" ></textarea></div>
                        </div>

                        <div id="album-box" class="custom-form-element">
                          <div class="form-title">Linked Album</div>
                          <div class="form-area">
                            <select id="txt_album" name="txt_album" class="form-text-dropdown">
                              <option value="0">- Select an existing album -</option>
							<?php
							if (count($albums)):
								foreach ($albums as $details):
									$ids = $details['aid'];
									$name = $details['title'];
								?>
									<option value="<?=$ids?>"><?=$name?></option>
								<?php
								endforeach;
							endif;
							?>
                      </select>
                      &nbsp;&nbsp;OR&nbsp;&nbsp;<span style="color:#1E5886;">+</span> <a id="create-photo-album" href="#">Create a new album</a>
                      <div id="news-ablum-created-status">You have successfully created the album</div>
                    </div>
                  </div>
                      <!--<div class="form-title"><input type="checkbox" name="featured_post" value="featured_post">Featured Post</div>-->

                  <div id="date-period-box" class="custom-form-element offer-page" >
                    <div class="form-title">OFFER PERIOD</div>
                    <div class="form-area">
                      <input type="text" class="date_field" name="txt_date_start" value="Start date" readonly="readonly" id="txt_date_start" />
                      <input type="text" class="date_field" name="txt_date_end"  value="End date" readonly="readonly" id="txt_date_end" style="margin-left:20px;" />
                      <span id="error_offer_date_time" class="error-msg"></span>
                    </div>
                  </div>
                  
                  <div id="offer-notification-box" class="custom-form-element offer-page" >
                    <?if($offer_concepts):?>
                    <div class="form-title">Select the concept that the Offer belongs to <span id="error_offer_notification" class="error-msg"></span></div>
                    <div class="form-area">
                      <select id="offer-notification" name="offer-notification" class="form-text-dropdown">
                        <option value="0">- Select a concept -</option>
						<?php
							if (count($offer_concepts) > 0):
								$count = 0;
								foreach ($offer_concepts as $row):
									if($row['show_in_offer'] == 2):
									continue;//Skip flagged concepts
									endif;
									?>
									<option value="<?=$row['id']?>"><?=$row['name']?></option>
									<?php
								endforeach;
							endif;
						?>
                      </select>
                    </div>
                      <?php else:?>
                      <p>NOTE: Email notification would be sent to people who have subscribed for <?=$user_details['concept']?> under their offer preferences.</p>
                      <input type="hidden" id="offer-notification" name="offer-notification" value="<?=$user_details['concept_id']?>" />
                      <?php endif;?>
                  </div>

                  <div id="datetime-period-box" class="custom-form-element event-page" >
                    <div class="form-title">WHEN?  <span id="error_date_time" class="error-msg"></span></div>
                    <div class="form-area">
                      <input type="text" class="date_field" name="txt_date" value="Pick a date" readonly="readonly" id="txt_date" />
                      <input type="text" name="txt_time" readonly="readonly"  value="Pick a time" id="txt_time" style="margin-left:20px;" />
                      &nbsp;&nbsp;<span class="event-sep" style="color:#1E5886;">+</span> <a id="show-end-date" href="#">Add end date</a>
                    </div>
                  </div>
                  <div id="datetime-to-box" class="custom-form-element event-page" >
                    <div class="form-title">TO?</div>
                    <div class="form-area">
                      <input type="text" class="date_field" name="txt_to_date" value="Pick a date" readonly="readonly" id="txt_to_date" />
                      <input type="text" name="txt_to_time" class="time_field" readonly="readonly"  value="Pick a time" id="txt_to_time" style="margin-left:20px;" />
                    </div>
                  </div>

                  <div id="location-concept-box" class="custom-form-element event-page" >
                    <div class="form-title">WHERE?</div>
                    <div class="form-area">
                      <select id="cmb_location" name="cmb_location" class="form-text-dropdown">
                        <option value="0">- Select a location -</option>
                    <?php if (count($cmb_location)): foreach ($cmb_location as $loc): ?>
                            <option value="<?= $loc['id'] ?>"><?= $loc['name'] ?></option>
                    <?php endforeach;
                          endif; ?>
                        </select>
                        <select id="cmb_concept" name="cmb_concept" class="form-text-dropdown"  style="margin-left:20px;">
                          <option value="0">- Select a concept -</option>
                    <?php if (count($cmb_concepts)): foreach ($cmb_concepts as $cp): ?>
                              <option value="<?= $cp['id'] ?>"><?= $cp['name'] ?></option>
                    <?php endforeach;
                            endif; ?>
                          </select>
                          <span id="error_cmb_location" class="error-msg"></span>
                        </div>
                      </div>

                      <div id="album_category-period-box" class="custom-form-element album-page" >
                        <div class="form-title">ALBUM CATEGORY <span id="error_album_category" class="error-msg"></span></div>
                        <div class="form-area">
                          <select id="album_category_id" name="album_category_id" class="form-text-dropdown">
                            <option value="0">- Select a category -</option>
                    <?php if (count($cmb_album_category)): foreach ($cmb_album_category as $alb_cat): ?>
                                <option value="<?= $alb_cat['cid'] ?>"><?= $alb_cat['name'] ?></option>
                    <?php endforeach;
                              endif; ?>
                            </select>
                          </div>
                        </div>

                        <div id="add-photo-box" class="custom-form-element album-page" >
                          <input type="hidden" id="tmpfoldername" name="tmpfoldername" value="<?php echo date('dmHis',time());?>">
                          <input type="hidden" name="album_folder" id="album_folder" />
                          <input type="hidden" name="album_id" id="album_id" />
                          <input type="hidden" name="alb_publish_status" id="alb_publish_status" />
                          <input type="hidden" name="news_create_flag" id="news_create_flag" val="no" />

                          <div class="form-title">ADD PHOTOS <span id="error_album_photos" class="error-msg"></div>
                          <div class="form-area" style="background-color:#FFFFFF;padding: 10px;width:500px;">
                            <a href="#" id="clear_queue" style="color:#ff0000;position: absolute;margin-left: 125px;margin-top: 4px;">Clear queue</a> <span id="upload"></span>
                          </div>
                        </div>

                        <input name="txt_publish" id="txt_publish" type="hidden" value="1" />
                        <div id="email-notify-box" class="custom-form-element">
                          <div class="form-title">Who would you like to notify VIA EMAIL</div>
                          <div class="email-holder" style="margin-left: 5px;width: 693px;">
                            <div class="email-area-c">
                              <div class="email-title"><strong>Concepts</strong> <span>(<a id="concAll" href="javascript:;">All</a> | <a id="concNone" href="javascript:;">None</a></span>)</div>
                    <?php $i = 0;
					if (count($concepts)): foreach ($concepts as $details):
					$ids = $details['id'];
					$name = $details['name'];
					if($ids == 5) // default coparate concept selected
					{
					$checked = "checked";
					}
					else
					{
					$checked ='';
					}
					?>
					<div class="concept"><input id="conc<?=$i?>" name="conc[]" type="checkbox" value="<?=$ids?>" class="conc" <?=$checked?> /><label for="conc<?=$i?>" class="pButton"><?= ucfirst(strtolower($name))?></label></div>
					<?php 
					$i++;
					endforeach;
						endif;
					?>
                            </div>
                            <div class="email-area-t">
                              <div class="email-title"><strong>Territories</strong> <span>(<a id="terrAll" href="javascript:;">All</a> | <a id="terrNone" href="javascript:;">None</a>)</span></div>
                    <?php $i = 0;
					if (count($location)): foreach ($location as $details):
					$ids = $details['id'];
					$name = $details['name'];
					?>
					<div class="location"><input id="terr<?=$i?>" name="terr[]" type="checkbox" value="<?=$ids?>" class="terr" /><label for="terr<?=$i?>" class="pButton"><?=ucfirst(strtolower($name))?></label></div>
					<?php
					$i++;
					endforeach;
						endif;
					?>
                            </div>
                            <div class="email-area-g">
                              <div class="email-title"><strong>Grades</strong> <span>(<a id="gradAll" href="javascript:;">All</a> | <a id="gradNone" href="javascript:;">None</a>)</span></div>
					<?php $i = 0;
					if (count($grades)): foreach ($grades as $details):	
					$ids = $details['id'];
					$name = $details['name'];					
					?>
					<div class="grade"><input id="grad<?=$i?>" name="grad[]" type="checkbox" value="<?=$ids?>" class="grad" /><label for="grad<?=$i?>" class="pButton"><?=ucfirst(strtolower($name))?></label></div>
					<?php
					$i++;
					endforeach;
						endif;
					?>
					
                            </div>
                          </div>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>

                    <div class="button-holder" style="width:713px;display:none;">
					<?php
					if(isset($_SERVER['HTTP_REFERER']))
					{
					$cancel =  $_SERVER['HTTP_REFERER'];
					}
					else
					{
					$cancel =  site_url("manage/news");
					}
					?>
                      <a href="<?php echo $cancel;?>" class="clearLink" style="display: block; float:left; margin-left: 12px; margin-top:6px; color:#cf0b0b; font-weight:bold; text-decoration:underline">Back</a>
                      <div style="float:right;">
                        <a href="#" class="previewLink" style="display: block; float:left;margin-right:10px; margin-top:6px; background:url(<?= base_url() ?>images/moderation/preview.gif) no-repeat; height:20px; padding-left:20px; color:#1f5987; font-weight:bold; text-decoration:underline">Preview</a>
                        <input id="save_content" class="submit_entry ItemButtonDraft" name="save_draft" type="submit" value="Save as draft" />
                        <input id="submit_content" class="ItemButtonSave submit_entry" name="save" type="submit" title="<?php echo ($user_permissions['review'])? 'Publish Now':'Submit for Review' ; ?>" value="<?php echo ($user_permissions['review'])? 'Publish Now':'Submit for Review' ; ?>" />
                      </div><div class="clear"></div>
                    </div>
                  </form>

                </div>
                <input type="hidden" name="base_url" id="base_url" value="<?= base_url() ?>" />
                <script type="text/javascript">
                  $('.previewLink').click(function(){
                    var options = {
                      success:       showResponse,
                      url:'<?= site_url('manage/preview_mt_entry'); ?>'
                    };

                    var text = $(".nicEdit-main").html();
                    $("#txt_body").val(text);
                    $('#frm_post').ajaxSubmit(options);
                    return false;
                  });
                  // post-submit callback
                  function showResponse(responseText, statusText, xhr, $form) {
                      jQuery.facebox(responseText);
                  }
      </script>
    </div>
    <!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
  </body>
</html>
