<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/calendrical.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'photos';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
  <div class="content">
    <div class="top-heading">
      <h1>Photo Albums</h1>
    </div>
    <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
    <?php if(isset($_SESSION['status_ok'])):?>
        <div class="msg-ok">
          <?php echo $_SESSION['status_ok']; unset($_SESSION['status_ok']); ?>
        </div>
        <?php endif;?>
		<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
          <?=$this->session->flashdata('status_error');?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_del')):?>
      <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
    <?php endif;?>
    
    <div class="main-table">
    <form name="form" method="post" action="<?=site_url("manage/photos_bulk_action")?>">
      <table id="list-table" width="897" border="0" cellspacing="0">
         <tr>
          <th class="odd" colspan="7" style="margin-left:0px; padding-left:0px;">
          <div class="ArrowButton"></div>
          <div><input class="ItemButton" id="delete_item" name="Delete" type="submit" value="Remove" /></div>
          <div class="searchArea">Search: <input type="text" name="SearchText" id="SearchBox" placeholder="<?php echo "Search Albums";?>"  value="<?php echo ($search_term ? $search_term :'');?>"/></div>
          <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />
          <ul class="pagination"><? echo $navlinks; ?></ul></th>
        </tr>
        <tr>
          <th width="5"><input type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
          <th><strong>Title</strong></th>
          <th>Status</th>
          <th><strong>Photos</strong></th>
          <th><strong>Author (Concept)</strong></th>
          <th><strong>Created</strong></th>
          <th><strong>View</strong></th>
        </tr>
		<?php if(count($allAlbums) > 0) {
            $i = 1;
            foreach($allAlbums as $row){
			if($i % 2 == 0)	{ $tableStyle = "odd"; }else { $tableStyle = "even"; }
			?>
        <tr>
          <th width="5" class="<?=$tableStyle;?>"><input class="selectBox" name="sel[]" type="checkbox" value="<? echo $row['album_id'];?>" /></th>
          <th class="<?=$tableStyle;?>"><a href="<?php echo site_url('manage/edit_album').'/'.$row['album_id'];?>"><? echo $row['album_title'];?></a></th>
          <th width="150" class="<?=$tableStyle;?>">
          <? if($row['album_status'] == 0){?>
          <div class="post_status published">Live</div>
          <? }elseif($row['album_status'] == 1){?>
          <div class="post_status draft">Draft</div>
          <? }?>
          </th>
          <th class="<?=$tableStyle;?>"><?php echo $row['pictures_count'];?></th>
          <th class="<?=$tableStyle;?>"><? echo $row['name']." ". $row['surname']." (". ucfirst(strtolower($row['concept_name'])). ")"; ?></th>
          <th class="<?=$tableStyle;?>"><?php echo time_ago($row['picture_ctime']);?></th>
          <th class="<?=$tableStyle;?>">
            <?php if($row['album_status'] == 0):?>
            <a href="<?=site_url("about/view_photos/".$row['album_id'])?>" target="_blank"><img style="margin-left:9px" src="<?=base_url()?>images/moderation/view.gif" alt="View" /></a>
            <?php else:?>
            <img style="margin-left:9px" src="<?=base_url()?>images/moderation/view_disabled.gif" alt="View Disabled" />
            <?php endif;?>
          </th>
        </tr>
        <? $i++; }} else {?>

			  <tr>
                <th class ="odd" colspan="6" align="right">NO data present</th>
              </tr>

		<?php }?>
         <tr>
          <th colspan="7" align="right"><ul class="pagination"> <? echo $navlinks; ?> </ul></th>
        </tr>
      </table>
      </form>
    </div>
  </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  
</body>
</html>
