<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/calendrical.css" />
<script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
</head>
<body>
<div class="wrapper">
 <?php
        $data['active_tab'] = 'news';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
  <div class="content">
				<div class="top-heading">
					<h1>Edit News Item</h1>
				</div>
		<?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
		<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
				
				
				
				
				<form method="post" action="<?=site_url("manage/submit_events")?>" class="advanced-search">
  				<div class="main-table form">
  					<label for="txt_title">Title</label> <span id="error_txt_title" class="error-msg"></span><br />
  					<input type="text" name="txt_title" id="txt_title" />
  					
  					<br /><br />
  					<label for="txtar_desc">Description</label> <span id="error_txtar_desc" class="error-msg"></span><br />
            <textarea rows="6" cols="134" name="txtar_desc" id="txtar_desc"></textarea>
  				</div>
  				<div class="event-details"> 
            <div>EVENT DETAILS</div>
            <div class="event-date">
              <label for="txt_date">Date</label>
              <input type="text" name="txt_date" readonly="readonly" id="txt_date" />
              
              <label for="txt_time">Time</label>
              <input type="text" name="txt_time" readonly="readonly" id="txt_time" /> 
              <span id="error_date_time" class="error-msg"></span>
            </div>
            <br /><br />
            <label for="cmb_location">Location</label><span id="error_cmb_location" class="error-msg"></span><br />
            <select id="cmb_location" name="cmb_location">
              <option value="0">Select Location</option>
              <?=$location?>
            </select>
            <br /><br />
            <label for="cmb_concept">Concept</label><br />
            <select id="cmb_concept" name="cmb_concept">
              <option value="0">Select Concept</option>
              <?=$concepts?>
            </select>
            <br /><br />
            <label for="cmb_album">LINKED ALBUM</label><br />
            <select id="cmb_album" name="cmb_album">
              <option value="0">Select Album</option>
              <?=$albums?>
            </select>
        </div> 
				 <div class="submit-button">
          <a href="#">Clear</a>
          <div class="button-holder"><input type="image" src="<?=base_url()?>images/btn-send.jpg" id="event-submit" /></div>
         </div>
         <input type="hidden" name="hd_frm" id="hd_frm" value="1" />
       </form>
         
			</div>
</div>
<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>">
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  
</body>
</html>
