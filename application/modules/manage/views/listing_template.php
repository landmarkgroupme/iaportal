<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php

//print "<pre>";
//print_r($list_data);
//exit;
?>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
  </head>
  <body>
    <div class="wrapper">
      <?php
        $data['active_tab'] = strtolower($page_title);
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>

      <!-- #Content-->
      <div class="content">
        <div class="top-heading">
          <h1><?=$page_title?></h1>
        </div>
        
        <?php if($this->session->flashdata('status_ok')):?>
            <div class="msg-ok">
              <?=$this->session->flashdata('status_ok');?>
            </div>
            <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
            <div class="msg-error">
              <?=$this->session->flashdata('status_error');?>
            </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_del')):?>
          <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
        <?php endif;?>
        <!-- #Listing-->
        <div class="main-table">
          <form id="search_post" name="form" method="post" action="<?=site_url("manage/mt_delete_publish_action")?>">
            <table id="list-table" width="100%" border="0" cellspacing="0">
              <thead>
              <tr>
                <th class="odd" colspan="6" style="margin-left:0px; padding-left:0px;">
                  <div class="ArrowButton"></div>
                  <div><input class="ItemButton" id="delete_item" name="Delete" type="submit" value="Remove" /></div>
                  <div class="searchArea">Search: <input type="text" name="SearchText" id="SearchBox" placeholder="<?php echo "Search ".$page_title;?>"  value="<?php echo ($this->session->flashdata('SearchText')? $this->session->flashdata('SearchText'):'');?>"/></div>
                  <input type="hidden" name="mt_category" id="mt_category" value="<?php echo $mt_category;?>" />
                  <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />    
					<?php if($data['active_tab'] != 'offers') {?>
                  <div style="float:right;margin-top:5px;color:#000000;"><input id="show_review" <?php echo ($this->session->flashdata('show_review')? 'checked = "checked"':"" );?> name="show_review" type="checkbox" value="show_review" /> <label for="show_review"><strong>Only show items under review</strong></label></div>
				  <?php }
					if($data['active_tab'] == 'offers') {?>
                  <div style="float:right;margin-top:5px;color:#000000;"><input id="hide_expiry" <?php echo ($filterExpiry? 'checked = "checked"':"" );?> name="hide_expiry" type="checkbox" value="1" /> <label for="hide_expiry"><strong>Hide Expiry Offers</strong></label></div>
					<?php } ?>
                </th>
              </tr>
              <tr>
                <th width="5"><input  type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
                <th width="400"><strong>Title</strong></th>
                <th width="155">Status</th>
                <th width="200"><strong>Author (Concept)</strong></th>
                <th width="100"><strong>Created</strong></th>
                <th width="40"><strong>View</strong></th>
              </tr>
              </thead>
              <tbody>
			  <?php if (empty($list_data)){ ?>
			  <tr>
                <th class ="odd" colspan="6" align="right">NO data present</th>
              </tr>
			<?php } ?>
            <?php $i = 1; foreach($list_data as $row): if($i % 2 == 0)  { $tableStyle = "odd"; }else { $tableStyle = "even"; }?>
              <tr>
                <th class="<?=$tableStyle;?>" width="5"><input class="selectBox" name="sel[]" type="checkbox" value="<? echo $row['entry_id'];?>" /></th>
                <th class="<?=$tableStyle;?>" width="400"><a href="<?=site_url("manage/edit_".strtolower($page_title)."/".$row['entry_id'])?>"><? echo $row['entry_title'];?></a></th>
                <th class="<?=$tableStyle;?>" width="155"><?php
				switch ($row['entry_status']) {
				case 1: 
					echo '<div class="post_status draft">Draft</div>';
					break;
				case 2:
					echo '<div class="post_status published">Live</div>';
					break;
				case 3: 
					echo '<div class="post_status review">Under Review</div>';
					break;
				case 4: 
					echo '<div class="post_status rejected">Rejected</div>';
					break;
				default: 
					echo '<img style="margin-left:3px" src="' . base_url() . 'images/moderation/ico-question.gif" alt="unknown" />';
					break;
				}
				?></th>
                <th class="<?=$tableStyle;?>" width="200"><? echo $row['name']." ". $row['surname']." (". ucfirst(strtolower($row['concept_name'])). ")"; ?></th>
                <th class="<?=$tableStyle;?>" width="100"><? echo $row['entry_created_on'];?></th>
                <th class="<?=$tableStyle;?>" width="40">
                  <?php if($row['entry_status'] == 2):?>
                  <a target="_blank" href="<?=site_url(strtolower($page_title)."/".$row['entry_id'])?>"><img style="margin-left:9px" src="<?=base_url()?>images/moderation/view.gif" alt="View" /></a>
                  <?php else:?>
                  <img style="margin-left:9px" src="<?=base_url()?>images/moderation/view_disabled.gif" alt="View Disabled" />
                  <?php endif;?>
                </th>
              </tr>
               
            <?php $i++; endforeach;?>
              </tbody>
              
              <tr>
                <th colspan="6" align="right"><ul class="pagination"> <? echo $navlinks; ?></ul></th>
              </tr>
            </table>
          </form>
        </div>
       <!-- /Listing-->
      </div>
      <!-- /Content-->
    </div>
	<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>">
	<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
  </body>
</html>