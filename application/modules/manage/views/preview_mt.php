    <div id="preview-box">
		<div class="pop-win-header">
			Preview
			<span><a href="#"><img alt="close-win" src="<?=base_url()?>images/ico-50.jpg" class="pop-win-close"></a></span>
		</div>
	
        <div class="heading-box">
                <h2 id="preview-news-title"><?php echo $entry['entry_title'];?></h2>
        </div>
        <div class="container">
            <div class="details-attr">
                    <span class="details-attr-title"><strong>Date: </strong></span>
                    <span class="details-attr-res" id="preview-news-date"><?php echo date('l,F j, Y',strtotime($entry['entry_authored_on']));?></span> |
                    <span class="details-attr-title"><strong>Time: </strong></span>
                    <span class="details-attr-res" id="preview-news-time"><?php echo date('g:i a',strtotime($entry['entry_authored_on']));?></span>
            </div>
            <div  class="details-desc-box news">
                <div class="details-desc">
                        <div class="details-desc-title"><h2>Description</h2></div>
                            <p id="preview-news-description"><?php echo $entry['entry_text'];?></p>
                </div>
				<?php
				if (isset($album) && count($album)):
				?>
				<div class="album">
                      <a href="<?=$link?>"><img  src="<?=$thumbnail?>" alt="album" width="184" height="110" />
                      </a>
                      <div class="album-title"><?=$title?></div>
                      <div class="album-attr"><span class="album-date"><?=$ctime?></span><span class="pic-count">| <?=$total_pic?> Photos</span></div>
                    </div>
				<?php
				endif;
				?>
            </div>
        </div>
    </div>
