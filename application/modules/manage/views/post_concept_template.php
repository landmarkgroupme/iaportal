<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Landmark Intranet</title>
  <link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/facebox_manage.css" />
  <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.limit.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.form.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage_concept.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/nicEdit_admin.js"></script>
  <script type="text/javascript">
    bkLib.onDomLoaded(function() {
     //nicEditors({iconsPath : }).allTextAreas()
      new nicEditor({
        iconsPath : '<?=base_url();?>images/nicEditorIcons.gif',
        buttonList : ['image','upload','link','unlink'],
        uploadURI : '<?=base_url()?>lib/nicUpload.php'
      }).panelInstance('txt_body'); 
    });
  </script>
</head>
<body>
<div class="wrapper">
 <?php
		$this->load->helper('html');
		$selected = 'selected = "selected"';
        $data['active_tab'] = 'concept';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
  <div class="content">
    <div class="top-heading">
	<h1><?=isset($details['id'])?'Edit Concept':'Create Concept';?></h1>
    </div>

    <?php if($this->session->flashdata('status_ok')):?>
    <div class="msg-ok">
      <?=$this->session->flashdata('status_ok');?>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_error')):?>
    <div class="msg-ok">
      <?=$this->session->flashdata('status_error');?>
    </div>
    <?php endif;
	$attributes = array('class' => '', 'id' => 'frm_edit_concept');
	if(isset($details['id']))
	{
	$image_properties = array(
          'src' => 'images/concepts/thumbs/'.$details['thumbnail'],
          'alt' => $details['name'],
          'class' => 'post_images',
          'width' => '100',
          'height' => '100',
          'title' => $details['name'],
          'rel' => 'lightbox',
		  );
		  
	}
	?>
   <!-- <form action="<? //site_url("manage/post_concept")?>" method="post" id="frm_edit_concept"> -->
   	<?php echo form_open_multipart('manage/post_concept',$attributes);?>
      <input type="hidden" name="entry_category" id="entry_category" value="concept" />
	  <input name="concept_id" id="concept_id" type="hidden" value="<?=isset($details['id'])?$details['id']:'';?>" />
      <div class="main-table form">
        <div class="counter">Characters left: <span id="charsLeft"></span></div>
        <div class="form-title">Title <span id="error-title" class="error-msg"></span></div>
        <input type="text" name="txt_title" id="txt_title" class="form-text" value="<?=isset($details['name'])?$details['name']:'';?>" />  
       <div class="form-title">thumbnail <span id="userfile" class="error-msg"></span></div>
		<?php
		if(isset($details['id']))
		{ 
			echo img($image_properties);
		}
		?>
		
		<input type="file" name="userfile" id= "userfile" size="20" />
		<?php if(!isset($details['id']))
		{
		?>
		<div class="form-title">Default thumbnail</div>
		<input type="radio" name="default_image" value="default/1.png"><img src="<?php echo base_url();?>images/groups/thumbs/default/1.png" alt="Smiley face" width="50" height="50">
		<input type="radio" name="default_image" value="default/2.png"><img src="<?php echo base_url();?>images/groups/thumbs/default/2.png" alt="Smiley face" width="50" height="50">
		<input type="radio" name="default_image" value="default/3.png"><img src="<?php echo base_url();?>images/groups/thumbs/default/3.png" alt="Smiley face" width="50" height="50">
		<input type="radio" name="default_image" value="default/4.png"><img src="<?php echo base_url();?>images/groups/thumbs/default/4.png" alt="Smiley face" width="50" height="50">
		<?php } ?>
        <div class="form-title">Body <span id="error-body" class="error-msg"></span></div>
        <textarea rows="12" name="txt_body" id="txt_body" class="form-text"><?=isset($details['description'])?$details['description']:'';?></textarea>        
        <div style="padding-left:7px;">Maximum image width: 560 pixels and maximum file size: 70kb</div>
        
        <div class="form-title">Status</div>
        <div class="form-area">
          <select id="txt_status" name="txt_status" class="form-text-dropdown">
          <option value="">Select</option>
			<option value="active" <?=( isset($details['status']) && $details['status'] == 'active')?$selected:'';?>>Active</option>
			<option value="inactive" <?=(isset($details['status']) && $details['status']=='inactive')?$selected:'';?>>Inactive</option>
          </select>
        </div>
		<div class="form-title">Is Open</div>
        <div class="form-area">
          <select id="txt_open" name="txt_open" class="form-text-dropdown">
          <option value="">Select</option>
			<option value="yes" <?=(isset($details['is_open']) && $details['is_open'] == 'yes')?$selected:'';?>>Yes</option>
			<option value="no" <?=(isset($details['is_open']) && $details['is_open'] == 'no')? $selected:'';?>>No</option>
          </select>
        </div>
        <input name="txt_publish" id="txt_publish" type="hidden" value="1" />
		
        <div class="clear"></div>
      </div>     
            
      <div class="button-holder">
        <a href="<?=site_url("manage/group")?>" class="clearLink" style="display: block; float:left; margin-left: 12px; margin-top:6px; color:#cf0b0b; font-weight:bold; text-decoration:underline">Cancel</a>
        <div style="float:right;">
		<?php 
		if(isset($details['id']))
		{
		?>
          <input id="update_concept" class="ItemButtonSave submit_entry" name="save" type="submit" value="Update" />
		<?php 
		}
		else
		{
		?>
		<input id="submit_concept" class="ItemButtonSave submit_entry" name="save" type="submit" value="Submit" />
		<?php 
		}
		?>
        </div><div class="clear"></div>
      </div>
    </form>

  </div>
  <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>" />
    <script type="text/javascript">
        $('.previewLink').click(function(){
            var options = {
                success:       showResponse,
                url:'<?=site_url('manage/preview_mt_entry');?>'
            };
            
            var text = $(".nicEdit-main").html();
            $("#txt_body").val(text);
            $('#frm_create_news').ajaxSubmit(options);
            return false;
        });

        // post-submit callback
        function showResponse(responseText, statusText, xhr, $form)  {
            jQuery.facebox(responseText);
        } 
    </script>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>   
</body>
</html>
