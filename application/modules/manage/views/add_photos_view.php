<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <!-- Load the swf object for uploadify -->
    <script type="text/javascript" language="javascript" src="<?=base_url();?>js/swfobject.js"></script>
    <!-- End of swf object load -->
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>

    <!-- Includes for uploadify -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/uploadify/uploadify.css" />
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
    <!-- Includes for uploadify ends -->

    <script type="text/javascript" language="javascript">
        $(document).ready(function(){
            $("#upload").uploadify({
                uploader: '<?php echo base_url();?>js/uploadify/uploadify.swf',
                script: '<?php echo base_url();?>js/uploadify/uploadify.php',
                cancelImg: '<?php echo base_url();?>js/uploadify/cancel.png',
                folder: '<?php echo $album_upload_location;?>' + $("#albums-list option:selected").attr('id'),
                fileDesc: 'Image Files',
				fileExt: '*.jpg;*.jpeg;*.gif;*.png',
                scriptAccess: 'always',
                multi: true,
				sizeLimit: '1024000',
                auto : true,
                removeCompleted : false,
                onError : function (a, b, c, d) {
                                    if (d.status == 404)
                                        alert('Could not find upload script.');
                                    else if (d.type === "HTTP")
                                        alert('error '+d.type+": "+d.status);
                                    else if (d.type ==="File Size")
                                        alert(c.name+' '+d.type+' Limit: '+Math.round(d.info/1000)+'KB');
                                    else
                                        alert('error '+d.type+": "+d.text);
                            },
                onComplete   : function (event, queueID, fileObj, response, data) {
                                    //Post response back to controller
                                    var album_folder =  $("#albums-list option:selected").attr('id');
                                    var album_id = $("#albums-list").val();
                                    $.post('<?php echo site_url('manage/uploadify');?>',{filearray: response,album_folder: album_folder,album_id : album_id},function(info){
                                        $('#uploaded-photos-title').show();
                                        $("#target").append(info);  //Add response returned by controller
                                    });
                            }
            });

            $("#albums-list").change(function(){
                var base_location = '<?php echo $album_upload_location;?>';
                var album_folder =  $("#albums-list option:selected").attr('id');
                var upload_folder = base_location + album_folder;
                $('#upload').uploadifySettings('folder',upload_folder);
            });


        });
    </script>
    <style type="text/css">
        #target {overflow:hidden;padding:0px;width:555px;}
        .uploadify_result {margin:5px;}
    </style>
</head>
<body>
<div class="wrapper">
      <?php
        $data['active_tab'] = 'photos';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
    <div class="content">
        <div class="top-heading">
            <h1>Add Photos</h1>
        </div>
        <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
            <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
            <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
        <form action="<? site_url("manage/create_offers")?>" method="post" enctype="multipart/form-data" id="frm_create_news">
            <div id="create_content" class="main-table form">
                <div class="form-title">SELECT ALBUM <span id="error-title" class="error-msg"></span></div>
                
                <select name="album_id" id="albums-list" class="form-text">
					<?php
				if (count($albums_list)):
					foreach ($albums_list as $album):
						if ($album_id != null && $album_id == $album['aid']):
						?>
							<option id="<?=$album['keyword']?>" selected="selected" value="<?=$album['aid']?>"><?=$album['title']?></option>
						<?php
						else:
						?>
							<option id="<?$album['keyword']?>" value="<?=$album['aid']?>"><?=$album['title']?></option>
						<?php
						endif;
					endforeach;
				endif;
					?>
                </select>
                <input type="hidden" id="album-upload-location" value="/landmark/phpalbum/albums/" />
                <div style="padding-left:8px;">Maximum image width: 1000 pixels and maximum file size: 1024kb</div>
                <div class="form-title"><span id="error-title" class="error-msg"></span></div>

                <div class="form-area">
                    <?php echo form_upload(array('name' => 'Filedata', 'id' => 'upload'));?>
                </div>
                <div class="clear"></div>
                <div id="uploaded-photos">
                    <h2 id="uploaded-photos-title" style="display:none;">Uploaded photos</h2>
                    <ul id="target">
                        
                    </ul>
                </div>
            </div>
            <div class="button-holder">
                <a href="<?php echo site_url('manage/photos');?>" >Continue</a>
            </div>
        </form>
        <div style="display:none">
            <div id="previewBox" style="padding:10px; background:#fff;">
                <p><strong>This content comes from a hidden element on this page.</strong></p>
                <p>The inline option preserves bound JavaScript events and changes, and it puts the content back where it came from when it is closed.<br /></p>
            </div>
        </div>
    </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>