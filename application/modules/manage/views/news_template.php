<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/calendrical.css" />
<script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
</head>
<body>
<div class="wrapper">
    <?php
        $data['active_tab'] = 'news';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
  <div class="content">
    <div class="top-heading">
      <h1>News</h1>
    </div>
    <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
		<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_error');?>
        </div>
    <?php endif;?>   
    
    <div>
      <form action="<?=site_url("manage/createnews/")?>" method="post">
        <input class="ItemButton" name="Delete" type="submit" value="+ New News Item" />
      </form>
    </div>
    <div class="main-table">
    <form name="form" method="post" action="<?=site_url("manage/news")?>">
      <table width="100%" border="0" cellspacing="0">
         <tr>
          <th class="odd" colspan="6" style="margin-left:0px; padding-left:0px;">
          <div class="ArrowButton"></div>
          <div><input class="ItemButton ItemButtonHover" name="Publish" type="submit" value="Publish" /></div>
          <div><input class="ItemButton" name="Delete" type="submit" value="Delete" /></div>
          <div class="searchArea">Search: <input type="text" name="SearchText" id="SearchBox" value="Search News" /></div>
          <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />          
          <ul class="pagination"><? echo $navlinks; ?></ul></th>
        </tr>      
        <tr>
          <th width="5"><input class="selectBox" type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
          <th width="5"><img src="/landmark/images/moderation/flag.gif" alt="Flag" /></th>
          <th width="440"><strong>Title</strong></th>
          <th width="200"><strong>Author (Concept)</strong></th>
          <th width="150"><strong>Created</strong></th>
          <th width="40"><strong>View</strong></th>
        </tr>
		<?php if(count($allNews) > 0) {
            $i = 1;
            foreach($allNews as $row){ 
			if($i % 2 == 0)	{ $tableStyle = "odd"; }else { $tableStyle = "even"; }
			?>
        <tr>
          <th class="<?=$tableStyle;?>" width="5"><input class="selectBox" name="sel[]" type="checkbox" value="<? echo $row['entry_id'];?>" /></th>
          <th class="<?=$tableStyle;?>" width="5">
          <? if($row['entry_status'] == 2){?>
          <img style="margin-left:3px" src="/landmark/images/moderation/okay.gif" alt="Published" />
          <? } ?>
          </th>
          <th class="<?=$tableStyle;?>" width="440"><a href="editnews/<? echo $row['entry_id'];?>"><? echo $row['entry_title'];?></a></th>
          <th class="<?=$tableStyle;?>" width="200"><? echo $row['name']." ". $row['surname']." (". ucfirst(strtolower($row['concept_name'])). ")"; ?></th>
          <th class="<?=$tableStyle;?>" width="150"><? echo date_now($row['entry_created_on']);?></th>
          <th class="<?=$tableStyle;?>" width="40"><a target="_blank" href="<?=site_url("news/news_details/".$row['entry_id'])?>"><img style="margin-left:9px" src="/landmark/images/moderation/view.gif" alt="View" /></a></th>
        </tr> 
        <? $i++; }} ?>
         <tr>
          <th colspan="6" align="right"><ul class="pagination"> <? echo $navlinks; ?> </ul></th>
        </tr>
      </table>
      </form>
    </div>
  </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  
</body>
</html>
