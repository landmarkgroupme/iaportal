<html>
<head>
    <title>Add a user</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.core.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.widget.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.position.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.autocomplete.min.js"></script>
    <style type="text/css">
    .ui-autocomplete { position: absolute; cursor: default; }
    .ui-autocomplete-loading { background: #fff url('../images/loading.gif') right center no-repeat; }
    * html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

    .ui-menu {
            list-style:none;
            padding: 2px;
            margin: 0;
            display:block;
    }
    .ui-menu .ui-menu {
            margin-top: -3px;
    }
    .ui-menu .ui-menu-item {
            margin:0;
            padding: 0;
            zoom: 1;
            float: left;
            clear: left;
            width: 100%;
    }
    .ui-menu .ui-menu-item a {
            text-decoration:none;
            display:block;
            padding:.2em .4em;
            line-height:1.5;
            zoom:1;
    }
    .ui-menu .ui-menu-item a.ui-state-hover,
    .ui-menu .ui-menu-item a.ui-state-active {
            font-weight: normal;
            margin: -1px;
    }
    .ui-widget-content { border: 1px solid #aaaaaa/*{borderColorContent}*/; background: #ffffff; }
    .ui-widget-content a { color: #222222/*{fcContent}*/; }
    .ui-state-hover, .ui-widget-content .ui-state-hover,
    .ui-widget-header .ui-state-hover, .ui-state-focus,
    .ui-widget-content .ui-state-focus, .ui-widget-header
    .ui-state-focus {
            border: 1px solid #999999;
            background: #dadada url(../images/autocomplete-hover.png) 50% 50% repeat-x;
            font-weight: normal;
            color: #212121;
    }
    </style>
    <script type="text/javascript">
      $("#referesh-permission").click(function(){
        window.location= "<?=site_url("manage/permissions")?>";
      })
    </script>
</head>
<body>
<div style="margin-left:-20px;margin-top:0px;">
  <div style="float:right;margin-top:-15px;"><a style="text-decoration:underline;color:#3170a2;" href="#" id="referesh-permission" >Close</a></div>
  <h2 style="color:#4b4b4b;font-size:22px;font-weight:100;border-bottom:1px solid #ebebeb;padding-bottom:10px;">Add a User</h2>
  <div style="margin-bottom:30px;">
    <form method="post" action="<?=site_url('manage/make_user_as_admin')?>">
        <input type="text" name="user_name" id="user_name" style="width:250px;" /> <span class="error-msg" id="error-user_name"><?=form_error('user_name')?></span>
        <input type="hidden" name="user_id" id="user_id" />
        <input type="submit" value="Add" name="make_user_as_admin" class="ItemButtonHover"/>
    </form>
  </div>
</div>
<script type="text/javascript">
    var user_name_value = 'Start typing the name of the user';
    $('#user_name').val(user_name_value).css('color','#ccc').css('font-style','italic');
    $('#user_name').focus(function(){
        if($(this).val() == user_name_value){
            $(this).val('').css('color','#333').css('font-style','normal');
        }
    });
    $('#user_name').blur(function(){
        if($(this).val() == ''){
            $('#user_name').val(user_name_value).css('color','#ccc').css('font-style','italic');
        }
    });

    $("#user_name").autocomplete({
      source: function(request, response){
	      $.ajax({
				  url: '<?php echo site_url('manage/autocomplete_users_name');?>',
				  dataType: 'json',
				  type:"post",
				  data: {"term":extractLast(request.term)},
				  success: response
				});
      },
      search: function(){
        // custom minLength
        var term = extractLast(this.value);
				if(term== user_name_value ){
					return false;
				}
        if (term.length < 2) {
            return false;
        }
      },
      focus: function(){
	      // prevent value inserted on focus
	      return false;
      },
      select: function(event, ui){
	      var terms = split(this.value);
	      // remove the current input
	      terms.pop();
	      // add the selected item
	      terms.push(ui.item.value);
	      // add placeholder to get the comma-and-space at the end
	      terms.push("");
	      this.value = terms;
              $("#user_id").val(ui.item.id);
	      $("#error-user_name").html("");
	      return false;
      }
  });
    function extractLast(term){
        return split(term).pop();
    }
    function split(val){
        return val.split(/,\s*/);
    }
</script>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</head>