<table width="647" align="center" cellpadding="0" cellspacing="0" >
<tr>
<td align="left" valign="top"><table width="645" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" valign="top" bgcolor="#dddddd" style="padding:16px 22px 20px 23px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td align="left" valign="top">&nbsp;</td>
<td align="left" valign="top" style="padding:17px 15px 20px 11px;"><p style="margin:0px; margin-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:18px; line-height:20px;"><b>New <?=ucfirst($entry_category)?> Created</b><br />
<b style="font-size:14px; color:#b4b4b4;">Landmark Intranet</b></p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Hi <?= $review_user ?>,</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">A <?= ucfirst(strtolower($entry_category)) ?> has been submitted and is waiting for your approval.</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;"> Just click on the link below to review the new <?= strtolower($entry_category) ?>:<br />
<a href="<?= site_url('manage/edit_' . strtolower($entry_category) . '/' . $entry_id) ?>" style="color:#4f7f9a;"><?= site_url('manage/edit_' . strtolower($entry_category) . '/' . $entry_id)?></a></p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>