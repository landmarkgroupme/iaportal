<table width="647" align="center" cellpadding="0" cellspacing="0" >
<tr>
<td align="left" valign="top"><table width="645" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" valign="top" bgcolor="#dddddd" style="padding:16px 22px 32px 23px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td align="left" valign="top">&nbsp;</td>
<td align="left" valign="top" style="padding:17px 15px 20px 11px;"><p style="margin:0px; margin-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:18px; line-height:20px;"><b>Your <?=ucfirst($entry_category)?> announcement has been approved</b><br />

<b style="font-size:14px; color:#b4b4b4;">Landmark Intranet</b></p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Hi <?=ucfirst($event_owner)?>,</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Your event announcement has been approved and made live on the Intranet. Also, it will be sent out to all Landmarkers this coming Thursday as a part of the Intranet Digest.
<br />Please talk to <a href="mailto:parul.sharma@cplmg.com">Parul Sharma</a> (Ext. 4132) or <a href="mailto:anthony.crasto@cplmg.com">Anthony Crasto</a> (Ext. 4133) if you have any questions.
</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;"> Edit this Event:<br />
<a href="<?=site_url('manage/edit_event/' . $event_id)?>" style="color:#4f7f9a;"><?=site_url('manage/edit_event/' . $event_id)?></a></p>
<p>Regards,<br />
Landmark Corporate Communication Team</p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
</table>