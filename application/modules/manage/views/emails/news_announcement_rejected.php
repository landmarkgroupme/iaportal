<table width="647" align="center" cellpadding="0" cellspacing="0" >
<tr>
<td align="left" valign="top"><table width="645" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" valign="top" bgcolor="#dddddd" style="padding:16px 22px 32px 23px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td align="left" valign="top">&nbsp;</td>
<td align="left" valign="top" style="padding:17px 15px 20px 11px;"><p style="margin:0px; margin-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:18px; line-height:20px;"><b>Your <?= ucfirst($entry_category)?> announcement has been rejected</b><br />

<b style="font-size:14px; color:#b4b4b4;">Landmark Intranet</b></p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Hi <?=ucfirst($entry_owner)?>,</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Your <?=strtolower($entry_category)?> announcement has been reviewed but you need to make some changes before we can make it live.
<br />Please talk to <a href="mailto:parul.sharma@cplmg.com">Parul Sharma</a> (Ext. 4132) or <a href="mailto:anthony.crasto@cplmg.com">Anthony Crasto</a> (Ext. 4133) if you have any questions.
</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;"> View the <?strtolower($entry_category)?>:<br />
<a href="<?= site_url('manage/edit_' . strtolower($entry_category) . '/' . $entry_id)?>" style="color:#4f7f9a;"><?= site_url('manage/edit_' . strtolower($entry_category) . '/' . $entry_id) ?></a></p>
<p>Regards,<br />
Landmark Corporate Communication Team</p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
</table>