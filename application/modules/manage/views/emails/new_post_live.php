<table width="647" align="center" cellpadding="0" cellspacing="0" >
<tr>
<td align="left" valign="top"><table width="645" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" valign="top" bgcolor="#dddddd" style="padding:16px 22px 32px 23px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td align="left" valign="top">&nbsp;</td>
<td align="left" valign="top" style="padding:17px 15px 20px 11px;"><p style="margin:0px; margin-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:18px; line-height:20px;"><b>Your new <?=strtolower($entry_category)?> is live</b><br />

<b style="font-size:14px; color:#b4b4b4;">Landmark Intranet</b></p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Hi <?=ucfirst($entry_owner)?>,</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;">Your <?=strtolower($entry_category)?> has been accepted and has been made live.</p>
<p style="margin:0px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;"> View the <?=strtolower($entry_category)?>:<br />
<a href="<?=site_url(strtolower($entry_category) . '/'.strtolower($entry_category) .'_details/' . $entry_id)?>" style="color:#4f7f9a;"><?=site_url(strtolower($entry_category) . '/'.strtolower($entry_category) .'_details/' . $entry_id)?></a></p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
</table>