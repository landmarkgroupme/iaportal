<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/style.css" />

<!--<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/facebox_manage.css" />-->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input-facebook.css" />

 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.tokeninput.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>
<style type="text/css">
  .main-table th.permission-box{border-left:1px #dcdcdc solid;text-align: center;}
</style>

</head>
<body>
<div class="wrapper">
  <?php
		$this->load->helper('form');
        $data['active_tab'] = 'permissions';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
  <div class="content">
    <div class="top-heading">
      <h1>Role &amp; Permissions</h1>
    </div>
    <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
		<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
          <?=$this->session->flashdata('status_error');?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_del')):?>
      <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
    <?php endif;?>
    
    <div class="main-table">
    <input type="hidden" value="<?=base_url()?>" id="base_url" name="base_url" /> 
     <form name="form" method="post" action="<?=site_url("manage/remove_role")?>"> 
    
      <table id="list-table" width="100%" border="0" cellspacing="0">
         <tr>
          <th class="odd" colspan="11" style="margin-left:0px; padding-left:0px;">
          <!-- <div class="ArrowButton"></div>
          <div><input class="ItemButton ItemButtonHover" name="Delete" type="submit" value="Delete" /></div> -->
            <div>Available Roles</div>
          </th>
        </tr>
        <tr>
          <th width="5"><input type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
          <th><strong>Name</strong></th>
          <th style="background:#becfdc;"><strong>Review</strong></th>
          <th><strong>Announcements</strong></th>
          <th><strong>News</strong></th>
          <th><strong>Offers</strong></th>
          <th><strong>Photos</strong></th>
          <th><strong>Events</strong></th>
          <th><strong>Users</strong></th>
          <th><strong>Employees</strong></th>
          <th><strong>Moderate</strong></th>
        </tr>
		<?php if(count($roles) > 0) {
            $i = 1;
            foreach($roles as $row){
			if($i % 2 == 0)	{ $tableStyle = "odd";$tableStyle_review = 'odd_review'; }else { $tableStyle = "even"; $tableStyle_review = 'even_review';}
			?>
        <tr>
          <th class="<?=$tableStyle;?>" ><input class="selectBox" <?php if($row->id == $this->session->userdata('userid')){ echo 'disabled="disabled"';} ?> name="sel[]" type="checkbox" value="<? echo $row->id;?>" /></th>
          <th class="<?=$tableStyle;?>" ><strong><? echo $row->name;?></strong></th>
          <th class="<?=$tableStyle_review;?> permission-box">
            <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/review/'.$row->id);?>"><?php endif;?>
            <?php if($row->review == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box" >
            <?php if($row->id != $this->session->userdata('userid')):?>
            <a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/announcements/'.$row->id);?>">
            <?php endif;?>
            <?php if($row->announcements == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?>
            </a>
            <?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box">
            <?php if($row->id != $this->session->userdata('userid')):?>
            <a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/news/'.$row->id);?>">
            <?php endif;?>
            <?php if($row->news == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?>
            </a>
            <?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box" >
            <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/offers/'.$row->id);?>"><?php endif;?>
            <?php if($row->offers == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
              <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box" >
            <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/photos/'.$row->id);?>"><?php endif;?>
            <?php if($row->photos == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box">
            <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/events/'.$row->id);?>"><?php endif;?>
            <?php if($row->events == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box">
            <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/users/'.$row->id);?>"><?php endif;?>
            <?php if($row->users == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box">
            <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/employees/'.$row->id);?>"><?php endif;?>
            <?php if($row->employees == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
          <th class="<?=$tableStyle;?> permission-box">
          <?php if($row->id != $this->session->userdata('userid')):?><a class="toggle-status-link" href="<?=site_url('manage/roll_permissions_toggle/moderate/'.$row->id);?>"><?php endif;?>
            <?php if($row->moderate == 1) { ?>
                <img src="<?=base_url();?>images/moderation/ico-active.gif" alt="Active" title="Active"/>
              <?php }else{ ?>
                <img src="<?=base_url();?>images/moderation/ico-inactive.gif" alt="Inactive"  title="Inactive"/>
              <?php } ?>
            <?php if($row->id != $this->session->userdata('userid')):?></a><?php endif;?>
          </th>
        </tr>

        <?
		$i++; 
		
		}} ?>
       
      </table>
     </form>
    </div>
  </div>
</div>

<script type="text/javascript">

		
    $('.toggle-status-link').click(function(){
        var the_link = $(this);
        var href = the_link.attr('href');
        $.post(href,{},function(data){
            if(data.result == 1){
                the_link.html(data.html);
            }else{
                alert('Sorry, there was an error while updating permissions. Please try again later');
            }
        },'json');
        return false;
    });
    $('a[rel*=facebox]').facebox({closeImage:'<?=base_url()?>images/facebox/closelabel.gif',loadingImage:'<?=base_url()?>images/facebox/loading.gif'}) 
    
			$(document).ready(function(){
			$(".inline").colorbox({inline:true, width:"50%"});
			
		   $("input[type=button]").click(function () {
		  // $('#target').submit();
            //alert("Would submit: " + $(this).siblings("input[type=text]").val());
        });
				});
</script>
<!-- This contains the hidden content for inline calls -->
	<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>  	
</body>
</html>