<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'photos';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
    <div class="content">
        <div class="top-heading">
            <h1>Create Album</h1>
        </div>
        <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
            <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
            <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="main-table form">
                <div class="form-title">Album Name <span id="error_txt_title" class="error-msg"></span></div>
                <input type="text" name="album_name" id="txt_title" class="form-text" value="" />
                <div class="form-title">Album Description <span id="error_txtar_desc" class="error-msg"></span></div>
                <textarea rows="6" cols="3" name="album_description" id="txtar_desc" class="form-text"></textarea>
                <div class="form-title">Album Category</div>
                <div class="form-area">
                    <select name="album_category_id" class="form-text-dropdown  form-area">
                        <?=$categories_list;?>
                    </select>
                </div>
                <div class="clear"></div>
            </div>
            <div class="button-holder">
                <a href="<?=site_url("manage/photos")?>" class="clearLink" style="display: block; float:left; margin-left: 12px; margin-top:6px; color:#cf0b0b; font-weight:bold; text-decoration:underline">Cancel</a>
                <div style="float:right;">
                  <input class="submit_entry" style="display: block; float:left;  margin-top:4px; background:url(<?=base_url()?>images/moderation/ico-draft.gif) 0 2px no-repeat; height:20px; width:115px; color:#1f5987; font-weight:bold; text-decoration:underline;border:none;cursor:pointer;" name="save" type="submit" value="Save as draft" />
                  <input id="submit_album" class="ItemButtonSave" name="save" type="submit" value="Publish now" />
                </div><div class="clear"></div>  
            </div>
        </form>
        <div style="display:none">
            <div id="previewBox" style="padding:10px; background:#fff;">
                <p><strong>This content comes from a hidden element on this page.</strong></p>
                <p>The inline option preserves bound JavaScript events and changes, and it puts the content back where it came from when it is closed.<br /></p>
            </div>
        </div>
    </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
