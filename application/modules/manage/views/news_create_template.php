<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Landmark Intranet</title>
  <link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/facebox_manage.css" />
  <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.limit.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.form.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage_news_create.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/nicEdit_admin.js"></script>
  <script type="text/javascript">
    bkLib.onDomLoaded(function() {
     //nicEditors({iconsPath : }).allTextAreas()
      new nicEditor({
        iconsPath : '<?=base_url();?>images/nicEditorIcons.gif',
        buttonList : ['image','upload','link','unlink'],
        uploadURI : '<?=base_url()?>lib/nicUpload.php'
      }).panelInstance('txt_body'); 
    });
  </script>
</head>
<body>
<div class="wrapper">
 <?php
        $data['active_tab'] = 'news';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
  <div class="content">
    <div class="top-heading">
      <h1>Create News Item</h1>
    </div>

    <?php if($this->session->flashdata('status_ok')):?>
    <div class="msg-ok">
      <?=$this->session->flashdata('status_ok');?>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_error')):?>
    <div class="msg-ok">
      <?=$this->session->flashdata('status_error');?>
    </div>
    <?php endif;?>
    <form action="<? site_url("manage/createnews")?>" method="post" id="frm_create_news">
      <input type="hidden" name="entry_category" id="entry_category" value="news" />
      <div class="main-table form">
        <div class="counter">Characters left: <span id="charsLeft"></span></div>
        <div class="form-title">Title <span id="error-title" class="error-msg"></span></div>
        <input type="text" name="txt_title" id="txt_title" class="form-text" value="" />  
        <div class="counter">Characters left: <span id="charsLeft2"></span></div>            
        <div class="form-title">Excerpt </div>
        <textarea rows="6" name="txt_excerpt" id="txt_excerpt" class="form-text"></textarea>
        
        <div class="form-title">Body <span id="error-body" class="error-msg"></span></div>
        <textarea rows="12" name="txt_body" id="txt_body" class="form-text"></textarea>        
        <div style="padding-left:7px;">Maximum image width: 560 pixels and maximum file size: 70kb</div>
        
        <div class="form-title">Linked Album</div>
        <div class="form-area">
          <select id="txt_album" name="txt_album" class="form-text-dropdown">
          <option value="0">Select Album</option>
          <?php
			if (count($albums)): foreach ($albums as $details):
				$ids = $details['aid'];
				$name = $details['title'];
			?>
			<option value="<?=$ids?>"><?=$name?></option>
			<?php
				endforeach;
			endif;
		  ?>
          </select>
        </div>
        <input name="txt_publish" id="txt_publish" type="hidden" value="1" />
        <div class="clear"></div>
      </div>     
            
      <div class="button-holder">
        <a href="<?=site_url("manage/news")?>" class="clearLink" style="display: block; float:left; margin-left: 12px; margin-top:6px; color:#cf0b0b; font-weight:bold; text-decoration:underline">Cancel</a>
        <div style="float:right;">
          <a href="#" class="previewLink" style="display: block; float:left;margin-right:40px; margin-top:6px; background:url(<?=base_url()?>images/moderation/preview.gif) no-repeat; height:20px; padding-left:20px; color:#1f5987; font-weight:bold; text-decoration:underline">Preview</a>
          <input class="submit_entry" style="display: block; float:left;  margin-top:4px; background:url(<?=base_url()?>images/moderation/ico-draft.gif) 0 2px no-repeat; height:20px; width:115px; color:#1f5987; font-weight:bold; text-decoration:underline;border:none;cursor:pointer;" name="save_draft" type="submit" value="Save as draft" />
          <input id="submit_news" class="ItemButtonSave submit_entry" name="save" type="submit" value="Publish Now" />
        </div><div class="clear"></div>
      </div>
    </form>

  </div>
  <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>" />
    <script type="text/javascript">
        $('.previewLink').click(function(){
            var options = {
                success:       showResponse,
                url:'<?=site_url('manage/preview_mt_entry');?>'
            };
            
            var text = $(".nicEdit-main").html();
            $("#txt_body").val(text);
            $('#frm_create_news').ajaxSubmit(options);
            return false;
        });

        // post-submit callback
        function showResponse(responseText, statusText, xhr, $form)  {
            jQuery.facebox(responseText);
        } 
    </script>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>   
</body>
</html>
