<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Landmark Intranet</title>
<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/calendrical.css" />
<script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>

</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'events';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
  <div class="content">
    <div class="top-heading">
      <h1>Events</h1>
    </div>

    <?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
    <?php if($this->session->flashdata('status_error')):?>
        <div class="msg-error">
          <?=$this->session->flashdata('status_error');?>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_del')):?>
      <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
    <?php endif;?>
    <div class="main-table">
    <form name="form" id="event_form" method="post" action="<?=site_url("manage/event_delete_action")?>">
      <table width="100%" border="0" cellspacing="0">
         <tr>
          <th class="odd" colspan="7" style="margin-left:0px; padding-left:0px;">
          <div class="ArrowButton"></div>
          <div><input class="ItemButton ItemButtonHover" name="Delete" type="submit" value="Delete" /></div>
          <div class="searchArea">Search: <input type="text" name="SearchText" id="SearchBox" value="Search Events" /></div>
          <input class="ItemButton" id="Search" name="Search" type="submit" value="Search" />
          <div style="float:right;margin-top:5px;color:#000000;"><input id="event_review" <?php echo ($this->session->flashdata('event_review')? 'checked = "checked"':"" );?> name="event_review" type="checkbox" value="event_review" /> <label for="event_review"><strong>Only show items under review</strong></label></div>
        </tr>
        <tr>
          <th width="5"><input class="selectBox" type="checkbox" name="checkAllAuto" id="checkAllAuto" /></th>
          <th width="340"><strong>Title</strong></th>
          <th width="150"><strong>Status</strong></th>
          <th width="150"><strong>Event Date</strong></th>
          <th width="300"><strong>Author (Concept)</strong></th>
          <th width="150"><strong>Created</strong></th>
          <th width="40"><strong>View</strong></th>
        </tr>
		<?php if(count($allEvents) > 0) {
            $i = 1;
            foreach($allEvents as $row){
			         if($i % 2 == 0)	{ $tableStyle = "odd"; }else { $tableStyle = "even"; }
			         switch ($row['event_status']) {
                  case 1: $event_status = '<div class="post_status published">Live</div>';
                    break;
                  case 2: $event_status = '<div class="post_status review">Under Review</div>';
                    break;
                  case 3: $event_status = '<div class="post_status rejected">Rejected</div>';
                    break;
                  default: $event_status = '<img style="margin-left:3px" src="' . base_url() . 'images/moderation/ico-question.gif" alt="unknown" />';
                    break;
                }
      
			?>
        <tr>
          <th class="<?=$tableStyle;?>" width="5"><input class="selectBox" name="sel[]" type="checkbox" value="<? echo $row['event_id'];?>" /></th>
          <th class="<?=$tableStyle;?>" width="340"><a href="<?php echo site_url('manage/edit_event/'.$row['event_id']) ;?>"><? echo $row['event_title'];?></a></th>
          <th class="<?=$tableStyle;?>" width="150"><?php echo $event_status;?></th>
          <th class="<?=$tableStyle;?>" width="150"><? echo date('d M Y',$row['event_datetime']);?></th>
          <th class="<?=$tableStyle;?>" width="300"><? echo $row['name']." ". $row['surname']." (". ucfirst(strtolower($row['concept_name'])). ")"; ?></th>
          <th class="<?=$tableStyle;?>" width="150"><? echo time_ago($row['added_on']);?></th>
          <th class="<?=$tableStyle;?>" width="40"><a target="_blank" href="<?=site_url("events/event_detail/".$row['event_id'])?>"><img style="margin-left:9px" src="<?=base_url()?>images/moderation/view.gif" alt="View" /></a></th>
        </tr>
        <? $i++; }} ?>
         <tr>
          <th colspan="7" align="right"><ul class="pagination"> <? echo $navlinks; ?> </ul></th>
        </tr>
      </table>
      </form>
    </div>
  </div>
</div>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
