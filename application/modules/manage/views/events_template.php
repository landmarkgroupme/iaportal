<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/moderation/calendrical.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.form.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/jquery.calendrical.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage_events_create.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/facebox_manage.css" />
    <script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
</head>
<body>
<div class="wrapper">
  <?php
        $data['active_tab'] = 'events';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
  ?>
    <div class="content events-content">
        <div class='top-heading'>
            <h1>Create an Event</h1>
	</div>
	<?php if($this->session->flashdata('status_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_ok');?>
        </div>
        <?php endif;?>
	<?php if($this->session->flashdata('status_error')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('status_error');?>
        </div>
        <?php endif;?>
        <?php if($this->session->flashdata('status_del')):?>
          <div class="msg-del"><?=$this->session->flashdata('status_del');?></div>
        <?php endif;?>
        <form method="post" id="create-event" action="<?=site_url("manage/submit_events")?>" class="advanced-search">
            <div class="main-table form">
                <div class="form-title">Title <span id="error_txt_title" class="error-msg"></span></div>
  		<input type="text" name="txt_title" class="form-text"  id="txt_title" />
                <div class="form-title">Description <span id="error_txtar_desc" class="error-msg"></span></div>
                <textarea rows="6" cols="134" name="txtar_desc" id="txtar_desc" class="form-text" ></textarea>
                <div class="form-title">Event Details</div>
                <div class="event-date">
                    <label class="form-title">Date</label>
                    <input type="text" name="txt_date" readonly="readonly" id="txt_date" />
                    <label class="form-title">Time</label>
                    <input type="text" name="txt_time" readonly="readonly" id="txt_time" />
                    <span id="error_date_time" class="error-msg"></span>
                </div>
                <div class="form-title">Location <span id="error_cmb_location" class="error-msg"></span></div>
                <select id="cmb_location" name="cmb_location" class="form-text-dropdown form-area">
                    <option value="0">Select Location</option>
                    <?php
					if (count($location)) : foreach ($location as $details):
					$id = $details['id'];
					$name = $details['name'];
					?>
					<option value="<?=$id?>" ><?=$name?></option>
					<?php

					endforeach;
					endif;
					?>
                </select>
                <div class="form-title">Concept</div>
                <select id="cmb_concept" name="cmb_concept" class="form-text-dropdown form-area">
                    <option value="0">Select Concept</option>
                    <?php
					if (count($concepts)) : foreach ($concepts as $details):
					$id = $details['id'];
					$name = $details['name'];
					?>
					<option value="<?=$id?>" ><?=$name?></option>
					<?php

					endforeach;
					endif;
					?>
                </select>
                <div class="form-title">Linked Album</div>
                <select id="cmb_album" name="cmb_album" class="form-text-dropdown form-area">
                    <option value="0">Select Album</option>
					<?php
					if (count($albums)): foreach ($albums as $details):
						$ids = $details['aid'];
						$name = $details['title'];
					?>
					<option value="<?=$ids?>"><?=$name?></option>
					<?php
						endforeach;
					endif;
					?>
                </select>
                <div class="clear"></div>
            </div>
              <div class="button-holder">
                <a href="<?=site_url("manage/events")?>" class="clearLink" style="display: block; float:left; margin-left: 12px; margin-top:6px; color:#cf0b0b; font-weight:bold; text-decoration:underline">Cancel</a>
                <div style="float:right;">
                  <a href="#" class="previewLink" style="display: block; float:left; margin-left: 535px; margin-top:6px; background:url(<?=base_url()?>images/moderation/preview.gif) no-repeat; height:20px; padding-left:20px; color:#1f5987; font-weight:bold; text-decoration:underline">Preview</a>
                  <input id="event-submit" class="ItemButtonSave" name="save" type="submit" value="SAVE" />
               </div>
            </div>
            <input type="hidden" name="hd_frm" id="hd_frm" value="1" />
       </form>
    </div>
</div>
<script type="text/javascript">
    $('.previewLink').click(function(){
        var options = {
            success:       showResponse,
            url:'<?=site_url('manage/preview_event');?>',
            dataType : 'json'
        };
        $('#create-event').ajaxSubmit(options);
        return false;
    });

        // post-submit callback
    function showResponse(responseText, statusText, xhr, $form)  {
        var location = $('#cmb_location option:selected').text();
        var event_title = $('#txt_title').val();
        var event_description = $('#txtar_desc').val();
        var html =     '<div id="preview-box"><div class="heading-box"><h2 id="preview-news-title">'+event_title+'</h2></div><div class="container"><div class="details-attr"><span class="details-attr-title"><strong>Date: </strong></span><span class="details-attr-res" id="preview-news-date">'+responseText.event_date+'</span> | <span class="details-attr-title"><strong>Time: </strong></span><span class="details-attr-res" id="preview-news-time">'+responseText.event_time+'</span> | <span><strong>Location:</strong> '+location+'</span> </div><div  class="details-desc-box news"><div class="details-desc"><div class="details-desc-title"><h2>Description</h2></div><p id="preview-news-description">' + event_description + '</p></div>'+ responseText.album_details + '</div></div></div>';
        jQuery.facebox(html);
    }
</script>
<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
