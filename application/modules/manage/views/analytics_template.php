<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Landmark Intranet</title>
	<link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
    <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
	
  </head>
	<body>
	<div class="wrapper">
		<?php
		$data['active_tab'] = strtolower($page_title);
		$data['user_name'] = $user_name;
		$this->load->view('includes/admin_header',$data);
		?>

		<!-- #Content-->
		<div class="content">
			<div class="top-heading">
			<h1><?=$page_title?></h1>
			</div>
			<div class="main-table">
				<table>
				
				<tr>
						<th>Total Intranet Users</th>
						<td><?php echo $intranet_users; ?></td>
					</tr>
					<tr>
						<th>Total Pageviews</th>
						<td><?php echo $pageviews; ?></td>
					</tr>
					<tr>
						<th>Total Visits</th>
						<td><?php echo $visits; ?></td>
					</tr>
					
					<tr>
						<th>Number of Active Users</th>
						<td><?php echo $active_users; ?></td>
					</tr>					
					<tr>
						<th>Number of Groups</th>
						<td><?php echo $group; ?></td>
					</tr>
					<tr>
						<th>Number of Concepts</th>
						<td><?php echo $concept; ?></td>
					</tr>
					<tr>
						<th>Number of Group Members</th>
						<td><?php echo $groupuser; ?></td>
					</tr>
					<tr>
						<th>Number of Concept Members</th>
						<td><?php  ?></td>
					</tr>
					
					<tr>
						<th>Number of Files</th>
						<td><?php echo $file; ?></td>
					</tr>
					<tr>
						<th>Message Posted</th>
						<td><?php  ?></td>
					</tr>
					<tr>
						<th>Users Online</th>
						<td><?php  echo $online_users;?></td>
					</tr>
					
					<tr>
						<th>Total Uploaded Files</th>
						<td><?php  ?></td>
					</tr>
					<tr>
						<th>Results Updated</th>
						<td><?php echo $updated; ?></td>
					</tr>
				</table>
			</div>
			<!-- /Listing-->
		</div>
		<!-- /Content-->
	</div>
	<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
	</body>
</html>