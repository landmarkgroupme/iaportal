<?php
  $user_permission = $this->session->userdata('permissions');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Landmark Intranet</title>
  <link rel="icon" href="<?php echo site_url(); ?>images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>/css/manage/style.css" />
  <script type="text/javascript" src="<?=base_url();?>media/js/jquery-1.8.3/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.limit.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.form.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage_announcement_create.js"></script>
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/facebox_manage.css" />
  <script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
   <script type="text/javascript" src="<?=base_url();?>js/nicEdit_admin.js"></script>
  <script type="text/javascript">
    bkLib.onDomLoaded(function() {
     //nicEditors({iconsPath : }).allTextAreas()
      new nicEditor({
        iconsPath : '<?=base_url();?>images/nicEditorIcons.gif',
        buttonList : ['image','upload','link','unlink'],
        uploadURI : '<?=base_url()?>lib/nicUpload.php'
      }).panelInstance('txt_body'); 
    });
  </script>
</head>
<body>
<div class="wrapper">
    <?php
        $data['active_tab'] = 'announcement';
        $data['user_name'] = $user_name;
        $this->load->view('includes/manage_header',$data);
      ?>
  <div class="content">
    <div class="top-heading">
      <h1>Edit Announcement</h1>
    </div>
    <?php if($this->session->flashdata('status_ok')):?>
    <div class="msg-ok">
      <?=$this->session->flashdata('status_ok');?>
    </div>
    <?php endif;?>
    <?php if($this->session->flashdata('status_error')):?>
    <div class="msg-ok">
      <?=$this->session->flashdata('status_error');?>
    </div>
    <?php endif;?>
    
  <form action="<? site_url("manage/edit_announcement")?>" method="post" id="frm_update_announcement">
    <input type="hidden" name="entry_category" id="entry_category" value="announcement" />
    <input type="hidden" name="entry_id" id="entry_id" value="<?=$entry_details['entry_id']?>" />
    <div id="create_content" class="main-table form">
    <div class="counter">Characters left: <span id="charsLeft"></span></div>
    <div class="form-title">Title <span id="error-title" class="error-msg"></span></div>
    <input type="text" name="txt_title" id="txt_title" class="form-text" value="<?=$entry_details['entry_title']?>" />  
    <div class="counter">Characters left: <span id="charsLeft2"></span></div>            
    <div class="form-title">Excerpt</div>
    <textarea rows="6" name="txt_excerpt" id="txt_excerpt" class="form-text"><?=$entry_details['entry_text_more']?></textarea>
             
    <div class="form-title">Body <span id="error-body" class="error-msg"></span></div>
    <div class="txt_body_holder"><textarea rows="12" name="txt_body" id="txt_body" ><?=$entry_details['entry_text']?></textarea></div>        
    <div style="padding-left:7px;">Maximum image width: 560 pixels and maximum file size: 70kb</div>
    
    <input name="txt_publish" id="txt_publish" type="hidden" value="1" />

    <?php if($entry_details['entry_status'] == 3 || $entry_details['entry_status'] == 1):  ?>
            <div id="email-notify-box" class="custom-form-element">
              <div class="form-title">Email Notifications</div>
              <div class="email-holder">
                <div class="email-area-c">
                  <div class="email-title"><strong>Concepts</strong> <span>(<a id="concAll" href="javascript:;">All</a> | <a id="concNone" href="javascript:;">None</a></span>)</div>
				  <?php
					$i = 0;
					if (count($concepts)): foreach ($concepts as $details):
						$ids = $details['id'];
						$name = $details['name'];
					/* Default checked value if form is saved or in review state */
						if(@in_array($ids, $concept_ids_arr)):
							$checked = 'checked = "checked"';
						else:
							$checked = null;
						endif;
					?>
					<div class="concept"><input id="conc<?=$i?>" name="conc[]" <?=$checked?> type="checkbox" value="<?=$ids?>" class="conc"/><label for="conc<?=$i?>" class="pButton"><?=ucfirst(strtolower($name))?></label></div>
					
					<?php 
						$i++;
						endforeach;
					endif;
					?>
				  
                </div>
                <div class="email-area-t">
                  <div class="email-title"><strong>Territories</strong> <span>(<a id="terrAll" href="javascript:;">All</a> | <a id="terrNone" href="javascript:;">None</a>)</span></div>
				  <?php 
					$i = 0;
					if (count($location)): foreach ($location as $details):
					$ids = $details['id'];
					$name = $details['name'];
						/* Default checked value if form is saved or in review state */
						if(@in_array($ids, $territory_ids_arr)):
						  $checked = 'checked = "checked"';
						else:
						  $checked = null;
						endif;
		?>
       <div class= "location"><input id="terr<?=$i?>" name="terr[]" <?= $checked?> type="checkbox" value= "<?=$ids?>" class="terr" /><label for= "terr<?=$i?>" class="pButton"><?=ucfirst(strtolower($name))?></label></div>
		<?php
						$i++;
						endforeach;
					endif;
				  ?>
                </div>
                <div class="email-area-g">
                  <div class="email-title"><strong>Grades</strong> <span>(<a id="gradAll" href="javascript:;">All</a> | <a id="gradNone" href="javascript:;">None</a>)</span></div>
					<?php
					$i = 0;
					if (count($grades)): foreach ($grades as $details):	
						$ids = $details['id'];
						$name = $details['name'];
						if(@in_array($ids, $band_ids_arr)):
							$checked = 'checked = "checked"';
						else:
							$checked = null;
						endif;
					?>
        <div class="grade"><input id="grad<?=$i?>" name="grad[]" <?=$checked?> type="checkbox" value="<?=$ids?>" class="grad" /><label for="grad<?=$i?>" class="pButton"><?=ucfirst(strtolower($name))?></label></div>
					<?php
						$i++;
						endforeach;
					endif;
					?>

                </div>
              </div>
            </div>
            <?php endif;?>

    <div class="clear"></div>
    </div>     
      
          <div class="button-holder">
            <div style="float:right;">
              <a href="#" class="previewLink" style="display: block; float:left;margin-right:10px; margin-top:6px; background:url(<?= base_url() ?>images/moderation/preview.gif) no-repeat; height:20px; padding-left:20px; color:#1f5987; font-weight:bold; text-decoration:underline">Preview</a>
              <input class="submit_entry ItemButtonDraft" name="save_draft" type="submit" value="Save as draft" />
              <?php if($entry_details['entry_status'] == 3 && $user_permission['review']):  ?>
              <input id="reject_announcement" class="ItemButtonSave submit_entry" name="reject" type="submit" value="Reject" />
              <?php endif; ?>
              <input id="submit_announcement" class="ItemButtonSave submit_entry" name="save" type="submit" value="<?php echo ($user_permission['review'])? 'Publish Now':'Submit for Review' ; ?>" />
            </div><div class="clear"></div>
          </div>
  </form>
</div>
<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>">
        <script type="text/javascript">
        $('.previewLink').click(function(){
            var options = {
                success:       showResponse,
                url:'<?=site_url('manage/preview_mt_entry');?>'
            };
            var text = $(".nicEdit-main").html();
            $("#txt_body").val(text);
			$('#frm_update_announcement').ajaxSubmit(options);
            return false;
        });

        // post-submit callback
        function showResponse(responseText, statusText, xhr, $form)  {
            jQuery.facebox(responseText);
        }
    </script>
	<!-- Global Footer -->
<?php $this->load->view('admin_global_footer.php'); ?>
</body>
</html>
