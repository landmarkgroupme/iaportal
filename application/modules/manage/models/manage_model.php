<?php

Class Manage_model extends CI_Model {

  function Manage_model() {
    parent::__construct();
  }

  function get_offers_users($sql_where) {
    $sql = "SELECT email,title,first_name,middle_name,last_name FROM ci_offers_users a, ci_users b WHERE a.{$sql_where} and user_id = b.id group by user_id";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Get data count for specified category from MT Database
  function get_count_mt_entries($mt_category, $query = '', $filterExpiry = '') {
    if (empty($query)) {
      $sql = 'SELECT
                  count(a.entry_id) as total_entries
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_basename = ? AND
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND ';
                  if($filterExpiry) {
                    $sql.= ' a.offers_end_date	>= "'.date("Y-m-d").'" AND ';
                  }
                  $sql.='d.concept_id = e.id AND
                              a.entry_status <> 0';
    } else {
      $sql = 'SELECT
                  count(a.entry_id) as total_entries
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_basename = ? AND
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND
                  d.concept_id = e.id AND
                  a.entry_status <> 0 AND
                  a.entry_title LIKE "%' . $query . '%"';
    }
    $result = $this->db->query($sql, array($mt_category));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  
  //Get data count for Review for specified category from MT Database
  function get_review_count_mt_entries($mt_category, $query = '') {
    if (empty($query)) {
     $sql = 'SELECT
                  count(a.entry_id) as total_entries
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_basename = ? AND
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND
                  d.concept_id = e.id AND 
                  a.entry_status = 3';
    } else {
      $sql = 'SELECT
                  count(a.entry_id) as total_entries
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_basename = ? AND
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND
                  d.concept_id = e.id AND
                  a.entry_status = 3 AND
                  a.entry_title LIKE "%' . $query . '%"';
    }

    $result = $this->db->query($sql, array($mt_category));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  
  
  //Get all content for sepcific category from MT Database
  function get_all_mt_entries($mt_category, $pagelimit, $offset, $query = '',$filterExpiry='') {
    $limit = (int) $pagelimit;
    $offset = (int) $offset;
    if (empty($query)) {
      $sql = 'SELECT
                  a.entry_id,
                  a.entry_created_on,
                  a.entry_text_more,
                  a.entry_title,
                  a.entry_status,
                  a.entry_excerpt,
                  d.first_name as name,
                  d.last_name as surname,
                  e.name as concept_name
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_basename = ? AND
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND
                  d.concept_id = e.id AND ';
                  if($filterExpiry) {
                    $sql.= ' a.offers_end_date >= "'.date("Y-m-d").'" AND ';
                  }
                  $sql.='a.entry_status <> 0
                ORDER BY a.entry_id DESC  LIMIT ? OFFSET ?';
    } else {
      $sql = 'SELECT
                      a.entry_id,
                      a.entry_created_on,
                      a.entry_text_more,
                      a.entry_title,
                      a.entry_status,
                      a.entry_excerpt,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      mt_entry a,
                      mt_category b,
                      mt_placement c,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      b.category_basename = ? AND
                      b.category_class = "category" AND
                      b.category_id = c.placement_category_id AND
                      c.placement_entry_id = a.entry_id AND
                      a.entry_author_id = d.id AND
                      d.concept_id = e.id AND
                      a.entry_status <> 0 AND
                      a.entry_title LIKE "%' . $query . '%"
                    ORDER BY a.entry_id DESC  LIMIT ? OFFSET ?';
    }
    $result = $this->db->query($sql, array($mt_category, $limit, $offset));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
//Get all content for sepcific category from MT Database
  function get_all_review_mt_entries($mt_category, $pagelimit, $offset, $query = '') {
    $limit = (int) $pagelimit;
    $offset = (int) $offset;
    if (empty($query)) {
      $sql = 'SELECT
                  a.entry_id,
                  a.entry_created_on,
                  a.entry_text_more,
                  a.entry_title,
                  a.entry_status,
                  a.entry_excerpt,
                  d.first_name as name,
                  d.last_name as surname,
                  e.name as concept_name
                FROM
                  mt_entry a,
                  mt_category b,
                  mt_placement c,
                  ci_users d,
                  ci_master_concept e
                WHERE
                  b.category_basename = ? AND
                  b.category_class = "category" AND
                  b.category_id = c.placement_category_id AND
                  c.placement_entry_id = a.entry_id AND
                  a.entry_author_id = d.id AND
                  d.concept_id = e.id AND
                  a.entry_status = 3
                ORDER BY a.entry_id DESC  LIMIT ? OFFSET ?';
    } else {
      $sql = 'SELECT
                      a.entry_id,
                      a.entry_created_on,
                      a.entry_text_more,
                      a.entry_title,
                      a.entry_status,
                      a.entry_excerpt,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      mt_entry a,
                      mt_category b,
                      mt_placement c,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      b.category_basename = ? AND
                      b.category_class = "category" AND
                      b.category_id = c.placement_category_id AND
                      c.placement_entry_id = a.entry_id AND
                      a.entry_author_id = d.id AND
                      d.concept_id = e.id AND
                      a.entry_status = 3 AND
                      a.entry_title LIKE "%' . $query . '%"
                    ORDER BY a.entry_id DESC  LIMIT ? OFFSET ?';
    }
    $result = $this->db->query($sql, array($mt_category, $limit, $offset));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  //Update MT Entries Status 1: un publish ; 0: Delete
  function update_mt_entries_status($new_status, $ids) {
    if (is_int($new_status)) {
      $data = array(
          'entry_status' => $new_status,
      );
      $this->db->where_in('entry_id', $ids);
      $this->db->update('mt_entry', $data);
      return $this->db->affected_rows();
    } else {
      return false;
    }
  }

  //Gets the MT Category details
  function get_mt_category_details($ct_basename) {
    $this->db->select('category_blog_id,category_id');
    $result = $this->db->get_where('mt_category', 'category_basename = "' . $ct_basename . '"');
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function get_entry_details($entry_id) {
    $result = $this->db->get_where('mt_entry', array('entry_id' => $entry_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Create a MT Entry for New, announcement and offers
  function update_mt_entry($data, $entry_id) {
    $this->db->where('entry_id', $entry_id);
    $this->db->update('mt_entry', $data);
    return $this->db->affected_rows();
  }

  //Create a MT Entry for New, announcement and offers
  function submit_mt_entry($data) {
    $str = $this->db->insert('mt_entry', $data);
    return $this->db->insert_id();
  }

  //Create MT Placement Insert for New, announcement and offers
  function submit_mt_placement($data) {
    $str = $this->db->insert('mt_placement', $data);
    return $this->db->insert_id();
  }
  
   function update_mt_placement($data, $placement_id) {
	$this->db->where('placement_id', $placement_id);
    $this->db->update('mt_placement', $data);
    return $this->db->affected_rows();
	
  }
  
  
   function get_mt_placement($entry_id) {

	$this->db->select('placement_id');
    $query = $this->db->get_where('mt_placement', array('placement_entry_id' => $entry_id));
    if($data = $query->result_array())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]['placement_id'];
      }
    }
    return false;
	
  }

//Get all the Territory List 
  function get_territory_names() {
    $sql = "select id,name from ci_master_territory ORDER BY name";
    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  //Get all the Concept List
  function get_concept_names() {
    $sql = "select id,name from ci_master_concept ORDER BY name";
    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  //Get all the band List
  function get_grade_names() {
    $sql = "select id, name from ci_master_band ORDER BY name";
    $qry = $this->db->query($sql);

    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  //Get all the ablum List
  function get_album_list() {
    $sql = "select aid,title from alb_albums WHERE  visibility = 0 ORDER BY aid DESC";
    $qry = $this->db->query($sql);
    $data = array();
    if ($qry->num_rows() > 0) {
      foreach ($qry->result_array() as $row) {
        $data[] = $row;
      }
    }
    $qry->free_result();
    return $data;
  }

  function get_events_list($per_page = 10, $offset = 0, $query = '') {
    $per_page = (int) $per_page;
    $offset = (int) $offset;
    if (!empty($query)) {
      $sql = 'SELECT
                      a.id as event_id,
                      a.title as event_title,
                      a.description as event_description,
                      a.event_datetime as event_datetime,
                      a.added_on as added_on,
                      a.event_status,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      ci_event a,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      a.user_id = d.id AND
                      d.concept_id = e.id AND
                      a.title LIKE "%' . $query . '%"
                    ORDER BY event_datetime DESC LIMIT ?,?';
    } else {
      $sql = 'SELECT
                      a.id as event_id,
                      a.title as event_title,
                      a.description as event_description,
                      a.event_datetime as event_datetime,
                      a.added_on as added_on,
                      a.event_status,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      ci_event a,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      a.user_id = d.id AND
                      d.concept_id = e.id
                    ORDER BY event_datetime DESC LIMIT ?,?';
    }
    $result = $this->db->query($sql, array($offset, $per_page));
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }
function get_events_review_list($per_page = 10, $offset = 0, $query = '') {
    $per_page = (int) $per_page;
    $offset = (int) $offset;
    if (!empty($query)) {
      $sql = 'SELECT
                      a.id as event_id,
                      a.title as event_title,
                      a.description as event_description,
                      a.event_datetime as event_datetime,
                      a.added_on as added_on,
                      a.event_status,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      ci_event a,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      a.user_id = d.id AND
                      d.concept_id = e.id AND
                      a.event_status = 2 AND
                      a.title LIKE "%' . $query . '%"
                    ORDER BY event_datetime DESC LIMIT ?,?';
    } else {
      $sql = 'SELECT
                      a.id as event_id,
                      a.title as event_title,
                      a.description as event_description,
                      a.event_datetime as event_datetime,
                      a.added_on as added_on,
                      a.event_status,
                      d.first_name as name,
                      d.last_name as surname,
                      e.name as concept_name
                    FROM
                      ci_event a,
                      ci_users d,
                      ci_master_concept e
                    WHERE
                      a.user_id = d.id AND
                      d.concept_id = e.id AND
                      a.event_status = 2
                    ORDER BY event_datetime DESC LIMIT ?,?';
    }
    $result = $this->db->query($sql, array($offset, $per_page));
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }
//Get data count for Review for specified category from MT Database
  function get_review_count_events($query = '') {
    if (empty($query)) {
     $sql = 'SELECT
                  count(a.id) as total_events
                FROM
                   ci_event a
                WHERE
                  a.event_status = 2';
      $result = $this->db->query($sql);
    }else{
      $sql = 'SELECT
                  count(a.id) as total_events
                FROM
                   ci_event a
                WHERE
                  a.event_status = 2 AND
                  a.title LIKE "%?%"';
      $result = $this->db->query($sql,array($query));
    }
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function get_events_count($query = '') {
    if (!empty($query)) {
      $this->db->like('title', $query);
      $this->db->from('ci_event');
      $events_count = $this->db->count_all_results();
    } else {
      $events_count = $this->db->count_all('ci_event');
    }
    return $events_count;
  }

  function event_delete($id) {
    $sql = "DELETE FROM `ci_event` WHERE `id` = ? LIMIT 1";
    $result = $this->db->query($sql, array($id));
    return $this->db->affected_rows();
  }

  //Insert Interest Groups
  function insert_events($data) {
    $str = $this->db->insert('ci_event', $data);
    return $this->db->insert_id();
  }

  function insert_events_album($data) {
    $str = $this->db->insert('ci_event_album', $data);
    return $this->db->insert_id();
  }

  function get_event_details($event_id) {
    $this->db->select('ci_event.id, ci_event.title,ci_event.description,ci_event.event_location,ci_event.event_datetime,ci_event.event_end_datetime,ci_event.added_on,ci_event.concept_id,ci_event.event_status,ci_event_album.album_id');
    $this->db->from('ci_event');
    $this->db->join('ci_event_album', 'ci_event_album.event_id = ci_event.id', 'left');
    $this->db->where('ci_event.id', $event_id);
    $result = $this->db->get();
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function update_event($event, $event_id) {
    $this->db->where('id', $event_id);
    return $this->db->update('ci_event', $event);
  }

  function update_event_album($event_album, $event_id) {
    $this->db->where('event_id', $event_id);
    return $this->db->update('ci_event_album', $event_album);
  }

  function is_event_album_update($event_id) {
    $this->db->get_where('ci_event_album', array('event_id' => $event_id));
    return $this->db->affected_rows();
  }

  function get_all_albums_for_index($per_page, $offset, $query = '') {
    $per_page = (int) $per_page;
    $offset = (int) $offset;
    if (empty($query)) {
      $sql = 'SELECT
              a.aid as album_id,
              a.title as album_title,
              a.description as album_description,
              a.visibility as album_status,
              (SELECT count(*) FROM alb_pictures as b WHERE b.aid = a.aid) as pictures_count,
              (SELECT min(ctime) FROM alb_pictures as c WHERE c.aid = a.aid) as picture_ctime,
              d.first_name as name,
              d.last_name as surname,
              e.name as concept_name
            FROM
              alb_albums a,
              ci_users d,
              ci_master_concept e
            WHERE
              a.owner = d.id AND
              d.concept_id = e.id AND
              a.visibility <> 2
            ORDER BY a.aid DESC LIMIT ?,?';
    } else {
      $sql = 'SELECT
              a.aid as album_id,
              a.title as album_title,
              a.description as album_description,
              a.visibility as album_status,
              (SELECT count(*) FROM alb_pictures as b WHERE b.aid = a.aid) as pictures_count,
              (SELECT min(ctime) FROM alb_pictures as c WHERE c.aid = a.aid) as picture_ctime,
              d.first_name as name,
              d.last_name as surname,
              e.name as concept_name
            FROM
              alb_albums a,
              ci_users d,
              ci_master_concept e
            WHERE
              a.owner = d.id AND
              d.concept_id = e.id AND
              a.visibility <> 2 AND
              a.title LIKE "%' . $query . '%"
            ORDER BY a.aid DESC LIMIT ?,?';
    }
    $result = $this->db->query($sql, array($offset, $per_page));
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function get_albums_count($query = '') {
    if (empty($query)) {
      $sql = "Select count(aid) as total FROM alb_albums WHERE  visibility <> 2";
      //$albums_count = $this->db->count_all('alb_albums');
      $result = $this->db->query($sql); 
    } else {
      $sql = 'Select count(aid) as total FROM alb_albums WHERE  visibility <> 2 AND title LIKE "%?%"  ';
      //$albums_count = $this->db->count_all('alb_albums');
      $result = $this->db->query($sql,array($query));
      //$albums_count = $this->db->count_all_results();
    }
    
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $albums_count = $row['total'];
      endforeach;
    }else{
      $albums_count = 0;
    }
    $result->free_result();
    
    return $albums_count;
  }

  function update_photo_status($new_status, $ids) {
    if (is_int($new_status)) {
      $new_status = (int) $new_status;
      $data = array(
          'visibility' => $new_status,
      );
      $this->db->where_in('aid', $ids);
      $this->db->update('alb_albums', $data);
      return $this->db->affected_rows();
    } else {
      return false;
    }
  }

  function get_album_details($album_id, $per_page, $offset) {
    $per_page = (int) $per_page;
    $offset = (int) $offset;
    $result = $this->db->get_where('alb_albums', array('aid' => $album_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row):
        $data['Album'] = $row;
      endforeach;
    }
    $result->free_result();
    $data['Pictures'] = array();
    $sql = " SELECT * FROM alb_pictures WHERE aid = ? LIMIT ?,?";
    $pictures_result = $this->db->query($sql, array($album_id, $offset, $per_page));
    if ($pictures_result->num_rows() > 0) {
      foreach ($pictures_result->result_array() as $row):
        $data['Pictures'][] = $row;
      endforeach;
    }
    return $data;
  }

  function get_pictures_count($album_id) {
    $this->db->get_where('alb_pictures', array('aid' => $album_id));
    return $this->db->affected_rows();
  }

  function update_album($album_id, $album) {
    $this->db->where('aid', $album_id);
    $status = $this->db->update('alb_albums', $album);
    return $status;
  }

  function update_photo_caption($photo_id, $photo_caption) {
    $this->db->where('pid', $photo_id);
    $this->db->update('alb_pictures', array('title' => $photo_caption));
    return $this->db->affected_rows();
  }

  function get_photos($photo_ids) {
    $this->db->where_in('pid', $photo_ids);
    $result = $this->db->get('alb_pictures');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function delete_photos($photo_ids) {
    $this->db->where_in('pid', $photo_ids);
    $result = $this->db->delete('alb_pictures');
    return $this->db->affected_rows();
  }

  function get_album_categories() {
    $this->db->select('cid,name');
    $result = $this->db->get('alb_categories');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function get_album_config($options) {
    $this->db->where_in('name', $options);
    $result = $this->db->get('alb_config');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row):
        $data[$row['name']] = $row['value'];
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function insert_album($album) {
    $str = $this->db->insert('alb_albums', $album);
    return $this->db->insert_id();
  }

  function get_all_albums($per_page = '', $offset = '') {
    $this->db->select('*');
    if (!empty($per_page) && !empty($offset)) {
      $this->db->limit($per_page, $offset);
    }
    $result = $this->db->get('alb_albums');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row):
        $data[] = $row;
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function insert_picture($picture) {
    $str = $this->db->insert('alb_pictures', $picture);
    return $this->db->insert_id();
  }

  function get_admins_and_permissions($per_page = 10, $offset = 0, $query = '') {
    $per_page = (int) $per_page;
    $offset = (int) $offset;
    if (!empty($query)) {
      $sql = 'SELECT
                                    d.id as id,
                                    d.first_name as name,
                                    d.last_name as surname,
									d.role_id,
                                    e.name as concept_name,
                                    a.announcements as announcements,
                                    a.news as news,
                                    a.offers as offers,
                                    a.photos as photos,
                                    a.events as events,
                                    a.review as review,
                                    a.moderate as moderate,
                                    a.employees as employees,
                                    a.users as users,
                                    a.id as permission_id
                            FROM
                                    ci_master_concept e JOIN
                                    ci_users d ON e.id = d.concept_id LEFT JOIN
                                    ci_permissions a ON d.id = a.user_id
                            WHERE
                                    d.user_type = 2 AND
                                    (d.first_name LIKE "%' . $query . '%" OR d.last_name LIKE "%' . $query . '%" )
                            ORDER BY d.first_name LIMIT ?,?';
    } else {
      $sql = 'SELECT
                        d.id as id,
                        d.first_name as name,
                        d.last_name as surname,
						d.role_id,
                        e.name as concept_name,
                        a.announcements as announcements,
                        a.news as news,
                        a.offers as offers,
                        a.photos as photos,
                        a.events as events,
                        a.review as review,
                        a.moderate as moderate,
                        a.employees as employees,
                        a.users as users,
                        a.id as permission_id
                FROM
                        ci_master_concept e JOIN
                        ci_users d ON e.id = d.concept_id LEFT JOIN
                        ci_permissions a ON d.id = a.user_id
                WHERE
                        d.user_type = 2
                ORDER BY d.first_name LIMIT ?,?';
    }

    $result = $this->db->query($sql, array($offset, $per_page));
    $data = array();
    $ids = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $data['Users'][] = $row;
        $ids[] = $row['id'];
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function get_admins_count($query = '') {
    if (!empty($query)) {
      $sql = "SELECT
                      COUNT(*) AS `numrows` 
                    FROM 
                      `ci_users` 
                    WHERE 
                      `user_type` = '2' AND (
                      `first_name` LIKE '%{$query}%' OR
                      `last_name` LIKE '%{$query}%') ";
      $result = $this->db->query($sql);
      $admins_count = $result->result();
      $admins_count = $admins_count[0]->numrows;
    } else {
      $this->db->where('user_type', 2);
      $admins_count = $this->db->count_all_results('ci_users');
    }
    return $admins_count;
  }

  function update_permission_record($permission_id, $column_name) {
    $sql = "UPDATE ci_permissions SET " . $column_name . " = IF(" . $column_name . " = 1, 0, 1) WHERE id = ?";
    return $this->db->query($sql, array($permission_id));
  }

  function insert_permission_record($permission) {
    return $this->db->insert('ci_permissions', $permission);
  }

  function check_insert_or_update_permissions($user_id) {
    $result = $this->db->get_where('ci_permissions', array('user_id' => $user_id));
    $data = array();
    $total_rows = $result->num_rows();
    $permission_id = 0;
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        $permission_id = $row['id'];
      endforeach;
    }
    $result->free_result();
    return $permission_id;
  }

  function get_permission($user_id, $column_name = '') {
    $result = $this->db->get_where('ci_permissions', array('user_id' => $user_id));
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row):
        if (!empty($column_name)) {
          $data = $row[$column_name];
        } else {
          $data = $row;
        }
      endforeach;
    }
    $result->free_result();
    return $data;
  }

  function permissions_get_all_users_name($src_term) {
    $this->db->select("id,first_name,last_name");
    $this->db->like('first_name', $src_term);
    $this->db->or_like('last_name', $src_term);
    $result = $this->db->get_where('ci_users', array('user_type' => 1));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $row['full_name'] = $row['first_name'] . ' ' . $row['last_name'];
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function make_admin($user_id) {
    $data = array(
        'user_type' => 2
    );

    $this->db->where('id', $user_id);
    return $this->db->update('ci_users', $data);
    ;
  }

  function remove_admin($ids) {
    $data = array(
        'user_type' => 1
    );

    $this->db->where_in('id', $ids);
    return $this->db->update('ci_users', $data);
    ;
  }

  function get_album_slug_count($album_slug) {
    $this->db->where('keyword', $album_slug);
    $this->db->from('alb_albums');
    return $this->db->count_all_results();
  }

  function create_email_manager($data) {
    $str = $this->db->insert('ci_email_campaign', $data);
    return $this->db->insert_id();
  }

  function create_email_queue($data) {
    $str = $this->db->insert('ci_email_queue', $data);
    return $this->db->insert_id();
  }

  function read_email_manager($src_term = NULL,$limit,$offset) {
    $limit = (int) $limit;
    $offset = (int) $offset;
    if($src_term){
     $sql = 'SELECT
                    a.id,
                    a.email_title,
                    a.email_type,
                    a.created_on,
                    b.first_name,
                    b.last_name
                  FROM
                    ci_email_campaign a,
                    ci_users b
                  WHERE
                    a.initiator = b.id AND email_title LIKE "%'.$src_term.'%"
                  ORDER BY a.id DESC  LIMIT ? OFFSET ?';
    }else{
      $sql = 'SELECT
                    a.id,
                    a.email_title,
                    a.email_type,
                    a.created_on,
                    b.first_name,
                    b.last_name
                  FROM
                    ci_email_campaign a,
                    ci_users b
                  WHERE
                    a.initiator = b.id
                  ORDER BY a.id DESC  LIMIT ? OFFSET ?';
    }
    
    
    $result = $this->db->query($sql, array($limit,$offset));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    //echo $this->db->last_query();
    $result->free_result();
    return $data;
  }

  function count_email_manager($src_term = NULL) {
    if($src_term){
      $sql = 'SELECT
              count(a.id) as total_emails
            FROM
              ci_email_campaign a,
              ci_users b
            WHERE
              a.initiator = b.id AND email_title LIKE "%'.$src_term.'%"';
    }else{
      $sql = 'SELECT
              count(a.id) as total_emails
            FROM
              ci_email_campaign a,
              ci_users b
            WHERE
              a.initiator = b.id';
    }


    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row['total_emails'];
      }
    }
    $result->free_result();
    return $data;
  }

  function count_email_queue($campaign_id) {
    $sql = 'SELECT count(id) as total_pending_queue FROM ci_email_queue WHERE email_campaign_id = ?';

    $result = $this->db->query($sql, array($campaign_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row['total_pending_queue'];
      }
    }
    $result->free_result();
    return $data;
  }

  function count_email_queue_failed($campaign_id) {
    $sql = 'SELECT count(id) as total_pending_queue FROM ci_email_queue WHERE number_of_tries >= 5 AND email_campaign_id = ?';

    $result = $this->db->query($sql, array($campaign_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row['total_pending_queue'];
      }
    }
    $result->free_result();
    return $data;
  }

  function count_email_finished_queue($campaign_id) {
    $sql = 'SELECT count(id) as total_finished_queue FROM ci_notify_email_log WHERE email_campaign_id = ? ';

    $result = $this->db->query($sql, array($campaign_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row['total_finished_queue'];
      }
    }
    $result->free_result();
    return $data;
  }
  function email_queue_errors($campaign_id){
    $sql = 'SELECT
              a.to_email,
              a.to_name,
              a.number_of_tries,
              a.email_error,
              a.queued_on,
              b.email_title,
              b.id
            FROM
              ci_email_queue a,ci_email_campaign b
            WHERE
              b.id = a.email_campaign_id AND
              a.number_of_tries >= 5 AND
              a.email_campaign_id = ?  ';

    $result = $this->db->query($sql, array($campaign_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function email_queue_process(){
    $sql = 'SELECT
              a.id,
              a.email_campaign_id,
              a.to_email,
              a.to_name,
              b.email_title,
              b.email_subject,
              b.email_body,
              b.from_name,
              b.from_email
            FROM
              ci_email_queue a,
              ci_email_campaign b
            WHERE
              number_of_tries < 5 AND
              a.email_campaign_id = b.id
            ORDER BY a.id ASC LIMIT 200';
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function clear_email_queue($id){
    $sql = 'DELETE FROM `ci_email_queue` WHERE `id` = ?';
    $result = $this->db->query($sql,array($id));
    return $this->db->affected_rows();
  }
  function failed_email_log($id,$error_msg){
    $sql = 'UPDATE ci_email_queue SET number_of_tries = number_of_tries+1,email_error = ? WHERE id = ?';
    $result = $this->db->query($sql,array($error_msg,$id));
    return $this->db->affected_rows();
  }
  function notification_log($data) {
    $str = $this->db->insert('ci_notify_email_log', $data);
    return $this->db->insert_id();
  }

  function check_album_thumb($album_id){
    $sql = 'SELECT aid,thumb FROM alb_albums WHERE aid = ?';
    $result = $this->db->query($sql,array($album_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function update_album_thumb($pic_id,$album_id){
    $sql = 'UPDATE alb_albums SET thumb = ? WHERE aid = ?';
    $result = $this->db->query($sql,array($pic_id,$album_id));
    return $this->db->affected_rows();
  }
  function save_post_notification($notification_save) {
    $str = $this->db->insert('ci_mt_entry_event_save_notification', $notification_save);
    return $this->db->insert_id();
  }
  function get_saved_notifications($page_id,$page_type = 1){
    /* page type
     * 1. MT Entry
     * 2. Event
     *
     * Default is set to MT entry
     */
    $sql = 'SELECT * FROM ci_mt_entry_event_save_notification WHERE page_id = ? AND page_type = ?';
    $result = $this->db->query($sql,array($page_id,$page_type));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function delete_saved_notifications($saved_notifications_id){
    return $this->db->delete('ci_mt_entry_event_save_notification', array('id' => $saved_notifications_id));
  }
  function update_saved_notifications($notification_save, $notification_save_id) {
    $this->db->where('id', $notification_save_id);
    $this->db->update('ci_mt_entry_event_save_notification', $notification_save);
    return $this->db->affected_rows();
  }
  function get_users_with_review_permission(){
    $sql = "SELECT
              a.id,
              a.email,
              a.title,
              a.first_name,
              a.middle_name,
              a.last_name
             FROM
              ci_users a,
              ci_permissions b
             WHERE
              b.user_id = a.id AND
              b.review = 1";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function get_event_owner($event_id){
    $sql = "SELECT
              a.id,
              a.email,
              a.title,
              a.first_name,
              a.middle_name,
              a.last_name
             FROM
              ci_users a,
              ci_event b
             WHERE
              b.user_id = a.id AND
              b.id = ?";
    $result = $this->db->query($sql,array($event_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  function get_mt_entry_owner($entry_id){
    $sql = "SELECT
              a.id,
              a.email,
              a.title,
              a.first_name,
              a.middle_name,
              a.last_name
             FROM
              ci_users a,
              mt_entry b
             WHERE
              b.entry_author_id = a.id AND
              b.entry_id = ?";
    $result = $this->db->query($sql,array($entry_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  
    function weekly_news_offer_announcement_count($sdate, $edate) {
    
    $sql = "SELECT WEEK( entry_created_on, 6 ) AS 'week', COUNT( entry_id ) As 'entry_count'
            FROM mt_entry 
            WHERE entry_created_on BETWEEN  ? AND ?
            GROUP BY WEEK( entry_created_on, 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function weekly_login_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( login_time, 6 ) AS 'week', COUNT( user_id ) AS 'no_of_logins'
            FROM ci_login_tracker 
            WHERE login_time BETWEEN  ? AND ?
            GROUP BY WEEK( login_time, 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function weekly_status_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( create_dttm, 6 ) AS 'week', COUNT( id ) AS value
            FROM ci_status_updates 
            WHERE parent_status_id = 0 and create_dttm BETWEEN  ? AND ?
            GROUP BY WEEK( create_dttm, 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  
  function weekly_polls_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( created, 6 ) AS 'week', COUNT( poll_id ) AS value
            FROM v2_polls
            WHERE created BETWEEN  ? AND ?
            GROUP BY WEEK( created, 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function weekly_files_uploaded_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( FROM_UNIXTIME( uploaded_on ) , 6 ) AS 'week', COUNT( id ) AS 'no_of_files'
            FROM ci_files
            WHERE uploaded_on BETWEEN  UNIX_TIMESTAMP( ? ) AND ( ? )
            GROUP BY WEEK( FROM_UNIXTIME( uploaded_on ), 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function weekly_comments_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( create_dttm, 6 ) AS 'week', COUNT( id ) AS value
            FROM ci_status_updates 
            WHERE 'parent_status_id' !=0
            AND create_dttm BETWEEN ? AND ?
            GROUP BY WEEK( create_dttm, 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function weekly_likes_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( create_dttm, 6 ) AS 'week', COUNT( id ) AS value
          FROM ci_likes
          WHERE create_dttm BETWEEN ? AND ?
          GROUP BY WEEK( create_dttm, 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function weekly_uploaded_pictures_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( FROM_UNIXTIME( ctime ) , 6 ) AS 'week', COUNT( pid ) AS value
            FROM alb_pictures pics
            WHERE pics.filepath LIKE 'User%'
            AND FROM_UNIXTIME( ctime ) BETWEEN ? AND ?
            GROUP BY WEEK( FROM_UNIXTIME( ctime ) , 6 ) ";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }  
  function weekly_chat_msgs_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( FROM_UNIXTIME( sent ) , 6 ) AS 'week', COUNT( id ) AS value
            FROM cometchat
            WHERE FROM_UNIXTIME( sent ) BETWEEN ? AND ?
            GROUP BY WEEK( FROM_UNIXTIME( sent ) , 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
   
    $result->free_result();
    return $count_details;
  }
  
  function weekly_market_place_item_count($sdate,$edate) {
    
    $sql = "SELECT WEEK( FROM_UNIXTIME( added_on ) , 6 ) AS 'week', COUNT( id ) AS value
            FROM ci_market_items
            WHERE 'items_status' =1
            AND 'added_on' BETWEEN UNIX_TIMESTAMP(?) AND UNIX_TIMESTAMP(?) 
            GROUP BY WEEK( FROM_UNIXTIME( added_on ) , 6 )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }  
  
  function monthly_logins_count($sdate,$edate) {
    
    $sql = "SELECT MONTHNAME( login_time ) AS 'month', COUNT( user_id ) AS 'no_of_logins'
            FROM ci_login_tracker
            WHERE login_time BETWEEN ? AND ?
            GROUP BY MONTH( login_time )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  function monthly_unique_logins_count($sdate,$edate) {
    
    $sql = "SELECT MONTHNAME( login_time ) AS 'month', COUNT( DISTINCT user_id ) AS 'no_of_logins'
          FROM ci_login_tracker
          WHERE login_time  BETWEEN ? AND ?
          GROUP BY MONTH( login_time )";
    $result = $this->db->query($sql,array($sdate,$edate));
    $count_details = array();
    $count_details = $result->result_array();
    $result->free_result();
    return $count_details;
  }
  
  /**
   * Update the tmp_id and tmp_path with the actual album_id
   */
  function update_tmp_album($updated_data,$tmp_id) {
	$tmp_filepath = $tmp_id.'/';
    $this->db->where('filepath', $tmp_filepath);
    $this->db->update('alb_pictures', $updated_data);   
  }
  /**
   * Fetch the Picture details based on Album id
   */
  function get_photos_by_aid($aid) {
   $this->db->select('pid');
    $query = $this->db->get_where('alb_pictures', array('aid' => $aid));
    return $query->result_array();
  }
}

