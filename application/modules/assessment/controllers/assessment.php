<?php
class Assessment extends General {
	
	private $mt_category = "assessment"; //This for MT database "mt_category->category_basename" For Announcement category

	
	function Assessment(){
		parent::__construct();
		$this->load->model('assessment_model');
    $this->load->model('concept');
    $this->load->model('category');
    $this->load->model('user');
	}

  //Default Announcement page
	function index(){
    
	}
  function create(){
    
    if(isset($_POST) && !empty($_POST))
    {
      echo "<pre>".print_r($this->input->post(),"\n")."</pre>";
            
      exit;
      
    }
    
    $data = array();
    $user_id = $this->session->userdata('userid');
    $data['concepts'] = $this->concept->getConcepts(null,array('name'=>'asc'),null,'active');
    $data['categories'] = $this->category->getCategories();
    $data['title'] = "Assessment - Landmark Group";
    $this->load->view("assessment_template",$data);
    
  }
}

/* End of file assessment.php */
/* Location: ./system/application/controllers/assessment.php */