<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Assessment - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php  $this->load->view('include_files/common_includes_new'); ?>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/jquery-ui-slider.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/manage/token-input-facebook.css" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.tokeninput.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/manage.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/facebox.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/custom_js/manage/jquery.colorbox-min.js"></script>

</head>
<body class="full-width">
<?php $this->load->view('global_header'); ?>
<div class="section wrapper clearfix">
	<h2>Assessment Creation</h2>
</div>

<div class="section wrapper clearfix">

<div class="left-contents">

<div class="container">

    <div class="assess-form">
<?php $attributes = array('id' => 'assessment_form');
echo form_open('assessment/create', $attributes);?>
        	<div class="row">
            	<label for="title">Title</label>
              <div class="textC">
                <?php
                $data = array(
                'id'          => 'title',
                'class'   => '',
                'name' => 'title',
                );
                echo form_input($data);
                ?>
              </div>
            </div>
            <div class="row">
            	<label for="Concept">Concept</label>
                <?php
                foreach($concepts as $concept)
                {
                  $concepts_options[$concept['id']] = ucfirst(strtolower($concept['name'])); 
                }
                echo form_dropdown('concept', array(''=>' -- Select Concept -- ')+$concepts_options);
                ?>
            </div>
            <div class="row">
            	<label for="Assign">Assign to</label>
                <div class="textC">
                	<?php
                  $data = array(
                  'id'   => 'assign',
                  'class'=> 'autocomplete_user',
                  'name' => 'assign',
                  );
                  echo form_input($data);
                  ?>
                </div>
            </div>
            <div class="row">
            	<label for="Category">Category</label>
                <?php
                foreach($categories as $category)
                {
                  $categories_options[$category['id']] = ucfirst(strtolower($category['name'])); 
                }
                echo form_dropdown('category', array(''=>' -- Select Categories -- ')+$categories_options);
                ?>
            </div>
            <?php for($i = 0;$i < 10;$i++){ 
                  $style = '';
                  if($i > 2)
                  {
                    $style = 'display:none;';
                  }
            ?>
            <div class="row quest" style="<?php print $style;?>">
                <div class="textC">
                	<?php
                     
                      $data = array(
                      'id'          => 'questions_'.$i,
                      'class'   => 'questions',
                      'placeholder' => 'Question',
                      'name' => 'questions_'.$i,
                      'style' => $style,
                      );
                      echo form_input($data);
                  ?>
                </div>
            </div>
            <?php } ?>
            <div class="row quest">
                <input type="submit" id="" class="btn-sm" value="Done" />
            </div>
        </form>
    </div>
	</div> <!-- container -->


    </div>
    
	
</div> <!-- section -->

<script type="text/javascript">
$(document).ready(function() {
$(".autocomplete_user").tokenInput("<?=base_url();?>manage/auto_users",{
theme: "facebook",
});

$('.quest').eq(2).append('<a href="#" id="more_tab">+More</a>');

$('#more_tab').click(function(e){
  
});

});
</script>
</body>
</html>