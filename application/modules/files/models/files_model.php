<?php

class Files_model extends CI_Model {

  function Files_model() {
    parent::__construct();
  }

  /* get all Files master Category */

  function files_get_all_categories($cat_filter = "important_cat = 1") {

    $sql = "SELECT id,name,category_icon,important_cat FROM ci_master_file_category WHERE {$cat_filter} ORDER BY sort_order ASC";
    $result = $this->db->query($sql);

    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Latest Files added to category */

  function files_get_latest_files($cat_id, $sort_by = "id") {
    $sql = "SELECT
	           a.id,
	           a.name,
	           a.unique_id,
	           a.file_size,
	           a.path,
	           b.name as cat_name
	         FROM
	           ci_files a,
	           ci_master_file_category b
	         WHERE
	           file_category_id = {$cat_id} AND
	           files_status = 1 AND
	           a.file_category_id = b.id
	         ORDER BY a.{$sort_by} DESC LIMIT 5";

    //$this->db->select('id,name,unique_id,file_size,path');
    //$this->db->order_by("id", "desc");
    //$result=$this->db->get_where('ci_files',array('file_category_id' => $cat_id,'files_status' => 1),5);
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Get tags for file */

  function files_get_tags_for_file($file_id) {
    $sql = "SELECT a.name, a.id FROM ci_master_file_tag a,ci_map_files_tag b WHERE a.id=b.file_tag_id AND b.files_id = ? ";
    $result = $this->db->query($sql, $file_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* List All Files system */

  function files_get_all_files($limit, $offset) {

    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.download_count,
			a.path,
			a.unique_id,
			a.uploaded_on,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* File Details */

  function files_get_files_details($file_id) {
    $sql = "SELECT
							a.name,
							a.description,
              a.file_category_id,
							a.file_size,
							a.download_count,
							a.uploaded_on,
							a.unique_id,
							a.path,
							a.user_id,
							b.important_cat
						FROM
							 ci_files a,
							 ci_master_file_category b
						WHERE
							a.id = ? AND
							a.files_status = 1 AND
							a.file_category_id = b.id";
    $result = $this->db->query($sql, $file_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* List All Files by Category system */

  function files_get_all_files_category($cat_id, $limit, $offset) {

    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.download_count,
			a.path,
			a.unique_id,
			a.uploaded_on,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id AND
			a.file_category_id = ?

			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql, $cat_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* List All Files by LINKS system */

  function files_get_all_files_category_links($cat_id, $limit, $offset) {

    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.download_count,
			a.path,
			a.unique_id,
			a.uploaded_on,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id AND
			a.file_size IS NULL
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql, $cat_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* List All Files by FILES system */

  function files_get_all_files_category_files($cat_id, $limit, $offset) {

    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.download_count,
			a.path,
			a.unique_id,
			a.uploaded_on,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id AND
			a.file_size > 0
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql, $cat_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* List All Files by Tag system */

  function files_get_all_files_tags($tag_id, $limit, $offset) {

    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.download_count,
			a.path,
			a.unique_id,
			a.uploaded_on,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c,
			ci_master_file_tag d,
			ci_map_files_tag e
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id AND
			d.id = '" . $tag_id . "' AND
			d.id = e.file_tag_id AND
			e.files_id = a.id
			ORDER BY a.id DESC
		limit {$limit} offset {$offset}";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Latest Files added to system */

  function files_latest_addded() {
    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.file_size,
			a.path,
			a.unique_id,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id
			ORDER BY a.id DESC
		limit 5";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* MOst Downloaded Files */

  function files_most_downloaded() {
    $this->db->select('id,name');
    $this->db->order_by("id", "desc");

    $sql = "SELECT
			a.name as title,
			a.id as file_id,
			a.path,
			a.download_count,
			a.unique_id,
			b.name as category,
			b.id as cat_id,
			c.first_name,
			c.last_name,
			c.id as user_id
		FROM
			`ci_files` a,
			ci_master_file_category b,
			ci_users c
		WHERE
			a.files_status = 1 AND
			a.file_category_id = b.id AND
			a.user_id = c.id
			ORDER BY a.download_count DESC
		limit 5";

    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* User details */

  function files_user_details($user_id) {
    $sql = "SELECT
							a.first_name,
							a.last_name,
							a.extension,
							a.phone,
							a.mobile,
							a.email,
							a.profile_pic,
							b.name as concept,
							c.name as designation
						FROM
							ci_users a,
							ci_master_concept b,
							ci_master_designation c
						WHERE
							a.id = ? AND
							a.concept_id = b.id AND
							a.designation_id = c.id	";


    $result = $this->db->query($sql, $user_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Files count in a category */

  function files_get_cat_files_count($cat_id) {
    $this->db->select('count(id)as files_count');
    $result = $this->db->get_where('ci_files', array('file_category_id' => $cat_id, 'files_status' => 1));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Files count All active files */

  function files_get_files_count() {
    $this->db->select('count(id)as files_count');
    $result = $this->db->get_where('ci_files', array('files_status' => 1));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Count All active files */

  function files_get_uploaded_files_count() {
    $sql = "SELECT count(a.id)as files_count FROM ci_files a,ci_master_file_category b WHERE a.file_size > 0 AND a.files_status = 1 AND b.id = a. file_category_id AND  b.important_cat =1";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Count All active Links */

  function files_get_uploaded_links_count() {
    $sql = "SELECT count(a.id)as files_count FROM ci_files a,ci_master_file_category b WHERE a.file_size IS NULL AND a.files_status = 1 AND b.id = a. file_category_id AND  b.important_cat =1";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Files count All active files for Category */

  function files_get_category_files_count($cat_id) {
    $sql = "SELECT count(a.id)as files_count FROM ci_files a WHERE a.file_category_id= ? AND files_status = 1";
    $result = $this->db->query($sql, $cat_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Files count All active LINKS for Category */

  function files_get_category_links_count($cat_id) {
    $sql = "SELECT count(a.id)as files_count FROM ci_files a WHERE a.file_size IS NULL AND files_status = 1";
    $result = $this->db->query($sql, $cat_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Files count All active FILES for Category */

  function files_get_category_upfiles_count($cat_id) {
    $sql = "SELECT count(a.id)as files_count FROM ci_files a WHERE a.file_size > 0 AND files_status = 1";
    $result = $this->db->query($sql, $cat_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Files count All active files for Tags */

  function files_get_tag_files_count($tag_id) {
    $sql = "SELECT count(a.id)as files_count FROM ci_files a,ci_map_files_tag b WHERE b.file_tag_id = ? AND files_status = 1 AND a.id = b.file_tag_id ";
    $result = $this->db->query($sql, $tag_id);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Get all Files Tags */

  function files_get_all_tag($src_term,$group_id = false) {
    $this->db->like('name', $src_term, 'after');
    if(isset($group_id) && !empty($group_id))
    {
      $this->db->where('group_id',$group_id);
    }
    else
    {
      $this->db->where('group_id',0);
    }
    $result = $this->db->get('ci_master_file_tag');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  /* Get all users email address */

  function files_get_all_users_email($src_term) {
    $this->db->select('id,email');
    $this->db->like('first_name', $src_term);
    $this->db->or_like('last_name', $src_term);
    $result = $this->db->get('ci_users');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Count Files Tags */

  function files_check_tags($tags,$group_id = false) {
    $attribute =  array();
    if(isset($group_id) && !empty($group_id))
    {
      $attribute['group_id'] = $group_id;
    }
    else
    {
      $attribute['group_id'] = 0;
    }
    $attribute['name'] = $tags;
    $result = $query = $this->db->get_where('ci_master_file_tag', $attribute);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  /* Add Files to Database */

  function files_submit_share($data) {
    $str = $this->db->insert('ci_files', $data);
    return $this->db->insert_id();
  }
  function files_update_submit_share($data, $file_id) {
    $this->db->update('ci_files', $data, array("id" => $file_id));
    return $this->db->affected_rows();
  }
  /* Add Dropbox to Database */

  function files_submit_dropbox($data) {
    $str = $this->db->insert('ci_files_dropbox', $data);
    return $this->db->insert_id();
  }

  /* Create Files Tags Mapping */

  function files_submit_map_tag($data) {
    $str = $this->db->insert('ci_map_files_tag', $data);
    return $this->db->insert_id();
  }

  /* Create dropbox send user Mapping */

  function files_submit_map_dropbox_send_user($data) {
    $str = $this->db->insert('ci_files_map_dropbox_send_user', $data);
    return $this->db->insert_id();
  }

  /* Add Files Tags to Database */

  function files_submit_tag($data) {
    $str = $this->db->insert('ci_master_file_tag', $data);
    return $this->db->insert_id();
  }

  /* Update Download COunt */

  function files_download_count($file_id) {
    $sql = "UPDATE ci_files SET download_count = (download_count +1) WHERE id = ?";
    $this->db->query($sql, $file_id);
    return $this->db->affected_rows();
  }

  /* Update dropbox Download COunt */

  function files_dropbox_download_count($file_id) {
    $sql = "UPDATE ci_files_dropbox SET download_count = (download_count +1) WHERE id = ?";
    $this->db->query($sql, $file_id);
    return $this->db->affected_rows();
  }

  /* Update File description */

  function files_update_file_desc($file_details, $file_id, $user_id) {
    $this->db->update('ci_files', $file_details, array("id" => $file_id, "user_id" => $user_id));
    return $this->db->affected_rows();
  }

  /* Flush File Tags */

  function files_tags_flush($file_id) {
    $this->db->delete('ci_map_files_tag', array('files_id' => $file_id));
    return $this->db->affected_rows();
  }

  /* File Download path */

  function files_download_path($unique_id) {
    $this->db->select('id,path,file_size');
    $result = $this->db->get_where('ci_files', array('unique_id' => $unique_id, 'files_status' => 1));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Active Dropbox files */

  function files_get_all_active_dropbox_files($user_id) {
    $this->db->select('id,path,unique_id,download_count,uploaded_on,file_size');
    $result = $this->db->get_where('ci_files_dropbox', array('user_id' => $user_id, 'dropbox_status' => 1));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* Dropbox file path */

  function files_dropbox_download_path($unique_id) {
    $this->db->select('id,path');
    $result = $this->db->get_where('ci_files_dropbox', array('unique_id' => $unique_id, 'dropbox_status' => 1));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  /* File Tag list for sidebar */

  function files_tag_list($tag_order) {
    $sql = "SELECT
							b.id,
							b.name as tags,
							count(a.id) as total_count
						FROM
							ci_files a,
							ci_master_file_tag b,
							ci_map_files_tag c
						WHERE
							a.files_status = 1 AND
							c.files_id = a.id AND
							c.file_tag_id = b.id
							GROUP BY b.id
							ORDER BY {$tag_order} ASC limit 30";

    $result = $this->db->query($sql, $tag_order);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        if (trim($row['tags'])) {
          $data[] = $row;
        }
      }
    }
    $result->free_result();
    return $data;
  }

  /* File category list for sidebar */

  function files_category_list($file_cat_order) {
    $sql = "SELECT
							b.id,
							b.name as file_category,
							count(a.id) as total_count
						FROM
							ci_files a,
							ci_master_file_category b
						WHERE
							a.files_status = 1 AND
							a.file_category_id = b.id
							GROUP BY b.id
							ORDER BY {$file_cat_order} ASC limit 20";

    $result = $this->db->query($sql, $file_cat_order);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Advance search
  function files_get_all_concepts() {
    $result = $this->db->get('ci_master_concept');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  function files_get_all_locations() {
    $result = $this->db->get('ci_master_territory');
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

//Sphinx Get Files
  function files_get_all_files_search($limit, $offset, $matcharray) {

    $i = 0;
    $j = 1;
    $data = array();
    foreach ($matcharray as $key => $value) {

      if ($i >= $offset && $j <= $limit) {
        $sql = "SELECT
					a.name as title,
					a.id as file_id,
					a.file_size,
					a.download_count,
					a.path,
					a.unique_id,
					a.uploaded_on,
					b.name as category,
					b.id as cat_id,
					c.first_name,
					c.last_name,
					c.id as user_id
				FROM
					`ci_files` a,
					ci_master_file_category b,
					ci_users c
				WHERE
					a.files_status = 1 AND
					a.file_category_id = b.id AND
					a.user_id = c.id AND
					a.id = $key";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
          foreach ($result->result_array() as $row) {
            $data[] = $row;
          }
          $j++;
        }
      }
      $i++;
    }
    $result->free_result();
    return $data;
  }
  function delete_file($file_id,$user_id){
    //$this->db->delete('ci_files', array('id' => $file_id,'user_id'=> $user_id));
	$this->db->update('ci_files', array('files_status'=>2), array('id' => $file_id));
    return $this->db->affected_rows();
  }
}