<?php
/*
* Name: jobs.php
* Type: Controller
* Description: Careers controller has all the basic functionality to list the jobs, single job description.
* Author: KUNAL PATIL
* Date: 19-09-2013
*/ 
class Files extends General {

  function __construct() {
    parent::__construct();


    $this->load->model('files_model');
    $this->load->model('File');
    $this->load->model('FileCategory');
    $this->load->model('ObjectMember'); 
    $this->load->model('GroupConceptPermission');
    $this->load->model('manage/manage_model');
    $this->load->model('includes/general_model'); //General Module

    $this->load->model('Survey');
    $this->load->model('Outlets');
    $this->config->load('audit');
  }
//Default File module page
 /* function index() {
    redirect("files/show_files");
  }*/
  function index()
  {
    $data['title'] = "Files and Links - Landmark Group";

    $this->load->model('Concept');
    $data['concepts'] = $this->Concept->getAll();
    $data['audit_constant'] = $this->config->item('audit');
    $this->load->model('FileCategory');
    $data['file_categories'] = $this->FileCategory->getFileCategories(null, 'active');

    //print_r($data['file_categories']);
    //die();


    $this->load->model('FileTag');
    $data['filetags'] = $this->FileTag->getPopularTags();

    $data['value_title'] = "";
    $data['value_desc'] = "";
    $data['value_category'] = "";
    $data['value_link'] = "";
    $data['value_tags'] = "";
    $this->load->view('files_links', $data);
  }

    function autosearch_ajax() {
      $user_id = $this->session->userdata('userid');
      //print_r($user_id);exit;
      $search_term = $this->input->get('term');
      $group = $this->input->get('group');
      $moderator = $this->input->get('moderator');
      if((isset($moderator) && !empty($moderator)) && ($moderator == 0))
      {
        $member_id = $user_id;
      }
	  if(isset($search_term) && trim($search_term) != ''){
      if(!(isset($member_id))) {
        $files = $this->File->searchFiles(10, 0, $search_term,$group);
      } else {
        $files = $this->File->searchFiles(10, 0, $search_term,$group,$member_id);
      }
	  }
      $data['json'] = json_encode($files);
      $this->load->view("print_json", $data);
    }

    function files_ajax() {
    	echo $user_id = $this->session->userdata('userid');
      $moderator = $this->input->get('moderator');
     	$limit = $this->input->get('limit');
    	$offset = $this->input->get('start');
    	$filters = array();
    	$search_term = $this->input->get('search_term');
    	$files = $this->input->get('files');
      $tag = $this->input->get('tag');
    	$concept = $this->input->get('concept');
      $month = $this->input->get('month');
      $year = $this->input->get('year');

      $category = $this->input->get('category');
      
      $group = $this->input->get('group');

      if(isset($category) && $category != '' && $category != 0)
      {
        $filters['cat_id'] = $category;
      }

      if(isset($concept) && $concept != '' && $concept != 0)
      {
        $filters['concept_id'] = $concept;
      }

      if(isset($tag) && $tag != '' && $tag != 0)
      {
        $filters['tag_id'] = $tag;
      }
      if(isset($group) && !empty($group))
      {
        $filters['group_file'] = 1;
        //if((isset($moderator) && !empty($moderator)) && ($moderator == 0))
        if(($moderator == 0))
        {
          $filters['member_id'] = $user_id;
        }
        $filters['group_id'] = $group;
        $filters['month'] = $month;
        $filters['year'] = $year;
      }
      else
      {
        $filters['group_file'] = 0;
      }
    	$country = $this->input->get('country');
    	if($files == '1')
    		$filters['is_file'] = $files;
    	if($files == '2')
      		$filters['is_link'] = $files;
    	if($search_term != '')
    		$filters['search_term'] = $search_term;

      //$users = $this->User->getAllUsers($user_id, $limit, $offset, $filters);
      $filelisting = $this->File->getFiles($limit,$offset,$filters);
       //print_r($filelisting);
      if(isset($filelisting[0]))
      {

	
	 foreach($filelisting as $key => $file)
	{
		 $view_path = ''; 
		$d = date('d M Y',$file['uploaded_on']);
		if ($file['file_size'])
		{
			$file_info = pathinfo($file['path']);
			$file_name = $file_info['filename'];
			$file_extension = $file_info['extension'];
			$tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";
			$view_path = '';      
      
			if (file_exists($file_name )) {
			$file_image_thumb = $tmp_thumb;
			} else {
			$file_image_thumb ="";
			}
			$target='';
		}
		else
		{
			$file_image_thumb = base_url() . "images/files_links/tnLink.png";
			$target='target="_blank"';
			$file['icon']='link';
		}
		
		$path = $file['path'];		
		$ext = pathinfo($path, PATHINFO_EXTENSION);


		$ppt = array("ppt", "pptx", "pps");
		$doc = array("doc", "docx", "rtf", "txt");
		$xls = array("xls", "xlsx", "xlr");

		$img = array("bmp", "gif", "jpg", "jpeg", "png");
		$open = array("odt", "ods", "odp");
		$pdf = array("pdf", "ppd");
		$audio = array("mp3", "mpa", "wav", "wma", "ra", "m3u", "aif" , "iff");
		$video = array("mp4", "mpg","mpeg");
		$zipped = array("7z", "tar", "rar", "zipx", "tar.gz", "rpm", "pkg","gz" ,"zip");
		if(in_array($ext,$ppt) && $file['file_size'])
		{
			$file['icon']='ppt';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnPowerpoint.png";
			}
			$view_path = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;
		}
		else if(in_array($ext,$doc)  && $file['file_size'])
		{
			$file['icon']='doc';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnDocument.png";
			}
			$view_path = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;
		}
		else if(in_array($ext,$xls)  && $file['file_size'])
		{
			$file['icon']='xls';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnSpreadsheet.png";
			}
			$view_path = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;

		}
		else if(in_array($ext,$video)  && $file['file_size'])
		{
			$file['icon']='video';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnVideo.png";
			}
      $view_path = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;
		}
		else if(in_array($ext,$audio)  && $file['file_size'])
		{
			$file['icon']='audio';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnSound.png";
			}

		}
		else if(in_array($ext,$img)  && $file['file_size'])
		{
			$file['icon']='img';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnPhoto.png";
			}
			$view_path = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;
		}
		else if(in_array($ext,$open)  && $file['file_size'])
		{
			$file['icon']='open';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnOpenFile.png";
			}
		}
		/*elseif($file['cat_id']== 8)
		{
			$file['icon']='link';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnLink.png";
			}
		}*/
	/*	elseif($file['cat_id']== 15)
		{
			$file['icon']='computer';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnComputer.png";
			}
		}*/

		else if(in_array($ext,$pdf)  && $file['file_size'])
		{
			$file['icon']='pdf';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnPdf.png";
			}
			$view_path = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;
		}
		
		else if(in_array($ext,$zipped) && $file['file_size'])
		{
			$file['icon']='zipped';
			if($file_image_thumb == "")
			{
				$file_image_thumb = base_url() . "images/files_links/tnZipRar.png";
			}
		}
		else
		{
			$file['icon']='General';
			$file_image_thumb = base_url() . "images/files_links/tnGeneral.png";
		}
		/*elseif($file['cat_id']== 17)
		{
			$file['icon']='corpcomm';
			if($file_image_thumb == "")
			{
			$file_image_thumb = base_url() . "images/files_links/tnCorpComm.png";
			}
		}
		*/
		$file['date']=$d;
		$file['target']=$target;
		$file['file_image_thumb']=$file_image_thumb;
		$file['download_path']= base_url()."files/files_download/".$file['unique_id'];
		$file['view_path']= $view_path; 
		$t[$d][] =  $file;
	}
	$data['filelisting']=$t;
	}
	else
	{
	  $data['filelisting']['search_term']=$search_term;
	  if($search_term != "")
	  {
	  $data['filelisting']['not_found']=true;
	  }
	  $data['filelisting']['no_result']=true;

	}
  /*	print_r($data['filelisting']);
  	die();*/
  	$data['json'] = json_encode($data['filelisting']);

  	$this->load->view("print_json", $data);
  }

  //File upload generate thumbnail
  function show_files() {
    //Advance Search
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();
    $sort_filter = $this->uri->segment(3, 0);

    if ($sort_filter) {
      if ($sort_filter == "popular") {
        $sort_order = "download_count";
        $data['sort_latest_file'] = '';
        $data['sort_popular_file'] = 'class="active"';
      } else {
        $sort_order = "id";
        $data['sort_latest_file'] = 'class="active"';
        $data['sort_popular_file'] = '';
      }
    } else {
      $sort_order = "id";
      $data['sort_latest_file'] = 'class="active"';
      $data['sort_popular_file'] = '';
    }


    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $test = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list

    $files_cat = $this->files_model->files_get_all_categories($cat_filter = "important_cat <> 0");

    $db_total_files = $this->files_model->files_get_files_count();
    $data['total_files'] = $db_total_files['files_count'];

    $db_total_uploaded_files = $this->files_model->files_get_uploaded_files_count();
    $db_total_uploaded_links = $this->files_model->files_get_uploaded_links_count();

    $file_count_check = 0;
    $link_count_check = 0;
    if (count($files_cat)) {
      foreach ($files_cat as $cat) {
        $cat_name = $cat['name'];
        $cat_icon = $cat['category_icon'];
        $cat_id = $cat['id'];
        $latest_files = $this->files_model->files_get_latest_files($cat_id, $sort_order);
        $db_files_count = $this->files_model->files_get_cat_files_count($cat_id);
        if ($db_files_count['files_count']) {
          if ($cat['important_cat'] == 1) {
            if (count($latest_files)) {
              foreach ($latest_files as $files) {
                //Icon handling
                if ($files['file_size']) {
                  $file_info = pathinfo($files['path']);
                  $file_name = $file_info['filename'];
                  $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

                  if ($file_name != "") {
                    $file_image_thumb = $tmp_thumb;
                  } else {
                    $file_image_thumb = base_url() . "images/files/files-prev.jpg";
                  }
                } else {
                  $file_image_thumb = base_url() . "images/files/link-prev.jpg";
                }

                if ($files['file_size']) {
                  if ($file_count_check < 5) {
                    $data['latest_files_cat']["Files"]['files_count'] = $db_total_uploaded_files['files_count'];
                    $data['latest_files_cat']["Files"]['files_cat_icon'] = $cat_icon;
                    $data['latest_files_cat']["Files"]['files_cat_id'] = "files";
                    $data['latest_files_cat']["Files"]['latest_files']['file_thumb'][] = $file_image_thumb;

                    $data['latest_files_cat']["Files"]['latest_files']['title'][] = $files['name'];
                    $data['latest_files_cat']["Files"]['latest_files']['link'][] = $files['id'];
                    $data['latest_files_cat']["Files"]['latest_files']['unique_id'][] = $files['unique_id'];
                    $data['latest_files_cat']["Files"]['latest_files']['cat_name'][] = $files['cat_name'];

                    $data['latest_files_cat']["Files"]['latest_files']['file'][] = '<a class="download-link" href="' . site_url("files/files_download/" . $files['unique_id']) . '">Download File</a>';
                    $data['latest_files_cat']["Files"]['latest_files']['link_class'][] = "tool-tip-file";
                    $data['latest_files_cat']["Files"]['latest_files']['link_pointer_img'][] = "bullet-tip.png";
                    $data['latest_files_cat']["Files"]['latest_files']['file_size'][] = $files['file_size'] . ' KB';

                    $ext = ($files['path']) ? substr(strrchr($files['path'], '.'), 1) : "URL";
                    $data['latest_files_cat']["Files"]['latest_files']['ext'][] = strtoupper($ext);
                    $file_count_check++;
                  }
                } else {
                  if ($link_count_check < 5) {
                    $data['latest_files_cat']["Links"]['files_count'] = $db_total_uploaded_links['files_count'];
                    $data['latest_files_cat']["Links"]['files_cat_icon'] = $cat_icon;
                    $data['latest_files_cat']["Links"]['files_cat_id'] = "links";
                    $data['latest_files_cat']["Links"]['latest_files']['file_thumb'][] = $file_image_thumb;

                    $data['latest_files_cat']["Links"]['latest_files']['title'][] = $files['name'];
                    $data['latest_files_cat']["Links"]['latest_files']['link'][] = $files['id'];
                    $data['latest_files_cat']["Links"]['latest_files']['unique_id'][] = $files['unique_id'];
                    $data['latest_files_cat']["Links"]['latest_files']['cat_name'][] = $files['cat_name'];

                    $data['latest_files_cat']["Links"]['latest_files']['file'][] = '<a target="_blank" class="view-link" href="' . $files['path'] . '">Visit link</a>';
                    $data['latest_files_cat']["Links"]['latest_files']['link_class'][] = "tool-tip-link";
                    $data['latest_files_cat']["Links"]['latest_files']['link_pointer_img'][] = "ico-61.png";
                    $data['latest_files_cat']["Links"]['latest_files']['file_size'][] = '';
                    $data['latest_files_cat']["Links"]['latest_files']['ext'][] = "URL";
                    $link_count_check++;
                  }
                }
              }
            }
          } else {
            $data['imp_files_cat'][$cat_name]['files_count'] = $db_files_count['files_count'];
            $data['imp_files_cat'][$cat_name]['files_cat_icon'] = $cat_icon;
            $data['imp_files_cat'][$cat_name]['files_cat_id'] = $cat_id;
            if (count($latest_files)) {
              foreach ($latest_files as $files) {

                if ($files['file_size']) {
                  $file_info = pathinfo($files['path']);
                  $file_name = $file_info['filename'];
                  $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

                  if (@fopen($tmp_thumb, "r")) {
                    $file_image_thumb = $tmp_thumb;
                  } else {
                    $file_image_thumb = base_url() . "images/files/files-prev.jpg";
                  }
                } else {
                  $file_image_thumb = base_url() . "images/files/link-prev.jpg";
                }

                //$file_image_thumb = base_url()."images/files/file_1.jpg";

                $data['imp_files_cat'][$cat_name]['latest_files']['title'][] = $files['name'];
                $data['imp_files_cat'][$cat_name]['latest_files']['link'][] = $files['id'];
                $data['imp_files_cat'][$cat_name]['latest_files']['unique_id'][] = $files['unique_id'];
                $data['imp_files_cat'][$cat_name]['latest_files']['cat_name'][] = $files['cat_name'];
                $data['imp_files_cat'][$cat_name]['latest_files']['file_thumb'][] = $file_image_thumb;




                if ($files['file_size']) {
                  $data['imp_files_cat'][$cat_name]['latest_files']['file'][] = '<a class="download-link" href="' . site_url("files/files_download/" . $files['unique_id']) . '">Download File</a>';
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_class'][] = "tool-tip-file";
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_pointer_img'][] = "bullet-tip.png";
                  $data['imp_files_cat'][$cat_name]['latest_files']['file_size'][] = $files['file_size'] . ' KB';

                  $ext = ($files['path']) ? substr(strrchr($files['path'], '.'), 1) : "URL";
                  $data['imp_files_cat'][$cat_name]['latest_files']['ext'][] = strtoupper($ext);
                } else {
                  $data['imp_files_cat'][$cat_name]['latest_files']['file'][] = '<a target="_blank" class="view-link" href="' . $files['path'] . '">Visit link</a>';
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_class'][] = "tool-tip-link";
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_pointer_img'][] = "ico-61.png";
                  $data['imp_files_cat'][$cat_name]['latest_files']['file_size'][] = '';

                  $data['imp_files_cat'][$cat_name]['latest_files']['ext'][] = "URL";
                }
              }
            }
          }
        }
      }
    }
    //echo "<xmp>".print_r($data,1)."</xmp>";exit;
    //Newly Uploaded files
    $db_latest_added = $this->files_model->files_latest_addded();
    //Data formating
    if (is_array($db_latest_added)) {
      for ($i = 0; $i < (count($db_latest_added)); $i++) {
        $icon_path = "";
        $fileName = $db_latest_added[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        /*if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }*/
        if(!$db_latest_added[$i]['file_size']){
          $icon_path = base_url() . "images/files/ext/ico-url.jpg";
        }else if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
        $db_latest_added[$i]['file_ext'] = $ext;
        $db_latest_added[$i]['file_icon'] = $icon_path;
      }
    }
    $data['latest_addded_files'] = $db_latest_added;
    //Most Downloaded Files
    $db_most_downloaded = $this->files_model->files_most_downloaded();
    //Data formating
    if (is_array($db_most_downloaded)) {
      for ($i = 0; $i < (count($db_most_downloaded)); $i++) {
        $icon_path = "";
        $fileName = $db_most_downloaded[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
        $db_most_downloaded[$i]['file_icon'] = $icon_path;
      }
    }
    $data['most_downloaded_files'] = $db_most_downloaded;
    $this->load->view("files_show_2_template", $data);
  }

  /* Dropbox */

  function files_dropbox() {
    $this->load->helper('time_ago');
    $user_id = $this->session->userdata('userid');
    $db_dropbox_active_file = $this->files_model->files_get_all_active_dropbox_files($user_id);

    //Expire time
    if (count($db_dropbox_active_file)) {
      $db_dropbox_active = array();
      for ($i = 0; $i < count($db_dropbox_active_file); $i++) {
        $uploaded_time = $db_dropbox_active_file[$i]['uploaded_on'];

        $db_dropbox_active[$i]['download_count'] = $db_dropbox_active_file[$i]['download_count'];
        $db_dropbox_active[$i]['unique_id'] = $db_dropbox_active_file[$i]['unique_id'];
        $db_dropbox_active[$i]['file_name'] = basename($db_dropbox_active_file[$i]['path']);
        $db_dropbox_active[$i]['file_size'] = $db_dropbox_active_file[$i]['file_size'] . " KB";

        //echo time_ago($db_dropbox_active[$i]['expire_time'])."<br />";
        $expire_timestamp = mktime(date("H", $uploaded_time), date("i", $uploaded_time), date("s", $uploaded_time), date("n", $uploaded_time), date("j", $uploaded_time) + 7, date("Y", $uploaded_time));
        $db_dropbox_active[$i]['expire_time'] = date("d M Y", $expire_timestamp);
      }
    } else {
      $db_dropbox_active = "";
    }
    //echo "Time check"; exit;
    $data['active_dropbox_files'] = $db_dropbox_active;
    //echo "<xmp>".print_r($db_dropbox_active,1)."</xmp>";exit;
    $this->load->view("files_dropbox_template", $data);
  }
//Users upload file page
  function your_files() {

    $this->load->helper('time_ago');
    $this->load->model("profile/m_profile");

    //Advance Search
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();
    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list
    //File Category Sort Order
    $file_cat_order = "total_count";
    $data['cat_sort_order'] = $file_cat_order;
    $data['file_cat_list'] = $this->files_model->files_category_list($file_cat_order); //Sidebar file category list

    $profile_id = $this->session->userdata("userid");
    $total_files = $this->m_profile->files_get_files_count($profile_id);
    $total_files = $total_files['files_count'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/files/your_files/';
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['uri_segment'] = 3;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(3, 0)) ? $this->uri->segment(3, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(3, 0) + $config['per_page']);
    $this->pagination->initialize($config);

    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->m_profile->files_get_all_files($profile_id, $config['per_page'], $this->uri->segment(3, 0));

    //Data formating
    if (is_array($db_all_files)) {
      for ($i = 0; $i < (count($db_all_files)); $i++) {

        $uploaded_on_time = $db_all_files[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $db_all_files[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $db_all_files[$i]['path'];

        $ext = substr(strrchr($fileName, '.'), 1);
        $file_size = $db_all_files[$i]['file_size'];

        $ext = ($file_size) ? $ext : "url";
        if ($file_size) {
          $file_info = pathinfo($fileName);
          $file_name = $file_info['filename'];
          $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

          if (@fopen($tmp_thumb, "r")) {
            $icon_path = $tmp_thumb;
          } else {
            $icon_path = base_url() . "images/files/files-prev.jpg";
          }
        } else {
          $icon_path = base_url() . "images/files/link-prev.jpg";
        }
/*
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
*/
        //Tags
        $db_file_tags = $this->m_profile->files_get_tags_for_file($db_all_files[$i]['file_id']);
        $tag_arr = array();
        if (is_array($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            $tag_arr[] = '<a class="grey-shade-1" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
          }
          $file_tag = @implode(", ", $tag_arr);
        }

        $db_all_files[$i]['file_tags'] = $file_tag;
        $db_all_files[$i]['file_ext'] = $ext;
        $db_all_files[$i]['file_icon'] = $icon_path;
      }
    }
    $data['files_list'] = $db_all_files;
    $this->load->view('your_files_template', $data);
  }

  /* Files Share page */

  function share_files() {

    $user_type = $this->session->userdata("usertype");
    if ($user_type == 2) {
      $cat_filter = "important_cat <> 0";
    } else {
      $cat_filter = "important_cat = 1";
    }

    $data['files_categories'] = $this->files_model->files_get_all_categories($cat_filter);

    $data['value_title'] = "";
    $data['value_desc'] = "";
    $data['value_category'] = "";
    $data['value_link'] = "";
    $data['value_tags'] = "";

    $this->load->view('files_share_template', $data);
  }
//file details page
  function files_details() {
    $file_id = $this->uri->segment(3, 0);
    $curr_user_id = $this->session->userdata("userid");
    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list
    //File Category Sort Order
    $file_cat_order = "total_count";
    $data['cat_sort_order'] = $file_cat_order;
    $data['file_cat_list'] = $this->files_model->files_category_list($file_cat_order); //Sidebar file category list

    $db_files_details = $this->files_model->files_get_files_details($file_id);
    if (!count($db_files_details)) {
      redirect("files/all_files");
      exit;
    }
    //Data formating
    for ($i = 0; $i < (count($db_files_details)); $i++) {
      $uploaded_on_time = $db_files_details[$i]['uploaded_on'];
      $file_size = $db_files_details[$i]['file_size'];
      $ext = ($db_files_details[$i]['path']) ? substr(strrchr($db_files_details[$i]['path'], '.'), 1) : "URL";

      $ext = ($file_size) ? $ext : "url";

      //Icon
      $icon = base_url() . "images/files/ext/big/ico-" . strtolower($ext) . ".jpg";
      if (@fopen($icon, "r")) {
        $file_icon = base_url() . "images/files/ext/big/ico-" . strtolower($ext) . ".jpg";
      } else {
        $file_icon = base_url() . "images/files/ext/big/ico-default.jpg";
      }
      //File Tags
      $db_file_tags = $this->files_model->files_get_tags_for_file($file_id);
      $tag_arr = array();
      if (is_array($db_file_tags)) {
        foreach ($db_file_tags as $tags) {
          $tag_arr[] = $tags['name'];
        }
        $file_tag = @implode(", ", $tag_arr);
      }
      $user_id = $db_files_details[$i]['user_id'];
      $file_unique_id = $db_files_details[$i]['unique_id'];


      $data['file_id'] = $file_id;
      $data['file_title'] = $db_files_details[$i]['name'];
      $data['file_description'] = $db_files_details[$i]['description'];
      $data['file_size'] = $file_size;
      $data['file_download_count'] = $db_files_details[$i]['download_count'];
      $data['file_uploaded_on'] = date("M d, Y", $uploaded_on_time);
      $data['file_extension'] = ($file_size) ? $ext : "URL";
      $data['file_ext_icon'] = $file_icon;
      $data['file_tags'] = $file_tag;
      $data['important_cat'] = $db_files_details[$i]['important_cat'];

      $data['file_editable'] = ($curr_user_id == $user_id) ? true : false;

      //Download Link
      if ($db_files_details[$i]['file_size']) {
        $data['download_path'] = '<a href="' . base_url() . 'index.php/files/files_download/' . $file_unique_id . '">Download the file</a>';
      } else {
        $data['download_path'] = '<a target="_blank" href="' . base_url() . 'index.php/files/files_download/' . $file_unique_id . '">View link</a>';
      }

      //User detials for vcard
      $db_user_details = $this->files_model->files_user_details($user_id);
      //echo "<xmp>".print_r($db_user_details,1)."</xmp>";exit;
      if (!count($db_user_details)) {
        redirect("files/all_files");
        exit;
      }
      $data['profile_pic'] = $db_user_details[0]['profile_pic'];
      $data['full_name'] = $db_user_details[0]['first_name'] . " " . $db_user_details[0]['last_name'];
      $data['user_designation'] = $db_user_details[0]['designation'];
      $data['user_concept'] = $db_user_details[0]['concept'];
      $data['user_ph_ext'] = $db_user_details[0]['extension'];
      $data['user_phone'] = $db_user_details[0]['phone'];
      $data['user_mobile'] = $db_user_details[0]['mobile'];
      $data['user_email'] = $db_user_details[0]['email'];
    }

    //echo "<xmp>".print_r($db_files_details,1)."</xmp>";
    //exit;
    $this->load->view('files_details_template', $data);
  }

  /* List All Files By Tags */

  function all_tags() {
    $tag_id = $this->uri->segment(3, 0);
    if (!$tag_id || !is_numeric($tag_id)) {
      redirect("files/all_files");
    }
    //Advance Search
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();

    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list
    //File Category Sort Order
    $file_cat_order = "total_count";
    $data['cat_sort_order'] = $file_cat_order;
    $data['file_cat_list'] = $this->files_model->files_category_list($file_cat_order); //Sidebar file category list

    $this->load->helper('time_ago'); //Time Ago Function
    $total_files = $this->files_model->files_get_tag_files_count($tag_id);
    $total_files = $total_files['files_count'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/files/all_tags/' . $tag_id . '/';
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";
    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->files_model->files_get_all_files_tags($tag_id, $config['per_page'], $this->uri->segment(4, 0));

    //Data formating
    if (is_array($db_all_files)) {
      for ($i = 0; $i < (count($db_all_files)); $i++) {

        $uploaded_on_time = $db_all_files[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $db_all_files[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $db_all_files[$i]['path'];

        $ext = substr(strrchr($fileName, '.'), 1);

        $file_size = $db_all_files[$i]['file_size'];
        $ext = ($file_size) ? $ext : "url";

        if ($file_size) {
          $file_info = pathinfo($fileName);
          $file_name = $file_info['filename'];
          $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

          if (@fopen($tmp_thumb, "r")) {
            $icon_path = $tmp_thumb;
          } else {
            $icon_path = base_url() . "images/files/files-prev.jpg";
          }
        } else {
          $icon_path = base_url() . "images/files/link-prev.jpg";
        }
/*
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
*/
        //Tags
        $db_file_tags = $this->files_model->files_get_tags_for_file($db_all_files[$i]['file_id']);
        $tag_arr = array();
        if (count($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            if (trim($tags['name'])) {
              $tag_arr[] = '<a class="grey-shade-1" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
              if ($tag_id == $tags['id']) {
                $data['current_tag'] = $tags['name'];
              }
            }
          }
          $file_tag = @implode(", ", $tag_arr);
        }

        $db_all_files[$i]['file_tags'] = $file_tag;
        $db_all_files[$i]['file_ext'] = $ext;
        $db_all_files[$i]['file_icon'] = $icon_path;

        if ($db_all_files[$i]['file_size']) {
          $db_all_files[$i]['dwd_link'] = '<a href="' . site_url("files/files_download/" . $db_all_files[$i]['unique_id']) . '"><img class="file-dwd-ico" src="' . base_url() . 'images/ico-62.png" alt="download" /></a>';
        } else {
          $db_all_files[$i]['dwd_link'] = '<a target="_blank" href="' . $db_all_files[$i]['path'] . '"><img class="file-dwd-ico" src="' . base_url() . 'images/ico-63.png" alt="download" /></a>';
        }
      }
    }
    $data['files_list'] = $db_all_files;
    $this->load->view('all_tags_template', $data);
  }

  /* List All Files By Category */

  function all_category() {
    $cat_id = $this->uri->segment(3, 0);
    if (!$cat_id) {
      redirect("files/all_files");
    }
    //Advance Search
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();

    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list
    //File Category Sort Order
    $file_cat_order = "total_count";
    $data['cat_sort_order'] = $file_cat_order;
    $data['file_cat_list'] = $this->files_model->files_category_list($file_cat_order); //Sidebar file category list

    $this->load->helper('time_ago'); //Time Ago Function


    switch ($cat_id) {
      case 'links': $total_files = $this->files_model->files_get_category_links_count($cat_id);
        break;
      case 'files': $total_files = $this->files_model->files_get_category_upfiles_count($cat_id);
        break;
      default: $total_files = $this->files_model->files_get_category_files_count($cat_id);
    }





    $total_files = $total_files['files_count'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/files/all_category/' . $cat_id . '/';
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['uri_segment'] = 4;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";
    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    switch ($cat_id) {
      case 'links': $db_all_files = $this->files_model->files_get_all_files_category_links($cat_id, $config['per_page'], $this->uri->segment(4, 0));
        $data['current_category'] = "Links";
        break;
      case 'files': $db_all_files = $this->files_model->files_get_all_files_category_files($cat_id, $config['per_page'], $this->uri->segment(4, 0));
        $data['current_category'] = "Files";
        break;
      default: $db_all_files = $this->files_model->files_get_all_files_category($cat_id, $config['per_page'], $this->uri->segment(4, 0));
    }


    //Data formating
    if (is_array($db_all_files)) {
      for ($i = 0; $i < (count($db_all_files)); $i++) {

        $uploaded_on_time = $db_all_files[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $db_all_files[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $db_all_files[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        $file_size = $db_all_files[$i]['file_size'];
        $ext = ($file_size) ? $ext : "url";
        //Icon handling
        if ($file_size) {
          $file_info = pathinfo($fileName);
          $file_name = $file_info['filename'];
          $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

          if (@fopen($tmp_thumb, "r")) {
            $icon_path = $tmp_thumb;
          } else {
            $icon_path = base_url() . "images/files/files-prev.jpg";
          }
        } else {
          $icon_path = base_url() . "images/files/link-prev.jpg";
        }

/*
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
*/
        //Tags
        $db_file_tags = $this->files_model->files_get_tags_for_file($db_all_files[$i]['file_id']);
        $tag_arr = array();
        if (is_array($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            if (trim($tags['name'])) {
              $tag_arr[] = '<a class="high" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
            }
          }
          $file_tag = @implode(", ", $tag_arr);
        }

        //Current Category
        if (!isset($data['current_category'])) {
          $data['current_category'] = $db_all_files[$i]['category'];
        }

        $db_all_files[$i]['file_tags'] = $file_tag;
        $db_all_files[$i]['file_ext'] = $ext;
        $db_all_files[$i]['file_icon'] = $icon_path;

        if ($db_all_files[$i]['file_size']) {
          $db_all_files[$i]['dwd_link'] = '<a href="' . site_url("files/files_download/" . $db_all_files[$i]['unique_id']) . '"><img class="file-dwd-ico" src="' . base_url() . 'images/ico-62.png" alt="download" /></a>';
        } else {
          $db_all_files[$i]['dwd_link'] = '<a target="_blank" href="' . $db_all_files[$i]['path'] . '"><img class="file-dwd-ico" src="' . base_url() . 'images/ico-63.png" alt="download" /></a>';
        }
      }
    }
    $data['files_list'] = $db_all_files;
    $this->load->view('all_category_template.php', $data);
  }

  /* List All Files */

  function all_files() {
    //Advance Search
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();

    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list
    //File Category Sort Order
    $file_cat_order = "total_count";
    $data['cat_sort_order'] = $file_cat_order;
    $data['file_cat_list'] = $this->files_model->files_category_list($file_cat_order); //Sidebar file category list

    $this->load->helper('time_ago'); //Time Ago Function
    $total_files = $this->files_model->files_get_files_count();
    $total_files = $total_files['files_count'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/files/all_files';
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(3, 0)) ? $this->uri->segment(3, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(3, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->files_model->files_get_all_files($config['per_page'], $this->uri->segment(3, 0));

    //Data formating
    if (is_array($db_all_files)) {
      for ($i = 0; $i < (count($db_all_files)); $i++) {

        $uploaded_on_time = $db_all_files[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $db_all_files[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $db_all_files[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);

        $file_size = $db_all_files[$i]['file_size'];
        $ext = ($file_size) ? $ext : "url";

        //Icon handling
          if ($file_size) {
            $file_info = pathinfo($fileName);
            $file_name = $file_info['filename'];
            $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

            if (@fopen($tmp_thumb, "r")) {
              $icon_path = $tmp_thumb;
            } else {
              $icon_path = base_url() . "images/files/files-prev.jpg";
            }
          } else {
            $icon_path = base_url() . "images/files/link-prev.jpg";
          }

/*

        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }


*/


        //Tags
        $file_tag = "";
        $db_file_tags = $this->files_model->files_get_tags_for_file($db_all_files[$i]['file_id']);
        $tag_arr = array();
        if (is_array($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            if (trim($tags['name'])) {
              $tag_arr[] = '<a class="high" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
            }
          }
          $file_tag = @implode(", ", $tag_arr);
        }
        //echo "<xmp>".print_r($tag_arr,1)."</xmp>";exit;

        $db_all_files[$i]['file_tags'] = $file_tag;
        $db_all_files[$i]['file_ext'] = strtoupper($ext);
        $db_all_files[$i]['file_icon'] = $icon_path;

        if ($db_all_files[$i]['file_size']) {
          $db_all_files[$i]['dwd_link'] = '<a href="' . site_url("files/files_download/" . $db_all_files[$i]['unique_id']) . '"><img class="file-dwd-ico" src="' . base_url() . 'images/ico-62.png" alt="download" /></a>';
        } else {
          $db_all_files[$i]['dwd_link'] = '<a target="_blank" href="' . $db_all_files[$i]['path'] . '"><img class="file-dwd-ico" src="' . base_url() . 'images/ico-63.png" alt="download" /></a>';
        }
      }
    }
    $data['files_list'] = $db_all_files;
    $this->load->view('all_files_template', $data);
  }

  /* Files home page */

  function bak_show_files() {
    //Advance Search
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();
    $sort_filter = $this->uri->segment(3, 0);

    if ($sort_filter) {
      if ($sort_filter == "popular") {
        $sort_order = "download_count";
        $data['sort_latest_file'] = '';
        $data['sort_popular_file'] = 'class="active"';
      } else {
        $sort_order = "id";
        $data['sort_latest_file'] = 'class="active"';
        $data['sort_popular_file'] = '';
      }
    } else {
      $sort_order = "id";
      $data['sort_latest_file'] = 'class="active"';
      $data['sort_popular_file'] = '';
    }


    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;
    $data['tag_list'] = $test = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list

    $files_cat = $this->files_model->files_get_all_categories($cat_filter = "important_cat <> 0");

    $db_total_files = $this->files_model->files_get_files_count();
    $data['total_files'] = $db_total_files['files_count'];
    if (count($files_cat)) {
      foreach ($files_cat as $cat) {
        $cat_name = $cat['name'];
        $cat_icon = $cat['category_icon'];
        $cat_id = $cat['id'];
        $latest_files = $this->files_model->files_get_latest_files($cat_id, $sort_order);
        $db_files_count = $this->files_model->files_get_cat_files_count($cat_id);
        if ($db_files_count['files_count']) {
          if ($cat['important_cat'] == 1) {
            $data['latest_files_cat'][$cat_name]['files_count'] = $db_files_count['files_count'];
            $data['latest_files_cat'][$cat_name]['files_cat_icon'] = $cat_icon;
            $data['latest_files_cat'][$cat_name]['files_cat_id'] = $cat_id;
            if (count($latest_files)) {
              foreach ($latest_files as $files) {
                $data['latest_files_cat'][$cat_name]['latest_files']['title'][] = $files['name'];
                $data['latest_files_cat'][$cat_name]['latest_files']['link'][] = $files['id'];
                $data['latest_files_cat'][$cat_name]['latest_files']['unique_id'][] = $files['unique_id'];
                $data['latest_files_cat'][$cat_name]['latest_files']['cat_name'][] = $files['cat_name'];

                if ($files['file_size']) {
                  $data['latest_files_cat'][$cat_name]['latest_files']['file'][] = '<a class="download-link" href="' . site_url("files/files_download/" . $files['unique_id']) . '">Download File</a>';
                  $data['latest_files_cat'][$cat_name]['latest_files']['link_class'][] = "tool-tip-file";
                  $data['latest_files_cat'][$cat_name]['latest_files']['link_pointer_img'][] = "bullet-tip.png";
                  $data['latest_files_cat'][$cat_name]['latest_files']['file_size'][] = 'File Size: ' . $files['file_size'] . 'KB';

                  $ext = ($files['path']) ? substr(strrchr($files['path'], '.'), 1) : "URL";
                  $data['latest_files_cat'][$cat_name]['latest_files']['ext'][] = $ext;
                } else {
                  $data['latest_files_cat'][$cat_name]['latest_files']['file'][] = '<a target="_blank" class="view-link" href="' . $files['path'] . '">Visit link</a>';
                  $data['latest_files_cat'][$cat_name]['latest_files']['link_class'][] = "tool-tip-link";
                  $data['latest_files_cat'][$cat_name]['latest_files']['link_pointer_img'][] = "ico-61.png";
                  $data['latest_files_cat'][$cat_name]['latest_files']['file_size'][] = '';
                  $data['latest_files_cat'][$cat_name]['latest_files']['ext'][] = "URL";
                }
              }
            }
          } else {
            $data['imp_files_cat'][$cat_name]['files_count'] = $db_files_count['files_count'];
            $data['imp_files_cat'][$cat_name]['files_cat_icon'] = $cat_icon;
            $data['imp_files_cat'][$cat_name]['files_cat_id'] = $cat_id;
            if (count($latest_files)) {
              foreach ($latest_files as $files) {
                $data['imp_files_cat'][$cat_name]['latest_files']['title'][] = $files['name'];
                $data['imp_files_cat'][$cat_name]['latest_files']['link'][] = $files['id'];
                $data['imp_files_cat'][$cat_name]['latest_files']['unique_id'][] = $files['unique_id'];
                $data['imp_files_cat'][$cat_name]['latest_files']['cat_name'][] = $files['cat_name'];
                if ($files['file_size']) {
                  $data['imp_files_cat'][$cat_name]['latest_files']['file'][] = '<a class="download-link" href="' . site_url("files/files_download/" . $files['unique_id']) . '">Download File</a>';
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_class'][] = "tool-tip-file";
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_pointer_img'][] = "bullet-tip.png";
                  $data['imp_files_cat'][$cat_name]['latest_files']['file_size'][] = 'File Size: ' . $files['file_size'] . 'KB';

                  $ext = ($files['path']) ? substr(strrchr($files['path'], '.'), 1) : "URL";
                  $data['imp_files_cat'][$cat_name]['latest_files']['ext'][] = $ext;
                } else {
                  $data['imp_files_cat'][$cat_name]['latest_files']['file'][] = '<a target="_blank" class="view-link" href="' . $files['path'] . '">Visit link</a>';
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_class'][] = "tool-tip-link";
                  $data['imp_files_cat'][$cat_name]['latest_files']['link_pointer_img'][] = "ico-61.png";
                  $data['imp_files_cat'][$cat_name]['latest_files']['file_size'][] = '';

                  $data['imp_files_cat'][$cat_name]['latest_files']['ext'][] = "URL";
                }
              }
            }
          }
        }
      }
    }
    //echo "<xmp>".print_r($data,1)."</xmp>";exit;
    //Newly Uploaded files
    $db_latest_added = $this->files_model->files_latest_addded();
    //Data formating
    if (is_array($db_latest_added)) {
      for ($i = 0; $i < (count($db_latest_added)); $i++) {
        $icon_path = "";
        $fileName = $db_latest_added[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
        $db_latest_added[$i]['file_ext'] = $ext;
        $db_latest_added[$i]['file_icon'] = $icon_path;
      }
    }
    $data['latest_addded_files'] = $db_latest_added;
    //Most Downloaded Files
    $db_most_downloaded = $this->files_model->files_most_downloaded();
    //Data formating
    if (is_array($db_most_downloaded)) {
      for ($i = 0; $i < (count($db_most_downloaded)); $i++) {
        $icon_path = "";
        $fileName = $db_most_downloaded[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
        $db_most_downloaded[$i]['file_icon'] = $icon_path;
      }
    }
    $data['most_downloaded_files'] = $db_most_downloaded;

    //echo "<xmp>".print_r($latest_addded_files,1)."</xmp>";exit;
    $this->load->view('files_show_template', $data);
  }

  /* Files TAG Auto complete */

  function autocomplete_files_tag() {
    $group_id = '';
    $src_term = $this->input->post('term');
    $group_id = $this->input->post('group_id');
    $files_tags = $this->files_model->files_get_all_tag($src_term,$group_id);
    $json_result = array();
    foreach ($files_tags as $val) {
      $json_result[] = '{"id": "' . $val['id'] . '", "label": "' . $val['name'] . '"}';
    }
    if (count($json_result)) {
      $result = '[ ' . @implode(",", $json_result) . ' ]';
	  
    } else {
      $result = '[ {"id": "0", "label": "'.$src_term.'"} ]';
    }
    $data['json'] = $result;
	$this->load->view("files_print_json", $data);
  }

  /* Files Send To Auto complete (users email) */

  function autocomplete_files_send_to() {
    $src_term = $this->input->post('term');
    $files_tags = $this->files_model->files_get_all_users_email($src_term);
    $json_result = array();

    foreach ($files_tags as $val) {
      $json_result[] = '{"id": "' . $val['id'] . '", "label": "' . $val['email'] . '"}';
    }
    if (count($json_result)) {
      $result = '[ ' . @implode(",", $json_result) . ']';
    } else {
      $result = '[ {"id": "0", "label": "No Results"} ]';
    }
    $data['json'] = $result;
    $this->load->view("files_print_json", $data);
  }

//Add to Dropbox
  function submit_file_dropbox() {
    $config = array();
    if (!$this->input->post("hd_submit_form")) {
      redirect("files/files_dropbox");
    }
    $this->load->library('email');

    //File Uploading Module Configuration
    $config['upload_path'] = './files_dropbox';
    $config['allowed_types'] = 'cdr|psd|ai|avi|wmv|mpg|mpeg|3gp|mp4|mp3|wav|wma|csv|xml|flv|swf|fla|html|htm|mht|odt|odp|ods|pdf|ppt|pptx|doc|docx|xls|xlsx|jpg|gif|png|tif|jpeg';
    $config['max_size'] = '102400';
    $config['remove_spaces'] = true;
    $config['overwrite'] = false;
    $config['max_width'] = '0';
    $config['max_height'] = '0';
    $fileName = "file_share";
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload($fileName)) {
      $this->session->set_flashdata('file_upload_error', $this->upload->display_errors());
      redirect("files/files_dropbox");
    } else {
      $file_share_details = $this->upload->data();
      $mime_type = $file_share_details['file_type'];
      $file_path = $file_share_details['full_path'];
      $file_size = $file_share_details['file_size'];
    }

    //$files_tags = $this->input->post('txt_tags');
    //$files_tags = @explode(",",$files_tags);

    $email_arr = @explode(",", $this->input->post('txt_sendto'));

    if (is_array($email_arr)) {
      $user_id = $this->session->userdata('userid');
      $user_details = $this->files_model->files_user_details($user_id);
      $sender_email = $user_details[0]['email'];
      $sender_name = $user_details[0]['first_name'] . " " . $user_details[0]['last_name'];


      $unique_id = uniqid();
      $unique_code = md5($unique_id);

      $subject = $this->input->post('txt_subject');
      $dropbox_message = $this->input->post('txtar_message');

      $download_link = base_url() . 'index.php/files/files_dropbox_download/' . $unique_code;


      if (!$subject) {
        $subject = "You have a received a file From " . $sender_name;
      }

      if (!$dropbox_message) {
        $dropbox_message = "You have received a file, below is the download link: ";
      }

      $dropbox_message.= "<br /><br />Sent By,<br />" . $sender_name;
      $message = '<table width="520" border="0" style="font-family:Arial;">
        <tr>
          <td style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
            <p style="color:#8c8d8d; font-family:Arial;margin:30px 0 0 0;padding:0px;font-size:13px;">DROPBOX ALERT</p>
            <p style="color:#1e5886;font-size:21px;font-family:Arial;font-weight:bold;margin:10px 0 0 0;padding:0px;">' . $subject . '</p>
            <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0; line-height:25px;">' . $dropbox_message . '</p>
            <p style="color:#3e3e3e;font-size:14px; margin:13px 0 0 0;">Download Link</p>
            <p style="font-size:16px; margin:0px;"><a style="color:#1e5886;" href="' . $download_link . '">' . $download_link . '</a></p>
          </td>
        </tr>
      </table>';


      //creating Insert array for database
      $dropbox_files = array(
          'id' => NULL,
          'subject' => $subject,
          'message' => $message,
          'path' => $file_path,
          'file_size' => $file_size,
          'download_count' => 0,
          'user_id' => $this->session->userdata('userid'),
          'uploaded_on' => time(),
          'unique_id' => $unique_code,
          'dropbox_status' => 1
      );
      $last_id = $this->files_model->files_submit_dropbox($dropbox_files);

      /*
      $config = array();
      $config['protocol'] = 'smtp';
      $config['smtp_host'] = 'airtelupdates.in';
      $config['smtp_user'] = 'guru@airtelupdates.in';
      $config['smtp_pass'] = 'ip31415';
      $config['mailtype'] = "html";
       *
       */
      $this->load->helper('smtp_config');
      $smtp_config = smtp_config();
      //$config['']='';
      $this->email->initialize($smtp_config);
      $this->email->set_newline("\r\n");
      $this->email->from($sender_email, $sender_name);

      $this->email->subject($subject);
      $this->email->message($message);

      foreach ($email_arr as $email) {
        if (trim($email)) {
          $files_map_user = array(
              'id' => NULL,
              'dropbox_id' => $last_id,
              'user_email' => $email
          );
          $this->files_model->files_submit_map_dropbox_send_user($files_map_user);

          $this->email->to($email);
          $this->email->send();
        }
      }
    }
    $this->session->set_flashdata('file_upload_ok', 'Your file has been sent successfully.');
    redirect("files/files_dropbox/", "refresh");
  }

//Add Files to the system
  function submit_share_files() {
  /*print "hi";
  print_r($_POST);
  exit;*/
    if (!$this->input->post("hd_submit_form")) {
      redirect("files/share_files");
    }
    $file_id = $this->input->post('file_share_id');
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('', '');
    $this->form_validation->set_rules('cmb_cat', 'Item Category', 'required|trim');
    $this->form_validation->set_rules('txt_title', 'Title', 'required|trim');
    //$this->form_validation->set_rules('txtar_description', 'Description', 'required|trim');
    //$this->form_validation->set_rules('txt_tags', 'Tags', 'trim|callback_file_tag_check');//Calling custom function
    if (!$this->form_validation->run($this)) {
      $data['value_title'] = set_value('txt_title');
      $data['value_desc'] = set_value('txtar_description');
      $data['value_category'] = set_value('cmb_cat');
      $data['value_link'] = set_value('txt_link');
      $data['value_tags'] = set_value('txt_tags');
      $data['files_categories'] = $this->files_model->files_get_all_categories();
      $this->load->view('files_share_template', $data);
    } else {
          if (strlen($this->input->post('txt_link')) > 5) {
            $file_path = $this->input->post('txt_link');
            $mime_type = "";
            $file_size = "";
          } else {
            //File Uploading Module Configuration
            //base_url()."files/files_download/".$file['unique_id'];
            $config['upload_path'] = './files_downloads';
            $config['allowed_types'] = 'cdr|psd|ai|avi|wmv|mpg|mpeg|3gp|mp4|mp3|wav|wma|csv|xml|flv|swf|fla|html|htm|mht|odt|odp|ods|pdf|ppt|pptx|doc|docx|xls|xlsx|jpg|gif|png|tif|jpeg|txt|rtf';
            $config['max_size'] = '102400';
            $config['remove_spaces'] = true;
            $config['overwrite'] = false;
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            if($_FILES['file_share']['type'] == "video/mp4") {


                 $raw_file_name_d = $_FILES['file_share']['name'];
                 $info = pathinfo($raw_file_name_d);
                 $file_name_d = $info['filename'];

                 $raw_file_name_new = trim($file_name_d , "_");
                // $raw_file_name_before = preg_replace('/[^a-zA-Z0-9-]/', '_', $raw_file_name_new);
                 $raw_file_name_before = str_replace( array( '-', '.' , '(' , ')' , '[' , ']' , '{' , '}' , ' ', '<', '>', '&', '{', '}', '*' , '$' , '@' , '!' , '#', '+' , '?' ,'~' , ':' , '^' , '%' , '`',';' , '/' ,'"' , '=' , ',' , '|'), '', $raw_file_name_new);
                 
                 $raw_file_name_remove_underscore = preg_replace('/[_]+/','_',$raw_file_name_before);

                 $config['file_name'] = $raw_file_name_remove_underscore;
              }

            $fileName = "file_share";
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ( ! is_dir($config['upload_path']) ) die("THE UPLOAD DIRECTORY DOES NOT EXIST");
              if (!$file_id){
               // echo "here";
                if (!$this->upload->do_upload($fileName)) {

                  $this->session->set_flashdata('file_upload_status', $this->upload->display_errors());
                  //redirect("files/share_files/");
                } else {
                  $file_share_details = $this->upload->data();
                  $mime_type = $file_share_details['file_type'];
                  $file_path = $file_share_details['full_path'];
                  $file_size = $file_share_details['file_size'];

                  $dir_path = $file_share_details['file_path'];
                  $file_ext = strtolower($file_share_details['file_ext']);
                  $raw_file_name = $file_share_details['raw_name'];
				  //convert image to prograsive
				 if(in_array($file_ext, array('jpeg','jpg','gif','bmp','png')))
				 {
					progressive_image($file_path, $file_ext);
				 }

                  $level_one_conv = array('.doc', '.docx', '.ppt', '.pptx', '.xls', '.xlsx', '.csv', '.txt', '.rtf'); // Level one conversion fo office files
              
                  if (in_array($file_ext, $level_one_conv)) {
                    //Document converter
                    $doc_converter = "/var/www/html/DocumentConverter.py"; //Converter Script File
                    //$doc_converter = "/var/www/landmark/intranet/trunk/DocumentConverter.py"; //Converter Script File
                    $pdf_file_path = "{$dir_path}tmp_files/{$raw_file_name}.pdf"; //Converted PDF file Path

                    $cmd = "python {$doc_converter} {$file_path} $pdf_file_path";

                    if (!file_exists("/tmp/soffice")) {
                      mkdir("/tmp/soffice");
                      chmod("/tmp/soffice", 0777);
                    }
                    //Creating a enviornment variable for openoffice
                    putenv("HOME=/tmp/soffice");
                    exec($cmd, $output, $returnValue);
                    if (file_exists($pdf_file_path)) {
                      $img_file_path = "{$dir_path}file_thumbs/{$raw_file_name}.jpg"; // Convert Ijmage path

                      $cmd_image = "convert {$pdf_file_path}[0] -resize 110x143 -gravity center -extent 110x143 {$img_file_path}";
                      exec($cmd_image, $img_output, $img_returnValue);
                      unlink($pdf_file_path);
                    }
                  } elseif ($file_ext == '.pdf') {
                    $img_file_path = "{$dir_path}file_thumbs/{$raw_file_name}.jpg"; // Convert Ijmage path
                    $cmd_image = "convert {$file_path}[0] -resize 110x143 -gravity center -extent 110x143 {$img_file_path}";
                    exec($cmd_image, $img_output, $img_returnValue);
                  }
                  //echo "<xmp>".print_r($file_share_details,1)."</xmp>";exit;
                  
                }
              }
          }
        $GroupExist = $this->input->post('group');
        if(isset($GroupExist) && !empty($GroupExist))
        {
          $name_exist = $this->File->checkfilenameExist($GroupExist,$this->input->post('txt_title'));
        }
        else
        {
          $name_exist = $this->File->getByFilters(array('name'=>$this->input->post('txt_title'),'files_status' => 1),'*');
        }
        
        if(isset($name_exist['id']) && $name_exist['id'] != '' && empty($file_id))
        {
            $fails['done']= 'no';
            $fails['samename'] = 'yes';
            /*$temp['json'] = json_encode($fails);
            echo $temp['json']; exit;*/
            
          /*  
          echo $success; exit;
          print "<pre>";
          print_r($name_exist);
          exit;*/
        }
        else
        {
          $unique_id = uniqid();
          $unique_code = md5($unique_id);

          //creating Insert array for database
          $file_concept = $this->input->post('file_concept');
          $group = '';
          $group = $this->input->post('group');
          if(!$file_concept) {
            $file_concept = 0;
          }
          $files_assignee = $this->input->post('file_assignee');
          $file_id = $this->input->post('file_share_id');
          $user_id = $this->session->userdata('userid');
          if ($file_id) {
          
            $share_files = array(
              'id' => $file_id,
              'name' => $this->input->post('txt_title'),
              'description' => $this->input->post('txtar_description'),
              'file_category_id' => $this->input->post('cmb_cat'),
            );
            
            $last_id = $this->files_model->files_update_file_desc($share_files, $file_id, $user_id);
            $last_id = $file_id;
          } else {
            $share_files = array(
              'id' => NULL,
              'name' => $this->input->post('txt_title'),
              'description' => $this->input->post('txtar_description'),
              'file_category_id' => $this->input->post('cmb_cat'),
              'path' => $file_path,
              'file_mime_type' => ($mime_type) ? $mime_type : NULL,
              'file_size' => ($file_size) ? $file_size : NULL,
              'download_count' => 0,
              'user_id' => $this->session->userdata('userid'),
              'uploaded_on' => time(),
              'unique_id' => $unique_code,
              'files_status' => 1,
              'concept_id' => $file_concept,
              'group_file' => (isset($group) && !empty($group))?1:0,
              'have_assignee' => (isset($files_assignee) && !empty($files_assignee))?1:0,
            );
            $last_id = $this->files_model->files_submit_share($share_files);
          }
          if((isset($group) && !empty($group)) && (isset($files_assignee) && !empty($files_assignee)))
          {
            $this->load->model('Group');
            $RawAssginee = explode(',',$files_assignee);
            foreach($RawAssginee as $assignee)
            {
              $details = array();
              $details['object_id'] = $assignee;
              $details['object_type'] = 'User';
              $details['parent_type'] = 'File';
              $details['parent_id']  = $last_id;
              $data = $this->ObjectMember->getByFilters($details);
              if(!isset($data) || empty($data))
              {
                $this->ObjectMember->followObject($details);
              }
              
              $groupDetail = $this->Group->getGroup($group);
              $notification_html = 'A File has been added to Group <a href="'.base_url().'groups/'.$group.'">'.$groupDetail['name'].'</a>.';
              $notification = array(
                'object_id' => $assignee,
                'object_type' => 'User',
                'parent_id' => $this->session->userdata('userid'),
                'parent_type' => 'User',
                'activity_source' => $group,
                'activity_type' => 'Group_File',
                'dttm' => date('Y-m-d H:i:s',time()),
                'status' => 'unviewed',
                'notification' => $notification_html,
              );
              $this->Activities->groupUpdateNotification($notification);
            }
          }

          $files_tags = $this->input->post('txt_tags');
          if ($files_tags == "Start typing..") {
            $files_tags = "";
          }
          
          $files_tags = @explode(",", $files_tags);

          if (is_array($files_tags)) {
            $this->files_model->files_tags_flush($last_id);
            
            foreach ($files_tags as $tags) {
              $tags = trim($tags);
              if ($tags != "") {
                $result = $this->files_model->files_check_tags($tags,$group);
                if (count($result)) {
                  $tag_id = $result[0]['id'];
                } else {
                  $files_tag = array(
                      'id' => NULL,
                      'name' => $tags,
                      'group_id' => (isset($group) && !empty($group))?$group:0,
                  );
                  $tag_id = $this->files_model->files_submit_tag($files_tag);
                }
                if ($tag_id) {
                  $files_map_tag = array(
                      'id' => NULL,
                      'files_id' => $last_id,
                      'file_tag_id' => $tag_id
                  );
                  //echo"<pre>";print_r($files_map_tag);exit;
                  $this->files_model->files_submit_map_tag($files_map_tag);
                }
              }
            }
          }
          $this->session->set_flashdata('file_upload_status', 'Your file has been uploaded successfully .');
          //redirect("files/your_files/", "refresh");
         if ($file_id) {
          $fails['new_file']= 'no';
         }else {
          $fails['new_file']= 'yes';
         }
         $fails['done']= 'yes';
         $fails['samename'] = 'no';
         $fails['file_id'] = $last_id;
         $fails['details'] = $this->File->getFileDetails($last_id);

          $video_formats = array('.mp4', '.mpg', '.mpeg', '.avi', '.flv'); 
                 
          if(in_array($file_ext, $video_formats)){
                    
              $fails['video'] = 'yes'; 
              $fails['dir_path'] =  $dir_path; 
              $fails['raw_file_name'] = $raw_file_name;      
           }
          else {
             $fails['video'] = 'no';   
          }

        }
       
       $temp['json'] = json_encode($fails);
       echo $temp['json'];
    }
}

  /* Form validation For File Tags */

  function file_tag_check($str) {
    if ($str == "Start typing.." || $str == "No Results, " || $str == "No Results") {
      $this->form_validation->set_message('file_tag_check', 'The %s is required');
      return false;
    } else {
      return true;
    }
  }

  /* Edit File description */

  function submit_edit_file_description() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $file_desc = $this->input->post("file_desc");
    $file_id = $this->input->post("file_id");
    if (!trim($file_desc)) {
      $status = 1;
      $status_msg = "File Description is required.";
    } else {
      $status = 2;
    }
    if ($status == 2) {
      //creating update array for database
      $file_details = array(
          'description' => $file_desc
      );
      $desc_status = $this->files_model->files_update_file_desc($file_details, $file_id, $user_id);
      if ($desc_status) {
        $status_msg = "File description Changed";
      }
    }
    $result = '{"status": "' . $status_msg . '"}';
    $data['json'] = $result;
    $this->load->view("files_print_json", $data);
  }

  /* Edit File Tags */

  function submit_edit_file_tags() {
    $file_id = $this->input->post("file_id");
    $user_id = $this->session->userdata("userid");
    $files_tags = $this->input->post('file_tags');
    $files_tags = @explode(",", $files_tags);
    $status_msg = "";
    if (is_array($files_tags)) {
      $this->files_model->files_tags_flush($file_id);

      foreach ($files_tags as $tags) {
        $tags = trim($tags);
        $result = $this->files_model->files_check_tags($tags);
        if (count($result)) {
          $tag_id = $result[0]['id'];
        } else {
          $files_tag = array(
              'id' => NULL,
              'name' => $tags
          );
          $tag_id = $this->files_model->files_submit_tag($files_tag);
        }
        if ($tag_id) {
          $files_map_tag = array(
              'id' => NULL,
              'files_id' => $file_id,
              'file_tag_id' => $tag_id
          );
          $this->files_model->files_submit_map_tag($files_map_tag);
        }
      }
    } else {
      $status_msg = "No file tags found";
    }
    $result = '{"status": "' . $status_msg . '"}';
    $data['json'] = $result;
    $this->load->view("files_print_json", $data);
  }

  /* Files Download */

  function files_download() {
    $this->load->helper('download');
    $unique_id = $this->uri->segment(3, 0);
    $download_path = $this->files_model->files_download_path($unique_id);
    if (!count($download_path)) {
      redirect("files/all_files");
    } else {
      $file_id = $download_path['id'];
      $file_path = $download_path['path'];
      $file_size = $download_path['file_size'];

      $this->files_model->files_download_count($file_id);

      if (!$file_size) {
        redirect($file_path);
      }

      $ext = substr(strrchr($file_path, 'http'), 1);
      if (!(substr(strrchr($file_path, 'http'), 1)) || substr(strrchr($file_path, 'www'), 1)) {
        $file_name = basename($file_path);
        $get_file_content = file_get_contents($file_path);
        force_download($file_name, $get_file_content);
      } else {
        redirect($file_path);
      }
    }
    //echo "<xmp>".print_r($download_path,1)."</xmp>";
  }

  /* Dropbox download file */

  function files_dropbox_download() {
    $this->load->helper('download');
    $unique_id = $this->uri->segment(3, 0);
    $download_path = $this->files_model->files_dropbox_download_path($unique_id);
    if (!$unique_id || !count($download_path)) {
      redirect("files/files_dropbox");
    } else {
      $file_id = $download_path['id'];
      $file_path = $download_path['path'];
      $this->files_model->files_dropbox_download_count($file_id);
      $file_name = basename($file_path);
      $get_file_content = file_get_contents($file_path);

      force_download($file_name, $get_file_content);
    }
  }

  //Popular Tag Sort .
  function tag_sort() {
    $tag_order = $this->input->post("sort_order");
    $data['tag_list'] = $this->files_model->files_tag_list($tag_order);
    $data['sort_order'] = $tag_order;
    $this->load->view("files_sidebar_popular_tags", $data);
  }

  //File category Sort .
  function file_category_sort() {
    $category_sort = $this->input->post("sort_order");
    $data['file_cat_list'] = $this->files_model->files_category_list($category_sort);
    $data['cat_sort_order'] = $category_sort;
    $this->load->view("files_sidebar_category", $data);
  }

  /* Sphinx Search Files */

  function search() {
    //Advance Search
    if ($this->input->post('term')) {
      $searchterm = $this->input->post('term');
      $this->session->set_userdata(array('searchterm1' => $searchterm));
    } else {
      $searchterm = $this->session->userdata('searchterm1');
    }
    $result = array();
    $data['concepts'] = $this->files_model->files_get_all_concepts();
    $data['locations'] = $this->files_model->files_get_all_locations();

    //Tag Sort Order
    $tag_order = "total_count";
    $data['sort_order'] = $tag_order;

    $data['tag_list'] = $this->files_model->files_tag_list($tag_order); //Sidebar Tag list
    //File Category Sort Order
    $file_cat_order = "total_count";
    $data['cat_sort_order'] = $file_cat_order;
    $data['file_cat_list'] = $this->files_model->files_category_list($file_cat_order); //Sidebar file category list
    //Sphinx Part
    $this->load->library('sphinx');
    $this->sphinx->SetLimits(0, 100, 100);
    $this->sphinx->SetMatchMode(SPH_MATCH_EXTENDED2);
    $this->sphinx->SetFieldWeights(array('name' => 200, 'description' => 100, 'ci_master_file_category.name' => 50));

    if ($this->input->post('concept') != "") {
      $concept = $this->input->post('concept');
      $this->sphinx->setFilter('concept_id', $concept, false);
    }
    else
      $concept = array();

    if ($this->input->post('territory') != "") {
      $territory = $this->input->post('territory');
      $this->sphinx->setFilter('territory_id', $territory, false);
    }
    else
      $territory = array();

    $this->sphinx->SetSortMode(SPH_SORT_EXTENDED, "@relevance DESC");
    $result = $this->sphinx->Query($searchterm, "test2");
    //Sphinx End
    if ($result == "") {
      $result = array();
      $result['total_found'] = 0;
    }

    $this->load->helper('time_ago'); //Time Ago Function
    $total_files = $result['total_found'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/files/search';
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];

    $data['limit'] = ($this->uri->segment(3, 0)) ? $this->uri->segment(3, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(3, 0) + $config['per_page']);
    $this->pagination->initialize($config);


    $data['navlinks'] = $this->pagination->create_links();

    $data['searchterm'] = $searchterm;
    if (array_key_exists('matches', $result))
      $files_list = $this->files_model->files_get_all_files_search($config['per_page'], $this->uri->segment(3, 0), $result['matches']);
    else
      $files_list = array();

    //Data formating
    if (is_array($files_list)) {
      for ($i = 0; $i < (count($files_list)); $i++) {

        $uploaded_on_time = $files_list[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $files_list[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $files_list[$i]['path'];
        $file_size = $files_list[$i]['file_size'];
        $ext = substr(strrchr($fileName, '.'), 1);

        if ($file_size) {
          $file_info = pathinfo($fileName);
          $file_name = $file_info['filename'];
          $tmp_thumb = base_url() . "files_downloads/file_thumbs/{$file_name}.jpg";

          if (@fopen($tmp_thumb, "r")) {
            $icon_path = $tmp_thumb;
          } else {
            $icon_path = base_url() . "images/files/files-prev.jpg";
          }
        } else {
          $icon_path = base_url() . "images/files/link-prev.jpg";
        }

/*
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }
*/
        //Tags
        $db_file_tags = $this->files_model->files_get_tags_for_file($files_list[$i]['file_id']);
        $tag_arr = array();
        if (is_array($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            $tag_arr[] = '<a class="grey-shade-1" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
          }
          $file_tag = @implode(", ", $tag_arr);
        }
        //echo "<xmp>".print_r($tag_arr,1)."</xmp>";exit;

        $files_list[$i]['file_tags'] = $file_tag;
        $files_list[$i]['file_ext'] = $ext;
        $files_list[$i]['file_icon'] = $icon_path;
      }
    }
    $data['src_concept_id_arr'] = $concept;
    $data['src_country_id_arr'] = $territory;
    $data['files_list'] = $files_list;
    $this->load->view('search_files_template', $data);
  }

  //Delete File page
  function delete_file(){
	$file_id = (int)$this->input->post('file');
    $user_id = $this->session->userdata('userid');
	//check SA user
	$admin = $this->session->userdata("logged_user");
	
    $file_details = $this->files_model->files_get_files_details($file_id);

    //print_r($file_details);exit;
    if(count($file_details)){
      if(($user_id == $file_details[0]['user_id']) || ($admin['permissions']['key']=='SA') || $admin['permissions']['key']=='CONCM'){
		if($file_details[0]['file_size']){
          @unlink($file_details[0]['path']);
        }
		if($this->files_model->delete_file($file_id,$user_id)){
          $output = array('status' => 1,'msg' => '');
        }else{
          $output = array('status' => 2,'msg' => 'Refresh the page and try again');
        }	
      }else{
		$output = array('status' => 2,'msg' => 'You dont have authority on this file');
      }
	}

    echo json_encode($output);
  }

 function convert_video_file(){

   $file_id = (int)$this->input->post('file');
   $dir_path = $this->input->post('dir_path');
   $raw_file_name = $this->input->post('raw_file_name');
   $file_details = $this->files_model->files_get_files_details($file_id);
  
    $data = array_shift($file_details);
    $file_path = $data['path'];

   //$result = shell_exec( $dir_path."FFmpeg/bin/ffmpeg.exe -i ".$file_path." ".$dir_path.$raw_file_name.".ogg");
                    
   // $result2 = shell_exec( $dir_path."FFmpeg/bin/ffmpeg.exe -i ".$file_path." ".$dir_path.$raw_file_name.".webm");
    //$result = shell_exec("D:/wamp/www/lmgintranet/public_html/files_downloads/FFmpeg/bin/ffmpeg.exe -i "."D:/wamp/www/lmgintranet/public_html/files_downloads/Monkey_Got_Ak_47_Very_Funny_ncdsfon4.mp4 D:/wamp/www/lmgintranet/public_html/files_downloads/Monkey_Got_Ak_47_Very_Funny_ncdsfon4.ogg");
      
    $result = shell_exec("ffmpeg -i ".$file_path." -vcodec libtheora -acodec libvorbis ".$dir_path.$raw_file_name.".ogg");
    $result2 = shell_exec("ffmpeg -i ".$file_path." ".$dir_path.$raw_file_name.".webm");
  // echo $result;
  // print_r($result2);
   //echo "done";

 }
  
  function edit_file(){
    $file_id = (int)$this->input->post('file');
    $user_id = $this->session->userdata('userid');
    //check SA user
    $admin = $this->session->userdata("logged_user");
    $file_details = $this->files_model->files_get_files_details($file_id);
    $tags = $this->files_model->files_get_tags_for_file($file_id);
    $data = array_shift($file_details);
    $data['tags'] = $tags;
    
/*       if(count($file_details)){
        if(($user_id == $file_details[0]['user_id']) || ($admin['permissions']['key']=='SA') || $admin['permissions']['key']=='CONCM'){
      if($file_details[0]['file_size']){
            @unlink($file_details[0]['path']);
          }
      if($this->files_model->delete_file($file_id,$user_id)){
            $output = array('status' => 1,'msg' => '');
          }else{
            $output = array('status' => 2,'msg' => 'Refresh the page and try again');
          }	
        }else{
      $output = array('status' => 2,'msg' => 'You dont have authority on this file');
        }
    } */

      echo json_encode($data);
  }
  function ajax_group_file(){
    $group_id = $this->input->post('group_id');
    $file_id = $this->input->post('file_id');
    if((isset($group_id) && !empty($group_id)) && (isset($file_id) && !empty($file_id)))
    {
      $data['object_id'] = $file_id;
      $data['object_type'] = 'File';
      $data['parent_id'] = $group_id;
      $data['parent_type'] = 'Group';
      $data['added_on'] =  date('Y-m-d g:i:s',time());
      $data['status'] = 'active';
      $result = array();
      $jsondata = array();
      if($this->ObjectMember->followObject($data))
      {
        $result['status'] = true;
      }
      else
      {
        $result['status'] = false;
      }
    }
    else
    {
      $result['status'] = false;
    }
    #echo "<pre>".print_r($result,"\n")."</pre>";
    $jsondata['json'] = json_encode($result);
  	$this->load->view("print_json", $jsondata);
  }
}