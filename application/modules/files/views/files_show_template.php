<?php
//Active Navigation Page
  $active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = 'class="active"';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/files/files.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.core.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.widget.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.position.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.autocomplete.min.js"></script>
  
  <script type="text/javascript" src="<?=base_url();?>js/custom_js/files/dropbox_files.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper" class="files-page">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="#">Files</a></li>
					<li>All Files</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>All Files</h2>
					<ul class="item-options">
						<li><a href="<?=base_url();?>/index.php/files/share_files/" class="add-item">Upload Files</a></li>
						<li><a href="<?=base_url();?>/index.php/files/files_dropbox/" class="share-privately">Share Privately</a></li>
					</ul>
				</div>
				<!-- advanced search form -->
				<?php $this->load->view("files_advance_search");?>
				<div class="imp-header">
				  <div class="heading-box">
            <!-- filters list -->
            <div class="filters">
              <strong>Show:</strong>
              <ul>
                <li <?=$sort_latest_file?>><a href="<?=site_url("files/show_files")?>"><span><em>Latest</em></span></a></li>
                <li <?=$sort_popular_file?>><a href="<?=site_url("files/show_files/popular")?>"><span><em>Popular</em></span></a></li>
              </ul>
            </div>
          </div>
				  Important Files &amp; Links
				</div>
				<div class="imp-files-cat">
          <div class="files-category-listing-holder">
          <?php foreach($imp_files_cat as $cat_name => $files):?>
            <div class="files-category-box">
              <h2><img src="<?=base_url();?>images/files/cat-icon/<?=$files['files_cat_icon']?>" alt="<?=$cat_name?>"/><a class="file-links" href="<?=base_url()?>index.php/files/all_category/<?=$files['files_cat_id']?>"><?=$cat_name?></a> (<?=$files['files_count']?>)</h2>
              <ul>
              <?php for($i=0;$i<count($files['latest_files']['link']);$i++):?>
                <li><a id="<?=$files['latest_files']['link'][$i]?>" <?=(!$files['latest_files']['file_size'][$i])? 'target="_blank"':'' ?> class="show-tip" href="<?=site_url("files/files_download/".$files['latest_files']['unique_id'][$i])?>/"><?=$files['latest_files']['title'][$i]?></a><div id="show_tip_<?=$files['latest_files']['link'][$i]?>" class="tool-tip <?=$files['latest_files']['link_class'][$i]?>"><img src="<?=base_url()?>images/<?=$files['latest_files']['link_pointer_img'][$i]?>"><p><?=$files['latest_files']['file_size'][$i]?></p><p>Type: <?=$files['latest_files']['ext'][$i]?></p><p>Category: <?=$files['latest_files']['cat_name'][$i]?></p><p><?=$files['latest_files']['file'][$i]?></p></div></li>
              <?php endfor;?>
              </ul>
            </div>
          <?php endforeach;?>
          </div>
        </div>
          
          
				<div class="files-sub-header">
					<div class="heading-box">
						<h2>Files Categories</h2>
					</div>
				</div>
				<div class="files-category-listing-holder">

					<?php foreach($latest_files_cat as $cat_name => $files):?>
					<div class="files-category-box">
						<h2><img src="<?=base_url();?>images/files/cat-icon/<?=$files['files_cat_icon']?>" alt="<?=$cat_name?>"/><a class="file-links" href="<?=base_url()?>index.php/files/all_category/<?=$files['files_cat_id']?>"><?=$cat_name?></a> (<?=$files['files_count']?>)</h2>
						<ul>
							<?php for($i=0;$i<count($files['latest_files']['link']);$i++):?>
							<li><a id="<?=$files['latest_files']['link'][$i]?>" <?=(!$files['latest_files']['file_size'][$i])? 'target="_blank"':'' ?> class="show-tip" href="<?=site_url("files/files_download/".$files['latest_files']['unique_id'][$i])?>"><?=$files['latest_files']['title'][$i]?></a><div id="show_tip_<?=$files['latest_files']['link'][$i]?>" class="tool-tip <?=$files['latest_files']['link_class'][$i]?>"><img src="<?=base_url()?>images/<?=$files['latest_files']['link_pointer_img'][$i]?>"><p><?=$files['latest_files']['file_size'][$i]?></p><p>Type: <?=$files['latest_files']['ext'][$i]?></p><p>Category: <?=$files['latest_files']['cat_name'][$i]?></p><p><?=$files['latest_files']['file'][$i]?></p><p class="report"><a class="report_files" rel="pop_win" href="<?=site_url("profile/report_file/".$files['latest_files']['link'][$i])?>"> Report this file</a></p></div></li>
							<?php endfor;?>
						</ul>
					</div>
					<?php endforeach;?>
				</div>
				<div class="view-all-files"><a class="file-links" href="<?=base_url()?>index.php/files/all_files/">View all <?=$total_files?> Files</a></div>
				<div class="files-listing">
					<div class="files-added">
						<h2>Latest Files Added </h2>
						<table class="files-list-table" summary="Latest added FIles" border="0" width="98%" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th width="250">File Name</th>
									<th>File info</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($latest_addded_files as $added_files):?>
								<tr>
									<td>
										<img src="<?=$added_files['file_icon']?>" alt="files"/>
										<div class="list-file-details">
											<span><a <?=(!$added_files['file_size'])? 'target="_blank"':"" ?> href="<?=site_url("files/files_download/".$added_files['unique_id'])?>"><?=$added_files['title']?></a></span> 
											<div class="file-upload-details">In <a href="<?=base_url()?>index.php/files/all_category/<?=$added_files['cat_id']?>"><?=$added_files['category']?></a> By <a href="<?=site_url("profile/view_profile/".$added_files['user_id'])?>"><?=$added_files['first_name']." ".$added_files['last_name']?></a></div> 
										</div>
									</td>
									<td>
										<strong><?=($added_files['file_size'])? $added_files['file_ext']:"URL" ?></strong><?php if($added_files['file_size']) :?>, <span class="files-size"><?=$added_files['file_size']?></span> KB<?php endif;?>
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
					<div class="files-downloaded">
						<h2>Most Downloaded Files </h2>
						<table class="files-list-table" summary="Latest added FIles" border="0" width="98%" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th width="250">File Name</th>
									<th>Downloads</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($most_downloaded_files as $downloaded_file):?>
								<tr>
									<td>
										<img src="<?=$downloaded_file['file_icon']?>" alt="files"/>
										<div class="list-file-details">
											<span><a <?=(!$downloaded_file['file_size'])? 'target="_blank"':"" ?> href="<?=site_url("files/files_download/".$downloaded_file['unique_id'])?>"><?=$downloaded_file['title']?></a></span> 
											<div class="file-upload-details">In <a href="<?=base_url()?>index.php/files/all_category/<?=$downloaded_file['cat_id']?>"><?=$downloaded_file['category']?></a> By <a href="<?=site_url("profile/view_profile/".$downloaded_file['user_id'])?>"><?=$downloaded_file['first_name']." ".$downloaded_file['last_name']?></a></div> 
										</div>
									</td>
									<td class="align-right">
										<strong><?=$downloaded_file['download_count']?></strong>
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
					<div id="files-popular-tag">
						<?php $this->load->view("files_sidebar_popular_tags")?>
					</div>
					<h2 class="files-share-privately">Share a file privately <a id="show-dropbox-help" href="#"><img src="<?=base_url();?>images/ico-28.jpg" alt="share file"/></a></h2>
					
					<div id="dropbox-info">
            <h3 class="dropbox">What is Dropbox?</h3>
            <p>Dropbox is a file sharing tool for you to send big or small files to your peers.</p>
            <p>Just select a file from your computer and send. An email will be sent out with a unique web address for the receiver to download from.</p>
            <p>The link for a sent file remains valid for a week after which it is expired.</p>
					</div>
					
					
					
					<div class="files-share-privately-form">
						  <form action="<?=base_url();?>index.php/files/submit_file_dropbox"  id="frm_share_privately" enctype="multipart/form-data" method="post">
							<div class="frm-holder">
								<label for="txt_send_to">Send To</label><br />
								<input type="text" name="txt_sendto" id="txt_sendto" value="Start typing name.."/>
								<!--a href="#">Choose from Contact List</a-->
							</div>
							<div class="frm-holder">
								<label for="file_name">Select File <span class="sub-info">Max file size 100MB</span></label><br />
								<input type="file" name="file_share" id="file_share" size="12" />
							</div>
							<div class="frm-holder">
								<label for="txt_subject"><a id="show-subject" class="toggle-hide-label" href="#">Subject</a> <span class="sub-info">(optional)</span></label><br />
								<span id="subject-toggle"><input class="txt_title" type="text" name="txt_subject" id="txt_subject" value=""/></span><br />
							</div>
							<div class="frm-holder">
								<label for="txtar_message"> <a id="show-msg" class="toggle-hide-label" href="#">Message</a> <span class="sub-info">(optional)</span></label><br />
								<span id="message-toggle"><textarea name="txtar_message" id="txtar_message" rows="5" cols="10"></textarea></span>
							</div>
							<div class="frm-submit-button-holder">
								<input type="image" src="<?=base_url();?>images/btn-send-file.jpg" class="frm-submit-button"/> <a href="<?=site_url("files/show_files")?>">Reset</a>
							</div>
							<div class="frm-bottom-link"><a class="file-links" href="<?=site_url("files/files_dropbox#my_files")?>">Your sent &amp; received files</a></div>
							<input type="hidden" id="hd_submit_form"  name="hd_submit_form" value="1" />
						</form>	
					</div>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=base_url();?>index.php/files/all_files/" tabindex="15"><span>All Files</span></a></li>
						<li><a href="<?=base_url();?>index.php/files/files_dropbox/" tabindex="17"><span>Dropbox</span></a></li>
						<li><a href="<?=base_url();?>index.php/files/your_files/" tabindex="16"><span>Your Files</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>