<h2>Popular File Tags</h2>
	<?php if($sort_order == "total_count"):?>
	<span class="sort-by">Sort by: <strong>No. of files</strong>, <a  class="tag_sort_name"  href="<?=base_url()?>index.php/files/tag_sort/">File Tag</a></span>
	<?php elseif($sort_order == "tags"):?>
	<span class="sort-by">Sort by: <a class="tag_sort_no" href="<?=base_url()?>index.php/files/tag_sort/"> No. of files</a><strong>, File Tag</strong></span>
	<?php endif;?>
<!-- Tag list -->
<ul class="tag-list">
	<?php foreach($tag_list as $tags):if($tags['tags']):?>
	<li><a href="<?=base_url()?>index.php/files/all_tags/<?=$tags['id']?>/"><span><?=$tags['total_count']?></span><?=$tags['tags']?></a></li>
	<?php endif; endforeach;?>
</ul>
<?php if(count($tag_list) >16 ):?>
<div class="files-see-more"><a class="show-all" href="#">See more</a></div>
<?php else:?>
<br />
<?php endif;?>