<?php 
/*
 * Two inputs array required $concepts,$locations
 * 
 * */
  if(!isset($src_concept_id_arr))
 	$src_concept_id_arr = array();
 	
 if(!isset($src_country_id_arr))
 	$src_country_id_arr = array();
?>
<form action="<?=base_url()?>index.php/files/search" class="advanced-search" method="post">
					<fieldset>
						<div class="container">
							<a href="#" class="search-options">Search options</a>
							<div class="search-w">
								<input type="text" id="files_term" class="text" value="Search for files" title="Search for files" name="term"/>
								<input type="image" class="btn-search" src="<?=base_url()?>images/btn-search-advanced.gif" alt="Search!"/>
							</div>
						</div>
						<div class="checkboxes">
							<div class="columns">
								<div class="holder1">
									<h3>Concepts <a class="concept_check_all" href="#">all,</a> <a class="concept_check_none" href="#">none</a></h3>
									<div class="container">
										<div class="column">
											<?php
												$num_rows = 0;
											 	foreach($concepts as $db_data): 
											 		$num_rows++;
											 		$checkstatus = '';
								
													if(in_array($db_data['id'], $src_concept_id_arr)){
														$checkstatus = 'checked="true"';
													}
											?>
											<div class="row">
												<input class="checkbox_concepts" type="checkbox" name="concept[]" id="cid_<?=$db_data['id']?>" value="<?=$db_data['id']?>" <?=$checkstatus?> />
												<label for="cid_<?=$db_data['id']?>"><?=ucfirst(strtolower($db_data['name']))?></label>
											</div>
										<?php if($num_rows >6):?>
										</div>
										<div class="column">
											<?php $num_rows=0; endif?>
											<?php endforeach;?>
										</div>
									</div>
								</div>
								<div class="holder2">
									<h3>Locations <a class="location_check_all" href="#">all,</a> <a class="location_check_none" href="#">none</a></h3>
									<div class="container">
										<div class="column">
											<?php
												$num_rows = 0;
											 	foreach($locations as $db_data): 
											 		$num_rows++;
											 		$checkstatus = '';
								
													if(in_array($db_data['id'], $src_country_id_arr)){
														$checkstatus = 'checked="true"';
													}
											?>
											<div class="row">
												<input class="checkbox_locations" type="checkbox" name="territory[]" id="lid_<?=$db_data['id']?>" value="<?=$db_data['id']?>" <?=$checkstatus?> />
												<label for="lid_<?=$db_data['id']?>"><?=ucfirst(strtolower($db_data['name']))?></label>
											</div>
											<?php if($num_rows >6):?>
										</div>
										<div class="column">
											<?php $num_rows=0; endif;?>
											<?php endforeach;?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</form>