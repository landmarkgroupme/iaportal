<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title>Files and Links - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>

<?php

$this->load->helper('html');

?>
</head>
<body class="full-width">

	<?php $this->load->view('global_header.php'); ?>

	<div class="section wrapper clearfix">
		<h2>Files and Links</h2>
		 <ul class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li><span>&gt;&gt;</span></li>
			<li>Files</li>
		</ul>
	</div>

	<div class="section wrapper clearfix">

		<div class="left-contents">

			<div class="container">

				<?php
					$filters_data = array('search_placeholder' => 'Search for files by keyword');
					$this->load->view('partials/file_filters', $filters_data);
				?>
				<div class="upload-share-block for_tags">
					<div class="tags"><strong>Popular Tags: </strong>
						<?php
							$last_key = end(array_keys($filetags)); 
							foreach($filetags as $key => $filetag) { 
								?>
								<label class="lbl"><span value="<?php echo $filetag['id']; ?>"><?php echo ucwords($filetag['name']).' ('.$filetag['total'].')'; ?></span></label>
								<?php 
							} 
						?>
					</div>
					<a href="#upload_file-box" id ="upload_file_edit" class="btn-sm fancybox editProfile" >Upload File</a> &nbsp;
					<!-- <a href="" class="btn-sm btn-wht icon share">Share Privately</a> -->
				</div>
				<div class="no-result-block d-n"></div>
				<div class="files-links-block">

				</div>
				<div class="loader-more">&nbsp;</div>


			</div> <!-- container -->

		</div> <!-- left-contents -->

	</div> <!-- section -->

<?php $this->load->view('global_footer');
$attributes = array('class' => '', 'id' => 'frm_add_item');
?>

<div id="upload_file-box" class="fb-container-box d-n">
<h3>Upload File</h3>
<ul class="msg d-n"></ul>
	<!--<form action="<?//=base_url();?>files/submit_share_files"  id="frm_add_item" enctype="multipart/form-data" method="post"> -->
	<?php echo form_open_multipart('files/submit_share_files',$attributes);?>
	<input type="hidden" id="base_url" name="base_url" value="<?=base_url();?>" />

	<div class="form-fields">

	<dl class="cols-two">
		<dd><label for="cmb_cat">Category<span class="required">*</span></label><span class="error-msg" id="error-cmb_cat"><?=form_error('cmb_cat')?></span><br />
		<div class="block">
			<div class="selectC">
				<label class="custom-select">
					<select name="cmb_cat" id="cmb_cat">
					<option value="">Select a category</option>
					<?php	foreach($file_categories as $db_data):?>
					<option value="<?php echo $db_data['id']; ?>"><?php echo $db_data['name'];?></option>
					<?php endforeach?>
					</select>
				</label>
			</div>
		</div>
		</dd>
	</dl>
	<dl class="files-box-holder">
		<dd class="files-box patt-2">
			<label for="file_share">Upload a File<span class="required">*</span></label>
			<span id="error-files_upload" class="error-msg"></span>

			<div class="block" id="share-file" style="display: block;">
				<div id="upload-image">
						<input type="file" size="71" id="file_share" name="file_share"/>
            			<input type="hidden" size="71" id="file_share_id" name="file_share_id"/>
            			<p>"Note: Special characters are not allowed in file names (e.g. {sample})"</p>
					    <p>{sample} can be the words or characters you want to supply as an example.</p>
				</div>
			</div>
		</dd>
		<div id="progressbox" style="display:none;"><div id="fileprogressbar"></div ><div id="statustxt">0%</div></div>
		<!-- <dd class="patt-or">
			<label for="txt_link" id="share-file-link" style="display: inline;">-- OR --</label>
		</dd>
		<dd class="patt-3">
				<div id="share-link" style="display: block;">
					<div class="block">
						<label for="txt_link">Share a link to the files</label>
						<span class="sub-text"> eg. http://www.yourdomain.com/folder_path/file_name.doc</span><br />
						<div class="textC"><input type="text"  id="txt_link" name="txt_link" placeholder="Share a link" /></div>
					</div>
				</div>
		</dd> -->
		<dd>
			<div class="block">
				<label for="txt_title">Title<span class="required">*</span></label>
				<span id="error-txt_title" class="error-msg"><?=form_error('txt_title')?></span><br>
				<div class="textC"><input type="text" id="txt_title" name="txt_title" placeholder="Title" value="<?=$value_title?>" /></div>
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txtar_description">Description </label>
				<span id="error-txtar_description" class="error-msg"><?=form_error('txtar_description')?></span><br>
				<div class="textC"><textarea cols="55" rows="10" id="txtar_description" name="txtar_description" placeholder="Enter File Description"><?=$value_desc?></textarea></div>
			</div>
		</dd>
		<dd>
			<div class="block">
				<label for="txt_tags">Tags </label><span class="sub-text">Add descriptive words about the content, marketing, HR, public relations. </span>
				<span id="error-txt_tags" class="error-msg"><?=form_error('txt_tags')?></span><br>
				<div class="textC"><input type="text" id="txt_tags" name="txt_tags" class="txt_title ui-autocomplete-input" autocomplete="off" role="textbox"aria-autocomplete="list" aria-haspopup="true" placeholder="Start typing..." value="<?=($value_tags)? $value_tags:""?>"></div>
			</div>
		</dd>
		<dd>
			<input type="hidden" value="1" name="hd_submit_form" />
			<div class="submit-div">
				<span id="progress-status"></span>
        <p>Files in this section can be seen by the public. Send via chat, or create a private group to enable private sharing</p>
				<input type="button" id="bt_add_item" class="btn-sm edit fancybox" value="Upload File" />
				<input type="button" id="cancel" class="btn-sm" value="Cancel" />
        
			</div>
		</dd>
	</dl>
</div>

</form>
</div>

	<?php $this->load->view('partials/js_footer'); ?>

	<script type="text/javascript" src="<?php echo site_url(); ?>js/custom_js/files/share_files.js"></script>

<script type="text/javascript">
var limit = 10;
	var start = 0;
	var end = false;
	var nearToBottom = 250;
	var tag_id = 0;
$(document).ready(function() {

	var progressbox     = $('#progressbox');
	var progressbar     = $('#fileprogressbar');
	var statustxt       = $('#statustxt');
	var completed       = '0%';
	

		$( "#txtSearch" ).autocomplete({
	      source: '<?php echo site_url(); ?>files/autosearch_ajax',
		  position: { my: "left-11 top", at: "left bottom", collision: "none" },
		  appendTo: "#filter-search .search",
			select: function (a, b) {
				$(this).val(b.item.label);

				//window.location.href = '/files/files_download/' + b.item.value;
				start = 0;
				$('#ddCategories').focus();
				loadFiles({'noclear': 0, 'searched': 1});
				
				//e.preventDefault();
		    	return false;
			}
	    });
		
		$( "#txtSearchMobile" ).autocomplete({
	      source: '<?php echo site_url(); ?>files/autosearch_ajax',
		  position: { my: "left-11 top", at: "left bottom", collision: "none" },
		 appendTo: "#filter-searchMobile .search",
			select: function (a, b) {
				$(this).val(b.item.label);

				//window.location.href = '/files/files_download/' + b.item.value;
				start = 0;
				$('#ddCategoriesMobile').focus();
				loadFiles({'noclear': 0, 'searched': 1});
				
				//e.preventDefault();
		    	return false;
			}
	    });

		//console.log('here');
		$('#bt_add_item').click(function(e) {
			e.preventDefault();
		 
		$('.fb-container-box').animate({scrollTop: -1}, '500');
		$.fancybox.resize;
		inactive_form();

		var base_url = $("#base_url").val();
		var cat = $.trim($("#cmb_cat").val());
		var file_share = $.trim($("#file_share").val());
		var file_share_id = $.trim($("#file_share_id").val());
		var txt_link = $.trim($("#txt_link").val());
		var title = $.trim($("#txt_title").val());
		//var desc = $.trim($("#txtar_description").val());
		//var txtfilestag = $.trim($("#txt_tags").val());

		if(!cat){

		$("#error-cmb_cat").html("Required");
		$("#cmb_cat").focus();
		$('.msg').removeClass('success');
		
		active_form();
		return false;
		}
		else
		{
		$("#error-cmb_cat").html("");
		$('.msg').removeClass('success');
		}


		if(file_share || file_share_id){
      $("#error-files_upload").html("");
      $('.msg').removeClass('success');
		}
		else if((txt_link == "") || (txt_link.length < 2) &&(file_share == "")){

			$("#error-files_upload").html("Required");
			$("#error-files_upload").focus();
			$('.msg').removeClass('success');
			
			active_form();
			return false;
		}
		else if(txt_link){

			if(url_validation(txt_link)){
				$("#error-files_upload").html("");
			}else{
				$("#error-files_upload").html("Not a valid link");
				$('.msg').removeClass('success');
				
				active_form();
				return false;
			}
		}
		else{
		$("#error-files_upload").html("");
		$('.msg').removeClass('success');
		}

		if(!title){
			$("#error-txt_title").html("Required");
			$("#txt_title").focus();
			$('.msg').removeClass('success');
			
			active_form();
			return false;
		}
		else{

			file_upload_submit();
		
		}
		
		return false;

	});

	$('.editProfile').fancybox({
		maxHeight: 500,
		afterClose  : function() {
		   $("#frm_add_item input[type=text], #frm_add_item textarea , #frm_add_item select ").val("");
		    $("#frm_add_item input[type=text], #frm_add_item textarea , #frm_add_item select ").blur();
		   if($.browser.msie){
							    $("input[type='file']").replaceWith($("input[type='file']").clone(true));

								} else {
									$("input[type='file']").val('');
								}
           $('#upload_file-box .msg').removeClass('success');
           $('#upload_file-box .msg').removeClass('error');
           $('#upload_file-box .msg').html('');
		   $('#upload_file-box .msg').fadeIn();
		   $('.error-msg').html('');
		   progressbox.hide();
		   }
	});
  
  
	$('.loader-more').show();
	loadFiles({'noclear': 0});

	$('#ddfiles, #ddCategories, #ddConcepts').change(function(){
		start = 0;
		tag_id = 0;
		$('.tags .lbl').removeClass('active');
		$('#txtSearch').val('');
		loadFiles({'noclear': 0});

	});
	$('#ddfilesMobile, #ddCategoriesMobile, #ddConceptsMobile').change(function(){
		start = 0;
		tag_id = 0;
		$('.tags .lbl').removeClass('active');
		$('#txtSearchMobile').val('');
		loadFiles({'noclear': 0});
		$(this).blur();

	});

	$('.lbl span').click(function(){ 

		start = 0;
		
		$('#txtSearch').val('');
		$(this).blur();
		$('#ddCategories').val('0');

		tag_id = $(this).attr('value');
		loadFiles({'noclear': 0});
		//console.log($(this).attr('value'))
	});


	$('#clear').on( "click", function() {
		start = 0;
		tag_id = 0;
		$('.tags .lbl').removeClass('active');
		$('#ddCategories').val('0');
		$('#ddConcepts').val('0');

		loadFiles({'noclear': 0});

	});
	$('#clearMobile').on( "click", function() {
		start = 0;
		tag_id = 0;
		$('.tags .lbl').removeClass('active');
		$('#ddCategoriesMobile').val('0');
		$('#ddConceptsMobile').val('0');

		loadFiles({'noclear': 0});

	});

	$('#cancel').on( "click", function() {
		$('.msg').hide();
		$.fancybox.close();

	});
	$('#upload_file_edit').on( "click", function() {
		$('.msg').hide();
		//$.fancybox.close();

	});

	$('#txtSearch').enterKey(function(){
		start = 0;
		$('#ddCategories').focus();
		$('#ddCategories').val('0');
		$('#ddConcepts').val('0');
		loadFiles({'noclear': 0, 'searched': 1});
		
		//e.preventDefault();
    	return false;
	});
	$('#go').on( "click", function() {
		start = 0;
		loadFiles({'noclear': 0, 'searched': 1});
		//e.preventDefault();
    	return false;
	});
	$('#goMobile').on( "click", function() {
		var searched = $('#txtSearchMobile').val();
		if(searched == '')
		{
			$("#txtSearchMobile").attr("placeholder", "Please enter keywords").placeholder();
			return false;
		}
		start = 0;
		loadFiles({'noclear': 0, 'searched': 1});
		//e.preventDefault();
    	return false;
	});
	
	$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			start += 10;
		 	loadFiles({'noclear': 1});
		}
	});
	
	//delete file ajax
	$('.file_delete').live('click',function(){
		var r=confirm("Do you really want to delete the file");
		if (r==true)
		{
			var parent_div = $(this).closest('.fl-contents');
			var file_id = $(this).attr('id');
			var service_url = siteurl+"files/delete_file";
			 $.ajax({
			  url: service_url,
			  data: {'file':file_id},
			  async: false,
			  dataType: "json",
			  type: "POST",
			  success: function(msg){
				if(msg.status == 1){
				  //Upadate if Tabs are All or Status update

					$(parent_div).slideUp('fast');
					//console.log(msg);
					return;
				}
			  },
			});
		}
		
	});


function file_upload_submit()
{

//$.fancybox.showLoading();

			//$('#frm_add_item').submit();
			//var formData = new FormData($('#frm_add_item')[0]);

		var options = { 
					url     : $('#frm_add_item').attr('action'),
					type    : $("#frm_add_item").attr('method'),
					dataType: 'json',
					cache: false,
					beforeSubmit: function() { 
							inactive_form(); 
							progressbox.show(); //show progressbar
							progressbar.width(completed); //initial value 0% of progressbar
							statustxt.html(completed); //set status text
							statustxt.css('color','#000'); //initial color of status text
		
					},
					uploadProgress: OnProgress,
					success:function( data ) {

					if(data.done === 'yes' && data.samename === 'no')
						{
							active_form();
							$("input[type=text], textarea , select ").val("");
							if($.browser.msie){
								$("input[type='file']").replaceWith($("input[type='file']").clone(true));
							} else {
								$("input[type='file']").val('');
							}
							if (data.new_file === 'yes') {
								$('.msg').addClass('success');
								$('.msg').append('<li/>').html('Your file has been uploaded successfully!');
								$('.msg').fadeIn();
							}
							setTimeout(function(){$.fancybox.close();}, 500);
							//$.fancybox.close();
							loadFiles({'noclear': 0});

							//alert(data.file_id);

							if(data.video === 'yes') {
									 var service_url = siteurl+"files/convert_video_file";
								     $.ajax({
								        url: service_url,
								        data: {'file':data.file_id , 'dir_path' : data.dir_path , 'raw_file_name' : data.raw_file_name},
								       
								        dataType: "json",
								        type: "POST",
								        success: function(msg){
								       
								        }
								     });
							}


						}
						else if(data.samename === 'yes')
						{
							active_form();
							$('.msg').removeClass('success');
							$('.msg').addClass('error');
							$('.msg').append('<li/>').html('A file with this name already exists. Please change name to continue.');
							$('.msg').fadeIn();
						}
						else
						{
							active_form();
							$('.msg').removeClass('success');
							$('.msg').addClass('error');
							$('.msg').append('<li/>').html('Please retry, enter data');
							$('.msg').fadeIn();
						}


					},
			};
			$('#frm_add_item').ajaxSubmit(options);
                return false;
}
function OnProgress(event, position, total, percentComplete)
{
	//Progress bar
	progressbar.width(percentComplete + '%') //update progressbar percent complete
	statustxt.html(percentComplete + '%'); //update status text
	if(percentComplete>50)
		{
			statustxt.css('color','#fff'); //change status text to white after 50%
		}
}
function active_form()
{
$.fancybox.hideLoading();
$('#bt_add_item').removeClass('inactive');
$('#bt_add_item').removeAttr('disabled');
$('#cancel').removeClass('inactive');
$('#cancel').removeAttr('disabled');
}

function inactive_form()
{
$.fancybox.showLoading();
$('#bt_add_item').addClass('inactive');
$('#bt_add_item').attr('disabled','disabled');
$('#cancel').addClass('inactive');
$('#cancel').attr('disabled','disabled');
}
function url_validation(url) {
  var v = new RegExp();
  v.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");
  if (!v.test(url)) {
    return false;
  }else{
    return true;
  }
}
function remove_tags(html)
  {
       return html.replace(/<(?:.|\n)*?>/gm, '');
  }
function loadFiles(prop)
{
	if($('.media-show').is(':visible'))
	{
		//alert('mobile');
		//alert('desktop');
		var searched = ($('#txtSearchMobile').val() != '' && $('#txtSearchMobile').val() != 'Search for files by keyword' && $('#txtSearchMobile').val() != 'Please enter keywords' ) ? true : false;
		if(searched)
		{
			var search = remove_tags($('#txtSearchMobile').val());
			var data = {'limit' : limit, 'start' : start, 'search_term': search};
		}  else {
			var files = $('#ddfilesMobile').val();
			var category = $('#ddCategoriesMobile').val();
			var concept = $('#ddConceptsMobile').val();
			var data = {'limit' : limit, 'start' : start, 'files': files, 'category' : category, 'concept': concept, 'tag': tag_id};
		}
	}
	else
	{
		//alert('desktop');
		var searched = ($('#txtSearch').val() != '' && $('#txtSearch').val() != 'Search for files by keyword' && $('#txtSearch').val() != 'Please enter keywords' )? true : false;
		if(searched)
		{
			var search = remove_tags($('#txtSearch').val());
			var data = {'limit' : limit, 'start' : start, 'search_term': search};
		}  else {
			var files = $('#ddfiles').val();
			var category = $('#ddCategories').val();
			var concept = $('#ddConcepts').val();
			var data = {'limit' : limit, 'start' : start, 'files': files, 'category' : category, 'concept': concept, 'tag': tag_id};
		}
	}


	

	if(!prop.noclear)
		$('div.files-links-block').html('');
		$('div.no-result-block').html('');
		$('div.no-result-block').hide();

	var service_url = siteurl + "files/files_ajax";



	
	$.ajax({
		beforeSend: function() {
			$('.loader-more').show();
		},
		complete: function() {
			$('.loader-more').hide();
		},
		url: service_url,
		data: data,
		async: false,
		cache: false,
		dataType: "json",
		type: "GET",
		success: function(data){

			var counter = 2;
			if(data.not_found != true)
			{

				$.each(data, function() {
					//video extensions
					var extArr =  [ "video/3gp", "video/avi", "video/flv", "video/m4v", "video/mov", "video/mp4", "video/mpg", "video/vob", "video/wmv", "video/mpeg", "video/mpe" ];	
					if(this[0]){

						var html='<dl class="files-links clearfix">';
						html +='<dt>'+this[0].date+'</dt><dd>';
			        	$.each(this, function() {
							len = '';
							if(this.description.length > 1)
							{
								len='...';
							}
							html +='<div class="fl-contents clearfix">';
							//html +='<blockquote class="icon '+this.icon+'">'+this.icon+'</blockquote>';
							if(this.view_path!=''){
                if(this.view_path.indexOf('.jpg')!= -1 || this.view_path.indexOf('.png')!= -1 || this.view_path.indexOf('.jpeg')!= -1 || this.view_path.indexOf('.gif')!= -1 || this.view_path.indexOf('.bmp')!= -1)
				{
							html +='<span class="ph"><a class="fancybox image_view" href="'+this.view_path+'"><img src="'+this.file_image_thumb+'" width="97" alt="'+this.title+'"/></a></span>';
							html +='<div class="f1-c2"><h2><a class="fancybox image_view" href="'+this.view_path+'">'+this.title+'</a><!-- <label class="lbl"><span>Label</span></label> --></h2>';
							}
							else if(jQuery.inArray( this.file_mime_type, extArr ) != -1)
							{
								html +='<span class="ph"><a class="fancybox video_view" data-fancybox-type="iframe" href="'+siteurl+'lib/video.php?url='+this.view_path+'"><img src="'+this.file_image_thumb+'" width="97" alt="'+this.title+'"/></a></span>';
								html +='<div class="f1-c2"><h2><a class="fancybox video_view" data-fancybox-type="iframe" href="'+siteurl+'lib/video.php?url='+this.view_path+'">'+this.title+'</a><!-- <label class="lbl"><span>Label</span></label> --></h2>';
							}
							else
							{	
								html +='<span class="ph"><a class="fancybox doc_view" target="_blank" href="//docs.google.com/viewer?url='+this.view_path+'&embedded=true"><img src="'+this.file_image_thumb+'" width="97" alt="'+this.title+'"/></a></span>';
								html +='<div class="f1-c2"><h2><a class="fancybox doc_view" target="_blank" href="//docs.google.com/viewer?url='+this.view_path+'&embedded=true">'+this.title+'</a><!-- <label class="lbl"><span>Label</span></label> --></h2>';
							}
							}else
							{
								html +='<span class="ph"><a href="#"><img src="'+this.file_image_thumb+'" width="97" alt="'+this.title+'"/></a></span>';
								html +='<div class="f1-c2"><h2><a href="#">'+this.title+'</a><!-- <label class="lbl"><span>Label</span></label> --></h2>';
							}
							
							if(this.user_status == 'active')
							{
								html +='<p class="dgrey">'+this.description.substring(0,150)+len+' by <strong><a href="'+this.reply_username +'">'+this.display_name+'</a></strong></p>';	
							} else {
								html +='<p class="dgrey">'+this.description.substring(0,150)+len+' by <strong>'+this.display_name.replace(/\\/g, '')+'</strong></p>';
							}
							
							html +='<div class="rp-social">';
							html +='<a class="pDelete" '+this.target+' href="'+this.download_path+'">Download</a>';
										
              if(this.view_path!=''){
                if(this.view_path.indexOf('.jpg')!= -1){
				html +='&#8226;<a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a>'; 
                }
                else if(this.view_path.indexOf('.png')!= -1){
                html +='&#8226;<a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a>';  				
                }
                else if(this.view_path.indexOf('.jpeg')!= -1){
                html +='&#8226;<a class="pDelete fancybox" href="'+this.view_path+'">View</a>';  
                }
                else if(this.view_path.indexOf('.gif')!= -1){
                html +='&#8226;<a class="pDelete fancybox" href="'+this.view_path+'">View</a>';  
                }
                else if(this.view_path.indexOf('.png')!= -1){
                html +='&#8226;<a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a>';  
                }
                else if(this.view_path.indexOf('.bmp')!= -1){
                html +='&#8226;<a class="pDelete fancybox image_view" href="'+this.view_path+'">View</a>';  
                }
				else if(jQuery.inArray( this.file_mime_type, extArr ) != -1)
                {
					html +='&#8226;<a class="pDelete fancybox video_view" data-fancybox-type="iframe" href="'+siteurl+'lib/video.php?url='+this.view_path+'">Watch</a>';
                }
                else{
					html +='&#8226;<a class="pDelete fancybox doc_view" target="_blank" href="//docs.google.com/viewer?url='+this.view_path+'&embedded=true">View</a>';
                }
              }
					if(myprofile.id != this.user_id && this.concept_id == 0){
					html +='&#8226;<a class="pDelete fancybox report_file" id="'+this.file_id+'" href="#pop-win-file">Report</a>';
					}
					//delete file link
					if(myprofile.id == this.user_id || myprofile.role_id == 1){
						html +='&#8226;<a class="pDelete file_delete" id="'+this.file_id+'" href="javascript:void(0)">Delete</a>';
					}
          if(myprofile.id == this.user_id){
            
						html +='&#8226;<a class="pEdit fancybox file_edit" id="'+this.file_id+'" href="#upload_file-box">Edit</a>';
					}
					html +='</div></div></div>';
					});
					html +='</dd></dl>';
					$('div.files-links-block').append(html);
						counter++;
					}
				});
			}
			else
			{
				if($('div.files-links-block').html() == '')
				{
					$('div.no-result-block').show();
					var noResult = 'No result found for <b>"'+data.search_term+'"</b>.';
					$('div.no-result-block').append(noResult);
				}
			}

			if(data.no_result == true && start == 0 )
			{
				$('.loader-more').hide();
				$('div.no-result-block').show();
				var noResult = 'No results found.';
				$('div.no-result-block').html(noResult);
			}
		}
	});
  $('.image_view').fancybox({
		maxHeight: 500
  });
  $('.doc_view').fancybox({
		'width'	: '75%',
		'height' : '95%',
    'autoScale' : false,
    'transitionIn' : 'none',
		'transitionOut'	: 'none',
		'type' : 'iframe'
	});
	$('.video_view').fancybox({
		width	: '435',
	});
  $('.report_file').fancybox({
  beforeLoad : function() {
     //console.log(this.element[0].id);
     $('#txtfileid').val(this.element[0].id);
     $('#txtarcomments-file').val('');
     $(".error-msg").html('');
    }
  });
  
  $('.file_edit').fancybox({
    beforeLoad : function() {
      var fileid = this.element[0].id;
      
       var service_url = siteurl+"files/edit_file";
       $.ajax({
        url: service_url,
        data: {'file':fileid},
        async: false,
        dataType: "json",
        type: "POST",
        success: function(msg){
        //console.log(msg);
          $('#txt_title').val(msg.name);
          $('#txtar_description').val(msg.description);
          $('#cmb_cat').val(msg.file_category_id);
          $('#file_share_id').val(fileid);
          $('.patt-2').hide();
          var a = msg.tags;         
          var i = 0;
          var tags_array = new Array();

          $.each(a, function (i, entry) {
            if (entry.name != '') {
              tags_array[i] = entry.name;
              i++;
            }
          });
          tags_array.sort();
          $('#txt_tags').val(tags_array);
        }
       });
    }
  });
}
});



</script>

</body>
</html>
