<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = 'class="active"';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/editableText.css" />

  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.core.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.widget.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.position.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.autocomplete.min.js"></script>

	<script type="text/javascript" src="<?=base_url();?>js/custom_js/files/files.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/files/files_details.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/jquery.editableText.js"></script>
</head>
<body>
	
	<!-- wrapper -->
	<div id="wrapper" class="files-page">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url("files")?>">Files</a></li>
					<li><?=$file_title?></li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?=$file_title?></h2>
				</div>
				
				<!-- recently added products -->
				
				<!-- latest items -->
				<div class="container">
					<div class="details-attr">
						<div class="details-attr-title">File type: </div>
						<div class="details-attr-res"><?=$file_extension?></div>
						<div class="details-attr-title">Size: </div>
						<div class="details-attr-res"><?=($file_size)?$file_size." KB":"Not Available"?></div>
						<div class="details-attr-title">Downloads: </div>
						<div class="details-attr-res"><?=$file_download_count?></div>
						<div class="details-attr-title">Uploaded: </div>
						<div class="details-attr-res"><?=$file_uploaded_on?></div>
					</div>
					
					<div  class="details-desc-box">
						<div class="details-desc">
							<div class="details-desc-title">Description</div>
							<div <?=($file_editable)? 'class="edit-file-desc"':""?> id="<?=$file_id?>"><?=$file_description?></div>
							<br />
							<br />
							<div class="details-tags">
								<span class="tag-title">Tags:</span> 
                
								<span id="file-tags"><?=$file_tags?></span>
								<?php if($file_editable):?>
								<div class="editableToolbar" id="edit-file-tags">
                  <a class="edit" id="edit-tags" href="#"></a>
                </div>
                <?php endif;?>
							</div>
						</div>
						
						<div class="details-img">
							<div class="details-img">
								<img src="<?=$file_ext_icon?>" width="252" height="189" alt="file icon" />
							</div>
							<div class="download-box">
                <?=$download_path?>
              </div>
              <div class="file-report">
                If the contents of this file are inappropriate or offensive, please <a rel="pop_win" class="report_files" href="<?=site_url("profile/report_file/".$file_id)?>">report this file</a>.
              </div>
						</div>
					</div>
					<?php if($important_cat == 1):?>
					<div>
						<div class="details-desc-title">Uploaded By</div>
						<div class="contact-vcard">
							<div class="contact-vcard-img">
								<img height="84" width="84" src="<?=base_url()?>images/user-images/105x101/<?=$profile_pic?>" alt="user vcard img" />
							</div>
							<div class="contact-vcard-details">
								<div class="contact-vcard-name"><?=$full_name?></div>
								<div class="contact-vcard-desg"><strong><?=$user_designation?></strong>, <?=$user_concept?></div>
								<div class="contact-vcard-info">
									<div class="contact-vcard-attr">Extension: </div>
									<div class="contact-vcard-res"><?=$user_ph_ext?></div>
								</div>
								<div class="contact-vcard-info">
									<div class="contact-vcard-attr">Landline: </div>
									<div class="contact-vcard-res"><?=$user_phone?></div>
								</div>
								<div class="contact-vcard-info">
									<div class="contact-vcard-attr">Mobile: </div>
									<div class="contact-vcard-res"><?=$user_mobile?></div>
								</div>
								<div class="contact-vcard-info">
									<div class="contact-vcard-attr">Email: </div>
									<div class="contact-vcard-res"><a href="mailto:<?=$user_email?>"><?=$user_email?></a></div>
								</div>
							</div>
						</div>
					</div>
					<?php endif;?>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<div id="files-popular-tag">
						<?php $this->load->view("files_sidebar_popular_tags")?>
					</div>
					<div class="category-list-holder">
						<?php $this->load->view("files_sidebar_category")?>
				</div>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=base_url();?>index.php/files/all_files/" tabindex="15"><span>All Files</span></a></li>
						<li><a href="<?=base_url();?>index.php/files/files_dropbox/" tabindex="17"><span>Dropbox</span></a></li>
						<li><a href="<?=base_url();?>index.php/files/your_files/" tabindex="16"><span>Your Files</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>