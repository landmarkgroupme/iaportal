<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = 'class="active"';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.core.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.widget.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.position.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>js/jquery.ui.autocomplete.min.js"></script>
	
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/files/dropbox_files.js"></script>
</head>
<body>
	
	<!-- wrapper -->
	<div id="wrapper" class="files-share">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="#">Files</a></li>
					<li>Dropbox</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>Dropbox</h2>
					<h4>Share a big file</h4>
				</div>
				<?php if($this->session->flashdata('file_upload_ok')):?>
        <div class="msg-ok">
          <?=$this->session->flashdata('file_upload_ok');?>
        </div>
        <?php endif;?>
				<?php if($this->session->flashdata('file_upload_error')):?>
        <div class="msg-error">
          <?=$this->session->flashdata('file_upload_error');?>
        </div>
        <?php endif;?>
				
				<!-- Dropbox  -->
				<div class="add-item">
                                          <form action="<?=site_url("files/submit_file_dropbox")?>"  onsubmit="return form_validation_init();"  id="frm_add_item" enctype="multipart/form-data" method="post">
						<div class="form-fields">
							<div class="block">
								<label for="txt_sendto">Send To</label><span class="error-msg" id="error-txt_sendto"><?=form_error('txt_sendto')?></span><br />
								<!--div class="dropbox-choose"><a href="#">Choose from contact list</a></div-->
								<!--input class="txt_title" type="text" name="txt_sendto" id="txt_sendto" value="Start typing name.."/-->
								<textarea cols="55" rows="2" class="txt_title" type="text" name="txt_sendto" id="txt_sendto">Start typing name..</textarea>
							</div>
							<div class="block">
								<label for="file_share">Select a File </label><span class="sub-text">Max filesize 100MB</span><span class="error-msg" id="error-file_share"></span><br />
								<div id="upload-image">
									<input type="file" name="file_share" id="file_share" size="75" />
								</div>
							</div>
							
							<div class="block">
								<label for="txt_subject"><a id="show-subject" class="toggle-hide-label" href="#">Subject</a></label> <span class="sub-text">(Optional)</span><span class="error-msg" id="error-txt_subject"></span><br />
								<span id="subject-toggle"><input class="txt_title" type="text" name="txt_subject" id="txt_subject" value=""/></span>
							</div>
							<div class="block">
								<label for="txtar_message"><a id="show-msg" class="toggle-hide-label" href="#">Message</a></label> <span class="sub-text">(Optional)</span><br />
								<span id="message-toggle"><textarea name="txtar_message" id="txtar_message" rows="2" ></textarea></span>
							</div>
						</div>	
						<div class="submit-div"><input type="image" src="<?=base_url();?>images/btn-send-file.jpg" name="bt_send_file" id="bt_send_file" /> <a href="<?=site_url("files/")?>">Cancel</a></div>
						<input type="hidden" id="hd_submit_form"  name="hd_submit_form" value="1" />
					</form>
				</div>
				<div class="dropbox-active">
					<h4>Your active Dropbox files</h4>
					<a name="my_files"></a>
					<table width="100%" id="dropbox-table" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th class="align-left">File Info / File Link</th>
								<th width="100"  class="align-center">Expires on</th>
								<th class="align-center">Download</th>
							</tr>
						</thead>
						<tbody>
						<?php if(is_array($active_dropbox_files)): foreach($active_dropbox_files as $active_files):?>
							<tr>
								<td><strong><?=$active_files['file_name'];?></strong> (<?=$active_files['file_size'];?>)<div class="dropbox-dwd-link">Link: <a href="<?=base_url()."index.php/files/files_dropbox_download/".$active_files['unique_id']."/"?>"><?=base_url()."index.php/files/files_dropbox_download/".$active_files['unique_id']."/"?></a></div></td>
								<td class="align-center"><?=$active_files['expire_time'];?></td>
								<td class="align-center"><?=$active_files['download_count'];?></td>
							</tr>
						<?php endforeach; else:?>
							<tr>
								<td>&nbsp;</td>
								<td class="align-center">No Active Dropbox File </td>
								<td class="align-center">&nbsp;</td>
							</tr>
							<?php endif;?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<h3 class="dropbox">What is Dropbox?</h3>
				<p>Dropbox is a file sharing tool for you to send big or small files to your peers.</p>
				<p>Just select a file from your computer and send. An email will be sent out with a unique web address for the receiver to download from.</p>
				<p>The link for a sent file remains valid for a week after which it is expired.</p>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li><a href="<?=base_url();?>index.php/files/all_files/" tabindex="15"><span>All Files</span></a></li>
						<li class="active"><a href="<?=base_url();?>index.php/files/files_dropbox/" tabindex="17"><span>Dropbox</span></a></li>
						<li><a href="<?=base_url();?>index.php/files/your_files/" tabindex="16"><span>Your Files</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
	<img src="<?=base_url()?>images/indicator.gif" alt="loader" style="display:none;" />
</body>
</html>