<h2>File Categories</h2>
<?php if($cat_sort_order == "total_count"):?>
<span class="sort-by">Sort by: <strong>No. of files</strong>, <a  class="file_cat_sort_name"  href="<?=base_url()?>index.php/files/file_category_sort/">Category</a></span>
<?php elseif($cat_sort_order == "file_category"):?>
<span class="sort-by">Sort by: <a class="file_cat_sort_no" href="<?=base_url()?>index.php/files/file_category_sort/"> No. of files</a><strong>, Category</strong></span>
<?php endif;?>

<!-- category list -->
<ul class="category-list">
	<?php foreach($file_cat_list as $cat): ?>
	<li><a href="<?=base_url()?>index.php/files/all_category/<?=$cat['id']?>/"><span><?=$cat['total_count']?></span><?=$cat['file_category']?></a></li>
	<?php endforeach;?>
</ul>