<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = '';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = 'class="active"';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.core.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.widget.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.position.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/jquery.ui.autocomplete.min.js"></script>
	
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/files/share_files.js"></script>
</head>
<body>
	
	<!-- wrapper -->
	<div id="wrapper" class="files-share">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="#">Files</a></li>
					<li><a href="#">Your Files</a></li>
					<li>Share a File</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>Share a File</h2>
				</div>
				<!-- latest items -->
				<div class="add-item">
					<form action="<?=base_url();?>index.php/files/submit_share_files" onsubmit="return form_validation_init()" id="frm_add_item" enctype="multipart/form-data" method="post">
						<div class="form-fields">
							<div class="block">
								<label for="cmb_cat">Category<span class="required">*</span></label><span class="error-msg" id="error-cmb_cat"><?=form_error('cmb_cat')?></span><br />
								<select name="cmb_cat" id="cmb_cat">
									<option value="">Select a category</option>
									<?php	foreach($files_categories as $db_data):?>
									<option value="<?=$db_data['id']?>"><?=$db_data['name']?></option>
									<?php endforeach?>
								</select>
							</div>
							
							<div class="files-box-holder">
								<label for="file_share">Specify the File<span class="required">*</span></label>	<span class="error-msg" id="error-files_upload"></span>
								<div class="files-box">
									
									<div id="share-file" class="block">
										<label for="file_share">Upload a File </label><span class="sub-text">Format (.doc, .pdf) Max filesize 100MB</span><br />
										<div id="upload-image">
											<input type="file" name="file_share" id="file_share" size="71" />
										</div>
                    <br />
									</div>
 									<label id="share-file-link" for="txt_link">--OR--</label>
									<div id="share-link">
  									<div class="block">
  										<label for="txt_link">Share a link to the files</label><span class="sub-text"> Eg http://www.cplmg.com/policies/privacy.doc</span><br />
  										<input class="txt_link" type="text" name="txt_link" id="txt_link" value="<?=$value_link?>"/>
  									</div>
									</div>
									
								</div>	
							</div>
							
							
							<div class="block">
								<label for="txt_title">Title<span class="required">*</span></label> <span class="error-msg" id="error-txt_title"><?=form_error('txt_title')?></span><br />
								<input class="txt_title" type="text" name="txt_title" id="txt_title" value="<?=$value_title?>"/>
							</div>
							<div class="block">
								<label for="txtar_description">Description </label> <span class="error-msg" id="error-txtar_description"><?=form_error('txtar_description')?></span><br />
								<textarea name="txtar_description" id="txtar_description" rows="10" cols="55"><?=$value_desc?></textarea>
							</div>
							<div class="block">
								<label for="txt_tags">Tags </label><span class="sub-text"> Add descriptive words about the content, marketing, HR, public relations.</span><span class="error-msg" id="error-txt_tags"><?=form_error('txt_tags')?></span><br />
								<input class="txt_title" type="text" name="txt_tags" id="txt_tags" value="<?=($value_tags)? $value_tags:"Start typing.."?>"/>
							</div>
						</div>
						<input type="hidden" name="hd_submit_form" value="1" />	
						<div class="submit-div">
						  <?php if($this->session->flashdata('file_upload_status')):?>
                <div class="msg-error">
                  <?=$this->session->flashdata('file_upload_status');?>
                </div>
                <?php endif;?>
              <span id="progress-status"></span>
						  <input type="image" src="<?=base_url();?>images/btn-post-files.jpg" name="bt_add_item" id="bt_add_item" /> <a href="<?=site_url("files/share_files/")?>">Cancel</a>
						</div>
						
					</form>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<h2>File Upload Rules:</h2>
				<p><strong>Do not</strong> upload files that are confidential in nature or are bound by Non-disclosure agreements by external organisations. If at all you need to share such content, <a href="<?=base_url();?>/index.php/files/files_dropbox/">share it privately</a>.</p>
				<p><strong>Avoid</strong> sharing offensive or objectionable content in the public section. Remember, all the files you share are marked with your name as the owner.</p>
				<p>Your peers have faith in you, so play by the rules and keep it that way ;)</p>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li><a href="<?=base_url();?>index.php/files/all_files/" tabindex="15"><span>All Files</span></a></li>
						<li><a href="<?=base_url();?>index.php/files/files_dropbox/" tabindex="17"><span>Dropbox</span></a></li>
						<li class="active"><a href="<?=base_url();?>index.php/files/your_files/" tabindex="16"><span>Your Files</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
	<img src="<?=base_url()?>images/indicator.gif" alt="loader" style="display:none;" />
</body>
</html>