<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
$active_tab['home_active'] = '';
$active_tab['people_active'] = '';
$active_tab['market_place_active'] = '';
$active_tab['files_active'] = 'class="active"';
$active_tab['events_active'] = '';
$active_tab['about_lmg_active'] = '';
$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Landmark Intranet</title>
    <?php $this->load->view("include_files/common_files"); ?>
    <script type="text/javascript" src="<?=base_url(); ?>js/custom_js/files/files.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.file-holder').hover(
          function () {
            var close_img = $(this).find('.close-img').attr('src');
            var new_img = close_img.replace('file-del-o.png','file-del-h.png');
            $(this).find('.close-img').attr('src',new_img);
            //$(this).find('.del-files').show();
          },
          function () {
            var close_img = $(this).find('.close-img').attr('src');
            var new_img = close_img.replace('file-del-h.png','file-del-o.png');
            $(this).find('.close-img').attr('src',new_img);
            //$(this).find('.del-files').hide();
          }
        )

        $('.remove-file').click(function(){
          var e = window.confirm('Are you sure you want to delete this file?')
          if(!e){
            return false;
          }
          var fileid = $(this).attr('rel');
          var scope_var = this;
          $.ajax({
            url: '<?php echo site_url('files/delete_file')?>',
            data: {"file":fileid},
            async: false,
            dataType: "json",
            type: "POST",
            success: function(result){
              if(result.status == 1){
                $(scope_var).parents('.file-holder').hide();
              }else{
                alert(result.msg);
              }
            }
          });


          
          return false;
        })


      })
    </script>
  </head>
  <body>
    <!-- wrapper -->
    <div id="wrapper" class="files-page">
    <?php $this->load->view("includes/admin_nav"); ?>
      <!-- logo -->
      <h1 class="logo vcard"><a href="<?=base_url() ?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
      <!-- main -->
      <div id="main">
        <!-- content -->
        <div id="content">
          <!-- breadcrumbs -->
          <ul class="breadcrumbs">
            <li><a href="#">Files</a></li>
            <li>Your Files</li>
          </ul>
          <!-- heading -->
          <div class="heading-box">
            <h2>Your Files</h2>
            <ul class="item-options">
              <li><a href="<?=base_url(); ?>/index.php/files/share_files/" class="upload-file">Upload a File</a></li>
            </ul>
          </div>
          <!-- advanced search form -->
          <?php $this->load->view("files_advance_search"); ?>
          <?php if ($this->session->flashdata('file_upload_status')): ?>
            <div class="msg-ok">
  	        <?=$this->session->flashdata('file_upload_status'); ?>
          </div>
          <?php endif; ?>
          <!-- latest FIles -->
          <div class="container">
            <div class="pagination-count">Showing <strong><?=$limit ?></strong> - <strong><?=$offset ?></strong> of <strong><?=$total_count ?></strong> files</div>
            <?php $zebra_strip = 0;
            foreach ($files_list as $file_attr):if ($zebra_strip == 1) {
                $zebra_class = " zebra-strip";
                $zebra_strip = 0;
              } else {
                $zebra_class = "";
                $zebra_strip = 1;
              } ?>
            <div class='file-holder<?=$zebra_class ?>'>
              <div class='img-thumb'><img height="63" src="<?=$file_attr['file_icon'] ?>" alt="files image" /></div>
              <div class="file-info">
                <div><a <?=(!$file_attr['file_size']) ? 'target="_blank"' : "" ?> href="<?=site_url('files/files_download/' . $file_attr['unique_id']) ?>" class="list-table-title"><?=$file_attr['title'] ?></a> <span class="file-type"><?=($file_attr['file_size']) ? "(" . $file_attr['file_ext'] . ", " . $file_attr['file_size'] . "KB)" : "(URL)" ?></span> <span class="del-files"><a class="remove-file" title="Delete" rel="<?php echo $file_attr['file_id'];?>" href="#"><img class="close-img" src="<?php echo base_url();?>images/file-del-o.png" /></a></span></div>
                <div class="file-det">
                    <?=$file_attr['uploaded_on'] ?> <a class="high" href="<?=site_url("files/all_category/" . $file_attr['cat_id']) ?>"><?=$file_attr['category'] ?></a> &#183; Downloaded <span class="high"><?=$file_attr['download_count'] ?> times</span>
                </div>
                <div>Tags: <?=$file_attr['file_tags'] ?></div>
              </div>
            </div>
<?php endforeach; ?>
            <div class="pagination-footer">
              <div class="pagination-footer-count">Showing <strong><?=$limit ?></strong> - <strong><?=$offset ?></strong> of <strong><?=$total_count ?></strong> files</div>
              <div class="pagination-footer-list">
                <ul>
								<?=$navlinks ?>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <!-- sidebar -->
        <div id="sidebar">
          <div id="files-popular-tag">
<?php $this->load->view("files_sidebar_popular_tags") ?>
          </div>
          <div class="category-list-holder">
<?php $this->load->view("files_sidebar_category") ?>
          </div>
        </div>
      </div>
      <!-- header -->
      <div id="header">
        <div class="header-holder">
          <!-- navigation -->
<?php $this->load->view("includes/navigation", $active_tab); ?>
            <div class="head-bar">
              <!-- sub navigation -->
              <ul class="subnav">
                <li><a href="<?=base_url(); ?>index.php/files/all_files/" tabindex="15"><span>All Files</span></a></li>
              <li><a href="<?=base_url(); ?>index.php/files/files_dropbox/" tabindex="17"><span>Dropbox</span></a></li>
              <li class="active"><a href="<?=base_url(); ?>index.php/files/your_files/" tabindex="16"><span>Your Files</span></a></li>
            </ul>
            <!-- header search form -->
<?php $this->load->view("includes/search_form"); ?>
                </div>
              </div>
            </div>
            <!-- footer -->
<?php $this->load->view("includes/footer"); ?>
    </div>
  </body>
</html>