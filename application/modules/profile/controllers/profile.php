<?php

Class Profile extends General {
//Constructor
  function __construct() {
    parent::__construct();
    $this->load->model('Group');
    $this->load->model('GroupUser');
    $this->load->model('m_profile');
    $this->load->model('UserFollow');
    $this->load->model('ObjectMember');
	$this->load->model('Prompt');
	$this->load->model('manage/manage_model');
	$this->load->model('Album');
	$this->load->model('User');

   $this->load->model('Survey');
   $this->load->model('Outlets');
   $this->config->load('audit');
  }

//image resizing and croping


  /* Private function for Newcommer */
  function _newcommer_list() {
    $newcommer_show_limit = 3;
    $user_id = $this->session->userdata('userid');
    $db_newcommer = $this->User->getNewUsers($user_id, $newcommer_show_limit); //General Model function for New Commers
    for ($i = 0; $i < count($db_newcommer); $i++) {
      $contact_user = $db_newcommer[$i]['contact_user'];
      $profile_pic = base_url() . "images/user-images/105x101/" . $db_newcommer[$i]['profile_pic'];
      if (!$db_newcommer[$i]['profile_pic']) {
        $db_newcommer[$i]['profile_pic'] = "default.jpg";
      } else if (!@fopen($profile_pic, "r")) {
        $db_newcommer[$i]['profile_pic'] = "default.jpg";
      }
      $id = $db_newcommer[$i]['id'];
      if ($list['contact_user']) {
        $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $list['id'] . '" href="' . base_url() . 'index.php/interest_groups/" class="remove-contact">Remove from my contacts</a>';
      } else {
        $db_newcommer[$i]['add_remove_contact_link'] = '<a id="' . $id . '" href="' . base_url() . 'index.php/interest_groups/" class="add-contact">Add to My Contacts</a>';
      }
    }
    return $db_newcommer;
  }

  /* Private function for Interest Groups */

  function _interest_group_list() {
    $show_limit = 20;
    $user_id = $this->session->userdata('userid');
    $db_interest_groups = $this->GroupUser->getUserGroups($user_id, $show_limit); //General Model function for Interest Groups
    /*print_r($db_interest_groups);
    return;*/
    if (!count($db_interest_groups)) {
      $db_interest_groups = array();
    }
    return $db_interest_groups;
  }

  /* Private function for My Reminders */

  function _my_reminders() {
    $user_id = $this->session->userdata('userid');
    $time = time();
    $this->load->model('market_place/market_place_model');
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($user_id);
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($user_id);
    $db_event_reminders = $this->general_model->gn_my_event_reminders($user_id, $time);
    $my_reminders['count_item_sale'] = count($db_item_sale);
    $my_reminders['count_item_requested'] = count($db_item_requested);
    $my_reminders['count_event_reminders'] = count($db_event_reminders);
    return $my_reminders;
  }
//Default page for profile, redirect to the the logged in users profile
  function index() {
      redirect("profile/myprofile");

  }

//profile Progress Bar
  function profile_progress_bar() {
    $progress = 5;

    $user_id = $this->session->userdata('userid');
    //Profile Details
    $user_data = $this->m_profile->get_profile($user_id, $user_id);

    //Intrest Group
    $db_interest_groups = $this->m_profile->get_users_interest_groups($user_id);

    $user_skills = $user_data['skills'];
    $user_about = $user_data['about'];
    $profile_pic = $user_data['profile_pic'];

    if (trim($user_skills)) {
      $progress += 25;
    }
    if (trim($user_about)) {
      $progress += 25;
    }

    if ($profile_pic != "default.jpg" && $profile_pic != "default-f.jpg") {
      $progress += 20;
    }

    if (count($db_interest_groups)) {
      $progress += 25;
    }

    $result = '{"progress": "' . $progress . '"}';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }
  //Profile View page this page has to be called with the user id else it will redirect to myprofile
  function view_profile($id) {
    $my_user = $this->session->userdata('logged_user');

    $user_email = $my_user['useremail'];
    $user_id = $my_user['userid'];

    if (!$id) {
      redirect('profile');
    }

    $profile_details = $this->User->getUserProfile($id, $user_id);


    if (!count($profile_details)) {
      redirect('profile');
    }



    $this->load->model('UserFollow');
    $profile_details['following'] = $this->UserFollow->countUserFollowings($id);
    $profile_details['followers'] = $this->UserFollow->countUserFollowers($id);
    $data = array(
      'profile' => $profile_details,
      'myprofile' => $this->User->getByEmail($user_email)
    );


    $data['birthdays'] = $this->User->getBirthdays($user_id, 5);
    //$data['followings'] = $this->UserFollow->getUserFollowings($id);

    $data['groups'] = $this->GroupUser->getUserGroups($id);
    //$data['intrest_group'] = $data['groups'];
    $data['intrest_group'] = $this->GroupUser->getUserIntrests($id);
	$data['myprofile']['total_notifications'] = $this->Activities->countUserActivities($user_id);

    //echo"<pre>";print_R($data['intrest_group']);exit;
    if($user_id == $data['profile']['id'])
    {
      //Loading Feed and Feed comments
      $this->load->model('Updates');
      $feed = $this->Updates->getHomePageFeed($user_id, 10, 0);
      $items = array();
      $this->load->model('Statuses');

      foreach($feed as $item)
      {
		if($item['category'] == 'status updates')
        {
		$content_type = 'Status';
        $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type,0,10);
        $totalComment = $this->Statuses->getStatusRepliesCount($item['id'],$content_type);
        $item['total_comments'] = $totalComment;
		}
		else if($item['category'] == 'photo')
		{
			$content_type = 'Photo';
			$item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type,0,10);
      $totalComment = $this->Statuses->getStatusRepliesCount($item['id'],$content_type);
      $item['total_comments'] = $totalComment;
		}
		$item['likes'] = $this->Statuses->getLike($item['id'], $my_user['userid']);
        $items[] = $item;
      }
      $data['feed'] = $items;
    }

    	$filters = array(
	'feature' => 'Profile',
	'user_id' => $my_user['userid']
	);
	$prompt_data =  $this->Prompt->getByFilters($filters);
	if(isset($prompt_data) &&  $prompt_data!= '')
	{
		$data['user_prompt'] = 0;
	}
	else
	{
		$data['user_prompt'] = 1;
	}
	$data['myprofile']['total_notifications'] = $this->Activities->countUserActivities($user_id);
    $data['progress'] = $this->_calculateProfileCompleteness($data['profile'], count($data['groups']));

    $data['audit_constant'] = $this->config->item('audit');
    $concepts = $this->Survey->getSurveyConcept();
    if(isset($concepts) && !empty($concepts))
    {
      foreach($concepts as $concept)
      {
        $data['concept_manager'][] = $concept->user_id;
      }
    }
    $outlets = $this->Outlets->getOutletbyFilter(array());
    if(isset($outlets) && !empty($outlets))
    {
      foreach($outlets as $outlet)
      {
        $data['outlet_manager'][] = $outlet->user_id;
      }
    }
    $this->load->view('profile', $data);
  }

  function _calculateProfileCompleteness($profile, $total_groups)
  {
    $progress = 5;
    if (trim($profile['skills']))
    {
     $progress += 25;
    }
    if (trim($profile['about']))
    {
       $progress += 25;
    }

    if(!preg_match('/^default/', $profile['profile_pic']))
    {
      $progress += 20;
    }

    $progress += 25;

    /*
    if (isset($total_groups) && $total_groups > 0)
    {
      $progress += 25;
    }
    */
    return $progress;
  }



//Status update tab for logged in user
  function status_updates() {
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');
    $profile_id = (int) $this->uri->segment(3);
    $userId = $this->session->userdata("userid");
    $user_email = $this->session->userdata("useremail");
    $db_myprofile_details = $this->m_profile->get_profile_by_email($user_email);

    $profile_details = $this->m_profile->get_profile($profile_id, $userId);
    $profile_name = ucfirst(strtolower($profile_details['first_name'])) . " " . ucfirst(strtolower($profile_details['last_name']));

    $data = array(
        'profile_id' => $profile_id,
        'profile_name' => $profile_name,
        'myprofile' => $db_myprofile_details,
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );

    $pagelimit = 0;
    $data['messagelist'] = $this->m_profile->su_show_my_updates($pagelimit, $userId, $profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $message = $data['messagelist'][$i]['message'];
      $formated_time = time_ago($message_time); //calling time ago helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $data['messagelist'][$i]['message'] = text_to_link($message); //Text to link helper
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }
    $db_msg_count = count($data['messagelist']);
    $page_limit = $this->session->userdata('cfgpagelimit');
    if ($db_msg_count >= $page_limit) {
      $data['show_load_more'] = true;
    } else {
      $data['show_load_more'] = false;
    }
    $this->load->view('status_update_template', $data);
  }

//Load More pagination
  function pagination() {
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');

    $offset = $this->input->post("pagesize");
    $userId = $this->session->userdata("userid");
    $profileId = $this->input->post("profile_id");
    $data['messagelist'] = $this->m_profile->su_show_my_updates($offset, $userId, $profileId);
    //Customizing the Database field
    for ($i = 0; $i < count($data['messagelist']); $i++) {
      $message_time = $data['messagelist'][$i]['message_time'];
      $formated_time = time_ago($message_time); //calling time ago helper
      $data['messagelist'][$i]['message_time'] = $formated_time;
      $email = $data['messagelist'][$i]['email'];
      list($reply_user, $domain) = @explode("@", $email); //Reply user name is the first part of the email address
      $data['messagelist'][$i]['reply_user'] = $reply_user;
    }
    $this->load->view('show_status_message', $data);
  }

  ////Files tab for logged in user
  function files() {
    $this->load->helper('time_ago');
    $this->load->helper('text_to_link');
    $profile_id = (int) $this->uri->segment(3);
    $userId = $this->session->userdata("userid");
    $user_email = $this->session->userdata("useremail");
    $db_myprofile_details = $this->m_profile->get_profile_by_email($user_email);

    $profile_details = $this->m_profile->get_profile($profile_id, $userId);
    $profile_name = ucfirst(strtolower($profile_details['first_name'])) . " " . ucfirst(strtolower($profile_details['last_name']));

    $data = array(
        'profile_id' => $profile_id,
        'myprofile' => $db_myprofile_details,
        'profile_name' => $profile_name,
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );

    $total_files = $this->m_profile->files_get_files_count($profile_id);
    $total_files = $total_files['files_count'];
    $this->load->library('pagination');
    $config['base_url'] = base_url() . 'index.php/profile/files/' . $profile_id;
    $config['total_rows'] = $total_files;
    $config['per_page'] = 20;
    $config['num_links'] = 2;
    $config['uri_segment'] = 4;
    $config['prev_link'] = "Prev";
    $config['next_link'] = "Next";
    $config['first_link'] = "&#171;";
    $config['last_link'] = "&#187;";
    $config['first_tag_open'] = "<li>";
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tag_close'] = "</li>";
    $config['num_tag_open'] = "<li>";
    $config['num_tag_close'] = "</li>";
    $config['cur_tag_open'] = "<li><strong>";
    $config['cur_tag_close'] = "</strong></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tag_clos'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tag_clos'] = "</li>";

    $data['total_count'] = $config['total_rows'];
    $data['limit'] = ($this->uri->segment(4, 0)) ? $this->uri->segment(4, 0) : 1;
    $data['offset'] = ($config['total_rows'] <= $config['per_page']) ? $config['total_rows'] : ($this->uri->segment(4, 0) + $config['per_page']);
    $this->pagination->initialize($config);

    $data['navlinks'] = $this->pagination->create_links();

    $db_all_files = $this->m_profile->files_get_all_files($profile_id, $config['per_page'], $this->uri->segment(4, 0));

    //Data formating
    if (is_array($db_all_files)) {
      for ($i = 0; $i < (count($db_all_files)); $i++) {

        $uploaded_on_time = $db_all_files[$i]['uploaded_on'];
        $formated_time = time_ago($uploaded_on_time); //calling time ago helper
        $db_all_files[$i]['uploaded_on'] = $formated_time;

        $icon_path = "";
        $fileName = $db_all_files[$i]['path'];
        $ext = substr(strrchr($fileName, '.'), 1);
        if ($ext) {
          $icon_path = base_url() . "images/files/ext/ico-" . $ext . ".jpg";
        }
        if (!@fopen($icon_path, "r")) {
          $icon_path = base_url() . "images/files/ext/ico-default.jpg";
        }

        //Tags
        $db_file_tags = $this->m_profile->files_get_tags_for_file($db_all_files[$i]['file_id']);
        $tag_arr = array();
        if (is_array($db_file_tags)) {
          foreach ($db_file_tags as $tags) {
            $tag_arr[] = '<a class="grey-shade-1" href="' . base_url() . "index.php/files/all_tags/" . $tags['id'] . '/">' . $tags['name'] . '</a>';
          }
          $file_tag = @implode(", ", $tag_arr);
        }

        $db_all_files[$i]['file_tags'] = $file_tag;
        $db_all_files[$i]['file_ext'] = $ext;
        $db_all_files[$i]['file_icon'] = $icon_path;
      }
    }
    $data['files_list'] = $db_all_files;
    $this->load->view('files_template', $data);
  }

  ////Your posted marketplace items tab for logged in user
  function items() {
    $this->load->helper('time_ago');
    $this->load->model("market_place/market_place_model");
    $profile_id = (int) $this->uri->segment(3);
    $user_email = $this->session->userdata("useremail");
    $userId = $this->session->userdata("userid");
    $db_myprofile_details = $this->m_profile->get_profile_by_email($user_email);

    $profile_details = $this->m_profile->get_profile($profile_id, $userId);
    $profile_name = ucfirst(strtolower($profile_details['first_name'])) . " " . ucfirst(strtolower($profile_details['last_name'])) ;

    $data = array(
        'profile_id' => $profile_id,
        'myprofile' => $db_myprofile_details,
        'profile_name' => $profile_name,
        'gn_newcommer_list' => $this->_newcommer_list(),
        'gn_interest_groups_list' => $this->_interest_group_list(),
        'gn_my_reminders' => $this->_my_reminders()
    );
    //Items For Sales
    $db_item_sale = $this->market_place_model->mp_get_all_your_items_sale($profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($db_item_sale); $i++) {
      $added_on_time = $db_item_sale[$i]['added_on'];
      $formated_time = time_ago($added_on_time); //calling time ago helper
      $db_item_sale[$i]['added_on'] = $formated_time;
      $image_path = base_url() . "images/marketplace/" . $db_item_sale[$i]['item_image'];
      if ($db_item_sale[$i]['item_image']) {
        if (@fopen($image_path, 'r')) {
          $db_item_sale[$i]['item_image'] = $image_path;
        } else {
          $db_item_sale[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
        }
      } else {
        $db_item_sale[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
      }
    }
    $data['your_item_sale_limit'] = 1;
    $data['your_item_sale_offset'] = count($db_item_sale);
    $data['your_item_sale_total_count'] = count($db_item_sale);
    $data['your_items_sale'] = $db_item_sale;
    //Requested Items list
    $db_item_requested = $this->market_place_model->mp_get_all_your_items_requested($profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($db_item_requested); $i++) {
      $added_on_time = $db_item_requested[$i]['added_on'];
      $formated_time = time_ago($added_on_time); //calling time ago helper
      $db_item_requested[$i]['added_on'] = $formated_time;
      $image_path = base_url() . "images/marketplace/" . $db_item_requested[$i]['item_image'];
      if ($db_item_requested[$i]['item_image']) {
        if (@fopen($image_path, 'r')) {
          $db_item_requested[$i]['item_image'] = $image_path;
        } else {
          $db_item_requested[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
        }
      } else {
        $db_item_requested[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
      }
    }
    $data['your_item_requested_limit'] = 1;
    $data['your_item_requested_offset'] = count($db_item_requested);
    $data['your_item_requested_total_count'] = count($db_item_requested);
    $data['your_items_requested'] = $db_item_requested;
    //Freebies List
    $db_item_freebies = $this->market_place_model->mp_get_all_your_items_freebies($profile_id);
    //Customizing the Database field
    for ($i = 0; $i < count($db_item_freebies); $i++) {
      $added_on_time = $db_item_freebies[$i]['added_on'];
      $formated_time = time_ago($added_on_time); //calling time ago helper
      $db_item_freebies[$i]['added_on'] = $formated_time;
      $image_path = base_url() . "images/marketplace/" . $db_item_freebies[$i]['item_image'];
      if ($db_item_freebies[$i]['item_image']) {
        if (@fopen($image_path, 'r')) {
          $db_item_freebies[$i]['item_image'] = $image_path;
        } else {
          $db_item_freebies[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
        }
      } else {
        $db_item_freebies[$i]['item_image'] = base_url() . "images/marketplace/no-image.jpg";
      }
    }
    $data['your_item_freebies_limit'] = 1;
    $data['your_item_freebies_offset'] = count($db_item_freebies);
    $data['your_item_freebies_total_count'] = count($db_item_freebies);
    $data['your_items_freebies'] = $db_item_freebies;
    $this->load->view("items_template", $data);
  }

  //Change Password
  function change_password() {
    $this->load->view("pop_windows/change_password");
  }

  //Change Photo
  function change_photo() {
    $user_email = $this->session->userdata("useremail");
    $db_data = $this->m_profile->get_profile_by_email($user_email);
    $data['profile_pic'] = $db_data['profile_pic'];
    $this->load->view("pop_windows/change_photo", $data);
  }

  //Add Fav. Links
  function fav_links() {
    $this->load->view("pop_windows/fav_links");
  }

  //Report File Screen
  function report_file() {
    $data['file_id'] = (int) $this->uri->segment(3);
    $this->load->view("pop_windows/report_file", $data);
  }

  //Report Markst Screen
  function report_market() {
    $data['item_id'] = (int) $this->uri->segment(3);
    $this->load->view("pop_windows/report_market", $data);
  }

  //Report Status update Screen
  function report_status_update() {
    $data['message_id'] = (int) $this->uri->segment(3);
    $this->load->view("pop_windows/report_status_update", $data);
  }

  //Remove Fav. Link
  function fav_remove_link() {
    $user_id = $this->session->userdata("userid");
    $link_id = $this->input->post("link_id");
    $link_details = array(
        'status' => 2,
    );
    //Update Links details
    $row = $this->m_profile->update_fav_link($link_details, $user_id, $link_id);
    if ($row) {
      $status = 1;
    } else {
      $status = 0;
    }
    $result = '{"status": "' . $status . '"}';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Download Vcard of user
  function download_vcard() {
    $profile_id = (int) $this->uri->segment(3);
    $user_id = $this->session->userdata("userid");
    $user_data = $this->m_profile->get_profile($profile_id, $user_id);
    //echo "<xmp>".print_r($user_data,1)."</xmp>";exit;
    $v_card = "BEGIN:VCARD
							VERSION:3.0
							N:" . ucfirst(strtolower($user_data['last_name'])) . ";" . ucfirst(strtolower($user_data['middle_name'])) . ";" . ucfirst(strtolower($user_data['first_name'])) . "
							FN:" . ucfirst(strtolower($user_data['first_name'])) . " " . ucfirst(strtolower($user_data['last_name'])) . "
							ORG:" . $user_data['concept'] . "
							ROLE:" . $user_data['department'] . "
							TITLE:" . $user_data['designation'] . "
							TEL;TYPE=WORK,PHONE:" . $user_data['phone'] . "
							TEL;TYPE=WORK,cell:" . $user_data['mobile'] . "
							ADR;TYPE=WORK:;;;;" . $user_data['location'] . ";;" . $user_data['territory'] . "
							LABEL;TYPE=WORK:100 Waters Edge\nBaytown, LA 30314\nUnited States of America
							EMAIL;TYPE=PREF,INTERNET:" . $user_data['email'] . "
							REV:20080424T195243Z
							END:VCARD";
    $data['vcard_name'] = ucfirst(strtolower($user_data['first_name']));
    $data['vcard'] = $v_card;
    $this->load->view('v_vcard_print', $data);
  }

  //Interest Group Autocomplete
  function autocomplete_interest_groups() {
    $src_term = $this->input->post('term');
    $interest_groups = $this->m_profile->get_all_interest_groups($src_term);
    $json_result = array();
    foreach ($interest_groups as $val) {
      $json_result[] = '{"id": "' . $val['id'] . '", "label": "' . $val['name'] . '"}';
    }
    if (count($json_result)) {
      $result = '[ ' . @implode(",", $json_result) . ' ]';
    } else {
      $result = '[ {"id": "0", "label": "No Results"} ]';
    }
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

//Edit profile Submit
  function submit_edit_profile() {
   $error='no';
   $username = $this->input->post('username');
   $user_id = $this->session->userdata("userid");
    $user_skill = $this->input->post('skills_experience');
    $interest_groups = $this->input->post('interests');
    $about_me = $this->input->post('about_me');
	$display_name = $this->input->post('display_name');

  
    $mobile = $this->input->post('mobile');

   
   $phone = $this->input->post('phone');
   $extension = $this->input->post('extension');

  //exit;
  if($this->input->post('check_user') == 1)
  {
	$temp_name = $this->User->getByUsername($username);
	if(isset($temp_name['id']))
	{

		if($temp_name['id'] == $user_id)
		{

		}
		else
		{
			$error='yes';
		}

	}
	 print $error;exit;
  }
  else
  {
	$temp_name = $this->User->getByUsername($username);
	if(isset($temp_name['id']))
	{

		if($temp_name['id'] == $user_id)
		{
			$user_details = array(
				'skills' => $user_skill,
				'about' => $about_me,
				'display_name' =>$display_name,
        'extension' => $extension,
        'phone' => $phone,
        'mobile' => $mobile
			);
			 $this->User->update($user_details, $user_id);
		}
		else
		{
			$error='yes';
		}

	}
	else
	{

    //creating update array for database



			$user_details = array(
				'skills' => $user_skill,
				'about' => $about_me,
				'reply_username'=>$username,
				'display_name' =>$display_name,
        'extension' => $extension,
        'phone' => $phone,
        'mobile' => $mobile
			);
			 $this->User->update($user_details, $user_id);


	}
	  $interest_groups = @explode(",", $interest_groups);
    if (is_array($interest_groups)) {
      $this->m_profile->remove_user_interest_groups($user_id);

      foreach ($interest_groups as $groups) {

          $user_group_details = array(
              'intrest_groups_id' => $groups,
              'user_id' => $user_id,
              'joined_on' => time()
          );
          //Insert user Intrest Group
          $this->m_profile->insert_user_interest_groups($user_group_details);
        }
      }

	  $errorlist['error']=$error;
	  print $error;exit;
	  //print json_encode($errorlist);
    /*$this->session->set_flashdata('profile_status', 'Your profile has been edited successfuly.');
    redirect(site_url("profile/edit_profile"));*/
	}
  }

  // Change feed setting
  function submit_change_setting(){
  $user_id = $this->session->userdata("userid");
  $feed_allow_status = $this->input->post("people");
  $data = array(
  'all_feeds' => $feed_allow_status
  );
  $status = $this->User->update($data, $user_id);
  if($status)
  {
	 $result['status'] = 1;
	 $result['status_msg'] = "Your Feed setting changed successfully!";
  } else {
	$result['status'] = 0;
	 $result['status_msg'] = "Something went wrong while updating setting.";
  }
    $temp['json'] = json_encode($result);
	 echo $temp['json'];
  // update(array $data, $id)
  }
  //Submit change Password
  function submit_change_password() {

    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
	$current_password = $this->input->post("cPassword");
    $password = $this->input->post("nPassword");
    $cnf_password = $this->input->post("nPassword2");

	$old_pass = $this->User->getPassword(intval($user_id));
	if($old_pass != md5($current_password))
	{
	$result['status'] = 0;
    $result['status_msg'] = "Current password is wrong.";
	}
    elseif (!trim($password)) {
      $result['status'] = 1;
      $result['status_msg'] = "New password is required.";
    } elseif ($password != $cnf_password) {
      $result['status'] = 1;
      $result['status_msg'] = "Passwords do not match";
    } else {
      $result['status'] = 2;
    }
    if ($result['status'] == 2) {
      //creating update array for database
      $user_details = array(
          'password' => md5($password)
      );
      $pass_status = $this->m_profile->update_user_password($user_details, $user_id);

        $result['status_msg'] = "Your password changed successfully!";

    }
  // $result = '{"status": "' . $status_msg . '"}';
    //$data['json'] = $result;
	 $temp['json'] = json_encode($result);
	 echo $temp['json'];
   // $this->load->view("profile_print_json", $data);
  }

  //Submit Change Profile Pic
  function submit_change_profile_pic() {
    //replacing the users file to avoid duplications
    $user_id = $this->session->userdata('userid');
    $user_email = $this->session->userdata('useremail');
    $db_user_details = $this->m_profile->get_profile_by_email($user_email);
    $profile_pic = $db_user_details['profile_pic'];
    //If default image
    if (($profile_pic == "default.jpg") || ($profile_pic == "default-f.jpg")) {
      $profile_pic = $user_id . ".jpg";
      $user_details = array("profile_pic" => $profile_pic);
      $this->m_profile->update_profile_pic($user_details, $user_id);
    }
    $status = 0;


    $img_path = $_FILES['file_new_pic']['tmp_name'];
    $img_thumb = './images/user-images/105x101/' . $profile_pic;
    $this->load->helper('image_resize');

    //$this->crop($img_path,$img_thumb);

    $config['upload_path'] = './images/user-images/tmp_images/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['overwrite'] = false;
    $config['max_width'] = '0';
    $config['max_height'] = '0';

    $this->load->library('upload', $config);
    if ($this->upload->do_upload("file_new_pic")) {
      $file_details = $this->upload->data();

      $resizeObj = new resize($file_details['full_path']);
      $resizeObj->resizeImage(105, 105, 'crop');
      $resizeObj->saveImage($img_thumb, 100);

      if (@fopen($img_thumb, "r")) {
        $status = 1;
        @unlink($file_details['full_path']);
      } else {
        $status = 0;
      }
    } else {
      $status = 0;
    }
    if ($status) {
      $status_msg = "Profile photo changed";
    } else {
      $status_msg = "Please try again";
    }
    $new_pic = base_url() . "images/user-images/105x101/" . $profile_pic;
    $result = '{"status": "' . $status_msg . '","pic":"' . $new_pic . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }
//profile pic optimising
  public function crop($img_path, $img_thumb) {
    //$img_path = 'uploads\capsamples.jpg';
    //$img_thumb = 'uploads\capsamples_thumb.jpg';

    $config['image_library'] = 'gd2';
    $config['source_image'] = $img_path;
    $config['create_thumb'] = TRUE;
    $config['maintain_ratio'] = FALSE;

    $img = imagecreatefromjpeg($img_path);
    $_width = imagesx($img);
    $_height = imagesy($img);

    $img_type = '';
    $thumb_size = 105;

    if ($_width > $_height) {
      // wide image
      $config['width'] = intval(($_width / $_height) * $thumb_size);
      if ($config['width'] % 2 != 0) {
        $config['width']++;
      }
      $config['height'] = $thumb_size;
      $img_type = 'wide';
    } else if ($_width < $_height) {
      // landscape image
      $config['width'] = $thumb_size;
      $config['height'] = intval(($_height / $_width) * $thumb_size);
      if ($config['height'] % 2 != 0) {
        $config['height']++;
      }
      $img_type = 'landscape';
    } else {
      // square image
      $config['width'] = $thumb_size;
      $config['height'] = $thumb_size;
      $img_type = 'square';
    }

    $this->load->library('image_lib');
    $this->image_lib->initialize($config);
    $this->image_lib->resize();

    // reconfigure the image lib for cropping
    $conf_new = array(
        'image_library' => 'gd2',
        'source_image' => $img_thumb,
        'create_thumb' => FALSE,
        'maintain_ratio' => FALSE,
        'width' => $thumb_size,
        'height' => $thumb_size
    );

    if ($img_type == 'wide') {
      $conf_new['x_axis'] = ($config['width'] - $thumb_size) / 2;
      $conf_new['y_axis'] = 0;
    } else if ($img_type == 'landscape') {
      $conf_new['x_axis'] = 0;
      $conf_new['y_axis'] = ($config['height'] - $thumb_size) / 2;
    } else {
      $conf_new['x_axis'] = 0;
      $conf_new['y_axis'] = 0;
    }

    $this->image_lib->initialize($conf_new);

    $this->image_lib->crop();
  }

  //Submit Add Link
  function submit_add_fav_link() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txturl = $this->input->post("txturl");
    $txturltitle = $this->input->post("txturltitle");

    if (!$txturl) {
      $status = 1;
      $status_msg = "Url is required";
    } else if (!$txturltitle) {
      $status = 1;
      $status_msg = "Url Title is required";
    } else {
      $status = 2;
    }

    $add_link = array(
        'id' => NULL,
        'link_name' => $txturltitle,
        'link' => $txturl,
        'user_id' => $user_id,
        'status' => 1
    );
    $row = $this->m_profile->insert_fav_link($add_link);
    if ($row) {
      $status_msg = "Url has been added";
      $link = "<li><a target='_blank' href='" . $txturl . "'>" . $txturltitle . "</a></li>";
    } else {
      $status_msg = "Please try again";
      $link = "";
    }
    $result = '{"status": "' . $status_msg . '","link":"' . $link . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit report files
  function submit_report_files() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txtfile_id = $this->input->post("file_id");
    $comments = $this->input->post("comments");

    if (!$txtfile_id) {
      $status = 1;
      $status_msg = "File is required";
    } else if (!$comments) {
      $status = 1;
      $status_msg = "Comments Required";
    } else {
      $status = 2;
    }

    $update_file_status = array(
        "files_status" => 2
    );
    $row_file_status = $this->m_profile->update_file_status($update_file_status, $txtfile_id);

    if ($row_file_status) {
      $report_file = array(
          'id' => NULL,
          'file_id' => $txtfile_id,
          'comment' => $comments,
          'user_id' => $user_id,
          'status_id' => 1,
          'report_time' => time()
      );
      $row = $this->m_profile->insert_file_report($report_file);
      if ($row) {
        $status_msg = "This file has been reported to Admin";
      } else {
        $status_msg = "This file is already reported to Admin";
      }
    } else {
      $status_msg = "This file is already reported to Admin";
    }
    $result = '{"status": "' . $status_msg . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit report market item
  function submit_report_market() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txtfile_id = $this->input->post("item_id");
    $comments = $this->input->post("comments");

    if (!$txtfile_id) {
      $status = 1;
      $status_msg = "File is required";
    } else if (!$comments) {
      $status = 1;
      $status_msg = "Comments Required";
    } else {
      $status = 2;
    }

    $update_item_status = array(
        "items_status" => 2
    );
    $row_item_status = $this->m_profile->update_item_status($update_item_status, $txtfile_id);

    if ($row_item_status) {
      $report_item = array(
          'id' => NULL,
          'market_id' => $txtfile_id,
          'comment' => $comments,
          'user_id' => $user_id,
          'status_id' => 1,
          'report_time' => time()
      );
      $row = $this->m_profile->insert_market_report($report_item);
      if ($row) {
        $status_msg = "This item has been reported to Admin";
      } else {
        $status_msg = "This item is already reported to Admin";
      }
    } else {
      $status_msg = "This item is already reported to Admin";
    }
    $result = '{"status": "' . $status_msg . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  //Submit report files
  function submit_report_status_update() {
    $status = 1;
    $status_msg = "";
    $user_id = $this->session->userdata("userid");
    $txtmessage_id = $this->input->post("message_id");
    $comments = $this->input->post("comments");

    if (!$txtmessage_id) {
      $status = 1;
      $status_msg = "Message is required";
    } else if (!$comments) {
      $status = 1;
      $status_msg = "Comments Required";
    } else {
      $status = 2;
    }

    $update_message_status = array(
        "status" => 2
    );
    $row_message_status = $this->m_profile->update_message_status($update_message_status, $txtmessage_id);

    if ($row_message_status) {
      $report_status_update = array(
          'id' => NULL,
          'users_status_update_id' => $txtmessage_id,
          'comment' => $comments,
          'user_id' => $user_id,
          'status_id' => 1,
          'report_time' => time()
      );
      $row = $this->m_profile->insert_message_report($report_status_update);
      if ($row) {
        $status_msg = "This message has been reported to Admin";
      } else {
        $status_msg = "This message is already reported to Admin";
      }
    } else {
      $status_msg = "This message is already reported to Admin";
    }
    $result = '{"status": "' . $status_msg . '" }';
    $data['json'] = $result;
    $this->load->view("profile_print_json", $data);
  }

  function upload_picture(){
    $upload = array();
    $temp = array();
    $config = array();
	$src = '';
	$file_name = '';
	$user_id = $this->session->userdata('userid');
    $user_email = $this->session->userdata('useremail');
    $db_user_details = $this->User->getUserProfile($user_id);
    $profile_pic = $db_user_details['profile_pic'];
    $directory = 'images/user-images/tmp';
    if(!is_dir($directory))
    {
      mkdir($directory,0777);
    }
        $config['upload_path'] = $directory;
		$config['allowed_types'] = 'gif|jpg|png|bmp';
		$config['max_size']	= '10240';
		#$config['max_width']  = '1024';
		#$config['max_height']  = '768';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('fileUploadPicture'))
		{
			$error = array('error' => $this->upload->display_errors());
			$upload['status'] = $error;
		}
		else
		{
			$upload['status'] = 'true';
		  $upload['data'] = $this->upload->data();
		  //print_r($upload['data']);
		 // exit;
		 if($upload['data']['image_width'] >= $upload['data']['image_height'])
		 {
			$dim = 'height';
		 }
		 else
		 {
			$dim = 'width';
		 }
		 
				$src = 'images/user-images/tmp/'.$upload['data']['file_name'];
				$file_name = $upload['data']['file_name'];
				$type = strtolower(substr(strrchr($src,"."),1));
				if($type == 'jpeg')
				{
				$type = 'jpg';
				}
				
				/*switch($type){
				case 'bmp': $img_r = imagecreatefromwbmp($src); break;
				case 'gif': $img_r = imagecreatefromgif($src); break;
				case 'jpg': $img_r = imagecreatefromjpeg($src); break;
				case 'png': $img_r = imagecreatefrompng($src); break;
				default :
				$upload['status'] = 'false';
				}*/
				/*
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

				$dst_r_s = ImageCreateTrueColor( 25, 25 );

				if(imagecopyresampled($dst_r,$img_r,0,0,$this->input->post('x'),$this->input->post('y'),$targ_w,$targ_h,$this->input->post('w'),$this->input->post('h')) && imagecopyresampled($dst_r_s,$img_r,0,0,$this->input->post('x'),$this->input->post('y'),25,25,$this->input->post('w'),$this->input->post('h')))
				{
				if(imagejpeg($dst_r,'images/user-images/105x101/'.$user_id.'.'.$type, $jpeg_quality) && imagejpeg($dst_r_s,'images/user-images/25x25/'.$user_id.'.'.$type, $jpeg_quality)){
				unlink($src);
				*/
				if($upload['status'] == 'true')
				{
					$this->load->library('image_lib');
					$config['image_library'] = 'gd2';
					$config['source_image']	=  $src;
					//$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = $dim;
					$config['width']	 = 105;
					$config['height']	= 105;
					$config['new_image'] = 'images/user-images/105x101/'.$user_id.'.'.$type;
					//$this->load->library('image_lib', $config);
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Crop Image
					$config['image_library'] = 'gd2';
					$config['source_image']	=  'images/user-images/105x101/'.$user_id.'.'.$type;
					$config['maintain_ratio'] = FALSE;
					$config['width']	 = 105;
					$config['height']	= 105;
					$config['x_axis'] = '0';
					$config['y_axis'] = '0';
					$this->image_lib->initialize($config);
					$this->image_lib->crop();
					$this->image_lib->clear();
					//End Crop Image

					$config1['image_library'] = 'gd2';
					$config1['source_image']	=  $src;
					//$config['create_thumb'] = TRUE;
					$config1['maintain_ratio'] = TRUE;
					$config1['master_dim'] = $dim;
					$config1['width']	 = 25;
					$config1['height']	= 25;
					$config1['new_image'] = 'images/user-images/25x25/'.$user_id.'.'.$type;
					//$this->load->library('image_lib', $config);
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Crop Image
					$config1['image_library'] = 'gd2';
					$config1['source_image']	=  'images/user-images/25x25/'.$user_id.'.'.$type;
					$config1['maintain_ratio'] = FALSE;
					$config['x_axis'] = '0';
					$config['y_axis'] = '0';
					$config1['width']	 = 25;
					$config1['height']	= 25;
					$this->image_lib->initialize($config1);
					$this->image_lib->crop();
					$this->image_lib->clear();
					// End Crop Image
					$user_details = array("profile_pic" => $user_id.'.'.$type);
					$this->m_profile->update_profile_pic($user_details, $user_id);
					$upload['new_image'] = 'images/user-images/105x101/'.$user_id.'.'.$type;
					$upload['status'] = 'true';
				}
				
				
				
				// add profile pic to user album
				
				$owner_id = $user_id;
				$owner_type = 'User';
				$target_id =  0;
				$target_type = '';
				$album_name = 'User_Photos_'.$user_id;
				$count_album_slug = $this->manage_model->get_album_slug_count($album_name);
				$album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath'));
				$create_album_directory = str_replace(base_url(), '', $album_config['ecards_more_pic_target']) . $album_config['fullpath'];
				$create_album_directory .= $album_name;
				$alb_dir_path = str_replace("/system/", '', BASEPATH);
				  //$create_album_directory = $alb_dir_path . '/public_html/phpalbum/albums/' . $album_name;
				$create_album_directory = 'phpalbum/albums/' . $album_name;
				$album = array(
				  'title' => $album_name,
				  'description' => 'User profile album',
				  'visibility' => 1, //Always publish as daraft mode unless photos are uploaded
				  'category' => 1,
				  'owner' => $this->session->userdata('userid'),
				  'keyword' => $album_name
				);
				
				if(!is_dir($create_album_directory))
				{
				 @mkdir($create_album_directory, 0777, true);
				  $album_id = $this->manage_model->insert_album($album);
				}
				  $album_config = $this->manage_model->get_album_config(array('ecards_more_pic_target', 'fullpath', 'thumb_pfx', 'thumb_height', 'thumb_width'));
				  $album_id = $this->Album->getIdByTitle($album_name);
				 
					if(!copy($src, $create_album_directory.'/'.$file_name))
						{
							echo "failed to copy $file_name...\n";
							
						} else {
						
							$config1['image_library'] = 'gd2';
							$config1['source_image']	= $src;
							$config1['maintain_ratio'] = TRUE;
							$config1['create_thumb'] = TRUE;
							$config1['width']	 = 184;
							$config1['height']	= 122;
							$config1['new_image']	= $create_album_directory.'/'.$album_config['thumb_pfx'].$file_name;
							$this->image_lib->initialize($config1);
							$this->image_lib->resize();
							$this->image_lib->clear();
						}
					 
					$picture = array(
					'pid' => NULL,
					'aid' => $album_id,
					'filepath' => $album_name . '/',
					'filename' => $file_name,
					'filesize' => 10,
					'owner_id' => $owner_id,
					'owner_type' => $owner_type,
					'target_id' => $target_id,
					'target_type' => $target_type,
					'ctime' =>  time(),
					'mtime' =>date('Y-m-d H:i:s'),
					'title' =>   '@#'.$db_user_details['reply_username'].':'.$db_user_details['fullname'].': changed the profile picture.'
				  );
				  $picture_id = $this->manage_model->insert_picture($picture); 
				  
				
		}

    $temp['json'] = json_encode($upload);
    echo $temp['json'];
  }
  function submit_new_change_profile_pic(){
    $upload = array();
    $temp = array();
    #echo "<pre>".print_r($this->input->post(),"\n")."</pre>";exit;
    $user_id = $this->session->userdata('userid');
    $user_email = $this->session->userdata('useremail');
    $db_user_details = $this->m_profile->get_profile_by_email($user_email);
    $profile_pic = $db_user_details['profile_pic'];
    $filename = $this->input->post('img');
    $targ_w = 105;
    $targ_h = 101;
    $jpeg_quality = 90;
    if($this->input->post('x') != "NaN" && $this->input->post('y') != "NaN" && $this->input->post('w') != "NaN"  && $this->input->post('5') != "NaN")
    {
      $src = 'images/user-images/tmp/'.$filename;
      $type = strtolower(substr(strrchr($src,"."),1));
      if($type == 'jpeg') $type = 'jpg';
      switch($type){
      case 'bmp': $img_r = imagecreatefromwbmp($src); break;
      case 'gif': $img_r = imagecreatefromgif($src); break;
      case 'jpg': $img_r = imagecreatefromjpeg($src); break;
      case 'png': $img_r = imagecreatefrompng($src); break;
      default : return "Unsupported picture type!";
      }

      $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

      $dst_r_s = ImageCreateTrueColor( 25, 25 );

      if(imagecopyresampled($dst_r,$img_r,0,0,$this->input->post('x'),$this->input->post('y'),$targ_w,$targ_h,$this->input->post('w'),$this->input->post('h')) && imagecopyresampled($dst_r_s,$img_r,0,0,$this->input->post('x'),$this->input->post('y'),25,25,$this->input->post('w'),$this->input->post('h')))
      {
        if(imagejpeg($dst_r,'images/user-images/105x101/'.$user_id.'.'.$type, $jpeg_quality) && imagejpeg($dst_r_s,'images/user-images/25x25/'.$user_id.'.'.$type, $jpeg_quality)){
          unlink($src);

          $user_details = array("profile_pic" => $user_id.'.'.$type);
          $this->m_profile->update_profile_pic($user_details, $user_id);

       	  $upload['status'] = 'true';
        }
        else{
          $upload['status'] = 'false';
        }
      }
      else
      {
        $upload['status'] = 'false';
      }
    }
    else
    {
      $upload['status'] = 'false';
    }
    $temp['json'] = json_encode($upload);
    echo $temp['json'];
  }
}