<?php

class Auth extends MX_Controller {
		

	function __construct() {
		parent:__construct();
	}
	function index() {
		
		$this->load->view('v_loginform');
	}
		
	function validate_user() {
		
		//date_default_timezone_set('Asia/Calcutta');
		date_default_timezone_set("Asia/Baghdad"); //default timezone setting
		$this->load->model('M_user');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if ($this->M_user->authenticate($username, $password)) {

			$this->load->model('M_people');
			
			$userdata = array(
				'is_logged_in' => true,
				'profile' => $this->M_people->get_profile_by_email($username),
				'userlastlogin' => strtotime($time)
			);
			$this->session->set_userdata($userdata);
			
			if ($comingfrom = get_cookie('comingfrom')) {

				// delete_cookie('comingfrom');
				setcookie('comingfrom', '', time() - 3600, '/ci');
				header("Location: $comingfrom");
			}
			elseif ($this->session->userdata('requested_uri')) {

				redirect($this->session->userdata('requested_uri'));
			}
			else {

				redirect('people');
			}
		}
		else {

			$data['login_failed'] = true;
			$this->load->view('v_loginform', $data);
		}
	}
	
	function logout() {
		
		$myprofile = $this->session->userdata('profile');
		$userid = $myprofile['id'];

		$userdata = array(
			'last_login_time' => $this->session->userdata('userlastlogin')
		);
		
		$this->db->update('ci_users', $userdata, "id = $userid");
		
		$this->session->sess_destroy();
		redirect('auth');
	}
}
