<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="<?php echo site_url(); ?>media/images/icons/favicon-01.png" type="image/x-icon">
<meta name="robots" content="index, nofollow">
<title><?php echo $profile['fullname']; ?>'s Profile - Landmark Group</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php $this->load->view('include_files/common_includes_new'); ?>
<link rel="stylesheet" href="<?php echo site_url(); ?>media/css/token-input.css" type="text/css" />
<link rel="stylesheet" href="<?php echo site_url(); ?>media/css/token-input-facebook.css" type="text/css" />
</head>
<body class="profile">

<?php 
use com\google\i18n\phonenumbers\PhoneNumberUtil;
use com\google\i18n\phonenumbers\PhoneNumberFormat;
use com\google\i18n\phonenumbers\NumberParseException;
require_once (APPPATH.'modules/libphone/PhoneNumberUtil.php');
$this->load->helper('form');
$this->load->view('global_header.php');
//$this->load->view('include_files/phone_formatting.php');

// Loading the intranet library for formatPhoneNumber()
$this->load->library('intranet');
$phoneUtil = PhoneNumberUtil::getInstance();

$cc =   &get_instance();
$cc->db->select('name');
$cc->db->from('ci_master_territory');
$query = $cc->db->get();
$ccq = $query->result();
/*print_r($ccq);*/
$countryName = array();
foreach ($ccq as $key => $value) {
	if($value->name == "UAE") {
		$countryName['AE'] = $value->name;	
	}
	if($value->name == "KSA") {
		$countryName['SA'] = $value->name;	
	}
	if($value->name == "Kuwait") {
		$countryName['KW'] = $value->name;	
	}
	if($value->name == "Bahrain") {
		$countryName['BH'] = $value->name;	
	}
	if($value->name == "Oman") {
		$countryName['OM'] = $value->name;	
	}
	if($value->name == "Qatar") {
		$countryName['QA'] = $value->name;	
	}
	if($value->name == "Jordan") {
		$countryName['JO'] = $value->name;	
	}
	if($value->name == "Lebanon") {
		$countryName['LB'] = $value->name;	
	}
	if($value->name == "Egypt") {
		$countryName['EG'] = $value->name;	
	}				        	
}

	//print_r($profile);exit;
$countryKey = array_search($profile['territory'], $countryName);
?>

<div class="section wrapper clearfix">
	<h2>Profile</h2>
	<ul class="breadcrumb">
    	<li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><span>&gt;&gt;</span></li>
        <li>Profile</li>
    </ul>
</div>

<div class="section wrapper clearfix">
	<div class="left-contents">

          <div class="profile-block clearfix">

              <div class="p-lft"><img class="avatar-big shadow-3"
                                      src="<?php echo site_url(); ?>images/user-images/105x101/<?php echo $profile['profile_pic'] . '?' . time(); ?>"
                                      alt="Avatar"/>
                  <?php if($profile['id'] == $myprofile['id']):?>
      <?php echo form_open_multipart('profile/upload_picture',array('id'=>'upload_picture','class'=>'fb-container-box d-n'));?>
      <h2>Upload your profile picture.</h2>
			 <input id="fileUploadPicture" name="fileUploadPicture" type="file" style="display: block;" />
			 <div id="profile-img-error" class="d-n" style="color:#F70F0F; padding-top: 10px;"></div>
      </form>
	  
				<div class="block-edit"><a href="#upload_picture" id="upload_picture_popup" class="editPicture iconPhoto d-n fancybox" href="#">&nbsp;</a></div>

				<div class="progress-c">
					Profile Completeness:
					<div id="progressBar"><div></div></div>
				</div>
			<?php 
			endif;
			?>
			</div>
			<div class="media-hide">
            <div class="p-rgt profile-intro">
			<?php if($profile['id'] == $myprofile['id']):?>
				<div class="block-edit"><a class="btn-sm btn-gry icon edit editProfile" href="#edit-profile-box">Edit Profile</a></div>
			<?php endif; ?>
            	<h1><?php echo stripcslashes($profile['fullname']); ?></h1>
            	<span><?php echo $profile['designation']; ?>, <?php echo $profile['concept']; ?><?php echo !empty($profile['territory']) ? ", ".$profile['territory'] : "" ?>
		</span>
                <ul>
                	<?php 
					if(isset($profile['mobile']) && $profile['mobile'] != ''):
					?>
                  	<li class="iconMobile">&nbsp;<a href="">
                  	<?php 
                  		$mobileNumberStr = $profile['mobile'];
                    echo $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
                    ?>
                        </a></li>
                  	<?php
					endif;
						  
					if(isset($profile['phone']) && $profile['phone'] != ''):
                  	?>
                  	<li class="iconTelephone">&nbsp;<a href="">
                  	<?php 
                  		$mobileNumberStr = $profile['phone'];
                    echo $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
                    ?>
                        </a> </li>
                	<?php                 			
					endif;
					if(isset($profile['extension']) && $profile['extension'] != ''): ?>
					<li class="iconExtno">&nbsp;
					 <?php echo "  Ext. No. ".$profile['extension']; ?>
					</li>
					<?php endif; 
					if(isset($profile['email']) && $profile['email'] != ''): ?>
                    <li><a href="mailto:<?php echo $profile['email']; ?>"><?php echo $profile['email']; ?></a></li>
                    <?php 
					endif;
					if($profile['id'] != $myprofile['id']):
					
					if($profile['follow_user'] > 0): ?>
            			<input type="button" class="btn-sm btn-follow btn-grn" name="submit" value="Following" user_id="<?php echo $profile['id']; ?>">
            		<?php else: ?>
            			<li><input type="button" class="btn-sm btn-follow" name="submit" value="Follow" user_id="<?php echo $profile['id']; ?>"/>
            		<?php endif; 
					endif;
					?>

				</ul>
				<div class="p-social">
                	<a href="#followings-view-all-box" id="bd-view-all-follow" class="fancybox">Following (<?php echo $profile['following']; ?>)</a> &#8226; <a href="#followers-view-all-box" id="bd-view-all-follower" class="fancybox">Followers</a> (<?php echo $profile['followers']; ?>) 
					<?php if($profile['id'] != $myprofile['id']): ?>
					<span class="ico"><a href="javascript:void(0)" onclick="javascript:jqcc.cometchat.chatWith(<?php echo $profile['id']; ?>);" title="Chat"><img src="media/images/icons/iconChat.png" alt="chat"></a> 
					<?php if(isset($profile['email']) && $profile['email'] != ''): ?>
					<a href="mailto:<?php echo $profile['email']; ?>" title="Mail"><img src="media/images/icons/iconMail_04.png" alt="chat"></a>
					<?php endif; ?>
					</span>
					<?php endif; ?>
                </div>
                <hr/>
				
					<div class="profile-block-description">
						<?php echo $profile['about'] != '' ? '<strong>About: </strong><div class="shortDesc">'. $profile['about']. '</div>' : ''; ?>
					</div>
				<?php if(strlen($profile['about']) > 200){ ?>
					<a href="#" class="more-less">More</a> 
				<?php } ?>
				
					<div class="profile-block-description">
						<?php echo $profile['skills'] != '' ? '<strong>Skills: </strong><div class="shortDesc">'. $profile['skills'].'</div>' : ''; ?>
					</div>
				<?php if(strlen($profile['skills']) > 200) { ?>
					<a href="#" class="more-less">More</a> 
				<?php } ?>
            </div>
        </div>
        
       

                 <!-- Shows in Responsive View -->
            <div class="media-show">
                <div class="p-rgt profile-intro">
                   <h1><?php echo stripcslashes($profile['fullname']); ?></h1>
                   <span><?php echo $profile['designation']; ?>, <?php echo $profile['concept']; ?><?php echo !empty($profile['territory']) ? ", ".$profile['territory'] : "" ?></span>
                   <ul>
                      <?php 
                         if(isset($profile['mobile']) && $profile['mobile'] != ''):
                         ?>
                      <li class="iconMobile">&nbsp;<a href=""> 
                         <?php 
                            $mobileNumberStr = $profile['mobile'];
                         echo $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
                         ?>
                          </a></li>
                      <?php
                         endif;
                            
                         if(isset($profile['phone']) && $profile['phone'] != ''):
                                        ?>
                      <li class="iconTelephone">&nbsp;<a href="">
                         <?php 
                            $mobileNumberStr = $profile['phone'];
                         echo $this->intranet->formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey);
                         ?>
                          </a></li>
                      <?php                 			
                       endif;
					   if(isset($profile['extension']) && $profile['extension'] != ''): ?>
						<li class="iconExtno">&nbsp;
						 <?php echo "  Ext. No. ".$profile['extension']; ?>
						</li>
					<?php endif;
                         if($profile['id'] != $myprofile['id']):
                         if($profile['follow_user'] > 0): ?>
                      <input type="button" class="btn-sm btn-follow btn-grn" name="submit" value="Following" user_id="<?php echo $profile['id']; ?>">
                      <?php else: ?>
                      <li><input type="button" class="btn-sm btn-follow" name="submit" value="Follow" user_id="<?php echo $profile['id']; ?>"/>
                         <?php endif; 
                            endif;
                            ?>
                   </ul>
                   <div class="p-social">
                      <a href="#followings-view-all-box" id="bd-view-all-follow" class="fancybox">Following <span><?php echo $profile['following']; ?></span></a> &#8226; <a href="#followers-view-all-box" id="bd-view-all-follower" class="fancybox">Followers <span><?php echo $profile['followers']; ?></span></a> 
                      <?php if($profile['id'] != $myprofile['id']): ?>
                      <span class="ico"><a href="javascript:void(0)" onclick="javascript:jqcc.cometchat.chatWith(<?php echo $profile['id']; ?>);" title="Chat"><img src="media/images/icons/iconChat.png" alt="chat"></a>
					<?php if(isset($profile['email']) && $profile['email'] != ''): ?>
					  <a href="mailto:<?php echo $profile['email']; ?>" title="Mail"><img src="media/images/icons/iconMail_04.png" alt="chat"></a>
					  <?php endif; ?>
					  </span>
                      <?php endif; ?>
                   </div>
									 <?php if($profile['id'] == $myprofile['id']):?>
                   <div class="block-edit"><a class="btn-sm btn-gry icon edit editProfile" href="#edit-profile-box">Edit Profile</a></div>
                   <?php endif; ?>
                </div>
            </div>
                
            <div class="p-btm profile-intro media-show">
			<?php if(isset($profile['email']) && $profile['email'] != ''): ?>
				<ul>
					<li class="iconMail"><a href="mailto:<?php echo $profile['email']; ?>"><?php echo $profile['email']; ?></a></li>
				</ul>
			<?php endif; ?>
			<hr/>
			<?php if(strlen($profile['about']) > 200){ ?>
				<div class="profile-block-description">
					<?php echo $profile['about'] != '' ? '<strong>About: </strong><div class="shortDesc">'. $profile['about']. '</div>' : ''; ?>
				</div>
				<a href="#" class="more-less">More</a> 
			<?php } else { ?>
				<div class="profile-block-short-description">
					<?php 	echo $profile['about'] != '' ? '<strong>About: </strong><div class="shortDesc">'. $profile['about']. '</div>' : ''; ?>
				</div>
			<?php } ?>
					
			<?php if(strlen($profile['skills']) > 200){ ?>
				<div class="profile-block-description">
					<?php echo $profile['skills'] != '' ? '<strong>Skills: </strong><div class="shortDesc">'. $profile['skills']. '</div>' : ''; ?>
				</div>
				<a href="#" class="more-less">More</a> 
			<?php } else { ?>
				<div class="profile-block-short-description">
			<?php 
				echo $profile['skills'] != '' ? '<strong>Skills: </strong><div class="shortDesc">'. $profile['skills']. '</div>' : ''; 
			?>
			</div>
			<?php } ?>
            </div>
            <!-- End Shows in Responsive View -->
            
        </div>

        <?php $this->load->view('partials/profile_post_status', array('target_id' => $profile['id'], 'target_type' => 'User')); ?>

        <?php $this->load->view('partials/profile_content_updates', array('statuses_filter' => $profile['id']) ); ?>

   </div> <!-- left-contents -->

	<div class="right-contents media-widget">

		<?php
		$this->load->view('widgets/birthday_widget');
		$this->load->view('widgets/profile_following', array('profile_id' => $profile['id']));
		$this->load->view('widgets/profile_followers', array('profile_id' => $profile['id']));
		$this->load->view('widgets/groups_widget');
		$this->load->view('profile_concepts_widget', array('profile_id' => $profile['id'])); 

		$this->load->view('poll_widget', array('created_by' => $profile['id'], 'created_by_type' => 'User'));

		?>

    </div> <!-- right-contents -->

</div> <!-- section -->

<?php $this->load->view('global_footer.php'); ?>

<?php if($profile['id'] == $myprofile['id']):?>
<div id="edit-profile-box" class="fb-container-box d-n">
	
	<h3><a id="edit_profile" href="#">Edit My Profile</a> &nbsp;|&nbsp; <a id="change_password" href="">Change Password</a>&nbsp;|&nbsp; <a id="change_setting" href="">Settings</a></h3>

	<div class="dlgEditProfile">
	<?php
	$attributes = array('id' => 'frmEditProfile');
	echo form_open('profile/submit_edit_profile', $attributes);
?>
		<!--<form id="frmEditProfile" method="post" action="ssi/ajax_response.php">-->
			<fieldset>
			<ul class="msg d-n"></ul>
			<dl class="cols-two">
				<dd>Display Name	<div class="textC"><input type="text"  id="display_name" name="display_name" placeholder="Display Name"  value="<?php echo  set_value('display_name',$myprofile['display_name']);?>" /></div></dd>
				<dd>Username	<div class="textC"><input type="text" id="username" name="username" placeholder="Username" value="<?php echo  set_value('username',$myprofile['reply_username']);?>"/></div></dd>
			</dl>
			<dl class="cols">
				<dd>Mobile Number	<div class="textC"><input type="text"  id="mobile" name="mobile" placeholder="Mobile Number"  value="<?php echo set_value('mobile',$myprofile['mobile']);?>" /></div></dd>
				<dd>Landline Number	<div class="textC"><input type="text"  id="phone" name="phone" placeholder="Landline Number"  value="<?php echo set_value('phone',$myprofile['phone']);?>" /></div></dd>
				<dd>Extension Number	<div class="textC"><input type="text"  id="extension" name="extension" placeholder="Extension"  value="<?php echo  set_value('extension',$myprofile['extension']);?>" /></div></dd>
				<dd>About Me	<div class="textC"><textarea id="about_me" name="about_me" rows="3" cols="50" placeholder="About Me"><?php echo  set_value('about_me',$myprofile['about']);?></textarea></div></dd>
				<dd>Interests	<div class="textC"><input type="text" Xid="interests" id="demo-input-facebook-theme" name="interests" placeholder="Interests" /></div></dd>
				<dd>Skills & Experience	<div class="textC"><textarea id="skills_experience" name="skills_experience" rows="3" cols="50" placeholder="Skills & Experience"><?php echo  set_value('skills_experience',$myprofile['skills']);?></textarea></div></dd>
				<dd><input type="button" id="update_profile" name="update_profile" class="btn-sm submit" value="Update Profile" /></dd>
			</dl>
			</fieldset>
		</form>
	</div>

	<div class="dlgChangePassword d-n">
	<!-- <h3>Change Password</h3> -->
	<form id="frmChangePassword" method="post" action="<?php echo site_url(); ?>profile/submit_change_password">
		<fieldset>
		<ul class="msg d-n"></ul>
		<dl class="cols-one">
			<dd>Current Password	<div class="textC"><input type="password"  id="cPassword" name="cPassword" placeholder="Current Password" /></div></dd>
			<dd>New Password	<div class="textC"><input type="password" id="nPassword" name="nPassword" placeholder="New Password" /></div></dd>
			<dd>Retype New Password	<div class="textC"><input type="password" id="nPassword2" name="nPassword2" placeholder="Retype New Password" /></div></dd>
			<dd><input type="button" id="submit_password" name="change_password" class="btn-sm" value="Change Password" /></dd>
		</dl>
		</fieldset>
	</form>
	</div>
	
	<div class="dlgsetting d-n holder">
	<!-- <h3>Change Password</h3> -->
	<form id="frmChangeSetting" method="post" action="<?php echo site_url(); ?>profile/submit_change_setting">
		<ul class="msg d-n"></ul>
		<h4>Manage your wall</h4>
		<p>See posts on your news feed from :<br/></p>
		<p><input type="radio" name="people" id="p1" value= "0"><label for="p1">People you follow</label></p>
		<p><input type="radio" name="people" id="all" value= "1"><label for="all">All</label></p>
		<p><input type="button" id="submit_setting" name="setting" class="btn-sm" value="Save" /></p>
	</form>
	</div>
	

</div>

<!--
<div id="upload-popup" class="fb-container-box upload-photo d-n">
	<h3>Upload your photograph</h3>
	Select area to be cropped...
	<div class="photo-placeholder"><img src="" id="target-photo" /></div>
	<form id="fUploadPictureN" name="uploadPicN" action="<?php// echo site_url(); ?>profile/submit_new_change_profile_pic" method="post">
		<fieldset>
		<input type="hidden" id="x" name="x" /> <input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" /> <input type="hidden" id="h" name="h" />
    <input type="hidden" id="img" name="img" />
		<input type="button" id="photoCropN" name="photoCropN" class="btn-sm icon edit" value="Crop" /> &nbsp;
		<input type="reset" id="photoResetN" name="photoResetN" class="btn-sm btn-gry" value="Reset" />
		<fieldset>
	</form>
</div>
-->
<?php endif; ?>

<?php $this->load->view('partials/js_footer'); ?>

<?php $this->load->view('templates/js/content_updates'); ?>

<?php $this->load->view('templates/js/new_status'); ?>
<?php $this->load->view('templates/js/new_upload_image'); ?>
<?php $this->load->view('templates/js/new_status_poll'); ?>
<?php $this->load->view('partials/interaction_js'); ?>
<!-- <script src="<?php // echo site_url(); ?>media/js/jquery.Jcrop.min.js"></script> -->
<script type="text/javascript">
var feedtype = 'profile';
var feedtype_id = <?php echo $profile['id']; ?>;
var limit = 10;
var start = 0;
var nearToBottom = 250;

$(document).ready(function(){

  $('input[name=people]').val([<?php echo $myprofile['all_feeds'];?>]);  
	$('.fancybox-thumb').fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		},
	});
      
	$(".fancybox").fancybox({
		maxHeight	: 800
	}); 
	$('.loader-more').show();
	loadUpdates('', $('#recent-posts ul.filter li:first'));

		$('.content').scroll(function() {
		if ($('.content').scrollTop() + $('.content').height() >=
		    $('.left-contents').height() ) {
			if(loadProgress == false){
			var selected = $('#recent-posts ul.filter li[class=active] a');
			start += 10;
		 	loadUpdates($(selected).attr('type'), $('#recent-posts ul.filter li[class=active]'));
			}
		}
	});

	$('#bd-view-all').fancybox({
		maxHeight	: 500
	})
  $('#upload_picture_popup').fancybox({
		maxHeight	: 500
	});    
      
  $('#upload-popup').fancybox({
    type: 'inline',
    "width":"auto",
    "height":"500px",
    autoScale: true,
    afterClose: function() {
	    $('#fileUploadPicture').val('');
	}
  });
	
	$('#bd-view-all-follow').fancybox({
		maxHeight	: 500
	})
	$('#bd-view-all-follower').fancybox({
		maxHeight	: 500
	})
	$('#groups-all-view').fancybox({
	maxHeight	: 500
	})
	$('#concepts-all-view').fancybox({
	maxHeight	: 500
	})
	$('#poll-view-all').fancybox({
		maxHeight	: 500
	})
	if(typeof poll_results !== 'undefined')
	{
		$.each(poll_results, function(index, item){
			_html	= '<li><label>'+item['title']+'</label><div id="progressBarWid'+index+'" class="progressBar"><div style="width:'+item['percentage']+'%">'+Math.round(Number(item['percentage']))+'%&nbsp;</div></div></li>';
			$('.widpoll').append(_html);
			//progress(item['percentage'], $('#progressBarWid'+index));
		});
	}
$("#update_profile").click(function() {

var username_length = $("#username").val().length;
var status = 1;
    
	///////////
	
	if ($('#display_name').val().length < 1 || $('#username').val().length < 1) {
			$('#username').addClass('invalid');
			$('#username').removeClass('valid');
			$('#edit-profile-box .msg').removeClass('success');
			$('#edit-profile-box .msg').addClass('error');
			$('#edit-profile-box .msg').append('<li/>').html('Please retry, enter data');
			$('#edit-profile-box .msg').fadeIn();
			//$.fancybox.resize();
			return false;
		}
		if ($('#about_me').val() == 'About Me'){
		$('#about_me').val('');
		}
		
		if ($('#skills_experience').val() == 'Skills & Experience'){
		$('#skills_experience').val('');
		}

		if($('#mobile').val() == 'Mobile Number'){
			$('#mobile').val('');

		}
		if($('#extension').val() == 'Extension'){
			$('#extension').val('');

		}
		if($('#phone').val() == 'Landline Number'){
			$('#phone').val('');

		}
		if(/^[0-9+ ()]*$/.test($('#phone').val()) == false) {

			$('#phone').addClass('invalid');
			$('#phone').removeClass('valid');

			$('#edit-profile-box .msg').removeClass('success');
			$('#edit-profile-box .msg').addClass('error');
			$('#edit-profile-box .msg').append('<li/>').html('Please enter corrent number');
			$('#edit-profile-box .msg').fadeIn();
					//$.fancybox.resize();
			return false;

		}

		if(/^[0-9+ ()]*$/.test($('#mobile').val()) == false) {

			$('#mobile').addClass('invalid');
			$('#mobile').removeClass('valid');

			$('#edit-profile-box .msg').removeClass('success');
			$('#edit-profile-box .msg').addClass('error');
			$('#edit-profile-box .msg').append('<li/>').html('Please enter corrent number');
			$('#edit-profile-box .msg').fadeIn();
					//$.fancybox.resize();
			return false;

		}
				
		if(/^[0-9+ ()]*$/.test($('#extension').val()) == false) {

			$('#extension').addClass('invalid');
			$('#extension').removeClass('valid');

			$('#edit-profile-box .msg').removeClass('success');
			$('#edit-profile-box .msg').addClass('error');
			$('#edit-profile-box .msg').append('<li/>').html('Please enter corrent number');
			$('#edit-profile-box .msg').fadeIn();
					//$.fancybox.resize();
			return false;

		}		
		

	if(/^[a-zA-Z0-9-.]*$/.test($('#username').val()) == false) 
		{
			$('#username').addClass('invalid');
			$('#username').removeClass('valid');
			$('#edit-profile-box .msg').removeClass('success');
			$('#edit-profile-box .msg').addClass('error');
			$('#edit-profile-box .msg').append('<li/>').html('Please retry, enter data');
			$('#edit-profile-box .msg').fadeIn();
			//$.fancybox.resize();
			return false;
		}
		

		$.fancybox.showLoading();

		$.ajax({
			type	: 'POST',
			cache	: false,
			url		: $('#frmEditProfile').attr('action'),
			data	: $('#frmEditProfile').serializeArray(),
			success	: function(data2) {
			console.log(data2);
			
			//console.log(data2);
			if(data2 == "yes")
			{
				$('#username').addClass('invalid');
				$('#username').removeClass('valid');
				$('#edit-profile-box .msg').removeClass('success');
				$('#edit-profile-box .msg').addClass('error');
				$('#edit-profile-box .msg').append('<li/>').html('Please enter correct username');
				$('#edit-profile-box .msg').fadeIn();
			//$.fancybox.resize();
			}
			else
			{	
			$('#username').addClass('valid');
				$('#username').removeClass('invalid');
				$('#edit-profile-box .msg').addClass('success');
				$('#edit-profile-box .msg').append('<li/>').html('Your profile updated successfully!');
				$('#edit-profile-box .msg').fadeIn();
				//$.fancybox.resize();
				// $.fancybox(data);
				//location.reload();
        window.location = $('#username').val();
			}
			$.fancybox.hideLoading();
			}
		});

		
	
	///////////
	
	
	return false;
});

$("#username").keyup(function() {
if ($('#username').val().length < 1) {
			$('#username').addClass('invalid');
			$('#username').removeClass('valid');
			//$.fancybox.resize();
			return false;
		} 
		else if(/^[a-zA-Z0-9-.]*$/.test($('#username').val()) == false) 
		{
			$('#username').addClass('invalid');
			$('#username').removeClass('valid');
			//$.fancybox.resize();
			return false;
		}
// else if($('#username').val().indexOf("@") >= 0)
var username = $('#username').val();
var check_user= 1;
		$.ajax({
			type	: 'POST',
			cache	: false,
			url		: $('#frmEditProfile').attr('action'),
			data	: {username: username, check_user: check_user},
			success	: function(data2) {
			console.log(data2);
			//console.log(data2);
			if(data2 == "yes")
			{
			console.log('in if');
			$.fancybox.hideLoading();
			$('#username').addClass('invalid');
			$('#username').removeClass('valid');
			
			//$.fancybox.resize();
			}
			else
			{	
			$('#username').addClass('valid');
			$('#username').removeClass('invalid');	//$.	// $.fancybox(data);
			}
			
			}
		});
		$.fancybox.hideLoading();
});

$("#submit_setting").click(function() {

  $.ajax({
			type	: 'POST',
			cache	: false,
			dataType: "json",
			url		: $('#frmChangeSetting').attr('action'),
			data	: $('#frmChangeSetting').serializeArray(),
			success	: function(data) {
			
			if(data.status){
			setSuccess('#frmChangeSetting .msg', data.status_msg);
			} else {
			setError('#frmChangeSetting .msg', data.status_msg);
			}
			
			
			}
			
		});
});

$("#submit_password").click(function() {

var cPassword = $("#cPassword").val();
var nPassword = $("#nPassword").val();
var nPassword2 = $("#nPassword2").val();
var status = 1;
    
	///////////
	
	if ($('#cPassword').val().length < 1 || $('#nPassword').val().length < 1 || $('#nPassword2').val().length < 1) {
			
			$('#edit-profile-box .msg').removeClass('success');
			$('#edit-profile-box .msg').addClass('error');
			$('#edit-profile-box .msg').append('<li/>').html('Please retry, enter data');
			$('#edit-profile-box .msg').fadeIn();
			//$.fancybox.resize();
			return false;
		}
	else{
		$.fancybox.showLoading();
    $.ajax({
			type	: 'POST',
			cache	: false,
			url		: $('#frmChangePassword').attr('action'),
			data	: $('#frmChangePassword').serializeArray(),
			success	: function(data2) {
			console.log(data2);
			obj = JSON.parse(data2);
			console.log(obj.status);
			
			//console.log(data2);
			if(obj.status == 1 || obj.status == 0)
			{
				$('#edit-profile-box .msg').removeClass('success');
				$('#edit-profile-box .msg').addClass('error');
				$('#edit-profile-box .msg').append('<li/>').html(obj.status_msg);
				$('.msg').fadeIn();
			return false;
			}
			else if(obj.status == 2)
			{
				$('#edit-profile-box .msg').addClass('success');
				$('#edit-profile-box .msg').append('<li/>').html(obj.status_msg);
				$('#edit-profile-box .msg').fadeIn();
				//$.fancybox.resize();
				// $.fancybox(data);
			}
			
			}
			
		});
		$.fancybox.hideLoading();
}
		
	
	///////////
	
	
	return false;
});


	$('.editProfile').fancybox({
		maxHeight: 500,
		//scrolling   : true,
		 autoSize    : true,
         autoScale   : true,
		 afterClose  : function() { 
          $("#frmChangePassword")[0].reset();
          $('#edit-profile-box .msg').removeClass('success');
          $('#edit-profile-box .msg').removeClass('error');
          $('#edit-profile-box .msg').html('');
		  $('#edit-profile-box .msg').fadeIn();
		  $('.dlgEditProfile .msg').hide();
		
		$('.dlgChangePassword').slideUp();
		$('.dlgsetting').slideUp();
		$('.dlgEditProfile').slideDown();
		$('#change_password').removeClass('active');
		$('#change_setting').removeClass('active');
		$('#edit_profile').addClass('active');
        }
	});
	


	$('.token-input-dropdown').css('z-index', '99999');
	
	$('#edit_profile').addClass('active');
	$('#edit_profile').click(function(){
		$('.dlgEditProfile .msg').hide();
		$('.dlgChangePassword').slideUp();
		$('.dlgsetting').slideUp();
		$('.dlgEditProfile').slideDown();
		$('#change_password').removeClass('active');
		$('#change_setting').removeClass('active');
		$('#edit_profile').addClass('active');
		return false;
	})
	$('#change_password').click(function(){
		$('.dlgChangePassword .msg').hide();
		$('.dlgEditProfile').slideUp();
		$('.dlgsetting').slideUp();
		$('.dlgChangePassword').slideDown();
		$('#edit_profile').removeClass('active');
		$('#change_setting').removeClass('active');
		$('#change_password').addClass('active');
		return false;
	})	
	
	$('#change_setting').click(function(){
		$('.dlgEditProfile').slideUp();
		$('.dlgChangePassword').slideUp();
		$('.dlgsetting').slideDown();
		$('#edit_profile').removeClass('active');
		$('#change_password').removeClass('active');
		$('#change_setting').addClass('active');
		return false;
	})	
	
	$("#demo-input-facebook-theme").tokenInput("<?=base_url();?>manage/auto_group", {
		theme: "facebook",
		zindex: 99999,
		<?php 
		if(isset($intrest_group)): ?>
		prePopulate: /*[
					format sample
                    {id: 123, name: "Slurms MacKenzie"}
                ]*/
		<?php 
		print json_encode($intrest_group);
		endif;
		?>
	});
  //$jC = $.Jcrop('#target-photo');
  
	$('#photoCrop').attr('disabled', true); $('#photoReset').attr('disabled', true);
	
	/*$jC.setOptions({
		aspectRatio : 1,
		minSize : [ 105, 101 ],
    maxSize : [ 105, 101 ],
		onSelect	: updateCoords,
    allowResize:false,
	});
  $jC.setSelect(getRandom());*/
	progress(<?php print $progress;?>, $('#progressBar'));

});


/*function progress(percent, $element) {
		var progressBarWidth = percent * $element.width() / 100;
		$element.find('div').animate({ width: progressBarWidth }, 500).html(Math.round(Number(percent)) + "%&nbsp;");
	}
}*/


$(function() {
    $('#fileUploadPicture').change(function(){
    var ext = $(this).val().split('.').pop().toLowerCase();
    var allow = new Array('gif','png','jpg','jpeg');
    if($.inArray(ext, allow) == -1) {
    alert("Invalid File");
    return false;
    }
    var options = {
    		beforeSend: function() {
    					$.fancybox.showLoading();
    		},
    		type	: 'POST',
    		cache	: false,
    		url		: $('#upload_picture').attr('action'),
    		success	: function(data) {
			  $.fancybox.hideLoading();
			  obj = JSON.parse(data);          					 
			  if(obj.status == 'true'){
				$(".avatar-big").removeAttr("src").attr("src", obj.new_image+'?'+(Math.random()*10000000000000000));
				$(".accounts a").children('img').removeAttr("src").attr("src", obj.new_image+'?'+(Math.random()*10000000000000000));
				var res = obj.new_image.split("/");
				myprofile.profile_pic =  res.pop()+'?'+(Math.random()*10000000000000000);
				$.fancybox.close();            
			  }else{
				//alert('Your profile picture must be less than 10 Mb');
				setError('#profile-img-error', 'Picture must be less than 10 Mb');
				$.fancybox.resize;
			  }
    
    	},
	  	cache: false,
    	contentType: false,
    	processData: false
    };
    $('#upload_picture').ajaxSubmit(options);      
	});
 });  

$(window).bind("load", function() { 
    var timeout = setTimeout(function() {$("img.lazy").trigger("sporty")}, 5000);
}); 

<?php if($profile['id'] == $myprofile['id']):?>

$(window).load(function(){
		var tourSubmitFunc = function(e,v,m,f){
			if(v === -1){
				$.prompt.prevState();
				return false;
			}
			else if(v === 1){
				$.prompt.nextState();
				return false;
			}
			else if(v === 0){
				$.prompt.goToState(0);
				return false;
			}
},
tourStates = [
	{
		title: 'Edit',
		html: 'Write something about yourself',
		buttons: { Next: 1 },
		focus: 0,
		position: { container: '.profile-intro', x: 220, y: 40, width: 300, arrow: 'tc' },
		submit: tourSubmitFunc
	},
	{
		title: 'Profile Picture',
		html: 'Click to change your picture',
		buttons: { Prev: -1, Next: 1 },
		focus: 1,
		position: { container: '.p-lft', x: 30, y: 135, width: 300, arrow: 'tl' },
		submit: tourSubmitFunc
	},
	{
		title: 'Follow',
		html: 'This area shows who you follow and the people who follow you',
		buttons: { Prev: -1, First: 0, Done: 2 },
		focus: 0,
		position: { container: '.right-contents', x: -330, y: 50, width: 300, arrow: 'rt' },
		submit: tourSubmitFunc
	}
];
if(<?php echo $user_prompt;?>)
{
    $.prompt(tourStates);
	var data1 = {'user_id' : myprofile.id, 'feature' : 'Profile'};
	var url = siteurl+"status_update/add_prompt";
	$.ajax({
		url: url,
		data: data1,
		async: false,
		dataType: "json",
		type: "POST",
		success: function(data){

		}
	});
}	
});

<?php endif; ?>

</script>
</body>
</html>
