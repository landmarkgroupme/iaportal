<div class="pop-win">
	<div class="pop-win-header">
		Add a Link
		<span><a href="#"><img class="pop-win-close" src="<?=base_url()?>/images/ico-50.jpg" alt="close-win" /></a></span>
	</div>
	<p class="error-msg"></p>
	<form action="#" method="post">
		<div class="login-field-box">
			<div class="login-label">
				<label for="txturl">Enter the URL : <span>Ex http://google.com</span></label>
			</div>
			<div class="login-field">
				<input type="text" name="txturl" id="txturl" value="" />
			</div>
		</div>
		<div class="login-field-box">
			<div class="login-label">
				<label for="txturltitle">Give URL Title : <span>Ex Google Homepage</span></label>
			</div>
			<div class="login-field">
				<input type="text" name="txturltitle" id="txturltitle" value="" />
			</div>
		</div>
	<input class="fav-link-button" id="fn-fav-link" type="image" src="<?=base_url()?>images/btn-save-link.jpg"/>
	</form>
</div>