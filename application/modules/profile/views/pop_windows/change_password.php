<div class="pop-win">
	<div class="pop-win-header">
		Edit Password
		<span><a href="#"><img class="pop-win-close" src="<?=base_url()?>/images/ico-50.jpg" alt="close-win" /></a></span>
	</div>
	<p class="error-msg"></p>
  <p class="error-msg done-msg" style="color:#2a8c3c;"></p>
	<form action="#" method="post">
		<div class="login-field-box">
			<div class="login-label">
				<label for="txtpassword">New Password</label>
			</div>
			<div class="login-field">
				<input type="password" name="txtpassword" id="txtpassword" value="" />
			</div>
		</div>
		<div class="login-field-box">
			<div class="login-label">
				<label for="txtcnfpassword">Confirm Password</label>
			</div>
			<div class="login-field">
				<input type="password" name="txtcnfpassword" id="txtcnfpassword" value="" />
			</div>
		</div>
	<input class="change-pass-button" id="fn-change-pass" type="image" src="<?=base_url() ?>images/btn-change-pass.jpg"/>
	</form>
</div>