<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/all.css" />
	<script type="text/javascript" src="<?php echo base_url(); ?>js/ie-hover-ns-pack.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/addclass.js"></script>
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/lt7.css" /><![endif]-->
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
	  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url()."people"?>">People</a></li>
					<li>Company Directory</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2>Company Directory</h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li class="active"><a href="#"><span><em>All People</em></span></a></li>
							<li><a href="#"><span><em>My Contacts</em></span></a></li>
						</ul>
					</div>
				</div>
				<!-- alphabet sorting -->
				<ul class="sorting">
					<li class="active"><a href="#">All</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/a'); ?>">a</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/b'); ?>">b</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/c'); ?>">c</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/d'); ?>">d</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/e'); ?>">e</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/f'); ?>">f</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/g'); ?>">g</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/h'); ?>">h</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/i'); ?>">i</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/j'); ?>">j</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/k'); ?>">k</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/l'); ?>">l</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/m'); ?>">m</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/n'); ?>">n</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/o'); ?>">o</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/p'); ?>">p</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/q'); ?>">q</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/r'); ?>">r</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/s'); ?>">s</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/t'); ?>">t</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/u'); ?>">u</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/v'); ?>">v</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/w'); ?>">x</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/x'); ?>">w</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/y'); ?>">y</a></li>
					<li><a href="<?php echo site_url('people/alphabetical/z'); ?>">z</a></li>
				</ul>
				<!-- advanced search form -->
				<form action="<?php echo site_url('people/search'); ?>" method="post" class="advanced-search">
					<fieldset>
						<div class="container">
							<a href="#" class="search-options">Search options</a>
							<div class="search-w">
								<input type="text" class="text" name="term" value="Search for people" title="Search for people" />
								<input type="image" class="btn-search" name="search" value="search" src="images/btn-search-advanced.gif" alt="Search!" />
							</div>
						</div>
						<div class="checkboxes">
							<div class="columns">
								<div class="holder1">
									<h3>Concepts <a href="#">all,</a> <a href="#">none</a></h3>
									<div class="container">
									<?php foreach ($concepts as $conceptcol) : ?>
										<div class="column">
										<?php foreach ($conceptcol as $concept) : ?>
											<div class="row">
												<input type="checkbox" id="concept<?php echo $concept['id']; ?>" name="concept[]" value="<?php echo $concept['id']; ?>" />
												<label for="concept<?php echo $concept['id']; ?>"><?php echo $concept['name']; ?></label>
											</div>
										<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
									</div>
								</div>
								<div class="holder2">
									<h3>Locations <a href="#">all,</a> <a href="#">none</a></h3>
									<div class="container">
									<?php foreach ($countries as $countrycol) : ?>
										<div class="column">
										<?php foreach ($countrycol as $country) : ?>
											<div class="row">
												<input type="checkbox" id="country<?php echo $country['id']; ?>" name="country[]" value="<?php echo $country['id']; ?>" />
												<label for="country<?php echo $country['id']; ?>"><?php echo $country['name']; ?></label>
											</div>
										<?php endforeach; ?>
										</div>
									<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
				<!-- peoples list -->
				<div class="people-box">
					<span class="showing">Showing <strong><?php echo "$first &ndash; $last"; ?></strong> of <strong><?php echo $total; ?></strong> people</span>
					<ul>
					<?php foreach ($people as $person) : ?>
						<li class="vcard">
							<div class="visual">
								<img src="images/img<?php echo $person['id']; ?>.jpg" alt="<?php echo 'Img: ' . $person['id']; ?>" width="53" height="53" />
							</div>
							<div class="main-info">
								<a href="<?php echo site_url('people/profile/' . $person['id']); ?>" class="fn url"><?php echo $person['first_name'] . ' ' . $person['last_name']; ?></a>
								<span><?php echo $person['designation'] . ', ' . $person['concept']; ?></span>
								<span class="adr"><span class="locality"><?php echo $person['location']; ?></span>, <span class="country-name"><?php echo $person['territory']; ?></span></span>
							</div>
							<div class="contact-info">
								<span><?php echo $person['extension']; ?></span>
								<span class="tel"><?php echo $person['phone']; ?></span>
								<a href="mailto:<?php echo $person['email']; ?>" class="email"><?php echo $person['email']; ?></a>
							</div>
							<ul>
								<li><a href="#" class="add-contact">Add to My Contacts</a></li>
								<li><a href="#" class="follow">Follow Updates</a></li>
							</ul>
						</li>
					<?php endforeach; ?>	
					</ul>
					<!-- pagination -->
					<div class="paging">
						<?php echo $this->pagination->create_links(); ?>
						<span class="showing">Showing <strong><?php echo "$first &ndash; $last"; ?></strong> of <strong><?php echo $total; ?></strong> people</span>
					</div>
				</div>
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- user box -->
				<div class="user-box user-box-type2">
					<div class="photo-box">
						<span><img src="images/img06.jpg" alt="image description" width="89" height="86" /></span>
						<a href="#">Change your photo</a>
					</div>
					<div class="info vcard">
						<span>Welcome,</span>
						<a href="#" class="name fn url"><?php echo $myprofile['first_name'] . ' ' . $myprofile['last_name']; ?></a>
						<ul>
							<li><a href="<?php echo site_url('people/myprofile/' . $myprofile['id']); ?>">View your Profile</a></li>
							<li><a href="#">Add your specialities</a></li>
							<li><a href="#">Add your favorite links</a></li>
						</ul>
						<!-- progress bar -->
						<span class="progress"><span></span></span>
						<em>Profile is <strong>75%</strong> complete</em>
					</div>
				</div>
				<!-- sidebar box -->
				<div class="box open">
					<div class="heading">
						<h2>My Reminders</h2>
						<a href="#" class="open-close">Open Close</a>
						<a href="#" class="edit">Edit</a>
					</div>
					<div class="box-content">
						<ul>
							<li><a href="#" class="events">Upcoming Events (4)</a></li>
							<li><a href="#" class="market">Marketplace Items Listed (12)</a></li>
							<li><a href="#" class="market">Marketplace Items Wanted (2)</a></li>
						</ul>
					</div>
				</div>
				<!-- sidebar box -->
				<div class="box open">
					<div class="heading">
						<h2>My Interest Groups</h2>
						<a href="#" class="open-close">Open Close</a>
						<a href="#" class="edit">Edit</a>
					</div>
					<div class="box-content">
						<!-- my groups list -->
						<ul class="my-groups">
							<li><a href="#" class="group-name">Travel</a><a href="#" class="number">1223 people</a></li>
							<li><a href="#" class="group-name">Photography</a><a href="#" class="number">54 people</a></li>
							<li><a href="#" class="group-name">Reading</a><a href="#" class="number">321 people</a></li>
							<li><a href="#" class="group-name">Swimming</a><a href="#" class="number">76 people</a></li>
							<li><a href="#" class="group-name">Chess</a><a href="#" class="number">27 people</a></li>
							<li><a href="#" class="group-name">Internet</a><a href="#" class="number">54 people</a></li>
							<li><a href="#" class="group-name">Coffee</a><a href="#" class="number">541 people</a></li>
							<li><a href="#" class="group-name">Movies</a><a href="#" class="number">456 people</a></li>
							<li><a href="#" class="group-name">Sailing</a><a href="#" class="number">37 people</a></li>
						</ul>
					</div>
				</div>
				<!-- sidebar box -->
				<div class="box open">
					<div class="heading">
						<h2>Newcomers</h2>
						<a href="#" class="open-close">Open Close</a>
						<a href="#" class="edit">Edit</a>
					</div>
					<div class="box-content">
						<ul class="newcomers">
							<li>
								<div class="visual">
									<img src="images/img23.jpg" alt="image description" width="51" height="51" />
								</div>
								<div class="text vcard">
									<a href="#" class="name fn url">Muzamil Shaikh</a>
									<span>Retail Marketing, Max</span>
									<ul>
										<li><a href="#">Email</a></li>
										<li><a href="#">Add to my contacts</a></li>
									</ul>
								</div>
							</li>
							<li>
								<div class="visual">
									<img src="images/img24.jpg" alt="image description" width="51" height="51" />
								</div>
								<div class="text vcard">
									<a href="#" class="name fn url">Radwan Mousalli</a>
									<span>Brand Manager, Citymax Hotels</span>
									<ul>
										<li><a href="#">Email</a></li>
										<li><a href="#">Add to my contacts</a></li>
									</ul>
								</div>
							</li>
							<li>
								<div class="visual">
									<img src="images/img25.jpg" alt="image description" width="51" height="51" />
								</div>
								<div class="text vcard">
									<a href="#" class="name fn url">Betsy Matthew</a>
									<span>Retailer, Shcemart</span>
									<ul>
										<li><a href="#">Email</a></li>
										<li><a href="#">Add to my contacts</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<a href="<?php echo site_url('auth/logout'); ?>" class="logout" tabindex="14">Logout</a>
				<!-- navigation -->
				<ul id="nav">
					<li><a href="#" class="home" tabindex="2" accesskey="1"><span>Home</span><em></em></a></li>
					<li class="active"><a href="#" tabindex="3"><span>People</span><em></em></a></li>
					<li><a href="#" tabindex="4"><span>Marketplace</span><em></em></a>
						<div class="drop">
							<span class="t"></span>
							<ul>
								<li><a href="#" tabindex="9">All Items</a></li>
								<li><a href="#" tabindex="10">Items for Sale</a></li>
								<li><a href="#" tabindex="11">Items Wanted</a></li>
								<li><a href="#" tabindex="12">Freebies</a></li>
								<li><a href="#" tabindex="13">Your Items</a></li>
							</ul>
							<span class="b"></span>
						</div>
					</li>
					<li><a href="#" tabindex="5"><span>Files</span><em></em></a></li>
					<li><a href="#" tabindex="6"><span>Events</span><em></em></a></li>
					<li><a href="#" tabindex="7"><span>About Landmark</span><em></em></a></li>
					<li><a href="#" tabindex="8" accesskey="2"><span>News &amp; Updates</span><em></em></a></li>
				</ul>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="#" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="#" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="#" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<form action="#" class="search-form">
						<fieldset>
							<div class="category">
								<strong>People</strong>
								<ul>
									<li><a href="#" tabindex="18">All Items</a></li>
									<li><a href="#" tabindex="19">Items for Sale</a></li>
									<li><a href="#" tabindex="20">Items Wanted</a></li>
									<li><a href="#" tabindex="21">Freebies</a></li>
									<li><a href="#" tabindex="22">Your Items</a></li>
								</ul>
							</div>
							<input accesskey="4" tabindex="23" type="text" class="text" value="Search by name, job role or concept.." title="Search by name, job role or concept.." />
							<input tabindex="24" type="image" class="btn-search" src="images/btn-search.gif" alt="Search!"/>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<!-- footer -->
		<div id="footer">
			<ul>
				<li><a href="#">Help &amp; Support</a></li>
				<li><a href="#">Report Abuse</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Terms of Use</a></li>
			</ul>
			<p>&copy; 2010 Landmark Group. All rights reserved.</p>
		</div>
	</div>
</body>
</html>