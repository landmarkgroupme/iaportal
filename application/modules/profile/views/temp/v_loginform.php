<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Login</title>
	</head>
	<body>
		<div class="container">
			<div class="span-24 last">
				<div id="banner"></div>
			</div>
			<div class="prepend-7 span-10 append-7 last">
				<?php 
					echo form_open('auth/validate_user', array('name' => 'login_form', 'id' => 'login_form'));
					echo form_fieldset('Sign In');
						if (isset($login_failed)) {
							echo '<p class="error">Incorrect Login. Please Try Again!</p>';
						}
						echo form_label('User Name: ', 'username');
						echo form_input('username', set_value('username'));
						echo form_label('Password: ', 'password');
						echo form_password('password', set_value('password'));
						echo form_submit('submit', 'Sign In');
					echo form_fieldset_close();
				?>
			</div> 
		</div>
	</body>
</html>