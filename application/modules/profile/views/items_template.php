<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/profile/profile_status_update.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url()."/people/"?>">People</a></li>
					<li><a href="<?=site_url("profile/view_profile/".$profile_id)?>">Profile</a></li>
					<li>Market Items</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?=$profile_name?></h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li><a href="<?=site_url("profile/view_profile/".$profile_id)?>"><span><em>Info</em></span></a></li>
							<li><a href="<?=site_url("profile/status_updates/".$profile_id)?>"><span><em>Status Updates</em></span></a></li>
							<li><a href="<?=site_url("profile/files/".$profile_id)?>"><span><em>Files</em></span></a></li>
							<li class="active"><a href="<?=site_url("profile/items/".$profile_id)?>"><span><em>Items</em></span></a></li>
						</ul>
					</div>
				</div>
				<!-- Market Items-->
				
				<div class="container">
					<div class="your-item-count">Showing <strong><?=$your_item_sale_limit?></strong> - <strong><?=$your_item_sale_offset?></strong> of <strong><?=$your_item_sale_total_count?></strong> items</div>
					<div class="your-item-table-header">Your items for sale</div>
					<table id="list-table" width="100%" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th class="list-table-head"><strong>Item Image</strong></th>
								<th class="list-table-head"><strong>Item Details</strong></th>
								<th class="list-table-head"><strong>Category</strong></th>
								<th class="list-table-head"><strong>Added</strong></th>
								<th class="list-table-head"><strong>Item Price</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($your_items_sale as $list):?>
							<tr>
								<td width="95"><img src="<?=$list['item_image']?>" width="75" height="62" alt="<?=$list['title']?>" /></td>
								<td width="260">
									<div><a class="list-table-title" href="<?=base_url()?>index.php/market_place/items_details/<?=$list['id']?>"><?=$list['title']?></a></div>
									
								</td>
								<td><a class="<?=$list['category_id']?>" href="<?=base_url()?>index.php/market_place/items_category/<?=$list['category_id']?>"><?=$list['category']?></a></td>
								<td><?=$list['added_on']?></td>
								<td><span class="mkt-item-price"><?=$list['item_max_price']?></span> <?=$list['currency']?></td>
							</tr>
						<?php endforeach;?>
						</tbody>
					</table>
					<br /><br />
					<div class="your-item-count">Showing <strong><?=$your_item_requested_limit?></strong> - <strong><?=$your_item_requested_offset?></strong> of <strong><?=$your_item_requested_total_count?></strong> items</div>
					<div class="your-item-table-header">Items you have requested</div>
					<table id="list-table" width="100%" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th class="list-table-head"><strong>Item Image</strong></th>
								<th class="list-table-head"><strong>Item Details</strong></th>
								<th class="list-table-head"><strong>Category</strong></th>
								<th class="list-table-head"><strong>Added</strong></th>
								<th class="list-table-head"><strong>Price Around</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($your_items_requested as $list):?>
							<tr>
								<td width="95"><img src="<?=$list['item_image']?>" width="75" height="62" alt="<?=$list['title']?>" /></td>
								<td width="260">
									<div><a  class="list-table-title" href="<?=base_url()?>index.php/market_place/items_details/<?=$list['id']?>"><?=$list['title']?></a></div>
									
								</td>
								<td><a class="<?=$list['category_id']?>" href="<?=base_url()?>index.php/market_place/items_category/<?=$list['category_id']?>"><?=$list['category']?></a></td>
								<td><?=$list['added_on']?></td>
								<td><span class="mkt-item-price"><?=$list['item_min_price']." ~ ".$list['item_max_price']?></span> <?=$list['currency']?></td>
							</tr>
						<?php endforeach;?>
						</tbody>
					</table>

					<br /><br />
					<div class="your-item-count">Showing <strong><?=$your_item_freebies_limit?></strong> - <strong><?=$your_item_freebies_offset?></strong> of <strong><?=$your_item_freebies_total_count?></strong> items</div>
					<div class="your-item-table-header">Your Freebies</div>
					<table id="list-table" width="100%" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th class="list-table-head"><strong>Item Image</strong></th>
								<th class="list-table-head"><strong>Item Details</strong></th>
								<th class="list-table-head"><strong>Category</strong></th>
								<th class="list-table-head"><strong>Added</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($your_items_freebies as $list):?>
							<tr>
								<td width="95"><img src="<?=$list['item_image']?>" width="75" height="62" alt="<?=$list['title']?>" /></td>
								<td width="260">
									<div><a  class="list-table-title" href="<?=base_url()?>index.php/market_place/items_details/<?=$list['id']?>"><?=$list['title']?></a></div>
									</td>
								<td><a class="<?=$list['category_id']?>" href="<?=base_url()?>index.php/market_place/items_category/<?=$list['category_id']?>"><?=$list['category']?></a></td>
								<td><?=$list['added_on']?></td>
							</tr>
						<?php endforeach;?>
						</tbody>
					</table>
					
				</div>
				
				
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- sidebar box, user box -->
				<?php $this->load->view("includes/useful_link");?>
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/latest_update");?>
				<!-- sidebar box, My Interest Groups -->
				<?php $this->load->view("includes/my_interest_groups");?>
				<!-- sidebar box, Newcomers -->
				<?php $this->load->view("includes/newcomers");?>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=site_url()."/people/"?>" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="<?=site_url()."/status_update/"?>" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=site_url()."/interest_groups/"?>" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>