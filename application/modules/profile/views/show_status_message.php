<?php foreach($messagelist as $msg):?>
	<li class="type2 hentry">
			<div class="frame">
				<span class="visual">
					<img src="<?=base_url();?>images/user-images/105x101/<?=$msg['profile_pic']?>" alt="<?=$msg['first_name']." ".$msg['last_name']?>" width="53" height="53" />
				</span>
				<div class="text">
					<strong class="entry-title"><a href="<?=site_url('profile/view_profile/' . $msg['user_id'])?>"><?=ucfirst(strtolower($msg['first_name']))." ".ucfirst(strtolower($msg['last_name']))?></a><em><?=$msg['designation'].", ".$msg['concept']?></em></strong>
					<p><?=$msg['message']?></p>
					<span><?=$msg['message_time']?></span>
					<ul class="actions">
						<li><a class="msg-reply" id="<?=$msg['reply_user']?>" href="#">Reply</a></li>
						<li><a href="mailto:<?=$msg['email']?>">Email</a></li>
						<?php if($this->session->userdata('userid') == $msg['user_id']):?>
						<li><a class="msg-delete" id="<?=$msg['message_id']?>" href="#">Delete</a></li>
						<?php elseif($this->session->userdata('userid') != $msg['user_id']): ?>
						<li><a class="msg-report" href="#">Report</a></li>
						<li><a class="<?=($msg['following_user_id'])? "msg-unfollow":"msg-follow"?>" rel="<?=$msg['user_id']?>" href="#"><?=($msg['following_user_id'])? "Unfollow":"Follow"?></a></li>
						<?php endif;?>
					</ul>
				</div>
			</div>
	</li>
<?php endforeach;?>