<?php
//Active Navigation Page
$active_tab['photos_active'] = '';
	$active_tab['home_active'] = '';
	$active_tab['people_active'] = 'class="active"';
	$active_tab['market_place_active'] = '';
	$active_tab['files_active'] = '';
	$active_tab['events_active'] = '';
	$active_tab['about_lmg_active'] = '';
	$active_tab['news_update_active'] = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Landmark Intranet</title>
	<?php $this->load->view("include_files/common_files");?>
	<script type="text/javascript" src="<?=base_url();?>js/custom_js/profile/profile_status_update.js"></script>
</head>
<body>
	<!-- wrapper -->
	<div id="wrapper">
  <?php $this->load->view("includes/admin_nav");?>
		<!-- logo -->
		<h1 class="logo vcard"><a href="<?=base_url()?>" class="fn org url" tabindex="1">Landmark Intranet</a></h1>
		<!-- main -->
		<div id="main">
			<!-- content -->
			<div id="content">
				<!-- breadcrumbs -->
				<ul class="breadcrumbs">
					<li><a href="<?=site_url()."/people/"?>">People</a></li>
					<li><a href="<?=site_url("profile/view_profile/".$profile_id)?>">Profile</a></li>
					<li>Files</li>
				</ul>
				<!-- heading -->
				<div class="heading-box">
					<h2><?=$profile_name?></h2>
					<!-- filters list -->
					<div class="filters">
						<strong>Show:</strong>
						<ul>
							<li><a href="<?=site_url("profile/view_profile/".$profile_id)?>"><span><em>Info</em></span></a></li>
							<li><a href="<?=site_url("profile/status_updates/".$profile_id)?>"><span><em>Status Updates</em></span></a></li>
							<li class="active"><a href="<?=site_url("profile/files/".$profile_id)?>"><span><em>Files</em></span></a></li>
							<li><a href="<?=site_url("profile/items/".$profile_id)?>"><span><em>Items</em></span></a></li>
						</ul>
					</div>
				</div>
				<!-- File details -->
				
				<div class="container">
					<div class="pagination-count">Showing <strong><?=$limit?></strong> - <strong><?=$offset?></strong> of <strong><?=$total_count?></strong> files</div>
					<table id="list-table" width="100%" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th class="list-table-head">&nbsp;</th>
								<th class="list-table-head"><strong>File info</strong></th>
								<th class="list-table-head"><strong>Added By</strong></th>
								<th class="list-table-head"><strong>File size</strong></th>
								<th class="list-table-head"><strong>Added</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php $zebra_strip=0; foreach($files_list as $file_attr):if($zebra_strip == 1){$zebra_class = " class='zebra-strip'";$zebra_strip=0;}else{$zebra_class = "";$zebra_strip=1;}?>
							<tr>
								<td <?=$zebra_class?>><img src="<?=$file_attr['file_icon']?>" alt="files image" /></td>
								<td <?=$zebra_class?>><div><a  class="list-table-title" href="<?=site_url('files/files_download/' . $file_attr['unique_id']) ?>"><?=$file_attr['title']?></a></div><div class="files-attr-tags">Tags: <?=$file_attr['file_tags']?></div></td>
								<td <?=$zebra_class?>><a class="grey-shade-1" href="<?=site_url("profile/view_profile/".$file_attr['user_id'])?>"><?= ucfirst(strtolower($file_attr['first_name']))." ".ucfirst(strtolower($file_attr['last_name']))?></a></td>
								<td <?=$zebra_class?>><?php if($file_attr['file_size']) :?><span class="grey-shade-2"><?=$file_attr['file_size']?></span> KB<?php endif;?></td>
								<td <?=$zebra_class?>><span class="grey-shade-2"><?=$file_attr['uploaded_on']?></span></td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					<div class="pagination-footer">
						<div class="pagination-footer-count">Showing <strong><?=$limit?></strong> - <strong><?=$offset?></strong> of <strong><?=$total_count?></strong> files</div>
						<div class="pagination-footer-list">
							<ul>
								<?=$navlinks?>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				
				
			</div>
			<!-- sidebar -->
			<div id="sidebar">
				<!-- sidebar box, user box -->
				<?php $this->load->view("includes/useful_link");?>
				<!-- sidebar box, My Reminders -->
				<?php $this->load->view("includes/latest_update");?>
				<!-- sidebar box, My Interest Groups -->
				<?php $this->load->view("includes/my_interest_groups");?>
				<!-- sidebar box, Newcomers -->
				<?php $this->load->view("includes/newcomers");?>
			</div>
		</div>
		<!-- header -->
		<div id="header">
			<div class="header-holder">
				<!-- navigation -->
				<?php $this->load->view("includes/navigation",$active_tab);?>
				<div class="head-bar">
					<!-- sub navigation -->
					<ul class="subnav">
						<li class="active"><a href="<?=site_url()."/people/"?>" tabindex="15"><span>Company Directory</span></a></li>
						<li><a href="<?=site_url()."/status_update/"?>" tabindex="16"><span>Status Updates</span></a></li>
						<li><a href="<?=site_url()."/interest_groups/"?>" tabindex="17"><span>Interest Groups</span></a></li>
					</ul>
					<!-- header search form -->
					<?php $this->load->view("includes/search_form");?>
				</div>
			</div>
		</div>
		<!-- footer -->
		<?php $this->load->view("includes/footer");?>
	</div>
</body>
</html>