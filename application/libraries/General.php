<?php

class General extends MX_Controller {

	function __construct() {
		parent::__construct();

		date_default_timezone_set("Asia/Dubai");

		
		$this->load->helper('auth');
		auth();

		$this->load->model('Activities');
		$this->load->model('User');
		$this->load->model('SurveyUser');
		//$my_user = $this->session->userdata("logged_user");
		$this->load->helper('cookie');

		$this->load->model('Activities');
		$this->load->model('User');
		$this->load->model('SurveyUser');
		$my_user = $this->session->userdata("logged_user");

		$data['myprofile'] = $this->User->getByUserID($my_user['userid']);
		$data['myprofile']['total_notifications'] = $this->Activities->countUserActivities($my_user['userid']);
		$data['myprofile']['has_surveys'] = $this->SurveyUser->countUserSurveys($my_user['userid']);


		$this->load->model('Role');
		$permission_roles = array();
		foreach($this->Role->getRoleKeys() as $role)
		{
			$permission_roles[$role['key']] = $role['id'];
		}
		$data['permission_roles'] = $permission_roles;
		$this->load->vars($data);

		 if ($this->session->userdata('submit_form') == true)  { 
		        $this->session->set_userdata('submit_form' , false );
		        $this->load->view('login',$data);
		        exit;
		  }
		

		
	}

}

/* End of file general.php */
/* Location: ./system/application/controllers/general.php */