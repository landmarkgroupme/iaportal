<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
use com\google\i18n\phonenumbers\PhoneNumberUtil;
use com\google\i18n\phonenumbers\PhoneNumberFormat;
use com\google\i18n\phonenumbers\NumberParseException;

require_once(APPPATH . 'modules/libphone/PhoneNumberUtil.php');

/**
 * Intranet Library
 *
 * @author        Aqeel Ahmad
 * @link        http://intranet.landmarkgroup.com/aqeel.phool
 */
class Intranet
{

    function generateUniqueToken($number = 6)
    {
        $alphabets = range('a', 'z');
        $numbers = range(0, 9);
        $arr = array_merge($alphabets, $numbers);
        $token = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $token .= $arr[$index];
        }

        //return $token;
        if ($this->isAlreadyTaken($token)) {
            return generateUniqueToken($number);
        } else {
            return $token;
        }
    }

    function isAlreadyTaken($token)
    {
        if (isset($token) && $token) {
            $CI =   & get_instance();
            $CI->db->select('id');
            $CI->db->from('ci_users');
            $CI->db->where('reset_password_token', $token);
            $query = $CI->db->get();
            $query->result();

            if ($query->num_rows() > 0) {
                return true;
            }
        }
        return false;
    }

    function filterInput($text)
    {
        $text = strip_tags($text);
        return $text;
    }

    /**
     * @param $phoneUtil Object to validate the mobile/phone
     * @param $mobileNumberStr Mobile/Phone Number
     * @param $countryKey Country Code
     * @return string Formatted PhoneNumber
     */
    function formatPhoneNumber($phoneUtil, $mobileNumberStr, $countryKey)
    {
        $formattedNumber = $mobileNumberStr;
        if ((!empty($mobileNumberStr)) && (!empty($countryKey))) {

            $mobileNumberProto = $phoneUtil->parseAndKeepRawInput($mobileNumberStr, $countryKey);
            $isValid = $phoneUtil->isValidNumber($mobileNumberProto);
            if ($isValid) {
                $formattedNumber = $phoneUtil->format($mobileNumberProto, PhoneNumberFormat::INTERNATIONAL) . PHP_EOL;
            }
        }
        return $formattedNumber;
    }
}

/* End of file Intranet.php */
/* Location: ./application/libraries/Intranet.php */