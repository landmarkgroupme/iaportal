<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Form Validation Exxtended Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Validation
 * 
 */
class MY_Form_validation extends CI_Form_validation{
    
    function run($module = '', $group = ''){
        (is_object($module)) AND $this->CI = &$module;
            return parent::run($group);
    }
    
} 
// END Form Validation Extended Class

/* End of file Form_validation.php */
/* Location: ./application/libraries/Form_validation.php */