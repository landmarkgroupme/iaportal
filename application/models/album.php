<?php
/**
  * This is a Album model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Album extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('alb_albums', 'aid');
	}

  /**
  * returns an Album against its ID
  * @return array
  */
  
  public function remove($id)
  {
    $this->db->where('aid', $id);
    if($this->db->delete($this->table))
    {
      return true;
    } else {
      return false;
    }
  }
  
    function getLike($status_id, $user_id) {
	$this->db->select('u.id, u.first_name, u.email, u.reply_username');
    $this->db->from('ci_users u');
	$this->db->join('ci_likes b', 'b.user_id = u.id', 'left');
    $this->db->where('b.status_id', $status_id);
	$this->db->where('b.status_type', 'Albums');
	$this->db->where('b.user_id !=', $user_id);
	$this->db->order_by('b.id', 'DESC');
	$this->limits('2','0');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function get($id)
  {
    if(!is_int($id))
    {
      return 'Not a valid Id';
    }
    $this->db->select('a.aid, a.title, b.ctime, b.filepath, count(b.pid) as total_pic, thumb.filename, a.total_likes');
    $this->db->from($this->table.' a');
    $this->db->join('alb_pictures b', 'a.aid = b.aid');
    $this->db->join('alb_pictures thumb', 'a.thumb = thumb.pid');
    $this->db->join('alb_categories c', 'a.category = c.cid');
    $this->db->where('a.aid', $id);
    $query = $this->db->get();

    $album = $query->result_array();
    $album = $album[0];
    //querying this table separately to enhance performance
    $this->db->select('value, name');
    $this->db->from('alb_config');
    $this->db->where_in('name', array('ecards_more_pic_target', 'fullpath', 'thumb_pfx'));

    //this mapping is created to keep the same response as it was in v1
    $maps = array('ecards_more_pic_target' => 'image_path', 'fullpath' => 'sub_dir', 'thumb_pfx' => 'thumb');
    $query = $this->db->get();
    $data = array();
    foreach($query->result() as $conf)
    {
      $data[$maps[$conf->name]] = $conf->value;
    }
    $data = array_merge($album, $data);
    return $data;
  }
  
   function getIdByTitle($title)
  {
    $this->db->select($this->primary_key);
    $query = $this->db->get_where($this->table, array('title' => $title));
    if($data = $query->result_array())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0][$this->primary_key];
      }
    }
    return false;
  }
  
  function getAlbumData($id,$user_id){
  
  $sql= "(SELECT
                a.aid as id,
                FROM_UNIXTIME(b.ctime) as publish_date,
                a.description  as description,
                a.title as title,
                NULL as album_id,
                'album' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = a.aid and lll.status_type ='Albums' and lll.user_id = '$user_id' limit 1) as you_like,
                a.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                NULL as object_id,
                NULL as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
                NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                albumcon.thumbnail as thumbnail,
                albumcon.name as concept_name,
                albumcon.slug as slug,
				NULL as path,
				'Albums' as content_type
              FROM
                alb_albums a,
                alb_pictures b,
				ci_master_concept albumcon
              WHERE
                a.aid = '$id' AND visibility = 0 AND
                a.aid = b.aid AND
				albumcon.id = a.concept_id
              GROUP BY b.aid
            )";
			
	$sql .= " order by publish_date DESC LIMIT 1";
    $query= $this->db->query($sql);
    $data =  $query->result_array();
	$return_data = array();
	 foreach ($data as $index => $item) {
		if($item['category'] == 'album')
          {
            $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
            $item['display_name'] = $item['concept_name'];
            $item['username'] = 'concepts/'.$item['slug'];
          }
		  $return_data[] = $item;
		 }
	return $return_data;
  }

}