<?php
/**
  * This is a Concept model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Concept extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_concept');
	}


  /**
  * Returns All or specific number of Entities from Model
  * @return array
  */
  public function getConcepts($limit = null, array $sortby = null, $user_id = null, $status = 'all')
  {
  	$sql = 'con.*';
  	if(isset($user_id))
  	{
  		$sql .= ', mem.object_id as member';
  	}
  	$this->db->select($sql);
  	$this->db->from($this->table.' con');
    if(isset($status) && $status != 'all')
    {
      $this->db->where('con.status', $status);
    }


  	if(isset($user_id))
  	{
  		$this->db->join('ci_map_objects mem', "parent_type = 'Concept' and parent_id = con.id and object_type = 'User' and object_id = $user_id", 'left', false);
  	}
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit,$offset);
    }
    if(isset($sortby))
    {
      foreach($sortby as $column => $order )
      {
        $this->db->order_by($column, $order);
      }

    } else {
      $this->db->order_by('name', 'Asc');
    }

    $query = $this->db->get();
    return $query->result_array();
  }

  function getConcept($concept_id, $user_id)
  {
  	$this->db->select('con.*, mem.object_id as member');
  	$this->db->from($this->table.' con');
  	$this->db->join('ci_map_objects mem', "parent_type = 'Concept' and parent_id = con.id and object_type = 'User' and object_id = $user_id", 'left', false);
  	$this->db->where('con.id', $concept_id);
  	$query = $this->db->get();
  	foreach ($query->result() as $concept) {
  		return (array)$concept;
  	}
  	return false;
  }

  function getConceptBySlug($slug, $user_id)
  {
    $this->db->select('con.*, mem.object_id as member');
    $this->db->from($this->table.' con');
    $this->db->join('ci_map_objects mem', "parent_type = 'Concept' and parent_id = con.id and object_type = 'User' and object_id = $user_id", 'left', false);
    $this->db->where('con.slug', $slug);
    $query = $this->db->get();
    foreach ($query->result() as $concept) {
      return (array)$concept;
    }
    return false;
  }
  
  function getConceptManagers($concept_id)
  {
    $this->db->select('u.id, u.reply_username, u.display_name, u.profile_pic');
    $this->db->from('ci_users u');
    $this->db->join('ci_object_permissions p', 'p.user_id = u.id');
    $this->db->where('p.object_id', $concept_id);
    $this->db->where('p.object_type', 'Concept');
    $query = $this->db->get();
    return $query->result_array();
  }
  function getCountConceptManagers($concept_id)
  {
  	$this->db->select('count(u.id) as managers');
  	$this->db->from('ci_users u');
	$this->db->join('ci_object_permissions p', 'p.user_id = u.id');
    $this->db->where('p.object_id', $concept_id);
	$this->db->where('p.object_type', 'Concept');
	$result = $this->db->get();
	//$result = $query->result_array();
	$data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    return $data;
  }

  function isConceptManager($concept_id, $user_id)
  {
    $this->db->select('u.id');
    $this->db->from('ci_users u');
    $this->db->join('ci_object_permissions p', 'p.user_id = u.id');
    $this->db->where('p.object_id', $concept_id);
    $this->db->where('p.object_type', 'Concept');
    $this->db->where('u.id', $user_id);
    $query = $this->db->get();
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        //return user id if manager it is manager
        return (array)$data[0];
      }
    }
    return false;

  }

  public function getConceptNames($limit = null, array $sortby = null, $offset = null, $name = null, $format = null)
  {
    $this->db->where('status', 'active');
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit,$offset);
    }
    if(isset($sortby))
    {
      foreach($sortby as $column => $order )
      {
        $this->db->order_by($column, $order);
      }

    } else {
      $this->db->order_by('name', 'Asc');
    }

  if(isset($name))
  {
    $this->db->like('name', $name, 'both');
   $query = $this->db->get($this->table);
  }
  else
  {
    $query = $this->db->get($this->table);
  }
  if(isset($format))
  {
    if($format == 'array')
    {
      return $query->result_array();
    }
  }
    return $query->result();
  }

  function increaseFollowers($concept_id)
  {
      $this->db->where('id', $concept_id);
      $this->db->set('total_members', ' (total_members + 1)', FALSE);
      $this->db->update($this->table);
  }

  function decreaseFollowers($concept_id)
  {
      $this->db->where('id', $concept_id);
      $this->db->set('total_members', ' (total_members - 1)', FALSE);
      $this->db->update($this->table);
  }
  
    public function getConceptsForJoin($user_id)
  {
  	
  	$sql = 'con.*';
  	if(isset($user_id))
  	{
  		$sql .= ', mem.object_id as member';
  	}
  	$this->db->select($sql);
  	$this->db->from($this->table.' con');
    if(isset($status) && $status != 'all')
    {
      $this->db->where('con.status', $status);
    }


  	if(isset($user_id))
  	{
  		$this->db->join('ci_map_objects mem', "parent_type = 'Concept' and parent_id = con.id and object_type = 'User' and object_id = $user_id", 'left', false);
  	}
	$this->db->where('con.status', 'active');
	$this->db->where('con.is_open', 'yes');
  	$this->db->order_by('con.total_members', 'Desc');
    $query = $this->db->get();
    $data = $query->result_array();
	$result = array();
		foreach($data as $key => $value)
		{
			if($value['member'] != '')
			{
				unset($data[$key]);
				continue;
			}
			$result[] = $value;
		}
	return $result;
  }

  function getNamesArray() {
    $this->db->select('id, name');
    $this->db->from($this->table);
    $query = $this->db->get();
    $desigs = $query->result();
    
    $data = array();
    foreach($desigs as $des){
      $data[$des->id] = $des->name;
    }
    return $data;
  }
  function getConceptByGroup($group_id = false)
  {
    $sql = 'con.*';
  	$this->db->select($sql);
  	$this->db->from($this->table.' con');
    if(isset($group_id) && !empty($group_id))
    {
      $this->db->join('ci_files f',"f.concept_id = con.id");
      $this->db->join('ci_map_objects map',"map.object_type = 'File' AND map.parent_type='Group' AND f.id = map.object_id",null,false);
      $this->db->where('map.parent_id',$group_id);
    }
    $this->db->group_by('con.id');
    $query = $this->db->get();
    $data = $query->result();
    return $data;    
  }
}