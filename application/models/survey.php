<?php
/**
  * This is a Department model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Survey extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_surveys');
	}
  public function getConcepts($limit = null, array $sortby = null, $user_id = null, $status = 'all')
  {
  	$this->db->select('con.*');
  	$this->db->from('ci_master_concept  con');
	$this->db->join('ci_master_outlets o',"o.concept_id = con.id");
	$this->db->group_by('con.id');
    $this->db->order_by('name', 'Asc');
    $query = $this->db->get();
    return $query->result_array();
  }
  function getSurveyByUser($user_id){
    $this->db->select('s.*');
    $this->db->from($this->table. ' s');
    if($user_id)
    {
      $this->db->join('v2_survey_users u', 'u.survey_id = s.id');
      $this->db->where('u.user_id', $user_id);
    }
    $this->db->order_by('s.create_dttm','DESC');
    $query = $this->db->get();
    return $query->result();
  }
  
  function getSurveyById($id = false,$limit = 0,$offset = 0){
    $this->db->select('s.*');
    $this->db->from($this->table. ' s');
    if($id)
    {
      $this->db->where('s.id', $id);
    }
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit,$offset);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function removeSurvey($id){
    return $this->remove($id);
  }
  function getSurveybyFilter($filter)
  {
    $this->db->select('s.*');
    $this->db->from($this->table. ' s');
    
    if($filter['isAdmin'] == false && (isset($filter['user_id']) && !empty($filter['user_id'])))
    {
      $this->db->join('v2_survey_users u', 'u.survey_id = s.id');
      $this->db->where('u.user_id', $filter['user_id']);
    }
    if(isset($filter['concept']) && !empty($filter['concept']))
    {
      $this->db->where('s.concept_id', $filter['concept']);
    }
    if(isset($filter['date']) && !empty($filter['date']))
    {
      $where = 'YEAR(s.create_dttm) = \''.$filter['date'].'\' ';
      $this->db->where($where);
    }
    $this->db->where('s.status','active');
    $this->db->order_by('s.create_dttm','DESC');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
  function getSurveyConceptNames()
  {
    $this->db->select('c.*');
    $this->db->from('ci_master_concept c');
    $this->db->join('ci_master_outlets as o', 'o.concept_id = c.id');
    $this->db->group_by('c.id');
    $this->db->order_by('c.name','DESC');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
  function getSurveyConcept(){
    $this->db->select('c.*');
    $this->db->from(' v2_survey_concept c');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
}