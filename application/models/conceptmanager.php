<?php
/**
  * This is a Outlet model.
  *
  * @author  Sandeep Kumbhatil
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class ConceptManager extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_survey_concept');
	}
  function getConceptbyFilter($filters)
  {
    $query = $this->db->get_where($this->table, $filters);
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return $data;
      }
    }
  }
  function getAllConceptManager($name){
    $this->db->select('u.first_name,u.last_name,u.id');
    $this->db->from($this->table. ' c');
    if($name)
    {
      $this->db->join('ci_users u', 'u.id = c.user_id');
      $this->db->like('reply_username', $name, 'both');
      $this->db->or_like('first_name', $name);
      $this->db->or_like('last_name', $name);
    }
    $this->db->group_by('u.id');
    $query = $this->db->get();
    return $query->result();
  }
  function addConcept($data)
  {
    $filter = array('user_id'=>$data['user_id']);
    $count = $this->countAll($filter);
    if($count > 0)
    {
      return false;
    }
    else
    {
      return $this->add($data);
    }
  }
}