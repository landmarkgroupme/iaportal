<?php

/*
 * This is a common model for small widgets/gadgets
 * "gn" stands for general
 * */

Class General_model extends CI_Model {

  function General_model() {
    parent::__construct();
  }

  //Newcommers list
  function gn_newcomers($user_id, $show_limit) {
    $sql = "SELECT
							a.id,
							CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                                                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
							a.email,
							a.profile_pic,
							b.name as designation,
							c.name as concept,
							(SELECT COUNT(user_id) FROM ci_users_contacts WHERE contact_users_id = a.id  AND contact_status = 1 AND user_id = ?)as contact_user
						FROM
							ci_users a,
							ci_master_designation b,
							ci_master_concept c
						WHERE
							a.designation_id = b.id AND
							a.concept_id = c.id
						ORDER BY a.joinedon desc LIMIT ?";
    $result = $this->db->query($sql, array($user_id, $show_limit));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  // Interest Groups list
  function gn_intrest_groups($user_id, $show_limit) {
    $show_limit = 20;//Override custom limit
    $sql = "SELECT
							a.id ,
							name as interest_group,
							(SELECT count(b.user_id) FROM ci_map_intrest_users b where b.intrest_groups_id = a.id ) as total_members
						FROM
							ci_interest_groups a,
							ci_map_intrest_users b
						WHERE
							a.group_status = 1 AND
							b.user_id = ? AND
							a.id = b.intrest_groups_id
							GROUP BY b.intrest_groups_id
						ORDER BY id desc LIMIT ?";
    $result = $this->db->query($sql, array($user_id, $show_limit));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //favourite Links
  function gn_favourite_links($user_id, $show_limit) {
    $sql = "SELECT
              id ,
              link_name,
              link
            FROM
              ci_fav_links
            WHERE
              user_id = ? AND
              status  = 1
            ORDER BY id desc LIMIT ?";
    $result = $this->db->query($sql, array((int) $user_id, (int) $show_limit));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //Event Reminders for user
  function gn_my_event_reminders($user_id, $time) {
    $sql = "SELECT id as total_event FROM ci_event_reminder WHERE event_date_time > ? AND user_id = ? AND status = 1 GROUP BY id";
    $result = $this->db->query($sql, array((int) $time, (int) $user_id));
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  //General Department list
  function gn_get_department() {
    $sql = "SELECT id,name FROM ci_master_department WHERE 1 ORDER BY name";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
  //General Territory list
  function gn_get_territory() {
    $sql = "SELECT id,name FROM ci_master_territory ORDER BY name";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
//General Concept list
  function gn_get_concepts() {
    $sql = "SELECT id,CONCAT(UCASE(SUBSTRING(`name`, 1,1)),LOWER(SUBSTRING(`name`, 2))) as name,show_in_offer FROM ci_master_concept ORDER BY name";
    $result = $this->db->query($sql);
    $data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }
}