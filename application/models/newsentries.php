<?php
/**
  * This is a News model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/entry.php';
Class NewsEntries extends Entry {
	function __construct() {
		parent::__construct();
    $this->mt_category = 'news';
	}

  /**
  * get List of News
  *
  * Names in v1: news_model -> news_get_all
  * @return array
  */
  function getNewsList($limit = 10, $offset = 0, $concept = null)
  {
    return $this->getEntries($this->mt_category, $limit, $offset, $concept);
  }

   function getLike($status_id, $user_id, $type) {
	$this->db->select('u.id, u.first_name, u.email, u.reply_username');
    $this->db->from('ci_users u');
	$this->db->join('ci_likes b', 'b.user_id = u.id', 'left');
    $this->db->where('b.status_id', $status_id);
	$this->db->where('b.status_type', $type);
	$this->db->where('b.user_id !=', $user_id);
	$this->db->order_by('b.id', 'DESC');
	$this->limits('2','0');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function get_all_data($id)
  {
    if(!is_int($id))
    {
      return 'Not a valid Id';
    }
    $query = $this->db->get_where($this->table, array('entry_id' => $id));
    return $query->result();
  }
  /**
  * get News Details
  *
  * Names in v1: news_model -> news_get_details
  * @return array
  */
  function getNewsDetails($news_id)
  {
    return $this->get($news_id);
  }
  
  function getEntryData($entry_id, $user_id){

    $sql = "(SELECT
              e.entry_id as id,
              e.entry_created_on as publish_date,
              e.entry_text as description,
			  e.entry_text_more as excerpt,
              e.entry_title as title,
              e.entry_excerpt as album_id,
              c.category_basename as category,
              NULL as file_unique_id,
              NULL as file_size,
              NULL as following_user_id,
              (select id from ci_likes lll where lll.status_id = e.entry_id and lll.user_id = '$user_id' limit 1) as you_like,
              e.total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as display_name,
              con.thumbnail as profile_pic,
              con.name as concept,
              NULL as object_id,
              NULL as object_type,
              NULL as target_type,
              NULL as target_url,
              NULL as target_name,
               con.slug as concept_url,
              NULL as posted_concept_name,
			  NULL as group_url,
			  NULL as posted_group_name,
              NULL as thumbnail,
              NULL as concept_name,
              NULL as slug,
			  NULL as path
            FROM
              mt_entry e
              join mt_category c on c.category_basename IN('news','announcement','offers') AND c.category_class = 'category'
              join mt_placement p on c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id
              LEFT JOIN ci_master_concept con on con.id = e.concept_id
			  WHERE e.entry_status = 2 AND e.entry_id = $entry_id)";
	$sql .= " order by publish_date DESC";

    $query= $this->db->query($sql);

    $data =  $query->result_array();

       // print_r($data);exit;



        $return_data = array();

        foreach ($data as $index => $item) {
		if($item['category'] == 'album')
          {
            $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
            $item['display_name'] = $item['concept_name'];
            $item['username'] = 'concepts/'.$item['slug'];
          }

          if($item['object_type'] == 'Concept')
          {
		  if($item['category'] == 'photo'){
			$item['profile_pic'] = 'images/concepts/thumbs/'.$item['profile_pic'];
		  } else {
            $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
			}
            $item['display_name'] = $item['concept_name'];
            $item['username'] = 'concepts/'.$item['slug'];
          }
		  elseif($item['object_type'] == 'Group')
          {
            $item['profile_pic'] = 'images/groups/thumbs/'.$item['thumbnail'];
            $item['display_name'] = $item['concept_name'];
            $item['username'] = 'groups/'.$item['slug'];
          }
          elseif($item['object_type'] == 'User')
          {
            $item['profile_pic'] = 'images/user-images/105x101/'.$item['profile_pic'];
            if($item['target_type']== 'Concept')
            {
              $item['target_name']  = $item['posted_concept_name'];
              $item['target_url']   = 'concepts/'.$item['concept_url'];
            }
			if($item['target_type']== 'Group')
            {
              $item['target_name']  = $item['posted_group_name'];
              $item['target_url']   = 'groups/'.$item['group_url'];
            }
          }

		  if($item['category'] == 'files')
			{
				if($item['file_size'])
				{
				$file_info = pathinfo($item['path']);
				$file_name = $file_info['filename'];
				$file_extension = $file_info['extension'];
				$item['view_path'] = base_url() . "files_downloads/".urlencode($file_name).".".$file_extension;
				}
				else
				{
					$item['view_path'] = $item['path'];
				}
			}
          $return_data[] = $item;
        }

        return $return_data;


    //echo $this->db->last_query();

    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data[] = $row;
      }
    }
    $result->free_result();
   /* print_r($data);
    die();*/
    return $data;
  }
  
  

}