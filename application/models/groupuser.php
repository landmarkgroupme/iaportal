<?php
/**
  * This is a GroupUser model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/objectmember.php';
Class GroupUser extends objectmember {
	function __construct() {
		parent::__construct();
	}

  /**
  * get a list of user Groups
  *
  * Names in v1: get_users_interest_groups
  * @return array
  */
  function getUserGroups($user_id, $limit = null, $offset = null)
  {
    $this->db->select('g.id, g.name , g.thumbnail');
    $this->db->from('ci_interest_groups g');
    $this->db->join('ci_map_objects map', "map.parent_id = g.id and map.parent_type = 'Group'");
    $this->db->where('g.group_status', '1');
    $this->db->where('map.object_type', 'User');
    $this->db->where('map.object_id', $user_id);
	$this->db->order_by('g.name', 'ASC');
    $this->limits($limit, $offset);
    $query = $this->db->get();
    return $query->result_array();
  }
  
  /**
  * get a list of user Groups
  *
  * Names in v1: get_users_interest_groups
  * @return array
  */
  function getUserIntrests($user_id)
  {
    $this->db->select('g.id, g.name , g.thumbnail');
    $this->db->from('ci_interest_groups g');
    $this->db->join('ci_map_intrest_users map', "map.intrest_groups_id = g.id");
    $this->db->where('map.user_id', $user_id);
    $this->db->order_by('g.name', 'ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Removes a User from All User Groups
  *
  * Names in v1: remove_user_interest_groups
  * @return array
  */
  function removeUserFromGroups($user_id){
    $this->db->where('object_id', $user_id);
    $this->db->where('object_type', 'User');
    $this->db->where('parent_type', 'Group');
    if($this->db->delete($this->table))
    {
      return true;
    } else {
      return false;
    }

  }

  function countUserGroups($user_id)
  {
    $this->db->select('count(g.id) as total', false);
    $this->db->from('ci_interest_groups g');
    $this->db->join('ci_map_objects map', "map.parent_id = g.id and map.parent_type = 'Group'");
    $this->db->where('g.group_status', '1');
    $this->db->where('map.object_type', 'User');
    $this->db->where('map.object_id', $user_id);
    $this->limits($limit, $offset);

    $query = $this->db->get();
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return 0;
  }


}