<?php
/**
  * This is a Group model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Group extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_interest_groups');
	}

  /**
  * get a list of Groups starting with query name
  *
  * Names in v1: get_all_interest_groups
  * @return array
  */
  function increaseFollowers($group_id)
  {
      $this->db->where('id', $group_id);
      $this->db->set('total_members', ' (total_members + 1)', FALSE);
      $this->db->update($this->table);
  }

  function decreaseFollowers($group_id)
  {
      $this->db->where('id', $group_id);
      $this->db->set('total_members', ' (total_members - 1)', FALSE);
      $this->db->update($this->table);
  }
  
  function findGroupsByName($src_term){
    $this->db->like('name',$src_term , 'after');
    $result=$this->db->get($this->table);
    $data = array();
    if($result->num_rows() > 0){
      foreach($result->result_array() as $row){
        $data[] = $row;
      }
    }
    $result->free_result();
    return $data;
  }

  public function getIntrestGroup($id)
  {
	//$names = array('Frank', 'Todd', 'James');

	if(isset($id))
	{
	$this->db->select('id, name');
	$this->db->where_in('id', $id);
	$query = $this->db->get($this->table);
    return $query->result();
	}
	else
	{
	return false;
	}
  }

  /**
  * Returns All or specific number of Groups
  * @return array
  */
  public function getGroups($limit = null, $offset = null, array $sortby = null, $user_id = null)
  {
  	$sql = 'grp.*';
  	if(isset($user_id))
  	{
  		$sql .= ', mem.object_id as member';
  	}
  	$this->db->select($sql);
  	$this->db->from($this->table.' grp');
  	if(isset($user_id))
  	{
  		$this->db->join('ci_map_objects mem', "parent_type = 'Group' and parent_id = grp.id and object_type = 'User' and object_id = $user_id", 'left', false);
  	}
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit,$offset);
    }
    if(isset($sortby))
    {
      foreach($sortby as $column => $order )
      {
        $this->db->order_by($column, $order);
      }

    } else {
      $this->db->order_by('name', 'Asc');
    }

    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function getGroup($group_id, $user_id = null)
  {
  	$sql = 'grp.*';
  	if(isset($user_id))
  	{
  		$sql .= ', mem.object_id as member';
  	}
  	$this->db->select($sql);
  	$this->db->from($this->table.' grp');
	if(isset($user_id))
  	{
  		$this->db->join('ci_map_objects mem', "parent_type = 'Group' and parent_id = grp.id and object_type = 'User' and object_id = $user_id", 'left', false);
  	}
  	$this->db->where('grp.id', $group_id);
  	$query = $this->db->get();
  	foreach ($query->result() as $group) {
  		return (array)$group;
  	}
  	return false;
	}
  function checkGroupMember($group_id,$user_id)
  {
    $sql = 'grp.id';
  	$this->db->select($sql);
  	$this->db->from($this->table.' grp');
    if(isset($user_id))
  	{
  		$this->db->join('ci_map_objects mem', "parent_type = 'Group' and parent_id = grp.id and object_type = 'User' and object_id = $user_id", null, false);
  	}
  	$this->db->where('grp.id', $group_id);
  	$query = $this->db->get();
    $record = $query->result();
    if(isset($record) && !empty($record))
    {
      return true;
    }
    return false;
  }
 function getGroupManagers($group_id)
  {
  	$this->db->select('u.id, u.reply_username, u.display_name, u.profile_pic');
  	$this->db->from('ci_users u');
	$this->db->join('ci_object_permissions p', 'p.user_id = u.id');
    $this->db->where('p.object_id', $group_id);
	$this->db->where('p.object_type', 'Group');
	$query = $this->db->get();
    return $query->result_array();
  }
  
  function getCountGroupManagers($group_id)
  {
  	$this->db->select('count(u.id) as managers');
  	$this->db->from('ci_users u');
	$this->db->join('ci_object_permissions p', 'p.user_id = u.id');
    $this->db->where('p.object_id', $group_id);
	$this->db->where('p.object_type', 'Group');
	$result = $this->db->get();
	//$result = $query->result_array();
	$data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    return $data;
  }
  
  function getCountGroupMembers($group_id)
  {
  	$this->db->select('count(u.id) as members');
  	$this->db->from('ci_users u');
	$this->db->join('ci_map_objects p', 'p.object_id = u.id');
    $this->db->where('p.parent_id', $group_id);
	$this->db->where('p.parent_type', 'Group');
	$result = $this->db->get();
	//$result = $query->result_array();
	$data = array();
    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $data = $row;
      }
    }
    return $data;
  }
  function saveGroupFileMapping($data){
    $this->db->insert('ci_object_mapping', $data);
  }
  function getGroupByFilter($filter = false)
  {
    $sql = 'grp.*';
    if(isset($filter['user_id']) && !empty($filter['user_id']))
    {
      $sql .= ', mem.object_id as member';
    }
    $this->db->select($sql);
    $this->db->from($this->table.' grp');
    if(isset($filter['user_id']) && !empty($filter['user_id']))
    {
      $this->db->join('ci_map_objects mem', "parent_type = 'Group' and parent_id = grp.id and object_type = 'User' and object_id = $user_id", 'left', false);
    }
    if(isset($filter['alpha']) && !empty($filter['alpha']))
    {
      $this->db->like('grp.name',$filter['alpha'],'after');
    }
    if(isset($filter['exclude']) && !empty($filter['exclude']))
    {
      $this->db->where_not_in('grp.id',$filter['exclude']);
    }
    if(isset($filter['sort']))
    {
      foreach($filter['sort'] as $column => $order )
      {
        $this->db->order_by($column, $order);
      }
    
    } 
    else {
      $this->db->order_by('name', 'Asc');
    }
    $query = $this->db->get();
    return $query->result_array();

  }
  
  public function getGroupsForJoin($user_id)
  {
  	
  	$sql = 'grp.*';
  	if(isset($user_id))
  	{
  		$sql .= ', mem.object_id as member';
  	}
  	$this->db->select($sql);
  	$this->db->from($this->table.' grp');
  	if(isset($user_id))
  	{
  		$this->db->join('ci_map_objects mem', "parent_type = 'Group' and parent_id = grp.id and object_type = 'User' and object_id = $user_id", 'left', false);
  	}
	$this->db->where('grp.group_status', 1);
	$this->db->where('grp.is_open', 'yes');
  	$this->db->order_by('grp.total_members', 'Desc');
    $query = $this->db->get();
    $data = $query->result_array();
	$result = array();
		foreach($data as $key => $value)
		{
			if($value['member'] != '')
			{
				unset($data[$key]);
				continue;
			}
			$result[] = $value;
		}
	return $result;
  }

}