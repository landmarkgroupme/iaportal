<?php
/**
  * This is a Invite model.
  *
  * @author  Sandeep Kumbhatil <sandeep.kumbhatil@intelliswift.co.in>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Invite extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_invitation');
	}
  function fetchAllInvitation($group_id){
    $this->db->select('i.invite_to');
    $this->db->from($this->table.' i');
    $this->db->where('i.object_id', $group_id);
    $this->db->where('i.object', 'Group');
    $query = $this->db->get();
    $userid = array();
    foreach($query->result_array() as $user)
    {
      $userid[$user['invite_to']] = $user['invite_to'];
    }
    return $userid;
  }
  function fetchInvitationByUserGroup($user_id,$group_id)
  {
    $this->db->select();
    $this->db->from($this->table);
    $this->db->where('object_id', $group_id);
    $this->db->where('object', 'Group');
    $this->db->where('invite_to', $user_id);
    $this->db->where('invite_type', 'User');
    $query = $this->db->get();
    $data = $query->result_array();
    if(isset($data) && !empty($data))
    {
      return $data[0];
    }
    else
    {
      return false;
    }
  }
}