<?php
Class Activities extends CI_Model {
  var $table = null;
	function __construct() {
		parent::__construct();
    $this->table = 'ci_activity_log';
	}

  public function getUserStatusUpdates($limit = 20, $offset = 0, $id = NULL, $my_stasuses = FALSE) {

    //underlaying query for reference
    /*
    SELECT
              0 as following_user_id,
              b.id as message_id,
              b.message,
              b.object_id as user_id,
              b.message_time,
              CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
              CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
              c.profile_pic,
              c.email,
              d.name as designation,
              e.name as concept
            FROM
              ci_status_updates b,
              ci_users c,
              ci_master_designation d,
              ci_master_concept e
            WHERE
              c.id = ? AND
              b.status = 1 AND
              b.object_id = c.id AND b.object_type = 'User' AND
              d.id = designation_id AND
              e.id = concept_id AND
              b.id = ?

              (SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.object_id ) as following_user_id

    */

    $columns = "su.id message_id, parent_status_id, total_likes, message, object_id, su.create_dttm, u.profile_pic, u.email, des.name as designation, con.name as concept, u.first_name, u.last_name, CONCAT('') as following_user_id";

    $columns .= ', likes.id liked';

    $this->db->select($columns);
    $this->db->from('ci_status_updates su');
    $this->db->join('ci_users u', "u.id = su.object_id AND su.object_type = 'User'");
    $this->db->join('ci_master_designation des', 'u.designation_id = des.id');
    $this->db->join('ci_master_concept con', 'u.concept_id = con.id');

    if(isset($id)) {
      $this->db->join('ci_users_following_users follow', "follow.user_id = su.object_id and follow.follow_status = 1 and follow.user_id = $id", 'left');
    }
    $this->db->join('ci_likes likes', "likes.status_id = su.id AND likes.user_id = $id", 'left');
    $this->db->where('su.status', 1);
    //$this->db->where('su.parent_status_id', 0);
    if(isset($id) && $my_stasuses) {
        $this->db->where('u.id', $id);
    }
    $this->db->where("su.object_type = 'User'");
    $this->db->limit($offset, $limit);
    $this->db->order_by('create_dttm desc');
    $query = $this->db->get();
    /*echo $this->db->last_query();
    die();*/
    //return $query->result_array();
    $data = array();
    foreach ($query->result_array() as $status) {
      if($status['parent_status_id'] == '0') {
        //its not a comment
        $data[$status['message_id']]['data'] = $status;
      } else {
        $data[$status['parent_status_id']]['comments'][$status['message_id']] = $status;
        ksort($data[$status['parent_status_id']]['comments']);
      }


       # code...
     }
     /*echo '<pre>';
     print_r($data);
     die();*/
     return $data;
  }

  function addActivity($details)
  {
    $this->db->insert($this->table, $details);


    return true;
  }



  function likeUserStatus($liker_user_id, $liked_user_id, $status_id, $post_type = NULL) {
    $details = array();
    $details['object_id'] = $liked_user_id;
    $details['object_type'] = 'User';
	if(isset($post_type) && $post_type != '' )
	{
		 $details['activity_source_type'] = $post_type;
	}

    $details['activity_source'] = $status_id;
    $details['activity_type'] = 'User_Status_Like';

    $details['parent_id'] = $liker_user_id;
    $details['parent_type'] = 'User';
    $this->addActivity($details);
  }

  function getUserActivities($user_id, $limit = 0)
  {
	$this->db->select('l.activity_type, l.dttm as time, liker.reply_username, liker.display_name as fullname, liker.profile_pic,l.notification, l.activity_source, l.parent_type, con_liker.thumbnail, con_liker.name, con_liker.slug, l.activity_source_type');
	$this->db->from($this->table. ' l');
	$this->db->join('ci_users liker', "liker.id = l.`parent_id` and l.`parent_type` = 'User'",'Left');
	$this->db->join('ci_master_concept con_liker', "con_liker.id = l.`parent_id` and l.`parent_type` = 'Concept'",'Left');
	$this->db->join('ci_users you', "you.id = l.`object_id` and l.`object_type` = 'User'");
	$this->db->where('l.object_id', $user_id);
	if($limit > 0)
	{
	$this->db->limit($limit);
	}
	$this->db->order_by('time', 'DESC');
	$query = $this->db->get();
	$results = $query->result_array();
	$notification = array();
	foreach($results as $result)
	{
		if($result['parent_type'] == 'Concept')
		{
			$result['reply_username'] = 'concepts/'.$result['slug'];
			$result['fullname'] = $result['name'];
			$result['profile_pic'] = 'images/concepts/thumbs/'.$result['thumbnail'];
		} else if($result['parent_type'] == 'User'){
			$result['profile_pic'] = 'images/user-images/105x101/'.$result['profile_pic'];
		}
		$notification[] = $result;
	}
	return $notification;

  }
  
  function getUserActivitiesUnviewed($user_id)
  {
	$this->db->select('l.activity_type, l.dttm as time, liker.reply_username, liker.display_name as fullname, liker.profile_pic,l.notification, l.activity_source, l.parent_type, con_liker.thumbnail, con_liker.name, con_liker.slug, l.activity_source_type');
	$this->db->from($this->table. ' l');
	$this->db->join('ci_users liker', "liker.id = l.`parent_id` and l.`parent_type` = 'User'",'Left');
	$this->db->join('ci_master_concept con_liker', "con_liker.id = l.`parent_id` and l.`parent_type` = 'Concept'",'Left');
	$this->db->join('ci_users you', "you.id = l.`object_id` and l.`object_type` = 'User'");
	$this->db->where('l.object_id', $user_id);
	$this->db->where('l.status', 'unviewed');
	$this->db->limit(10);
	$this->db->order_by('time', 'DESC');
	$query = $this->db->get();
	$results = $query->result_array();
	$notification = array();
	foreach($results as $result)
	{
		if($result['parent_type'] == 'Concept')
		{
			$result['reply_username'] = 'concepts/'.$result['slug'];
			$result['fullname'] = $result['name'];
			$result['profile_pic'] = 'images/concepts/thumbs/'.$result['thumbnail'];
		} else if($result['parent_type'] == 'User'){
			$result['profile_pic'] = 'images/user-images/105x101/'.$result['profile_pic'];
		}
		$notification[] = $result;
	}
	return $notification;
  }

  function countUserActivities($user_id)
  {
	$this->db->select('count(l.id) as total');
	$this->db->from($this->table. ' l');
	$this->db->join('ci_users you', "you.id = l.`object_id` and l.`object_type` = 'User'");
	$this->db->where('you.id', $user_id);
	$this->db->where('l.status', 'unviewed');
	$query = $this->db->get();
	if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return 0;
  }

  function resetUserNotifications($user_id)
  {
  	$data['status'] = 'viewed';
  	$this->db->where('object_id', $user_id);
  	$this->db->where('object_type', "'User'", false);
	$this->db->update($this->table, $data);
	if($this->db->affected_rows() > 0)
    {
      return true;
    } else
    {
      $error = array();
      $error['err_code'] = $this->db->_error_number();
      return $error;
    }
  }


  function commentUserStatus($commenter, $commented_user_id, $status_id) {
    $details = array();
    $details['object_id'] = $commented_user_id;
    $details['object_type'] = 'User';

    $details['activity_source'] = $status_id;
    $details['activity_type'] = 'Comment';

    $details['parent_id'] = $commenter;
    $details['parent_type'] = 'User';
    $this->addActivity($details);
  }
  
  function userTagStatus($parent_id, $parent_type, $activity_source_type, $status_id, $tag_user_id) {
    $details = array();
    $details['object_id'] = $tag_user_id;
    $details['object_type'] = 'User';
    $details['activity_source'] = $status_id;
	$details['activity_source_type'] = $activity_source_type;
    $details['activity_type'] = 'Tag';
    $details['parent_id'] = $parent_id;
    $details['parent_type'] = $parent_type;
    $this->addActivity($details);
  }

  function postOnWall($poster, $commented_user_id, $status_id) {
    $details = array();
    $details['object_id'] = $commented_user_id;
    $details['object_type'] = 'User';

    $details['activity_source'] = $status_id;
    $details['activity_type'] = 'WallPost';

    $details['parent_id'] = $commenter;
    $details['parent_type'] = 'User';
    $this->addActivity($details);
  }

  function groupUpdateNotification($data){
    $this->addActivity($data);
  }
}