<?php
/**
  * This is a AlbumPicture model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class AlbumPicture extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('alb_pictures');
	}

  /**
  * counts pictures in an Album
  * @return int
  */
  function countAlbumPictures($id) {
    $this->db->select('count(pid) as total_count');
    $this->db->from($this->table);
    $this->db->where('aid', $id);
    $query = $this->db->get();
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total_count;
      }
    }
    return false;
  }

  /**
  * gets a list of pictures in an Album
  * @return array
  */
  function getAlbumPictures($alb_id, $limit = null, $offset = null) {
    $alb_id = (int) $alb_id;
    $this->db->select('b.pid, b.ctime, b.filepath, b.filename');
    $this->db->from($this->table.' b');
    $this->db->join('alb_albums a', 'a.aid = b.aid');
    $this->db->where(array('a.aid' => $alb_id, 'a.visibility' => 0));
    if(isset($limit))
    {
      if(isset($offset))
      {
        $this->db->limit((int)$limit,(int)$offset);
      }
      else {
        $this->db->limit((int)$limit);
      }
    }
    $query = $this->db->get();

    $pictures = $query->result_array();
    return $pictures;
  }

}