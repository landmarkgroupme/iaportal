<?php
/**
  * This is a Concept model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class GroupConceptPermission extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_object_permissions');
	}

	 /**
  * Removes given entry from Entity Model
  * @return boolean
  */
  public function remove_all_permissions($filters= null)
  {
	if(isset($filters))
	{
		foreach($filters as $column => $filter )
		{
			$this->db->where($column, $filter);
		}
		if($this->db->delete($this->table))
		{
			return true;
		} else {
			return false;
		}
	}
	else
	{
		return false;
	}

  }

  public function getAllConcept($limit = null, $name = null)
  {
	$this->db->select('name, id');
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit);
    }

      $this->db->order_by('name', 'Asc');


	if(isset($name))
	{
	$this->db->like('name', $name, 'both');
	 $query = $this->db->get('ci_master_concept');
	}
	else
	{
    $query = $this->db->get('ci_master_concept');
	}
    return $query->result();
  }

  public function getAllGroup($limit = null, $name = null)
  {
	$this->db->select('name, id');
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit);
    }

      $this->db->order_by('name', 'Asc');


	if(isset($name))
	{
	$this->db->like('name', $name, 'both');
	 $query = $this->db->get('ci_interest_groups');
	}
	else
	{
    $query = $this->db->get('ci_interest_groups');
	}
    return $query->result();
  }
  public function getAllUser($limit = null, $name = null)
  {
	$this->db->select('first_name ,last_name , id');
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit);
    }

      $this->db->order_by('first_name', 'Asc');


	if(isset($name))
	{
	$this->db->like('first_name', $name, 'both');
	$this->db->or_like('last_name', $name);
	 $query = $this->db->get('ci_users');
	}
	else
	{
    $query = $this->db->get('ci_users');
	}
    return $query->result();
  }
  
  public function get_conept_managers($limit = null, $name = null)
  {
	$role_ids = array('1', '2', '3', '4'); // Super Admin, Content Moderator, Concept Manager
	// we are allowing normal users to be selected as concept managers. there is not a need of separate user role.
	$this->db->select('first_name ,last_name , id');
	//$this->db->where_in('role_id', $role_ids);
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit);
    }

    $this->db->order_by('first_name', 'Asc');


	if(isset($name))
	{
		$this->db->like('first_name', $name, 'both');
		$this->db->or_like('last_name', $name);
		$this->db->or_like('display_name', $name);

		$query = $this->db->get('ci_users');
	
	}
	else
	{
    	$query = $this->db->get('ci_users');
	}
    return $query->result();
  }
  
 
            
   public function getUserGroup($id)
  {
	//$names = array('Frank', 'Todd', 'James');

	if(isset($id))
	{
	$this->db->select('id, name');
	$this->db->where_in('id', $id);
	$query = $this->db->get('ci_interest_groups');
    return $query->result();
	}
	else
	{
	return false;
	}
  }
  public function getGroupUser($id)
  {
	//$names = array('Frank', 'Todd', 'James');

	if(isset($id))
	{
	//$this->db->select('id, first_name, last_name');
	$this->db->select('id');
	$this->db->select("CONCAT(first_name, '.', last_name) AS name", FALSE);
	$this->db->where_in('id', $id);
	$query = $this->db->get('ci_users');
    return $query->result();
	}
	else
	{
	return false;
	}
  }
   public function getUserGroupPer($id, $type = null)
  {

	if(!is_int($id))
    {
      return 'Not a valid Id';
    }
	$this->db->where('user_id', $id);

	if($type != "")
	{
	$this->db->where('object_type',$type);
	}
    $query = $this->db->get($this->table);
    return $query->result();
  }

   public function getGroupUserPer($id, $type = null)
  {

	if(!is_int($id))
    {
      return 'Not a valid Id';
    }
	$this->db->where('object_id', $id);

	if($type != "")
	{
	$this->db->where('object_type',$type);
	}
    $query = $this->db->get($this->table);
    return $query->result();
  }


}