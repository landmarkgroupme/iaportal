<?php
require_once dirname(__FILE__) . '/crud.php';

Class Statuses extends Crud
{
  var $table = null;

  function __construct()
  {
    parent::__construct();
    $this->setTable('ci_status_updates');
    $this->load->helper('time_ago_helper');
  }

  public function getUserStatusUpdates($limit = 20, $offset = 0, $id = NULL, $my_stasuses = FALSE)
  {

    //underlaying query for reference
    /*
    SELECT
              0 as following_user_id,
              b.id as message_id,
              b.message,
              b.object_id as user_id,
              b.message_time,
              CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
              CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
              c.profile_pic,
              c.email,
              d.name as designation,
              e.name as concept
            FROM
              ci_status_updates b,
              ci_users c,
              ci_master_designation d,
              ci_master_concept e
            WHERE
              c.id = ? AND
              b.status = 1 AND
              b.object_id = c.id AND b.object_type = 'User' AND
              d.id = designation_id AND
              e.id = concept_id AND
              b.id = ?

              (SELECT following_user_id FROM ci_users_following_users WHERE follow_status = 1 AND user_id = ? AND following_user_id = b.object_id ) as following_user_id

    */

    $columns = "su.id message_id, parent_status_id, total_likes, message, object_id, su.create_dttm, u.profile_pic, u.email, des.name as designation, con.name as concept, u.first_name, u.last_name, CONCAT('') as following_user_id";

    $columns .= ', likes.id liked';

    $this->db->select($columns);
    $this->db->from('ci_status_updates su');
    $this->db->join('ci_users u', "u.id = su.object_id AND su.object_type = 'User'");
    $this->db->join('ci_master_designation des', 'u.designation_id = des.id');
    $this->db->join('ci_master_concept con', 'u.concept_id = con.id');

    if(isset($id)) {
      $this->db->join('ci_users_following_users follow', "follow.user_id = su.object_id and follow.follow_status = 1 and follow.user_id = $id", 'left');
    }
    $this->db->join('ci_likes likes', "likes.status_id = su.id AND likes.user_id = $id", 'left');
    $this->db->where('su.status', 1);
    //$this->db->where('su.parent_status_id', 0);
    if(isset($id) && $my_stasuses) {
      $this->db->where('u.id', $id);
    }
    $this->db->where("su.object_type = 'User'");
    $this->db->limit($offset, $limit);
    $this->db->order_by('create_dttm desc');
    $query = $this->db->get();
    /*echo $this->db->last_query();
    die();*/
    //return $query->result_array();
    $data = array();
    foreach($query->result_array() as $status) {
      if($status['parent_status_id'] == '0') {
        //its not a comment
        $data[$status['message_id']]['data'] = $status;
      } else {
        $data[$status['parent_status_id']]['comments'][$status['message_id']] = $status;
        ksort($data[$status['parent_status_id']]['comments']);
      }


      # code...
    }
    /*echo '<pre>';
    print_r($data);
    die();*/
    return $data;
  }

  function getStatus($status_id, $user_id)
  {
    if(isset($concept) && $concept != '') {
      $sql .= " and con.id = ? ";
      $vars = array($concept, $user_id);
    } else {
      $vars = array($user_id);
    }
    $sql = "(
              SELECT
                stat.id,
                stat.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes ll where ll.status_id = stat.id and ll.user_id = ?  limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                user.display_name,
                user.profile_pic,
                NULL as concept,
                stat.object_id,
                stat.object_type,
                stat.target_type,
                posted_to_user.reply_username target_url,
                posted_to_user.display_name as target_name,
                posted_to_concept.slug as concept_url,
                posted_to_concept.name as posted_concept_name,
				posted_to_group.id as group_url,
				posted_to_group.name as posted_group_name,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug
                FROM
                ci_status_updates stat";
    //, ci_users_following_users fol
    $sql .= "
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_users posted_to_user on posted_to_user.id = stat.target_id and stat.target_type = 'User'
                LEFT JOIN ci_master_concept posted_to_concept on posted_to_concept.id = stat.target_id and stat.target_type = 'Concept'
				LEFT JOIN ci_interest_groups posted_to_group on posted_to_group.id = stat.target_id and stat.target_type = 'Group'
                LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept'

                WHERE
                stat.status = 1 ";
    /*if(isset($concept) && $concept != '') {
      $sql .= " AND (stat.object_type = 'Concept' or ( stat.target_id = $concept && stat.target_type = 'Concept')) ";
    } else {
      $sql .= " AND stat.object_type = 'User' ";
    }*/

    $sql .= " AND stat.parent_status_id = 0  AND stat.id = $status_id";
    // AND fol.follow_status = 1 AND fol.user_id = ? AND fol.following_user_id = s.object_id
    $sql .= " )";
    $sql .= "order by publish_date DESC LIMIT 1";
    $query = $this->db->query($sql, $vars);
    $data = $query->result_array();
    //print_r($data);exit;
    $return_data = array();
    //print_r($data);
    $concepts = array();
    foreach($data as $index => $item) {
      $content_type = 'Status';
      $item['comments'] = $this->Statuses->getStatusReplies($item['id'], $content_type, $limit = 0, $offset = 10, $user_id);
      $totalComment = $this->Statuses->getStatusRepliesCount($item['id'], $content_type);
      $item['total_comments'] = $totalComment;
      foreach($item['comments'] as $key2 => $comment) {
        $item['comments'][$key2]['likes'] = $this->getLike($comment['id'], $user_id);
        $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $comment['message'])));
        $item['comments'][$key2]['time_ago'] = time_ago(strtotime($comment['create_dttm']));

        $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
        $replace = '<a href="' . site_url() . '$1">$2</a>';
        $item['comments'][$key2]['message'] = stripslashes(htmlentities(preg_replace($pattern, $replace, $item['comments'][$key2]['message'])));
      }
      $item['likes'] = $this->getLike((int)$status_id, $user_id);
      $replacedText = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $item['message']);

      if($replacedText == $item['message']) {
        $item['message'] = $this->MakeUrls($replacedText);
      } else {
        $item['message'] = $replacedText;
      }

      $pattern = '/@#([a-zA-Z0-9_.]+):([a-zA-Z0-9_. ]+):/';
      $replace = '<a href="' . site_url() . '$1">$2</a>';
      $item['message'] = preg_replace($pattern, $replace, $item['message']);

      //print "<pre>";
      // print_r($item);exit;
      if($item['object_type'] == 'Concept') {
        if($item['category'] == 'photo') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['profile_pic'];
        } else {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
        }
        // $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
        $item['display_name'] = $item['concept_name'];
      } elseif($item['object_type'] == 'User') {
        $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        if($item['target_type'] == 'Concept') {
          $item['target_name'] = $item['posted_concept_name'];
          $item['target_url'] = $item['concept_url'];
        }
        if($item['target_type'] == 'Group') {
          $item['target_name'] = $item['posted_group_name'];
          $item['target_url'] = $item['group_url'];
        }
      }
      $concepts[] = $item;

    }
    //print_r($concepts);exit;
    return $concepts;

  }

  function getLike($status_id, $user_id)
  {
    $this->db->select('u.id, u.first_name, u.email, u.reply_username');
    $this->db->from('ci_users u');
    $this->db->join('ci_likes b', 'b.user_id = u.id', 'left');
    $this->db->where('b.status_id', $status_id);
    $this->db->where('b.user_id !=', $user_id);
    $this->db->order_by('b.id', 'DESC');
    $this->limits('2', '0');
    $query = $this->db->get();
    return $query->result_array();
  }

  function MakeUrls($str)
  {
    $find = array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si', '`((?<!//)(www\.\S+[[:alnum:]]/?))`si');
    $replace = array('a href="$1" target="_blank">$1</a>', '<a href="http://$1"    target="_blank">$1</a>');
    return preg_replace($find, $replace, $str);
  }

  function addLike($status_id, $post_type, $user_id)
  {
    $this->db->trans_start();
    $this->db->insert('ci_likes', array('status_id' => $status_id, 'status_type' => $post_type, 'user_id' => $user_id));


    if($post_type == 'Status') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('ci_status_updates');
    } else if($post_type == 'Photo') {
      $this->db->where('pid', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('alb_pictures');
    } else if($post_type == 'Albums') {
      $this->db->where('aid', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('alb_albums');
    } else if($post_type == 'Announcement' || $post_type == 'Offers' || $post_type == 'News') {
      $this->db->where('entry_id', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('mt_entry');
    } else if($post_type == 'Marketplace') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('ci_market_items');
    } else if($post_type == 'Files') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('ci_files');
    }
	else if($post_type == 'Widget') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes + 1)', FALSE);
      $this->db->update('widgets');
    }
    $this->db->last_query();
    $this->db->trans_complete();
    return true;
  }

  function removeLike($status_id, $post_type, $user_id)
  {
    $this->db->trans_start();
    $this->db->where(array('status_id' => $status_id, 'status_type' => $post_type, 'user_id' => $user_id));
    $this->db->delete('ci_likes');
    if($post_type == 'Status') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update($this->table);
    } elseif($post_type == 'Photo') {
      $this->db->where('pid', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update('alb_pictures');
    } elseif($post_type == 'Albums') {
      $this->db->where('aid', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update('alb_albums');
    } else if($post_type == 'Announcement' || $post_type == 'Offers' || $post_type == 'News') {
      $this->db->where('entry_id', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update('mt_entry');
    } else if($post_type == 'Marketplace') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update('ci_market_items');
    } else if($post_type == 'Files') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update('ci_files');
    }else if($post_type == 'Widget') {
      $this->db->where('id', $status_id);
      $this->db->set('total_likes', ' (total_likes - 1)', FALSE);
      $this->db->update('widgets');
    }
    $this->db->trans_complete();
    return true;
  }
  
  function setFeaturedComment($status_id,$post_type,$val)
  {
 
	if($post_type == 'Status') {
	$this->db->where('id', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('ci_status_updates');
	}
	if($post_type == 'Photo') {
	$this->db->where('pid', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('alb_pictures');
	}
	if($post_type == 'Albums') {
	$this->db->where('aid', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('alb_albums');
	}
	if($post_type == 'Widget') {
	$this->db->where('id', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('widgets');
	}
	if($post_type == 'Marketplace') {
	$this->db->where('id', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('ci_market_items');
	}
	if($post_type == 'Files') {
	$this->db->where('id', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('ci_files');
	}
	if($post_type == 'Polls') {
	$this->db->where('poll_id', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('v2_polls');
	}
	if($post_type == 'Announcement' || $post_type == 'Offers' || $post_type == 'News')
	{
	$this->db->where('entry_id', $status_id);
	$this->db->set('featured', $val, FALSE);
	$this->db->update('mt_entry');
	}

	return true;
  }

  function getStatusRepliesCount($status_id = false, $content_type = false)
  {
    $this->db->select('COUNT(con.id) as total_count');
    $this->db->from($this->table . ' con');
    if(isset($content_type) && !empty($content_type)) {
      $this->db->where('con.parent_type', $content_type);
    }
    if(isset($status_id) && !empty($status_id)) {
      $this->db->where('con.parent_status_id', $status_id);
    }
    $query = $this->db->get();
    $data = $query->result();
    #echo "<pre>".print_r($data,"\n")."</pre>";
    return $data[0]->total_count;
  }

  function getStatusReplies($status_id, $content_type, $limit = 0, $offset = 0, $user_id = 0)
  {
    if($user_id == 0) {
      $sql = "(SELECT
                su.id,
                su.message,
				'User' as object_type,
				u.id as object_id,
                u.first_name,
                u.last_name,
                u.display_name,
                u.reply_username,
                u.profile_pic,
                su.create_dttm
                
              FROM
			  ci_status_updates su,
			  ci_users u
				 WHERE
                 u.id = su.object_id and su.object_type = 'User' and su.parent_status_id = '$status_id' and su.parent_type = '$content_type'
				)UNION ALL(
                SELECT
                st.id,
                st.message,
				st.object_type as object_type,
				st.id as object_id,
                NULL as first_name,
                NULL as last_name,
                con.name as display_name,
                con.slug as reply_username,
                con.thumbnail as profile_pic,
                st.create_dttm
              FROM
			  ci_status_updates st,
			  ci_master_concept con
				 WHERE
                 con.id = st.object_id and st.object_type = 'Concept' and st.parent_status_id = '$status_id' and st.parent_type = '$content_type'
				)";
    } else {

      $sql = "(SELECT
                su.id,
                su.message,
				'User' as object_type,
				u.id as object_id,
                u.first_name,
                u.last_name,
                u.display_name,
                u.reply_username,
                u.profile_pic,
                su.create_dttm,
				(select id from ci_likes ll where ll.status_id = su.id and ll.user_id = '$user_id'  limit 1) as you_like,
				su.total_likes
                
              FROM
			  ci_status_updates su,
			  ci_users u
				 WHERE
                 u.id = su.object_id and su.object_type = 'User' and su.parent_status_id = '$status_id' and su.parent_type = '$content_type'
				)UNION ALL(
                SELECT
                st.id,
                st.message,
				'Concept' as object_type,
				con.id as object_id,
                NULL as first_name,
                NULL as last_name,
                con.name as display_name,
                con.slug as reply_username,
                con.thumbnail as profile_pic,
                st.create_dttm,
				NULL as you_like,
				st.total_likes
              FROM
			  ci_status_updates st,
			  ci_master_concept con
				 WHERE
                 con.id = st.object_id and st.object_type = 'Concept' and st.parent_status_id = '$status_id' and st.parent_type = '$content_type'
				)";
    }
    $sql .= " order by create_dttm DESC";
    if($limit > 0 || $offset > 0) {
      $sql .= " LIMIT " . $limit . "," . $offset . " ";
    }

    $query = $this->db->query($sql);
    $data = $query->result_array();
    $return_data = array();
    foreach($data as $index => $item) {
      if($item['object_type'] == 'Concept') {
        $item['profile_pic'] = 'images/concepts/thumbs/' . $item['profile_pic'];
        //$item['display_name'] = $item['concept_name'];
        $item['reply_username'] = 'concepts/' . $item['reply_username'];
      } elseif($item['object_type'] == 'User') {
        $item['profile_pic'] = 'images/user-images/25x25/' . $item['profile_pic'];
      }
      $return_data[] = $item;
    }

    return $return_data;
    /*print_r($data);
    exit;

      $this->db->select('su.id, su.message, u.first_name, u.last_name, u.display_name, u.reply_username, u.profile_pic, su.create_dttm');
      $this->db->from($this->table. ' su');
      $this->db->join('ci_users u', "u.id = su.object_id and su.object_type = 'User'", 'left', false);
      $this->db->where('parent_status_id', $status_id);
      $this->db->order_by('su.create_dttm', 'DESC');
      $this->limits($limit, $offset);
      $result = $this->db->get();
    return $query->result_array();
  */
  }


  function deleteStatus($status_id, $user_id)
  {
    $status = $this->get((int)$status_id);
    $status = (array)$status[0];
    /*print_r($status);
    die();*/

    if(($status['object_type'] == 'User' && $status['object_id'] == $user_id) || ($status['object_type'] == 'Concept')) {
      //delete the actual message and any replies to that message
      $this->db->where('id', $status_id);
      $this->db->or_where('parent_status_id', $status_id);
      $this->db->delete($this->table);
      return $this->db->affected_rows();
    }
    return 0;

  }


}