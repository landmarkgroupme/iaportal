<?php
/**
  * This is a Currency model.
  *
  * @author Kunal Patil
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Widget extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('widgets');
	}
	
	function getByFilters(array $filters, $columns = null)
	{
    if(isset($columns))
    {
      $this->db->select($columns, false);
    }
	$this->db->order_by('weight', 'Asc');
    $query = $this->db->get_where($this->table, $filters);
	$data = array();
    if($data = $query->result())
    {

      if($this->db->affected_rows() > 0)
      {
        //Returning array here to keep a consistency accross system
        return (array)$data;
      }
    }
    return $data;
  }
  /**
   * @param $widget_id Integer Widget Id
   * @return Return the associative array of Wideget information
   */
  function getWidget($widget_id) {
    $this->db->select(' wid.name as widget_title, wid.widget as widget_data, wid.create_dttm, wid.weight, wid.total_likes, wic.name as category_title');
    $this->db->from("$this->table wid");
    $this->db->join('wid_categories wic', 'wid.category_id = wic.id');
    $this->db->where('wid.status', 1);
    $this->db->where('wid.id', $widget_id);
    $query = $this->db->get();
    return $query->result_array();
  }
  
   function getWidgetajax($widget_id ,$user_id = NULL ) {
    $this->db->select("wid.id, wid.name as widget_title, wid.widget as widget_data, wid.create_dttm, wid.weight, wid.total_likes, wic.name as category_title, (select id from ci_likes lll where lll.status_id = wid.id and lll.status_type = 'Widget' and lll.user_id = '$user_id' limit 1) as you_like, wid.create_dttm as publish_date, 'widgets' as category,", false);
    $this->db->from("$this->table wid");
    $this->db->join('wid_categories wic', 'wid.category_id = wic.id');
    $this->db->where('wid.status', 1);
    $this->db->where('wid.id', $widget_id);
    $query = $this->db->get();
    return $query->result_array();
  }
  /**
   * @param $widget_id Integer Widget Id
   * @return Return the associative array of User Details who have liked the widget
   */
  function getWidgetLike($widget_id, $user_id) {
    $this->db->select('u.id, u.first_name, u.email, u.reply_username');
    $this->db->from('ci_users u');
    $this->db->join('ci_likes b', 'b.user_id = u.id', 'left');
    $this->db->where('b.status_id', $widget_id);
    $this->db->where('b.status_type', 'Widget');
    $this->db->where('b.user_id !=', $user_id);
    $this->db->order_by('b.id', 'DESC');
    $this->limits('2','0');
    $query = $this->db->get();
    return $query->result_array();
  }
}