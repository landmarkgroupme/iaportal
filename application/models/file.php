<?php
/**
  * This is a File model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class File extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_files');
	}

  /**
  * Count Files uploaded by a User
  *
  * @return int
  */
  function countUserFiles($user_id){
    $this->db->select('count(id) as files_count');
    $query = $this->db->get_where($this->table, array('files_status' => 1,'user_id' => $user_id));
    $data = 0;
    if($query->num_rows() > 0){
      foreach($query->result_array() as $row){
        $data  = (int)$row['files_count'];
      }
    }
    return $data;
  }

  /**
  * get Files by a limit, 3rd parameter can restrict to user only files
  *
  * @return array
  */
  function getFiles($limit = null, $offset = null, $filters = null)
  {

    $this->db->select("f.name as title, f.id as file_id, f.file_size, f.download_count, f.path, f.description, f.file_mime_type, f.unique_id, f.uploaded_on, c.name as category, c.id as cat_id, f.concept_id,
                      u.display_name,
                      u.id as user_id, u.reply_username, u.status as user_status, con.name as concept_name, con.slug as concept_slug", false);
    $this->db->from("$this->table f");
    $this->db->join('ci_master_file_category c', 'f.file_category_id = c.id');
	$this->db->join('ci_master_concept con', 'f.concept_id = con.id','left');

    if(isset($filters['user_id']))
    {
      $this->db->join('ci_users u', 'f.user_id = u.id AND u.id = '.$filters['user_id'], 'left', false);
    }
    else
    {
      $this->db->join('ci_users u', 'f.user_id = u.id', 'left');
    }

    if(isset($filters['cat_id']))
    {
      $this->db->where('f.file_category_id', $filters['cat_id']);
    }

    if(isset($filters['concept_id']))
    {
      $this->db->where('f.concept_id', $filters['concept_id']);
    }

    if(isset($filters['is_link']))
    {
      $this->db->where('f.file_size is NULL', null, false );
    }
    if(isset($filters['is_file']))
    {
      $this->db->where('f.file_size > 0', null, false );
    }
  	if(isset($filters['search_term']))
  	{
      $this->db->join('ci_map_files_tag ft', 'ft.files_id = f.id', 'left');
      $this->db->join('ci_master_file_tag t', 't.id = ft.file_tag_id', 'left');
      $this->db->where("(f.name like '%".$filters['search_term']."%' or t.name like '%".$filters['search_term']."%')");
    }
    if(isset($filters['group_id']) && !empty($filters['group_id']))
    {
      $this->db->join('ci_map_objects omap','f.id = omap.object_id AND omap.object_type = \'File\' AND omap.parent_id = '.$filters['group_id'].' AND omap.parent_type = \'Group\'');
    }
    if(isset($filters['tag_id']))
    {
      $this->db->join('ci_master_file_tag t', 't.id = '. $filters['tag_id'], null, false);
      $this->db->join('ci_map_files_tag ft', 't.id = ft.file_tag_id and ft.files_id = f.id');
      //$this->db->where('t.id', );
    }
    
    $this->db->where('f.group_file',$filters['group_file']);
    if(isset($filters['member_id']) && !empty($filters['member_id']))
    {
      $this->db->join('ci_map_objects map','f.id = map.parent_id AND map.parent_type = \'File\'','LEFT');
      $where = 'CASE WHEN f.have_assignee = 1 THEN map.object_type = \'User\' AND map.object_id = '.$filters['member_id'].' ELSE 1=1 END';
      $this->db->where($where,null,FALSE);
    }
    if((isset($filters['month']) && !empty($filters['month'])) && (!isset($filters['year']) || empty($filters['year'])))
    {
      $where = '(f.uploaded_on >= '.strtotime('1-'.$filters['month'].'-'.(date('Y',time())-5)).' AND f.uploaded_on <= '.strtotime('1-'.$filters['month'].'-'.date('Y',time()).'last day of this month').')';
      $this->db->where($where);
    }
    elseif((!isset($filters['month']) || empty($filters['month'])) && (isset($filters['year']) && !empty($filters['year'])))
    {
      $where = '(f.uploaded_on >= '.strtotime('1-1-'.$filters['year']).' AND f.uploaded_on <= '.strtotime('1-12-'.$filters['year'].'last day of this month').')';
      $this->db->where($where);
    }
    elseif((isset($filters['month']) && !empty($filters['month'])) && (isset($filters['year']) && !empty($filters['year'])))
    {
      $where = '(f.uploaded_on >= '.strtotime('1-'.$filters['month'].'-'.$filters['year']).' AND f.uploaded_on <= '.strtotime('1-'.$filters['month'].'-'.$filters['year'].'last day of this month').')';
      $this->db->where($where);
    }
    
    $this->db->where('f.files_status', 1);
    $this->db->group_by('f.id');
    $this->limits($limit, $offset);
    $this->db->order_by('f.uploaded_on', 'DESC');
    $query = $this->db->get();
    #echo $this->db->last_query();
	$data = $query->result_array();
	foreach($data as $key => $file)
	{
		if($file['concept_name']!= '' && $file['concept_slug'])
		{
			$data[$key]['display_name']=$file['concept_name'];
			$data[$key]['reply_username']='concepts/'.$file['concept_slug'];
		}
	}
    //return $query->result_array();
	return $data;
  }

  /**
  * get Latest files by category
  *
  * default number of files is 5, but can be given any number of files in 3rd parameter
  *
  * @return array
  */
  function getLatestFiles($cat_id, $sort_by = 'id', $limit = 5)
  {
    $this->db->select('f.id, f.name, f.unique_id, f.file_size, f.path, fc.name as cat_name');
    $this->db->from($this->table.' f');
    $this->db->join('ci_master_file_category fc', 'f.file_category_id = fc.id');
    $this->db->where('f.files_status', 1);
    $this->db->where('fc.id', $cat_id);
    $this->limits($limit, null);
    $this->db->order_by('f.'.$sort_by, 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * get a File details by File ID
  *
  * @return array
  */
  function getFileDetails($file_id)
  {
    $this->db->select('f.name,
              f.description,
              f.file_size,
              f.download_count,
              f.uploaded_on,
              f.unique_id,
              f.path,
              f.user_id,
              fc.important_cat');
    $this->db->from($this->table.' f');
    $this->db->join('ci_master_file_category fc', 'fc.id = f.file_category_id');
    $this->db->where('f.files_status', 1);
    $this->db->where('f.id', $file_id);
    $this->limits(1, 0);
    $query = $this->db->get();
    $result = $query->result_array();
	return $result;
  }

  function getFilesByTag($limit = null, $offset = null, $tag_id)
  {

    $this->db->select("f.name as title, f.id as file_id, f.file_size, f.download_count, f.path, f.description, f.file_mime_type, f.unique_id, f.uploaded_on, c.name as category, c.id as cat_id,
                      CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                      CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                      u.id as user_id, u.reply_username", false);
    $this->db->from("$this->table f");
    $this->db->join('ci_master_file_category c', 'f.file_category_id = c.id');
    $this->db->join('ci_users u', 'f.user_id = u.id');
    $this->db->join('ci_master_file_tag t', 'f.user_id = u.id');
    $this->db->join('ci_map_files_tag ft', 't.id = ft.file_tag_id and ft.files_id = f.id');

    $this->db->where('t.id', $tag_id);
    $this->db->where('f.files_status', 1);
    $this->limits($limit, $offset);
    $this->db->order_by('f.id', 'DESC');
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result_array();
  }

  /**
  * autosearch files
  *
  * @return array
  */
  function searchFiles($limit = null, $offset = null, $search_term,$group = false,$member_id = null)
  {

    //$this->db->select("f.name as label, f.file_size, f.download_count, f.path, f.unique_id, c.name as category, c.id as cat_id", false);
    $this->db->select("f.name as label, f.name as value, GROUP_CONCAT(t.name) as tags", false);
    $this->db->from("$this->table f");
    $this->db->join('ci_master_file_category c', 'f.file_category_id = c.id');

    
    $this->db->join('ci_map_files_tag ft', 'ft.files_id = f.id');
    $this->db->join('ci_master_file_tag t', 't.id = ft.file_tag_id', null);

    $this->db->where("(f.name like '%". $search_term ."%' or t.name like '%". $search_term ."%')");
    if(isset($group) && !empty($group))
    {
      $this->db->join('ci_map_objects omap','f.id = omap.object_id AND omap.object_type = \'File\' AND omap.parent_id = '.$group.' AND omap.parent_type = \'Group\'');
      $this->db->where('f.group_file', 1);  
      if(isset($member_id) && !empty($member_id))
      {
        $this->db->join('ci_map_objects map','f.id = map.parent_id AND map.parent_type = \'File\'','LEFT');
        $where = 'CASE WHEN f.have_assignee = 1 THEN map.object_type = \'User\' AND map.object_id = '.$member_id.' ELSE 1=1 END';
        $this->db->where($where,null,FALSE);
      }
    }
    else
    {
      $this->db->where('f.group_file', 0);
    }
    
    $this->db->where('f.files_status', 1);
    
    $this->limits($limit, $offset);
    $this->db->order_by('f.name', 'ASC');
    $this->db->group_by('f.id');
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result_array();
  }
  
  function checkfilenameExist($group_id = false,$file_name = false){
    $this->db->select("f.id", false);
    $this->db->from("$this->table f");
    if(isset($group_id) && !empty($group_id))
    {
      $this->db->join('ci_map_objects omap','f.id = omap.object_id AND omap.object_type = \'File\' AND omap.parent_id = '.$group_id.' AND omap.parent_type = \'Group\'');
      $this->db->where('f.group_file', 1);  
    }
    $this->db->where('f.files_status', 1);
    $this->db->where('f.name',$file_name);
    $this->db->group_by('f.id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    $record = $query->result_array();
    #echo "<pre>".print_r($record,"\n")."</pre>";
    if(isset($record) && !empty($record))
    {
      return $record[0];
    }
    return false;
    
  }


}