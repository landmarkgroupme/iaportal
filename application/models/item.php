<?php
/**
  * This is a Item model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Item extends Crud {
  var $freebies, $wanted, $forsale;
	function __construct() {
		parent::__construct();
    $this->setTable('ci_market_items');
    $this->freebies = 3;
    $this->wanted = 2;
    $this->forsale = 1;
	}

  /**
  * get Item Images
  *
  * Names in v1: market_place_model -> mp_item_images
  * @return array
  */
  
   function getLike($status_id, $user_id) {
	$this->db->select('u.id, u.first_name, u.email, u.reply_username');
    $this->db->from('ci_users u');
	$this->db->join('ci_likes b', 'b.user_id = u.id', 'left');
    $this->db->where('b.status_id', $status_id);
	$this->db->where('b.status_type', 'Marketplace');
	$this->db->where('b.user_id !=', $user_id);
	$this->db->order_by('b.id', 'DESC');
	$this->limits('2','0');
    $query = $this->db->get();
    return $query->result_array();
  }
  function getAllItems($limit = null, $offset = null, array $filters = null) {
		$this->db->select("
		a.id,
		(SELECT original_image FROM ci_map_items_images WHERE market_items_id = a.id LIMIT 1)as item_image,
		(SELECT thumbnail FROM ci_map_items_images WHERE market_items_id = a.id LIMIT 1)as item_thumb,
		a.title,
		a.user_id,
		a.item_max_price,
		a.item_min_price,
		a.added_on,
		b.code as currency,
		d.default_thumb_path,
		d.name as category_name,
		u.reply_username,
		u.display_name,
		u.profile_pic,
    a.item_type
		", false);
		$this->db->from("$this->table a");
		$this->db->join('ci_master_currency b', 'a.currency_id = b.id', 'left');
		$this->db->join('ci_master_category d', 'a.category_id = d.id','left');
		$this->db->join('ci_users u', 'a.user_id = u.id');
		$this->db->where('a.items_status', 1);

		if(isset($filters['type']))
		{
			$this->db->where('a.item_type', $filters['type']);
		}
		if(isset($filters['user']))
		{
			$this->db->where('a.user_id', $filters['user']);
		}
		if(isset($filters['category']))
		{
			$this->db->where('a.category_id', $filters['category']);
		}

		if(isset($filters['concept']))
		{
			$this->db->where('u.concept_id', $filters['concept']);
		}
		if(isset($filters['search_term']))
		{

			$this->db->like('a.title', $filters['search_term']);
		}
		if(isset($filters['price']))
		{
			if($filters['price'] == 1)
			{
				$sort ="ASC";
			}
			else
			{
				$sort ="DESC";
			}
			$this->db->order_by('a.item_max_price', $sort);
		}
		else
		{
		 $this->db->order_by('a.id', 'DESC');
		 }
		  $this->limits($limit, $offset);
		 $query = $this->db->get();

		//echo $this->db->last_query();
		return $query->result_array();


	}
	
	 /**
  * autosearch files
  *
  * @return array
  */
  function searchItems($limit = null, $offset = null, $search_term)
  {

	////////
	$this->db->select("
		a.title as label,
		a.id as value", false);
		$this->db->from("$this->table a");
		$this->db->like('a.title', $search_term);
		$this->db->where('a.items_status', 1);

    $this->limits($limit, $offset);
    $this->db->order_by('a.title', 'ASC');
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result_array();
  }

  function getItemImages($item_id) {
    $this->db->select('id, original_image, thumbnail');
    $this->db->from('ci_map_items_images');
    $this->db->where('market_items_id', $item_id);
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Returns an Item Details
  *
  * Names in v1: market_place_model -> mp_item_details
  * @return array
  */
  function getItem($item_id)
  {
    $this->db->select(' i.title, i.description, i.user_id, i.item_min_price, i.item_max_price, i.item_condition,
                        i.item_type, i.category_id, cur.code, l.name as location, c.name as category, c.default_thumb_path, c.default_image_path');
    $this->db->from("$this->table i");
    $this->db->join('ci_users u', 'i.user_id = u.id');
    $this->db->join('ci_master_location l', "u.location_id = l.id");
    $this->db->join('ci_master_category c', "i.category_id = c.id");
    $this->db->join('ci_master_currency cur', "cur.id = i.currency_id", 'left');

    $this->db->where('i.items_status', 1);
    $this->db->where('i.id', $item_id);
    $query = $this->db->get();
    return $query->result_array();
  }
  
  function getItemData($item_id, $user_id)
  {
   $this->db->select("i.id,  i.title, i.user_id,(select id from ci_likes lll where lll.status_id = i.id and lll.status_type ='Marketplace' and lll.user_id = '$user_id' limit 1) as you_like, i.total_likes, 'marketplace' as category, i.added_on as time_ago,'Marketplace' as content_type ", false);
    $this->db->from("$this->table i");
    $this->db->where('i.items_status', 1);
    $this->db->where('i.id', $item_id);
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Returns an Items by Category and Type
  *
  * Names in v1: market_place_model -> mp_item_category
  * @return array
  */
  function getItemsByCategory($category_id, $item_type, $limit = null, $offset = null)
  {
    ///
    $this->db->select('
                        img.original_image as item_image,
                        cur.code as currency,
                        i.id, i.title, i.user_id,
                        CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                        i.item_max_price, i.item_condition,
                        i.added_on, i.category_id, cur.code as currency, c.id as category_id, c.name as category, c.default_image_path', false);
    $this->db->from("$this->table i");
    $this->db->join('ci_users u', 'i.user_id = u.id');
    $this->db->join('ci_map_items_images img', " i.id = img.market_items_id", 'left');
    $this->db->join('ci_master_category c', "i.category_id = c.id");
    $this->db->join('ci_master_currency cur', "cur.id = i.currency_id", 'left');

    $this->db->where('i.items_status', 1);
    $this->db->where('i.item_type', $item_type);
    $this->db->where('i.category_id', $category_id);
    $this->limits($limit, $offset);
    $this->db->order_by('i.id', 'ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

   /**
  * Returns an Freebies in Market Place
  *
  * If the 3rd parament is provided which is user then user only freebies are to be returned
  *
  * Names in v1: market_place_model -> mp_item_freebies
  * @return array
  */
  function getFreebies($user_id = null, $limit = null, $offset = null )
  {
    ///
    $item_type = $this->freebies;
    $this->db->select('
                        img.original_image as item_image,
                        cur.code as currency,
                        i.id, i.title, i.user_id,
                        CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                        i.added_on, i.category_id, cur.code as currency, c.id as category_id, c.name as category, c.default_image_path', false);
    $this->db->from("$this->table i");
    $this->db->join('ci_users u', 'i.user_id = u.id');
    $this->db->join('ci_map_items_images img', " i.id = img.market_items_id", 'left');
    $this->db->join('ci_master_category c', "i.category_id = c.id");
    $this->db->join('ci_master_currency cur', "cur.id = i.currency_id", 'left');

    $this->db->where('i.items_status', 1);
    $this->db->where('i.item_type', $item_type);
    if(isset($user_id))
    {
      $this->db->where('i.user_id', $user_id);
    }
    $this->limits($limit, $offset);
    $this->db->order_by('i.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Returns an Wanted Items in Market Place
  *
  * If the 3rd parament is provided which is user then user only wated items are to be returned
  *
  * Names in v1: market_place_model -> mp_item_wanted
  * @return array
  */
  function getWantedItems($user_id = null, $limit = null, $offset = null)
  {
    ///
    $item_type = 2;
    $this->db->select('
                        img.original_image as item_image,
                        i.id,
                        i.title,
                        i.item_max_price,
                        i.item_min_price,
                        i.added_on,
                        cur.code as currency,
                        i.user_id,
                        CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                        i.category_id, c.name as category, c.default_image_path', false);
    $this->db->from("$this->table i");
    $this->db->join('ci_users u', 'i.user_id = u.id');
    $this->db->join('ci_map_items_images img', " i.id = img.market_items_id", 'left');
    $this->db->join('ci_master_category c', "i.category_id = c.id");
    $this->db->join('ci_master_currency cur', "cur.id = i.currency_id", 'left');

    $this->db->where('i.items_status', 1);
    $this->db->where('i.item_type', $item_type);
    if(isset($user_id))
    {
      $this->db->where('i.user_id', $user_id);
    }
    $this->limits($limit, $offset);
    $this->db->order_by('i.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }


  /**
  * Returns Items for sale in Market Place
  *
  * If the 1st parament is not null which is user then user only items for sale are to be returned
  *
  * Names in v1: market_place_model -> mp_item_sale
  * @return array
  */
  function getItemsforSale($user_id = null, $limit = null, $offset = null)
  {
    ///
    $item_type = 1;
    $this->db->select('
                        img.original_image as item_image,
                        i.id,
                        i.title,
                        i.item_max_price,
                        i.item_min_price,
                        i.added_on,
                        cur.code as currency,
                        i.user_id,
                        CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                        CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                        i.category_id, c.name as category, c.default_image_path', false);
    $this->db->from("$this->table i");
    $this->db->join('ci_users u', 'i.user_id = u.id');
    $this->db->join('ci_map_items_images img', " i.id = img.market_items_id", 'left');
    $this->db->join('ci_master_category c', "i.category_id = c.id");
    $this->db->join('ci_master_currency cur', "cur.id = i.currency_id", 'left');

    $this->db->where('i.items_status', 1);
    $this->db->where('i.item_type', $item_type);
    if(isset($user_id))
    {
      $this->db->where('i.user_id', $user_id);
    }
    $this->limits($limit, $offset);
    $this->db->order_by('i.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Returns 5 most recent Items for sale in Market Place
  *
  * Names in v1: market_place_model -> mp_recently_added_item_sale
  * @return array
  */
  function getRecentItemsforSale()
  {
    return $this->getItemsforSale(null, 5, 0);
  }

  /**
  * Returns 10 latest freebies in Market Place
  *
  * Names in v1: market_place_model -> mp_latest_freebies
  * @return array
  */
  function getLatestFreebies()
  {
    return $this->getLatestItems($this->freebies, 10, 0);
  }

   /**
  * Returns x no of most recent Items for any item type in Market Place
  * @return array
  */
  function getLatestItems($item_type, $limit, $offset)
  {
    $this->db->select('i.id, i.title');
    $this->db->from($this->table.' i');
    $this->db->where('i.items_status', 1);
    $this->db->where('i.item_type', $item_type);
    $this->limits($limit, $offset);
    $this->db->order_by('i.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Returns 10 latest wanted items in Market Place
  *
  * Names in v1: market_place_model -> mp_latest_item_wanted
  * @return array
  */
  function getLatestWanted()
  {
    return $this->getLatestItems($this->wanted, 10, 0);
  }

  /**
  * count wanted items in the market place
  *
  * Names in v1: market_place_model -> mp_get_count_item_wanted
  * @return array
  */
  function countWantedItems($category = null, array $filters = null)
  {
    return $this->countItems($this->wanted, $category, $filters);
  }
  
  function countFreebiesItems($category = null, array $filters = null)
  {
    return $this->countItems($this->freebies, $category, $filters);
  }

  function countUserItems($user , array $filters = null)
  {
	$this->db->select('count(a.id) as total');
	$this->db->from("$this->table a");
	$this->db->join('ci_users u', 'a.user_id = u.id');
	if(isset($user))
	{
      $this->db->where('a.user_id', $user);
	}
	$this->db->where('a.items_status', 1);
   
		
		if(isset($filters['category']))
		{
			$this->db->where('a.category_id', $filters['category']);
		}

		if(isset($filters['concept']))
		{
			$this->db->where('u.concept_id', $filters['concept']);
		}
		if(isset($filters['search_term']))
		{

			$this->db->like('a.title', $filters['search_term']);
		}
		

    $query = $this->db->get();
    //echo $this->db->last_query();
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return 0;
  }
  /**
  * count all items in market place by item type and category (optional)
  * @return int
  */
  function countItems($item_type, $category = null, array $filters = null)
  {
    $this->db->select('count(a.id) as total');
	$this->db->from("$this->table a");
		
		$this->db->join('ci_users u', 'a.user_id = u.id');
		$this->db->where('a.items_status', 1);
   
    $this->db->where('a.item_type', $item_type);
		
		if(isset($filters['category']))
		{
			$this->db->where('a.category_id', $filters['category']);
		}

		if(isset($filters['concept']))
		{
			$this->db->where('u.concept_id', $filters['concept']);
		}
		if(isset($filters['search_term']))
		{

			$this->db->like('a.title', $filters['search_term']);
		}
    if(isset($category))
      $this->db->where('category_id', $category);
	  
    $query = $this->db->get();
    //echo $this->db->last_query();
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return false;
  }

  /**
  * count freebies in the market place
  *
  * Names in v1: market_place_model -> mp_get_count_item_freebies
  * @return array
  */
  function countFreebies()
  {
    return $this->countItems($this->freebies);
  }

  /**
  * count no of items for sale in the market place
  *
  * Names in v1: market_place_model -> mp_get_count_item_sale
  * @return array
  */
  function countItemsforSale(array $filters = null)
  {

    $this->db->select('count(a.id) as total');
	$this->db->from("$this->table a");
	$this->db->join('ci_users u', 'a.user_id = u.id');
	$this->db->join('ci_master_currency cur', "cur.id = a.currency_id");
	$this->db->where('a.items_status', 1);
	$this->db->where('a.item_type', $this->forsale);
		
		if(isset($filters['category']))
		{
			$this->db->where('a.category_id', $filters['category']);
		}

		if(isset($filters['concept']))
		{
			$this->db->where('u.concept_id', $filters['concept']);
		}
		if(isset($filters['search_term']))
		{

			$this->db->like('a.title', $filters['search_term']);
		}

    $query = $this->db->get();
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return false;
  }

  /**
  * Returns Item category list by item type
  * @return array
  */
  function getCategoryList($category_order, $item_type)
  {
    $this->db->select('c.id, c.name as category, c.default_image_path, count(i.id) as total_count');
    $this->db->from($this->table.' i');
    $this->db->join('ci_master_category c', 'i.category_id = c.id');
    $this->db->where('i.items_status', 1);
    $this->db->where('i.item_type', $item_type);
    $this->db->group_by('c.id');
    $this->db->order_by($category_order, 'ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * deletes an item from market place
  *
  * Item is not deleted in actual but becomes inactive
  * @return boolean
  */
  function deleteItem($item_id)
  {
    if($this->update(array('items_status' => 2), $item_id))
    {
      return true;
    }
    return false;
  }

  /**
  * updates an item status in market place
  *
  * @return boolean
  */
  function updateStatus($item_id, $status)
  {
    if($this->update(array('items_status' => $status), $item_id))
    {
      return true;
    }
    return false;
  }

}