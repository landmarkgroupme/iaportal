<?php
/**
  * This is a Territory model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Territory extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_territory');
	}
	
	function getTerritoryByConcept($concept_id){
    $this->db->select('s.*');
    $this->db->from($this->table. ' s');
    if($concept_id)
    {
      $this->db->join('ci_master_outlets con', 'con.territory_id = s.id');
      $this->db->where('con.concept_id', $concept_id);
    }
    $this->db->order_by('s.name','ASC');
    $query = $this->db->get();
    return $query->result();
  }

}