<?php
/**
  * This is a FileCategory model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class FileCategory extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_file_category');
	}

   /**
  * get File categories by Filter
  *
  * @return array
  */
  function getFileCategories($important_cat = 1, $status = 'all') {
    $this->db->select('id, name, category_icon, important_cat');
    
    if(isset($important_cat))
      $this->db->where('important_cat', $important_cat);
    
    if($status != 'all')
      $this->db->where('status', $status);
    
    $query = $this->db->get($this->table); 
    return $query->result_array();
  }
  function getFileCategoriesByGroup($important_cat = 1, $status = 'all',$group_id = false)
  {
    $this->db->select('cat.id, cat.name, cat.category_icon, cat.important_cat');
    
    if(isset($group_id) && !empty($group_id))
    {
      $this->db->join('ci_files f',"f.file_category_id = cat.id");
      $this->db->join('ci_map_objects map',"map.object_type = 'File' AND map.parent_type='Group' AND f.id = map.object_id",null,false);
      $this->db->where('map.parent_id',$group_id);
      $this->db->where('f.group_file',1);            
    }
    
    if(isset($important_cat))
    {
      $this->db->where('cat.important_cat', $important_cat);
    }
    
    if($status != 'all')
    {
      $this->db->where('cat.status', $status);
    }
    $this->db->group_by('cat.id');
    $query = $this->db->get($this->table.' cat'); 
    return $query->result_array();
  }
}