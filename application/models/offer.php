<?php
/**
  * This is a Offer model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/entry.php';
Class Offer extends Entry {
  private $mt_category = null;
	function __construct() {
		parent::__construct();
    $this->mt_category = 'offers';
	}

  /**
  * get List of Offers
  *
  * Names in v1: offers_model -> offers_get_all
  * @return array
  */
  function getOffers($limit = null, $offset = null)
  {
    return $this->getEntries($this->mt_category, $limit, $offset);
  }

  /**
  * get Offer Details
  *
  * Names in v1: offers_model -> offers_get_details
  * @return array
  */
  function getOfferDetails($offer_id)
  {
    return $this->get($offer_id);
    /*$this->db->select('e.entry_id, e.entry_created_on, e.entry_text, e.entry_title');
    $this->db->from("$this->table e");
    $this->db->join('mt_placement p', 'p.placement_entry_id = e.entry_id');
    $this->db->join('mt_category c', "c.category_id = p.placement_category_id and c.category_class = 'category' and e.entry_blog_id = c.category_blog_id and c.category_basename = '$this->mt_category'");
    $this->db->where('e.entry_id', $offer_id);
    $this->db->where('e.entry_status', 2);
    $this->db->order_by('e.entry_id', 'desc');
    $query = $this->db->get();
    /// echo $this->db->last_query();
    return $query->result_array();*/
  }

}