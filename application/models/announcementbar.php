<?php
/**
  * This is a AnnouncementBar model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class AnnouncementBar extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_announcement_bar');
	}

  /**
  * Returns a list of AnnouncementBars
  * @return array
  */
  function getAll($limit = null, $offset = null) {
    $this->db->select("a.id, a.announcement_bar_text, CONCAT(u.first_name,' ',u.last_name) as postedby, a.created_at, a.expiry_date, a.status", false);
    $this->db->from("$this->table a");
    $this->db->join('ci_users u', 'u.id = a.posted_by_user_id');
    $this->db->order_by('a.id', 'DESC');
    $this->limits($limit, $offset);
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * add a new Announcement Bar
  * @return int $id
  */
  public function add(array $data)
  {
    $this->db->update($this->table, array('status' => 2)); //disabling all announcement bars by default when adding a new
    return parent::add($data);
  }



}