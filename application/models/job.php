<?php
Class Job extends CI_Model {

  //private $job_feed = "http://www.landmarkgroup.com/hrxml/website_job_xml/jobs_secure.xml";
  private $job_feed = "http://www.landmarkgroup.com/hrxml/website_job_xml/jobs.xml";
  private $result_array = array();

	function __construct() {
		parent::__construct();
		$this->load->model('Territory');
		$this->load->model('Concept');
	}

  function getFeed() {
    $xmlStr = file_get_contents($this->job_feed);
    $xmlObj = @simplexml_load_string($xmlStr);

    $arrXml = $this->convert_object_to_array($xmlObj);
    return $arrXml;
  }
   function getJobByCode($code)
  {
	$this->db->select('name, job_id, code');
	$this->db->from('v2_jobs');
	$this->db->where('code', $code);
   // $this->db->order_by('name', 'ASC');
	//$this->limits($limit, $offset);
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result_array();
  }
  


  function importJobs() {


    $arrXml = $this->getFeed();

    $jobs_sorted = Array();
    $jobs_xml = $arrXml['job'];
    $job_count = count($jobs_xml);
    //echo $job_count. 'Jobs<br />';
    //Call to sorting function
    $jobs_sorted = $this->aasort($jobs_xml,"dateposted");
    $joblist = array();
	$data = array(
	'status' => 0
	);
	// Start: make status 0 for external jobs
	$this->db->where('job_for', 'external');
	$this->db->update('v2_jobs', $data);
	// End
    foreach($jobs_sorted as $list ){
      $job_code = ($list['@attributes']['code'])?$list['@attributes']['code']:'#';


      $job_name = ($list['name'])?$list['name']:'Not available';
      if ($job_name=='Not available'){
        continue;
      }
      //$job_name = ucwords(strtolower($job_name));
      $job_concept = ($list['businessunit'])?ucwords(strtolower($list['businessunit'])):'Not available';
      $job_region = ($list['region'])?$list['region']:'Not available';
      $job_country = ($list['country'])?$list['country']:'Not available';
      $job_type = ($list['type'])?ucwords(strtolower($list['type'])):'Not available';

      /* Show new tag when new job posted
      $posted_date = strtotime($list['dateposted'])+604800; //7 days
      $show_new_status = ($posted_date > time())? '<span>New</span>': '';
      */

      $show_new_status = "";

      //String formating
      $job_full_name = $job_name;
      $job_name = (strlen($job_name) > 100)? substr($job_name,0,100).'..':$job_name;
      if ($job_country=='United Arab Emirates'){
          $job_country = 'UAE';
      }
      if ($job_concept=='Lmi'){
          $job_concept = 'Landmark International';
      }
      $job_country = (strlen($job_country) > 15)? substr($job_country,0,15).'..':$job_country;


      $dateposted = date('Y-m-d', strtotime($list['dateposted']));
      $openings = $list['openings'];

      $skills = serialize($list['skills']);

     $territory_id = $this->Territory->getIdByName($job_country);
     $concept_id = $this->Concept->getIdByName($job_concept);





      //$
      $job = array(
                      'code' => $job_code,
                      'name' => $job_name,
                      'concept_id' => $concept_id ? $concept_id : 0,
                      'type' => $job_type,
                      'territory_id' =>  $territory_id ? $territory_id : 0,
                      'region' => $job_region,
                      'date_posted' => $dateposted,
                      'updated_dttm' => date('Y-m-d H:i:s'),
                      'no_of_openings' => $openings,
                      'skills' => $skills,
					  'status' => 1,
					  'job_for' => 'external'
                      );


      $job_brief = (count($list['description']))?($list['description']):'Not available';
      if ($job_brief==NULL){
          $job_brief = "Not available";
      }
      $job['description'] = json_encode($job_brief);




      $job_nationality = ($list['nationality'])?$list['nationality']:'Not available';
      $job['nationality'] = $job_nationality;

      $job_experience = ($list['experience'])?$list['experience']:'Not available';
      $job['experience'] = trim($job_experience);

      $job_qualification = (count($list['qualifications']))? ($list['qualifications']):'Not available';
      if ($job_qualification==NULL){
          $job_qualification = "Not available";
      }
      $job['qualifications'] = serialize($job_qualification);


	  $job_data= $this->getJobByCode($job_code); 
      if(empty($job_data)) {
        $this->db->insert('v2_jobs', $job);
          //job is already in intranet database so just update the information
      } else {
		$this->db->where('code', $job_code);
        $this->db->update('v2_jobs', $job);
      }

    }
    return false;

  }


  function getJobsListing($limit = 20, $offset = 0, $filters = array()) {
  	$this->db->select('job.job_id as id, job.name as title, job.description, job.date_posted, job.no_of_openings, job.region, job.code, concept.name as concept, territory.name as territory');
  	$this->db->from('v2_jobs job');
  	$this->db->join('ci_master_territory territory', 'territory.id = job.territory_id', 'left');
  	$this->db->join('ci_master_concept concept', 'concept.id = job.concept_id', 'left');
	$this->db->where('job.status', 1);
  	if(isset($filters['concept']))
    	$this->db->where('concept.id', $filters['concept']);
    if(isset($filters['country']))
    	$this->db->where('territory.id', $filters['country']);
    if(isset($filters['search_term']))
	{
		$this->db->like('job.name', $filters['search_term']);
		$this->db->or_like('job.description', $filters['search_term']);
	}
  	$this->db->order_by('job.date_posted', 'DESC');
	$this->db->limit($limit,$offset);
  	$query = $this->db->get();
  	return $query->result_array();
  }
  
  function searchJobs($limit = 10, $offset = 0, $search_term)
  {
	$this->db->select('name as label, name as value, job_id as link');
	$this->db->from('v2_jobs');
	$this->db->where('status', 1);
	$this->db->like('name', $search_term);
    $this->db->order_by('name', 'ASC');
	//$this->limits($limit, $offset);
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result_array();
  }

  function getLiveJob($jcode) {

    /*echo $jcode;
    die();*/

    $arrXml = $this->getFeed();
    /*echo '<pre>';
    print_r($arrXml);
    echo '</pre>';*/
    $job = array();

    foreach($arrXml['job'] as $list ){
      $job_code = $list['@attributes']['code'];

      if($job_code  != $jcode){
          continue;
      }

      //print_r($list);

      $job_name = ($list['name'])?$list['name']:'Not available';
      $job['job_name'] = $job_name;

      //$job_name = ucwords(strtolower($job_name));
      $job_concept = ($list['businessunit'])?$list['businessunit']:'Not available';


      $job_concept_print = strtolower($job_concept);
      $job_concept_print = ucwords($job_concept_print);
      if ($job_concept_print=='Lmi'){
          $job_concept_print = 'Landmark International';
      }
      $job['job_concept_print'] = $job_concept_print;


      $job_concept = strtoupper($job_concept);
      $job['job_concept'] = $job_concept;

      $job_region = ($list['region'])?$list['region']:'Not available';
      $job['job_region'] = $job_region;

      $job_country = ($list['country'])?$list['country']:'Not available';
      $job['job_country'] = $job_country;

      $job_type = ($list['type'])?ucwords(strtolower($list['type'])):'Not available';
      $job['job_type'] = $job_type;

      $job_opening = ($list['openings'])?$list['openings']:'0';
      $job['job_opening'] = $job_opening;

      $posted_date = date('d M Y',strtotime($list['dateposted']));
      $job['posted_date'] = $posted_date;

      $job_brief = (count($list['description']))?($list['description']):'Not available';
      if ($job_brief==NULL){
          $job_brief = "Not available";
      }
      $job['job_brief'] = $job_brief;

      $job_nationality = ($list['nationality'])?$list['nationality']:'Not available';
      $job['job_nationality'] = $job_nationality;

      $job_experience = ($list['experience'])?$list['experience']:'Not available';
      $job['job_experience'] = $job_experience;

      $job_qualification = (count($list['qualifications']))? ($list['qualifications']):'Not available';
      if ($job_qualification==NULL){
          $job_qualification = "Not available";
      }
      $job['job_qualification'] = $job_qualification;

      $job_skills = (count($list['skills']))?($list['skills']):'Not available';
      if ($job_skills==NULL){
          $job_skills = "Not available";
      }
      $job['job_skills'] = $job_skills;

      if ($job_country=='United Arab Emirates'){
          $job_country = 'UAE';
      }
      $job_country = (strlen($job_country) > 30)? substr($job_country,0,30).'..':$job_country;
      $job['job_country'] = $job_country;

      break;

    }

    echo '<pre>';
    print_r($job);
    echo '</pre>';

    return $job;

  }

  function getLocalJob($id) {
  	$this->db->select('job.job_id as id, job.experience, job.skills, job.qualifications, job.name as title, job.description, job.date_posted, job.no_of_openings, job.region, concept.name as concept, territory.name as territory, job.code');
  	$this->db->from('v2_jobs job');
  	$this->db->join('ci_master_territory territory', 'territory.id = job.territory_id', 'left');
  	$this->db->join('ci_master_concept concept', 'concept.id = job.concept_id', 'left');
    $this->db->where(array('job_id' => $id));
	$this->db->where('job.status', 1);
    $query = $this->db->get();
    foreach($query->result_array() as $job)
    {
      return $job;
    }

  }

  function convert_object_to_array($arrObjData, $arrSkipIndices = array()) {

    $arrData = array();
    if (is_object($arrObjData)) {
      $arrObjData = get_object_vars($arrObjData);
    }
    if (is_array($arrObjData)) {
    //echo '!!!'.$level.'<xmp>'.print_r( $arrObjData,1).'</xmp>';
      foreach ($arrObjData as $index => $value) {

        if (is_object($value) || is_array($value)) {
          $value = $this->convert_object_to_array($value, $arrSkipIndices);
        }
        if (in_array($index, $arrSkipIndices)) {
          continue;
        }
        $arrData[$index] = $value;

      }

    }
    return $arrData;
  }

  function aasort(&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $date = strtotime($va[$key]);
        $sorter[$ii]=$date;
    }
    arsort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    return $ret;
  }

  function getJobsListingCompact($limit = 20, $offset = 0, $filters = array()) {
    $this->db->select('job.job_id as id, job.name as title, job.date_posted, job.no_of_openings, job.region, job.code, concept.name as concept, territory.name as territory');
    $this->db->from('v2_jobs job');
    $this->db->join('ci_master_territory territory', 'territory.id = job.territory_id', 'left');
    $this->db->join('ci_master_concept concept', 'concept.id = job.concept_id', 'left');
    $this->db->where('job.status', 1);
    if(isset($filters['concept']))
      $this->db->where('concept.id', $filters['concept']);
    if(isset($filters['country']))
      $this->db->where('territory.id', $filters['country']);
    if(isset($filters['search_term']))
    {
      $this->db->like('job.name', $filters['search_term']);
      $this->db->or_like('job.description', $filters['search_term']); 
    }

    $this->db->order_by('job.date_posted', 'DESC');
    $this->db->limit($limit,$offset);
    $query = $this->db->get();
    return $query->result_array();
  }


}