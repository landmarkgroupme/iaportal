<?php
/**
  * This is a LoginTracker model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class LoginTracker extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_login_tracker');
	}

}