<?php
/**
  * This is a Department model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class SurveyQuestion extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_survey_questions');
	}

  function getQuestions($survey_id, $category_id)
  {
	$question_categories = array();
    $this->db->select('id, title,question_tips');
    $this->db->from($this->table);
    $this->db->where('survey_id', $survey_id);
    $this->db->where('category_id', $category_id);
    $this->db->where('status', 'active');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get();
    $question_categories = $query->result_array();
   if(isset($question_categories[0])){
	foreach($question_categories as $key => $value)
	{
		$question_categories[$key]['question_tips'] = htmlentities(stripslashes($value['question_tips']));
		$question_categories[$key]['title'] = htmlentities(stripslashes($value['title']));
		
	}
	}
	return $question_categories;
  }

  function getSurveyQuestions($survey_id = false)
  {
	$question_categories = array();
    $this->db->select('question.id, question.title, cat.id as category_id, cat.title as category_name, subcat.id as subcategory_id, subcat.title as subcategory_name,question.question_tips');
    $this->db->from($this->table. ' question');
    $this->db->join('v2_survey_question_category subcat', 'subcat.id = question.category_id');
    $this->db->join('v2_survey_question_category cat', 'cat.id = subcat.parent_id');
    if(isset($survey_id) && !empty($survey_id))
    {
      $this->db->where('survey_id', $survey_id);
    }
    
    $query = $this->db->get();
    
    //return $query->result();
	$question_categories = $query->result();
/*	print "<pre>";
	print_r($question_categories);
	exit;*/
	 if(isset($question_categories[0])){
	foreach($question_categories as $key => $value)
	{
		$question_categories[$key]->question_tips = htmlentities(stripslashes($value->question_tips));
		$question_categories[$key]->title = htmlentities(stripslashes($value->title));
		
	}
	}
	return $question_categories;
  }
  function getSurveyQuestionsCount($survey_id){
    $this->db->select('count(id) as total');
    $this->db->from($this->table);
    $this->db->where('survey_id',$survey_id);
    $query = $this->db->get();
    $data = $query->result();
    if(isset($data) && !empty($data))
    {
      return $data[0]->total;
    }
    else
    {
      return 0;
    }
  }
  

}