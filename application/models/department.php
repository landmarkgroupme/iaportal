<?php
/**
  * This is a Department model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Department extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_department');
	}

  function getNamesArray() {
    $this->db->select('id, name');
    $this->db->from($this->table);
    $query = $this->db->get();
    $desigs = $query->result();
    
    $data = array();
    foreach($desigs as $des){
      $data[$des->id] = $des->name;
    }
    return $data;
  }

}