<?php
/**
  * This is a ConceptMember model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class ObjectMember extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_map_objects');
	}

	function getMembers($parent_type, $parent_id, $limit = null, $offset = null)
	{
		$this->db->select('LOWER(u.reply_username) as reply_username, u.display_name as fullname, u.profile_pic,u.id as user_id ');
		$this->db->from('ci_users u');
		$this->db->join('ci_map_objects map', "map.object_id = u.id and map.object_type = 'User'");
		$this->db->where('u.status', 'active');
		$this->db->where('map.parent_type', $parent_type);
		$this->db->where('map.parent_id', $parent_id);
		$this->limits($limit, $offset);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getMembersByFilter($parent_type, $parent_id, $name, $limit = null, $offset = null)
	{
	  $my_user = $this->session->userdata("logged_user");
    $my_user_id = $my_user['userid'];
   
		$this->db->select('u.first_name,u.last_name,u.id');
		$this->db->from('ci_users u');
		$this->db->join('ci_map_objects map', "map.object_id = u.id and map.object_type = 'User'");
		$this->db->where('u.status', 'active');
    $this->db->where('u.id !=', $my_user_id);
		$this->db->where('map.parent_type', $parent_type);
		$this->db->where('map.parent_id', $parent_id);
    $where = "(reply_username LIKE '%".$name."%' 
    OR first_name LIKE '%".$name."%'
    OR last_name LIKE '%".$name."%')";
    $this->db->where($where);
    $this->db->group_by('u.id');    
		$this->limits($limit, $offset);
		$query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
	}
	function getFollowingConcepts($user_id, $limit = null, $offset = null)
	{
		$this->db->select('c.id, c.name , c.slug, c.thumbnail');
		$this->db->from('ci_master_concept c');
		$this->db->join('ci_map_objects map', "map.parent_id = c.id and map.parent_type = 'Concept'");
		$this->db->where('c.status', 'active');
		$this->db->where('map.object_type', 'User');
		$this->db->where('map.object_id', $user_id);
		$this->db->order_by('c.name', 'ASC');
		$this->limits($limit, $offset);
		$query = $this->db->get();
		return $query->result_array();
	}


  function followObject($details)
  {
  	if($this->add($details))
  	{
  		if($details['parent_type'] == 'Concept')
  		{
  			$this->load->model('Concept');
  			$this->Concept->increaseFollowers($details['parent_id']);
	    	return true;
  		}
		  else if($details['parent_type'] == 'Group')
  		{
  		  if($details['object_type'] != 'File')
        {
          $this->load->model('Group');
    			$this->Group->increaseFollowers($details['parent_id']);
        }
	    	return true;
  		}
  		return true;
  	}
  	return false;
  }

  function unFollowObject($details)
  {
  	if($this->db->delete($this->table, $details))
  	{
  		if($details['parent_type'] == 'Concept')
  		{
  			$this->load->model('Concept');
  			$this->Concept->decreaseFollowers($details['parent_id']);
	    	return true;
  		}
		else if($details['parent_type'] == 'Group')
  		{
  			$this->load->model('Group');
  			$this->Group->decreaseFollowers($details['parent_id']);
	    	return true;
  		}
  		return true;
  	}
  	return false;
  }


}