<?php
/**
  * This is a sample model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Sample extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('table_name');
	}

}