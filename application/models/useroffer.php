<?php
/**
  * This is a UserOffer model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class UserOffer extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_offers_users');
	}

  /**
  * get User Subcribed Concepts for Offers
  *
  * Names in v1: offers_model -> offer_user_list
  * @return array
  */
  function getUserOfferConcepts($user_id) {
    $this->db->select('concept_id');
    $this->db->from($this->table);
    $this->db->where('user_id', $user_id);
    $query = $this->db->get(); 
    return $query->result_array();
  }

  /**
  * Remove User form Offers
  *
  * Names in v1: offers_model -> flush_offer_user_map
  * @return array
  */
  function removeUserOffers($user_id) {
    $this->db->where('user_id', $user_id);
    if($this->db->delete($this->table))
    {
      return true;
    } else {
      return false;
    }
  }



}