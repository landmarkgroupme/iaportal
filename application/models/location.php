<?php
/**
  * This is a Location model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Location extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_location');
	}

}