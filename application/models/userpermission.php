<?php
/**
  * This is a UserPermission model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class UserPermission extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_permissions');
	}

  /**
  * get User Permissions
  *
  * Names in v1: sso_model -> permissions
  * @return array
  */
  function getUserPermissions($user_id) {
    $this->db->select('announcements, news, offers, photos, events, users, review, moderate');
    $this->db->from($this->table);
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();
    $data = $query->result_array();
    if(!empty($data))
    	return $data[0];
    else
    	return false;
  }

}