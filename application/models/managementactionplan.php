<?php
/**
  * This is a Management Action Plan model.
  *
  * @author  Sandeep Kumbhatil <sandeep.kumbhatil@intelliswift.co.in>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class ManagementActionPlan extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_management_action_plan');
	}
  function getManagementActionPlan($filter)
  {
    $this->db->select('map.*,c.title as qustion_category');
    $this->db->from($this->table.' map');
    $this->db->join('v2_survey_question_category c','map.category_id = c.id');
    if(isset($filter['survey_id']) && !empty($filter['survey_id']))
    {
      $this->db->where('survey_id', $filter['survey_id']);
    }
    if(isset($filter['user_id']) && !empty($filter['user_id']))
    {
      $this->db->where('user_id', $filter['user_id']);
    }
    if(isset($filter['category_id']) && !empty($filter['category_id']))
    {
      $this->db->where('category_id', $filter['category_id']);
    }
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
  function createManagementActionPlan($data)
  {
    $this->add($data);
  }
  function createManagementActionPlanAnswers($data)
  {
    $sql = 'insert into v2_managment_action_plan_answers (user_id, question_id, answer, actionplan,responsibility, actionplan_dttm,answered_nodate, 	answered_yesdate,na_reason,status) values('.$data['user_id'].','.$data['question_id'].",'".$data['answer']."','".$data['actionplan']."','".$data['responsibility']."','".$data['actionplan_dttm']."','".$data['answered_nodate']."','".$data['answered_yesdate']."','".$data['na_reason']."','".$data['status']."') on duplicate key update answer=values(answer), actionplan=values(actionplan),responsibility=values(responsibility), actionplan_dttm=values(actionplan_dttm),answered_nodate=values(answered_nodate),answered_yesdate=values(answered_yesdate),na_reason=values(na_reason),status=values(status)";
    $query = $this->db->query($sql);
  }
  function getManagementActionPlanAnswers($filter){
    $this->db->select('*');
    $this->db->from('v2_managment_action_plan_answers');
    if(isset($filter['user_id']) && !empty($filter['user_id']))
    {
      $this->db->where('user_id', $filter['user_id']);
    }
    if(isset($filter['question_id']) && !empty($filter['question_id']))
    {
      $this->db->where('question_id', $filter['question_id']);
    }
    $query = $this->db->get();
    return $query->result();
  }
}