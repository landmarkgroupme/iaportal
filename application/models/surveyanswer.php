<?php
/**
  * This is a Department model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class SurveyAnswer extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_survey_answers');
	}

  function getAnswers($survey_id)
  {
    $this->db->select('id, title');
    $this->db->from($this->table);
    $this->db->where('survey_id', $survey_id);
    $this->db->where('category_id', $category_id);
    $this->db->where('status', 'active');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  function getSurveyQuestions($survey_id)
  {
    $this->db->select('question.id, question.title, cat.id as category_id, cat.title as category_name, subcat.id as subcategory_id, subcat.title as subcategory_name');
    $this->db->from($this->table. ' question');
    $this->db->join('v2_survey_question_category subcat', 'subcat.id = question.category_id');
    $this->db->join('v2_survey_question_category cat', 'cat.id = subcat.parent_id');
    $this->db->where('survey_id', $survey_id);
    
    $query = $this->db->get();
    return $query->result();
  }
  
  function getSurveyAnswers($survey_id = false, $user_id = false){
    $this->db->select("answer.*,question.title as questions,question.question_tips", false);
    $this->db->from('v2_survey_answers answer');
    $this->db->join('v2_survey_questions question', 'question.id = answer.question_id');
    $this->db->join('v2_surveys survey', 'survey.id = question.survey_id');
    if(isset($survey_id) && !empty($survey_id))
    {
      $this->db->where('survey.id', $survey_id);
    }
    if(isset($user_id) && !empty($user_id))
    {
      $this->db->where('answer.user_id', $user_id);
    }
    $this->db->where_in('answer.answer', array('No'));
    $this->db->group_by('question.id');
    $query = $this->db->get();
    return $query->result();
  }
  function getSurveyAnswersCount($survey_id = false, $user_id = false){
    $this->db->select('COUNT(answer.id) as total');
    $this->db->from('v2_survey_answers answer');
    $this->db->join('v2_survey_questions question', 'question.id = answer.question_id');
    $this->db->join('v2_surveys survey', 'survey.id = question.survey_id');
    $this->db->where('survey.id', $survey_id);
    $this->db->where('answer.user_id', $user_id);
    $this->db->group_by('answer.user_id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    $data = $query->result();
    if(isset($data) && !empty($data))
    {
      return $data[0]->total;
    }
    else
    {
      return 0;
    }
  }
  function getSurveyManagementActionPlanQuestion($filter = array()){
    
    $this->db->select("CONCAT(first_name,' ',last_name) as manager_name,outlet.outlet_name,question.id, question.title, cat.id as category_id, cat.title as category_name, subcat.id as subcategory_id, subcat.title as subcategory_name,question.question_tips,answer.answer,answer.actionplan,answer.responsibility,answer.answered_nodate,answer.answered_yesdate,su.assigned_dttm as assessment_date,answer.actionplan_dttm", false);
    $this->db->from('v2_survey_answers answer');
    $this->db->join('v2_survey_questions question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category subcat', 'subcat.id = question.category_id');
    $this->db->join('v2_survey_question_category cat', 'cat.id = subcat.parent_id');
    $this->db->join('v2_surveys survey', 'survey.id = question.survey_id');
    $this->db->join('ci_master_outlets outlet','outlet.user_id = answer.user_id');
    $this->db->join('ci_users u','outlet.user_id = u.id');
    $this->db->join('v2_survey_users as su','outlet.user_id = su.user_id AND su.survey_id = survey.id');
    $this->db->not_like('answer.answer','N/A');
    $this->db->where('su.status','completed');
    $where = '';
    
    if((isset($filter['start_date']) && !empty($filter['start_date'])) && (isset($filter['end_date']) && !empty($filter['end_date'])))
    {
      /*$where = 'CASE 
      WHEN answer.answer = "No" THEN answer.answered_nodate >= "'.$filter['start_date'].'" AND answer.answered_nodate <= "'.$filter['end_date'].'" 
      WHEN answer.answer = "Yes" THEN answer.answered_yesdate >= "'.$filter['start_date'].'" AND answer.answered_yesdate <= "'.$filter['end_date'].'" 
      END';*/
      $where = '';
      $where = 'survey.create_dttm >= \''.$filter['start_date'].'\' AND survey.create_dttm <= \''.$filter['end_date'].'\'';
      $this->db->where($where,NULL,false);
    } 
    if(isset($filter['concept']) && !empty($filter['concept']))
    {
      $this->db->where('survey.concept_id',$filter['concept']);
    }
    if(isset($filter['outlet']) && !empty($filter['outlet']))
    {
      $this->db->where('outlet.id',$filter['outlet']);
    }
    if(isset($filter['territory']) && !empty($filter['territory']))
    {
      $this->db->where('outlet.territory_id',$filter['territory']);
    }
    $where = "CASE WHEN answer.answer = 'Yes' THEN answer.answered_nodate < answer.answered_yesdate ELSE 1=1 END";
    $this->db->where($where,null,false);
    $this->db->group_by('question.id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
    
    
  }
  
  function getSurveyDraftAnswers($survey_id = false, $user_id = false){
    $this->db->select("answer.*,question.title as questions,question.question_tips", false);
    $this->db->from('v2_survey_answers answer');
    $this->db->join('v2_survey_questions question', 'question.id = answer.question_id');
    $this->db->join('v2_surveys survey', 'survey.id = question.survey_id');
    if(isset($survey_id) && !empty($survey_id))
    {
      $this->db->where('survey.id', $survey_id);
    }
    if(isset($user_id) && !empty($user_id))
    {
      $this->db->where('answer.user_id', $user_id);
    }
    $this->db->where('answer.status',0);
    $this->db->group_by('question.id');
    $query = $this->db->get();
    return $query->result();
  }

  function saveSurveyAnswer($data)
  {
    $sql = 'insert into v2_survey_answers (user_id, question_id, answer, actionplan,responsibility, actionplan_dttm,answered_nodate, 	answered_yesdate,na_reason,status) values('.$data['user_id'].','.$data['question_id'].",'".$data['answer']."','".$data['actionplan']."','".$data['responsibility']."','".$data['actionplan_dttm']."','".$data['answered_nodate']."','".$data['answered_yesdate']."','".$data['na_reason']."','".$data['status']."') on duplicate key update answer=values(answer), actionplan=values(actionplan),responsibility=values(responsibility), actionplan_dttm=values(actionplan_dttm),answered_nodate=values(answered_nodate),answered_yesdate=values(answered_yesdate),na_reason=values(na_reason),status=values(status)";
    $query = $this->db->query($sql);

  }
  function saveSurveyActionPlan($values)
  {
    $data = array( 
      'actionplan' => $values['actionplan'],
      'responsibility' => $values['responsibility'],
      'actionplan_dttm' => $values['actionplan_dttm'],
      'answered_yesdate' => $values['answered_yesdate'],
    );    
    $this->update($data,$values['id']);
        
  }
  function updateSurveyAnswer($id,$filter){
    $this->update($filter,$id);
  }
  function surveyResult($survey_id, $user_id)
  {
    $this->db->select("c.title as qustion_category,c.id as category_id,count(answer.id) as total_controls, sum(if(answer.answer = 'No', 1, 0)) as total_no, sum(if(answer.answer = 'Yes', 1, 0)) as total_yes,question.survey_id,answer.user_id ", false);
    $this->db->from('v2_survey_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id','LEFT');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id','LEFT');
    $this->db->not_like('answer.answer','N/A');
    if(isset($survey_id) && !empty($survey_id))
    {
      $this->db->where('question.survey_id',$survey_id);
    }
    if(isset($user_id) && !empty($user_id))
    {
      $this->db->where('answer.user_id',$user_id);
    }
    $this->db->group_by('c.id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
  function surveyResultbyConcept($conceptid = false,$territoryid = false,$dateFilter = false,$outletid = false){
    $this->db->select("o.id,round(sum(if(answer.answer = 'No', 1, 0)) / count(answer.id) * 100) as ".$dateFilter['type']."_percentage,o.outlet_name", false);
    $this->db->from('v2_managment_action_plan_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id');
    $this->db->join('v2_surveys as s', 'question.survey_id = s.id');
    $this->db->join('ci_master_outlets as o', 'answer.user_id = o.user_id');
    $this->db->join('v2_survey_users as su','o.user_id = su.user_id AND su.survey_id = s.id');
    $this->db->join('ci_master_territory as t', 'o.territory_id = t.id');
    $this->db->not_like('answer.answer','N/A');
    $this->db->where('su.status','completed');
    if(isset($territoryid) && !empty($territoryid))
    {
      $this->db->where('o.territory_id', $territoryid);
    }
    if(isset($conceptid) && !empty($conceptid))
    {
      $this->db->where('o.concept_id', $conceptid);
    }
    if(isset($outletid) && !empty($outletid))
    {
      $this->db->where('o.id', $outletid);
    }
    if(isset($dateFilter) && !empty($dateFilter))
    {
      $where = 's.create_dttm >= \''.$dateFilter['startDate'].'\' AND s.create_dttm <= \''.$dateFilter['endDate'].'\'';
      $this->db->where($where);
    }
    $this->db->group_by('o.id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
  function surveyResultbyTerritory($territoryid = false,$conceptid = false,$dateFilter = false,$outletid = false)
  {
    $this->db->select("o.territory_id,t.name as territory_name,c.title as qustion_category,c.id as category_id,round(sum(if(answer.answer = 'No', 1, 0)) / count(answer.id) * 100) as percentage, count(answer.id) as total_controls,sum(if(answer.answer = 'No', 1, 0)) as total_no", false);
    $this->db->from('v2_managment_action_plan_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id');
    $this->db->join('v2_surveys as s', 'question.survey_id = s.id');
    $this->db->join('ci_master_outlets as o', 'answer.user_id = o.user_id');
    $this->db->join('ci_master_territory as t', 'o.territory_id = t.id');
    $this->db->join('v2_survey_users as su','o.user_id = su.user_id AND su.survey_id = s.id');
    $this->db->where('su.status','completed');
    $this->db->not_like('answer.answer','N/A');
    
    if(isset($territoryid) && !empty($territoryid))
    {
      $this->db->where('o.territory_id', $territoryid);
    }
    if(isset($conceptid) && !empty($conceptid))
    {
      $this->db->where('o.concept_id', $conceptid);
    }
    if(isset($outletid) && !empty($outletid))
    {
      $this->db->where('o.id', $outletid);
    }
    if(isset($dateFilter) && !empty($dateFilter))
    {
      $where = 's.create_dttm >= \''.$dateFilter['startDate'].'\' AND s.create_dttm <= \''.$dateFilter['endDate'].'\'';
      $this->db->where($where);
    }
    $this->db->group_by('c.id,o.territory_id');
    $query = $this->db->get();
    #echo $this->db->last_query();exit;
    return $query->result();
  }
  function surveyResultbyCategoryTerritory($categoryid = false,$territoryid = false,$conceptid = false,$dateFilter = false){
    $this->db->select("o.id,round(sum(if(answer.answer = 'No', 1, 0)) / count(answer.id) * 100) as percentage,o.outlet_name,c.title,c.id as category_id,t.name as territory_name", false);
    $this->db->from('v2_managment_action_plan_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id');
    $this->db->join('v2_surveys as s', 'question.survey_id = s.id');
    $this->db->join('ci_master_outlets as o', 'answer.user_id = o.user_id');
    $this->db->join('ci_master_territory as t', 'o.territory_id = t.id');
    $this->db->join('v2_survey_users as su','o.user_id = su.user_id AND su.survey_id = s.id');
    $this->db->where('su.status','completed');
    $this->db->not_like('answer.answer','N/A');
    
    if(isset($territoryid) && !empty($territoryid))
    {
      $this->db->where('o.territory_id', $territoryid);
    }
    if(isset($conceptid) && !empty($conceptid))
    {
      $this->db->where('o.concept_id', $conceptid);
    }
    if(isset($outletid) && !empty($outletid))
    {
      $this->db->where('o.id', $outletid);
    }
    if(isset($categoryid) && !empty($categoryid))
    {
      $this->db->where('c.id', $categoryid);
    }
    if(isset($dateFilter) && !empty($dateFilter))
    {
      $where = 's.create_dttm >= \''.$dateFilter['startDate'].'\' AND s.create_dttm <= \''.$dateFilter['endDate'].'\'';
      $this->db->where($where);
    }
    $this->db->group_by('c.id,o.territory_id,o.id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result();
  }
  function surveyResultbyOutlets($outletid = false,$territoryid = false,$dateFilter = false)
  {
    $this->db->select("CONCAT(u.first_name,' ',u.last_name) as manager_name,o.id,o.territory_id,t.name as territory_name,c.title as qustion_category,c.id as category_id,round(sum(if(answer.answer = 'No', 1, 0)) / count(answer.id) * 100) as percentage,count(question.id) as total_controls,sum(if(answer.answer = 'No', 1, 0)) as total_no,o.outlet_name,su.assigned_dttm as assessment_date", false);
    $this->db->from('v2_managment_action_plan_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id');
    $this->db->join('v2_surveys as s', 'question.survey_id = s.id');
    $this->db->join('ci_master_outlets as o', 'answer.user_id = o.user_id');
    $this->db->join('ci_master_territory as t', 'o.territory_id = t.id');
    $this->db->join('ci_users as u','o.user_id = u.id');
    $this->db->join('v2_survey_users as su','o.user_id = su.user_id AND su.survey_id = s.id');
    $this->db->where('su.status','completed');
    $this->db->not_like('answer.answer','N/A');
    
    if(isset($territoryid) && !empty($territoryid))
    {
      $this->db->where('o.territory_id', $territoryid);
    }
    if(isset($outletid) && !empty($outletid))
    {
      $this->db->where('o.id', $outletid);
    }
    if(isset($dateFilter) && !empty($dateFilter))
    {
      $where = 's.create_dttm >= \''.$dateFilter['startDate'].'\' AND s.create_dttm <= \''.$dateFilter['endDate'].'\'';
      $this->db->where($where);
    }
    $this->db->group_by('c.id,o.id');
    $query = $this->db->get();
    #echo $this->db->last_query();exit;
    return $query->result();
  }
  function getRiskControlMatrix($outletId = false,$dateFilter = false)
  {
    $this->db->select("CONCAT(u.first_name,' ',u.last_name) as manager_name,o.id as outletid,o.outlet_name,c.id as risk_id,c.title as risk_impact,qc.id as desc_id,qc.title as risk_desc,answer.answer, answer.actionplan, answer.responsibility, answer.actionplan_dttm,question.id as question_id,question.title as questions,su.assigned_dttm as assessment_date", false);
    $this->db->from('v2_managment_action_plan_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id');
    $this->db->join('v2_surveys as s', 'question.survey_id = s.id');
    $this->db->join('ci_master_outlets as o', 'answer.user_id = o.user_id');
    $this->db->join('ci_users as u','o.user_id = u.id');
    $this->db->join('v2_survey_users as su','o.user_id = su.user_id AND su.survey_id = s.id');
    $this->db->where('su.status','completed');
    #$this->db->not_like('answer.answer','N/A');
    if(isset($outletId) && !empty($outletId))
    {
      $this->db->where('o.id',$outletId);
    }
    if(isset($dateFilter) && !empty($dateFilter))
    {
      $where = 's.create_dttm >= \''.$dateFilter['startDate'].'\' AND s.create_dttm <= \''.$dateFilter['endDate'].'\'';
      $this->db->where($where);
    }
    $this->db->order_by('risk_id');
    $query = $this->db->get();
    #echo $this->db->last_query();exit;
    return $query->result();
  }
  function getRiskControlMatrixByOutlet($outlet_id,$category_id,$dateFilter = false){
    $this->db->select("o.outlet_name,CONCAT(u.first_name,' ',u.last_name) as manager_name,c.id as risk_id,c.title as risk_impact,qc.id as desc_id,qc.title as risk_desc,answer.answer, answer.actionplan, answer.responsibility, answer.actionplan_dttm,question.id as question_id,question.title as questions,su.assigned_dttm as assessment_date,answer.na_reason", false);
    $this->db->from('v2_managment_action_plan_answers answer');
    $this->db->join('v2_survey_questions as question', 'question.id = answer.question_id');
    $this->db->join('v2_survey_question_category as qc', 'question.category_id = qc.id');
    $this->db->join('v2_survey_question_category as c', 'qc.parent_id = c.id');
    $this->db->join('v2_surveys as s', 'question.survey_id = s.id');
    $this->db->join('ci_master_outlets as o', 'answer.user_id = o.user_id');
    $this->db->join('ci_users as u','u.id = o.user_id');
    $this->db->join('v2_survey_users as su','o.user_id = su.user_id AND su.survey_id = s.id');
    $this->db->where('su.status','completed');
    //$this->db->not_like('answer.answer','N/A');
    if(isset($outlet_id) && !empty($outlet_id))
    {
      $this->db->where('o.id',$outlet_id);
    }
    if(isset($category_id) && !empty($category_id))
    {
      $this->db->where('c.id',$category_id); 
    }
    if(isset($dateFilter) && !empty($dateFilter))
    {
      $where = 's.create_dttm >= \''.$dateFilter['startDate'].'\' AND s.create_dttm <= \''.$dateFilter['endDate'].'\'';
      $this->db->where($where);
    }
    $query = $this->db->get();
    #echo $this->db->last_query();exit;
    return $query->result();
  }
}