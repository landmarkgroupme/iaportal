<?php
/**
  * This is a Category model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Category extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_category');
	}
  function getCategories(array $sortby = null){
    $sql = 'cat.*';
  	$this->db->select($sql);
  	$this->db->from($this->table.' cat');
    if(isset($sortby))
    {
      foreach($sortby as $column => $order )
      {
        $this->db->order_by($column, $order);
      }

    } else {
      $this->db->order_by('name', 'Asc');
    }
    $query = $this->db->get();
    return $query->result_array();
  }

}