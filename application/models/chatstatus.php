<?php
/**
  * This is a ChatStatus model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class ChatStatus extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('cometchat_status');
	}

  /**
  * Check User Comet Chat Status by User Id
  *
  * @return int
  */
  function CheckUserChatStatus($user_id) {
    $data = array(
        'userid' => $user_id
    );
    $chat = $this->getByFilters($data);
    return $chat === false ? 0 : 1;
  }

}