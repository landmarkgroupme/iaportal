<?php
/**
  * This is a Entry model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Entry extends Crud {
	function __construct() {
		parent::__construct();
    	$this->setTable('mt_entry');
	}

  /**
  * Returns an Entry Details from MT Entry Table
  *
  * Names in v1: get_mt_entry_details
  * @return array
  */
  
  public function remove($id)
  {
    $this->db->where('entry_id', $id);
    if($this->db->delete($this->table))
    {
      return true;
    } else {
      return false;
    }
  }
  
  function get($entry_id)
  {
    $this->db->select('e.entry_id, e.entry_title, e.entry_text, e.entry_text_more, e.entry_text, c.category_label, entry_created_on, entry_excerpt');
    $this->db->from("$this->table e");
    $this->db->join('mt_placement p', 'p.placement_entry_id = e.entry_id');
    $this->db->join('mt_category c', "c.category_id = p.placement_category_id and c.category_class = 'category'");
    //$this->db->where('e.entry_status', 2);
    $this->db->where('e.entry_id', $entry_id);
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * Returns an Entry IDs and Titles from MT Entry Table based on Category name
  *
  * Names in v1: get_category_title_from_mt
  * @return array
  */
  function getEntryTitlesByCategory($mt_category) {
    $this->db->select('e.entry_id, e.entry_title');
    $this->db->from("$this->table e");
    $this->db->join('mt_placement p', 'p.placement_entry_id = e.entry_id');
    $this->db->join('mt_category c', "c.category_id = p.placement_category_id and c.category_class = 'category'");
    $this->db->where('e.entry_status', 2);
    $this->db->where('c.category_basename', $mt_category);
    $this->db->order_by('e.entry_id', 'desc');
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * get List of Entries
  *
  * @return array
  */
  function getEntries($category, $limit = null, $offset = null, $concept = null)
  {
    $this->db->select('e.entry_id, e.entry_title, e.entry_text_more, e.entry_created_on, cp.name');
    $this->db->from("$this->table e");
    $this->db->join('mt_placement p', 'p.placement_entry_id = e.entry_id');
    $this->db->join('ci_master_concept cp', 'cp.id  = e.concept_id');
    $this->db->join('mt_category c', "c.category_id = p.placement_category_id and c.category_class = 'category' and c.category_basename = '$category'");
    $this->db->where('e.entry_status', 2);
    if($concept != null)
    {
      $this->db->where('cp.id', $concept);
    }
    $this->db->order_by('e.entry_id', 'desc');
    $this->limits($limit, $offset);
    $this->db->order_by('e.entry_id', 'DESC');
    $query = $this->db->get();
    /// echo $this->db->last_query();
    return $query->result_array();
  }
}