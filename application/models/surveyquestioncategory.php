<?php
/**
  * This is a Department model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class SurveyQuestionCategory extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_survey_question_category');
	}

  function getCategories($parent_id = 0)
  {
	$question_categories = array();
    $this->db->select('id, title');
    $this->db->from($this->table);
    $this->db->where('parent_id', $parent_id);
    $query = $this->db->get();
    ///return $query->result_array();
	$question_categories = $query->result_array();
   if(isset($question_categories[0])){
	foreach($question_categories as $key => $value)
	{
		$question_categories[$key]['title'] = stripslashes($value['title']);	
	}
	}
	return $question_categories;
  }

}