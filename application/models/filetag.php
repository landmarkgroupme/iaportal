<?php
/**
  * This is a FileTag model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class FileTag extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_map_files_tag');
	}

  /**
  * get list of tags for a given file
  *
  * Names in v1: files_get_tags_for_file
  * @return array
  */
  function getFileTags($file_id)
  {
    $this->db->select('t.name, t.id');
    $this->db->from("$this->table ft");
    $this->db->join('ci_master_file_tag t', 't.id = ft.file_tag_id');
    $this->db->where('ft.files_id', $file_id);
    $query = $this->db->get(); 
    return $query->result_array();
  }

  /**
  * get list of tags order by mostly used
  *
  * @return array
  */
  function getPopularTags($limit = 10,$group_id = false,$logged_id = false,$concept_id = false)
  {
    /*
    SELECT tag.name, count(filetag.id) as total FROM `ci_master_file_tag` tag 
    left join ci_map_files_tag filetag on tag.id = filetag.file_tag_id group by tag.id order by total desc
    */
    $groupFile = 0; 
    if(isset($group_id) && !empty($group_id))
    {
      $groupFile = 1;
    }
    $this->db->select('tag.id, tag.name, count(DISTINCT(f.id)) as total', false);
    $this->db->from("ci_master_file_tag tag");
    $this->db->join('ci_map_files_tag filetag', 'tag.id = filetag.file_tag_id', 'left');
    $this->db->join('ci_files f', 'filetag.files_id = f.id AND f.group_file = '.$groupFile);
    $this->db->group_by('tag.id');
    $this->db->order_by('total', 'desc');
    $this->db->where('tag.name <>', '');
    $this->db->where('f.files_status', 1);
    if(isset($group_id) && !empty($group_id))
    {
      $this->db->where('tag.group_id', $group_id);
      if(isset($logged_id) && !empty($logged_id))
      {
        $this->db->join('ci_map_objects map','f.id = map.parent_id AND map.parent_type = \'File\'','LEFT');
        $where = 'CASE WHEN f.have_assignee = 1 THEN map.object_type = \'User\' AND map.object_id = '.$logged_id.' ELSE 1=1 END';
        $this->db->where($where,null,FALSE);
      }
    }
    if(isset($concept_id) && !empty($concept_id))
    {
      $this->db->where('f.concept_id',$concept_id);
    }
    if(isset($limit) && !empty($limit))
    {
      $this->limits($limit, 0);
    }
    #$this->db->group_by('tag.id,f.id');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result_array();
  }
  function getConceptPerGroupTags($group_id = false, $concept_id = false, $logged_id = false,$limit = false)
  {
    /*
    SELECT tag.name, count(filetag.id) as total FROM `ci_master_file_tag` tag 
    left join ci_map_files_tag filetag on tag.id = filetag.file_tag_id group by tag.id order by total desc
    */
    $groupFile = 0; 
    if(isset($group_id) && !empty($group_id))
    {
      $groupFile = 1;
    }
    $this->db->select('tag.id, tag.name, count(filetag.id) as total', false);
    $this->db->from("ci_master_file_tag tag");
    $this->db->join('ci_map_files_tag filetag', 'tag.id = filetag.file_tag_id', 'left');
    $this->db->join('ci_files f', 'filetag.files_id = f.id AND f.group_file = '.$groupFile);
    $this->db->group_by('tag.id');
    $this->db->order_by('total', 'desc');
    $this->db->where('tag.name <>', '');
    $this->db->where('f.files_status', 1);
    if(isset($group_id) && !empty($group_id))
    {
      $this->db->where('tag.group_id', $group_id);
      if(isset($logged_id) && !empty($logged_id))
      {
        $this->db->join('ci_map_objects map','f.id = map.parent_id AND map.parent_type = \'File\'','LEFT');
        $where = 'CASE WHEN f.have_assignee = 1 THEN map.object_type = \'User\' AND map.object_id = '.$logged_id.' ELSE 1=1 END';
        $this->db->where($where,null,FALSE);
      }
    }
    if(isset($concept_id) && !empty($concept_id))
    {
      $this->db->where('f.concept_id',$concept_id);
    }
    if(isset($limit) && !empty($limit))
    {
      $this->limits($limit, 0);
    }
    
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result_array();
  }

    

}