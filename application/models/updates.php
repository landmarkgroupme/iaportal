<?php

Class Updates extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function getGroupTagFile($group_id, $limit, $offset, $category, $tag, $logged_uid)
  {
    $sql = '';
    $sql = "
              SELECT
                a.id as id,
                FROM_UNIXTIME(a.uploaded_on,'%Y-%m-%d %H:%i:%s') as publish_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as thumbnail,
                NULL as concept_name,
                NULL as slug,
                NULL as object_id,
                NULL as object_type,
				a.path
              FROM
                ci_files a,
                ci_map_objects o,
                ci_map_files_tag m,
                ci_map_objects ob
              WHERE
                a.files_status = 1 AND
                a.id = o.object_id AND
                a.group_file = 1 AND
                o.object_type = 'File' AND
                o.parent_id = $group_id AND
                a.id = m.files_id AND
                m.file_tag_id = $tag AND
                o.parent_type = 'Group' AND
                CASE WHEN a.have_assignee = 1 THEN
                  (a.id = ob.parent_id AND
                  ob.parent_type = 'File' AND
                  ob.object_id = $logged_uid AND
                  ob.object_type = 'User')
                ELSE
                  1=1
                END
                group by a.id

            ";

    $sql .= " order by publish_date DESC LIMIT $offset, $limit";

    $union_query = $this->db->query($sql);


    $data = $union_query->result_array();

    $concepts = array();
    #echo "<pre>".print_r($data,"\n")."</pre>"; exit;
    foreach($data as $index => $item) {
      $concepts[] = $item;
    }
    return $concepts;
  }

  function getGroupFeed($group_id, $limit, $offset, $category, $logged_uid = false)
  {

    /* echo 'here';
     die();*/
    if(!isset($category) or $category == '') {

      $sql = "(
              SELECT
                stat.id,stat.featured,
                stat.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = stat.id and lll.status_type ='Status' and lll.user_id = '$logged_uid' limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                stat.object_id as user_id,
                user.first_name,
                user.last_name,
                user.display_name,
                user.profile_pic,
                user.email,
                NULL as designation,
                concept.thumbnail,
                concept.name as concept_name,
                concept.id as slug,
                stat.object_id,
                stat.object_type,
				NULL as path
              FROM
                ci_status_updates stat";
      //, ci_users_following_users fol
      $sql .= "
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User' and target_type = 'Group' and target_id = $group_id
                LEFT JOIN ci_interest_groups concept on concept.id = stat.target_id and stat.target_type = 'Group' and concept.id = $group_id


                WHERE
                stat.status = 1 ";

      $sql .= " AND (concept.id is not null) ";


      $sql .= " AND stat.parent_status_id = 0";
      // AND fol.follow_status = 1 AND fol.user_id = ? AND fol.following_user_id = s.object_id
      $sql .= " )UNION ALL(
              SELECT
                a.id as id,a.featured,
                FROM_UNIXTIME(a.uploaded_on,'%Y-%m-%d %H:%i:%s') as publish_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
				NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as thumbnail,
                NULL as concept_name,
                NULL as slug,
                NULL as object_id,
                NULL as object_type,
				a.path
              FROM
                ci_files a,
                ci_map_objects o,
                ci_map_objects ob
              WHERE
                a.files_status = 1 AND
                a.id = o.object_id AND
                a.group_file = 1 AND
                o.object_type = 'File' AND
                o.parent_id = $group_id AND
                o.parent_type = 'Group' AND
                CASE WHEN a.have_assignee = 1 THEN
                  (a.id = ob.parent_id AND
                  ob.parent_type = 'File' AND
                  ob.object_id = $logged_uid AND
                  ob.object_type = 'User')
                ELSE
                  1=1
                END
                group by a.id
            ) UNION ALL(
              SELECT
                p.poll_id as id,p.featured,				
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				NULL as you_like,
                NULL as total_likes,
                u.reply_username as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                u.display_name,
                u.profile_pic,
                NULL as email,
                NULL as designation,
				con.thumbnail,
				con.name as concept_name,
				NULL as slug,
                p.created_by as object_id,
                p.created_by_type as object_type,
				NULL as path
              FROM
			  v2_polls p
			  JOIN ci_interest_groups con on con.id = p.target_id and p.target_type = 'Group' and p.target_id = $group_id
			  LEFT JOIN ci_users u on u.id = p.created_by and p.created_by_type = 'User'
				)UNION ALL(
              SELECT
                b.pid as id,b.featured,
                b.mtime as publish_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = b.pid and lll.status_type ='Photo' and lll.user_id = '$logged_uid' limit 1) as you_like,
                b.total_likes,
                c.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                c.display_name,
                c.profile_pic,
				NULL as email,
                NULL as designation,
				 CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug,
				b.owner_id as object_id,
                b.owner_type as object_type,
				NULL as path

              FROM
                ci_users c,
                alb_pictures b
				JOIN ci_interest_groups pcon on pcon.id = b.target_id and b.target_type = 'Group'
              WHERE
                b.owner_id = c.id AND b.owner_type = 'User' AND b.target_type = 'Group' AND b.target_id = $group_id
            )
				";

      $sql .= " order by publish_date DESC LIMIT $offset, $limit";
      $union_query = $this->db->query($sql);


      $data = $union_query->result_array();

      $concepts = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
        }
        if($item['object_type'] == 'Group') {
          $item['profile_pic'] = 'images/groups/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'groups/' . $item['object_id'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }

        if(in_array($item['category'], array('news', 'offers', 'announcement'))) {

          $item['concept'] = $item['concept_name'];
          $item['concept_url'] = $item['slug'];
        }

        if($item['category'] == 'album') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
        }
        if($item['category'] == 'files') {
          if($item['file_size']) {
            $file_info = pathinfo($item['path']);
            $file_name = $file_info['filename'];
            $file_extension = $file_info['extension'];
            $item['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
          } else {
            $item['view_path'] = $item['path'];
          }
        }

        $concepts[] = $item;
      }
      /* "hi";
      print_r($concepts);
      exit;*/
      return $concepts;

    }
    if($category == 'status_update') {

      $this->db->select("stat.id,stat.featured,
                  stat.create_dttm as publish_date,
                  'status updates' as category,
                  stat.total_likes,
                  stat.id as message_id,
                  stat.message,
                  stat.object_id,
                  stat.object_type,
                  user.profile_pic,
                  user.display_name,
                  concept.thumbnail,
                  concept.name as concept_name,
                  concept.id as slug", false);
      $this->db->from('ci_status_updates stat');
      $this->db->join('ci_users user', "user.id = stat.object_id and stat.object_type = 'User' and target_type = 'Group' and target_id = " . $group_id, 'left');
      $this->db->join('ci_interest_groups concept', "concept.id = stat.object_id and stat.object_type = 'Group' and concept.id = " . $group_id, 'left');
      $this->db->where('stat.status', 1);
      $this->db->where('stat.parent_status_id', 0);
      //$this->db->where("((stat.object_type = 'Concept' and stat.object_id = ".$concept_id.") or ( stat.target_id = ".$concept_id." && stat.target_type = 'Concept'))");
      $this->db->where('(user.concept_id is not null or concept.id is not null)');
      $this->db->order_by('publish_date', 'desc');
      $this->db->limit($limit, $offset);
      $query = $this->db->get();

      //echo $this->db->last_query();
      $data = $query->result_array();

      //print_r($data);


      $concepts = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }
        $concepts[] = $item;
      }

      return $concepts;

    } elseif(in_array($category, array('news', 'offers', 'announcement'))) {

      $this->db->select(
        "e.entry_id as id,e.featured,
            e.entry_created_on as publish_date,
            e.entry_text_more as description,
            e.entry_title as title,
            e.entry_excerpt as album_id,
            c.category_basename as category,
            NULL as file_unique_id,
            NULL as file_size,
            NULL as following_user_id,
            NULL as you_like,
            NULL as total_likes,
            NULL as username,
            NULL as message_id,
            NULL as message,
            NULL as user_id,
            NULL as first_name,
            NULL as last_name,
            NULL as display_name,
            con.thumbnail as profile_pic,
            NULL as email,
            NULL as designation,
            con.name as concept,
      con.slug as concept_url", false
      );
      $this->db->from('mt_entry e');
      $this->db->join('ci_master_concept con', 'con.id = e.concept_id and con.id = ' . $concept_id);
      $this->db->join('mt_category c', "c.category_basename = '$category' AND c.category_class = 'category'", null, false);
      $this->db->join('mt_placement p', "c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id");
      $this->db->order_by('publish_date', 'desc');
      $this->db->limit($limit, $offset);

      $query = $this->db->get();
      $data = $query->result_array();
      return $data;

    } elseif(in_array($category, array('polls'))) {
      $sql = "
              SELECT
                p.poll_id as id,p.featured,				
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as total_likes,
                u.reply_username as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                u.display_name,
                u.profile_pic,
                NULL as email,
                NULL as designation,
				con.thumbnail,
				con.name as concept_name,
				NULL as slug,
                p.created_by as object_id,
                p.created_by_type as object_type,
				NULL as path
              FROM
			  v2_polls p
			  JOIN ci_interest_groups con on con.id = p.target_id and p.target_type = 'Group' and p.target_id = $group_id
			  LEFT JOIN ci_users u on u.id = p.created_by and p.created_by_type = 'User'
				";

      $sql .= " order by publish_date DESC LIMIT $offset, $limit";
      $query = $this->db->query($sql);
      $data = $query->result_array();
      // $query = $this->db->get();
      // $data =  $query->result_array();
      $return_data = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'concepts/' . $item['slug'];
        }
        if($item['object_type'] == 'Group') {
          $item['profile_pic'] = 'images/groups/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'groups/' . $item['object_id'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }
        $return_data[] = $item;
      }

      return $return_data;

    } elseif($category == 'files') {
      $sql = "SELECT
                a.id as id,a.featured,
                FROM_UNIXTIME(a.uploaded_on,'%Y-%m-%d %H:%i:%s') as publish_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as thumbnail,
                NULL as concept_name,
                NULL as slug,
                c.id as object_id,
                'User' as object_type,
				a.path
              FROM
                ci_files a,
                ci_map_objects o,
                ci_map_objects ob,
				ci_users c
              WHERE
				a.user_id = c.id AND
                a.files_status = 1 AND
                a.id = o.object_id AND
                a.group_file = 1 AND
                o.object_type = 'File' AND
                o.parent_id = $group_id AND
                o.parent_type = 'Group' AND
                CASE WHEN a.have_assignee = 1 THEN
                  (a.id = ob.parent_id AND
                  ob.parent_type = 'File' AND
                  ob.object_id = $logged_uid AND
                  ob.object_type = 'User')
                ELSE
                  1=1
                END
                group by a.id";

      $sql .= " order by publish_date DESC LIMIT $offset, $limit";
      $union_query = $this->db->query($sql);
      $data = $union_query->result_array();
      foreach($data as $key => $file) {
        if($file['file_size']) {
          $file_info = pathinfo($file['path']);
          $file_name = $file_info['filename'];
          $file_extension = $file_info['extension'];
          $data[$key]['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
        } else {
          $data[$key]['view_path'] = $file['path'];
        }
      }
      return $data;

    } elseif($category == 'album') {

      $this->db->select("album.aid as id,a.featured,
                FROM_UNIXTIME(pictures.ctime) as publish_date,
                album.description  as description,
                album.title as title,
                'album' as category,
                concept.thumbnail as thumbnail,
                concept.name as concept_name,
                concept.slug as slug", false);
      $this->db->from('alb_albums album');
      $this->db->join('alb_pictures pictures', 'album.aid = pictures.aid');
      $this->db->join('ci_master_concept concept', 'concept.id = album.concept_id');
      $this->db->where('album.visibility', 0);
      $this->db->where('album.concept_id', $concept_id);
      $this->db->group_by('pictures.aid');
      /*SELECT
              album.aid as id,
              FROM_UNIXTIME(pictures.ctime) as publish_date,
              album.description  as description,
              album.title as title,
              NULL as album_id,
              'album' as category,
              NULL as file_unique_id,
              NULL as file_size,
              NULL as following_user_id,
              NULL as total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as user_id,
              NULL as first_name,
              NULL as last_name,
              NULL as display_name,
              NULL as profile_pic,
              NULL as email,
              NULL as designation,
              concept.thumbnail as thumbnail,
              concept.name as concept_name,
              concept.slug as slug,
              NULL as object_id,
              NULL as object_type


            FROM
              alb_albums album
              join alb_pictures pictures on album.aid = pictures.aid
              join ci_master_concept as concept on concept.id = $concept_id and album.concept_id = concept.id
            WHERE
              album.visibility = 0
            GROUP BY pictures.aid*/


      $query = $this->db->get();
      #echo $this->db->last_query();46
      return $query->result_array();

    }

  }

  function getConceptFeed($concept_id, $limit, $offset, $category, $user_id)
  {

    /* echo 'here';
     die();*/
    $entry_type = ucfirst($category);
    if(!isset($category) or $category == '') {
      $sql = "(SELECT
              e.entry_id as id,e.featured,
              e.entry_created_on as publish_date,
              e.offers_end_date as offers_end_date,
              e.entry_text_more as description,
              e.entry_title as title,
              e.entry_excerpt as album_id,
              c.category_basename as category,
              NULL as file_unique_id,
              NULL as file_size,
              NULL as following_user_id,
			  (select id from ci_likes ll where ll.status_id = e.entry_id and ll.status_type = UCASE(c.category_basename) and ll.user_id = $user_id  limit 1) as you_like,
			   e.total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as user_id,
              NULL as first_name,
              NULL as last_name,
              NULL as display_name,
              con.thumbnail as profile_pic,
              NULL as email,
              NULL as designation,
              NULL as thumbnail,
              con.name as concept_name,
              con.slug,
              NULL as object_id,
              NULL as object_type,
			  NULL as path
            FROM
              mt_entry e
              join mt_category c on c.category_basename IN('news','announcement','offers') AND c.category_class = 'category'
              join mt_placement p on c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id
              JOIN ci_master_concept con on con.id = e.concept_id
            WHERE
              con.id = $concept_id
            ";


      $sql .= "

            )UNION ALL(
              SELECT
                a.id as id,a.featured,
                FROM_UNIXTIME(a.uploaded_on) as publish_date,
                NULL as offers_end_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
				NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                a.user_id as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as thumbnail,
                a.concept_id as concept_name,
                NULL as slug,
                c.id as object_id,
                'User' as object_type,
				a.path
              FROM
                ci_files a,
                ci_users c
              WHERE
                a.files_status = 1 AND
                a.user_id = c.id AND
                a.concept_id = $concept_id AND
                a.group_file <> 1
            )

            UNION ALL(
              SELECT
                album.aid as id,album.featured,
                FROM_UNIXTIME(pictures.ctime) as publish_date,
                NULL as offers_end_date,
                album.description  as description,
                album.title as title,
                NULL as album_id,
                'album' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = album.aid and lll.user_id = '$user_id' limit 1) as you_like,
                album.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                concept.thumbnail as thumbnail,
                concept.name as concept_name,
                concept.slug as slug,
                NULL as object_id,
                NULL as object_type,
				NULL as path


              FROM
                alb_albums album
                join alb_pictures pictures on album.aid = pictures.aid
                join ci_master_concept as concept on concept.id = $concept_id and album.concept_id = concept.id
              WHERE
                album.visibility = 0
              GROUP BY pictures.aid
            ) UNION ALL (
              SELECT
                wid.id as id,wid.featured,
                wid.create_dttm as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                wid.name as title,
                NULL as album_id,
                'widgets' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = wid.id and lll.status_type = 'Widget' and lll.user_id = '$user_id' limit 1) as you_like,
                wid.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                concept.thumbnail as thumbnail,
                concept.name as concept_name,
                concept.slug as slug,
                NULL as object_id,
                NULL as object_type,
				        NULL as path
                FROM
                widgets wid
                join ci_master_concept as concept on concept.id = $concept_id and wid.parent_id = concept.id
              WHERE
                wid.status = 1
            )
            UNION ALL(
              SELECT
                p.poll_id as id,p.featured,				
                p.created as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
				con.thumbnail,
				con.name as concept_name,
				con.slug,
                p.created_by as object_id,
                p.created_by_type as object_type,
				NULL as path
              FROM
			  v2_polls p
			  JOIN ci_master_concept con on con.id = p.created_by and p.created_by_type = 'Concept' and p.created_by = $concept_id
				)UNION ALL(
              SELECT
                b.pid as id,b.featured,
                b.mtime as publish_date,
                NULL as offers_end_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = b.pid and lll.user_id = '$user_id' limit 1) as you_like,
                b.total_likes,
                c.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                c.display_name,
                c.profile_pic,
				NULL as email,
                NULL as designation,
				 CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug,
				b.owner_id as object_id,
                b.owner_type as object_type,
				NULL as path

              FROM
                ci_users c,
                alb_pictures b
				LEFT JOIN ci_master_concept pcon on pcon.id = b.target_id and b.target_type = 'Concept'
              WHERE
                b.owner_id = c.id AND b.owner_type = 'User' AND b.target_type = 'Concept' AND b.target_id = $concept_id
            )UNION ALL(
              SELECT
                ab.pid as id,ab.featured,
                ab.mtime as publish_date,
                NULL as offers_end_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = ab.pid and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                pconcept.slug as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                pconcept.name as display_name,
                pconcept.thumbnail as profile_pic,
				NULL as email,
                NULL as designation,
				CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                pconcept.name as concept_name,
                pconcept.slug as slug,
				ab.owner_id as object_id,
                ab.owner_type as object_type,
				NULL as path

              FROM
                ci_master_concept pconcept,
                alb_pictures ab
              WHERE
                ab.owner_id = pconcept.id AND ab.owner_id = $concept_id AND ab.owner_type = 'Concept'
            )
            UNION ALL(
              SELECT
                stat.id,stat.featured,
                stat.create_dttm as publish_date,
                NULL as offers_end_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = stat.id and lll.user_id = '$user_id' limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                stat.object_id as user_id,
                user.first_name,
                user.last_name,
                user.display_name,
                user.profile_pic,
                user.email,
                NULL as designation,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug,
                stat.object_id,
                stat.object_type,
				NULL as path
              FROM
                ci_status_updates stat";
      //, ci_users_following_users fol
      $sql .= "
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User' and target_type = 'Concept' and target_id = $concept_id
                LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept' and concept.id = $concept_id


                WHERE
                stat.status = 1 ";

      $sql .= " AND (user.concept_id is not null or concept.id is not null) ";


      $sql .= " AND stat.parent_status_id = 0";
      // AND fol.follow_status = 1 AND fol.user_id = ? AND fol.following_user_id = s.object_id
      $sql .= " ) ";
      $sql .= " order by publish_date DESC LIMIT $offset, $limit";
      $union_query = $this->db->query($sql);

      $data = $union_query->result_array();
      $concepts = array();

      foreach($data as $index => $item) {


        if($item['object_type'] == 'Concept') {
          if($item['category'] == 'photo') {
            $item['profile_pic'] = 'images/concepts/thumbs/' . $item['profile_pic'];
            $item['username'] = 'concepts/' . $item['slug'];
          } else {
            $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          }
          $item['display_name'] = $item['concept_name'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }

        if(in_array($item['category'], array('news', 'offers', 'announcement'))) {

          $item['concept'] = $item['concept_name'];
          $item['concept_url'] = $item['slug'];
        }

        if($item['category'] == 'album') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'concepts/' . $item['slug'];
        }

        if($item['category'] == 'files') {
          if($item['file_size']) {
            $file_info = pathinfo($item['path']);
            $file_name = $file_info['filename'];
            $file_extension = $file_info['extension'];
            $item['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
          } else {
            $item['view_path'] = $item['path'];
          }
        }

        $concepts[] = $item;
      }

      return $concepts;

    }
    if($category == 'status_update') {
      /*
              $this->db->select("stat.id,
                        stat.create_dttm as publish_date,
                        'status updates' as category,
                        stat.total_likes,
                        stat.id as message_id,
                        stat.message,
                        stat.object_id,
                        stat.object_type,
                        user.profile_pic,
                        user.display_name,
                        concept.thumbnail,
                        concept.name as concept_name,
                        concept.slug", false);
              $this->db->from('ci_status_updates stat');
              $this->db->join('ci_users user', "user.id = stat.object_id and stat.object_type = 'User' and target_type = 'Concept' and target_id = ".$concept_id, 'left');
              $this->db->join('ci_master_concept concept',  "concept.id = stat.object_id and stat.object_type = 'Concept' and concept.id = ".$concept_id);
              $this->db->where('stat.status', 1);
              $this->db->where('stat.parent_status_id', 0);
              //$this->db->where("((stat.object_type = 'Concept' and stat.object_id = ".$concept_id.") or ( stat.target_id = ".$concept_id." && stat.target_type = 'Concept'))");
              $this->db->where('(user.concept_id is not null or concept.id is not null)');
              $this->db->order_by('publish_date', 'desc');
              $this->db->limit($limit, $offset);
              $query = $this->db->get();

              //echo $this->db->last_query();
              $data =  $query->result_array();

              //print_r($data);

      */
      $sql = "(SELECT
                b.pid as id,b.featured,
                b.mtime as publish_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = b.pid and lll.user_id = '$user_id' limit 1) as you_like,
                b.total_likes,
                c.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                c.display_name,
                c.profile_pic,
				NULL as email,
                NULL as designation,
				 CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug,
				b.owner_id as object_id,
                b.owner_type as object_type,
				NULL as path

              FROM
                ci_users c,
                alb_pictures b
				LEFT JOIN ci_master_concept pcon on pcon.id = b.target_id and b.target_type = 'Concept'
              WHERE
                b.owner_id = c.id AND b.owner_type = 'User' AND b.target_type = 'Concept' AND b.target_id = $concept_id
            )UNION ALL(
              SELECT
                ab.pid as id,ab.featured,
                ab.mtime as publish_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = ab.pid and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                pconcept.slug as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                pconcept.name as display_name,
                pconcept.thumbnail as profile_pic,
				NULL as email,
                NULL as designation,
				CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                pconcept.name as concept_name,
                pconcept.slug as slug,
				ab.owner_id as object_id,
                ab.owner_type as object_type,
				NULL as path

              FROM
                ci_master_concept pconcept,
                alb_pictures ab
              WHERE
                ab.owner_id = pconcept.id AND ab.owner_id = $concept_id AND ab.owner_type = 'Concept'
            )
            UNION ALL(
              SELECT
                stat.id,stat.featured,
                stat.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = stat.id and lll.user_id = '$user_id' limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                stat.object_id as user_id,
                user.first_name,
                user.last_name,
                user.display_name,
                user.profile_pic,
                user.email,
                NULL as designation,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug,
                stat.object_id,
                stat.object_type,
				NULL as path
              FROM
                ci_status_updates stat";
      //, ci_users_following_users fol
      $sql .= "
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User' and target_type = 'Concept' and target_id = $concept_id
                LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept' and concept.id = $concept_id


                WHERE
                stat.status = 1 ";

      $sql .= " AND (user.concept_id is not null or concept.id is not null) ";


      $sql .= " AND stat.parent_status_id = 0";
      // AND fol.follow_status = 1 AND fol.user_id = ? AND fol.following_user_id = s.object_id
      $sql .= " ) ";


      $sql .= " order by publish_date DESC LIMIT $offset, $limit";

      $union_query = $this->db->query($sql);


      $data = $union_query->result_array();
      $concepts = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          if($item['category'] == 'photo') {
            $item['profile_pic'] = 'images/concepts/thumbs/' . $item['profile_pic'];
            $item['username'] = 'concepts/' . $item['slug'];
          } else {
            $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          }
          $item['display_name'] = $item['concept_name'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }
        $concepts[] = $item;
      }

      return $concepts;

    } elseif(in_array($category, array('news', 'offers', 'announcement'))) {

              /* NULL as file_size,*/
      $this->db->select(
        "e.entry_id as id,e.featured,
              e.entry_created_on as publish_date,
              e.offers_end_date as offers_end_date,
              e.entry_text_more as description,
              e.entry_title as title,
              e.entry_excerpt as album_id,
              c.category_basename as category,
              NULL as file_unique_id,
              NULL as following_user_id,
              (select id from ci_likes ll where ll.status_id = e.entry_id and ll.status_type = '$entry_type' and ll.user_id = $user_id  limit 1) as you_like,
			   e.total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as user_id,
              NULL as first_name,
              NULL as last_name,
              NULL as display_name,
              con.thumbnail as profile_pic,
              NULL as email,
              NULL as designation,
              con.name as concept,
			  con.slug as concept_url", false
      );
      $this->db->from('mt_entry e');
      $this->db->join('ci_master_concept con', 'con.id = e.concept_id and con.id = ' . $concept_id);
      $this->db->join('mt_category c', "c.category_basename = '$category' AND c.category_class = 'category'", null, false);
      $this->db->join('mt_placement p', "c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id");
      $this->db->order_by('publish_date', 'desc');
      $this->db->limit($limit, $offset);

      $query = $this->db->get();
      $data = $query->result_array();
      return $data;

    } elseif(in_array($category, array('polls'))) {
      $this->db->select("
                p.poll_id as id,p.featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				        con.slug as concept_url,
                con.name as posted_concept_name,
                con.thumbnail,
                con.name as concept_name,
                con.slug", false
      );
      $this->db->from('v2_polls p');
      $this->db->join('ci_master_concept con', 'con.id = p.created_by and con.id = ' . $concept_id);
      $this->db->order_by('publish_date', 'desc');
      $this->db->limit($limit, $offset);
      $query = $this->db->get();
      $data = $query->result_array();
      $return_data = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'concepts/' . $item['slug'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }
        $return_data[] = $item;
      }

      return $return_data;

    } elseif($category == 'files') {
      $this->db->select("file.id as id,file.featured,
                FROM_UNIXTIME(file.uploaded_on) as publish_date,
                file.description,
                file.name as title,
                NULL as album_id,
                'files' as category,
                file.unique_id as file_unique_id,
                file.file_size,
				file.path,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as first_name,
                NULL as last_name,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as concept,
				file.concept_id as concept_name,
				c.id as object_id", false);
      $this->db->from('ci_files file');
      //$this->db->join('ci_users uploader', 'file.user_id = uploader.id');
      $this->db->join('ci_users c', 'c.id = file.user_id');
      $this->db->where('file.files_status', 1);
      //this is to make sure group files are not shown here
      $this->db->where('file.group_file !=', 1);
      $this->db->where('file.concept_id', $concept_id);
      $this->db->limit($limit, $offset);
      $this->db->order_by('publish_date', 'DESC');

      $query = $this->db->get();
      $data = $query->result_array();
      foreach($data as $key => $file) {
        if($file['file_size']) {
          $file_info = pathinfo($file['path']);
          $file_name = $file_info['filename'];
          $file_extension = $file_info['extension'];
          $data[$key]['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
        } else {
          $data[$key]['view_path'] = $file['path'];
        }
      }
      return $data;

    } elseif($category == 'album') {

      $this->db->select("album.aid as id,album.featured,
                FROM_UNIXTIME(pictures.ctime) as publish_date,
                album.description  as description,
                album.title as title,
                'album' as category,
                concept.thumbnail as thumbnail,
                concept.name as concept_name,
				(select id from ci_likes lll where lll.status_id = album.aid and lll.user_id = '$user_id' limit 1) as you_like,
                album.total_likes,
                concept.slug as slug", false);
      $this->db->from('alb_albums album');
      $this->db->join('alb_pictures pictures', 'album.aid = pictures.aid');
      $this->db->join('ci_master_concept concept', 'concept.id = album.concept_id');
      $this->db->where('album.visibility', 0);
      $this->db->where('album.concept_id', $concept_id);
      $this->db->group_by('pictures.aid');
      $this->db->limit($limit, $offset);
      $this->db->order_by('publish_date', 'DESC');
      /*SELECT
              album.aid as id,
              FROM_UNIXTIME(pictures.ctime) as publish_date,
              album.description  as description,
              album.title as title,
              NULL as album_id,
              'album' as category,
              NULL as file_unique_id,
              NULL as file_size,
              NULL as following_user_id,
              NULL as total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as user_id,
              NULL as first_name,
              NULL as last_name,
              NULL as display_name,
              NULL as profile_pic,
              NULL as email,
              NULL as designation,
              concept.thumbnail as thumbnail,
              concept.name as concept_name,
              concept.slug as slug,
              NULL as object_id,
              NULL as object_type


            FROM
              alb_albums album
              join alb_pictures pictures on album.aid = pictures.aid
              join ci_master_concept as concept on concept.id = $concept_id and album.concept_id = concept.id
            WHERE
              album.visibility = 0
            GROUP BY pictures.aid*/


      $query = $this->db->get();

      //echo $this->db->last_query();

      $data = $query->result_array();
      $albums = array();
      foreach($data as $item) {
        if($item['category'] == 'album') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'concepts/' . $item['slug'];
        }
        $albums[] = $item;
      }
      return $albums;
    }

  }

  function getHomePageFeed($user_id, $page_size, $offset, $category = null, $concept = null, $time = null)
  {
    $all_feeds_staus = $this->get_feed_status($user_id);
    if(isset($time) && $time != '') {
      $time = date("Y-m-d H:i:s", $time);
    }
    if(isset($category)) {
      if($category == 'status_update') {

        if(isset($concept) && $concept != '') {
          $vars = array($concept, $user_id, $offset, $page_size);
        } else {
          $vars = array($user_id, $offset, $page_size);
        }

        $sql = "(
              SELECT
                b.pid as id,
				b.featured as featured,
                b.mtime as publish_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = b.pid and lll.status_type = 'Photo' and lll.user_id = '$user_id' limit 1) as you_like,
                b.total_likes,
                c.reply_username as username,
                NULL as message_id,
                NULL as message,
                c.display_name,
                c.profile_pic,
                NULL as concept,
                b.owner_id as object_id,
                b.owner_type as object_type,
                b.target_type as target_type,
                pcon.slug as target_url,
                pcon.name as target_name,
                pcon.slug as concept_url,
                pcon.name as posted_concept_name,
				pgroup.id as group_url,
				pgroup.name as posted_group_name,
                CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug
              FROM
                ci_users c,
                alb_pictures b
				LEFT JOIN ci_master_concept pcon on pcon.id = b.target_id and b.target_type = 'Concept'
				LEFT JOIN ci_interest_groups pgroup on pgroup.id = b.target_id and b.target_type = 'Group' 
				LEFT JOIN ci_map_objects group_mem ON pgroup.id = group_mem.parent_id and group_mem.parent_type = 'Group' ";
        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = b.owner_id and b.owner_type = 'User' and f.user_id = $user_id ";
        }
        $sql .= " WHERE
                b.owner_id = c.id AND b.owner_type = 'User' and (CASE WHEN b.target_type = 'Group' THEN group_mem.object_id = $user_id and group_mem.object_type = 'User' and pgroup.group_status = 1 ELSE 1=1 END)";

        if($all_feeds_staus == 0) {
          $sql .= "and(( f.following_user_id = b.owner_id and b.owner_type = 'User' and f.user_id = $user_id) or(b.owner_id = $user_id and b.owner_type = 'User')) ";
        }
        if(isset($time) && $time != 0) {
          $sql .= " and b.mtime > '$time' and (b.owner_id != $user_id and b.owner_type = 'User') ";
        }

        $sql .= " )UNION ALL(
              SELECT
                ab.pid as id,
				ab.featured as featured,
                ab.mtime as publish_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = ab.pid and lll.status_type = 'Photo' and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                pconcept.slug as username,
                NULL as message_id,
                NULL as message,
                pconcept.name as display_name,
                pconcept.thumbnail as profile_pic,
                NULL as concept,
                ab.owner_id as object_id,
                ab.owner_type as object_type,
                ab.target_type as target_type,
                NULL as target_url,
                NULL as target_name,
                pconcept.slug as concept_url,
                pconcept.name as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                pconcept.name as concept_name,
                pconcept.slug as slug
              FROM
                ci_master_concept pconcept,
                alb_pictures ab ";
        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = ab.owner_id and ab.owner_type = 'User' and f.user_id = $user_id ";
        }
        $sql .= " WHERE
                ab.owner_id = pconcept.id AND ab.owner_type = 'Concept' ";
        if($all_feeds_staus == 0) {
          $sql .= " and(( f.following_user_id = ab.owner_id and ab.owner_type = 'User' and f.user_id = $user_id) or(ab.owner_id = $user_id and ab.owner_type = 'User')) ";
        }
        if(isset($time) && $time != 0) {
          $sql .= " and ab.mtime > '$time' and (ab.owner_id != $user_id and ab.owner_type = 'User') ";
        }

        $sql .= " )UNION ALL(
              SELECT
                stat.id,
				stat.featured as featured,
                stat.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes ll where ll.status_id = stat.id and ll.status_type = 'Status' and ll.user_id = ?  limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                user.display_name,
                user.profile_pic,
                NULL as concept,
                stat.object_id,
                stat.object_type,
                stat.target_type,
                posted_to_user.reply_username target_url,
                posted_to_user.display_name as target_name,
                posted_to_concept.slug as concept_url,
                posted_to_concept.name as posted_concept_name,
				posted_to_group.id as group_url,
				posted_to_group.name as posted_group_name,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug
                FROM
                ci_status_updates stat
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_users posted_to_user on posted_to_user.id = stat.target_id and stat.target_type = 'User' ";
        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id ";
        }
        $sql .= " LEFT JOIN ci_master_concept posted_to_concept on posted_to_concept.id = stat.target_id and stat.target_type = 'Concept'
				LEFT JOIN ci_interest_groups posted_to_group on posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'yes'
				LEFT JOIN ci_map_objects group_mem ON posted_to_group.id = group_mem.parent_id and group_mem.parent_type = 'Group' 
                LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept'
                WHERE
                stat.status = 1 and (CASE WHEN stat.target_type = 'Group' THEN posted_to_group.name != '' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and posted_to_group.group_status = 1 ELSE 1=1 END) AND stat.parent_status_id = 0 and stat.status = 1 ";
        if(isset($time) && $time != 0) {
          $sql .= " and stat.create_dttm > '$time' and (stat.object_id != $user_id and stat.object_type = 'User') ";
        }
        if($all_feeds_staus == 0) {
          $sql .= " and(( f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id) or(stat.object_id = $user_id and stat.object_type = 'User')) ";
        }

        $sql .= " )UNION ALL(
              SELECT
                stat.id,
				stat.featured as featured,
                stat.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes ll where ll.status_id = stat.id and ll.status_type = 'Status' and ll.user_id = $user_id  limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                user.display_name,
                user.profile_pic,
                NULL as concept,
                stat.object_id,
                stat.object_type,
                stat.target_type,
                posted_to_user.reply_username target_url,
                posted_to_user.display_name as target_name,
                posted_to_concept.slug as concept_url,
                posted_to_concept.name as posted_concept_name,
				        posted_to_group.id as group_url,
				        posted_to_group.name as posted_group_name,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug
                FROM
                ci_status_updates stat
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_users posted_to_user on posted_to_user.id = stat.target_id and stat.target_type = 'User' ";
        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id ";
        }
        $sql .= " LEFT JOIN ci_master_concept posted_to_concept on posted_to_concept.id = stat.target_id and stat.target_type = 'Concept'
				JOIN ci_interest_groups posted_to_group on posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'no'
				INNER JOIN ci_map_objects group_mem ON posted_to_group.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and posted_to_group.group_status = 1
                LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept'
                WHERE
                stat.status = 1 and (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = posted_to_group.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END) AND stat.parent_status_id = 0 and stat.status = 1 ";
        if(isset($time) && $time != 0) {
          $sql .= " and stat.create_dttm > '$time' and (stat.object_id != $user_id and stat.object_type = 'User') ";
        }
        if($all_feeds_staus == 0) {
          $sql .= " and(( f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id) or(stat.object_id = $user_id and stat.object_type = 'User')) ";
        }

        $sql .= " ) order by featured DESC,publish_date DESC LIMIT ?, ?";
 //echo $sql;exit;
        $query = $this->db->query($sql, $vars);		
        $data = $query->result_array();   
        $return_data = array();      
        $concepts = array();

        foreach($data as $index => $item) {
          if($item['object_type'] == 'Concept') {
            if($item['category'] == 'photo') {
              $item['profile_pic'] = 'images/concepts/thumbs/' . $item['profile_pic'];
            } else {
              $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
            }
            // $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
            $item['display_name'] = $item['concept_name'];
          } elseif($item['object_type'] == 'User') {
            $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
            if($item['target_type'] == 'Concept') {
              $item['target_name'] = $item['posted_concept_name'];
              $item['target_url'] = $item['concept_url'];
            }
            if($item['target_type'] == 'Group') {
              $item['target_name'] = $item['posted_group_name'];
              $item['target_url'] = 'groups/' . $item['group_url'];
            }
          }
          $concepts[] = $item;
        }

        return $concepts;


      } else if(in_array($category, array('news', 'offers', 'announcement'))) {
        $entry_type = ucfirst($category);
        $sql = "SELECT
              e.entry_id as id,
			  e.featured as featured,
              e.entry_created_on as publish_date,
              e.offers_end_date as offers_end_date,
              e.entry_text_more as description,
              e.entry_title as title,
              e.entry_excerpt as album_id,
              c.category_basename as category,
              con.thumbnail as profile_pic,
              con.name as concept,
			  con.slug as concept_url,
			  (select id from ci_likes ll where ll.status_id = e.entry_id and ll.status_type = '$entry_type' and ll.user_id = $user_id  limit 1) as you_like,
			   e.total_likes
            FROM
              mt_entry e
              join mt_category c on c.category_basename = ? AND c.category_class = 'category'
              join mt_placement p on c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id
              LEFT JOIN ci_master_concept con on con.id = e.concept_id
            WHERE
              e.entry_status = 2 ";
        if(isset($time) && $time != 0) {
          $sql .= " and e.entry_created_on > '$time' ";
        }
        if(isset($concept) && $concept != '') {
          $sql .= " and con.id = ? ";
          $sql .= " order by featured DESC,publish_date DESC LIMIT ?, ?";
          $vars = array($category, $concept, $offset, $page_size);
        } else {
          $sql .= " order by featured DESC,publish_date DESC LIMIT ?, ?";
          $vars = array($category, $offset, $page_size);
        }


        $query = $this->db->query($sql, $vars);
        //echo $this->db->last_query();
        return $query->result_array();

      } else if(in_array($category, array('photos'))) {


        $sql = "SELECT
							a.aid,
							a.featured as featured,
							a.title,
							b.ctime,
							b.filepath,
							count(pid) as total_pic,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb,
							(SELECT filename FROM alb_pictures WHERE pid = a.thumb) as filename

						FROM
							alb_albums a,
							alb_pictures b,
							alb_categories c
						WHERE
							visibility = 0 AND
							a.aid = b.aid	AND
							a.category = c.cid ";
        if(isset($time) && $time != 0) {
          $sql .= " and b.mtime > '$time' and (b.owner_id != $user_id and b.owner_type = 'User') ";
        }
        $sql .= " GROUP BY b.aid
						ORDER BY a.aid DESC,featured DESC LIMIT ?,?";

        $vars = array($offset, $page_size);
        $query = $this->db->query($sql, $vars);
        //echo $this->db->last_query();
        return $query->result_array();

      } else if(in_array($category, array('polls'))) {
        $sql = "(
              SELECT
                p.poll_id as id,
				p.featured as featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				con.slug as concept_url,
                con.name as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                con.thumbnail,
                con.name as concept_name,
                con.id as slug,
				NULL as path
              FROM
			  v2_polls p,
			  ci_master_concept con
			  WHERE
                 p.created_by_type ='Concept' AND p.created_by =con.id ";

        if(isset($time) && $time != 0) {
          $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
        }

        $sql .= "	)UNION ALL(
              SELECT
                p.poll_id as id,
				p.featured as featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                u.display_name,
                u.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                'Group' as target_type,
                gp.id as target_url,
                gp.name as target_name,
				gp.id as concept_url,
                gp.name as posted_concept_name,
				gp.id as group_url,
				gp.name as posted_group_name,
                gp.thumbnail,
                gp.name as concept_name,
                gp.id as slug,
				NULL as path
              FROM
			  v2_polls p
			  JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'yes'
			  INNER JOIN ci_map_objects group_mem ON gp.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and gp.group_status = 1
			  LEFT JOIN ci_users u on u.id = p.created_by and p.created_by_type = 'User' ";
        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id ";
        }
        $sql .= " WHERE
                 p.created_by_type ='User' AND target_type = 'Group' ";
        if(isset($time) && $time != 0) {
          $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
        }
        if($all_feeds_staus == 0) {
          $sql .= "and(( f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id) or(p.created_by = $user_id and p.created_by_type = 'User')) ";
        }

        $sql .= " )UNION ALL(
          SELECT
                p.poll_id as id,
				p.featured as featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                u.display_name,
                u.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                'Group' as target_type,
                gp.id as target_url,
                gp.name as target_name,
				gp.id as concept_url,
                gp.name as posted_concept_name,
				gp.id as group_url,
				gp.name as posted_group_name,
                gp.thumbnail,
                gp.name as concept_name,
                gp.id as slug,
				NULL as path
              FROM
			  v2_polls p
			  JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'no'
			  INNER JOIN ci_map_objects group_mem ON gp.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and gp.group_status = 1
			  LEFT JOIN ci_users u on u.id = p.created_by and p.created_by_type = 'User' ";
        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id ";
        }
        $sql .= " WHERE
                 p.created_by_type ='User' AND target_type = 'Group' AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = gp.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END) ";
        if(isset($time) && $time != 0) {
          $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
        }
        if($all_feeds_staus == 0) {
          $sql .= "and(( f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id) or(p.created_by = $user_id and p.created_by_type = 'User')) ";
        }

        $sql .= " )UNION ALL(
              SELECT
                p.poll_id as id,
				p.featured as featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                k.reply_username as username,
                NULL as message_id,
                NULL as message,
                k.display_name,
                k.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
               k.profile_pic as thumbnail,
                NULL as concept_name,
                NULL as slug,
				NULL as path
              FROM
			  v2_polls p
              LEFT JOIN ci_users k on k.id = p.created_by AND p.created_by_type ='User' ";

        if($all_feeds_staus == 0) {
          $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id ";
        }

        $sql .= " WHERE
                p.created_by = k.id AND p.created_by_type ='User' AND p.target_type != 'Group' ";
        if(isset($time) && $time != 0) {
          $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
        }
        if($all_feeds_staus == 0) {
          $sql .= "and(( f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id) or(p.created_by = $user_id and p.created_by_type = 'User')) ";
        }

        $sql .= " ) order by featured DESC, publish_date DESC LIMIT ?, ?";
        $vars = array($offset, $page_size);
        $query = $this->db->query($sql, $vars);

        $data = $query->result_array();

//        print_r($data);


        $return_data = array();

        foreach($data as $index => $item) {
          if($item['object_type'] == 'Concept') {
            $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
            $item['display_name'] = $item['concept_name'];
            $item['username'] = 'concepts/' . $item['slug'];
          } elseif($item['object_type'] == 'User') {
            $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];

            if($item['target_type'] == 'Group') {
              $item['target_name'] = $item['posted_group_name'];
              $item['target_url'] = 'groups/' . $item['group_url'];
            }
          }
          $return_data[] = $item;
        }

        return $return_data;

      } else if(in_array($category, array('profilePhotos'))) {


        $album_name = 'User_Photos_' . $user_id;

        $sql = "SELECT
							a.aid,
							a.featured as featured,
							a.title,
							b.title as description,
							b.ctime,
							b.filepath,
							b.pid,
							b.filename,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb
						FROM
							alb_pictures b,
							alb_albums a
						WHERE
							a.aid = b.aid	AND
							a.title = ?

						ORDER BY b.pid DESC,featured DESC LIMIT ?,?";

        $vars = array($album_name, $offset, $page_size);
        $query = $this->db->query($sql, $vars);
        //echo $this->db->last_query();
        return $query->result_array();


      } else if($category == 'files') {
        $sql = "(SELECT
          a.id as id,
		  a.featured as featured,
          FROM_UNIXTIME(a.uploaded_on) as publish_date,
          a.description,
          a.name as title,
          'files' as category,
          a.unique_id as file_unique_id,
          a.file_size,
          a.path,
          c.id as object_id,
          a.concept_id as concept_name,
          c.id as object_id
          FROM
          ci_files a
              LEFT JOIN ci_users c on a.user_id = c.id
              WHERE a.files_status =  1 AND a.group_file = 0
          )UNION ALL(
          SELECT
          a.id as id,
		  a.featured,
          FROM_UNIXTIME(a.uploaded_on) as publish_date,
          a.description,
          a.name as title,
          'files' as category,
          a.unique_id as file_unique_id,
          a.file_size,
          a.path,
          c.id as object_id,
          a.concept_id as concept_name,
          c.id as object_id
          FROM
              ci_files a
              JOIN ci_users c ON a.user_id = c.id
              JOIN ci_map_objects map ON a.id = map.object_id and map.object_type = 'File' and map.parent_type = 'Group'
			  JOIN ci_map_objects map2 ON $user_id = map2.object_id and map2.object_type = 'User' and map2.parent_type = 'Group' and map2.parent_id = map.parent_id
			  JOIN ci_interest_groups gp ON map.parent_id = gp.id and gp.group_status = 1 and gp.is_open = 'yes'
			  INNER JOIN ci_map_objects group_mem ON gp.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and gp.group_status = 1
              WHERE a.files_status =  1 AND a.group_file = 1
          )
          ORDER BY featured DESC, publish_date DESC LIMIT ?,?";
        $vars = array($offset, $page_size);
        $query = $this->db->query($sql, $vars);
        //echo $this->db->last_query();
        $data = $query->result_array();
        foreach($data as $key => $file) {
          if($file['file_size']) {
            $file_info = pathinfo($file['path']);
            $file_name = $file_info['filename'];
            $file_extension = $file_info['extension'];
            $data[$key]['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
          } else {
            $data[$key]['view_path'] = $file['path'];
          }
        }
        return $data;
        //return $query->result_array();

      }
    }

    $entry_type = ucfirst($category);
    $sql = "(SELECT
              e.entry_id as id,
			  e.featured,
              e.entry_created_on as publish_date,
              e.offers_end_date as offers_end_date,
              e.entry_text_more as description,
              e.entry_title as title,
              e.entry_excerpt as album_id,
              c.category_basename as category,
              NULL as file_unique_id,
              NULL as file_size,
              NULL as following_user_id,
              (select id from ci_likes ll where ll.status_id = e.entry_id and ll.status_type = UCASE(c.category_basename) and ll.user_id = $user_id  limit 1) as you_like,
			   e.total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as display_name,
              con.thumbnail as profile_pic,
              con.name as concept,
              NULL as object_id,
              NULL as object_type,
              NULL as target_type,
              NULL as target_url,
              NULL as target_name,
               con.slug as concept_url,
              NULL as posted_concept_name,
			  NULL as group_url,
			  NULL as posted_group_name,
              NULL as thumbnail,
              NULL as concept_name,
              NULL as slug,
			  NULL as path,
			  NULL as item_min_price,
			  NULL as item_max_price,
			  NULL as category_id,
			  NULL as item_category,
			  NULL as currency
            FROM
              mt_entry e
              join mt_category c on c.category_basename IN('news','announcement','offers') AND c.category_class = 'category'
              join mt_placement p on c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id
              LEFT JOIN ci_master_concept con on con.id = e.concept_id
			  WHERE e.entry_status = 2
            ";
    if(isset($concept) && $concept != '') {
      $sql .= " and con.id = ? ";
      $vars = array($concept, $user_id, $offset, $page_size);
    } else {
      $vars = array($user_id, $offset, $page_size);
    }
    if(isset($time) && $time != 0) {
      $sql .= " and e.entry_created_on > '$time' ";
    }

    $sql .= "

            )UNION ALL(
              SELECT
                a.aid as id,
				 a.featured,
                FROM_UNIXTIME(b.ctime) as publish_date,
                NULL as offers_end_date,
                a.description  as description,
                a.title as title,
                NULL as album_id,
                'album' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = a.aid and lll.status_type = 'Albums' and lll.user_id = '$user_id' limit 1) as you_like,
                a.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                NULL as object_id,
                NULL as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
                NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                albumcon.thumbnail as thumbnail,
                albumcon.name as concept_name,
                albumcon.slug as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
                alb_albums a,
                alb_pictures b,
				ci_master_concept albumcon
              WHERE
                visibility = 0 AND
                a.aid = b.aid AND
				albumcon.id = a.concept_id ";
    if(isset($time) && $time != 0) {
      $sql .= " and FROM_UNIXTIME(b.ctime) > '$time' ";
    }
    // GROUP BY b.aid ";

    $sql .= " GROUP BY b.aid )
    UNION ALL(
              SELECT
                wid.id as id,
				wid.featured,
                wid.create_dttm as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                wid.name as title,
                NULL as album_id,
                'widgets' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = wid.id and lll.status_type = 'Widget' and lll.user_id = '$user_id' limit 1) as you_like,
                wid.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                NULL as object_id,
                NULL as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
                NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                concept.thumbnail as thumbnail,
                concept.name as concept_name,
                concept.slug as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
                widgets wid,
				        ci_master_concept concept
              WHERE
                wid.status = 1 AND
				wid.parent_id = concept.id ";
    if(isset($time) && $time != 0) {
      $sql .= " AND wid.create_dttm > '$time' ";
    }
    $sql .= " ) UNION ALL(
              SELECT
                b.pid as id,
				b.featured,
                b.mtime as publish_date,
                NULL as offers_end_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = b.pid and lll.status_type = 'Photo' and lll.user_id = '$user_id' limit 1) as you_like,
                b.total_likes,
                c.reply_username as username,
                NULL as message_id,
                NULL as message,
                c.display_name,
                c.profile_pic,
                NULL as concept,
                b.owner_id as object_id,
                b.owner_type as object_type,
                b.target_type as target_type,
                pcon.slug as target_url,
                pcon.name as target_name,
                pcon.slug as concept_url,
                pcon.name as posted_concept_name,
				pgroup.id as group_url,
				pgroup.name as posted_group_name,
                CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
                ci_users c,
                alb_pictures b
				LEFT JOIN ci_master_concept pcon on pcon.id = b.target_id and b.target_type = 'Concept'
				LEFT JOIN ci_interest_groups pgroup on pgroup.id = b.target_id and b.target_type = 'Group' 
				LEFT JOIN ci_map_objects group_mem ON pgroup.id = group_mem.parent_id and group_mem.parent_type = 'Group'";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = b.owner_id and b.owner_type = 'User' and f.user_id = $user_id ";
    }
    $sql .= " WHERE
                b.owner_id = c.id AND b.owner_type = 'User' and (CASE WHEN b.target_type = 'Group' THEN group_mem.object_id = $user_id and group_mem.object_type = 'User' and pgroup.group_status = 1 ELSE 1=1 END) ";
    if(isset($time) && $time != 0) {
      $sql .= " and b.mtime > '$time' and (b.owner_id != $user_id and b.owner_type = 'User') ";
    }
    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = b.owner_id and b.owner_type = 'User' and f.user_id = $user_id) or(b.owner_id = $user_id and b.owner_type = 'User')) ";
    }
    //----------------------------------------

    $sql .= " )UNION ALL(
              SELECT
                item.id as id,
				item.featured,
                FROM_UNIXTIME(item.added_on) as publish_date,
                NULL as offers_end_date,
                item.description as description,
                item.title as title,
                NULL as album_id,
                'marketplace' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = item.id and lll.status_type = 'Marketplace' and lll.user_id = '$user_id' limit 1) as you_like,
                item.total_likes,
                user.reply_username as username,
                item.id as message_id,
                item.description as message,
                user.display_name,
                user.profile_pic,
                NULL as concept,
                item.user_id as object_id,
                'User' as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
                NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                (SELECT thumbnail FROM ci_map_items_images WHERE market_items_id = item.id LIMIT 1) as thumbnail,
                NULL as concept_name,
                NULL as slug,
				c.default_thumb_path as path,
				item.item_min_price,
				item.item_max_price,
				item.category_id,
				c.name as item_category,
				cur.code as currency
                FROM
                ci_market_items item
				JOIN ci_master_category c on item.category_id = c.id
                LEFT JOIN ci_users user on user.id = item.user_id
				LEFT JOIN ci_master_currency cur on cur.id = item.currency_id ";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = item.user_id  and f.user_id = $user_id ";
    }
    $sql .= " WHERE
                item.items_status = 1 ";
    if(isset($time) && $time != 0) {
      $sql .= " and FROM_UNIXTIME(item.added_on) > '$time'";
    }
    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = item.user_id and f.user_id = $user_id) or(item.user_id = $user_id ))  ";
    }
    //-----------------------------------------
    $sql .= " )UNION ALL(
              SELECT
                ab.pid as id,
				ab.featured,
                ab.mtime as publish_date,
                NULL as offers_end_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = ab.pid and lll.status_type = 'Photo' and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                pconcept.slug as username,
                NULL as message_id,
                NULL as message,
                pconcept.name as display_name,
                pconcept.thumbnail as profile_pic,
                NULL as concept,
                ab.owner_id as object_id,
                ab.owner_type as object_type,
                ab.target_type as target_type,
                NULL as target_url,
                NULL as target_name,
                pconcept.slug as concept_url,
                pconcept.name as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                pconcept.name as concept_name,
                pconcept.slug as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
                ci_master_concept pconcept,
                alb_pictures ab
				WHERE
                ab.owner_id = pconcept.id AND ab.owner_type = 'Concept' ";
    if(isset($time) && $time != 0) {
      $sql .= " and ab.mtime > '$time' and (ab.owner_id != $user_id and ab.owner_type = 'User') ";
    }
    /*if($all_feeds_staus == 0) {
    $sql .= " and(( f.following_user_id = ab.owner_id and ab.owner_type = 'User' and f.user_id = $user_id) or(ab.owner_id = $user_id and ab.owner_type = 'User')) ";
    }*/
    $sql .= ")UNION ALL(
              SELECT
                p.poll_id as id,
				p.featured,
                p.created as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                u.display_name,
                u.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                'Group' as target_type,
                gp.id as target_url,
                gp.name as target_name,
				gp.id as concept_url,
                gp.name as posted_concept_name,
				gp.id as group_url,
				gp.name as posted_group_name,
                gp.thumbnail,
                gp.name as concept_name,
                gp.id as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
			  v2_polls p
			  JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'yes'
			  INNER JOIN ci_map_objects group_mem ON gp.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and gp.group_status = 1
			  LEFT JOIN ci_users u on u.id = p.created_by and p.created_by_type = 'User' ";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id ";
    }
    $sql .= " WHERE
                 p.created_by_type ='User' AND (CASE WHEN p.target_type = 'Group' THEN gp.name != '' ELSE 1=1 END) ";
    if(isset($time) && $time != 0) {
      $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
    }
    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id) or(p.created_by = $user_id and p.created_by_type = 'User')) ";
    }

    $sql .= " )UNION ALL(
          SELECT
                p.poll_id as id,
				p.featured,
                p.created as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                u.display_name,
                u.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                'Group' as target_type,
                gp.id as target_url,
                gp.name as target_name,
				gp.id as concept_url,
                gp.name as posted_concept_name,
				gp.id as group_url,
				gp.name as posted_group_name,
                gp.thumbnail,
                gp.name as concept_name,
                gp.id as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
			  v2_polls p
			  JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'no'
			  INNER JOIN ci_map_objects group_mem ON gp.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and gp.group_status = 1
			  LEFT JOIN ci_users u on u.id = p.created_by and p.created_by_type = 'User' ";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id ";
    }
    $sql .= " WHERE p.created_by_type ='User' AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = gp.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END) ";
    if(isset($time) && $time != 0) {
      $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
    }
    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id) or(p.created_by = $user_id and p.created_by_type = 'User')) ";
    }
    $sql .= " )UNION ALL(
              SELECT
                p.poll_id as id,
				p.featured,
                p.created as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				con.slug as concept_url,
                con.name as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                con.thumbnail,
                con.name as concept_name,
                con.slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
			  v2_polls p,
			  ci_master_concept con
				 WHERE
                 p.created_by_type ='Concept' AND p.created_by =con.id ";
    if(isset($time) && $time != 0) {
      $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
    }

    $sql .= "	)UNION ALL(
              SELECT
                p.poll_id as id,
				p.featured,
                p.created as publish_date,
                NULL as offers_end_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                k.reply_username as username,
                NULL as message_id,
                NULL as message,
                k.display_name,
                k.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                k.profile_pic as thumbnail,
                NULL as concept_name,
                NULL as slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
			  v2_polls p
			  LEFT JOIN ci_users k on p.created_by= k.id and p.created_by_type = 'User' ";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id ";
    }
    $sql .= " where p.created_by = k.id AND p.created_by_type ='User' and p.target_type != 'Group' ";
    if(isset($time) && $time != 0) {
      $sql .= " and p.created > '$time' and (p.created_by != $user_id and p.created_by_type = 'User') ";
    }
    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = p.created_by and p.created_by_type = 'User' and f.user_id = $user_id) or(p.created_by = $user_id and p.created_by_type = 'User')) ";
    }
    $sql .= " )UNION ALL(
              SELECT
                a.id as id,
				a.featured,
                FROM_UNIXTIME(a.uploaded_on) as publish_date,
                NULL as offers_end_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = a.id and lll.status_type = 'Files' and lll.user_id = '$user_id' limit 1) as you_like,
                a.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                c.id as object_id,
                'User' as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
                NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                NULL as thumbnail,
                a.concept_id as concept_name,
                NULL as slug,
				a.path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
              ci_files a
              LEFT JOIN ci_users c on a.user_id = c.id
              WHERE a.files_status =  1 AND a.group_file = 0 ";
    if(isset($time) && $time != 0) {
      $sql .= " and FROM_UNIXTIME(a.uploaded_on) > '$time' ";
    }
    $sql .= " )UNION ALL(
             SELECT
                a.id as id,
				a.featured,
                FROM_UNIXTIME(a.uploaded_on) as publish_date,
                NULL as offers_end_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                (select id from ci_likes lll where lll.status_id = a.id and lll.status_type = 'Files' and lll.user_id = '$user_id' limit 1) as you_like,
                a.total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as display_name,
                NULL as profile_pic,
                NULL as concept,
                c.id as object_id,
                'User' as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
                NULL as concept_url,
                NULL as posted_concept_name,
				NULL as group_url,
				NULL as posted_group_name,
                NULL as thumbnail,
                a.concept_id as concept_name,
                NULL as slug,
				a.path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
              FROM
              ci_files a
              JOIN ci_users c ON a.user_id = c.id
              JOIN ci_map_objects map ON a.id = map.object_id and map.object_type = 'File' and map.parent_type = 'Group' AND a.group_file = 1
			  JOIN ci_map_objects map2 ON $user_id = map2.object_id and map2.object_type = 'User' and map2.parent_type = 'Group' and map2.parent_id = map.parent_id AND a.group_file = 1
			  JOIN ci_interest_groups gp ON map.parent_id = gp.id and gp.group_status = 1 and gp.is_open = 'yes' AND a.group_file = 1
			  INNER JOIN ci_map_objects group_mem ON gp.id = group_mem.parent_id and group_mem.parent_type = 'Group' and group_mem.object_id = $user_id and group_mem.object_type = 'User' and gp.group_status = 1
              WHERE a.files_status =  1 ";
    if(isset($time) && $time != 0) {
      $sql .= " and FROM_UNIXTIME(a.uploaded_on) > '$time' ";
    }
    $sql .= " )UNION ALL(
              SELECT
                stat.id,
				stat.featured,
                stat.create_dttm as publish_date,
                NULL as offers_end_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes ll where ll.status_id = stat.id and ll.status_type = 'Status' and ll.user_id = ?  limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                user.display_name,
                user.profile_pic,
                NULL as concept,
                stat.object_id,
                stat.object_type,
                stat.target_type,
                posted_to_user.reply_username target_url,
                posted_to_user.display_name as target_name,
                posted_to_concept.slug as concept_url,
                posted_to_concept.name as posted_concept_name,
				posted_to_group.id as group_url,
				posted_to_group.name as posted_group_name,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
                FROM
                ci_status_updates stat
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_users posted_to_user on posted_to_user.id = stat.target_id and stat.target_type = 'User' ";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id ";
    }
    $sql .= " LEFT JOIN ci_master_concept posted_to_concept on posted_to_concept.id = stat.target_id and stat.target_type = 'Concept'
	LEFT JOIN ci_interest_groups posted_to_group on posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'yes'
	LEFT JOIN ci_map_objects group_mem ON posted_to_group.id = group_mem.parent_id and group_mem.parent_type = 'Group'
    LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept'
				WHERE stat.status = 1 and (CASE WHEN stat.target_type = 'Group' THEN posted_to_group.name != '' AND group_mem.object_id = $user_id and group_mem.object_type = 'User' and posted_to_group.group_status = 1  ELSE 1=1 END) AND stat.parent_status_id = 0 and stat.status = 1 ";
    /*if($all_feeds_staus == 0) {
    $sql .= " AND f.user_id = $user_id AND stat.object_id = f.following_user_id ";
    }*/
    if(isset($time) && $time != 0) {
      $sql .= " and stat.create_dttm > '$time' and (stat.object_id != $user_id and stat.object_type = 'User') ";
    }
    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id) or(stat.object_id = $user_id and stat.object_type = 'User')) ";
    }

    $sql .= " )UNION ALL(
                SELECT
                stat.id,
				stat.featured,
                stat.create_dttm as publish_date,
                NULL as offers_end_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                (select id from ci_likes ll where ll.status_id = stat.id and ll.status_type = 'Status' and ll.user_id = $user_id  limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                user.display_name,
                user.profile_pic,
                NULL as concept,
                stat.object_id,
                stat.object_type,
                stat.target_type,
                posted_to_user.reply_username target_url,
                posted_to_user.display_name as target_name,
                posted_to_concept.slug as concept_url,
                posted_to_concept.name as posted_concept_name,
        		posted_to_group.id as group_url,
        		posted_to_group.name as posted_group_name,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug,
				NULL as path,
			    NULL as item_min_price,
			    NULL as item_max_price,
			    NULL as category_id,
			    NULL as item_category,
				NULL as currency
                FROM
                ci_status_updates stat
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_users posted_to_user on posted_to_user.id = stat.target_id and stat.target_type = 'User' ";
    if($all_feeds_staus == 0) {
      $sql .= " LEFT JOIN ci_users_following_users f on f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id ";
    }
    $sql .= " LEFT JOIN ci_master_concept posted_to_concept on posted_to_concept.id = stat.target_id and stat.target_type = 'Concept'
	JOIN ci_interest_groups posted_to_group ON posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'no'
	LEFT JOIN ci_map_objects group_mem ON posted_to_group.id = group_mem.parent_id and group_mem.parent_type = 'Group' 
                LEFT JOIN ci_master_concept concept on concept.id = stat.object_id and stat.object_type = 'Concept'
                WHERE
                stat.status = 1 AND stat.parent_status_id = 0 AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = posted_to_group.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)
                ";
    /*	if($all_feeds_staus == 0) {
      $sql .= " AND f.user_id = $user_id AND stat.object_id = f.following_user_id ";
      } */
    if(isset($time) && $time != 0) {
      $sql .= " and stat.create_dttm > '$time' and (stat.object_id != $user_id and stat.object_type = 'User') ";
    }

    if($all_feeds_staus == 0) {
      $sql .= " and(( f.following_user_id = stat.object_id and stat.object_type = 'User' and f.user_id = $user_id) or(stat.object_id = $user_id and stat.object_type = 'User')) ";
    }


    $sql .= " ) order by featured DESC, publish_date DESC LIMIT ?, ?";
//echo $sql;
    $query = $this->db->query($sql, $vars);

    $data = $query->result_array();

    // print_r($data);exit;
    //$sql = $this->db->last_query();
    //print_r($sql);exit;


    $return_data = array();

    foreach($data as $index => $item) {
      if($item['category'] == 'album') {
        $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
        $item['display_name'] = $item['concept_name'];
        $item['username'] = 'concepts/' . $item['slug'];
      } else if($item['category'] == 'files') {
        if($item['file_size']) {
          $file_info = pathinfo($item['path']);
          $file_name = $file_info['filename'];
          $file_extension = $file_info['extension'];
          $item['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
        } else {
          $item['view_path'] = $item['path'];
        }
      } else if($item['category'] == 'marketplace') {
        $item['title'] = ucwords(strtolower($item['title']));
        if($item['thumbnail'] != '' && file_exists("images/marketplace/" . $item['thumbnail'])) {
          $item['thumbnail'] = "images/marketplace/" . $item['thumbnail'];
        } else {
          $item['thumbnail'] = $item['path'];
        }
      }

      if($item['object_type'] == 'Concept') {
        if($item['category'] == 'photo') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['profile_pic'];
        } else {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
        }
        $item['display_name'] = $item['concept_name'];
        $item['username'] = 'concepts/' . $item['slug'];
      } elseif($item['object_type'] == 'Group') {
        $item['profile_pic'] = 'images/groups/thumbs/' . $item['thumbnail'];
        $item['display_name'] = $item['concept_name'];
        $item['username'] = 'groups/' . $item['slug'];
      } elseif($item['object_type'] == 'User') {
        $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        if($item['target_type'] == 'Concept') {
          $item['target_name'] = $item['posted_concept_name'];
          $item['target_url'] = 'concepts/' . $item['concept_url'];
        }
        if($item['target_type'] == 'Group') {
          $item['target_name'] = $item['posted_group_name'];
          $item['target_url'] = 'groups/' . $item['group_url'];
        }
      }


      $return_data[] = $item;
    }

    return $return_data;


    //echo $this->db->last_query();

    $data = array();
    if($result->num_rows() > 0) {
      foreach($result->result_array() as $row) {
        $data[] = $row;
      }
    }
    $result->free_result();
    /* print_r($data);
     die();*/
    return $data;
  }

  function get_feed_status($user_id)
  {
    //return $this->getColumnByEmail('id', $email);
    $this->db->select('all_feeds'); #Because I need the value
    $this->db->where('id', $user_id); #Because I need the variable column entitled siteoverview
    $query = $this->db->get("ci_users"); #From the settings table
    $data = $query->result();
    $data = $query->result_array();
    if(isset($data[0])) {
      return $data[0]['all_feeds'];
    } else {
      return 1;
    }
  }

  function getProfileFeed($profile_id, $limit, $offset, $category, $user_id)
  {


    if(!isset($category) or $category == '') {

      $album_name = 'User_Photos_' . $profile_id;
      $album_path = 'User_Photos_' . $profile_id . '/';

      $sql = "(
              SELECT
                a.id as id,
				a.featured,
                FROM_UNIXTIME(a.uploaded_on) as publish_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as thumbnail,
                a.concept_id as concept_name,
                NULL as slug,
                c.id as object_id,
                'User' as object_type,
                a.path,
                NULL as target_url,
                NULL as target_name,
                NULL as target_id,
                NULL as target_type
              FROM
              ci_files a
              JOIN ci_users c ON a.user_id = c.id
              LEFT JOIN ci_map_objects map ON a.id = map.object_id and map.object_type = 'File' and map.parent_type = 'Group'
              LEFT JOIN ci_interest_groups gp on gp.id = map.parent_id and gp.is_open = 'yes'
              WHERE a.files_status =  1
              AND a.user_id =  $profile_id
              AND (CASE WHEN map.parent_type = 'Group' THEN gp.name != '' ELSE 1=1 END)
            )UNION ALL(
              SELECT
              a.id as id,
			  a.featured,
                FROM_UNIXTIME(a.uploaded_on) as publish_date,
                a.description,
                a.name as title,
                NULL as album_id,
                'files' as category,
                a.unique_id as file_unique_id,
                a.file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                NULL as username,
                NULL as message_id,
                NULL as message,
                NULL as user_id,
                NULL as display_name,
                NULL as profile_pic,
                NULL as email,
                NULL as designation,
                NULL as thumbnail,
                a.concept_id as concept_name,
                NULL as slug,
                c.id as object_id,
                'User' as object_type,
                a.path,
                NULL as target_url,
                NULL as target_name,
                NULL as target_id,
                NULL as target_type
              FROM ci_files a
              JOIN ci_users c ON a.user_id = c.id
              LEFT JOIN ci_map_objects map ON a.id = map.object_id and map.object_type = 'File' and map.parent_type = 'Group'
              JOIN ci_interest_groups gp on gp.id = map.parent_id and gp.is_open = 'no'
              WHERE a.files_status =  1 AND a.group_file = 1
              AND a.user_id =  $profile_id
              AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = gp.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)
            )UNION ALL(SELECT
                p.poll_id as id,p.featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				NULL as you_like,
                NULL as total_likes,
                k.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                k.display_name,
                k.profile_pic,
				NULL as email,
				NULL as designation,
				k.profile_pic as thumbnail,
                NULL as concept_name,
                NULL as slug,
                p.created_by as object_id,
                p.created_by_type as object_type,
				NULL as path,
				NULL as target_url,
				NULL as target_name,
				NULL as target_id,
				NULL as target_type
              FROM
              v2_polls p
                         JOIN ci_users k ON p.created_by = k.id
                LEFT JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'yes'
			          WHERE p.created_by_type ='User' AND  p.created_by = $profile_id AND (CASE WHEN p.target_type = 'Group' THEN gp.name != '' ELSE 1=1 END)
		    )
        UNION ALL(SELECT
        p.poll_id as id,
		p.featured,		
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				NULL as you_like,
                NULL as total_likes,
                k.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                k.display_name,
                k.profile_pic,
				NULL as email,
				NULL as designation,
				k.profile_pic as thumbnail,
                NULL as concept_name,
                NULL as slug,
                p.created_by as object_id,
                p.created_by_type as object_type,
				NULL as path,
				NULL as target_url,
				NULL as target_name,
				NULL as target_id,
				NULL as target_type
        FROM
              v2_polls p
                JOIN ci_users k ON p.created_by = k.id
                JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'no'
			          WHERE p.created_by_type ='User' AND p.created_by = $profile_id AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = gp.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)                
				)
		UNION ALL(
              SELECT
                b.pid as id,
				b.featured,
                b.mtime as publish_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = b.pid and lll.user_id = '$user_id' limit 1) as you_like,
                b.total_likes,
                u.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                u.display_name,
                u.profile_pic,
				NULL as email,
				NULL as designation,
				CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
				NULL as concept_name,
                NULL as slug,
                b.owner_id as object_id,
                b.owner_type as object_type,
				NULL as path,
				NULL as target_url,
				NULL as target_name,
				NULL as target_id,
				NULL as target_type
              FROM
                ci_users u,
                alb_pictures b
              WHERE
                b.owner_id = u.id AND
                b.owner_id = $profile_id AND b.owner_type = 'User' AND b.filepath = '$album_path' AND b.target_id = '0'
            )
			UNION ALL(
              SELECT
                ab.pid as id,
				ab.featured,
                ab.mtime as publish_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = ab.pid and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                cu.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                cu.display_name,
                cu.profile_pic,
				NULL as email,
				NULL as designation,
				CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                pgroup.name as concept_name,
                pgroup.id as slug,
                ab.owner_id as object_id,
                ab.owner_type as object_type,
				NULL as path,
				pconcept.slug as target_url,
				pconcept.name as target_name,
				ab.target_id,
				ab.target_type
              FROM
			   alb_pictures ab
			    LEFT JOIN ci_users cu on cu.id = ab.owner_id and ab.owner_type = 'User'
				LEFT JOIN ci_master_concept pconcept on pconcept.id = ab.target_id and ab.target_type = 'Concept' and ab.owner_type = 'User' and ab.owner_id = $profile_id
				LEFT JOIN ci_interest_groups pgroup on pgroup.id = ab.target_id and ab.target_type = 'Group' and ab.owner_type = 'User' and ab.owner_id = $profile_id
				WHERE
				ab.target_id <> 0 and ab.owner_id = $profile_id
            )
            UNION ALL
            (
              SELECT
                stat.id,
				stat.featured,
                stat.create_dttm as publish_date,
                NULL as description,
                NULL as title,
                NULL as album_id,
                'status updates' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = stat.id and lll.user_id = '$user_id' limit 1) as you_like,
                stat.total_likes,
                user.reply_username as username,
                stat.id as message_id,
                stat.message,
                stat.object_id as user_id,
                user.display_name,
                user.profile_pic,
                user.email,
                NULL as designation,
                concept.thumbnail,
                concept.name as concept_name,
                concept.slug,
                stat.object_id,
                stat.object_type,
				NULL as path,
				posted_user.reply_username as target_url,
				posted_user.display_name as target_name,
				NULL as target_id,
				NULL as target_type
              FROM
                ci_status_updates stat";
      //, ci_users_following_users fol
      $sql .= "
                LEFT JOIN ci_users user on user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_master_concept concept on concept.id = stat.target_id and stat.target_type = 'Concept' and stat.object_type = 'User' and stat.object_id = $profile_id
                LEFT JOIN ci_users posted_user on posted_user.id = stat.target_id and stat.target_type = 'User'
                LEFT JOIN ci_interest_groups posted_to_group on posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'yes'
                ";

      $sql .= "  WHERE
                ((stat.status = 1 AND stat.object_type = 'User') OR (stat.status = 1 AND stat.target_type = 'User')) and (CASE WHEN stat.target_type = 'Group' THEN posted_to_group.name != '' ELSE 1=1 END)";

      $sql .= " AND (user.id = $profile_id or posted_user.id = $profile_id) ";


      $sql .= " AND stat.parent_status_id = 0";
      // AND fol.follow_status = 1 AND fol.user_id = ? AND fol.following_user_id = s.object_id
      $sql .= " )UNION ALL(
                
                SELECT 
                  stat.id,stat.featured,
                  stat.create_dttm as publish_date,
                  NULL as description,
                  NULL as title,
                  NULL as album_id,
                  'status updates' as category,
                  NULL as file_unique_id,
                  NULL as file_size,
                  NULL as following_user_id,
                  (select id from ci_likes lll where lll.status_id = stat.id and lll.user_id = '$user_id' limit 1) as you_like,
                  stat.total_likes,
                  user.reply_username as username,
                  stat.id as message_id,
                  stat.message,
                  stat.object_id as user_id,
                  user.display_name,
                  user.profile_pic,
                  user.email,
                  NULL as designation,
                  concept.thumbnail,
                  concept.name as concept_name,
                  concept.slug,
                  stat.object_id,
                  stat.object_type,
                  NULL as path,
                  posted_to_user.reply_username as target_url,
                  posted_to_user.display_name as target_name,
                  NULL as target_id,
                  NULL as target_type
                FROM ci_status_updates stat
                LEFT JOIN ci_users user ON user.id = stat.object_id and stat.object_type = 'User'
                LEFT JOIN ci_users posted_to_user ON posted_to_user.id = stat.target_id and stat.target_type = 'User'
                LEFT JOIN ci_master_concept concept ON concept.id = stat.target_id and stat.target_type = 'Concept'
                JOIN ci_interest_groups posted_to_group ON posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'no'
                WHERE stat.status =  1
                AND stat.parent_status_id =  0
                AND stat.object_id =  $profile_id
                AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = posted_to_group.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)
                OR stat.target_id =  $profile_id
                ) ";


      $sql .= " order by publish_date DESC LIMIT $offset, $limit";

      $union_query = $this->db->query($sql);
      //echo $this->db->last_query();


      $data = $union_query->result_array();

      $concepts = array();


      foreach($data as $index => $item) {

        if($item['object_type'] == 'User' || $item['concept_name'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];

        }

        if(in_array($item['category'], array('news', 'offers', 'announcement'))) {
          $item['concept'] = $item['concept_name'];
          $item['concept_url'] = $item['slug'];

        }

        if($item['category'] == 'album') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
        }
        if($item['category'] == 'photo') {
          //$item['thumbnail'] = $item['slug'];
          if($item['target_type'] == 'Group') {
            $item['target_url'] = 'groups/' . $item['slug'];
            $item['target_name'] = $item['concept_name'];
          } else if($item['target_type'] == 'Concept') {
            $item['target_url'] = 'concepts/' . $item['target_url'];
          }

        }

        if($item['category'] == 'files') {
          if($item['file_size']) {
            $file_info = pathinfo($item['path']);
            $file_name = $file_info['filename'];
            $file_extension = $file_info['extension'];
            $item['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
          } else {
            $item['view_path'] = $item['path'];
          }
        }


        $concepts[] = $item;
      }

      return $concepts;

    }
    if($category == 'status_update') {

      $sql = "
        (SELECT 
        stat.id,
		stat.featured,
        stat.create_dttm as publish_date, 
        'status updates' as category, 
        stat.total_likes, 
        stat.id as message_id, 
        stat.message, 
        stat.object_id, 
        stat.object_type,
		
        user.profile_pic, 
        user.display_name, 
        user.reply_username as username, 
        concept.thumbnail, 
        concept.name as concept_name, 
        concept.slug, 
        posted_to_user.reply_username as target_url, 
        (select id from ci_likes lll where lll.status_id = stat.id and lll.user_id = $user_id limit 1) as you_like, posted_to_user.display_name as target_name
        FROM ci_status_updates stat
        LEFT JOIN ci_users user ON user.id = stat.object_id and stat.object_type = 'User'
        LEFT JOIN ci_users posted_to_user ON posted_to_user.id = stat.target_id and stat.target_type = 'User'
        LEFT JOIN ci_master_concept concept ON concept.id = stat.target_id and stat.target_type = 'Concept'
        LEFT JOIN ci_interest_groups posted_to_group ON posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'yes'
        WHERE stat.status =  1
        AND stat.parent_status_id =  0
        AND stat.object_id =  $profile_id
        AND (CASE WHEN stat.target_type = 'Group' THEN posted_to_group.name != '' ELSE 1=1 END)
        OR stat.target_id =  $profile_id
        )UNION ALL(        
        SELECT 
        stat.id,stat.featured,
        stat.create_dttm as publish_date, 
        'status updates' as category, 
        stat.total_likes, 
        stat.id as message_id, 
        stat.message, 
        stat.object_id, 
        stat.object_type, 
        user.profile_pic, 
        user.display_name, 
        user.reply_username as username, 
        concept.thumbnail, 
        concept.name as concept_name, 
        concept.slug, 
        posted_to_user.reply_username as target_url, 
        (select id from ci_likes lll where lll.status_id = stat.id and lll.user_id = $user_id limit 1) as you_like, posted_to_user.display_name as target_name
        FROM ci_status_updates stat
        LEFT JOIN ci_users user ON user.id = stat.object_id and stat.object_type = 'User'
        LEFT JOIN ci_users posted_to_user ON posted_to_user.id = stat.target_id and stat.target_type = 'User'
        LEFT JOIN ci_master_concept concept ON concept.id = stat.target_id and stat.target_type = 'Concept'
        JOIN ci_interest_groups posted_to_group ON posted_to_group.id = stat.target_id and stat.target_type = 'Group' and posted_to_group.is_open = 'no'
        WHERE stat.status =  1  
        AND stat.parent_status_id =  0
        AND stat.object_id =  $profile_id
        AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = posted_to_group.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)
        OR stat.target_id =  $profile_id
        ) 
        ";
      $sql .= "order by publish_date DESC LIMIT ?, ?";
      $vars = array($offset, $limit);
      $query = $this->db->query($sql, $vars);

      #echo $this->db->last_query();
      $data = $query->result_array();

      //print_r($data);


      $concepts = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }
        $concepts[] = $item;
      }

      return $concepts;

    } elseif(in_array($category, array('news', 'offers', 'announcement'))) {

      $this->db->select(
        "e.entry_id as id,
			e.featured,
            e.entry_created_on as publish_date,
            e.entry_text_more as description,
            e.entry_title as title,
            e.entry_excerpt as album_id,
            c.category_basename as category,
            NULL as file_unique_id,
            NULL as file_size,
            NULL as following_user_id,
            NULL as you_like,
            NULL as total_likes,
            NULL as username,
            NULL as message_id,
            NULL as message,
            NULL as user_id,
            NULL as first_name,
            NULL as last_name,
            NULL as display_name,
            con.thumbnail as profile_pic,
            NULL as email,
            NULL as designation,
            con.name as concept,
      con.slug as concept_url", false
      );
      $this->db->from('mt_entry e');
      $this->db->join('ci_master_concept con', 'con.id = e.concept_id');
      $this->db->join('mt_category c', "c.category_basename = '$category' AND c.category_class = 'category'", null, false);
      $this->db->join('mt_placement p', "c.category_id = p.placement_category_id AND p.placement_entry_id = e.entry_id");
      $this->db->order_by('publish_date', 'desc');
      $this->db->limit($limit, $offset);

      $query = $this->db->get();
      $data = $query->result_array();
      return $data;

    } elseif(in_array($category, array('profilePhotos'))) {


      $album_name = 'User_Photos_' . $profile_id;

      $sql = "SELECT
							a.aid,
							a.featured,
							a.title,
							FROM_UNIXTIME(b.ctime) as publish_date,
							b.title as description,
							b.ctime,
							b.filepath,
							b.pid,
							b.filename,
							(SELECT value FROM alb_config WHERE name = 'ecards_more_pic_target') as image_path,
							(SELECT value FROM alb_config WHERE name = 'fullpath') as sub_dir,
							(SELECT value FROM alb_config WHERE name = 'thumb_pfx') as thumb
						FROM
							alb_pictures b,
							alb_albums a

						WHERE
						a.aid = b.aid	AND
						a.title = '$album_name'

						ORDER BY b.pid DESC LIMIT $offset, $limit";

      //$vars = array($album_name,$vars);
      $query = $this->db->query($sql);
      //echo $this->db->last_query();

      return $query->result_array();


    } elseif($category == 'files') {
      /*$this->db->select("file.id as id,
              FROM_UNIXTIME(file.uploaded_on) as publish_date,
              file.description,
              file.name as title,
              NULL as album_id,
              'files' as category,
              file.unique_id as file_unique_id,
              file.file_size,
              NULL as following_user_id,
              NULL as you_like,
              NULL as total_likes,
              NULL as username,
              NULL as message_id,
              NULL as message,
              NULL as user_id,
              NULL as first_name,
              NULL as last_name,
              NULL as display_name,
              NULL as profile_pic,
              NULL as email,
              NULL as designation,
              NULL as concept,
      file.concept_id as concept_name,
      file.user_id as object_id,
      NULL as path", false);
      $this->db->from('ci_files file');
      $this->db->join('ci_users uploader', 'file.user_id = uploader.id','LEFT');
      $this->db->where('file.files_status', 1);
      $this->db->where('file.group_file',0);
      $this->db->where('file.user_id', $profile_id);
      $this->db->limit($limit, $offset);
      $this->db->order_by('publish_date', 'DESC');*/

      $sql = "
        (SELECT file.id as id,file.featured,
        FROM_UNIXTIME(file.uploaded_on) as publish_date,
        file.description, 
        file.name as title, 
        NULL as album_id, 
        'files' as category, 
        file.unique_id as file_unique_id, 
        file.file_size, 
        NULL as following_user_id, 
        NULL as you_like, 
        NULL as total_likes, 
        NULL as username, 
        NULL as message_id, 
        NULL as message, 
        NULL as user_id, 
        NULL as first_name, 
        NULL as last_name, 
        NULL as display_name, 
        NULL as profile_pic, 
        NULL as email, 
        NULL as designation, 
        NULL as concept, 
        file.concept_id as concept_name, 
        file.user_id as object_id, 
        NULL as path,map.parent_type
        FROM ci_files file
        JOIN ci_users uploader ON file.user_id = uploader.id
        LEFT JOIN ci_map_objects map ON file.id = map.object_id and map.object_type = 'File' and map.parent_type = 'Group'
        LEFT JOIN ci_interest_groups gp on gp.id = map.parent_id and gp.is_open = 'yes'
        WHERE file.files_status =  1
        AND file.user_id =  $profile_id
        AND (CASE WHEN map.parent_type = 'Group' THEN gp.name != '' ELSE 1=1 END))
        UNION ALL(
        SELECT
        file.id as id,file.featured, 
        FROM_UNIXTIME(file.uploaded_on) as publish_date,
        file.description, 
        file.name as title, 
        NULL as album_id, 
        'files' as category, 
        file.unique_id as file_unique_id, 
        file.file_size, 
        NULL as following_user_id, 
        NULL as you_like, 
        NULL as total_likes, 
        NULL as username, 
        NULL as message_id, 
        NULL as message, 
        NULL as user_id, 
        NULL as first_name, 
        NULL as last_name, 
        NULL as display_name, 
        NULL as profile_pic, 
        NULL as email, 
        NULL as designation, 
        NULL as concept, 
        file.concept_id as concept_name, 
        file.user_id as object_id, 
        NULL as path,map.parent_type
        FROM ci_files file
        JOIN ci_users uploader ON file.user_id = uploader.id
        LEFT JOIN ci_map_objects map ON file.id = map.object_id and map.object_type = 'File' and map.parent_type = 'Group'
        JOIN ci_interest_groups gp on gp.id = map.parent_id and gp.is_open = 'no'
        WHERE file.files_status =  1 AND file.group_file = 1
        AND file.user_id =  $profile_id
        AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = gp.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)
        )
        ORDER BY publish_date DESC
        LIMIT ?,?
        ";


      $vars = array($offset, $limit);
      $query = $this->db->query($sql, $vars);

      #echo $this->db->last_query();
      $data = $query->result_array();
      foreach($data as $key => $file) {
        if($file['file_size']) {
          $file_info = pathinfo($file['path']);

          $file_name = $file_info['filename'];
          $file_extension = isset($file_info['extension']) ? $file_info['extension'] : '';
          $data[$key]['view_path'] = base_url() . "files_downloads/" . urlencode($file_name) . "." . $file_extension;
        } else {
          $data[$key]['view_path'] = $file['path'];
        }
      }

      return $data;
      // return $query->result_array();

    } elseif($category == 'album') {

      $this->db->select("album.aid as id,album.featured,
                FROM_UNIXTIME(pictures.ctime) as publish_date,
                album.description  as description,
                album.title as title,
                'album' as category,
                concept.thumbnail as thumbnail,
                concept.name as concept_name,
                concept.slug as slug", false);
      $this->db->from('alb_albums album');
      $this->db->join('alb_pictures pictures', 'album.aid = pictures.aid');
      $this->db->join('ci_master_concept concept', 'concept.id = album.concept_id');
      $this->db->where('album.visibility', 0);
      $this->db->where('album.concept_id', $concept_id);
      $this->db->where('pictures.aid');
      $query = $this->db->get();
      return $query->result_array();

    } elseif(in_array($category, array('polls'))) {
      $sql = "(SELECT
                p.poll_id as id,p.featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                k.reply_username as username,
                NULL as message_id,
                NULL as message,
                k.display_name,
                k.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				NULL as concept_url,
                NULL as posted_concept_name,
                k.profile_pic as thumbnail,
                NULL as concept_name,
                NULL as slug
              FROM
                v2_polls p
                JOIN ci_users k ON p.created_by = k.id
                LEFT JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'yes'
			          WHERE p.created_by_type ='User' AND  p.created_by = $profile_id AND (CASE WHEN p.target_type = 'Group' THEN gp.name != '' ELSE 1=1 END)
				)UNION ALL(SELECT
        p.poll_id as id,p.featured,
                p.created as publish_date,
                NULL  as description,
                p.title as title,
                NULL as album_id,
                'polls' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
                NULL as you_like,
                NULL as total_likes,
                k.reply_username as username,
                NULL as message_id,
                NULL as message,
                k.display_name,
                k.profile_pic,
                NULL as concept,
                p.created_by as object_id,
                p.created_by_type as object_type,
                NULL as target_type,
                NULL as target_url,
                NULL as target_name,
				NULL as concept_url,
                NULL as posted_concept_name,
                k.profile_pic as thumbnail,
                NULL as concept_name,
                NULL as slug
        FROM
              v2_polls p
                JOIN ci_users k ON p.created_by = k.id
                JOIN ci_interest_groups gp on gp.id = p.target_id and p.target_type = 'Group' and p.created_by_type = 'User' and gp.is_open = 'no'
			          WHERE p.created_by_type ='User' AND p.created_by = $profile_id AND (CASE WHEN (SELECT count(o.object_id) FROM ci_map_objects o  WHERE o.object_type = 'User' and o.parent_type = 'Group' and o.parent_id = gp.id AND o.object_id = $user_id) > 0 THEN 1=1 ELSE 1!=1 END)
      )";

      $sql .= "order by publish_date DESC LIMIT ?, ?";
      $vars = array($offset, $limit);
      $query = $this->db->query($sql, $vars);

      $data = $query->result_array();

//        print_r($data);


      $return_data = array();

      foreach($data as $index => $item) {
        if($item['object_type'] == 'Concept') {
          $item['profile_pic'] = 'images/concepts/thumbs/' . $item['thumbnail'];
          $item['display_name'] = $item['concept_name'];
          $item['username'] = 'concepts/' . $item['slug'];
        } elseif($item['object_type'] == 'User') {
          $item['profile_pic'] = 'images/user-images/105x101/' . $item['profile_pic'];
        }
        $return_data[] = $item;
      }

      return $return_data;

    }

  }


}