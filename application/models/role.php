<?php
/**
  * This is a Role model.
  *
  * @author  Kunal Patil <kunal.patil@intelliswift.co.in>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Role extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_role_permissions');
	}
	 function change_permission($permission_id, $column_name) {
    $sql = "UPDATE ci_role_permissions SET " . $column_name . " = IF(" . $column_name . " = 1, 0, 1) WHERE id = ?";
    return $this->db->query($sql, array($permission_id));
  }
  
  function get_permission($role_id, $column_name = '') {
    $result = $this->db->get_where('ci_role_permissions', array('id' => $role_id));
    $data = array();
    $total_rows = $result->num_rows();
    if ($total_rows > 0) {
      foreach ($result->result_array() as $row)
      {
        if (!empty($column_name)) {
          $data = $row[$column_name];
        } else {
          $data = $row;
        }
      }
    }
    $result->free_result();
    return $data;
  }
    
  function remove_role($ids) {
    $data = array(
        'status' => 'inactive',
    );

    $this->db->where_in('id', $ids);
    return $this->db->update('ci_role_permissions', $data);
    ;
  }

  function getRoleKeys() {
    $this->db->select('id, key');
    $query = $this->db->get($this->table);
    return $query->result_array();
  }
}