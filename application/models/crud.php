<?php
/**
  * This is a Crud designed to achieve basic insert, delete and update.
  *
  * This also includes the ability to select 1 or more than one objects from the model
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */


Class Crud extends CI_Model {
  /** @type string|null Should contain database table name */
  protected $table = null;
  /** @type string|null Should contain database table name */
  protected $primary_key = null;

	function __construct() {
		parent::__construct();
		$this->db->query("SET time_zone='+4:00'");
	}
  /**
  * set Entity Database table
  * @return int void
  */
  public function setTable($table, $key = null)
  {
    $this->table = $table;
    if(isset($key))
    {
      $this->primary_key = $key;
    } else {
      $this->primary_key = 'id';
    }
  }
  /**
  * add a new Entry to Model
  * @return int $id
  */
  public function add(array $data)
  {
    $this->db->insert($this->table, $data);
    if($this->db->affected_rows() > 0)
    {
      return $this->db->insert_id();
    } else
    {
      $error = array();
      $error['err_code'] = $this->db->_error_number();
      if($this->db->_error_number() == '1062') //check if the entry is duplicate then return and error
      {
        $error['msg'] = 'Duplicate Record';
      }
      return $error;
    }
  }

  /**
  * Removes given entry from Entity Model
  * @return boolean
  */
  public function remove($id)
  {
    $this->db->where($this->primary_key, $id);
    if($this->db->delete($this->table))
    {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Removes all the entries from the Entity Model.
  *
  * @return void
  */
  public function removeAll()
  {
    $this->db->empty_table($this->table);
  }

  /**
  * Update the Entity record with provided data set and ID
  * @return array
  */
  public function update(array $data, $id)
  {
    $this->db->where($this->primary_key, $id);
    $this->db->update($this->table, $data);
    if($this->db->affected_rows() > 0)
    {
      return true;
    } else
    {
      $error = array();
      $error['err_code'] = $this->db->_error_number();
      return $error;
    }
  }

  /**
  * Returns a single Entity matched against provided ID
  * @return array
  */
  public function get($id)
  {
    if(!is_int($id))
    {
      return 'Not a valid Id';
    }
    $query = $this->db->get_where($this->table, array($this->primary_key => $id));
    return $query->result();
  }

  /**
  * Returns All or specific number of Entities from Model
  * @return array
  */
  public function getAll($limit = null, array $sortby = null, $offset = null, $name = null, $format = null)
  {
    if(isset($limit))
    {
      if(!is_int($limit))
      {
        return 'Limit must be a number';
      }
      $this->db->limit($limit,$offset);
    }
    if(isset($sortby))
    {
      foreach($sortby as $column => $order )
      {
        $this->db->order_by($column, $order);
      }

    } else {
      $this->db->order_by('name', 'Asc');
    }

	if(isset($name))
	{
		$this->db->like('name', $name, 'both');
	 $query = $this->db->get($this->table);
	}
	else
	{
    $query = $this->db->get($this->table);
	}
	if(isset($format))
	{
		if($format == 'array')
		{
			return $query->result_array();
		}
	}
    return $query->result();
  }

  /**
  * Returns Entity ID by name
  * @return int
  */
  function getIdByName($name)
  {
    $this->db->select($this->primary_key);
    $query = $this->db->get_where($this->table, array('name' => $name));
    if($data = $query->result_array())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0][$this->primary_key];
      }
    }
    return false;
  }

  /**
  * Returns Entity by name
  * @return array
  */
  function getByName($name)
  {
    $query = $this->db->get_where($this->table, array('name' => $name));
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        //Returning array here to keep a consistency accross system
        return (array)$data[0];
      }
    }
    return false;
  }

  /**
  * Returns Entity by filters provided / where clause
  * @return array
  */
  function getByFilters(array $filters, $columns = null)
  {
    if(isset($columns))
    {
      $this->db->select($columns, false);
    }
    $query = $this->db->get_where($this->table, $filters);
    if($data = $query->result())
    {

      if($this->db->affected_rows() > 0)
      {
        //Returning array here to keep a consistency accross system
        return (array)$data[0];
      }
    }
    return false;
  }

  /**
  * Returns Entity Column by filters provided / where clause
  * @return array
  */
  function getColumnByFilters($column, array $filters)
  {
    $this->db->select($column);
    $query = $this->db->get_where($this->table, $filters);
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        //Returning array here to keep a consistency accross system
        return $data[0]->$column;
      }
    }
    return false;
  }

  /**
  * Applies the Limit and Offset on given Transaction
  * This is a custom funciton to avoid writing control structure for checking limit and offset in every Entity's Model
  * @return void
  */
  function limits($limit, $offset)
  {
    if(isset($limit))
    {
      if(isset($offset))
      {
        $this->db->limit($limit, $offset);
      }
      else
      {
        $this->db->limit($limit);
      }
    }
  }

  /**
  * count all entries
  * @return int
  */
  function countAll($filters = null)
  {
    $this->db->select('count('.$this->primary_key.') as total');
    if(isset($filters))
    {
      $this->db->where($filters);
    }
    $query = $this->db->get($this->table);
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return false;
  }

  function countAllSearch($filters = null)
  {
    $this->db->select('count('.$this->primary_key.') as total');
    if(isset($filters))
    {
    $this->db->like('name', $filters, 'both');
    }

    $query = $this->db->get($this->table);
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return false;
  }

}