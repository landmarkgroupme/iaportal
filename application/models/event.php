<?php
/**
  * This is a Event model.
  *
  * @author Kunal Patil <kunal.patil@intelliswift.co.in>
  *
  * @since 1.0
  */
require_once dirname(__FILE__).'/crud.php';
Class Event extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_event');
	}
	public function getUpcommingList($concept, $limit = null, $offset = null)
	{

	$this->db->select('e.id, e.title, e.description, e.event_datetime, e.added_on, e.event_status,c.name, e.concept_id');
    $this->db->from("$this->table e");
	$this->db->join('ci_master_concept c', 'c.id  = e.concept_id');
    $this->db->where('e.concept_id', $concept);
	$this->db->where('event_status',1);
	$this->db->where('event_datetime >',time());
	$this->db->order_by('e.id', 'DESC');
	 $query = $this->db->get();
    /// echo $this->db->last_query();
    return $query->result_array();

	}
}