<?php
/**
  * This is a Category model.
  *
  * @author Kunal Patil <kunal.patil@intelliswift.co.in>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Feedback extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_feedback');
	}

}