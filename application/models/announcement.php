<?php
/**
  * This is a Announcement model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/entry.php';
Class Announcement extends Entry {
	function __construct() {
		parent::__construct();
    $this->mt_category = 'announcement';
	}

  /**
  * get List of Announcement
  *
  * Names in v1: news_model -> news_get_all
  * @return array
  */
  function getAnnouncements($limit = 10, $offset = 0, $concept = 5)
  {
    //setting Corporate as default concept
    return $this->getEntries($this->mt_category, $limit, $offset, $concept);
  }

  /**
  * get Announcement Details
  *
  * Names in v1: news_model -> news_get_details
  * @return array
  */
  function getAnnouncementDetails($id)
  {
    return $this->get($id);
  }

}