<?php
/**
  * This is a ItemImage model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class ItemImage extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_map_items_images');
	}

  /**
  * get Item Images
  *
  * Names in v1: market_place_model -> mp_item_images
  * @return array
  */
  function getItemImages($item_id) {
    $this->db->select('id, original_image');
    $this->db->from($this->table);
    $this->db->where('market_items_id', $item_id);
    $query = $this->db->get(); 
    return $query->result_array();
  }

}