<?php
/**
  * This is a UserFollow model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class UserFollow extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_users_following_users');
	}

  /**
  * get User Subcribed Concepts for Offers
  *
  * Names in v1: offers_model -> offer_user_list
  * @return array
  */

  function countUserFollowings($user_id)
  {
	$this->db->distinct();
    $this->db->select('f.following_user_id as total');
	$this->db->join('ci_users u', 'f.following_user_id = u.id');
    $this->db->where('user_id', $user_id);
    $this->db->where('f.follow_status', 1);
	$this->db->where('u.status', 'active');
    $query = $this->db->get("$this->table f");
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)count($data);
      }
    }
    return false;
  }
  
  
  function getUserFollowings($user_id)
  {
    $this->db->distinct();
    $this->db->select('following_user_id');
    $this->db->select('u.reply_username');
    $this->db->select('u.display_name');
    $this->db->select('u.profile_pic');
    $this->db->join('ci_users u', 'following_user_id = u.id');
    $this->db->where('user_id', $user_id);
    $this->db->where('follow_status', 1);
	$this->db->where('u.status', 'active');
	$this->db->order_by('u.display_name', 'ASC');
    $query = $this->db->get($this->table);
    //echo $this->db->last_query();
    $data = $query->result_array();
	
    return $data;
  }

  function countUserFollowers($user_id)
  {
    $this->db->select('f.id as total');
	$this->db->join('ci_users u', 'f.user_id = u.id');
    $this->db->where('f.following_user_id', $user_id);
    $this->db->where('f.follow_status', 1);
	$this->db->where('u.status', 'active');
    $query = $this->db->get("$this->table f");
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)count($data);
      }
    }
    return false;
  }
  
  function getUserFollowers($user_id)
  {
	$this->db->distinct();
    $this->db->select('user_id');
	$this->db->select('u.reply_username');
	$this->db->select('u.display_name');
	$this->db->select('u.profile_pic');
	$this->db->join('ci_users u', 'user_id = u.id');
    $this->db->where('following_user_id', $user_id);
	$this->db->where('follow_status', 1);
	$this->db->where('u.status', 'active');
	$this->db->order_by('u.display_name', 'ASC');
    $query = $this->db->get($this->table);
    $data = $query->result_array();
    return $data;
  }

  /**
  * Remove User form Offers
  *
  * Names in v1: offers_model -> flush_offer_user_map
  * @return array
  */
  function removeUserOffers($user_id) {
    $this->db->where('user_id', $user_id);
    if($this->db->delete($this->table))
    {
      return true;
    } else {
      return false;
    }
  }

  function unfollowUser($user_id, $unfollow_user){
  		$this->db->where('user_id', $user_id);
  		$this->db->where('following_user_id', $unfollow_user);
  		$this->db->delete($this->table);
		return $this->db->affected_rows();
	}
	function followUser($user_id, $follow_user){
		$details = array();
  		$details['follow_status'] = 1;
  		$details['following_user_id'] = $follow_user;
  		$details['added_on'] = time();
  		$details['user_id'] = $user_id;
		$this->db->insert($this->table, $details);
		return $this->db->affected_rows();
	}



}