<?php
/**
  * This is a Outlet model.
  *
  * @author  Sandeep Kumbhatil
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Outlets extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('ci_master_outlets');
	}
  function getOutletbyFilter($filters)
  {
    $query = $this->db->get_where($this->table, $filters);
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return $data;
      }
    }
  }
  function getAllManager($name){
    $this->db->select('u.first_name,u.last_name,u.id');
    $this->db->from($this->table. ' o');
    if($name)
    {
      $this->db->join('ci_users u', 'u.id = o.user_id');
      $this->db->like('reply_username', $name, 'both');
      $this->db->or_like('first_name', $name);
      $this->db->or_like('last_name', $name);
    }
    $this->db->group_by('u.id');
    $query = $this->db->get();
    return $query->result();
  }
  function addOutlet($data)
  {
    $filter = array('user_id'=>$data['user_id']);
    $count = $this->countAll($filter);
    if($count > 0)
    {
      return false;
    }
    else
    {
      return $this->add($data);
    }
  }
  function fetchOutletByTerritory($territory_id = false, $concept_id = false)
  {
    $data = array();
    $this->db->select('id,outlet_name');
    $this->db->from($this->table);
    if(isset($territory_id) && !empty($territory_id))
    {
      $this->db->where('territory_id',$territory_id);
    }
	if(isset($concept_id) && !empty($concept_id) && $concept_id != 0 )
    {
      $this->db->where('concept_id',$concept_id);
    }
    $query = $this->db->get();
    foreach($query->result() as $outlet)
    {
      $data[$outlet->id] = $outlet->outlet_name;
    }
    return $data;
  }
}