<?php
/**
  * This is a Album model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class Photo extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('alb_pictures', 'pic');
	}

  /**
  * returns an Album against its ID
  * @return array
  */
  function getLike($status_id, $user_id) {
	$this->db->select('u.id, u.first_name, u.email, u.reply_username');
    $this->db->from('ci_users u');
	$this->db->join('ci_likes b', 'b.user_id = u.id', 'left');
    $this->db->where('b.status_id', $status_id);
	$this->db->where('b.status_type', 'Photo');
	$this->db->where('b.user_id !=', $user_id);
	$this->db->order_by('b.id', 'DESC');
	$this->limits('2','0');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function get($id)
  {
    if(!is_int($id))
    {
      return 'Not a valid Id';
    }
    $this->db->select('pic.*');
    $this->db->from($this->table.' pic');
    $this->db->where('pic.pid', $id);
    $query = $this->db->get();

    $album = $query->result_array();
    return $album;
  }
  
  public function get_photo($id, $user_id)
  {
        
		$album_path = 'User_Photos_';
	$sql = "( SELECT
                b.pid as id,
                b.mtime as publish_date,
                b.caption  as description,
                b.title as title,
                CONCAT('phpalbum/albums/',b.filepath,b.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = b.pid and lll.user_id = '$user_id' limit 1) as you_like,
                b.total_likes,
                u.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                u.display_name,
                u.profile_pic,
				NULL as email,
				NULL as designation,
				CONCAT('phpalbum/albums/',b.filepath,'thumb_',b.filename) as thumbnail,
				NULL as concept_name,
                NULL as slug,
                b.owner_id as object_id,
                b.owner_type as object_type,
				NULL as path,
				NULL as target_url,
				NULL as target_name,
				NULL as target_id,
				NULL as target_type
              FROM
                ci_users u,
                alb_pictures b
              WHERE
                b.pid = '$id' and b.owner_id = u.id AND b.owner_type = 'User' AND b.filepath like 'User_Photos_%' AND b.target_id = '0'
            )
			UNION ALL(
              SELECT
                ab.pid as id,
                ab.mtime as publish_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = ab.pid and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                cu.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                cu.display_name,
                cu.profile_pic,
				NULL as email,
				NULL as designation,
				CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug,
                ab.owner_id as object_id,
                ab.owner_type as object_type,
				NULL as path,
				pconcept.slug as target_url,
				pconcept.name as target_name,
				ab.target_id,
				ab.target_type
              FROM
			   alb_pictures ab,
               ci_users cu,
			   ci_master_concept pconcept
              WHERE
                ab.pid = '$id' and ab.owner_id = cu.id AND pconcept.id = ab.target_id AND
                ab.owner_type = 'User' AND ab.target_type = 'Concept' AND ab.filepath Like 'Concept_Photos_%')
				UNION ALL(
              SELECT
                ab.pid as id,
                ab.mtime as publish_date,
                ab.caption  as description,
                ab.title as title,
                CONCAT('phpalbum/albums/',ab.filepath,ab.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = ab.pid and lll.user_id = '$user_id' limit 1) as you_like,
                ab.total_likes,
                cu.reply_username as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                cu.display_name,
                cu.profile_pic,
				NULL as email,
				NULL as designation,
				CONCAT('phpalbum/albums/',ab.filepath,'thumb_',ab.filename) as thumbnail,
                NULL as concept_name,
                NULL as slug,
                ab.owner_id as object_id,
                ab.owner_type as object_type,
				NULL as path,
				grp.id as target_url,
				grp.name as target_name,
				ab.target_id,
				ab.target_type
              FROM
			   alb_pictures ab,
               ci_users cu,
			   ci_interest_groups grp
              WHERE
                ab.pid = '$id' and ab.owner_id = cu.id AND grp.id = ab.target_id AND
                ab.owner_type = 'User' AND ab.target_type = 'Group' AND ab.filepath Like 'Group_Photos_%')
				UNION ALL(
              SELECT
                abc.pid as id,
                abc.mtime as publish_date,
                abc.caption  as description,
                abc.title as title,
                CONCAT('phpalbum/albums/',abc.filepath,abc.filename) as album_id,
                'photo' as category,
                NULL as file_unique_id,
                NULL as file_size,
                NULL as following_user_id,
				(select id from ci_likes lll where lll.status_id = abc.pid and lll.user_id = '$user_id' limit 1) as you_like,
                abc.total_likes,
                pcon.slug as username,
                NULL as message_id,
                NULL as message,
				NULL as user_id,
                pcon.name as display_name,
                pcon.thumbnail as profile_pic,
				NULL as email,
				NULL as designation,
				CONCAT('phpalbum/albums/',abc.filepath,'thumb_',abc.filename) as thumbnail,
                pcon.name as concept_name,
                pcon.slug as slug,
                abc.owner_id as object_id,
                abc.owner_type as object_type,
				NULL as path,
				pcon.slug as target_url,
				NULL as target_name,
				abc.target_id,
				abc.target_type
              FROM
			   alb_pictures abc,
               ci_users cu,
			   ci_master_concept pcon
              WHERE
                abc.pid = '$id' and abc.owner_id = pcon.id AND abc.owner_type = 'Concept' AND abc.filepath Like 'Concept_Photos_%')";
			$sql .= " order by publish_date DESC limit 1";

              $union_query = $this->db->query($sql);
              //echo $this->db->last_query();
        $result = $union_query->result_array();
        $data = array();

        foreach ($result as $index => $item) {
          if($item['object_type'] == 'Concept')
          {
		  if($item['category'] == 'photo'){
			$item['profile_pic'] = 'images/concepts/thumbs/'.$item['profile_pic'];
		  } else {
            $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
			}
           // $item['profile_pic'] = 'images/concepts/thumbs/'.$item['thumbnail'];
            $item['display_name'] = $item['concept_name'];
          }
          elseif($item['object_type'] == 'User')
          {
            $item['profile_pic'] = 'images/user-images/105x101/'.$item['profile_pic'];
			if($item['target_type']== 'Group')
            {
              
              $item['target_url']   = 'groups/'.$item['target_url'];
            }
            
          }
          $data[] = $item;
        }
        return $data;
	
  }
  public function deletePhoto($photo_id,$user_id = false)
  {
    $photo = $this->get((int)$photo_id);
    $photo = (array)$photo[0];
   /* if( !($photo['owner_id'] == $user_id) && $photo['owner_type'] == 'User' )
    {
      return 0;
    } */
    $this->db->where('pid', $photo_id);
    $this->db->delete($this->table);
    return $this->db->affected_rows();
  } 
}