<?php
/**
  * This is a Department model.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class SurveyUser extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('v2_survey_users');
	}

  function countUserSurveys($user_id)
  {
    $this->db->select('count(id) as total');
    $this->db->where_in('status', array('pending', 'inprogress'));
    $this->db->where('user_id', $user_id);


    $query = $this->db->get($this->table);

    //echo $this->db->last_query();

    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return 0;
  }

  function getUserSurveys($survey_id)
  {
    $this->db->select('display_name, user.id');
    $this->db->from('ci_users user');
    $this->db->join('v2_survey_users survey_user', 'survey_user.user_id = user.id');

    $this->db->where('user.status', 'active');
    $this->db->where('survey_user.survey_id', $survey_id);


    $query = $this->db->get();

    //echo $this->db->last_query();

    return $data = $query->result();
  }
  function getSurveyByUser($survey_id, $user_id){
    $this->db->select();
    $this->db->from($this->table);
    $this->db->where('survey_id',$survey_id);
    $this->db->where('user_id',$user_id);
    $query = $this->db->get();

    $data = $query->result();
    #echo "<pre>".print_r($data,"\n")."</pre>";
    if(isset($data) && !empty($data))
    {
      return $data[0];
    }
    else
    {
      return false;
    }
  }
  function removeSurveyUsers($survey_id)
  {
    $this->db->where('survey_id', $survey_id);
    $this->db->delete($this->table);
  }

  function changeStatus($status,$survey_id,$user_id)
  {
     $data = array('status'=>$status,'assigned_dttm'=>date('Y-m-d H:i:s',time()));
     $this->db->where('survey_id', $survey_id);
     $this->db->where('user_id', $user_id);
     $this->db->update($this->table, $data);
  }

    function checksurveyforuser($survey_id, $user_id)
  {
    $this->db->select('count(id) as total');
    $this->db->where_in('status', array('pending', 'inprogress' ,'completed'));
    $this->db->where('user_id', $user_id);
    $this->db->where('survey_id', $survey_id);


    $query = $this->db->get($this->table);

    //echo $this->db->last_query();

    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return 0;
  }

  function getAllUsersOfSurvey($survey_id)
  {
    $this->db->select('user_id');
    $this->db->where('survey_id', $survey_id);
    $query = $this->db->get($this->table);
    if($data = $query->result())
    {
      return $data;
    }
    return 0;
  }

  function removeSurveyUser($survey_id, $user_id)
  {
    $this->db->where('user_id', $user_id);
    $this->db->where('survey_id', $survey_id);
    $this->db->delete($this->table);
  }
}