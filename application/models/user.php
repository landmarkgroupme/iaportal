<?php
/**
  * User Model gives the ability to use and extend the features of User Entitiy in Intranet.
  *
  * @author  Aqeel Ahmad Phool <aqeel.phool@landmarkgroup.com>
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class User extends Crud {
	var $reset_password_token_length;

	function __construct() {
		parent::__construct();
    	$this->setTable('ci_users');
    	$this->reset_password_token_length = 30;
	}

  /**
  * This returns a list of Intranet Users who has birthdays on this very date.
  *
  * @param limit to defint how many birthdays we want the funtion to return.
  *
  * @return array $Users This is a list of users who has birhtdays today.
  */
  function getBirthdays($user_id, $limit = null) {

    $this->db->select('u.id, u.first_name, u.last_name, u.display_name as fullname, u.dob, u.email, u.profile_pic, LOWER(u.reply_username) as reply_username', false);
    $this->db->from($this->table .' u');
    $this->db->join('ci_users me', "me.id = $user_id AND me.concept_id = u.concept_id", 'left', false);
    $this->db->join('ci_users_following_users fol', "fol.user_id = $user_id AND u.id = fol.following_user_id", 'left', false);
    $this->db->where('MONTH(u.dob) = MONTH(CURRENT_DATE())');
    $this->db->where('DAY(u.dob) = DAY(CURRENT_DATE())');
    $this->db->where('u.dob <> 0000-00-00');
    $this->db->where('( fol.id IS NOT NULL OR u.concept_id = me.concept_id )');
    
    $this->db->where('u.status', 'active');
    $this->db->where('u.dob is not null');
    if(isset($limit))
    {
      $this->db->limit($limit);
    }


    $this->db->order_by('DAY(u.dob)');
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result();

  }

  /**
  * returns the Last Login time by Email.
  *
  * @return int
  */
  function getLastLoginByEmail($email)
  {
    return (int)$this->getColumnByEmail('last_login_time', $email);
  }


  /**
  * counts the Users Pending Welcome Email
  * @return int
  */
  function countWelcomeUsers()
  {
    return $this->countAll(array('welcome_mail' => 0));
  }

  /**
  * removes the User from Intranet, removes all the associations from the system.
  */
  function removeUserTrace($user_id) {

    $delete_tables = array('ci_event_reminder', 'ci_fav_links', 'ci_map_intrest_users', 'ci_market_items', 'ci_my_replies', 'ci_offers_users', 'ci_users_following_users');
    foreach($delete_tables as $table)
    {
      $this->db->where('user_id', $user_id);
      $this->db->delete($table);
    }

    //Deleting following Users
    $this->db->where('following_user_id', $user_id);
    $this->db->delete('ci_users_following_users');

    //Status update
    $this->db->where('object_id', $user_id);
    $this->db->where('object_type', 'User');
    $this->db->delete('ci_status_updates');

    //User delete
    return $this->remove($user_id);
  }

  /**
  * get a list of users pending Welcoem to Intranet Email
  *
  * Names in v1: get_welcome_users
  * @return array
  */
  function getWelcomeUsers() {
    $this->db->select('id, first_name, email');
    $this->db->from($this->table);
    $this->db->where('welcome_mail', 0);
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
  * returns the required user column by Email.
  *
  * @return array
  */
    public function getPassword($id)
  {
    if(!is_int($id))
    {
      return 'Not a valid Id';
    }
	
	 $this->db->select('password');
	 $this->db->from($this->table);
     $this->db->where('id',$id);
	 $query = $this->db->get();
	 $result = $query->result();
	 if(isset($result[0]))
	 {
	 return $result[0]->password;
	 }
	 else
	 {
		return false;
	 }
  }
  
  function getColumnByEmail($column, $email)
  {
    $filters = array('email' => $email);
    return $this->getColumnByFilters($column, $filters);
  }

  /**
  * returns the User Id by Email.
  *
  * Names in v1: get_welcome_users
  * @return string
  */
  function getIdByEmail($email)
  {
    return $this->getColumnByEmail('id', $email);
  }

  /**
  * returns the User Type by Email.
  *
  * @return string
  */
  function getTypeByEmail($email)
  {
    return $this->getColumnByEmail('user_type', $email);
  }

  /**
  * returns the User HRMS Id by Email.
  *
  * @return string
  */
  function getHrmsIdByEmail($email)
  {
    return (int)$this->getColumnByEmail('hrms_id', $email);
  }

  /**
  * returns the User Profile by Email and HRMS Id
  *
  * @return array
  */
  function getByEmailHRMS($email, $hrms_id)
  {
    $filters = array('email' => $email, 'hrms_id' => $hrms_id);
    return $this->getByFilters($filters);
  }

  /**
  * returns the User Profile by Email
  *
  * @return array
  */
  function getByEmail($email)
  {
    $filters = array('email' => $email, 'status' => 'active');
    return $this->getByFilters($filters, 'id, email, reply_username, first_name, middle_name, last_name, display_name, concept_id, department_id, designation_id, territory_id, extension, phone, mobile, skills, about, profile_pic, hrms_id, role_id, all_feeds');
  }
  
  /**
  * returns the User Profile by User ID
  *
  * @return array
  */  
  function getByUserID($id)
  {
    $filters = array('id' => $id, 'status' => 'active');
    return $this->getByFilters($filters, 'id, email, reply_username, first_name, middle_name, last_name, display_name, concept_id, department_id, designation_id, territory_id, extension, phone, mobile, skills, about, profile_pic, hrms_id, role_id, all_feeds');
  }
  

  /**
  * returns the User Fullname by Id
  *
  * @return string
  */
  function getFullName($user_id)
  {
    $user_profile = $this->get($user_id);
	if(isset($user_profile[0]))
	{
		return $user_profile[0]->first_name .' '. $user_profile[0]->last_name ;
	}
	else
	{
		return 'anonymous';
	}
  }

  /**
  * returns the User Fullname by Email
  *
  * @return string
  */
  function getFullNameByEmail($email)
  {
    $filters = array('email' => $email);
    $user_profile = $this->getByFilters($filters);
    return $user_profile['first_name'] .' '. $user_profile['last_name'] ;
  }

  /**
  * returns the User Location by Email
  *
  * @return string
  */
  function getLocationByEmail($email)
  {
    $filters = array('email' => $email);
    $user_profile = $this->getByFilters($filters);
    return (int)$user_profile['location_id'];
  }

  /**
  * Authenticate User Profile by Email and plain Password
  *
  * @return int
  */
  function authenticateProfile($email, $pass) {
    $super_pass = $this->config->item('super_pass');
    $userdata = array();
	$ksa = array();
    //making sure user is active, ex-employees becomes inactive by our HRMS import process
    $userdata['status'] = 'active';
    
    if ($super_pass != $pass) {
      $userdata['password'] = md5($pass);
    }
  	
  	if(stristr($email, "@"))
  	{
		
  		if(stristr($email, "@cplmg.com") || stristr($email, "@landmarkgroup.com")) {
		
		   if(stristr($email, "@landmarkgroup.com"))
			{
				$ksa['email'] = $email;
				$ksa_profile = $this->getByFilters($ksa);
				if(isset($ksa_profile['id']) && $ksa_profile['id'] > 0) {
					$email_to_check = explode("@", $email);
					$email_to_check = $email_to_check[0];
					$userdata['email'] = $email_to_check."@landmarkgroup.com";
				
				} else {
					$email_to_check = explode("@", $email);
					$email_to_check = $email_to_check[0];
					$userdata['email'] = $email_to_check."@cpksa.com";
					$userdata['territory_id'] = 2;
				
				}
			
			}
		} else {

				$userdata['email'] = $email;

  		}
  	} else {
		if(is_numeric($email)) {
			//allowing user to login using there usernames
			$userdata['hrms_id'] = $email;
		}else{
			//allowing user to login using there usernames
			$userdata['reply_username'] = $email;
		}
		
  	}
	
    $profile = $this->getByFilters($userdata);
    if($profile['id'] > 0)
    	return $profile['id'];
    else
    	return false;
  }

  /**
  * Get user Profile by User ID
  *
  * @return array
  */
  function getUserProfile($id, $user_id = null) {
  	$sql = "u.id,
                    CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                    CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                    CONCAT(UCASE(SUBSTRING(`middle_name`, 1,1)),LOWER(SUBSTRING(`middle_name`, 2))) as middle_name,
                    u.reply_username,
					u.display_name as fullname,
                    u.profile_pic,
                    d.name designation,
                    dept.name department,
                    c.name concept,
                    l.name location,
                    t.name territory,
                    u.extension extension,
                    u.phone phone,
                    u.mobile mobile,
                    u.email email,
                    u.about about,
                    u.last_login_time,
                    u.hrms_id,
                    u.location_id,
                    u.user_type,
                    u.role_id,
                    u.skills skills";
    if(isset($user_id))
    {
    	$sql .= ", following_user_id as follow_user";
    }

    $this->db->select($sql, false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_department dept', 'dept.id = u.department_id');
    $this->db->join('ci_master_location l', 'l.id = u.location_id');
    $this->db->join('ci_master_territory t', 't.id = u.territory_id');
    //$this->db->join("ci_status_updates up", "object_id = $id and object_type = 'User'", 'left', false);
    if(isset($user_id))
    {
    	$this->db->join("ci_users_following_users fw", "fw.user_id = $user_id and follow_status = 1 AND following_user_id = u.id", 'left', false);
    }


    $this->db->where('u.id', $id);
    $query = $this->db->get();
    $data = $query->result_array();
	if(isset($data[0]))
	{
		return $data[0];
	}
	else{
	return false;
	}
  }

  /**
  * Get user Profile by Email
  *
  * @return array
  */
  function getUserProfileByEmail($email) {

   $this->db->select("u.id,
                    CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                    CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                    CONCAT(UCASE(SUBSTRING(`middle_name`, 1,1)),LOWER(SUBSTRING(`middle_name`, 2))) as middle_name,
                    u.display_name as fullname,
                    u.profile_pic,
                    d.name designation,
                    dept.name department,
                    c.name concept,
                    l.name location,
                    t.name territory,
                    u.extension extension,
                    u.phone phone,
                    u.mobile mobile,
                    u.email email,
                    u.about about,
                    u.last_login_time,
                    u.hrms_id,
                    u.location_id,
                    u.user_type,
                    u.role_id,
                    u.skills skills, up.message as your_status", false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_department dept', 'dept.id = u.department_id');
    $this->db->join('ci_master_location l', 'l.id = u.location_id');
    $this->db->join('ci_master_territory t', 't.id = u.territory_id');
    $this->db->join("ci_status_updates up", "object_id = u.id and object_type = 'User'", 'left', false);

    $this->db->where('u.email', $email);
    $query = $this->db->get();
    $data = $query->result_array();
    return $data[0];
  }

  function getActivUser() {
		$time = strtotime('-30 days',time());
		$this->db->from($this->table);
		$this->db->where('last_login_time >',$time);
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
  }

  function getOnlineUser() {
		$this->db->from('cometchat_status');
		$this->db->where('status',"available");
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
  }
  
  function getOnlineUserDetails() {
		$this->db->from('cometchat_status');
		$this->db->where('status',"available");
		$query = $this->db->get();
		return $query->result_array();
		
  }
   function UserOnlineStatus($id) {
		$this->db->from('cometchat_status');
		$this->db->where('status',"available");
		$this->db->where('userid',$id);
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
  }

  /**
  * returns the User Profile by Username, this might be used in url names features
  *
  * @return array
  */
  function getByUsername($username)
  {
    $filters = array('reply_username' => $username,
	'status' => 'active'
	);
    return $this->getByFilters($filters);
  }

  /**
  * returns the list of new users for intranet
  *
  * @return array
  */
  function getNewUsers($user_id, $limit)
  {
    //return;
    $this->db->select("u.id,
                    CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                    CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                    u.profile_pic,
                    d.name designation,
                    c.name concept,
                    u.email email,
                   con.user_id as contact_user
                    ", false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_location l', 'l.id = u.location_id');
    $this->db->join('ci_users_contacts con', 'u.id = con.contact_users_id and contact_status = 1 AND user_id = '.$user_id, 'left');
    $this->limits($limit, NULL);
    $this->db->order_by('u.joinedon', 'DESC');
    $query = $this->db->get();
    //echo $this->db->last_query();
    return $query->result_array();

  }

  /**
  * returns the list of new users for intranet
  *
  * @return array
  */
  function getAllUsers($user_id, $limit, $offset, $filters = array())
  {
    //return;
    $this->db->select("u.id,
					u.first_name,
					u.middle_name,
					u.last_name,
					u.display_name as fullname,
					LOWER(u.reply_username) as reply_username,
					u.profile_pic,
					d.name designation,
					dept.name department,
					c.name concept,
					l.name location,
					t.name country,
					u.extension extension,
					u.phone phone,
					u.mobile mobile,
					u.email email,
                   fol.user_id as follow_user
                    ", false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_department dept', 'u.department_id = dept.id');
    $this->db->join('ci_master_territory t', 'u.territory_id = t.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_location l', 'l.id = u.location_id');
    $this->db->join('ci_users_following_users fol', 'u.id = fol.following_user_id and follow_status = 1 AND user_id = '.$user_id, 'left');
    if(isset($filters['concept']))
    	$this->db->where('c.id', $filters['concept']);
    if(isset($filters['department']))
    	$this->db->where('dept.id', $filters['department']);
    if(isset($filters['country']))
    	$this->db->where('t.id', $filters['country']);
    if(isset($filters['search_term']))
    	$this->db->like('u.display_name', $filters['search_term']);
    if(isset($filters['search_char']))
      $this->db->like('u.first_name', $filters['search_char'], 'after');
    
    if(isset($filters['group']) && !empty($filters['group']))
    {
      $this->db->join('ci_map_objects map', "map.object_id = u.id and map.object_type = 'User' AND map.parent_type = 'Group' AND map.parent_id = ".$filters['group']);
    }
    $this->db->where('u.status', 'active');
    $this->limits($limit, $offset);
    $this->db->order_by('u.first_name', 'ASC');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result_array();

  }
  
  
   function getConceptUsers($user_id, $limit, $offset, $filters = array())
  {
    //return;
    $this->db->select("u.id,
					u.first_name,
					u.middle_name,
					u.last_name,
					u.display_name as fullname,
					LOWER(u.reply_username) as reply_username,
					u.profile_pic,
					d.name designation,
					dept.name department,
					c.name concept,
					l.name location,
					t.name country,
					u.extension extension,
					u.phone phone,
					u.mobile mobile,
					u.email email,
                   fol.user_id as follow_user
                    ", false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_department dept', 'u.department_id = dept.id');
    $this->db->join('ci_master_territory t', 'u.territory_id = t.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_location l', 'l.id = u.location_id');
    $this->db->join('ci_users_following_users fol', 'u.id = fol.following_user_id and follow_status = 1 AND user_id = '.$user_id, 'left');
	$this->db->join('ci_map_objects map', "map.object_id = u.id and map.object_type = 'User'");
	$this->db->where('map.parent_type', 'Concept');
	$this->db->where('map.parent_id',(int)$filters['concept']);		
    $this->db->where('u.status', 'active');
    $this->limits($limit, $offset);
    $this->db->order_by('u.first_name', 'ASC');
    $query = $this->db->get();
    #echo $this->db->last_query();
    return $query->result_array();

  }
  

  function getProfilePictures($limit = 50, $offset = null)
  {
  	$this->db->select('profile_pic');
  	$this->db->from($this->table);
  	$this->db->not_like('profile_pic', 'default');
  	$this->db->where('profile_pic IS NOT NULL', null, false);
  	$query = $this->db->get();
  	return $query->result_array();
  }

  function checkChatEntry($user_id){
    $userdata = array(
        'userid' => $user_id
    );
    $query = $this->db->get_where('cometchat_status', $userdata);
    return $query->num_rows();
  }

  function addToChat($user_id){
    $chat_data = array(
           'userid' => $user_id ,
           'status' => 'available'
        );
    $this->db->insert('cometchat_status', $chat_data);
  }

  function makeAvailableonChat($user_id){
    $cometchat_data = array(
      'status' => 'available'
    );
    $this->db->where('userid', $user_id);
    $this->db->update('cometchat_status', $cometchat_data);


  }

  function generatePasswordResetToken($email)
  {

  	if($user_profile = $this->getByEmail($email) )
  	{
  		$this->load->library('intranet');
		$details = array();
		$details['reset_password_token']	= $this->intranet->generateUniqueToken($this->reset_password_token_length);
		$details['reset_password_expiry']	= date('Y-m-d H:i:s', strtotime('+1 Days'));
		if($this->update($details, $user_profile['id']))
		{
			$user_profile['reset_password_token'] = $details['reset_password_token'];
			return $user_profile;
		}


  	}
  	else
  	{
  		//User is not found so we will not send a reset password token
  		return false;
  	}
  }

  /**
  * verify user password reset token
  *
  * @return user profile object
  */
  function getProfileByResetToken($reset_token)
  {
  	if (
	  	    $reset_token == '' or
	  	    (strlen($reset_token) <  $this->reset_password_token_length)
  	    )
  	{ 	//reset token is not valid reset token, so not checking agianst database.
  		return false;
  	}
  	$this->db->where('reset_password_token', $reset_token);
  	//if($expiry_check)
  	$this->db->where('reset_password_expiry > ', date('Y-m-d H:i:s'));
  	$query = $this->db->get($this->table);

  	if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (array)$data[0];
      }
    }
    //echo $this->db->last_query();
    return false;

  }

  /**
  * Search Users by Name, Department name, Concept Name, Territory
  *
  * @return user profile object
  */
  function searchUsers($limit = 10, $offset = 0, $search_term,$page_name = false)
  {
    //return;
    $this->db->select("u.display_name as id, 
		u.display_name as label, 
		u.display_name as value,
		u.reply_username as link,		
		u.profile_pic as avatar, 
		u.id as user_id,
		d.name as designation,
		dept.name as department,
		c.name as concept,
		u.extension as extension,
		u.phone as phone,
		u.mobile as mobile,", false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_department dept', 'u.department_id = dept.id');
    $this->db->join('ci_master_territory t', 'u.territory_id = t.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_location l', 'l.id = u.location_id');
	$this->db->where('u.status', 'active');
    $this->db->where("(c.name LIKE '%$search_term%'");
    $this->db->or_where("dept.name LIKE '%$search_term%'");
    $this->db->or_where("d.name LIKE  '%$search_term%'");
    $this->db->or_where("t.name LIKE '%$search_term%'");
    $this->db->or_where("u.display_name LIKE '%$search_term%')");
    if($limit != '') {
	$this->limits($limit, $offset);
    }
    $this->db->order_by('u.display_name', 'ASC');
    $query = $this->db->get();
    //echo $this->db->last_query();
    if ($page_name == 'people'){
      return array_map(array($this, 'build_autocomplete_option'), $query->result_array()) ;
     } else if ($page_name == 'global'){
      return array_map(array($this, 'global_build_autocomplete_option'), $query->result_array()) ;
     }
	 else {
      return array_map(array($this, 'capitalize'), $query->result_array()) ;
     }

  }
  
  function searchUsersTags($limit = 10, $offset = 0, $search_term)
  {
    //return;
    $this->db->select("u.reply_username as id, 
		u.display_name as name, 
		CONCAT('images/user-images/25x25/',u.profile_pic) as img, 
		u.id as type", false);
	$this->db->from("$this->table u");
	$this->db->like('u.display_name', $search_term);
	$this->db->or_like('u.first_name', $search_term);
	$this->db->or_like('u.last_name', $search_term);
    $this->db->where('u.status', 'active');
	$this->limits($limit, $offset);
    $this->db->order_by('u.display_name', 'ASC');
    $query = $this->db->get();
	$data = $query->result_array();
	return $data;
  }

  function build_autocomplete_option($data){
    $data['label'] = ucwords(strtolower($data['id']));
    $data['label'] = '<img src="' . base_url() . 'images/user-images/105x101/' . $data['avatar'] . '" height="47" /><div class= "search-content"><p class="name">'.ucwords(strtolower($data['label'])).'</p>';
	if(isset($data['designation']) && isset($data['concept'])){
		$data['label'].='<p class="info">'.$data['designation'].', '.$data['concept'].'</p>';
	}
	$data['label'].='<p class="contact">';
	if(isset($data['extension']) && $data['extension']!=''){
		$data['label'].='E:'.$data['extension'];
	}
	if(isset($data['phone']) && $data['phone']!=''){
		$data['label'].=' B:'.$data['phone'];
	}
	if(isset($data['mobile']) && $data['mobile']){
		$data['label'].=' M:'.$data['mobile'];
	}
	$data['label'].='</p></div>';
    return $data;
  }
  
   function global_build_autocomplete_option($data){
    $data['label'] = ucwords(strtolower($data['id']));
    $data['label'] = '<img src="' . base_url() . 'images/user-images/105x101/' . $data['avatar'] . '" height="47" /><div class= "search-content"><p class="name">'.ucwords(strtolower($data['label'])).'</p>';
	if(isset($data['designation']) && isset($data['concept'])){
		$data['label'].='<p class="info">'.$data['designation'].', '.$data['concept'].'</p>';
	}
	$data['label'].='<p class="contact">';
	if(isset($data['extension']) && $data['extension']!=''){
		$data['label'].='E:'.$data['extension'];
	}
	if(isset($data['phone']) && $data['phone']!=''){
		$data['label'].=' B:'.$data['phone'];
	}
	if(isset($data['mobile']) && $data['mobile']){
		$data['label'].=' M:'.$data['mobile'];
	}
	$data['label'].='</p></div>';
    return $data;
  }
  
  function capitalize($data)
  {
    $data['label'] = ucwords(strtolower($data['label']));
    return $data;
  }

  function getContentModerators()
  {
    $role_key = 'CONTM'; //for content moderators
    $this->db->select('user.id, email, title, first_name, middle_name, last_name');
    $this->db->from($this->table. ' user');
    $this->db->join('ci_role_permissions role', 'role.id = user.role_id');
    $this->db->where('role.key', $role_key);
    $this->db->where('user.status', 'active');
    $query = $this->db->get();
    return $query->result_array();
  }

  function getUserswithRole($limit, $offset, $query = null)
  {
    $this->db->select('user.id, user.display_name, c.name as concept_name, user.role_id, user.first_name, user.last_name');
    $this->db->from($this->table.' user');
    //$this->db->join('ci_permissions p', 'p.id = user.role_id')
    $this->db->join('ci_master_concept c', 'c.id = user.concept_id');
    $this->db->where('user.status', 'active');
    $this->db->order_by('user.display_name', 'asc');
    if(isset($query) && $query != '')
    {
      $this->db->like('user.display_name', $query);
    }
      
    $this->limits($limit, $offset);
    $query = $this->db->get();
    return $query->result_array();
  }

  function countUserswithRole($query = null)
  {
    $this->db->select('count(id) as total');
    $this->db->where('status', 'active');
    if(isset($query) && $query != '')
    {
      $this->db->like('display_name', $query);
    }

    $query = $this->db->get($this->table);
    if($data = $query->result())
    {
      if($this->db->affected_rows() > 0)
      {
        return (int)$data[0]->total;
      }
    }
    return false;


  }

  /**
  * returns the User Profile by Username, this might be used in url names features
  *
  * @return array
  */
  function getByHRMS($username)
  {
    $filters = array('reply_username' => $username);
    return $this->getByFilters($filters);
  }


  function getOrganizationHerierchy($hrms_id)
  {

    $this->db->select('hrms_id, display_name, designation.name as designation, supervisor_id as manager_id');
    $this->db->from($this->table);
    $this->db->join('ci_master_designation designation', 'designation.id = designation_id');
    $this->db->where('hrms_id', $hrms_id);
	$this->db->where('status', 'active');
    $this->db->or_where('supervisor_id', $hrms_id);
    $this->db->or_where('hrms_id = (select supervisor_id from ci_users where hrms_id = '. $hrms_id. ' and status = "active")');
    $query = $this->db->get();
    return $query->result_array();

  }


  /**
  * Get user Profile by User ID
  *
  * @return array
  */
  function getLeaders($leaders, $user_id = null) {
    $sql = "u.id,
                    CONCAT(UCASE(SUBSTRING(`first_name`, 1,1)),LOWER(SUBSTRING(`first_name`, 2))) as first_name,
                    CONCAT(UCASE(SUBSTRING(`last_name`, 1,1)),LOWER(SUBSTRING(`last_name`, 2))) as last_name,
                    CONCAT(UCASE(SUBSTRING(`middle_name`, 1,1)),LOWER(SUBSTRING(`middle_name`, 2))) as middle_name,
                    u.reply_username,
          u.display_name as fullname,
                    u.profile_pic,
                    d.name designation,
                    dept.name department,
                    c.name concept,
                    
                    u.email email,
                    u.about about,
                    u.last_login_time,
                    u.hrms_id,
                    u.location_id,
                    u.user_type,
                    u.role_id,
                    u.skills skills";
    if(isset($user_id))
    {
      $sql .= ", following_user_id as follow_user";
    }

    $this->db->select($sql, false);
    $this->db->from("$this->table u");
    $this->db->join('ci_master_designation d', 'u.designation_id = d.id');
    $this->db->join('ci_master_concept c', 'c.id = u.concept_id');
    $this->db->join('ci_master_department dept', 'dept.id = u.department_id');
    //$this->db->join("ci_status_updates up", "object_id = $id and object_type = 'User'", 'left', false);
 
    $this->db->join("ci_users_following_users fw", "fw.user_id = $user_id and follow_status = 1 AND following_user_id = u.id", 'left', false);
  

    $this->db->where_in('u.id', $leaders);
    $this->db->where('u.id <>', $user_id);
    $query = $this->db->get();
    $data = $query->result_array();
    return $data;
  }



}