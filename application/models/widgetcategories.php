<?php
/**
  * This is a wid_categories model.
  *
  * @author Kunal Patil
  *
  * @since 1.0
  */

require_once dirname(__FILE__).'/crud.php';
Class WidgetCategories extends Crud {
	function __construct() {
		parent::__construct();
    $this->setTable('wid_categories');
	}
	
 function getByFilters(array $filters, $columns = null)
  {
    if(isset($columns))
    {
      $this->db->select($columns, false);
    }
    $query = $this->db->get_where($this->table, $filters);
    if($data = $query->result_array())
    {

      if($this->db->affected_rows() > 0)
      {
        //Returning array here to keep a consistency accross system
        return (array)$data;
      }
    }
    return false;
  }
}