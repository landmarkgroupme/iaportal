<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Time Ago
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Kunal Patil
 */

// ------------------------------------------------------------------------

/**
 * Time Ago
 *
 * Outputs the Time in seconds,minutes,hours,weeks,Months,Month & Year.
 *
 * @access	public
 * @param	Unix Timestamp
 * @return	string
 */	

	function progressive_image($file = null, $type = null){
	if(isset($file) && $file != '' && isset($type) && $type != '' && file_exists($file) ) {
		if($type == 'jpeg')
		{
			$im = imagecreatefromjpeg($file);
			imageinterlace($im, true);
			imagejpeg($im, $file);
			imagedestroy($im);
		}
		else if($type == 'jpg')
		{
			$im = imagecreatefromjpeg($file);
			imageinterlace($im, true);
			imagejpeg($im, $file);
			imagedestroy($im);
		}
		else if($type == 'png' )
		{
			$im = imagecreatefrompng($file);
			imageinterlace($im, true);
			imagepng($im, $file);
			imagedestroy($im);
		}
		else if( $type == 'gif' )
		{
			$im = imagecreatefromgif($file);
			imageinterlace($im, true);
			imagegif($im, $file);
			imagedestroy($im);
		}

		else if($type == 'bmp' )
		{
			$im = imagecreatefromwbmp($file);
			imageinterlace($im, true);
			imagewbmp($im, $file);
			imagedestroy($im);
		}
	}

}