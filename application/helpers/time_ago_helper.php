<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Time Ago
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */

// ------------------------------------------------------------------------

/**
 * Time Ago
 *
 * Outputs the Time in seconds,minutes,hours,weeks,Months,Month & Year.
 *
 * @access	public
 * @param	Unix Timestamp
 * @return	string
 */	
if (!function_exists('time_ago')){
	function time_ago($timestamp){
		$current_time = time();
		$difference = $current_time-$timestamp;
		$periods = array('second','minute','hour','day','week','month',date("M Y",$timestamp));
		$periods_length = array(1,60,3600,86400,604800,2630880,7892640);
		for($v = sizeof($periods_length)-1; ($v >= 0)&&(($no = $difference/$periods_length[$v])<=1); $v--); 
		if($v < 0){
			$v = 0;
		}  
		$no = floor($no); 
		if(($no <> 1) && ($v <6)){
			$periods[$v] .='s';
		} 
		if($v < 6){
			$periods[$v].=" ago";
			$formated_time=sprintf("%d %s ",$no,$periods[$v]);
		}else{
			$formated_time=sprintf("%s ",$periods[$v]);
		}
		return $formated_time;
	}
}


/* End of file time_ago_helper.php */
/* Location: ./application/helpers/time_ago_helper.php */