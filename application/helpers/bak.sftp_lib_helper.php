<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');
/**
 * CodeIgniter and SFTP/SSH library integration
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */
// ------------------------------------------------------------------------

/**
 * SFTP/SSH Library
 *
 * SFTP/SSH include files
 *
 * @access	public
 * @param	None
 * @return	none
 */

/* Windows
ini_set("include_path", ini_get("include_path") . PATH_SEPARATOR . str_replace("/", "\\", BASEPATH) . "contrib\\");
require_once ('sftp-lib/NET/SFTP.php');
*/

  ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.BASEPATH."/contrib/");
  require_once ('sftp-lib/Net/SFTP.php');


/* End of file sftp_lib_helper.php */
/* Location: ./application/helpers/sftp_lib_helper.php */