<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');
/**
 * CodeIgniter and SFTP/SSH library integration
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */
// ------------------------------------------------------------------------

/**
 * SFTP/SSH Library
 *
 * SFTP/SSH include files
 *
 * @access	public
 * @param	None
 * @return	none
 */

/* Windows
ini_set("include_path", ini_get("include_path") . PATH_SEPARATOR . str_replace("/", "\\", BASEPATH) . "contrib\\");
require_once ('sftp-lib/NET/SFTP.php');
*/

  ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.BASEPATH."/contrib/");
  require_once ('sftp-lib/Net/SFTP.php');
  function sftp_configuration(){
    $sftp_config = array();
    $sftp_config['sftp_host'] = '83.111.79.197';
    $sftp_config['sftp_user'] = 'dotahead';
    $sftp_config['sftp_pass'] = 'D0t@head';
    $sftp_config['remote_directory'] = '/out/';
    $sftp_config['local_directory'] = '/var/www/html/sftp_files/';
    $sftp_config['log_path'] = '/var/www/html/sftp_reports/';

    return $sftp_config;
  }

/* End of file sftp_lib_helper.php */
/* Location: ./application/helpers/sftp_lib_helper.php */