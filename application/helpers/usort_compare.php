<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 */

// ------------------------------------------------------------------------

/**
 * Usort Comapre function
 *
 *
 */	
if (!function_exists('cmp')){
	function cmp($a, $b)
       {
          return strcmp($a["name"], $b["name"]);
       } 
}


/* End of file usort_compare.php */
/* Location: ./application/helpers/usort_compare.php */