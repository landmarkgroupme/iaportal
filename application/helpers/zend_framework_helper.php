<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter and Zend framework integration
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */

// ------------------------------------------------------------------------

/**
 * Zend Framework
 *
 * Zend Framework include files
 *
 * @access	public
 * @param	None
 * @return	none
 */
 /* Just in case
	ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.str_replace("/", "\\", BASEPATH)."contrib\\");
	require_once ('Zend/Loader.php');
*/	
	ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.BASEPATH."/contrib/");
	require_once 'Zend/Loader.php';




/* End of file zend_framework.php */
/* Location: ./application/helpers/zend_framework_helper.php */