<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Authentication
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */

// ------------------------------------------------------------------------

/**
 * Authentication
 *
 * Checks the user login
 *
 * @access	public
 * @return	string
 */	
if (!function_exists('auth')){
	function auth(){
		$CI =& get_instance();
		if (! $CI->session->userdata('is_logged_in')) {
			$CI->session->set_userdata('requested_uri', $CI->uri->uri_string());
			if(!empty($CI->input->post('fullname'))){

				$token= array(
					'access_token' => $CI->input->post('access_token'),
					'expires_in' => $CI->input->post('expires_in'),
					'token_type' => $CI->input->post('token_type'),
					'refresh_token' => $CI->input->post('refresh_token')
				);

				$permissions= array(
					'id' => $CI->input->post('id'),
					'name' => $CI->input->post('name'),
					'key' => $CI->input->post('key'),
					'moderate' => $CI->input->post('moderate'),
					'status' => $CI->input->post('status')
				);
				$userdata = array(
		          'fullname' => stripslashes($CI->input->post('fullname')), //$this->user->fullname($email),
		          'is_logged_in' => $CI->input->post('is_logged_in'),
		          'userid' => $CI->input->post('userid'),
		          'useremail' => $CI->input->post('useremail'),
		          'userlocation' => $CI->input->post('userlocation'),
		          'userlastlogin' => time(),
		          'first_login_time' => $CI->input->post('first_login_time'),
		          'usertype' => $CI->input->post('usertype'), // $this->user->usertype($email),
		          'cfgpagelimit' => 20,
		          'permissions' => $permissions,
		          'token' => $token
		      );
				//$array = json_decode($userdata, true);
				//print_r($array);
			 	$CI->session->set_userdata($userdata);
			 	$CI->session->set_userdata('logged_user',$userdata);
			 	//exit;
			}
			else if($CI->input->is_ajax_request()){
					$response['json']['redirect_uri'] = site_url();
					$response['json']['ajax_redirect'] = 'yes';
					 $data = json_encode($response);
					echo $data;
					 exit;
			}else{
				$CI->session->set_userdata('submit_form', true);
				//redirect('http://iaportal.net/login');
			}
		}
	}
}



// if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// /**
//  * CodeIgniter Authentication
//  *
//  * @package		CodeIgniter
//  * @subpackage	Helpers
//  * @category	Helpers
//  * @author		Raghunath, Dotahead
//  */

// // ------------------------------------------------------------------------

// /**
//  * Authentication
//  *
//  * Checks the user login
//  *
//  * @access	public
//  * @return	string
//  */	
// if (!function_exists('auth')){
// 	function auth(){
// 		$CI =& get_instance();
// 		// $this->load->library('session');
// 		$CI->load->helper('cookie');

// 		$cookieData = get_cookie("intranet_ck");
// 		$session_array = unserialize( $cookieData );
// 		//echo "<pre>";
// 		//print_r($cookieData);
// 		//echo "<br>";
		

// 		//echo $session_array['is_logged_in'];
		
// 		if (! $session_array['is_logged_in']) {

// 			if(!empty($cookieData)) {
// 			//setcookie("intranet_ck_new",$cookieData);
			
// 			//echo "<pre>";
// 			//print_r($session_array);
// 			$userdata = array(
// 		          'fullname' => $session_array['fullname'], //$this->user->fullname($email),
// 		          'is_logged_in' => true,
// 		          'userid' =>  $session_array['userid'],
// 		          'useremail' =>$session_array['useremail'],
// 		          'userlocation' => $session_array['userlocation'],
	
// 		          'usertype' => $session_array['usertype'], // $this->user->usertype($email),
// 		          'cfgpagelimit' => 20,
		  
// 		      );

// 		      $CI->session->set_userdata($userdata);

// 		      //storing the whole intranet user in one variable instead of many indexes.
// 		      //this has to be strictly followed in intranet v2
// 		      $CI->session->set_userdata('logged_user', $userdata);
// 			  $CI->session->set_userdata('just_login', 'yes');




// 		}

		

// 			//$CI->session->set_userdata('requested_uri', $CI->uri->uri_string());
// 			//redirect('/login');
// 			//$CI->session->set_userdata('requested_uri', 'http://iaportal.local');
// 			//redirect('http://intranet.local/login?requested_uri=http://iaportal.local');
// 			$CI->session->set_userdata('requested_uri', 'http://audit.apps.landmarkgroup.com/');
// 			$rurl = $CI->uri->uri_string() . 'hi';
// 			$redirect_url = urlencode($rurl);
// 			//redirect('http://intranet.local/login?requested_uri='.$redirect_url);
// 		}
// 	}
// }


/* End of file auth_helper.php */
/* Location: ./application/helpers/auth_helper.php */