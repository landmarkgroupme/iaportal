<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Text to link
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */

// ------------------------------------------------------------------------

/**
 * Text to link
 *
 * Function reads the text and converts into clickable links or a mailto.
 *
 * @access	public
 * @param	Any Text
 * @return	string
 */	
if (!function_exists('text_to_link')){
	function text_to_link($text) {
		/* FOR PHP v <5.0
		$text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)','<a target="_blank" href="\\1">\\1</a>', $text);
		$text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)','\\1<a class="user-post-link" target="_blank" href="http://\\2">\\2</a>', $text);
		$text = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})','<a class="user-post-link" href="mailto:\\1">\\1</a>', $text);
		*/
		$text = preg_replace('/(?<!S)((http(s?):\/\/)|(www.))+([\w.1-9\&=#?\-~%;\/]+)/', '<a class="user-post-link" target="_blank" href="http$3://$4$5">http$3://$4$5</a>', $text);
		$text = preg_replace('/\b(\S+@\S+)\b/','<a class="user-post-link" href="mailto:\1";>\1</a>', $text);
		
		return $text;
	}  
}


/* End of file text_to_link_helper.php */
/* Location: ./application/helpers/text_to_link_helper.php */