<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter SMTP CONFIG
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */

// ------------------------------------------------------------------------

/**
 * Text to link
 *
 * Function reads the text and converts into clickable links or a mailto.
 *
 * @access	public
 * @param	None
 * @return	Array
 */	
if (!function_exists('smtp_config')){
	function smtp_config() {
		$config = array();
                $config['protocol'] = 'smtp';

                $config['smtp_host'] = '83.111.79.200'; //Live server relay IP
                $config['mailtype'] = "html";
		
		return $config;
	}  
}


/* End of file text_to_link_helper.php */
/* Location: ./application/helpers/smtp_config_helper.php */