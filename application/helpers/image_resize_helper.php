<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter and external Image resize class
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Raghunath, Dotahead
 */

// ------------------------------------------------------------------------

/**
 * Image resizer
 *
 * IMage resize include files
 *
 * @access	public
 * @param	None
 * @return	none
 */
/* Just in case 
	ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.str_replace("/", "\\", BASEPATH)."contrib\\");
	require_once ('resize_class.php');
*/	

	ini_set("include_path", ini_get("include_path").PATH_SEPARATOR.BASEPATH."/contrib/");
	require_once 'resize_class.php';



/* End of file image_resize_helper.php */
/* Location: ./application/helpers/image_resize_helper.php */